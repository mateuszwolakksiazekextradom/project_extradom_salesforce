({
	onAccountChange: function(component, event, helper) {
        var editOrder = component.get("v.editOrder");
        if (editOrder && !$A.util.isUndefinedOrNull(editOrder.AccountId)){
            helper.getContacts(component, editOrder.AccountId);
        }
    },
    onOrderLoaded: function(component, event, helper) {
        var changeType = event.getParams().changeType;
        if (changeType === "LOADED") {
            var editOrder = component.get("v.editOrder");
            var updateOrder = false;
            if ($A.util.isUndefinedOrNull(editOrder.ShippingCountry)) {
                editOrder.ShippingCountry = $A.get("$Label.c.OrderAddress_Poland");
                updateOrder = true;
            }
            if ($A.util.isUndefinedOrNull(editOrder.BillingCountry)) {
                editOrder.BillingCountry = $A.get("$Label.c.OrderAddress_Poland");
                updateOrder = true;
            }
            if (updateOrder) {
                component.set("v.editOrder", editOrder);
            }
        }
    },
    copyAddress: function(component, event, helper) {
		var editOrder = component.get("v.editOrder");
        editOrder.ShippingFirstName__c = editOrder.BillingFirstName__c;
        editOrder.ShippingLastName__c = editOrder.BillingLastName__c;
        editOrder.ShippingCompany__c = editOrder.BillingCompany__c;
        editOrder.ShippingPhone__c = editOrder.BillingPhone__c;
        editOrder.ShippingStreet = editOrder.BillingStreet;
        editOrder.ShippingCountry = editOrder.BillingCountry;
        editOrder.ShippingCity = editOrder.BillingCity;
        editOrder.ShippingPostalCode = editOrder.BillingPostalCode;
        component.set("v.editOrder", editOrder);
        window.setTimeout(
            $A.getCallback(function() {
                component.find("orderField").reduce(function (validFields, inputCmp) {
                    if (inputCmp) {
                        var name = inputCmp.get("v.name");
                        if (name && name.startsWith("shipping")) {
                            inputCmp.showHelpMessageIfInvalid();
                        }
                    }
                }, true);
            }), 500
        );
	},
    close: function(component, event, helper) {
        var closeModalEvent = $A.get("e.c:closeAddressesModal");
        if (closeModalEvent) {
            closeModalEvent.fire();
        }
        var closeQuickAction = $A.get("e.force:closeQuickAction");
        if (closeQuickAction) {
            closeQuickAction.fire();
        }
    },
    saveOrder: function(component, event, helper) {
        if(helper.validateOrderForm(component)) {
            component.find("addressData").saveRecord(function(saveResult) {
                if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                    var resultsToast = $A.get("e.force:showToast");
                    if (resultsToast) {
                        resultsToast.setParams({
                            "title": $A.get("$Label.c.OrderAddress_Saved"),
                            "message": $A.get("$Label.c.OrderAddress_SavedDesc"),
                            "type": "success"
                        });
                        resultsToast.fire();
                    }
                    var closeModalEvent = $A.get("e.c:closeAddressesModal");
        			if (closeModalEvent) {
                        closeModalEvent.fire();
                    }
                    var closeQuickAction = $A.get("e.force:closeQuickAction");
                    if (closeQuickAction) {
                        closeQuickAction.fire();
                    }
                } else if (saveResult.state === "INCOMPLETE") {
                    console.log($A.get("$Label.c.OrderAddress_UserOffline"));
                } else if (saveResult.state === "ERROR") {
                   	var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "title": $A.get("$Label.c.OrderAddress_Error"),
                        "message": $A.get("$Label.c.OrderAddress_SaveError"),
                        "type": "error"
                    });
                    resultsToast.fire();
                } else {
                    console.log("Unknown problem, state: " + saveResult.state +
                                ", error: " + JSON.stringify(saveResult.error));
                }
            });
        }
    },
    handleBillingCountryChange: function(component, event, helper) {
        window.setTimeout(
            $A.getCallback(function() {
                component.find("orderField").reduce(function (validFields, inputCmp) {
                    if (inputCmp && inputCmp.get("v.name") == "billingPostalCode") {
                        inputCmp.showHelpMessageIfInvalid();
                    }
                }, true);
            }), 500
        );
    },
    handleShippingCountryChange: function(component, event, helper) {
        window.setTimeout(
            $A.getCallback(function() {
                component.find("orderField").reduce(function (validFields, inputCmp) {
                    if (inputCmp && inputCmp.get("v.name") == "shippingPostalCode") {
                        inputCmp.showHelpMessageIfInvalid();
                    }
                }, true);
            }), 500
        );
    },
    onEmailClick: function(component, event, helper) {
        var val = event.target.text;
        component.find("orderField").reduce(function (validFields, inputCmp) {
            if (inputCmp && inputCmp.get("v.name") == "billingEmail") {
                inputCmp.set("v.value", val);
            }
        }, true);
    },
    onBillingPhoneClick: function(component, event, helper) {
        var val = event.target.text;
        component.find("orderField").reduce(function (validFields, inputCmp) {
            if (inputCmp && inputCmp.get("v.name") == "billingPhone") {
                inputCmp.set("v.value", val);
            }
        }, true);
    },
    onNoBillingEmail: function(component, event, helper) {
        var val = component.find("noBillingEmailInput").get("v.checked");
        component.find("orderField").reduce(function (validFields, inputCmp) {
            if (inputCmp && inputCmp.get("v.name") == "billingEmail") {
                inputCmp.set("v.disabled", val);
            } 
        }, true);
    },
    onShippingPhoneClick: function(component, event, helper) {
        var val = event.target.text;
        component.find("orderField").reduce(function (validFields, inputCmp) {
            if (inputCmp && inputCmp.get("v.name") == "shippingPhone") {
                inputCmp.set("v.value", val);
            }
        }, true);
    }
})