({
	validateOrderForm: function(component) {
    	var validOrder = true;
        var allValid = component.find("orderField").reduce(function (validFields, inputCmp) {
            if (inputCmp) {
                inputCmp.showHelpMessageIfInvalid();
            	return validFields && inputCmp.get("v.validity").valid;
            } else {
                return validFields
            }
        }, true);
        return allValid; 
	},
    getContacts: function(component, accountId) {
        var getContacts = component.get("c.getContacts");
        var foundContacts = [];
        var emails = [];
        var phones = [];
        
        getContacts.setParams({accountId: accountId});
        getContacts.setCallback(this, function(response){                          
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                foundContacts = response.getReturnValue();
                for (var i in foundContacts) {
                    if (!$A.util.isUndefinedOrNull(foundContacts[i].Email)) {
                        emails.push(foundContacts[i].Email);
                    }
                    
                    if (!$A.util.isUndefinedOrNull(foundContacts[i].Phone)) {
                        phones.push(foundContacts[i].Phone);
                    }
                }
                component.set("v.phones", phones);
                component.set("v.emails", emails);
            }
        });
        $A.enqueueAction(getContacts);
    }
})