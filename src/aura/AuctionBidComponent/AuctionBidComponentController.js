/**
 * Created by grzegorz.dlugosz on 21.05.2019.
 */
({
    doInit: function(component, event, helper) {
        helper.doInit(component, event, helper);
    },

    addSecondBid: function(component, event, helper) {
        helper.addSecondBid(component, event, helper);
    },
})