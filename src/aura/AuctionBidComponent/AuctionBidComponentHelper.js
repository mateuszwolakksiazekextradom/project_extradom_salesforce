/**
 * Created by grzegorz.dlugosz on 21.05.2019.
 */
({
    // Load Bid__c record of current user, under requested Offer__c.
    // Try to set Bid__c and minimum bid value. In case of errors, show appropriate toast message.
    doInit: function(component, event, helper) {
        var action = component.get("c.getUserBidsSrv");
        action.setParams({ auctionId : component.get("v.recordId") });

        //count time difference between pulling data from server, so set first time
        var time0 = performance.now();

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var responseWrapper = response.getReturnValue();

                //count time difference
                var timeDifference = performance.now() - time0;
                var currentServerTime =  new Date(responseWrapper.currentTime);
                //add difference milliseconds
                currentServerTime = currentServerTime.setMilliseconds(currentServerTime.getMilliseconds() + timeDifference);

                //set modified date
                component.set("v.serverCurrentTime", currentServerTime);
                component.set("v.bidRecordList", responseWrapper.bidList);
                component.set("v.auctionEnd", responseWrapper.auctionEnd);

                if (responseWrapper && responseWrapper.auctionEnd && responseWrapper.currentTime) {
                    // DONE, init countdown, unless its already set.
                    if ($A.util.isEmpty(component.get("v.timeLeftMs"))) {
                        this.runCountDown(component, event, helper);
                    }
                }

            }
            else if (state === "INCOMPLETE") {
                component.find('notifLib').showToast({
                    "variant": "error",
                    "title": "Wystąpił problem na stronie!",
                    "message": "Wystąpił problem na stronie. Proszę przeładować stronę, a w razie dalszych problemów skontaktować się z administratorem."
                });
            }
            else if (state === "ERROR") {
                // DEBUGGERS for errors
                /*var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }*/

                component.find('notifLib').showToast({
                    "variant": "error",
                    "title": "Wystąpił problem na stronie!",
                    "message": "Wystąpił problem na stronie. Proszę przeładować stronę, a w razie dalszych problemów skontaktować się z administratorem."
                });
            }
        });

        $A.enqueueAction(action);
    },

    runCountDown: function(component, event, helper) {
        var auctionEndDate = new Date(component.get("v.auctionEnd")).getTime();
        var now = new Date(component.get("v.serverCurrentTime")).getTime();

        var interval = setInterval($A.getCallback(function() {
            var oldDistance = component.get("v.timeLeftMs");

            if (oldDistance != 0) {
                var distance;

                if (oldDistance != null && oldDistance != '') {
                    distance = oldDistance - 1000;
                } else {
                    distance = auctionEndDate - now;
                }

                // Time calculations for days, hours, minutes and seconds
                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                // Display the result in the element with id="demo"
                var timeLeft =  hours + "h " + minutes + "m " + seconds + "s ";

                if (distance < 0) {
                    distance = 0;
                    timeLeft = 0 + "h " + 0 + "m " + 0 + "s ";
                }
                component.set("v.timeLeft", timeLeft);
                component.set("v.timeLeftMs", distance);
            } else {
                // refresh list
                var refreshEvent = $A.get("e.c:AuctionListComponentRefreshEvent");
                refreshEvent.fire();

                // stop the interval
                clearInterval(interval);

            }
        }), 1000);
    },

    // add second bid
    addSecondBid: function(component, event, helper) {
        var action = component.get("c.addSecondBidSrv");
        action.setParams({ auctionId : component.get("v.recordId") });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                // handle resp
                if (response.getReturnValue() === 'SUCCESS') {
                    this.doInit(component, event, helper);
                    // refresh the list
                    var appEvent = $A.get("e.c:AuctionListComponentRefreshEvent");
                    appEvent.fire();
                }
            }
            else if (state === "INCOMPLETE") {
                component.find('notifLib').showToast({
                    "variant": "error",
                    "title": "Wystąpił problem na stronie!",
                    "message": "Wystąpił problem na stronie. Proszę przeładować stronę, a w razie dalszych problemów skontaktować się z administratorem."
                });
            }
            else if (state === "ERROR") {
                // DEBUGGERS for errors
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
                if (errors && errors[0] && errors[0].message && errors[0].message.includes('Aukcja zakończona')) {
                    component.find('notifLib').showToast({
                        "variant": "error",
                        "title": "Aukcja zakończona!",
                        "message": "Aukcja dobiegła końca, dziękujemy."
                    });
                } else {
                    component.find('notifLib').showToast({
                        "variant": "error",
                        "title": "Wystąpił problem na stronie!",
                        "message": "Wystąpił problem na stronie. Proszę przeładować stronę, a w razie dalszych problemów skontaktować się z administratorem."
                    });
                }
            }
        });

        $A.enqueueAction(action);
    }
})