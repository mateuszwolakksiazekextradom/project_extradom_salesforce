({
    doInit: function(component, event, helper) {
        helper.getPaymentMethods(component);
        helper.getShippingMethods(component);
        helper.getShippingCostSettings(component);
    },
    openModal: function(component, event, helper) {
        component.set("v.openEditAddressesModal", true);
    },
    closeModal: function(component, event, helper) {
        component.set("v.openEditAddressesModal", false);
    },
    setShippingCost: function(component, event, helper) {
        var order = component.get("v.editOrder");
        var shippingCosts = component.get("v.shippingCosts");
        var cost = 0;
        if (!$A.util.isUndefinedOrNull(order.Payment_Method__c) && !$A.util.isEmpty(shippingCosts)
            	&& !$A.util.isUndefinedOrNull(order.Shipping_Method__c)) {
            for (var i in shippingCosts) {
                if (shippingCosts[i].Payment_Method__c == order.Payment_Method__c
                    && shippingCosts[i].Shipping_Method__c == order.Shipping_Method__c) {
                    console.log(shippingCosts[i].Shipping_Cost__c);
                    cost = shippingCosts[i].Shipping_Cost__c;
                }
            }
        }
        order.Shipping_Cost__c = cost;
        component.set('v.editOrder', order);
    },
    saveOrder: function(component, event, helper) {
        if(helper.validate(component)) {
            component.find("orderEditData").saveRecord(function(saveResult) {
                if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                } else if (saveResult.state === "INCOMPLETE") {
                    console.log($A.get("$Label.c.OrderAddress_UserOffline"));
                } else if (saveResult.state === "ERROR") {
                   	var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "title": $A.get("$Label.c.OrderAddress_Error"),
                        "message": $A.get("$Label.c.OrderAddress_SaveError"),
                        "type": "error"
                    });
                    resultsToast.fire();
                } else {
                    console.log("Unknown problem, state: " + saveResult.state +
                                ", error: " + JSON.stringify(saveResult.error));
                }
            });
        }
    },
    validateOrder: function(component, event, helper) {
        return helper.validate(component);
    },
    sendShippingCost: function(component, event, helper) {
        var order = component.get("v.editOrder");
        var changeOrderAmount = $A.get("e.c:changeOrderAmount");
        changeOrderAmount.setParams({"shipmentCost": order.Shipping_Cost__c});
        changeOrderAmount.fire();
    }
})