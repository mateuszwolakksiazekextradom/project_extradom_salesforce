({
	getPaymentMethods: function(component) {
		var getPaymentMethods = component.get("c.getPaymentMethods");
        var paymentMethods = [];
        
        getPaymentMethods.setCallback(this, function(response){                          
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                paymentMethods = response.getReturnValue();
                component.set("v.paymentMethods", paymentMethods);
            }
        });
        $A.enqueueAction(getPaymentMethods);
	},
    getShippingMethods: function(component) {
		var getShippingMethods = component.get("c.getShippingMethods");
        var shippingMethods = [];
        
        getShippingMethods.setCallback(this, function(response){                          
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                shippingMethods = response.getReturnValue();
                component.set("v.shippingMethods", shippingMethods);
            }
        });
        $A.enqueueAction(getShippingMethods);
	},
    getShippingCostSettings: function(component) {
		var getShippingCosts = component.get("c.getShippingCostSettings");
        var shippingCosts = [];
        
        getShippingCosts.setCallback(this, function(response){                          
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                shippingCosts = response.getReturnValue();
                component.set("v.shippingCosts", shippingCosts);
                component.set("v.showShippingCosts", true);
            }
        });
        $A.enqueueAction(getShippingCosts);
	},
    validate: function(component) {
        var validForm = true;
        var elems = component.find("validatedField");
        var allValid = true;
        if (elems) {
            if ($A.util.isArray(elems)) {
                allValid = elems.reduce(function (validFields, inputCmp) {
                	inputCmp.showHelpMessageIfInvalid();
                	return validFields && inputCmp.get("v.validity").valid;
            	}, true);
            } else if ($A.util.isObject(elems)) {
                elems.showHelpMessageIfInvalid();
                allValid = elems.get("v.validity").valid
            }

        }
        return allValid;
    }
})