({
    getParamValue: function(url, tabName, paramName) {
        var allParams = url.substr(url.indexOf(tabName) + tabName.length+1).split('&');
        var paramValue = '';
        for(var i=0; i<allParams.length; i++) {
            if(allParams[i].split('=')[0] == paramName) {
                paramValue = allParams[i].split('=')[1];
            }
        }
        console.log(paramValue);
        return paramValue;
    }
})