({
    doInit: function(component, event, helper) {
        var workspaceAPI = component.find("workspace");
        workspaceAPI.isConsoleNavigation().then(function(response) {
            if (response) {
            	workspaceAPI.getFocusedTabInfo().then(function(response) {
                    var focusedTabId = response.tabId;
                    workspaceAPI.getTabURL({
                    	tabId: focusedTabId
                	}).then(function(url) {
                        var recordId = helper.getParamValue(url, 'TestComp', 'recordId');
    					component.set('v.recordId', recordId);
                	});
               	})
                .catch(function(error) {
                    console.log(error);
                });

            } else {
                var recordId = helper.getParamValue(window.location.href, 'TestComp', 'recordId');
                component.set('v.recordId', recordId);
            }
        })
        .catch(function(error) {
            console.log(error);
        });
    },
	close: function(component, event, helper) {
        var workspaceAPI = component.find("workspace");
        workspaceAPI.getEnclosingTabId().then(function(tabId) {
            workspaceAPI.openTab({
                recordId: component.get('v.recordId'),
                focus: true
            }).then(function(response) {
            })
            .catch(function(error) {
                   console.log(error);
            });
            workspaceAPI.closeTab({tabId: tabId});
        })
        .catch(function(error) {
            console.log(error);
        });
    }
})