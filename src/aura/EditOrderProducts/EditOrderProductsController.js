({
    doInit: function (component, event, helper) {
        var workspaceAPI = component.find("workspace");
        workspaceAPI.isConsoleNavigation().then(function(response) {
            component.set("v.isInLightningConsole", response);
            if (response) {
                var setLightningTitle = component.get("c.setLightningTitle");
        		$A.enqueueAction(setLightningTitle);
            }
        })
        .catch(function(error) {
            component.set("v.isInLightningConsole", false);
            console.log(error);
        });
        
        if (sforce) {
            var isClassicConsole = sforce.console.isInConsole();
            component.set("v.isInClassicConsole", isClassicConsole);
            if (isClassicConsole) {
                var setClassicTitle = component.get("c.setClassicTitle");
                $A.enqueueAction(setClassicTitle);
            }
        }
    },
	close: function(component, event, helper) {
        var isInLightningConsole = component.get("v.isInLightningConsole");
        var isInClassicConsole = component.get("v.isInClassicConsole");
        if (isInLightningConsole) {
            var closeLightning = component.get("c.closeLightningConsole");
        	$A.enqueueAction(closeLightning);
        } else if (isInClassicConsole) {
            var closeClassic = component.get("c.closeClassicConsole");
        	$A.enqueueAction(closeClassic);
        } else {
            window.location.href = '/' + component.get("v.recordId");
        }
    },
    save: function(component, event, helper) {
        var saveOrderItems = $A.get("e.c:saveOrderItems");
        saveOrderItems.fire();
    },
    setErrorMessage: function(component, event, helper) {
        var errorMessage = event.getParam("errorMessage");
        component.set("v.errorMessage", errorMessage);
    },
    closeLightningConsole: function(component, event, helper) {
        var workspaceAPI = component.find("workspace");
        workspaceAPI.getEnclosingTabId().then(function(tabId) {
            workspaceAPI.openTab({
                recordId: component.get("v.recordId"),
                focus: true
            }).then(function(response) {
            })
            .catch(function(error) {
                console.log(error);
            });
            workspaceAPI.closeTab({tabId: tabId});
        })
        .catch(function(error) {
            console.log(error);
        });
    },
    closeClassicConsole: function(component, event, helper) {
        var recordId = component.get("v.recordId");
        sforce.console.getEnclosingPrimaryTabId(function (result) {
            var primaryTabId = result.id;
            sforce.console.getEnclosingTabId(function (result) {
            	var tabId = result.id;
                sforce.console.closeTab(tabId, function () {
                    sforce.console.focusPrimaryTabById(primaryTabId);
                });
        	});
        });
    },
    setClassicTitle: function(component, event, helper) {
        var orderNumber = component.get("v.orderNumber");
        sforce.console.setTabTitle(orderNumber + ' - ' + $A.get("$Label.c.ProductConfig_EditOrderProducts"));
    },
    setLightningTitle: function(component, event, helper) {
        var workspaceAPI = component.find("workspace");
        var orderNumber = component.get("v.orderNumber");
        workspaceAPI.getFocusedTabInfo().then(function(response) {
            var focusedTabId = response.tabId;
            workspaceAPI.setTabLabel({
                tabId: focusedTabId,
                label: orderNumber + ' - ' + $A.get("$Label.c.ProductConfig_EditOrderProducts")
            });
        })
        .catch(function(error) {
            console.log(error);
        });
    }
})