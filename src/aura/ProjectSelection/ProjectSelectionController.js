({
    onChangeSearchText: function(component, event, helper) {
        helper.onChangeSearchText(component, event);
    },
    onFocusSearchText: function(component, event, helper) {
       	component.set('v.focused', true);
    },
    onBlurSearchText: function(component, event, helper) {
        window.setTimeout(
            $A.getCallback(function() {
                if (component.isValid()) {
                    component.set('v.focused', false);
                }
            }), 200
        );
    },
    clearSearch: function(component, event, helper) {
        component.set('v.searchText', '');
        component.set('v.designs', []);
    },
    clearDesign: function(component, event, helper) {
        component.set('v.searchText', '');
        component.set('v.design', null);
        component.set('v.designs', []);
    },
    onDesignSelect: function(component, event, helper) {
        var selectedDesign = event.currentTarget.dataset.index;
        var designs = component.get('v.designs');
        component.set('v.design', designs[selectedDesign]);
        component.set('v.designs', []);
    }
})