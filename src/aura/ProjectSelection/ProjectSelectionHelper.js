({
    onChangeSearchText: function(component, event){
        var searchInput = component.get('v.searchText');
        if (!$A.util.isUndefinedOrNull(searchInput)) {
            var foundDesigns = [];
            var getDesigns = component.get("c.getDesigns");
                
            getDesigns.setParams({designName: searchInput});
            getDesigns.setCallback(this, function(response){                          
                var state = response.getState();
                if (component.isValid() && state === "SUCCESS") {
                    foundDesigns = response.getReturnValue();
                    component.set('v.designs', foundDesigns); 
                    
                }
            });
            $A.enqueueAction(getDesigns);
        }
    },
    onDesignSelect: function(component, event) {
        var designId = component.get('v.designId');
        if (!$A.util.isUndefinedOrNull(designId)) {
            var designProducts = [];
            var getDesignProducts = component.get("c.getDesignProducts");
                
            getDesignProducts.setParams({designId: designId});
            getDesignProducts.setCallback(this, function(response){                          
                var state = response.getState();
                if (component.isValid() && state === "SUCCESS") {
                    designProducts = JSON.parse(response.getReturnValue());
                    component.set('v.designProducts', designProducts); 
                }
            });
            $A.enqueueAction(getDesignProducts);
        }
    }
})