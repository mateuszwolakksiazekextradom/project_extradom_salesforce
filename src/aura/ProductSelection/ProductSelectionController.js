({
	doInit: function(component, event, helper) {
		var designId = component.get('v.designId');
        var productFamily = component.get('v.productFamily');
        if (!$A.util.isUndefinedOrNull(productFamily)) {
        	if (!$A.util.isUndefinedOrNull(designId)) {
            	helper.getDesignProducts(component, event);
                helper.getDesignAdditionalProducts(component, event);
            } else {
                helper.getFamilyProducts(component, event);
            }
        }
	}
})