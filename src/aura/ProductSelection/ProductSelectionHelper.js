({
	getDesignProducts: function(component, event) {
		var designId = component.get('v.designId');
        var pricebookId = component.get('v.pricebookId');
        if (!$A.util.isUndefinedOrNull(designId)) {
            var getDesignProducts = component.get("c.getDesignProducts");
            var designProducts = [];
                
            getDesignProducts.setParams({pricebookId: pricebookId, designId: designId});
            getDesignProducts.setCallback(this, function(response){                          
                var state = response.getState();
                if (component.isValid() && state === "SUCCESS") {
                    designProducts = response.getReturnValue();
                    component.set('v.designProducts', designProducts); 
                }
            });
            $A.enqueueAction(getDesignProducts);
        }
	},
    getDesignAdditionalProducts: function(component, event) {
		var designId = component.get('v.designId');
        var pricebookId = component.get('v.pricebookId');
        var productFamily = component.get('v.productFamily');
        if (!$A.util.isUndefinedOrNull(designId) && !$A.util.isUndefinedOrNull(productFamily)) {
            var getAdditionalProducts = component.get("c.getAdditionalProducts");
            var additionalProducts = [];
                
            getAdditionalProducts.setParams({pricebookId: pricebookId, designId: designId, family: productFamily});
            getAdditionalProducts.setCallback(this, function(response){                          
                var state = response.getState();
                if (component.isValid() && state === "SUCCESS") {
                    additionalProducts = response.getReturnValue();
                    component.set('v.familyProducts', additionalProducts); 
                }
            });
            $A.enqueueAction(getAdditionalProducts);
        }
	},
    getFamilyProducts: function(component, event) {
        var pricebookId = component.get('v.pricebookId');
        var productFamily = component.get('v.productFamily');
        if (!$A.util.isUndefinedOrNull(productFamily)) {
            var getFamilyProducts = component.get("c.getFamilyProducts");
            var familyProducts = [];
                
            getFamilyProducts.setParams({pricebookId: pricebookId, family: productFamily});
            getFamilyProducts.setCallback(this, function(response){                          
                var state = response.getState();
                if (component.isValid() && state === "SUCCESS") {
                    familyProducts = response.getReturnValue();
                    component.set('v.familyProducts', familyProducts); 
                }
            });
            $A.enqueueAction(getFamilyProducts);
        }
	}
})