({
	addProduct: function(component, event, helper) {
		var addProductEvent = $A.get("e.c:addProductConfiguration");
		addProductEvent.setParams({
            "pricebookEntryId" : event.getSource().get("v.value"),
            "designId": component.get("v.designId")
        });
		addProductEvent.fire();
	}
})