/**
 * Created by grzegorz.dlugosz on 28.05.2019.
 */
({
    doInit: function(component, event) {
        var action = component.get("c.getAuctionInfoSrv");
        action.setParams({ auctionId : component.get("v.recordId") });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var auctionRecord = response.getReturnValue();
                component.set("v.auctionRecord", auctionRecord);
            }
            else if (state === "INCOMPLETE") {
                component.find('notifLib').showToast({
                    "variant": "error",
                    "title": "Wystąpił problem na stronie!",
                    "message": "Wystąpił problem na stronie - Nie znaleziono szukanej aukcji. Proszę przeładować stronę, a w razie dalszych problemów skontaktować się z administratorem."
                });
            }
            else if (state === "ERROR") {
                // DEBUGGERS for errors
                /*var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }*/

                component.find('notifLib').showToast({
                    "variant": "error",
                    "title": "Wystąpił problem na stronie!",
                    "message": "Wystąpił problem na stronie - Nie znaleziono szukanej aukcji. Proszę przeładować stronę, a w razie dalszych problemów skontaktować się z administratorem."
                });
            }
        });

        $A.enqueueAction(action);
    }
})