({
    onItemRemove: function(component, event, helper) {
        var item = component.get("v.item");
        var removeItemEvent = component.getEvent("removeOrderItem");
        removeItemEvent.setParams({orderItemIndex: item.orderItem.Index__c});
        removeItemEvent.fire();
	},
    onListPriceChange: function(component, event, helper) {
        var item = component.get("v.item");
        if (item.product.maxDiscount) {
            item.minPrice = Math.round(item.orderItem.List_Price__c - item.product.maxDiscount * item.orderItem.List_Price__c/100);
        }
        item.unitPrice = item.unitPrice == 0 ? item.orderItem.List_Price__c : item.unitPrice;
        component.set("v.item", item);
        helper.recalculateTotalPrice(component);
        var action = component.get("c.onUnitPriceChange");
        $A.enqueueAction(action);
    },
    onQuantityChange: function(component, event, helper) {
        helper.recalculateTotalPrice(component);
    },
    onUnitPriceChange: function(component, event, helper) {
        var item = component.get("v.item");
        item.orderItem.Discount_Sales__c = item.orderItem.List_Price__c - item.unitPrice;
        var percentDiscount = !$A.util.isUndefinedOrNull(item.orderItem.List_Price__c) && item.orderItem.List_Price__c != 0 ?
            100*item.orderItem.Discount_Sales__c/item.orderItem.List_Price__c : 0;
        item.discountPercent = (Math.round(percentDiscount*Math.pow(10,2))/Math.pow(10,2)).toFixed(2);
        component.set("v.item", item);
        helper.recalculateTotalPrice(component);
    },
    onPercentDiscountChange: function(component, event, helper) {
        var item = component.get("v.item");
        item.unitPrice =
            Math.round(item.orderItem.List_Price__c - item.discountPercent * item.orderItem.List_Price__c/100);
        item.orderItem.Discount_Sales__c = item.orderItem.List_Price__c - item.unitPrice;
        var percentDiscount = !$A.util.isUndefinedOrNull(item.orderItem.List_Price__c) && item.orderItem.List_Price__c != 0 ?
            100*item.orderItem.Discount_Sales__c/item.orderItem.List_Price__c : 0;
        item.discountPercent = (Math.round(percentDiscount*Math.pow(10,2))/Math.pow(10,2)).toFixed(2);
        component.set("v.item", item);
        helper.recalculateTotalPrice(component);
    },
    onValueDiscountChange: function(component, event, helper) {
        var item = component.get("v.item");
        item.unitPrice = item.orderItem.List_Price__c - item.orderItem.Discount_Sales__c;
        var percentDiscount = !$A.util.isUndefinedOrNull(item.orderItem.List_Price__c) && item.orderItem.List_Price__c != 0 ?
            100*item.orderItem.Discount_Sales__c/item.orderItem.List_Price__c : 0;
        item.discountPercent = (Math.round(percentDiscount*Math.pow(10,2))/Math.pow(10,2)).toFixed(2);
        component.set("v.item", item);
        helper.recalculateTotalPrice(component);
    },
    setMinPrice: function(component, event, helper) {
        var item = component.get("v.item");
        item.unitPrice = item.minPrice;
        component.set("v.item", item);
        var action = component.get("c.onUnitPriceChange");
        $A.enqueueAction(action);
    },
    setActualPrice: function(component, event, helper) {
        var item = component.get("v.item");
        item.orderItem.List_Price__c = item.originalListPrice;
        component.set("v.item", item);
        var action = component.get("c.onListPriceChange");
        $A.enqueueAction(action);
    },
    setValueDiscount: function(component, event, helper) {
        var item = component.get("v.item");
        item.orderItem.Discount_Sales__c = item.orderItem.List_Price__c - item.minPrice;
        component.set("v.item", item);
        var action = component.get("c.onValueDiscountChange");
        $A.enqueueAction(action);
    },
    setPercentDiscount: function(component, event, helper) {
        var item = component.get("v.item");
        item.discountPercent = item.product.maxDiscount;
        component.set("v.item", item);
        var action = component.get("c.onPercentDiscountChange");
        $A.enqueueAction(action);
    },
    verifyPrices: function(component, event, helper) {
        var item = component.get("v.item");
        if (item && item.orderItem && item.orderItem.List_Price__c < item.unitPrice) {
            component.set('v.isUnitPriceWrong', true);
            component.set('v.unitPriceMessage', $A.get("$Label.c.ProductConfig_UnitPriceHigh"));
        } else if (item && item.orderItem && item.minPrice && item.unitPrice < item.minPrice) {
            component.set('v.isUnitPriceWrong', true);
            component.set('v.unitPriceMessage', $A.get("$Label.c.ProductConfig_UnitPriceLow"));
        } else {
            component.set('v.isUnitPriceWrong', false);
            component.set('v.unitPriceMessage', '');
        }
    },
    validateForm: function(component, event, helper) {
        return helper.validate(component);
    },
    changePriceOrder: function(component, event, helper) {
        var changeOrderAmount = $A.get("e.c:changeOrderAmount");
        changeOrderAmount.fire();
    }
})