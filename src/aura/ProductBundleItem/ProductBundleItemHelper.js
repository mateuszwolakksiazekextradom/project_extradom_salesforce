({
    recalculateTotalPrice: function(component) {
        var item = component.get("v.item");
        var supplierDiscount = item.orderItem.Discount_Supplier__c ? item.orderItem.Discount_Supplier__c : 0;
        var internalDiscount = item.orderItem.Discount_Internal__c ? item.orderItem.Discount_Internal__c : 0;
        item.totalPrice = (item.unitPrice - supplierDiscount - internalDiscount)  * item.orderItem.Quantity;
        component.set("v.item", item);
    },
    validate: function(component) {
    	var validForm = true;
        var elems = component.find("validatedField");
        var allValid = true;
        if (elems) {
            if ($A.util.isArray(elems)) {
                allValid = elems.reduce(function (validFields, inputCmp) {
                	inputCmp.showHelpMessageIfInvalid();
                	return validFields && inputCmp.get("v.validity").valid;
            	}, true);
            } else if ($A.util.isObject(elems)) {
                elems.showHelpMessageIfInvalid();
                allValid = elems.get("v.validity").valid
            }

        }
        return allValid; 
	}
})