({
	doInit: function(component, event, helper) {
        helper.getVersions(component);
        helper.getDiscountTypes(component);
        helper.getOrderItems(component);
	},
    addProduct: function(component, event, helper) {
		var pricebookEntryId = event.getParam("pricebookEntryId");
        var designId = event.getParam("designId");
        var shouldAddLine = true;
        var orderProducts = component.get("v.orderProducts");
        if ($A.util.isUndefinedOrNull(orderProducts)) {
            orderProducts = [];
        }
        if ($A.util.isUndefinedOrNull(designId)) {
            for (var i in orderProducts) {
                if (orderProducts[i].pricebookEntryId == pricebookEntryId) {
                    orderProducts[i].orderItem.Quantity = orderProducts[i].orderItem.Quantity + 1;
                    shouldAddLine = false;
                }
            }
        }
        if (shouldAddLine) {
            helper.addProductWithDetails(component, pricebookEntryId, designId);
        } else {
            component.set("v.orderProducts", orderProducts);
        }
	},
    handleOrderItemRemoveEvent: function(component, event, helper) {
        var index = event.getParam("orderItemIndex");
        var orderProducts = component.get("v.orderProducts");
        orderProducts.splice(index-1, 1);
        for (var i = index-1; i < orderProducts.length; i++ ) {
            orderProducts[i].orderItem.Index__c = i + 1;
        }
        component.set("v.orderProducts", orderProducts);
    },
    onSave: function(component, event, helper) {
        var isValid = true;
        var children = component.find("orderItem");
        var shipment = component.find("shipment");
        if (children) {
            if ($A.util.isArray(children)) {
                for (var i in children) {
                    isValid = isValid && children[i].validateItem();
                }
            } else {
                isValid = isValid && children.validateItem();
            }
        }
        isValid = isValid && shipment.validate();
        if (isValid) {
            shipment.save();
            helper.save(component);
        }
    },
    calculateTotalCost: function(component, event, helper) {
        var shipmentCost = event.getParam("shipmentCost");
        if (!$A.util.isUndefinedOrNull(shipmentCost)) {
            component.set("v.shipmentCost", shipmentCost);
        }
        helper.calculateTotalCost(component);
    }
})