({
    getVersions: function(component) {
		var getVersions = component.get("c.getVersionValues");
        var versions = [];
        
        getVersions.setCallback(this, function(response){                          
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                versions = response.getReturnValue();
                component.set("v.productVersions", versions);
            }
        });
        $A.enqueueAction(getVersions);
	},
    getDiscountTypes: function(component) {
		var getDiscountTypes = component.get("c.getDiscountTypes");
        var discountTypes = [];
        
        getDiscountTypes.setCallback(this, function(response){                          
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                discountTypes = response.getReturnValue();
                component.set("v.discountTypes", discountTypes);
            }
        });
        $A.enqueueAction(getDiscountTypes);
	},
    getOrderItems: function(component) {
		var getOrderItems = component.get("c.getOrderItems");
        var orderItems = [];
        
        var self = this;
        getOrderItems.setParams({orderId: component.get("v.recordId")});
        getOrderItems.setCallback(this, function(response){                          
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                orderItems = response.getReturnValue();
                for (var i in orderItems) {
                    if (orderItems[i].orderItem.Index__c == undefined) {
                        orderItems[i].orderItem.Index__c = i + 1;
                    }
                }
                component.set("v.orderProducts", orderItems);
                self.calculateTotalCost(component);
            }
        });
        $A.enqueueAction(getOrderItems);
	},
    save: function(component) {
        var saveOrderItems = component.get("c.saveOrderItems");
        var orderItems = component.get("v.orderProducts");
        var recordId = component.get("v.recordId");
        var orderProductList = [];
        for (var i in orderItems) {
            if (orderItems[i].orderItem.OrderId && orderItems[i].orderItem.Quantity > 0) {
                orderProductList.push(orderItems[i].orderItem);
            }
        }
        saveOrderItems.setParams({orderId: recordId, orderProducts: JSON.stringify(orderProductList)});
        
        saveOrderItems.setCallback(this, function(response){                          
            var state = response.getState();
            var responseValue = response.getReturnValue();
            if (component.isValid() && state === "SUCCESS" && $A.util.isUndefinedOrNull(responseValue)) {
                var cancelConfigurator = $A.get("e.c:cancelProductConfigurator");
        		cancelConfigurator.fire();
            } else if (state === "ERROR" || !$A.util.isUndefinedOrNull(responseValue)) {
                var showConfigurationError = $A.get("e.c:showConfigurationError");
                showConfigurationError.setParams({ "errorMessage" : response.getReturnValue() });
        		showConfigurationError.fire();
            }
        });
        $A.enqueueAction(saveOrderItems);
    },
	addProductWithDetails: function(component, pricebookEntryId, designId) {
		var prepareOrderItem = component.get("c.prepareOrderItem");
        var orderProducts = component.get("v.orderProducts");
        var orderId = component.get("v.recordId");
        var self = this;
        if ($A.util.isUndefinedOrNull(orderProducts)) {
            orderProducts = [];
        }
        
        prepareOrderItem.setParams({orderId: orderId, pricebookEntryId: pricebookEntryId, designId: designId});
        prepareOrderItem.setCallback(this, function(response){                          
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var orderItemsPrepared = response.getReturnValue();
                orderProducts = self.addOrderProducts(component, orderProducts, orderItemsPrepared);
                self.sortOrderProducts(component, orderProducts);
                self.calculateTotalCost(component);
            } else if (state === "ERROR") {
                var errors = response.getError();
                var showConfigurationError = $A.get("e.c:showConfigurationError");
                if (errors) {
                    var message = '';
                    if (errors[0] && errors[0].message) {
                        message = errors[0].message;
                    } else {
                        message = 'Unknown error';
                    }
                    showConfigurationError.setParams({ "errorMessage" : message });
                } else {
					showConfigurationError.setParams({ "errorMessage" : 'Unknown error' });
                }
                resultsToast.fire();
                var showConfigurationError = $A.get("e.c:showConfigurationError");
        		showConfigurationError.fire();
            }
        });
        $A.enqueueAction(prepareOrderItem);
	},
    addOrderProducts: function(component, orderProducts, newOrderProducts) {
        for (var i in newOrderProducts) {
        	var newOrderProduct = newOrderProducts[i];
            var addLine = true;
            for (var iterator in orderProducts) {
                if (orderProducts[iterator].pricebookEntryId == newOrderProduct.pricebookEntryId
                    && $A.util.isUndefinedOrNull(newOrderProduct.design)) {
                    orderProducts[iterator].orderItem.Quantity = orderProducts[iterator].orderItem.Quantity + 1;
                    addLine = false;
                    break;
                }
             }
             if (addLine) {
                orderProducts.push(newOrderProduct);
             }
        }
        return orderProducts;
    },
    sortOrderProducts: function(component, orderProducts) {
        orderProducts.sort(function(a,b){
        	var design1 = a.design && a.design.name ? a.design.name.toLowerCase(): null;
            var design2 = b.design && b.design.name ? b.design.name.toLowerCase(): null;
            
            var name1 = a.product.name.toLowerCase();
            var name2 = b.product.name.toLowerCase();
            
            var family1 = a.product.typeOrder;
            var family2 = b.product.typeOrder;
            
            if (design1 < design2 || (design1 !== null && design2 === null)) return -1;
            if (design1 > design2 || (design1 === null && design2 !== null)) return 1;
            if (family1 < family2 || (family1 !== null && family2 === null)) return -1;
            if (family1 > family2 || (family1 === null && family2 !== null)) return 1;
            if (name1 < name2 || (name1 !== null && name2 === null)) return -1;
            if (name1 > name2 || (name1 === null && name2 !== null)) return 1;
            return 0;
        });
        for (var i in orderProducts) {
            orderProducts[i].orderItem.Index__c = parseInt(i) + 1;
        }
        component.set("v.orderProducts", orderProducts);
    },
    calculateTotalCost: function (component) {
        var orderProducts = component.get("v.orderProducts");
        var totalCost = 0;
        for (var i in orderProducts) {
            if (!$A.util.isUndefinedOrNull(orderProducts[i].totalPrice)) {
                totalCost = totalCost + orderProducts[i].totalPrice;
            }
        }
        component.set("v.totalCost", totalCost);
    }
})