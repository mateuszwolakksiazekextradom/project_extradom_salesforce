/**
 * Created by grzegorz.dlugosz on 24.05.2019.
 */
({
    doInit: function(component, event, helper){
        var bidRecord = component.get("v.bidRecord");

        var minBid = 100;

        if (bidRecord.Value__c) {
            minBid = bidRecord.Value__c + 100;
        }

        if (bidRecord.Offer__r && bidRecord.Offer__r.Minimum_Value__c) {
            if (bidRecord.Offer__r.Minimum_Value__c > minBid) {
                minBid = bidRecord.Offer__r.Minimum_Value__c;
            }
        }

        component.set("v.newBid", minBid);
        component.set("v.minValue", minBid);
    },

    // Check if connected input is valid and if it is, show the pop-up
    showPopup: function(component, event, helper) {
        var validity = component.find("bidInput").get("v.validity");

        if (validity.valid) {
            component.set("v.showPopup", true);

            window.setTimeout(
                $A.getCallback(function () {
                    var el = document.getElementById("myDiv");
                    el.focus();
                }), 1
            );
        }
    },

    // Hides the pop-up
    hidePopup: function(component, event, helper) {
        component.set("v.showPopup", false);
    },

    // Saves new Value__c of bid. Shows toast on success / errors
    handleNewBid: function(component, event, helper) {
        // this parse is needed because of SFDC issue. If the input is binded with decimal attribute, after the user
        // changes input, the decimal value will be treated as String in JS controllers.
        var newBid = parseFloat(component.get("v.newBid"));
        var bidRec = component.get("v.bidRecord");
        bidRec.Value__c = newBid;

        var action = component.get("c.saveUserBidSrv");
        action.setParams({ bidRecord : bidRec });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                if (response.getReturnValue() === 'SUCCESS') {
                    component.set("v.bidRecord", bidRec);
                    newBid += 100;
                    component.set("v.newBid", newBid);
                    component.set("v.minValue", newBid);

                    component.set("v.showPopup", false);

                    component.find('notifLib').showToast({
                        "variant": "success",
                        "title": "Licytacja przebiegła pomyślnie",
                        "message": "Zalicytowana przez Ciebie kwota to: " + bidRec.Value__c + "zł"
                    });

                    // refresh the list
                    var appEvent = $A.get("e.c:AuctionListComponentRefreshEvent");
                    appEvent.fire();
                }
            }
            else if (state === "INCOMPLETE") {
                component.find('notifLib').showToast({
                    "variant": "error",
                    "title": "Wystąpił problem na stronie!",
                    "message": "Wystąpił problem na stronie. Proszę przeładować stronę, a w razie dalszych problemów skontaktować się z administratorem."
                });
            }
            else if (state === "ERROR") {
                // DEBUGGERS for errors
                var errors = response.getError();
                /*if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }*/
                if (errors && errors[0] && errors[0].message && errors[0].message.includes('Aukcja zakończona')) {
                    component.find('notifLib').showToast({
                        "variant": "error",
                        "title": "Aukcja zakończona!",
                        "message": "Aukcja dobiegła końca, dziękujemy."
                    });
                } else {
                    component.find('notifLib').showToast({
                        "variant": "error",
                        "title": "Wystąpił problem na stronie!",
                        "message": "Wystąpił problem na stronie. Proszę przeładować stronę, a w razie dalszych problemów skontaktować się z administratorem."
                    });
                }
            }
        });

        $A.enqueueAction(action);
    },
})