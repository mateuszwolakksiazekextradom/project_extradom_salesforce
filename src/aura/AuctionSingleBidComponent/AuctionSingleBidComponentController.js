/**
 * Created by grzegorz.dlugosz on 24.05.2019.
 */
({
    doInit: function(component, event, helper) {
        helper.doInit(component, event, helper);
    },

    showPopup: function(component, event, helper) {
        helper.showPopup(component, event, helper);

    },

    hidePopup: function(component, event, helper) {
        helper.hidePopup(component, event, helper);
    },

    handleNewBid: function(component, event, helper) {
        helper.handleNewBid(component, event, helper);
    },

    keyCheck: function(component, event, helper) {
        if (event.which == 27) {
            helper.hidePopup(component, event, helper);
        } else if (event.which == 13) {
            helper.handleNewBid(component, event, helper);
        }
    },

    validateBid: function(component, event, helper) {
        var bidInput = component.find("bidInput");
        var value = bidInput.get("v.value");

        var offerMinBid = component.get("v.bidRecord.Offer__r.Minimum_Value__c");

        if (value < offerMinBid) {
            bidInput.setCustomValidity("Twoja oferta jest zbyt niska. Minimalna wartość oferty wynosi " + offerMinBid + "zł");
        } else if (value > offerMinBid && value < component.get("v.minValue")) {
            bidInput.setCustomValidity("Podana kwota jest niższa niż Twoja aktualna oferta");
        } else {
            bidInput.setCustomValidity("");
        }

        bidInput.reportValidity();
    }
})