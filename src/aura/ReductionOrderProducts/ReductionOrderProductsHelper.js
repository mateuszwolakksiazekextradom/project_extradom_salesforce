({
	getReductionOrderItems: function(component) {
		var getReductionOrderItems = component.get("c.getReductionOrderItems");
        var reductionItems = [];
        
        var self = this;
        getReductionOrderItems.setParams({orderId: component.get("v.recordId")});
        getReductionOrderItems.setCallback(this, function(response){                          
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                reductionItems = response.getReturnValue();
                component.set("v.reductionItems", reductionItems);
            }
        });
        $A.enqueueAction(getReductionOrderItems);		
	},
    save: function(component) {
        var saveReductionOrderItems = component.get("c.saveReductionOrderItems");
        var reductionItems = component.get("v.reductionItems");
        var recordId = component.get("v.recordId");
        var orderItemsList = [];
        for (var i in reductionItems) {
            if (reductionItems[i].orderItem.Quantity > 0) {
                reductionItems[i].orderItem.Quantity = -reductionItems[i].orderItem.Quantity;
                orderItemsList.push(reductionItems[i].orderItem);
            }
        }
        saveReductionOrderItems.setParams({orderId: recordId, reductionOrderItems: JSON.stringify(orderItemsList)});
        
        saveReductionOrderItems.setCallback(this, function(response){                          
            var state = response.getState();
            var responseValue = response.getReturnValue();
            if (component.isValid() && state === "SUCCESS" && $A.util.isUndefinedOrNull(response.getReturnValue())) {
                var closeQuickAction = $A.get("e.force:closeQuickAction");
                if (closeQuickAction) {
                    closeQuickAction.fire();
                }
            } else if (state === "ERROR" || !$A.util.isUndefinedOrNull(responseValue)) {
                var errors = response.getError();
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "title": $A.get("$Label.c.OrderAddress_Error"),
                    "message": responseValue ? responseValue : "Unknown error",
                    "type": "error"
                });
                resultsToast.fire();
            }
        });
        $A.enqueueAction(saveReductionOrderItems);
    },
    validate: function(component) {
    	var validForm = true;
        var elems = component.find("validatedInput");
        var allValid = true;
        if (elems) {
            if ($A.util.isArray(elems)) {
                allValid = elems.reduce(function (validFields, inputCmp) {
                	inputCmp.showHelpMessageIfInvalid();
                	return validFields && inputCmp.get("v.validity").valid;
            	}, true);
            } else if ($A.util.isObject(elems)) {
                elems.showHelpMessageIfInvalid();
                allValid = elems.get("v.validity").valid
            }

        }
        return allValid; 
	}
})