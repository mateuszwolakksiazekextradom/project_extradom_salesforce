({
	doInit: function(component, event, helper) {
		helper.getReductionOrderItems(component);
	},
    close: function(component, event, helper) {
        var closeQuickAction = $A.get("e.force:closeQuickAction");
        if (closeQuickAction) {
            closeQuickAction.fire();
        }
    },
    save: function(component, event, helper) {
        if (helper.validate(component)) {
            helper.save(component);
            $A.get('e.force:refreshView').fire();
        }
    }
})