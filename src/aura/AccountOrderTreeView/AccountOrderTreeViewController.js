({
    doInit: function(component, event, helper) {
        component.set('v.gridColumns',
                      [
                          {label: $A.get("$Label.c.OrderTree_OrderNumber"), fieldName: 'ItemUrl', type: 'url',
                           typeAttributes: {
                               label: { fieldName: 'ItemName' }, target: '_self'
                           }
                          },
                          {label: $A.get("$Label.c.OrderTree_Type"), fieldName: 'IconDisplay', type: 'button',
                           typeAttributes: {
                               iconName: { fieldName: 'IconDisplay' }, disabled: 'true', variant: 'base', iconPosition: 'left',
                               title: { fieldName: 'IconTitle' }, label: { fieldName: 'IconLabel' }
                           }
                          },
                          {label: $A.get("$Label.c.OrderTree_Quantity"), fieldName: 'QuantityText', type: 'text'},
                          {label: $A.get("$Label.c.OrderTree_PODate"), fieldName: 'PoDate', type: 'date'},
                          {label: $A.get("$Label.c.OrderTree_Status"), fieldName: 'Status', type: 'text'}
                      ]
        	);
        helper.getOrders(component);
    },
    refresh: function(component, event, helper) {
        component.set('v.isLoading', "true");
        helper.getOrders(component);
    }
})