({
	getOrders: function(component) {
        var action = component.get("c.getOrders");
        action.setParams({accountId: component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var data = response.getReturnValue();
                for(var i=0; i<data.length;i++) {
                    data[i].ItemUrl = '/' + data[i].Id;
                    data[i].ItemName = data[i].OrderNumber;
                    if (data[i].IsReductionOrder) {
                         data[i].IconDisplay = 'utility:undo';
                         data[i].IconTitle = $A.get("$Label.c.OrderTree_Return");
                         data[i].IconLabel = '';
                    }
                    data[i]._children = data[i]['OrderItems'];
                    delete data[i].OrderItems; 
                    if (data[i]._children) {
                        for (var childIndex=0; childIndex < data[i]._children.length; childIndex++) {
                            data[i]._children[childIndex].ItemName = data[i]._children[childIndex].Product2.Name;
                            if (!$A.util.isUndefinedOrNull(data[i]._children[childIndex].Version__c)) {
                            	data[i]._children[childIndex].IconLabel = data[i]._children[childIndex].Version__c;
                            }
                            data[i]._children[childIndex].ItemUrl = '/' + data[i]._children[childIndex].Id;
    						data[i]._children[childIndex].QuantityText = '' + data[i]._children[childIndex].Quantity;
                        }
                    }
                }
                component.set('v.gridData', data);
                component.set('v.isLoading', false);
                var ordersTree = component.find('ordersTree');
        		ordersTree.expandAll();
            }
        });
        $A.enqueueAction(action);
	},
})