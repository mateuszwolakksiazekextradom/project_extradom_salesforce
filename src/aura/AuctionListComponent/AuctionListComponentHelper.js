/**
 * Created by grzegorz.dlugosz on 23.05.2019.
 */
({
    doInit: function(component, event, helper) {
        this.getBidListFromServ(component);

        window.setInterval(
            $A.getCallback(function() {
                if (component.isValid()) {
                    helper.getBidListFromServ(component);
                }

            }), 5000
        );
    },

    getBidListFromServ: function(component) {
        var action = component.get("c.getBidListSrv");
        action.setParams({ auctionId : component.get("v.recordId") });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var bids = response.getReturnValue();
                component.set("v.bidList", bids);
                var currentUserBids = [];

                for (var index in bids) {
                    if (bids[index].OwnerId == $A.get("$SObjectType.CurrentUser.Id")) {
                        currentUserBids.push(bids[index]);
                    }
                }

                currentUserBids.sort(function compare(a,b){
                    var dateA = new Date(a.CreatedDate);
                    var dateB = new Date(b.CreatedDate);
                    return dateA - dateB;
                });

                if (currentUserBids && currentUserBids.length > 0 ) {
                    if (currentUserBids[0]) {
                        component.set("v.currentUserBidId_1", currentUserBids[0].Id);
                    }
                    if (currentUserBids[1]) {
                        component.set("v.currentUserBidId_2", currentUserBids[1].Id);
                    }
                }
            }
            else if (state === "INCOMPLETE") {
            }
            else if (state === "ERROR") {
                // DEBUGGERS for errors
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        $A.enqueueAction(action);
    },
})