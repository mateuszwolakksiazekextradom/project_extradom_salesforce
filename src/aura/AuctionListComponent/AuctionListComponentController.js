/**
 * Created by grzegorz.dlugosz on 23.05.2019.
 */
({
    doInit: function(component, event, helper) {
        helper.doInit(component, event, helper);
    },

    handleListRefreshEvent: function(component, event, helper) {
         helper.getBidListFromServ(component);
    }
})