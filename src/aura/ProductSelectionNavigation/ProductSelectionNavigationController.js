({
	doInit: function(component, event, helper) {
		helper.getNavigationTabs(component, event);
	},
    onTabSelect: function(component, event, helper) {
        var selectedTab = event.currentTarget.dataset.index;
        component.set('v.selectedTab', parseInt(selectedTab)); 
    }
})