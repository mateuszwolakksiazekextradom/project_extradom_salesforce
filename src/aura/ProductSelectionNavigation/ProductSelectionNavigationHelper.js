({
	getNavigationTabs: function(component, event) {
        var getNavigationTabs = component.get("c.getNavigationTabs");
        var navigationTabs = [];
        
        getNavigationTabs.setCallback(this, function(response){                          
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                navigationTabs = response.getReturnValue();
                component.set('v.tabs', navigationTabs); 
            }
        });
        $A.enqueueAction(getNavigationTabs);
	}
})