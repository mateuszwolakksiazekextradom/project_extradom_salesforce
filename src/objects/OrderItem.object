<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>AddProduct</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>EditAllProduct</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>Order_Product_Compact_Layout</compactLayoutAssignment>
    <compactLayouts>
        <fullName>Order_Product_Compact_Layout</fullName>
        <fields>Product2Id</fields>
        <fields>UnitPrice</fields>
        <fields>Quantity</fields>
        <fields>TotalPrice</fields>
        <fields>Version__c</fields>
        <fields>ListPrice</fields>
        <fields>Discount__c</fields>
        <label>Order Product Compact Layout</label>
    </compactLayouts>
    <enableFeeds>false</enableFeeds>
    <fields>
        <fullName>Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Order Products</relationshipLabel>
        <relationshipName>Order_Products</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>AvailableQuantity</fullName>
    </fields>
    <fields>
        <fullName>COK_Bonus__c</fullName>
        <externalId>false</externalId>
        <label>COK Bonus</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Changes_Agreement__c</fullName>
        <externalId>false</externalId>
        <label>Changes Agreement</label>
        <length>5000</length>
        <trackHistory>true</trackHistory>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Confirmation_Price__c</fullName>
        <externalId>false</externalId>
        <label>Confirmation Price</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Description</fullName>
        <trackHistory>true</trackHistory>
    </fields>
    <fields>
        <fullName>Design_Type__c</fullName>
        <externalId>false</externalId>
        <label>Design Type</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Design__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Design</label>
        <referenceTo>Design__c</referenceTo>
        <relationshipLabel>Order Products</relationshipLabel>
        <relationshipName>Order_Products</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Discount_Internal__c</fullName>
        <defaultValue>0</defaultValue>
        <externalId>false</externalId>
        <label>Discount Internal</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>true</trackHistory>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Discount_Sales_Percent__c</fullName>
        <externalId>false</externalId>
        <formula>IF(Discount_Sales__c!=0,Discount_Sales__c/List_Price__c,0)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Discount Sales: Percent</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Discount_Sales__c</fullName>
        <defaultValue>0</defaultValue>
        <externalId>false</externalId>
        <label>Discount Sales</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>true</trackHistory>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Discount_Supplier__c</fullName>
        <defaultValue>0</defaultValue>
        <externalId>false</externalId>
        <label>Discount Supplier</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>true</trackHistory>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Discount_Type__c</fullName>
        <externalId>false</externalId>
        <label>Discount Type</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Gwarancja najniższej ceny</fullName>
                    <default>false</default>
                    <label>Gwarancja najniższej ceny</label>
                </value>
                <value>
                    <fullName>Rabat indywidualny</fullName>
                    <default>false</default>
                    <label>Rabat indywidualny</label>
                </value>
                <value>
                    <fullName>Karta Dużej Rodziny</fullName>
                    <default>false</default>
                    <label>Karta Dużej Rodziny</label>
                </value>
                <value>
                    <fullName>Kod promocyjny</fullName>
                    <default>false</default>
                    <label>Kod promocyjny</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Discount_Value__c</fullName>
        <externalId>false</externalId>
        <formula>IF(ListPrice=UnitPrice,null,ListPrice-UnitPrice)</formula>
        <label>Discount Value</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Discount__c</fullName>
        <externalId>false</externalId>
        <formula>Discount_Value__c / ListPrice</formula>
        <label>Discount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>EndDate</fullName>
        <trackHistory>true</trackHistory>
    </fields>
    <fields>
        <fullName>Hide_from_Emails__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Hide from Emails</label>
        <trackHistory>false</trackHistory>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Index__c</fullName>
        <externalId>false</externalId>
        <label>Index</label>
        <precision>5</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Influence_User_Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Influence User Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Order Products (Influence User Account)</relationshipLabel>
        <relationshipName>Order_Products_Influence_User_Account</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Influence_User_Source__c</fullName>
        <externalId>false</externalId>
        <label>Influence User Source</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Investor Order</fullName>
                    <default>false</default>
                    <label>Investor Order</label>
                </value>
                <value>
                    <fullName>Dealer Lead User</fullName>
                    <default>false</default>
                    <label>Dealer Lead User</label>
                </value>
                <value>
                    <fullName>COK Note</fullName>
                    <default>false</default>
                    <label>COK Note</label>
                </value>
                <value>
                    <fullName>COK Order</fullName>
                    <default>false</default>
                    <label>COK Order</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Influence_User__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Influence User</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Order_Products</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ListPrice</fullName>
    </fields>
    <fields>
        <fullName>List_Price__c</fullName>
        <externalId>false</externalId>
        <label>List Price</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>true</trackHistory>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>OrderId</fullName>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>OriginalOrderItemId</fullName>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Product2Id</fullName>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ProductCode</fullName>
    </fields>
    <fields>
        <fullName>Product_Code__c</fullName>
        <externalId>false</externalId>
        <label>Product Code</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Product_Family__c</fullName>
        <externalId>false</externalId>
        <label>Product Family</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Quantity</fullName>
        <trackHistory>true</trackHistory>
    </fields>
    <fields>
        <fullName>Quantity__c</fullName>
        <externalId>false</externalId>
        <formula>Quantity</formula>
        <label>Quantity</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Sales_Billing_Price__c</fullName>
        <externalId>false</externalId>
        <formula>IF(Order.IsReductionOrder,-(CASE(PricebookEntry.Product2.Family,&apos;Misc&apos;,0.00,OriginalOrderItem.List_Price__c)-OriginalOrderItem.Discount_Sales__c),CASE(PricebookEntry.Product2.Family,&apos;Misc&apos;,0.00,List_Price__c)-Discount_Sales__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Sales Billing Price</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>ServiceDate</fullName>
        <trackHistory>true</trackHistory>
    </fields>
    <fields>
        <fullName>StatusCode__c</fullName>
        <externalId>false</externalId>
        <formula>TEXT(Order.StatusCode)</formula>
        <label>Status Code</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>TotalPrice</fullName>
    </fields>
    <fields>
        <fullName>Total_COK_Bonus__c</fullName>
        <externalId>false</externalId>
        <formula>COK_Bonus__c * Quantity</formula>
        <label>Total COK Bonus</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Total_Confirmation_Price__c</fullName>
        <externalId>false</externalId>
        <formula>BLANKVALUE(Confirmation_Price__c,UnitPrice) * Quantity</formula>
        <label>Total Confirmation Price</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Total_Price_Available_Quantity__c</fullName>
        <externalId>false</externalId>
        <formula>AvailableQuantity*UnitPrice</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Total Price (Available Quantity)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Total_Price_Net_est__c</fullName>
        <externalId>false</externalId>
        <formula>0.01</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Total_Price_Net_est</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Total_Sales_Billing_Price__c</fullName>
        <externalId>false</externalId>
        <formula>ABS(Quantity)*Sales_Billing_Price__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Total Sales Billing Price</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Trade_Price__c</fullName>
        <externalId>false</externalId>
        <label>Trade Price</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>true</trackHistory>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>UnitPrice</fullName>
        <trackHistory>true</trackHistory>
    </fields>
    <fields>
        <fullName>Unit_Price_Verified__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Unit Price Verified</label>
        <trackHistory>true</trackHistory>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Version__c</fullName>
        <externalId>false</externalId>
        <label>Version</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Basic</fullName>
                    <default>false</default>
                    <label>Basic</label>
                </value>
                <value>
                    <fullName>Mirrored</fullName>
                    <default>false</default>
                    <label>Mirrored</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <webLinks>
        <fullName>Add_Products</fullName>
        <availability>online</availability>
        <displayType>massActionButton</displayType>
        <encodingKey>UTF-8</encodingKey>
        <height>600</height>
        <linkType>url</linkType>
        <masterLabel>Dodaj produkty</masterLabel>
        <openType>sidebar</openType>
        <protected>false</protected>
        <requireRowSelection>false</requireRowSelection>
        <url>/apex/EditOrderProducts?id={!Order.Id}</url>
    </webLinks>
</CustomObject>
