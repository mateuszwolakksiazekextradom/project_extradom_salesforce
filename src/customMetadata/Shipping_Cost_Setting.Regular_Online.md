<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Regular/Online</label>
    <protected>false</protected>
    <values>
        <field>Payment_Method__c</field>
        <value xsi:type="xsd:string">Online</value>
    </values>
    <values>
        <field>Shipping_Cost_External__c</field>
        <value xsi:type="xsd:double">18.0</value>
    </values>
    <values>
        <field>Shipping_Cost__c</field>
        <value xsi:type="xsd:double">19.0</value>
    </values>
    <values>
        <field>Shipping_Method__c</field>
        <value xsi:type="xsd:string">Regular</value>
    </values>
</CustomMetadata>
