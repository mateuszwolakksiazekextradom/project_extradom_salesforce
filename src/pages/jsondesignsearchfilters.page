<apex:page cache="true" expires="21600" contentType="application/x-JavaScript; charset=utf-8" applyBodyTag="false" applyHtmlTag="false" readOnly="true" standardController="Page__c" extensions="DesignSearchExtension" showHeader="false" standardStylesheets="false" sidebar="false">{
    "filters": [
        {
            "type": "range",
            "unit": "m²",
            "min": {
                "name": "minarea",
                <apex:outputText rendered="{!minarea!=null}">
                "value": {!minarea},
                </apex:outputText>
                "minValue": 1,
                "required": false,
                "label": "od"
            },
            "max": {
                "name": "maxarea",
                <apex:outputText rendered="{!maxarea!=null}">
                "value": {!maxarea},
                </apex:outputText>
                "minValue": 10,
                "required": false,
                "label": "do"
            },
            "samples": [
                { "label": "projekty domów do 100 m²", "max": 100 },
                { "label": "projekty domów 100-150 m²", "min": 100, "max": 150 },
                { "label": "projekty domów 150-200 m²", "min": 150, "max": 200 },
                { "label": "projekty domów od 200 m²", "min": 200 }
            ],
            "default": {
                "label": "dowolna powierzchnia"
            }
        },
        {
            "type": "select",
            "name": "level",
            <apex:outputText rendered="{!level!=null}">
            "value": {!level},
            </apex:outputText>
            "options": [
                { "label": "parter", "value": 47 },
                { "label": "parter + poddasze", "value": 48 },
                { "label": "parter + pełne piętro", "value": 63 }
            ],
            "default": {
                "label": "dowolne kondygnacje"
            }
        },
        {
            "type": "select",
            "name": "garage",
            <apex:outputText rendered="{!garage!=null}">
            "value": {!garage},
            </apex:outputText>
            "options": [
                { "label": "bez garażu", "value": 65 },
                { "label": "z garażem", "value": 79 },
                { "label": "z garażem 1 st.", "value": 50 },
                { "label": "z garażem 2 st.", "value": 52 },
                { "label": "z garażem 3+ st.", "value": 111 }
            ],
            "default": {
                "label": "garaż dowolny"
            }
        },
        {
            "type": "boolean",
            "name": "carport",
            "selected": {!carport==80},
            "label": "z wiatą garażową",
            "value": 80
        },
        {
            "type": "select",
            "name": "basement",
            <apex:outputText rendered="{!basement!=null}">
            "value": {!basement},
            </apex:outputText>
            "options": [
                { "label": "bez podpiwniczenia", "value": 72 },
                { "label": "z podpiwniczeniem", "value": 46 }
            ],
            "default": {
                "label": "domy z i bez podpiwniczenia"
            }
        },
        {
            "type": "range",
            "unit": "m",
            "max": {
                "name": "maxplotsize",
                <apex:outputText rendered="{!maxplotsize!=null}">
                "value": {!maxplotsize},
                </apex:outputText>
                "minValue": 1,
                "required": false,
                "label": "do"
            },
            "samples": [
                { "label": "na wąską działkę", "max": 16 }
            ],
            "default": {
                "label": "dowolna szerokość działki"
            }
        },
        {
            "type": "range",
            "unit": "m",
            "max": {
                "name": "maxheight",
                <apex:outputText rendered="{!maxheight!=null}">
                "value": {!maxheight},
                </apex:outputText>
                "minValue": 1,
                "required": false,
                "label": "do"
            },
            "samples": [
                { "label": "wys. budynku do 9 m", "max": 9 }
            ],
            "default": {
                "label": "dowolna wysokość budynku"
            }
        },
        {
            "type": "range",
            "unit": "m²",
            "max": {
                "name": "maxfootprint",
                <apex:outputText rendered="{!maxfootprint!=null}">
                "value": {!maxfootprint},
                </apex:outputText>
                "minValue": 1,
                "required": false,
                "label": "do"
            },
            "samples": [
                { "label": "zabudowa do 100 m²", "max": 100 }
            ],
            "default": {
                "label": "dowolna powierzchnia zabudowy"
            }
        },
        {
            "type": "select",
            "name": "roof",
            <apex:outputText rendered="{!roof!=null}">
            "value": {!roof},
            </apex:outputText>
            "options": [
                { "label": "dach dwuspadowy", "value": 4 },
                { "label": "dach czterospadowy", "value": 5 },
                { "label": "dach płaski", "value": 12 },
                { "label": "dach jednospadowy", "value": 13 },
                { "label": "dach kopertowy", "value": 7 },
                { "label": "dach naczółkowy", "value": 9 },
                { "label": "dach mansardowy", "value": 3 },
                { "label": "dach wielospadowy", "value": 6 },
                { "label": "dach namiotowy", "value": 8 }
            ],
            "default": {
                "label": "dowolny dach"
            }
        },
        {
            "type": "range",
            "unit": "°",
            "min": {
                "name": "minroofslope",
                <apex:outputText rendered="{!minroofslope!=null}">
                "value": {!minroofslope},
                </apex:outputText>
                "minValue": 0,
                "maxValue": 60,
                "required": false,
                "label": "od"
            },
            "max": {
                "name": "maxroofslope",
                <apex:outputText rendered="{!maxroofslope!=null}">
                "value": {!maxroofslope},
                </apex:outputText>
                "minValue": 0,
                "maxValue": 60,
                "required": false,
                "label": "do"
            },
            "samples": [
                { "label": "kąt dachu 30-40°", "min": 30, "max": 40 }
            ],
            "default": {
                "label": "dowolny kąt nachylenia dachu"
            }
        },
        {
            "type": "select",
            "name": "ridge",
            <apex:outputText rendered="{!ridge!=null}">
            "value": {!ridge},
            </apex:outputText>
            "options": [
                { "label": "kalenica równoległa do drogi", "value": 105 },
                { "label": "kalenica prostopadła do drogi", "value": 106 }
            ],
            "default": {
                "label": "dowolna kalenica"
            }
        },
        {
            "type": "boolean",
            "name": "entrance",
            "selected": {!entrance==45},
            "label": "z wejściem od południa",
            "value": 45
        },
        {
            "type": "select",
            "name": "garagelocation",
            <apex:outputText rendered="{!garagelocation!=null}">
            "value": {!garagelocation},
            </apex:outputText>
            "options": [
                { "label": "garaż w bryle budynku", "value": 66 },
                { "label": "garaż wysunięty", "value": 67 },
                { "label": "garaż cofnięty", "value": 73 },
                { "label": "garaż doklejony", "value": 70 },
                { "label": "garaż wolnostojący", "value": 78 }
            ],
            "default": {
                "label": "dowolne usytuowanie garażu"
            }
        }
    ]
}
</apex:page>