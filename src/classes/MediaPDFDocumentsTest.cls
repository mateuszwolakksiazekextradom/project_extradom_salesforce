@isTest
private class MediaPDFDocumentsTest {
    
    static testMethod void myTest() {
        
        Supplier__c s = new Supplier__c(Code__c='TST',Name='TST',Full_Name__c='TST');
        insert s;
        Design__c d = new Design__c(Supplier__c=s.Id,Code__c='TST1234',status__c='Active',Full_Name__c='TST',Name='TST');
        insert d;
        Media__c m1 = new Media__c(Design__c=d.Id,Category__c='Document PDF',Label__c='Rzut Parteru',Media_Name_Id__c='TST1',Subcategory__c='Plan');
        Media__c m2 = new Media__c(Design__c=d.Id,Category__c='Document PDF',Label__c='Kosztorys',Media_Name_Id__c='TST2',Subcategory__c='Estimates');
        Media__c m3 = new Media__c(Design__c=d.Id,Category__c='Document PDF',Label__c='Materiały',Media_Name_Id__c='TST3',Subcategory__c='Materials');
        Media__c m4 = new Media__c(Design__c=d.Id,Category__c='Document PDF',Label__c='Obrys',Media_Name_Id__c='TST4',Subcategory__c='Outline');
        insert new List<Media__c> { m1, m2, m3, m4 };
        
    }
    
}