public with sharing class TaskWithContactControllerExtension {
    
    private final Task t;
    public Contact c{get;set;}
    
    public TaskWithContactControllerExtension(ApexPages.StandardController stdController) {
        stdController.addFields(new List<String> {'WhoId', 'WhatId'});
        this.t = (Task)stdController.getRecord();
        if (String.isNotBlank(this.t.WhoId)) {
            c = [SELECT Account.Investment_Location__r.NotusID__c, Account.Investment_Location__r.Notus_ID__c FROM Contact WHERE Id = :this.t.WhoId];
        }
    }
    
}