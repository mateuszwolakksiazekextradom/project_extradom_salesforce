public with sharing class DesignFeaturedBatch implements Database.Batchable<sObject> {
    public Database.querylocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
                SELECT Id, Featured__c, Pending_Featured__c
                FROM Design__c
                WHERE Featured__c != null OR Pending_Featured__c != null
        ]);
    }

    public void execute(Database.BatchableContext BC, List<sObject> scope) {
        List<Design__c> designs = (List<Design__c> ) scope;
        for (Design__c design : designs) {
            if (design.Pending_Featured__c != null) {
                design.Featured__c = design.Pending_Featured__c;
            } else {
                design.Featured__c = null;
            }
        }
        DesignTriggerHandler.BYPASS_INVALIDATE_DESIGNS = true;
        update designs;
        DesignTriggerHandler.BYPASS_INVALIDATE_DESIGNS = false;
    }

    public void finish(Database.BatchableContext BC) {
        if (!Test.isRunningTest()) {
            Database.executeBatch(new DesignFeaturedRecentlyAddedBatch());
        }
    }
}