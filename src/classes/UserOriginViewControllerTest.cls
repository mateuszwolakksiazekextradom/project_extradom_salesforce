@IsTest(SeeAllData=true)
private class UserOriginViewControllerTest {

    static testMethod void myUnitTest() {
        
        PageReference pageRef = new PageReference('http://extradom.pl/jsonprofileview');
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', UserInfo.getUserId());
        ApexPages.currentPage().getHeaders().put('Origin', 'http://test.extradom.pl');
        
        UserOriginViewController uovc = New UserOriginViewController();
        uovc.getUser();
        uovc.getSFUser();
        uovc.getSFUserNews();
        uovc.getSFUserAbout();
        uovc.getSFUserOffer();
        uovc.getTopContentPosts();
        uovc.getCountContentPosts();
        uovc.getContentPosts();
        uovc.getDesignResults();
        uovc.getCountDesignResults();
        
    }
}