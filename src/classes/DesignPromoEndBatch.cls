public with sharing class DesignPromoEndBatch implements Database.Batchable<sObject> {
    public Database.querylocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
                SELECT Id, Promo_Name__c, Promo_Price__c, Promo_Price_End_Date__c, Promo_Price_inc__c
                FROM Design__c
                WHERE Promo_Price_End_Date__c <= :system.now() AND Promo_Price_End_Date__c != null
        ]);
    }

    public void execute(Database.BatchableContext BC, List<sObject> scope) {
        List<Design__c> designs = (List<Design__c> ) scope;
        for (Design__c design : designs) {
            design.Promo_Price__c = null;
            design.Promo_Price_inc__c = null;
            design.Promo_Name__c = null;
            design.Promo_Price_End_Date__c = null;
        }
        DesignTriggerHandler.BYPASS_INVALIDATE_DESIGNS = true;
        update designs;
        DesignTriggerHandler.BYPASS_INVALIDATE_DESIGNS = false;
    }

    public void finish(Database.BatchableContext BC) {
        if (!Test.isRunningTest()) {
            Database.executeBatch(new DesignSetGrossPriceBatch());
        }
    }
}