/**
 * Created by grzegorz.dlugosz on 28.05.2019.
 */

@isTest
private class AuctionListController_Test {

    @isTest
    public static void getOfferBidsWhenInProgress() {
        // create offer / aukcja
        Offer__c offer = TestUtils.newOffer(new Offer__c(End_Date__c = Datetime.now().addDays(1)), true);
        // put 1 bid of current user under this offer
        Bid__c bid1 = TestUtils.newBid(new Bid__c(Offer__c = offer.Id), true);

        User usr = TestUtils.prepareTestUser(false, ConstUtils.USER_ADMIN_PROFILE, ConstUtils.USER_COK_DEPARTMENT);
        usr.CompanyName = 'TestCompanyName';

        System.runAs(usr) {
            Bid__c bid2 = TestUtils.newBid(new Bid__c(Offer__c = offer.Id), true);
        }

        Test.startTest();
        List<Bid__c> resultBidList = AuctionListController.getBidListSrv(offer.Id);
        Test.stopTest();

        System.assertEquals(2, resultBidList.size());
        for (Bid__c bid : resultBidList) {
            if (bid.OwnerId == UserInfo.getUserId()) {
                System.assertNotEquals('Inna pracownia', bid.NazwaPracowni__c);
            } else {
                System.assertEquals('Inna pracownia', bid.NazwaPracowni__c);
            }
        }
    }

}