public with sharing class RecordTypeManager {
    private static Map<String, String> mRecordTypes;
    private static Map<String, String> mRecordTypeIds;

    static {
        String key;
        String value;

        if (mRecordTypes == null) {
            mRecordTypes = new Map<String, String>();
            mRecordTypeIds = new Map<String, String>();
            List<RecordType> rtlist = [Select Id, Name, DeveloperName, SObjectType From RecordType Limit 2000];

            for (RecordType rt : rtlist) {
                key = rt.SObjectType + '.' + rt.DeveloperName;
                value = rt.Id;

                mRecordTypes.put(key, value);
                mRecordTypeIds.put(value, rt.DeveloperName);
            }
        }
    }

    public static String RecordTypeId(String recordTypeName) {
        String result = '';
        if (mRecordTypes.containsKey(recordTypeName)) {
            result = mRecordTypes.get(recordTypeName);
        }
        return result;
    }

    public static String RecordTypeDeveloperName(String recordTypeId) {
        if (String.isNotEmpty(recordTypeId)) {
            recordTypeId = Id.valueOf(recordTypeId);
        }

        String result = '';
        if (mRecordTypeIds.containsKey(recordTypeId)) {
            result = mRecordTypeIds.get(recordTypeId);
        }
        return result;
    }
}