global class GoogleAnalyticsTools {
    @future(callout=true)
    public static void collectOrder(Id orderId) {
      Order o = [SELECT Id, OriginalOrderId, AccountId, Account.gaClientId__c, TotalAmount, Shipping_Cost__c, OrderSource__c, (SELECT Id, PricebookEntry.Product2.Name, UnitPrice, Quantity, PricebookEntry.Product2.ProductCode, PricebookEntry.Product2.Family FROM OrderItems) FROM Order WHERE Id = :orderId];
      Http h = new Http();
      HttpRequest req = new HttpRequest();
      req.setEndpoint('callout:GATracker/batch');
      req.setMethod('POST');
      List<String> params = new List<String> { 'v=1', 't=transaction', 'cu=PLN', 'ds=crm' };
      params.add('ti=T'+(String.isNotBlank(o.OriginalOrderId)?o.OriginalOrderId:o.Id));
      params.add('tid=UA-122850493-1'); // do poprawy jako zmienna
      params.add('uid='+o.AccountId);
      params.add('cid='+(String.isNotBlank(o.Account.gaClientId__c)?o.Account.gaClientId__c:o.AccountId));
      if (String.isBlank(o.Account.gaClientId__c)) { params.add('cs=SF'); params.add('cm=API'); }
      params.add('tr='+String.valueOf(o.TotalAmount));
      if (o.Shipping_Cost__c!=null) params.add('ts='+String.valueOf(o.Shipping_Cost__c));
      if (String.isNotBlank(o.OrderSource__c)) params.add('ta='+EncodingUtil.urlEncode(o.OrderSource__c, 'UTF-8'));
      String body = String.join(params, '&');
      for (OrderItem oi : o.OrderItems) {
          params = new List<String> { 'v=1', 't=item', 'cu=PLN', 'ds=crm' };
          params.add('ti=T'+(String.isNotBlank(o.OriginalOrderId)?o.OriginalOrderId:o.Id));
          params.add('tid=UA-122850493-1'); // do poprawy jako zmienna
          params.add('uid='+o.AccountId);
          params.add('cid='+(String.isNotBlank(o.Account.gaClientId__c)?o.Account.gaClientId__c:o.AccountId));
          if (String.isBlank(o.Account.gaClientId__c)) { params.add('cs=SF'); params.add('cm=API'); }
          params.add('in='+EncodingUtil.urlEncode(oi.PricebookEntry.Product2.Name, 'UTF-8'));
          params.add('ip='+String.valueOf(oi.UnitPrice));
          params.add('iq='+String.valueOf(Integer.valueOf(oi.Quantity)));
          if (String.isNotBlank(oi.PricebookEntry.Product2.ProductCode)) params.add('ic='+EncodingUtil.urlEncode(oi.PricebookEntry.Product2.ProductCode, 'UTF-8'));
          if (String.isNotBlank(oi.PricebookEntry.Product2.Family)) params.add('iv='+EncodingUtil.urlEncode(oi.PricebookEntry.Product2.Family, 'UTF-8'));
          body += '\n'+ String.join(params, '&');
      }
      req.setBody(body);
      System.debug(req);
      System.debug(req.getBody());
      HttpResponse res = h.send(req);
      System.debug(res);
    }
    @future(callout=true)
    public static void collectCreatedLeads(List<Id> accountIds) {
        List<Map<String,String>> events = new List<Map<String,String>>{};
        for (Account a : [SELECT Id, gaClientId__c FROM Account WHERE Id IN :accountIds]) {
            Map<String,String> event = new Map<String,String>{ 'ec' => 'SF', 'ea' => 'Lead Created', 'uid' => a.Id, 'cid' => (String.isNotBlank(a.gaClientId__c)?a.gaClientId__c:a.Id) };
            if (String.isBlank(a.gaClientId__c)) { event.put('cs', 'SF'); event.put('cm', 'API'); }
            events.add(event);
        }
        collectEvents(events);
    }
    @future(callout=true)
    public static void collectConvertedLeads(List<Id> accountIds) {
        List<Map<String,String>> events = new List<Map<String,String>>{};
        for (Account a : [SELECT Id, gaClientId__c FROM Account WHERE Id IN :accountIds]) {
            Map<String,String> event = new Map<String,String>{ 'ec' => 'SF', 'ea' => 'Lead Converted', 'uid' => a.Id, 'cid' => (String.isNotBlank(a.gaClientId__c)?a.gaClientId__c:a.Id) };
            if (String.isBlank(a.gaClientId__c)) { event.put('cs', 'SF'); event.put('cm', 'API'); }
            events.add(event);
        }
        collectEvents(events);
    }
    public static void collectEvents(List<Map<String,String>> events) {
        List<String> requestsBatch = new List<String>{};
        for (Map<String,String> event : events) {
            if (requestsBatch.size()==20) {
                collectBatch(requestsBatch);
                requestsBatch.clear();
            }
            List<String> params = new List<String> { 'v=1', 't=event', 'tid=UA-122850493-1', 'ds=crm' };
            for (String key : event.keySet()) {
                params.add(key+'='+EncodingUtil.urlEncode(event.get(key), 'UTF-8'));
            }
            requestsBatch.add(String.join(params, '&'));
        }
        collectBatch(requestsBatch);
    }
    public static void collectBatch(List<String> requests) {
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint('callout:GATracker/batch');
        req.setMethod('POST');
        String body = String.join(requests, '\n');
        req.setBody(body);
        System.debug(req);
        System.debug(req.getBody());
        HttpResponse res;
        if (!Test.isRunningTest()) {
            //res = h.send(req);
        }
        System.debug(res);
    }
}