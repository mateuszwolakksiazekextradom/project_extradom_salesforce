/**
 * Created by grzegorz.dlugosz on 16.05.2019.
 */
/* Tests for this batch are in DesignViewsPerMonthQueueable_Test */
global class DesignViewsPerMonthBatch implements Database.Batchable<Design__c> {

    private List<Design__c> designList;

    global DesignViewsPerMonthBatch(List<Design__c> designList) {
        this.designList = designList;
    }

    global Iterable<Design__c> start(Database.BatchableContext bc) {
        return designList;
    }

    global void execute(Database.BatchableContext param1, List<Design__c> designListToUpdate) {
        DesignTriggerHandler.BYPASS_INVALIDATE_DESIGNS = true;
        update designListToUpdate;
        DesignTriggerHandler.BYPASS_INVALIDATE_DESIGNS = false;
    }

    global void finish(Database.BatchableContext param1) {
    }
}