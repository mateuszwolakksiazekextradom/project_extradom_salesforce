global class Babylon {
    public List<Material> materials;
    global class Material {
        public String name;
        public String id;
        public String tags;
        public Boolean disableDepthWrite;
        public List<Double> ambient;
        public List<Double> diffuse;
        public List<Double> specular;
        public Double specularPower;
        public List<Double> emissive;
        public Double alpha;
        public Boolean backFaceCulling;
        public Boolean wireframe;
        public Texture diffuseTexture;
        public Texture ambientTexture;
        public Texture opacityTexture;
        public Texture reflectionTexture;
        public Texture refractionTexture;
        public Double indexOfRefraction;
        public Texture emissiveTexture;
        public Texture specularTexture;
        public Texture bumpTexture;
        public Texture lightmapTexture;
        public Boolean useLightmapAsShadowmap;
        public Boolean checkReadyOnlyOnce;
        public Boolean useReflectionFresnelFromSpecular;
        public Boolean useEmissiveAsIllumination;
        public Fresnel diffuseFresnelParameters;
        public Fresnel opacityFresnelParameters;
        public Fresnel reflectionFresnelParameters;
        public Fresnel refractionFresnelParameters;
        public Fresnel emissiveFresnelParameters;
    }
    global class Fresnel {
        public Boolean isEnabled;
        public List<Double> leftColor;
        public List<Double> rightColor;
        public Double bias;
        public Double power;
    }
    global class Texture {
        public String name {
            get {
                return name==null?null:(name.startsWith('https://')?name:'https://static3d.extradom.pl/'+UserInfo.getOrganizationId()+'/'+name);
            }
            set;
        }
        public Double level;
        public Boolean hasAlpha;
        public Boolean getAlphaFromRGB;
        public Integer coordinatesMode;
        public Double uOffset;
        public Double vOffset;
        public Double uScale;
        public Double vScale;
        public Double uAng;
        public Double vAng;
        public Double wAng;
        public Integer wrapU;
        public Integer wrapV;
        public Integer coordinatesIndex;
        public List<Animation> animations;
        public String base64String;
    }
    global class Animation {
        public Integer dataType;
        public Integer framePerSecond;
        public Integer loopBehavior;
        public String name;
        public String property;
        public List<AnimationKey> keys;
        public Boolean autoAnimate;
        public Integer autoAnimateFrom;
        public Integer autoAnimateTo;
        public Boolean autoAnimateLoop;
    }
    global class AnimationKey {
        public Integer frame;
        public List<Double> values;
    }
}