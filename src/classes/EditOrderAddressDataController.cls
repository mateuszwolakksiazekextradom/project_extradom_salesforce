public with sharing class EditOrderAddressDataController {
	@AuraEnabled
    public static List<Contact> getContacts(String accountId) {
        List<Contact> contacts = new List<Contact> ();
        if (String.isNotBlank(accountId)) {
           	contacts = [SELECT Email, Phone FROM Contact WHERE AccountId = :accountId];
        }
        return contacts;
    }
}