@IsTest
private class OfferViewExtension_Test {
    
    static testMethod void vf_tests() {
        
        test.startTest();
        
        Offer__c o = new Offer__c(Name='Test Offer', Minimum_Value__c=100);
        insert o;
        
        Bid__c b = new Bid__c(Value__c=200);
        insert b;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(o);
        OfferViewExtension ext = new OfferViewExtension(sc);
        ext.getTopBids();
        ext.getTop12Bids();
        
        test.stopTest();
        
    }
}