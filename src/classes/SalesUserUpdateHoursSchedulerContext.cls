global class SalesUserUpdateHoursSchedulerContext implements Schedulable {
    global SalesUserUpdateHoursSchedulerContext() {}
    global void execute(SchedulableContext ctx) {
        List<User> users = [Select Id, Name, Absence_Hours__c From User WHERE IsActive = true AND Department='COK'];

        List<AggregateResult> activeUsers = [
            Select userId
            FROM LoginHistory
            WHERE LoginTime = THIS_MONTH AND LoginTime <> TODAY AND SourceIp IN ('195.94.192.98', '80.55.66.17')
            AND UserId IN (Select Id From User WHERE IsActive = true AND Department='COK')
            GROUP BY UserId, day_in_month(loginTime)];
        
        List<AggregateResult> todayUsers = [
            SELECT userId, min(logintime) firstLogin
            FROM LoginHistory
            WHERE loginTime = TODAY AND SourceIp IN ('195.94.192.98', '80.55.66.17')
            AND UserId IN (Select Id From User WHERE IsActive = true AND Department='COK')
            GROUP BY userId];
        
        Double qTotalHours = 0.0;
        Double qHours = 0.0;
        Boolean isLive = false;
        
        List<User> usersToUpdate = new List<User>();
        for (User u : users) {
            
            qTotalHours = 0;
            isLive = false;
            
            for (AggregateResult au : activeUsers) {
                if(au.get('userId')==u.Id) {
                    qTotalHours += 8.0;
                }
            }
            
            for (AggregateResult tu : todayUsers) {
                if(tu.get('userId')==u.Id) {
                    qHours = (System.now().getTime()-((DateTime)tu.get('firstLogin')).getTime())/1000.0/60.0/60.0;
                    if (qHours > 0.0 && qHours < 8.0) {
                        isLive = true;
                    }
                    qTotalHours += Math.min(8.0,qHours);
                }
            }
            
            u.This_Month_Hours__c = u.Absence_Hours__c == null || qTotalHours == 0 ? qTotalHours : qTotalHours - u.Absence_Hours__c <= 1 ? 1 : qTotalHours - u.Absence_Hours__c;
            u.Is_Live__c = isLive;
            usersToUpdate.add(u);
         }
         update usersToUpdate;
    }
}