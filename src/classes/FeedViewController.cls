public with sharing class FeedViewController {

    private final String type;
    private final String order;
    private final String subjectId;
    private final String pageParam;
    
    private final ConnectApi.FeedType feedType;
    private final ConnectApi.FeedSortOrder feedSortOrder;
    
    public FeedViewController() {
    
        Map<String,ConnectApi.FeedType> types = new Map<String,ConnectApi.FeedType>{'news' => ConnectApi.FeedType.News, 'files' => ConnectApi.FeedType.Files, 'userprofile' => ConnectApi.FeedType.UserProfile, 'topics' => ConnectApi.FeedType.Topics, 'home' => ConnectApi.FeedType.Home, 'record' => ConnectApi.FeedType.Record, 'group' => ConnectApi.FeedType.Record, 'qa-all' => ConnectApi.FeedType.Home, 'qa-solved' => ConnectApi.FeedType.Home, 'qa-unsolved' => ConnectApi.FeedType.Home, 'qa-unanswered' => ConnectApi.FeedType.Home, 'bookmarks' => ConnectApi.FeedType.Bookmarks};
    
        this.type = ApexPages.currentPage().getParameters().get('type');
        this.feedType = types.get(type);
        
        try {
            this.pageParam = ApexPages.currentPage().getParameters().get('pageParam');
        } catch (Exception e) {
        }
        try {
            this.order = ApexPages.currentPage().getParameters().get('order');
            if (this.order=='lastmodifieddatedesc') {
                this.feedSortOrder = ConnectApi.FeedSortOrder.LastModifiedDateDesc;
            }
        } catch (Exception e) {
        }
        try {
            this.subjectId = ApexPages.currentPage().getParameters().get('subjectId');
        } catch (Exception e) {
        }
        
    }
    
    public ConnectApi.FeedElementPage getFeedElementsFromFeed() {
        if (this.type == 'home') {
            return ConnectApi.ChatterFeeds.getFeedElementsFromFeed(Network.getNetworkId(), this.feedType, this.pageParam, null, this.feedSortOrder);
        } else if (this.type == 'group') {
            return ConnectApi.ChatterFeeds.getFeedElementsFromFeed(Network.getNetworkId(), this.feedType, this.subjectId, 0, ConnectApi.FeedDensity.FewerUpdates, this.pageParam, null, this.feedSortOrder);
        } else if (this.type == 'qa-all') {
            return ConnectApi.ChatterFeeds.getFeedElementsFromFeed(Network.getNetworkId(), this.feedType, null, null, this.pageParam, null, this.feedSortOrder, ConnectApi.FeedFilter.AllQuestions);
        } else if (this.type == 'qa-solved') {
            return ConnectApi.ChatterFeeds.getFeedElementsFromFeed(Network.getNetworkId(), this.feedType, null, null, this.pageParam, null, this.feedSortOrder, ConnectApi.FeedFilter.SolvedQuestions);
        } else if (this.type == 'qa-unsolved') {
            return ConnectApi.ChatterFeeds.getFeedElementsFromFeed(Network.getNetworkId(), this.feedType, null, null, this.pageParam, null, this.feedSortOrder, ConnectApi.FeedFilter.UnsolvedQuestions);
        } else if (this.type == 'qa-unanswered') {
            return ConnectApi.ChatterFeeds.getFeedElementsFromFeed(Network.getNetworkId(), this.feedType, null, null, this.pageParam, null, this.feedSortOrder, ConnectApi.FeedFilter.UnansweredQuestions);
        }
        return ConnectApi.ChatterFeeds.getFeedElementsFromFeed(Network.getNetworkId(), this.feedType, this.subjectId, this.pageParam, null, this.feedSortOrder);
    }

}