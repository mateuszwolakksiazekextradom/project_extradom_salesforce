/**
* @author: Mateusz Wolak-Ksiazek
* class to store custom string methods
**/
global class StringUtilities {

    global enum Type {X_NUMERIC, X_INTEGER, X_BOOLEAN, X_DATETIME, X_DATE, X_STRING, X_NULL}

    /**
    * @author: Mateusz Wolak-Ksiazek
    * check if string is numeric / decimal
    *
    * @param 1: passed string, it can be either number (int, double, dec) or normal string
    *
    * @return if string is value of decimal
    **/
    global static Boolean isNumeric(String s){
        Boolean ReturnValue;
        try{
            Decimal.valueOf(s);
            ReturnValue = TRUE;
        } catch (Exception e) {
            ReturnValue = FALSE;
        }
        return ReturnValue;
    }

    /**
	* @author: Mateusz Wolak-Ksiazek
	* check if string is value of Integer
    *
	* @param 1: passed string, it can be either number (int, double, dec) or normal string
    *
    * @return if string is value of Integer
	**/
    global static Boolean isInteger(String s){
        Boolean ReturnValue;
        try{
            Integer.valueOf(s);
            ReturnValue = TRUE;
        } catch (Exception e) {
            ReturnValue = FALSE;
        }
        return ReturnValue;
    }

    /**
    * @author: Mateusz Wolak-Ksiazek
    * check if string is value of Integer
    *
    * @param 1: passed string, it can be either number (int, double, dec) or normal string
    *
    * @return if string is value of boolean
    **/
    global static Boolean isBoolean(String s){
        Boolean ReturnValue;
        try{
            Boolean.valueOf(s);
            ReturnValue = TRUE;
        } catch (Exception e) {
            ReturnValue = FALSE;
        }
        return ReturnValue;
    }

    /**
    * @author: Mateusz Wolak-Ksiazek
    * check if string is value of Integer
    *
    * @param 1: passed string, it can be either number (int, double, dec) or normal string
    *
    * @return if string is value of datetime
    **/
    global static Boolean isDatetime(String s){
        Boolean ReturnValue;
        try{
            Datetime.valueOf(s);
            ReturnValue = TRUE;
        } catch (Exception e) {
            ReturnValue = FALSE;
        }
        return ReturnValue;
    }

    /**
    * @author: Mateusz Wolak-Ksiazek
    * check if string is value of Date
    *
    * @param 1: passed string, it can be either number (int, double, dec) or normal string
    *
    * @return if stirng is value of date
    **/
    global static Boolean isDate(String s){
        Boolean ReturnValue;
        try{
            Date.valueOf(s);
            ReturnValue = TRUE;
        } catch (Exception e) {
            ReturnValue = FALSE;
        }
        return ReturnValue;
    }

    /**
    * @author: Mateusz Wolak-Ksiazek
    * check if string is null
    *
    * @param 1: passed string, it can be either number (int, double, dec) or normal string
    *
    * @return if string is value of null
    **/
    global static Boolean isNull(String s){
        Boolean ReturnValue;
        if( s == null) {
            ReturnValue = TRUE;
        } else {
            ReturnValue = FALSE;
        }
        return ReturnValue;
    }

    /**
    * @author: Mateusz Wolak-Ksiazek
    * check if string is value of Integer
    *
    * @param 1: passed string, it can be either number (int, double, dec) or normal string
    *
    * @return display type, enum value
    **/
    global static Schema.DisplayType whichType(String s){
        if(isNumeric(s)) {
            return Schema.DisplayType.DOUBLE;
        } else if(isInteger(s)) {
            return Schema.DisplayType.INTEGER;
        } else if(isDatetime(s)) {
            return Schema.DisplayType.DATETIME;
        } else if(isDate(s)) {
            return Schema.DisplayType.DATE;
        } else if(isBoolean(s)) {
            return Schema.DisplayType.BOOLEAN;
        } else if(isNull(s)) {
            return null;
        }
        return Schema.DisplayType.STRING;
    }


    /**
    * @author: Mateusz Wolak-Ksiazek
    * check if string is value of Integer
    *
    * @param 1: list of sobject (for example list of sales processes, resources etc.)
    * @param 2: field name from which will be gather values (most often ids, for example Proposal_from_Customer_Group__c)
    *
    * @return list of values gathered from specified field
    **/
    global static List<String> getListOfIdsFromSpecifiedField(List<sObject> listOfSobjects, String fieldName) {
        List<String> values = new List<String>();
        for(sObject record : listOfSobjects) {
            try {
                values.add(
                        String.valueOf(
                                record.get(fieldName)
                        )
                );
            } catch(Exception e) { continue; }
        }
        return values;
    }




    /**
    * @author: Mateusz Wolak-Ksiazek
    * put in specified field passed value
    *
    * @param 1: list of sobjects (for example list of sales processes, resources etc.)
    * @param 2: field name which will be filled with value provided in fieldValue (3rd parameter)
    * @param 3: field value which will be filled in the field name provided in second parameter
    *
    **/
    global static void putTransferredValueIntoProvidedField(List<sObject> sObjectList, String fieldName, String fieldValue) {

        switch on whichType(fieldValue) {
            when DOUBLE  {
                for(sObject s_ObjectRec : sObjectList) {
                    s_ObjectRec.put(fieldName, Decimal.valueOf(fieldValue));
                }
            }
            when INTEGER  {
                for(sObject s_ObjectRec : sObjectList) {
                    s_ObjectRec.put(fieldName, Integer.valueOf(fieldValue));
                }
            }
            when BOOLEAN  {
                for(sObject s_ObjectRec : sObjectList) {
                    s_ObjectRec.put(fieldName, Boolean.valueOf(fieldValue));
                }
            }
            when DATETIME  {
                for(sObject s_ObjectRec : sObjectList) {
                    s_ObjectRec.put(fieldName, Datetime.valueOf(fieldValue));
                }
            }
            when DATE  {
                for(sObject s_ObjectRec : sObjectList) {
                    s_ObjectRec.put(fieldName, Date.valueOf(fieldValue));
                }
            }
            when else { //null or string leave as it is
                for(sObject s_ObjectRec : sObjectList) {
                    s_ObjectRec.put(fieldName, fieldValue);
                }
            }
        }

    }


    /**
    * @author: Mateusz Wolak-Ksiazek
    * put in specified field passed value
    *
    * @param 1: list of wrappers containg sobject record with field name and field value
    *
    **/
    global static void putTransferredValuesIntoProvidedFields(List<PutValueWrapper> putValueWrappers) {

        for(PutValueWrapper putValueWrapper : putValueWrappers) {

            switch on whichType(putValueWrapper.fieldValue) {
                when DOUBLE  {
                    putValueWrapper.s_Object.put(putValueWrapper.fieldName, Decimal.valueOf(putValueWrapper.fieldValue));
                }
                when INTEGER  {
                    putValueWrapper.s_Object.put(putValueWrapper.fieldName, Integer.valueOf(putValueWrapper.fieldValue));
                }
                when BOOLEAN  {
                    putValueWrapper.s_Object.put(putValueWrapper.fieldName, Boolean.valueOf(putValueWrapper.fieldValue));
                }
                when DATETIME  {
                    putValueWrapper.s_Object.put(putValueWrapper.fieldName, Datetime.valueOf(putValueWrapper.fieldValue));
                }
                when DATE  {
                    putValueWrapper.s_Object.put(putValueWrapper.fieldName, Date.valueOf(putValueWrapper.fieldValue));
                }
                when else { //null or string leave as it is
                    putValueWrapper.s_Object.put(putValueWrapper.fieldName, putValueWrapper.fieldValue);
                }
            }

        }

    }

    /**
    * @author: Mateusz Wolak-Ksiazek
    * wrapper used for putTransferredValuesIntoProvidedFields  method
    **/
    global class PutValueWrapper {
        sObject s_Object;
        String fieldName;
        String fieldValue;

        global PutValueWrapper(sObject s_Object, String fieldName, String fieldValue) {
            this.s_Object = s_Object;
            this.fieldName = fieldName;
            this.fieldValue = fieldValue;
        }

    }


}