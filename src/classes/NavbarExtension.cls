public with sharing class NavbarExtension {
    
    public NavbarExtension() {
    }
    
    public NavbarExtension(ApexPages.StandardController stdController) {
    }
    
    public List<List<SObject>> getSearchResults() {
        String searchString = Apexpages.currentPage().getParameters().get('q')+'*';
        List<List<SObject>> searchResults = [FIND :searchString IN NAME FIELDS RETURNING Design__c(Id, Full_Name__c, Code__c, Thumbnail_Base_URL__c, Usable_Area__c WHERE Status__c = 'Active' LIMIT 10), Keyword__c(Page__c, Page__r.Name, Page__r.Type__c, Page__r.Design__r.Id, Page__r.Design__r.Full_Name__c, Page__r.Design__r.Code__c, Page__r.Design__r.Thumbnail_Base_URL__c, Page__r.Design__r.Usable_Area__c WHERE Page__r.Type__c = 'Collection' OR Page__r.Design__r.Status__c = 'Active' LIMIT 25)];
        
        Set<Id> pages = new Set<Id>{};
        List<Keyword__c> uniquePageKeywords = new List<Keyword__c>{};
        for (Keyword__c k : (List<Keyword__c>)searchResults[1]) {
            if (!pages.contains(k.Page__c)) {
                pages.add(k.Page__c);
                uniquePageKeywords.add(k);
            }
        }
        
        List<Keyword__c> uniqueCollectionKeywords = new List<Keyword__c>{};
        
        Set<Id> designs = new Set<Id>{};
        for (Design__c d : (List<Design__c>)searchResults[0]) {
            designs.add(d.Id);
        }
        
        for (Keyword__c k : uniquePageKeywords) {
            if (k.Page__r.Type__c == 'Design') {
                Design__c d = k.Page__r.Design__r;
                if (!designs.contains(d.Id)) {
                    designs.add(d.Id);
                    if (designs.size()<10) {
                        searchResults[0].add(d);
                    }
                }
            } else {
                if (uniqueCollectionKeywords.size()<5) {
                    uniqueCollectionKeywords.add(k);
                }
            }
        }
        
        searchResults[1] = uniqueCollectionKeywords;
        return searchResults;
    }
    
}