@IsTest(SeeAllData=true)
private class PlotViewExtensionTest {

    static testMethod void myUnitTest() {
    
        Plot__c p = new Plot__c(Name='Example plot');
        insert p;
        
        PageReference pageRef = new PageReference('https://www.extradom.pl/dzialka-123');
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(p);
        PlotViewExtension ext = new PlotViewExtension(sc);
        ext.getMySubscriptionReference();
        
        FollowExtension.follow(p.Id);
        ext.getMySubscriptionReference();
        ext.getFollowerPage();
        
        FollowExtension.unfollowBySubjectId(p.Id);
        
    }
    
}