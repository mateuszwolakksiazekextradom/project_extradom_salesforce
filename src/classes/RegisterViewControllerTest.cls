@IsTest public with sharing class RegisterViewControllerTest {
    @IsTest(SeeAllData=true) 
    public static void testCommunitiesSelfRegController() {
        PageReference pageRef = new PageReference('http://extradom.pl/projekty-domow');
        pageRef.getHeaders().put('gaClientId', 'abc');
        Test.setCurrentPage(pageRef);
        RegisterViewController controller = new RegisterViewController();
        controller.email = 'test@force.com';
        controller.communityNickname = 'test_unittests';
        controller.password = 'aAbcd123456!';
        controller.optInMarketing = true;
        controller.optInMarketingExtended = true;
        controller.optInMarketingPartner = true;
        controller.optInRegulations = true;
        controller.optInProfiling = true;
        controller.optInPhone = true;
        controller.optInEmail = true;
        
        // registerUser will always return null when the page isn't accessed as a guest user
        System.assert(controller.registerUser() == null);    
        
        Account a = new Account(Name='test');
        insert a;
        controller.email = 'test2@force.com';
        Contact c = new Contact(LastName='test',Email=controller.email,AccountId=a.Id);
        insert c;
        controller.password = 'aaaaaaaaaaaaa';
        try {
            controller.registerUser();
        } catch(Exception e) {
        }
        controller.password = 'aaaaaaa';
        try {
            controller.registerUser();
        } catch(Exception e) {
        }
        
        controller.email = UserInfo.getUserName();
        controller.getActiveStartUrl();
        controller.getActiveSource();
        controller.registerUser();
        
        controller.password = 'abcd1234';
        controller.confirmPassword = 'abcd123';
        System.assert(controller.registerUser() == null);  
        
        controller.email = 'test@force.com';
        controller.communityNickname = 'test_unittests';
        
        // registerUser will always return null when the page isn't accessed as a guest user
        System.assert(controller.registerUser() == null);    
        
        controller.email = 'test2@force.com';
        controller.communityNickname = 'test_unittests2';
        
        // registerUser will always return null when the page isn't accessed as a guest user
        System.assert(controller.registerUser() == null);   
        
        controller.password = 'abcd123456';
        controller.confirmPassword = 'abcd123456';       
        
        System.assert(controller.registerUser() == null);   
        
    }    
}