public with sharing class ContactExtension {
	
	private final Contact c;
    public ContactExtension(ApexPages.StandardController stdController) {
        this.c = (Contact)stdController.getRecord();
    }
    
    private ApexPages.StandardSetController stdSetController;
    public ContactExtension(ApexPages.StandardSetController stdSetController)
    {
        this.stdSetController = stdSetController;
    }
    
    public PageReference processFeaturedDesigns() {
    	ContactProfiler.processContact(c.Id);
        PageReference pageRef = new ApexPages.StandardController(c).cancel();
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    public PageReference massProcessFeaturedDesigns()
    {
        List<Contact> selectedContacts = (List<Contact>) stdSetController.getSelected();
        for(Contact c: selectedContacts) {
        	ContactProfiler.processContact(c.Id);
        }
        return stdSetController.cancel();
    }

}