@isTest
private class ConnectApiToolsTest {

    static testMethod void myUnitTest() {

        System.assertEquals(ConnectApiTools.LinkSegmentType,ConnectApi.MessageSegmentType.Link);
        System.assertEquals(ConnectApiTools.MentionSegmentType,ConnectApi.MessageSegmentType.Mention);
        System.assertEquals(ConnectApiTools.HashtagSegmentType,ConnectApi.MessageSegmentType.Hashtag);
        System.assertEquals(ConnectApiTools.TextSegmentType,ConnectApi.MessageSegmentType.Text);
        
        ConnectApiTools tools = new ConnectApiTools();
        tools.getTimeZoneValue();
        tools.getIntegerLocaleValue();
        tools.getDecimalLocaleValue();
        tools.dateTimeValue = System.now();
        tools.integerValue = 1;
        tools.decimalValue = 1.01;
        tools.getTimeZoneValue();
        tools.getIntegerLocaleValue();
        tools.getDecimalLocaleValue();
        
    }
    
}