@RestResource(urlMapping='/newsletterrequest')
global class NewsletterRequestRest {
    @HttpPost
    global static Map<String,String> doPost(String email, String phone, Boolean optInRegulations, Boolean optInPrivacyPolicy, Boolean optInMarketing, Boolean optInMarketingExtended, Boolean optInMarketingPartner, Boolean optInLeadForward, Boolean optInEmail, Boolean optInPhone, Boolean optInProfiling, Boolean optInPKO) {
        Restcontext.response.headers.put('Access-Control-Allow-Origin', '*');
        try {
            Map<String,Id> data = OrderMethods.getWebLeadContactDetails('Newsletter', '', email, email, phone, 'Internal', false, false, 'No');
            RestRequest req = RestContext.request;
            if (req.headers.containsKey('gaClientId')) {
                OrderMethods.updateGaClientId(data.get('accountId'), req.headers.get('gaClientId'));
            }
            OrderMethods.updateOptIns(email, phone, optInRegulations, optInPrivacyPolicy, optInMarketing, optInMarketingExtended, optInMarketingPartner, optInLeadForward, optInEmail, optInPhone, optInProfiling, optInPKO);
        } catch (Exception e) {
            RestResponse res = RestContext.response;
            res.statusCode = 400;
            Map<String,String> result = new Map<String,String>{'result' => 'Nieprawidłowe zapytanie'};
            return result;
        }
        Map<String,String> result = new Map<String,String>{'result' => 'Dziękujemy za zainteresowanie. Będziemy informować Cię o aktualnych promocjach i rabatach.'};
        return result;
    }
}