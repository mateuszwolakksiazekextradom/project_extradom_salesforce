@isTest
private class UserEntitySubscriptionExtensionTest {

    static testMethod void SubscriptionTest() {
    
        test.startTest();
        
        User u = [SELECT id FROM User WHERE id = :UserInfo.getUserId()];
        Topic t = new Topic (Name = 'test');
        insert t;
        EntitySubscription es = new EntitySubscription(ParentId=t.Id,SubscriberId=u.Id);
        insert es;
                
        ApexPages.StandardController sc = new ApexPages.standardController(u);        
        UserEntitySubscriptionExtension uese = new UserEntitySubscriptionExtension(sc);
             
        uese.getDesigns();
        uese.getTopics();
        uese.getPlots();
        uese.getUsers();
        
        test.stopTest();
    }
}