@RestResource(urlMapping='/app/orders/design')
global class AppOrdersDesign {
    @HttpPost
    global static ExtradomApi.DesignOrderResult doPost() {
        RestRequest req = RestContext.request;
        ExtradomApi.DesignOrderInput input = (ExtradomApi.DesignOrderInput)JSON.deserialize(req.requestBody.toString(), ExtradomApi.DesignOrderInput.class);
        
        RestResponse res = RestContext.response;
        
        RestContext.response.addHeader('Cache-Control', 'no-cache, must-revalidate, max-age=0, no-store, private, s-maxage=0');
        
        Design__c d = [SELECT Id, Full_Name__c, Code__c, Mirror__c, Gross_Price__c, Current_Price__c, Thumbnail_Base_URL__c, Type__c, Return_Conditions__c, Swap_Conditions__c FROM Design__c WHERE Code__c != '' AND Code__c = :input.code];
        Order__c o = new Order__c();
        o.Order_Source__c = input.source;
        o.Design__c = d.id;
        o.Quantity__c = 1;
        o.Design_Version__c = input.version;
        o.Billing_First_Name__c = input.billingAddress.firstName;
        o.Billing_Last_Name__c = input.billingAddress.lastName;
        o.Billing_Address_Street_Name__c = input.billingAddress.streetName;
        o.Billing_Address_Street_No__c = input.billingAddress.streetNumber;
        o.Billing_Address_Street_Flat__c = input.billingAddress.streetFlatNumber;
        o.Billing_Postal_Code__c = input.billingAddress.zip;
        o.Billing_City__c = input.billingAddress.city;
        o.Billing_Email__c = input.billingAddress.email;
        o.Billing_Phone__c = input.billingAddress.phone;
        o.Billing_Company__c = input.billingAddress.company;
        o.Billing_Tax_Number__c = input.billingAddress.vatNumber;
        if (input.shippingAddress != null) {
            o.Shipping_First_Name__c = input.shippingAddress.firstName;
            o.Shipping_Last_Name__c = input.shippingAddress.lastName;
            o.Shipping_Address_Street_Name__c = input.shippingAddress.streetName;
            o.Shipping_Address_Street_No__c = input.shippingAddress.streetNumber;
            o.Shipping_Address_Street_Flat__c = input.shippingAddress.streetFlatNumber;
            o.Shipping_Postal_Code__c = input.shippingAddress.zip;
            o.Shipping_City__c = input.shippingAddress.city;
            o.Shipping_Phone__c = input.shippingAddress.phone;
        }
        o.Shipping_Method__c = input.shippingMethod + ' (' + input.paymentMethod + ')';
        o.Add_Ons_Details__c = input.comments;
        List<String> addOns = new List<String>();
        for (ExtradomApi.AddOnInput addOnInput : input.addOns) {
            addOns.add(addOnInput.name);
        }
        o.Add_Ons__c = String.join(addOns,';');
        Boolean optIn = false;
        Boolean optInOrder = false;
        Boolean optInRegulations = false;
        for (ExtradomApi.OptInInput optInInput : input.optIns) {
            if (optInInput.name == 'optIn') {
                optIn = optInInput.value;
            }
            if (optInInput.name == 'optInOrder') {
                optInOrder = optInInput.value;
            }
            if (optInInput.name == 'optInRegulations') {
                optInRegulations = optInInput.value;
            }
        }
        o.Opt_In__c = optIn;
        o.Opt_In_Order__c = optInOrder;
        o.Opt_In_Regulations__c = optInRegulations;
        o.Account__c = OrderMethods.getAccountId(input.billingAddress.firstName, input.billingAddress.lastName, input.billingAddress.email, input.billingAddress.phone, input.source, 'Design', optIn, optIn, 'No');
        insert o;
        
        Order__c order = [SELECT Id, Shipping_Method__c, Pay_URL__c, Design__r.Id, Design__r.Code__c, Design__r.Full_Name__c, Design__r.Type__c, Design__r.Usable_Area__c, Design__r.Thumbnail_Base_URL__c, Design__r.Sketchfab_ID__c, Design__r.KW_Model__c, Design__r.Gross_Price__c, Design__r.Current_Price__c FROM Order__c WHERE Id = :o.Id];
        ExtradomApi.DesignOrderResult orderres = new ExtradomApi.DesignOrderResult();
        orderres.id = order.Id;
        orderres.design = new ExtradomApi.DesignResult(order.Design__r, null);
        orderres.version = input.version;
        if (order.Shipping_Method__c.contains('(Online)')) {
            orderres.payUrl = order.Pay_URL__c;
        }
        orderres.message = 'Dziękujemy za złożenie zamówienia. Potwierdzenie złożenia zamówienia zostało przesłane na podany adres poczty e-mail. Wkrótce skontaktujemy się z Państwem w celu ustalenia dogodnego terminu dostarczenia projektu.';
        orderres.comments = 'Dostarczenie projektu następuje przeważnie w terminie 3-5 dni roboczych od potwierdzenia zamówienia przez konsultanta Serwisu Extradom.pl. Maksymalny czas oczekiwania to 14 dni. Stworzenie Projektu domu na podstawie jego Koncepcji może wydłużyć czas realizacji, o czym zostaną Państwo poinformowani telefonicznie. Konsultant Serwisu Extradom.pl uprzedzi o terminie dostarczenia projektu z jednodniowym wyprzedzeniem.';
        return orderres;
        
    }
}