public with sharing class SettingsViewController {
    
    public Boolean receiveAllFeedsEmail { get; set; }
    public Boolean receiveFollowersEmail { get; set; }
    public Boolean receiveProfilePostEmail { get; set; }
    public Boolean receiveLikeEmail { get; set; }
    public Boolean receiveMessageEmail { get; set; }
    public Boolean receiveChangeCommentEmail { get; set; }
    public Boolean receiveProfPostCommentEmail { get; set; }
    public Boolean receiveLaterCommentEmail { get; set; }
    public Boolean receiveLaterBookmarkEmail { get; set; }
    public Boolean receiveCommentAfterLikeEmail { get; set; }
    private NetworkMember member;
    
    public SettingsViewController() {
        member = [SELECT PreferencesDisableAllFeedsEmail, PreferencesDisableFollowersEmail, PreferencesDisableProfilePostEmail, PreferencesDisableLikeEmail, PreferencesDisableMessageEmail, PreferencesDisableChangeCommentEmail, PreferencesDisProfPostCommentEmail, PreferencesDisableLaterCommentEmail, PreferencesDisableBookmarkEmail, PreferencesDisCommentAfterLikeEmail FROM NetworkMember WHERE MemberId = :UserInfo.getUserId() LIMIT 1];
        this.receiveAllFeedsEmail = !member.PreferencesDisableAllFeedsEmail;
        this.receiveFollowersEmail = !member.PreferencesDisableFollowersEmail;
        this.receiveProfilePostEmail = !member.PreferencesDisableProfilePostEmail;
        this.receiveLikeEmail = !member.PreferencesDisableLikeEmail;
        this.receiveMessageEmail = !member.PreferencesDisableMessageEmail;
        this.receiveChangeCommentEmail = !member.PreferencesDisableChangeCommentEmail;
        this.receiveProfPostCommentEmail = !member.PreferencesDisProfPostCommentEmail;
        this.receiveLaterCommentEmail = !member.PreferencesDisableLaterCommentEmail;
        this.receiveLaterBookmarkEmail = !member.PreferencesDisableBookmarkEmail;
        this.receiveCommentAfterLikeEmail = !member.PreferencesDisCommentAfterLikeEmail;
    }
    
    public PageReference quicksave() {
        member.PreferencesDisableAllFeedsEmail = !this.receiveAllFeedsEmail;
        member.PreferencesDisableFollowersEmail = !this.receiveFollowersEmail;
        member.PreferencesDisableProfilePostEmail = !this.receiveProfilePostEmail;
        member.PreferencesDisableLikeEmail = !this.receiveLikeEmail;
        member.PreferencesDisableMessageEmail = !this.receiveMessageEmail;
        member.PreferencesDisableChangeCommentEmail = !this.receiveChangeCommentEmail;
        member.PreferencesDisProfPostCommentEmail = !this.receiveProfPostCommentEmail;
        member.PreferencesDisableLaterCommentEmail = !this.receiveLaterCommentEmail;
        member.PreferencesDisableBookmarkEmail = !this.receiveLaterBookmarkEmail;
        member.PreferencesDisCommentAfterLikeEmail = !this.receiveCommentAfterLikeEmail;
        update member;
        return null;
    }
    
}