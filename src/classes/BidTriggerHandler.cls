/**
 * Created by grzegorz.dlugosz on 27.05.2019.
 */
public without sharing class BidTriggerHandler extends TriggerHandler {

    public override void beforeUpdate() {
        Map<Id, Bid__c> oldBidMap = (Map<Id, Bid__c>) Trigger.oldMap;
        Map<Id, Bid__c> newBidMap = (Map<Id, Bid__c>) Trigger.newMap;

        // updates NazwaPracowni whenever owner is changed.
        updateWorkShopName(oldBidMap, newBidMap.values());
    }

    public override void beforeInsert() {
        List<Bid__c> newBidList = (List<Bid__c>) Trigger.new;

        // updates NazwaPracowni whenever owner is changed.
        updateWorkShopName(null, newBidList);
    }

    private void updateWorkShopName(Map<Id, Bid__c> oldMap, List<Bid__c> newBidList) {
        Set<Id> ownersToQUery = new Set<Id>();

        // before update
        if (oldMap != null) {
            for (Bid__c newBid : newBidList) {
                Bid__c oldBid = oldMap.get(newBid.Id);

                if (newBid.OwnerId != oldBid.OwnerId) {
                    if (String.isBlank(newBid.OwnerId)) {
                        newBid.NazwaPracowni__c = null;
                    } else {
                        ownersToQUery.add(newBid.OwnerId);
                    }
                }
            }
        // before insert
        } else {
            for (Bid__c bid : newBidList) {
                if (String.isNotBlank(bid.OwnerId)) {
                    ownersToQUery.add(bid.OwnerId);
                }
            }
        }

        if (!ownersToQUery.isEmpty()) {
            Map<Id, User> ownerMap = new Map<Id, User>([
                SELECT CompanyName
                FROM User
                WHERE Id IN :ownersToQUery
            ]);

            for (Bid__c newBid : newBidList) {
                if (String.isNotBlank(newBid.OwnerId) && ownerMap.keySet().contains(newBid.OwnerId)) {
                    newBid.NazwaPracowni__c = ownerMap.get(newBid.OwnerId).CompanyName;
                }
            }
        }
    }
}