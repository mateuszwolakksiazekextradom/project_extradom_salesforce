public with sharing class ProductSelectionNavController {

    @AuraEnabled
    public static List<Product_Selection_Navigation__mdt> getNavigationTabs() {
        List<Product_Selection_Navigation__mdt> navTabs = [
            SELECT MasterLabel, Product_Family__c, Order__c FROM Product_Selection_Navigation__mdt
            ORDER BY Order__c ASC
        ];
        return navTabs;
    }
}