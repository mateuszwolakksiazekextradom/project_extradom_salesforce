@isTest
private class WebToCaseTest {

    static testMethod void DesignOrder() {
        
        Account a = new Account();
        a.Name = 'Domex';
        a.Type = 'Partner';
        a.Partner_ID__c = 'domex';
        insert a;
        
        Supplier__c s = new Supplier__c();
        s.Name = 'Projekty ABC';
        s.Full_Name__c = 'Projekty';
        s.Code__c = 'ABC';
        insert s;
        
        Design__c d = new Design__c();
        d.Name = 'Projekt ABC1234';
        d.Full_Name__c = 'Projekt';
        d.Code__c = 'ABC1234';
        d.Supplier__c = s.Id;
        insert d;
        
        Case cs = new Case();
        cs.Type = 'Design Order';
        cs.Order_Code__c = 'ABC1234';
        cs.Subject = 'Zamówienie projektu Projekt ABC1234';
        cs.Order_Code_Version__c = 'Mirrored';
        cs.Source_URL__c = 'http://extradom.pl/zamowienie';
        cs.Source_Partner_ID__c = 'domex';
        cs.SuppliedName = 'Adam';
        cs.Supplied_Last_Name__c = 'Nowak';
        cs.SuppliedCompany = 'Nowak SA';
        cs.Supplied_VAT_No__c = '895-123-12-12';
        cs.Supplied_Street__c = 'Krakowska';
        cs.Supplied_Street_No__c = '2';
        cs.Supplied_Street_Flat__c = '10';
        cs.Supplied_Postal_Code__c = '00-001';
        cs.Supplied_City__c = 'Warszawa';
        cs.SuppliedPhone = '601-000-000';
        cs.SuppliedEmail = 'sf7@extradom.pl';
        cs.Shipping_Name__c = 'Anna Kowalska';
        cs.Shipping_Phone__c = '601-000-002';
        cs.Shipping_Street__c = 'Opolska';
        cs.Shipping_Street_No__c = '12a';
        cs.Shipping_Street_Flat__c = '1';
        cs.Shipping_Postal_Code__c = '52-001';
        cs.Shipping_City__c = 'Wrocław';
        cs.Shipping_Method__c = 'DPD (Prepayment)';
        cs.Description = 'Zgoda na zmiany...';
        cs.Agreement_Website_Regulations__c = true;
        cs.Agreement_Commercial_Contact__c = true;
        cs.Agreement_Global_Transfer__c = true;
        insert cs;
        
    }
}