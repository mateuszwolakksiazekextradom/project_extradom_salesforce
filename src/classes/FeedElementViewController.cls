public with sharing class FeedElementViewController {

    private final String elementId;
    private final String pageParam;
    
    public FeedElementViewController() {
        this.elementId = ApexPages.currentPage().getParameters().get('id');
        try {
            this.pageParam = ApexPages.currentPage().getParameters().get('pageParam');
        } catch (Exception e) {
        }
    }
    
    public void doBookmark() {
        ConnectApi.ChatterFeeds.updateFeedElementBookmarks(Network.getNetworkId(), elementId, true);
    }
    
    public ConnectApi.FeedElement getFeedElement() {
        return ConnectApi.ChatterFeeds.getFeedElement(Network.getNetworkId(), this.elementId);
    }
    
    public ConnectApi.FeedElementPage getSimiliarFeedElements() {
        ConnectApi.FeedElement element = this.getFeedElement();
        List<String> topicNames = new List<String>{};
        if (element.capabilities.topics!=null) {
            ConnectApi.TopicsCapability topicsCapability = element.capabilities.topics;
            for (ConnectApi.Topic topic : topicsCapability.items) {
                topicNames.add(topic.name);
            }
        }
        String q = String.join(topicNames, ' OR ');
        if (q.length()>2) {
            return ConnectApi.ChatterFeeds.searchFeedElementsinFeed(Network.getNetworkId(), ConnectApi.FeedType.Record, element.parent.id, null, 3, ConnectApi.FeedSortOrder.LastModifiedDateDesc, q);
        }
        return ConnectApi.ChatterFeeds.searchFeedElementsinFeed(Network.getNetworkId(), ConnectApi.FeedType.Record, element.parent.id, null, 3, ConnectApi.FeedSortOrder.LastModifiedDateDesc, null);
    }
    
    public ConnectApi.CommentPage getCommentsForFeedElement() {
        return ConnectApi.ChatterFeeds.getCommentsForFeedElement(Network.getNetworkId(), this.elementId, this.pageParam, null);
    }

}