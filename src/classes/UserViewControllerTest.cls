@IsTest(SeeAllData=true)
private class UserViewControllerTest {

    static testMethod void myUnitTest() {
    
        ConnectApi.FeedElementPage testPage = new ConnectApi.FeedElementPage();
        List<ConnectApi.FeedItem> testItemList = new List<ConnectApi.FeedItem>();
        testItemList.add(new ConnectApi.FeedItem());
        testPage.elements = testItemList;
        
        ConnectApi.ChatterFeeds.setTestGetFeedElementsFromFeed(null, ConnectApi.FeedType.UserProfile, UserInfo.getUserId(), testPage);
        ConnectApi.ChatterFeeds.setTestGetFeedElementsFromFeed(null, ConnectApi.FeedType.Files, UserInfo.getUserId(), testPage);
        ConnectApi.ChatterFeeds.setTestGetFeedElementsFromFeed(null, ConnectApi.FeedType.Home, null, null, null, null, null, ConnectApi.FeedFilter.AllQuestions, testPage);
        
        PageReference pageRef = new PageReference('http://extradom.pl/user');
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', UserInfo.getUserId());
        
        UserViewController uvc = New UserViewController();
        uvc.getMe();
        uvc.updateMe();
        uvc.getFeedElementsFromFeed();
        uvc.getFeedElementsFromFeedFiles();
        uvc.getFollowers();
        uvc.getUser();
        uvc.getSFUser();
        uvc.getSFUserNews();
        uvc.getSFUserAbout();
        uvc.getSFUserOffer();
        uvc.getFollowings();
        uvc.getFollowingsUsers();
        uvc.getFollowingsTopics();
        uvc.getFollowingsFiles();
        uvc.getFollowingsDesigns();
        uvc.getTopContentPosts();
        uvc.getContentPosts();
        uvc.getCountContentPosts();
        uvc.getConstructions();
        
        ConnectApi.TopicPage testTopicPage = new ConnectApi.TopicPage();
        ConnectApi.Topics.setTestGetRecentlyTalkingAboutTopicsForUser(null, UserInfo.getUserId(), testTopicPage);
        
        QAViewController qvc = new QAViewController();
        qvc.getFeedElementsFromFeedAllQuestions();
        
        ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), 'me', ConnectApi.FeedElementType.FeedItem, 'On vacation this week.');
        ChatterMethods.getFeedElementsFromStringList(feedElement.id);
        
        pageRef = new PageReference('http://extradom.pl/feedelement');
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', feedElement.id);
        FeedElementViewController fvc = new FeedElementViewController();
        fvc.getFeedElement();
        
    }
}