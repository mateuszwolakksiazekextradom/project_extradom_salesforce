@RestResource(urlMapping='/app/models')
global with sharing class AppModels {
    @HttpGet
    global static List<ExtradomApi.ModelResult> doGet() {
        RestRequest req = RestContext.request;
        List<ID> ids = new List<ID>();
        List<ExtradomApi.ModelResult> models = new List<ExtradomApi.ModelResult>();
        if (UserInfo.getUserType()=='Guest') {
            RestContext.response.statusCode = 401; // UNAUTHORIZED
            return null;
        }
        for (Model__c m : [SELECT Id, Name, UserRecordAccess.MaxAccessLevel, Mirrored__c, Design__r.Id, Design__r.Code__c, Design__r.Full_Name__c, Design__r.Type__c, Design__r.Usable_Area__c, Design__r.Thumbnail_Base_URL__c, Design__r.Sketchfab_ID__c, Design__r.KW_Model__c, Design__r.Gross_Price__c, Design__r.Current_Price__c, (SELECT Id, Name, ParentId, Description, CreatedDate, LastModifiedDate FROM Attachments ORDER BY CreatedDate DESC) FROM Model__c WHERE RecordType.Name = 'Interior' AND OwnerId = :UserInfo.getUserId()]) {
            // need to be corrected - user record access when fetched over the set of records returns wrong result, why? workaround: soql within loop
            UserRecordAccess ura = [SELECT RecordId, MaxAccessLevel from UserRecordAccess where RecordId = :m.Id and UserId =: UserInfo.getUserId()];
            ExtradomApi.ModelResult model = new ExtradomApi.ModelResult(m);
            model.maxAccessLevel = ura.MaxAccessLevel;
            models.add(model);
        }
        if (UserInfo.getUserType()=='Guest') {
            RestContext.response.addHeader('Cache-Control', 'must-revalidate,max-age=0,s-maxage=3600');
        } else {
            RestContext.response.addHeader('Cache-Control', 'no-cache,must-revalidate,max-age=0,no-store,private,s-maxage=0');
        }
        return models;
    }
    public class ApiException extends Exception {}
}