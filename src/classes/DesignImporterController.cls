public with sharing class DesignImporterController {
	
	private final Attachment a;	
	
    public DesignImporterController(ApexPages.StandardController stdController) {
        this.a = (Attachment)stdController.getRecord();
    }
    
    public PageReference checkImport() {
    	DesignImporter.importAsyncXMLData(a.Id, false);
    	return null;
    }
    
    public PageReference runImport() {
    	DesignImporter.importAsyncXMLData(a.Id, true);
    	return null;
    }
	
}