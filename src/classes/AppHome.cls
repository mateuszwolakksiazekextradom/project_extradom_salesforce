@RestResource(urlMapping='/app/home')
global class AppHome {
    @HttpGet
    global static ExtradomApi.Home doGet() {
        ExtradomApi.Home home = new ExtradomApi.Home();
        home.slider = new List<ExtradomApi.SliderItem>();
        ExtradomApi.SliderItem sliderItem = new ExtradomApi.SliderItem();
        sliderItem.url = '/app/pages/a0Gb000000499vZ';
        sliderItem.imageUrl = 'https://static.extradom.pl/app/home1.jpg';
        home.slider.add(sliderItem);
        sliderItem = new ExtradomApi.SliderItem();
        sliderItem.url = '/app/pages/a0Gb000000499vZ';
        sliderItem.imageUrl = 'https://static.extradom.pl/app/starter-1.jpg';
        home.slider.add(sliderItem);
        sliderItem = new ExtradomApi.SliderItem();
        sliderItem.url = '/app/pages/a0Gb000000499vZ';
        sliderItem.imageUrl = 'https://static.extradom.pl/app/starter-2.jpg';
        home.slider.add(sliderItem);
        sliderItem = new ExtradomApi.SliderItem();
        sliderItem.url = '/app/pages/a0Gb000000499vZ';
        sliderItem.imageUrl = 'https://static.extradom.pl/app/starter-3.jpg';
        home.slider.add(sliderItem);
        sliderItem = new ExtradomApi.SliderItem();
        sliderItem.url = '/app/pages/a0Gb000000499vZ';
        sliderItem.imageUrl = 'https://static.extradom.pl/app/starter-4.jpg';
        home.slider.add(sliderItem);
        
        if (UserInfo.getUserType()=='Guest') {
            RestContext.response.addHeader('Cache-Control', 'must-revalidate, max-age=0, s-maxage=300');
        } else {
            RestContext.response.addHeader('Cache-Control', 'no-cache, must-revalidate, max-age=0, no-store, private, s-maxage=0');
        }
        
        return home;
    }
}