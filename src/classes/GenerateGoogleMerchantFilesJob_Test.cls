@IsTest
private class GenerateGoogleMerchantFilesJob_Test {
	public static String CRON_EXP = '0 0 0 15 3 ? 2080';

    @IsTest
    private static void shouldExecuteJob() {
        //given
        Supplier__c s = new Supplier__c(Name='Test Supplier ABC', Full_Name__c='Test Supplier', Code__c='ABC');
        insert s;
        Design__c d = new Design__c(Name='D1', Full_Name__c='D1', Code__c='ABC1000', Supplier__c=s.Id, Status__c='Active', Sent_Month__c=0.5, Views_Month__c = 1000, Gross_Price__c = 2500);
        insert d;
        Folder f = [SELECT Id from Folder where type = 'Document' LImit 1];
        Document d1 = new Document(Name = 'google-merchant-supplemental-feed1.tsv', FolderId = f.Id);
        Document d2 = new Document(Name = 'google-merchant-supplemental-feed2.tsv', FolderId = f.Id);
        insert new List<Document> {d1, d2};

        //when
        Test.startTest();
        String jobId = System.schedule('GenerateGoogleMerchantFilesJob_Test', CRON_EXP, new GenerateGoogleMerchantFilesJob());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];

        //then
        system.assertEquals(CRON_EXP, ct.CronExpression);
        system.assertEquals(0, ct.TimesTriggered);
        Test.stopTest();
    }
}