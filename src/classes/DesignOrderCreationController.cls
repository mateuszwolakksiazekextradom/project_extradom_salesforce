public without sharing class DesignOrderCreationController {
    private static final String ORDER_EXTRADOM_SOURCE = 'Extradom.pl';
    private static final String MIRRORED_VERSION = 'mirrored';
    private static final String DEFAULT_SHIPPING_METHOD = 'Regular (Prepayment)';
    private String code;
    private String ver;
    private String partner {get;set;}
    public String designVersion {get;set;}
    public String billingFirstName {get;set;}
    public String billingLastName {get;set;}
    public String billingAddressStreetName {get;set;}
    public String billingAddressStreetNo {get;set;}
    public String billingAddressStreetFlat {get;set;}
    public String billingPostalCodePart1 {get;set;}
    public String billingPostalCodePart2 {get;set;}
    public String billingCity {get;set;}
    public String billingEmail {get;set;}
    public String billingPhone {get;set;}
    public String billingCompany {get;set;}
    public String billingTaxNumber {get;set;}
    public String shippingFirstName {get;set;}
    public String shippingLastName {get;set;}
    public String shippingAddressStreetName {get;set;}
    public String shippingAddressStreetNo {get;set;}
    public String shippingAddressStreetFlat {get;set;}
    public String shippingPostalCodePart1 {get;set;}
    public String shippingPostalCodePart2 {get;set;}
    public String shippingCity {get;set;}
    public String shippingPhone {get;set;}
    public String addOnsDetails {get;set;}
    public String addOns {get;set;}
    public String shippingMethod {get;set;}
    // START RODO
    public Boolean optInRegulations {get;set;}
    public Boolean optInPrivacyPolicy {get;set;}
    public Boolean optInMarketing {get;set;}
    public Boolean optInMarketingExtended {get;set;}
    public Boolean optInMarketingPartner {get;set;}
    public Boolean optInLeadForward {get;set;}
    // END RODO
    // START RODO v2
    public Boolean optInRegulationsAndPrivacyPolicy {get;set;}
    public Boolean optInEmail {get;set;}
    public Boolean optInPhone {get;set;}
    public Boolean optInProfiling {get;set;}
    // END RODO v2
    
    public DesignOrderCreationController() {
        this.code = ApexPages.currentPage().getParameters().get('code');
        this.ver = ApexPages.currentPage().getParameters().get('ver');
        this.partner = ApexPages.currentPage().getParameters().get('partner');
        if (String.isBlank(this.partner)) {
            this.partner = ORDER_EXTRADOM_SOURCE;
        }
        // START RODO
        this.optInRegulations = false;
        this.optInPrivacyPolicy = false;
        this.optInMarketing = false;
        this.optInMarketingExtended = false;
        this.optInMarketingPartner = false;
        this.optInLeadForward = false;
        // END RODO
        // START RODO v2
        this.optInRegulationsAndPrivacyPolicy = false;
        this.optInEmail = false;
        this.optInPhone = false;
        this.optInProfiling = false;
        // END RODO v2
        this.shippingMethod = DEFAULT_SHIPPING_METHOD;
        this.designVersion = ConstUtils.ORDER_BASIC_VERSION;
        if (this.ver == MIRRORED_VERSION) this.designVersion = ConstUtils.ORDER_MIRRORED_VERSION;
    }
    public Design__c getDesign() {
        Design__c design = null;
        List<Design__c> designs = [
            SELECT Id, Full_Name__c, Code__c, Mirror__c, Gross_Price__c, Current_Price__c, Thumbnail_Base_URL__c,
                Type__c, Return_Conditions__c, Swap_Conditions__c, DefaultAddOns__c, KW_Model__c, KW_Model_Preparation__c
            FROM Design__c WHERE Code__c != '' AND Code__c = :this.code
        ];
        if (designs.size() > 0) {
            design = designs.get(0);
        }

        return design;
    }
    public List<Product2> getAddOnProducts() {
        return [select id, name, productcode, description, (SELECT id, unitprice, regular_price__c FROM PricebookEntries where isactive = true) from product2 where design__r.code__c = :this.code and design__r.code__c != '' and family='Add-On' and isactive = true and productcode != ''];
    }
    
    public PageReference save() {
        PageReference returnPage = null;
        Savepoint sp = Database.setSavepoint();
        try {
            ExtradomApi.DesignOrderInput orderInput = convertToInput();
            String orderId = OrderUtils.saveOrder(orderInput);
            Order insertedOrder = null;
            List<Order> orders = [SELECT Id, AccountId, Pay_URL__c, Payment_Method__c FROM Order WHERE Id = :orderId];
            if (orders.size() > 0) {
                insertedOrder = orders.get(0);
                if (ApexPages.currentPage().getHeaders().containsKey('gaClientId')) {
                    OrderMethods.updateGaClientId(insertedOrder.accountId, ApexPages.currentPage().getHeaders().get('gaClientId'));
                }
                if (insertedOrder.Payment_Method__c == ConstUtils.ORDER_ONLINE_PAYMENT && String.isNotBlank(insertedOrder.Pay_URL__c)) {
                    returnPage = new PageReference(insertedOrder.Pay_URL__c);
                    returnPage.setRedirect(true);
                } else {
                    returnPage = Page.DesignOrderViewComplete;
                    returnPage.getParameters().put('code', this.code);
                    if (this.designVersion == ConstUtils.ORDER_MIRRORED_VERSION) returnPage.getParameters().put('ver',MIRRORED_VERSION);
                    if (this.partner != ORDER_EXTRADOM_SOURCE) returnPage.getParameters().put('partner',this.partner);
                    returnPage.setRedirect(true);
                }
            }
        } catch (Exception e) {
            Database.rollback(sp);
            throw new DesignOrderCreationControllerException(e.getMessage());
        }
        return returnPage;
    }
    
    private ExtradomApi.DesignOrderInput convertToInput() {
        ExtradomApi.DesignOrderInput orderInput = new ExtradomApi.DesignOrderInput();
        orderInput.billingAddress = new ExtradomApi.AddressInput();
        orderInput.shippingAddress = new ExtradomApi.AddressInput();
        orderInput.addOns = new List<ExtradomApi.AddOnInput>();
        orderInput.optIns = new List<ExtradomApi.OptInInput>();
        orderInput.code = this.code;
        orderInput.version = this.designVersion;
        orderInput.source = this.partner;
        orderInput.comments = this.addOnsDetails;
        orderInput.paymentMethod = getPaymentMethod();
        orderInput.shippingMethod = getShippingMethod();
        
        orderInput.billingAddress.company = this.billingCompany;
        orderInput.billingAddress.vatNumber = this.billingTaxNumber;
        orderInput.billingAddress.firstName = this.billingFirstName;
        orderInput.billingAddress.lastName = this.billingLastName;
        orderInput.billingAddress.streetName = this.billingAddressStreetName;
        orderInput.billingAddress.streetNumber = this.billingAddressStreetNo;
        orderInput.billingAddress.streetFlatNumber = this.billingAddressStreetFlat;
        orderInput.billingAddress.zip = this.billingPostalCodePart1 + '-' + this.billingPostalCodePart2;
        orderInput.billingAddress.city = this.billingCity;
        orderInput.billingAddress.phone = this.billingPhone;
        orderInput.billingAddress.email = this.billingEmail;

        Boolean useBillingAddress = false;
        if (String.isBlank(this.shippingFirstName) && String.isBlank(this.shippingLastName) && String.isBlank(this.shippingAddressStreetName)
                && String.isBlank(this.shippingAddressStreetNo) && String.isBlank(this.shippingAddressStreetFlat) && String.isBlank(this.shippingPostalCodePart1)
                && String.isBlank(this.shippingPostalCodePart2) && String.isBlank(this.shippingCity) && String.isBlank(this.shippingPhone)) {
            useBillingAddress = true;
        }
        orderInput.shippingAddress.firstName = useBillingAddress ? this.billingFirstName : this.shippingFirstName;
        orderInput.shippingAddress.lastName = useBillingAddress ? this.billingLastName : this.shippingLastName;
        orderInput.shippingAddress.streetName = useBillingAddress ? this.billingAddressStreetName : this.shippingAddressStreetName;
        orderInput.shippingAddress.streetNumber = useBillingAddress ? this.billingAddressStreetNo : this.shippingAddressStreetNo;
        orderInput.shippingAddress.streetFlatNumber = useBillingAddress ? this.billingAddressStreetFlat : this.shippingAddressStreetFlat;
        orderInput.shippingAddress.zip = useBillingAddress ? this.billingPostalCodePart1 + '-' + this.billingPostalCodePart2
                : this.shippingPostalCodePart1 + '-' + this.shippingPostalCodePart2;
        orderInput.shippingAddress.city = useBillingAddress ? this.billingCity : this.shippingCity;
        orderInput.shippingAddress.phone = useBillingAddress ? this.billingPhone : this.shippingPhone;
        
        // START RODO
        if (this.optInRegulations) {
            ExtradomApi.OptInInput optinInputRegulations = new ExtradomApi.OptInInput();
            optinInputRegulations.name = ConstUtils.ORDER_OPT_IN_REGULATIONS_PARAM;
            optinInputRegulations.value = true;
            orderInput.optIns.add(optinInputRegulations);
        }
        if (this.optInPrivacyPolicy) {
            ExtradomApi.OptInInput optinInputPrivacyPolicy = new ExtradomApi.OptInInput();
            optinInputPrivacyPolicy.name = ConstUtils.ORDER_OPT_IN_PRIVACY_POLICY_PARAM;
            optinInputPrivacyPolicy.value = true;
            orderInput.optIns.add(optinInputPrivacyPolicy);
        }
        if (this.optInMarketing) {
            ExtradomApi.OptInInput optinInputMarketing = new ExtradomApi.OptInInput();
            optinInputMarketing.name = ConstUtils.ORDER_OPT_IN_MARKETING_PARAM;
            optinInputMarketing.value = true;
            orderInput.optIns.add(optinInputMarketing);
        }
        if (this.optInMarketingExtended) {
            ExtradomApi.OptInInput optinInputMarketingExtended = new ExtradomApi.OptInInput();
            optinInputMarketingExtended.name = ConstUtils.ORDER_OPT_IN_MARKETING_EXTENDED_PARAM;
            optinInputMarketingExtended.value = true;
            orderInput.optIns.add(optinInputMarketingExtended);
        }
        if (this.optInMarketingPartner) {
            ExtradomApi.OptInInput optinInputMarketingPartner = new ExtradomApi.OptInInput();
            optinInputMarketingPartner.name = ConstUtils.ORDER_OPT_IN_MARKETING_PARTNER_PARAM;
            optinInputMarketingPartner.value = true;
            orderInput.optIns.add(optinInputMarketingPartner);
        }
        if (this.optInLeadForward) {
            ExtradomApi.OptInInput optinInputLeadForward = new ExtradomApi.OptInInput();
            optinInputLeadForward.name = ConstUtils.ORDER_OPT_IN_LEAD_FORWARD_PARAM;
            optinInputLeadForward.value = true;
            orderInput.optIns.add(optinInputLeadForward);
        }
        // END RODO
        // START RODO v2
        if (this.optInRegulationsAndPrivacyPolicy) {
            ExtradomApi.OptInInput optinInputRegulationsAndPrivacyPolicy = new ExtradomApi.OptInInput();
            optinInputRegulationsAndPrivacyPolicy.name = ConstUtils.ORDER_OPT_IN_REGULATIONS_AND_PRIVACY_POLICY_PARAM;
            optinInputRegulationsAndPrivacyPolicy.value = true;
            orderInput.optIns.add(optinInputRegulationsAndPrivacyPolicy);
        }
        if (this.optInEmail) {
            ExtradomApi.OptInInput optinInputEmail = new ExtradomApi.OptInInput();
            optinInputEmail.name = ConstUtils.ORDER_OPT_IN_EMAIL_PARAM;
            optinInputEmail.value = true;
            orderInput.optIns.add(optinInputEmail);
        }
        if (this.optInPhone) {
            ExtradomApi.OptInInput optinInputPhone = new ExtradomApi.OptInInput();
            optinInputPhone.name = ConstUtils.ORDER_OPT_IN_PHONE_PARAM;
            optinInputPhone.value = true;
            orderInput.optIns.add(optinInputPhone);
        }
        if (this.optInProfiling) {
            ExtradomApi.OptInInput optinInputProfiling = new ExtradomApi.OptInInput();
            optinInputProfiling.name = ConstUtils.ORDER_OPT_IN_PROFILING_PARAM;
            optinInputProfiling.value = true;
            orderInput.optIns.add(optinInputProfiling);
        }
        // END RODO v2
        system.debug(this.addOns);
        if (String.isNotBlank(this.addOns)) {
            List<String> addOnsList = this.addOns.split(',');
            for (String addOn : addOnsList) {
                addOn = addOn.trim();
                if (String.isNotBlank(addOn)) {
                    ExtradomApi.AddOnInput addOnInput = new ExtradomApi.AddOnInput();
                    addOnInput.name = addOn;
                    orderInput.addOns.add(addOnInput);
                }
            }
        }
        return orderInput;
    }
    
    private String getPaymentMethod() {
        String paymentMethod = null;
        if (String.isNotBlank(this.shippingMethod)) {
            List<String> shipping = this.shippingMethod.split(' ');
            if (shipping.size() == 2) {
                String payMethod = shipping.get(1).trim().replace('(', '').replace(')', '');
                if (String.isNotBlank(payMethod)) {
                    paymentMethod = payMethod;
                }
            }
        }
        return paymentMethod;
    }
    
    private String getShippingMethod() {
        String shippingMethod = null;
        if (String.isNotBlank(this.shippingMethod)) {
            List<String> shipping = this.shippingMethod.split(' ');
            if (shipping.size() == 2) {
                String shipMethod = shipping.get(0).trim();
                if (String.isNotBlank(shipMethod)) {
                    shippingMethod = shipMethod;
                }
            }
        }
        return shippingMethod;
    }
    
    public class DesignOrderCreationControllerException extends Exception { }
}