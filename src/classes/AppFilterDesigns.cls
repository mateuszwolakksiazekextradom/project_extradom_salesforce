@RestResource(urlMapping='/app/filter/designs')
global class AppFilterDesigns {
    @HttpGet
    global static ExtradomApi.DesignResultPage doGet() {
    
        Integer p;
        
        String type;
        String mask;
        
        Integer minarea;
        Integer maxarea;
        Integer maxplotsize;
        Integer maxheight;
        Integer maxfootprint;
        Integer minroofslope;
        Integer maxroofslope;
        Integer rooms;
        String level;
        String garage;
        String basement;
        String livingtype;
        String roof;
        String ridge;
        String construction;
        String ceiling;
        String eco;
        String garagelocation;
        String style;
        String shape;
        String veranda;
        String pool;
        String wintergarden;
        String terrace;
        String oriel;
        String mezzanine;
        String entrance;
        String border;
        String carport;
        String cost;
        String gfroom;
        String eaves;
        String scarp;
        String fireplace;
        String bullseye;
        
        List<String> filters = new List<String>{'Status__c = \'Active\''};
        Map<String,String> canonicalParams = new Map<String,String>{};
        List<ID> internalTopDesignIds = new List<ID>();
        
        // Type
        if (RestContext.request.params.containsKey('type')) {
            type = RestContext.request.params.get('type');
            filters.add('type_'+String.valueOf(type)+'__c=1');
        } else {
            type = '1';
            filters.add('type_1__c=1');
        }
        canonicalParams.put('type',String.valueOf(type));
        // Page
        if (RestContext.request.params.containsKey('p')) {
            p = Integer.valueOf(RestContext.request.params.get('p'));
        } else {
            p = 1;
        }
        // mask
        if (RestContext.request.params.containsKey('mask')) {
            mask = RestContext.request.params.get('mask');
            canonicalParams.put('mask',RestContext.request.params.get('mask'));
            if (mask == 'latest') {
                filters.add('(Activation_Date__c = LAST_N_DAYS:90 OR Featured_Recently_Added__c > 0)');
            } else if (mask == 'promo') {
                filters.add('Promo_Price__c > 0');
            } else if (mask == 'top') {
                Map<Id,AggregateResult> topDesignsResults = new Map<Id,AggregateResult>([SELECT ParentId Id FROM Design__Feed WHERE Parent.Status__c = 'Active' AND Parent.Featured_Top_Built__c = null AND Parent.Top_Ready__c = true AND CreatedDate = last_n_days:30 AND Type = 'ContentPost' AND Visibility = 'AllUsers' GROUP BY ParentId ORDER BY SUM(LikeCount) DESC LIMIT 31]);
                internalTopDesignIds = new List<Id>(topDesignsResults.keySet());
                filters.add('(Featured_Top_Built__c > 0 OR Id IN :internalTopDesignIds)');
            }
        }
        // Usable Area
        if (RestContext.request.params.containsKey('minarea')) {
            minarea = Integer.valueOf(RestContext.request.params.get('minarea'));
            filters.add('Usable_Area__c >= '+String.valueOf(minarea));
            canonicalParams.put('minarea',RestContext.request.params.get('minarea'));
        }
        if (RestContext.request.params.containsKey('maxarea')) {
            maxarea = Integer.valueOf(RestContext.request.params.get('maxarea'));
            filters.add('Usable_Area__c <= '+String.valueOf(maxarea));
            canonicalParams.put('maxarea',RestContext.request.params.get('maxarea'));
        }
        // Plot Size
        if (RestContext.request.params.containsKey('maxplotsize')) {
            maxplotsize = Integer.valueOf(RestContext.request.params.get('maxplotsize'));
            filters.add('Minimum_Plot_Size_Horizontal__c <= '+String.valueOf(maxplotsize));
            canonicalParams.put('maxplotsize',RestContext.request.params.get('maxplotsize'));
        }
        // Height
        if (RestContext.request.params.containsKey('maxheight')) {
            maxheight = Integer.valueOf(RestContext.request.params.get('maxheight'));
            filters.add('Height__c <= '+String.valueOf(maxheight));
            canonicalParams.put('maxheight',RestContext.request.params.get('maxheight'));
        }
        // Footprint
        if (RestContext.request.params.containsKey('maxfootprint')) {
            maxfootprint = Integer.valueOf(RestContext.request.params.get('maxfootprint'));
            filters.add('Footprint__c <= '+String.valueOf(maxfootprint));
            canonicalParams.put('maxfootprint',RestContext.request.params.get('maxfootprint'));
        }
        // Roof Slope
        if (RestContext.request.params.containsKey('minroofslope')) {
            minroofslope = Integer.valueOf(RestContext.request.params.get('minroofslope'));
            filters.add('Roof_slope__c >= '+String.valueOf(minroofslope));
            canonicalParams.put('minroofslope',RestContext.request.params.get('minroofslope'));
        }
        if (RestContext.request.params.containsKey('maxroofslope')) {
            maxroofslope = Integer.valueOf(RestContext.request.params.get('maxroofslope'));
            filters.add('Roof_slope__c <= '+String.valueOf(maxroofslope));
            canonicalParams.put('maxroofslope',RestContext.request.params.get('maxroofslope'));
        }
        // Count Rooms
        if (RestContext.request.params.containsKey('rooms')) {
            rooms = Integer.valueOf(RestContext.request.params.get('rooms'));
            filters.add('Count_Rooms__c = '+String.valueOf(rooms));
            canonicalParams.put('rooms',RestContext.request.params.get('rooms'));
        }
        // Level
        if (RestContext.request.params.containsKey('level')) {
            level = RestContext.request.params.get('level');
            filters.add('level_'+String.valueOf(level)+'__c=1');
            canonicalParams.put('level',RestContext.request.params.get('level'));
        }
        // Garage
        if (RestContext.request.params.containsKey('garage')) {
            garage = RestContext.request.params.get('garage');
            filters.add('garage_'+String.valueOf(garage)+'__c=1');
            canonicalParams.put('garage',RestContext.request.params.get('garage'));
        }
        // Roof
        if (RestContext.request.params.containsKey('roof')) {
            roof = RestContext.request.params.get('roof');
            filters.add('roof_'+String.valueOf(roof)+'__c=1');
            canonicalParams.put('roof',RestContext.request.params.get('roof'));
        }
        // Living Type
        if (RestContext.request.params.containsKey('livingtype')) {
            livingtype = RestContext.request.params.get('livingtype');
            filters.add('livingtype_'+String.valueOf(livingtype)+'__c=1');
            canonicalParams.put('livingtype',RestContext.request.params.get('livingtype'));
        }
        // Style
        if (RestContext.request.params.containsKey('style')) {
            style = RestContext.request.params.get('style');
            filters.add('style_'+String.valueOf(style)+'__c=1');
            canonicalParams.put('style',RestContext.request.params.get('style'));
        }
        // Construcion
        if (RestContext.request.params.containsKey('construction')) {
            construction = RestContext.request.params.get('construction');
            filters.add('construction_'+String.valueOf(construction)+'__c=1');
            canonicalParams.put('construction',RestContext.request.params.get('construction'));
        }
        // Eco
        if (RestContext.request.params.containsKey('eco')) {
            eco = RestContext.request.params.get('eco');
            filters.add('eco_'+String.valueOf(eco)+'__c=1');
            canonicalParams.put('eco',RestContext.request.params.get('eco'));
        }
        // Garage Location
        if (RestContext.request.params.containsKey('garagelocation')) {
            garagelocation = RestContext.request.params.get('garagelocation');
            filters.add('garagelocation_'+String.valueOf(garagelocation)+'__c=1');
            canonicalParams.put('garagelocation',RestContext.request.params.get('garagelocation'));
        }
        // Basement
        if (RestContext.request.params.containsKey('basement')) {
            basement = RestContext.request.params.get('basement');
            filters.add('basement_'+String.valueOf(basement)+'__c=1');
            canonicalParams.put('basement',RestContext.request.params.get('basement'));
        }
        // Ridge
        if (RestContext.request.params.containsKey('ridge')) {
            ridge = RestContext.request.params.get('ridge');
            filters.add('ridge_'+String.valueOf(ridge)+'__c=1');
            canonicalParams.put('ridge',RestContext.request.params.get('ridge'));
        }
        // Shape
        if (RestContext.request.params.containsKey('shape')) {
            shape = RestContext.request.params.get('shape');
            filters.add('shape_'+String.valueOf(shape)+'__c=1');
            canonicalParams.put('shape',RestContext.request.params.get('shape'));
        }
        // Veranda
        if (RestContext.request.params.containsKey('veranda')) {
            veranda = RestContext.request.params.get('veranda');
            filters.add('veranda_'+String.valueOf(veranda)+'__c=1');
            canonicalParams.put('veranda',RestContext.request.params.get('veranda'));
        }
        // Pool
        if (RestContext.request.params.containsKey('pool')) {
            pool = RestContext.request.params.get('pool');
            filters.add('pool_'+String.valueOf(pool)+'__c=1');
            canonicalParams.put('pool',RestContext.request.params.get('pool'));
        }
        // Winter Garden
        if (RestContext.request.params.containsKey('wintergarden')) {
            wintergarden= RestContext.request.params.get('wintergarden');
            filters.add('wintergarden_'+String.valueOf(wintergarden)+'__c=1');
            canonicalParams.put('wintergarden',RestContext.request.params.get('wintergarden'));
        }
        // Terrace
        if (RestContext.request.params.containsKey('terrace')) {
            terrace = RestContext.request.params.get('terrace');
            filters.add('terrace_'+String.valueOf(terrace)+'__c=1');
            canonicalParams.put('terrace',RestContext.request.params.get('terrace'));
        }
        // Oriel
        if (RestContext.request.params.containsKey('oriel')) {
            oriel = RestContext.request.params.get('oriel');
            filters.add('oriel_'+String.valueOf(oriel)+'__c=1');
            canonicalParams.put('oriel',RestContext.request.params.get('oriel'));
        }
        // Mezzanine
        if (RestContext.request.params.containsKey('mezzanine')) {
            mezzanine = RestContext.request.params.get('mezzanine');
            filters.add('mezzanine_'+String.valueOf(mezzanine)+'__c=1');
            canonicalParams.put('mezzanine',RestContext.request.params.get('mezzanine'));
        }
        // Entrance
        if (RestContext.request.params.containsKey('entrance')) {
            entrance = RestContext.request.params.get('entrance');
            filters.add('entrance_'+String.valueOf(entrance)+'__c=1');
            canonicalParams.put('entrance',RestContext.request.params.get('entrance'));
        }
        // Border
        if (RestContext.request.params.containsKey('border')) {
            border = RestContext.request.params.get('border');
            filters.add('border_'+String.valueOf(border)+'__c=1');
            canonicalParams.put('border',RestContext.request.params.get('border'));
        }
        // Carport
        if (RestContext.request.params.containsKey('carport')) {
            carport = RestContext.request.params.get('carport');
            filters.add('carport_'+String.valueOf(carport)+'__c=1');
            canonicalParams.put('carport',RestContext.request.params.get('carport'));
        }
        // Cost
        if (RestContext.request.params.containsKey('cost')) {
            cost = RestContext.request.params.get('cost');
            filters.add('cost_'+String.valueOf(cost)+'__c=1');
            canonicalParams.put('cost',RestContext.request.params.get('cost'));
        }
        // Ground Floor Room
        if (RestContext.request.params.containsKey('gfroom')) {
            gfroom = RestContext.request.params.get('gfroom');
            filters.add('gfroom_'+String.valueOf(gfroom)+'__c=1');
            canonicalParams.put('gfroom',RestContext.request.params.get('gfroom'));
        }
        // Ceiling
        if (RestContext.request.params.containsKey('ceiling')) {
            ceiling = RestContext.request.params.get('ceiling');
            filters.add('ceiling_'+String.valueOf(ceiling)+'__c=1');
            canonicalParams.put('ceiling',RestContext.request.params.get('ceiling'));
        }
        // Eaves
        if (RestContext.request.params.containsKey('eaves')) {
            eaves = RestContext.request.params.get('eaves');
            filters.add('eaves_'+String.valueOf(eaves)+'__c=1');
            canonicalParams.put('eaves',RestContext.request.params.get('eaves'));
        }
        // Scarp
        if (RestContext.request.params.containsKey('scarp')) {
            scarp = RestContext.request.params.get('scarp');
            filters.add('scarp_'+String.valueOf(scarp)+'__c=1');
            canonicalParams.put('scarp',RestContext.request.params.get('scarp'));
        }
        // Fireplace
        if (RestContext.request.params.containsKey('fireplace')) {
            fireplace = RestContext.request.params.get('fireplace');
            filters.add('fireplace_'+String.valueOf(fireplace)+'__c=1');
            canonicalParams.put('fireplace',RestContext.request.params.get('fireplace'));
        }
        // Bull's Eye
        if (RestContext.request.params.containsKey('bullseye')) {
            bullseye = RestContext.request.params.get('bullseye');
            filters.add('bullseye_'+String.valueOf(bullseye)+'__c=1');
            canonicalParams.put('bullseye',RestContext.request.params.get('bullseye'));
        }
        
        String sortExpr = 'Featured__c DESC NULLS LAST, Order_Score__c DESC NULLS LAST';
        if (type == '2') {
            sortExpr = 'Featured_Garage__c DESC NULLS LAST, Order_Score__c DESC NULLS LAST';
        } else if (type == '10') {
            sortExpr = 'Featured_Utility_Building__c DESC NULLS LAST, Order_Score__c DESC NULLS LAST';
        } else if (mask == 'latest') {
            sortExpr = 'Featured_Recently_Added__c DESC NULLS LAST, Activation_Date__c DESC NULLS LAST';
        } else if (mask == 'promo') {
            sortExpr = 'Featured_Promo__c DESC NULLS LAST, Order_Score__c DESC NULLS LAST';
        } else if (mask == 'top') {
            sortExpr = 'Featured_Top_Built__c DESC NULLS LAST, Order_Score__c DESC NULLS LAST';
        }
        
        List<Design__c> designs;
        String query = 'SELECT Id, Code__c, Full_Name__c, Type__c, Gross_Price__c, Current_Price__c, Thumbnail_Base_URL__c, Usable_Area__c, Sketchfab_ID__c, KW_Model__c FROM Design__c';
        query += ' WHERE ' + String.join(filters,' AND ');
        if (p<=84) {
            query += ' ORDER BY '+sortExpr+' LIMIT 24 OFFSET '+String.valueOf((p-1)*24);
            designs = (List<Design__c>)Database.query(query);
        } else {
            query += ' ORDER BY '+sortExpr+' LIMIT '+String.valueOf((p-83)*24)+' OFFSET 1992'; // 83*24=1992
            List<Design__c> tmpDesigns = (List<Design__c>)Database.query(query);
            designs = new List<Design__c>{};
            for (Integer i=24*(p-84); i<tmpDesigns.size(); i++) {
                designs.add(tmpDesigns.get(i));
            }
        }
        
        PageReference currentPage = new PageReference('/app/filter/designs');
        currentPage.getParameters().putAll(canonicalParams);
        if (p>1) {
            currentPage.getParameters().put('p',String.valueOf(p));
        }
        
        ExtradomApi.DesignResultPage designResultPage;
        designResultPage = new ExtradomApi.DesignResultPage(designs);
        designResultPage.currentPageUrl = currentPage.getUrl();
        if (designResultPage.designResults.size()==24) {
            PageReference nextPage = new PageReference('/app/filter/designs');
            nextPage.getParameters().putAll(canonicalParams);
            nextPage.getParameters().put('p',String.valueOf(p+1));
            designResultPage.nextPageUrl = nextPage.getUrl();
        }
        
        if (UserInfo.getUserType()=='Guest') {
            RestContext.response.addHeader('Cache-Control', 'must-revalidate, max-age=0, s-maxage=3600');
        } else {
            RestContext.response.addHeader('Cache-Control', 'no-cache, must-revalidate, max-age=0, no-store, private, s-maxage=0');
        }
        
        return designResultPage;
    }
}