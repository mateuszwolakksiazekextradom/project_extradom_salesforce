@IsTest
private class DesignSetGrossPriceBatch_Test {
    private static final Integer NUM_OF_DESIGNS = 100;

    @IsTest
    private static void shouldProcessPendingDesigns() {
        // given
        Supplier__c newSupplier = new Supplier__c(Name = 'Supplier', Code__c = '11', Full_Name__c = 'Great Supplier');
        insert newSupplier;
        final String DESIGN_TO_PROCESS_PREFIX = 'Design Pending Before ';
        final String DESIGN_NOT_TO_PROCESS_PREFIX = 'Design Pending After ';
        List<Design__c> designs = new List<Design__c> ();
        for (Integer i = 0; i < NUM_OF_DESIGNS; i++) {
            designs.add(
                    new Design__c (
                            Name = DESIGN_TO_PROCESS_PREFIX + String.valueOf(i), Supplier__c = newSupplier.Id, Full_Name__c = DESIGN_TO_PROCESS_PREFIX + String.valueOf(i),
                            Type__c = ConstUtils.DESIGN_HOUSE_TYPE, Pending_Gross_Price__c = 20.0,
                            Pending_Promo_Name__c = 'Test Name', Pending_Gross_Price_Start_Date__c = system.now().addDays(-i)
                    )
            );
        }
        for (Integer i = 0; i < NUM_OF_DESIGNS; i++) {
            designs.add(
                    new Design__c (
                            Name = DESIGN_NOT_TO_PROCESS_PREFIX + String.valueOf(i), Supplier__c = newSupplier.Id, Full_Name__c = DESIGN_NOT_TO_PROCESS_PREFIX + String.valueOf(i),
                            Type__c = ConstUtils.DESIGN_HOUSE_TYPE, Pending_Gross_Price__c = 20.0,
                            Pending_Promo_Name__c = 'Test Name', Pending_Gross_Price_Start_Date__c = system.now().addDays(i+1)
                    )
            );
        }
        insert designs;

        // when
        Test.startTest();
        Database.executeBatch(new DesignSetGrossPriceBatch());
        Test.stopTest();
        List<Design__c> processedDesigns = [
                SELECT Id, Name, Pending_Gross_Price__c, Gross_Price__c, Pending_Gross_Price_Start_Date__c FROM Design__c
                WHERE Pending_Gross_Price_Start_Date__c = null
        ];

        // then
        System.assertEquals(NUM_OF_DESIGNS, processedDesigns.size());
        for (Design__c processedDesign : processedDesigns) {
            System.assert(processedDesign.Name.startsWith(DESIGN_TO_PROCESS_PREFIX));
            System.assertEquals(null, processedDesign.Pending_Gross_Price_Start_Date__c);
            System.assertEquals(null, processedDesign.Pending_Gross_Price__c);
            System.assertEquals(20.0, processedDesign.Gross_Price__c);
        }
    }
}