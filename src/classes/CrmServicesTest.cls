@isTest
private class CrmServicesTest {

    static testMethod void crmMethodsTest() {
        
        test.startTest();
        
        Account a = new Account(Name='123456789');
        insert a;
        Contact c = new Contact(LastName='123456789', Phone='123456789', AccountId=a.Id);
        insert c;
        
        Account a2 = new Account(Name='firma', Type='Company');
        insert a2;
        Contact c2 = new Contact(LastName='firma', Phone='123123123', AccountId=a2.Id);
        insert c2;
        
        Account a3 = new Account(Name='partner', Type='Partner');
        insert a3;
        Contact c3 = new Contact(LastName='partner', Phone='123412345', AccountId=a3.Id);
        insert c3;
        
        Account a4 = new Account(Name='supplier', Type='Supplier');
        insert a4;
        Contact c4 = new Contact(LastName='supplier', Phone='123456780', AccountId=a4.Id);
        insert c4;

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/crm/route';  
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        CrmRoute.doPost('717152060', '123456789', 'callid12345');
        
        req = new RestRequest(); 
        res = new RestResponse();
        req.requestURI = '/services/apexrest/crm/route';  
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        CrmRoute.doPost('717152067', '123456780', 'callid12346');
        
        req = new RestRequest(); 
        res = new RestResponse();
        req.requestURI = '/services/apexrest/crm/route';  
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        CrmRoute.doPost('717152067', '123123123', 'callid12346');
        
        req = new RestRequest(); 
        res = new RestResponse();
        req.requestURI = '/services/apexrest/crm/route';  
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        CrmRoute.doPost('717152067', '123412345', 'callid12346');
        
        req = new RestRequest(); 
        res = new RestResponse();
        req.requestURI = '/services/apexrest/crm/route';  
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        CrmRoute.doPost('717152076', '123412345', 'callid12346');
        
        req = new RestRequest(); 
        res = new RestResponse();
        req.requestURI = '/services/apexrest/crm/route';  
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        CrmRoute.doPost('717152076', '123123123', 'callid12346');
        
        req = new RestRequest(); 
        res = new RestResponse();
        req.requestURI = '/services/apexrest/crm/route';  
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        CrmRoute.doPost('717152076', '123456780', 'callid12346');
        
        req = new RestRequest(); 
        res = new RestResponse();
        req.requestURI = '/services/apexrest/crm/route';  
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        CrmRoute.doPost('717152076', '123123456', 'callid12346');

        test.stopTest();

    }
    
}