@isTest
private class OptInsControllerTest {
    private static testMethod void myUnitTest() {
    
        Account a = new Account(Name='Test');
        insert a;
        Contact c = new Contact(LastName='Test', Email='test@extradom.pl');
        insert c;
        
        PageReference pageRef = new PageReference('https://www.extradom.pl/OptInMarketingSetView?id='+c.Id);
        Test.setCurrentPage(pageRef);
        
        OptInsController ctrl = new OptInsController();
        ctrl.initSetOptInMarketing();
        ctrl.initSetOptInLeadForward();
        
        Contact c_after = [SELECT Id, Opt_In_Marketing__c, Opt_In_Lead_Forward__c FROM Contact WHERE Id = :c.Id];
        System.assertEquals(c_after.Opt_In_Marketing__c, true);
        System.assertEquals(c_after.Opt_In_Lead_Forward__c, true);
    
    }
}