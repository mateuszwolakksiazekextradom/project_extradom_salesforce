public class OrderSendEmailActionExtension {
    Order order;
    public OrderSendEmailActionExtension (ApexPages.StandardController stdController) {
        if (!Test.isRunningTest()) {
            stdController.addFields(new List<String> {'Send_Email__c'});
        }
        order = (Order)stdController.getRecord();
    }
    
    public void setSendEmailAction() {
        order.Send_Email__c = true;
        update order;
    }
}