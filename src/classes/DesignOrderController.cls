public without sharing class DesignOrderController {

    private String code;
    private String ver;
    public String designVersion {get;set;}
    public String billingFirstName {get;set;}
    public String billingLastName {get;set;}
    public String billingAddressStreetName {get;set;}
    public String billingAddressStreetNo {get;set;}
    public String billingAddressStreetFlat {get;set;}
    public String billingPostalCodePart1 {get;set;}
    public String billingPostalCodePart2 {get;set;}
    public String billingCity {get;set;}
    public String billingEmail {get;set;}
    public String billingPhone {get;set;}
    public String billingCompany {get;set;}
    public String billingTaxNumber {get;set;}
    public String shippingFirstName {get;set;}
    public String shippingLastName {get;set;}
    public String shippingAddressStreetName {get;set;}
    public String shippingAddressStreetNo {get;set;}
    public String shippingAddressStreetFlat {get;set;}
    public String shippingPostalCodePart1 {get;set;}
    public String shippingPostalCodePart2 {get;set;}
    public String shippingCity {get;set;}
    public String shippingPhone {get;set;}
    public String addOnsDetails {get;set;}
    public String addOns {get;set;}
    public String shippingMethod {get;set;}
    public Boolean optInRegulations {get;set;}
    public Boolean optInOrder {get;set;}
    public Boolean optIn {get;set;}
    public Boolean optInForward {get;set;}
    
    public DesignOrderController() {
        this.code = ApexPages.currentPage().getParameters().get('code');
        this.ver = ApexPages.currentPage().getParameters().get('ver');
        this.optInRegulations = true;
        this.optInOrder = true;
        this.optIn = false;
        this.optInForward = false;
        this.shippingMethod = 'Regular (Prepayment)';
        this.designVersion = 'Basic';
        if (this.ver=='mirrored') this.designVersion = 'Mirrored';
    }
    
    public Design__c getDesign() {
        return [SELECT Id, Full_Name__c, Code__c, Mirror__c, Gross_Price__c, Current_Price__c, Thumbnail_Base_URL__c, Type__c, Return_Conditions__c, Swap_Conditions__c FROM Design__c WHERE Code__c != '' AND Code__c = :this.code];
    }
    
    public PageReference save() {
        Order__c o = new Order__c();
        o.Design__c = this.getDesign().id;
        o.Quantity__c = 1;
        o.Design_Version__c = this.designVersion;
        o.Billing_First_Name__c = this.billingFirstName;
        o.Billing_Last_Name__c = this.billingLastName;
        o.Billing_Address_Street_Name__c = this.billingAddressStreetName;
        o.Billing_Address_Street_No__c = this.billingAddressStreetNo;
        o.Billing_Address_Street_Flat__c = this.billingAddressStreetFlat;
        o.Billing_Postal_Code_Part_1__c = this.billingPostalCodePart1;
        o.Billing_Postal_Code_Part_2__c = this.billingPostalCodePart2;
        o.Billing_City__c = this.billingCity;
        o.Billing_Email__c = this.billingEmail;
        o.Billing_Phone__c = this.billingPhone;
        o.Billing_Company__c = this.billingCompany;
        o.Billing_Tax_Number__c = this.billingTaxNumber;
        o.Shipping_First_Name__c = this.shippingFirstName;
        o.Shipping_Last_Name__c = this.shippingLastName;
        o.Shipping_Address_Street_Name__c = this.shippingAddressStreetName;
        o.Shipping_Address_Street_No__c = this.shippingAddressStreetNo;
        o.Shipping_Address_Street_Flat__c = this.shippingAddressStreetFlat;
        o.Shipping_Postal_Code_Part_1__c = this.shippingPostalCodePart1;
        o.Shipping_Postal_Code_Part_2__c = this.shippingPostalCodePart2;
        o.Shipping_City__c = this.shippingCity;
        o.Shipping_Phone__c = this.shippingPhone;
        o.Add_Ons_Details__c = this.addOnsDetails;
        o.Add_Ons__c = this.addOns;
        o.Shipping_Method__c = this.shippingMethod;
        o.Opt_In_Regulations__c = this.optInRegulations;
        o.Opt_In_Order__c = this.optInOrder;
        o.Opt_In__c = this.optIn;
        o.Opt_In_Forward__c = this.optInForward;
        List<Contact> contactsBillingEmail = [SELECT Id, AccountId FROM Contact WHERE EmailId__c = :this.billingEmail AND EmailId__c != '' LIMIT 1];
        List<Contact> contactsBillingPhone = [SELECT Id, AccountId FROM Contact WHERE PhoneId__c = :this.billingPhone AND PhoneId__c != '' LIMIT 1];
        List<Contact> contacts = new List<Contact>{};
        contacts.addAll(contactsBillingEmail);
        contacts.addAll(contactsBillingPhone);
        if (contacts.size()>0) {
            o.Account__c = contacts[0].AccountId;
        } else {
            Account a = new Account();
            if (String.isNotBlank(this.billingCompany)) {
                a.Name = this.billingCompany;
            } else {
                a.Name = this.billingFirstName + ' ' + this.billingLastName;
            }
            a.OwnerId = this.getNewAccountOwnerId();
            a.AccountSource = 'Extradom.pl (Design)';
            insert a;
            Contact c = new Contact(AccountId=a.Id,FirstName=this.billingFirstName,LastName=this.billingLastName,Email=this.billingEmail,Phone=this.billingPhone);
            c.OwnerId = a.OwnerId;
            insert c;
            o.Account__c = a.Id;
        }
        insert o;
        
        try {
            if (this.shippingMethod.contains('(Online)')) {
                Order__c order = [SELECT Id, Pay_URL__c FROM Order__c WHERE Id = :o.Id];
                PageReference payPageRef = new PageReference(order.Pay_URL__c);
                payPageRef.setRedirect(true);
                return payPageRef;
            }
        } catch(Exception e) {
        }
        
        PageReference retPageRef = Page.DesignOrderViewComplete;
        retPageRef.getParameters().put('code',this.getDesign().Code__c);
        if (this.designVersion == 'Mirrored') retPageRef.getParameters().put('ver','mirrored');
        retPageRef.setRedirect(true);
        return retPageRef;
    }
    
    private Id getNewAccountOwnerId() {
        List<AggregateResult> usersLogin = [SELECT userid, min(logintime) firstLogin FROM LoginHistory WHERE logintime = TODAY GROUP BY userid];
        List<GroupMember> members = [SELECT UserOrGroupId FROM GroupMember WHERE GroupId = '00Gb0000000Qpp7'];
        List<GroupMember> lastSeenMembers = New List<GroupMember>();
        
        List<AggregateResult> days = [Select DAY_IN_MONTH(loginTime) day FROM LoginHistory WHERE LoginTime = THIS_MONTH AND LoginTime <> TODAY AND SourceIp = '195.94.192.98' AND UserId IN (Select Id From User WHERE IsActive = true AND ProfileId = '00eb0000000I9GZ') GROUP BY DAY_IN_MONTH(loginTime) HAVING count_distinct(userId) > 1];
        
        Integer[] daysArray = new Integer[]{};
        for (AggregateResult day : days) {
            daysArray.add((Integer)day.get('day'));
        }
        
        List<AggregateResult> activeUsers = [Select userId FROM LoginHistory WHERE LoginTime = THIS_MONTH AND LoginTime <> TODAY AND SourceIp = '195.94.192.98' AND UserId IN (Select Id From User WHERE IsActive = true AND ProfileId = '00eb0000000I9GZ') GROUP BY UserId, day_in_month(loginTime)];
        
        String orderAgentId = '';
        Double orderAgentRate = 0;
        Integer orderAgentCount = 0;
        
        for (AggregateResult ul : usersLogin) {
           for (GroupMember m : members) {
                if (ul.get('userid')==(String)m.UserOrGroupId) {
                    DateTime qStart = (DateTime)ul.get('firstLogin');
                    Double qHours = (System.now().getTime()-qStart.getTime())/1000.0/60.0/60.0;
                    Double qTotalHours = qHours;
                    for (AggregateResult activeUser : activeUsers) {
                        if (activeUser.get('userId')==(String)m.UserOrGroupId) {
                            qTotalHours += 8.0;
                        }
                    }
                    lastSeenMembers.add(m);
                    if (qHours < 8.0) {
                        Integer qCount = 0;
                        qCount = [SELECT count() FROM Account WHERE OwnerId = :m.UserOrGroupId AND SPAM__c = false AND AccountSource like '%(Design)' AND CreatedDate = THIS_MONTH];
                        Double qRate = qCount / qTotalHours;
                        if (qRate <= orderAgentRate || orderAgentId == '') {
                            orderAgentId = (String)m.UserOrGroupId;
                            orderAgentRate = qRate;
                            orderAgentCount = qCount;
                        }
                    }
                }
            }
        }
        if (orderAgentId == '') {
            orderAgentId = '005b0000000RxOP';
        }
        return orderAgentId;
    }

}