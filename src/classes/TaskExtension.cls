public class TaskExtension {
 
    Task t;
    public TaskExtension(ApexPages.StandardController stdController) {
        this.t = (Task)stdController.getRecord();
    }
    
    public String getDesignCosts() {
        Id design = [SELECT Id, WhatId FROM Task WHERE What.Type = 'Design__c' AND Id=:this.t.Id LIMIT 1][0].WhatId;
        return [SELECT Id, Documents__c FROM Design__c WHERE Id = :design][0].Documents__c;
    }    
}