global class NotusToolsSchedule implements Schedulable, Database.AllowsCallouts, Database.Batchable<Telemarketing__c> {
   
    public List<Telemarketing__c> tels;
    
    public NotusToolsSchedule(List<Telemarketing__c> tels) {
        this.tels = tels;
    }
    
    public NotusToolsSchedule() {
    }
    
///////////////////////////////////////////////////////////////////////////////////////////////////////
//****************** SCHEDULEABLE SECTION ******************//
///////////////////////////////////////////////////////////////////////////////////////////////////////

   global void execute(SchedulableContext sc) {
       tels = [SELECT id, notus_id__c, notus_stage__c, notus_status__c FROM telemarketing__c WHERE notus_stage__c = 'PENDING' AND notus_id__c != ''];
       if(!tels.isEmpty()) {
           Database.executebatch(new NotusToolsSchedule(tels),100);
       }
   }
         
///////////////////////////////////////////////////////////////////////////////////////////////
//****************** BATCHABLE SECTION ******************//
///////////////////////////////////////////////////////////////////////////////////////////////

    global Iterable<Telemarketing__c> start(Database.BatchableContext BC){       
        return (tels);
    }

    global void execute(Database.BatchableContext BC, List<Telemarketing__c> scope){
               
        Http http = new Http();
        HTTPResponse res = new HTTPResponse();        
        HttpRequest req = new HttpRequest();  
        
        req.setMethod('GET');   
        req.setHeader('Content-Type','application/json');
        
        Response caseStatus = new Response();
            
        List<Telemarketing__c> tels = new List<Telemarketing__c>();
        for(Telemarketing__c tel : scope) {                      
            req.setEndpoint('callout:notus/hamster/v1/proposalCases/'+tel.Notus_Id__c);
                      
            try {
                res = http.send(req);
                caseStatus = (Response)JSON.deserializeStrict(res.getBody(), Response.class);
                system.debug(caseStatus);
            } catch (exception e) {
                system.debug('Notus New Lead Callout Exception: ' + e.getMessage());
            }
            if (tel.Notus_Status__c != caseStatus.statusSystemName || tel.Notus_Stage__c != caseStatus.Stage) {
                tel.Notus_Status__c = caseStatus.statusSystemName;
                tel.Notus_Stage__c = caseStatus.Stage;
                tels.add(tel);
            }
            
        }
        if (!tels.isEmpty()) {
            update tels;
        }
    }
    
    global void finish(Database.BatchableContext BC){
    }
    
////////////////////////////////////////////////////////////////////////////////////////////
//****************** INTERNAL CLASSES ******************//
////////////////////////////////////////////////////////////////////////////////////////////         
    
    public class Response {        
        String href;
        String hash;
        String statusSystemName;
        String stage;
        String message;
        String statusDateTime;
        String proposalLeadHash;
        String status;
    }
}