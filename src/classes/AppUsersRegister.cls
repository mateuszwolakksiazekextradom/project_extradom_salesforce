@RestResource(urlMapping='/app/users/register')
global without sharing class AppUsersRegister {
    
    global class OptIn {
        public String var;
        public Boolean value;
    }
    
    @HttpPost
    global static Map<String,String> doPost(String username, String password, List<OptIn> optIns) {
    
        Map<String,String> result = new Map<String,String>{};
        
        String profileId = null; // To be filled in by customer.
        String roleEnum = null; // To be filled in by customer.
        String accountId = ''; // To be filled in by customer.
        
        RestContext.response.addHeader('Cache-Control', 'no-cache, must-revalidate, max-age=0, no-store, private, s-maxage=0');
        
        if (!Site.isValidUsername(username)) {
            RestContext.response.statusCode = 400;
            return new Map<String,String>{'message' => 'Nieprawidłowy email'};
        }
        
        String nickname = username.split('@')[0];
        
        for (User u : [SELECT Id FROM User WHERE Username = :username LIMIT 1]) {
            RestContext.response.statusCode = 400;
            return new Map<String,String>{'message' => 'Pod tym emailem jest już użytkownik'};
        }
        
        for (User u : [SELECT Id FROM User WHERE communityNickname = :nickname LIMIT 1]) {
            nickname += String.valueOf(100+Math.round(Math.random()*10000));
        }

        User u = new User();
        u.Username = username;
        u.communityNickname = nickname;
        u.Email = username;
        u.ProfileId = profileId;
        u.LastName = nickname;
        
        List<Contact> contacts = [SELECT Id, AccountId FROM Contact WHERE EmailId__c = :username AND EmailId__c != '' LIMIT 1];
        if (contacts.size()>0) {
            Contact c = contacts[0];
            accountId = c.AccountId;
        } else {
            Account a = new Account();
            a.Name = username;
            a.OwnerId = '005b0000000RxOP';
            a.AccountSource = 'Extradom.pl (App)';
            a.Registration_Start_URL__c = '';
            insert a;
            accountId = a.Id;
        }
        
        try {
            ID userId = Site.createPortalUser(u, accountId, password);
            try {
                Boolean optInRegulations = false;
                Boolean optInPrivacyPolicy = false;
                Boolean optInMarketing = false;
                Boolean optInMarketingExtended = false;
                Boolean optInMarketingPartner = false;
                Boolean optInLeadForward = false;
                Boolean optInEmail = false;
                Boolean optInPhone = false;
                Boolean optInProfiling = false;
                Boolean optInPKO = false;
                for (OptIn optIn : optIns) {
                    if (optIn.var == ConstUtils.ORDER_OPT_IN_REGULATIONS_PARAM) {
                        optInRegulations = optIn.value;
                    }
                    if (optIn.var == ConstUtils.ORDER_OPT_IN_PRIVACY_POLICY_PARAM) {
                        optInPrivacyPolicy = optIn.value;
                    }
                    if (optIn.var == ConstUtils.ORDER_OPT_IN_MARKETING_PARAM) {
                        optInMarketing = optIn.value;
                    }
                    if (optIn.var == ConstUtils.ORDER_OPT_IN_MARKETING_EXTENDED_PARAM) {
                        optInMarketingExtended = optIn.value;
                    }
                    if (optIn.var == ConstUtils.ORDER_OPT_IN_MARKETING_PARTNER_PARAM) {
                        optInMarketingPartner = optIn.value;
                    }
                    if (optIn.var == ConstUtils.ORDER_OPT_IN_LEAD_FORWARD_PARAM) {
                        optInLeadForward = optIn.value;
                    }
                    if (optIn.var == ConstUtils.ORDER_OPT_IN_EMAIL_PARAM) {
                        optInEmail = optIn.value;
                    }
                    if (optIn.var == ConstUtils.ORDER_OPT_IN_PHONE_PARAM) {
                        optInPhone = optIn.value;
                    }
                    if (optIn.var == ConstUtils.ORDER_OPT_IN_PROFILING_PARAM) {
                        optInProfiling = optIn.value;
                    }
                    if (optIn.var == ConstUtils.ORDER_OPT_IN_PKO_PARAM) {
                        optInPKO = optIn.value;
                    }
                }
                OrderMethods.updateOptIns(username, '', optInRegulations, optInPrivacyPolicy, optInMarketing, optInMarketingExtended, optInMarketingPartner, optInLeadForward, optInEmail, optInPhone, optInProfiling, optInPKO);
            } catch (Exception e2) {
            }
            return new Map<String,String>{'id' => userId};
        } catch (Exception e) {
            RestContext.response.statusCode = 400;
            return new Map<String,String>{'message' => 'Hasło musi mieć 8 znaków (cyfry i litery)'};
        }
        
    }
}