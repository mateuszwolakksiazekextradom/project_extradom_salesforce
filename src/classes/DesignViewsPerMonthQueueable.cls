/**
 * Created by grzegorz.dlugosz on 14.05.2019.
 */
/* This Queueable class is called from scheduler. The purpose of this class is to call Google Analytics API
*  using Named Credentials and gather data related to how many views specified design got in the span of last
*  30 days. Gathered and matched data/designs are then passed to Batch that will update those records.*/
global class DesignViewsPerMonthQueueable implements Queueable, Database.AllowsCallouts {
    public static final String VIEW_ID = '118622780'; // id of view, specified in Google Analytics configuration
    public static final String PAGE_SIZE = '5000';  // How many records are requested per callout

    public String pageToken; // This is basically 'OFFSET' used for pagination in API

    global DesignViewsPerMonthQueueable(String pageToken) {
        this.pageToken = pageToken;
    }

    global void execute(QueueableContext qc) {
        // Build request
        String requestBody = getRequestBody();

        // Create Http Request and receive response
        HTTPResponse response = createRequestAndSend(requestBody);

        // Parse the response
        ResponseWrapper respWrapper = (ResponseWrapper) JSON.deserialize(response.getBody(), ResponseWrapper.class);

        // Find the designs to update based on  (already updated locally)
        List<Design__c> designToUpdateList = matchAndUpdateRecords(respWrapper);

        if (designToUpdateList != null && !designToUpdateList.isEmpty()) {
            // update records in batch - for better scalability
            DesignViewsPerMonthBatch btch = new DesignViewsPerMonthBatch(designToUpdateList);
            Database.executeBatch(btch, 200);
        }

        // update page token and determine if next request is needed - chain queueable
        String responsePageToken = respWrapper.reports.get(0).nextPageToken;
        if (String.isNotBlank(responsePageToken)) {
            System.enqueueJob(new DesignViewsPerMonthQueueable(responsePageToken));
        }
    }

    /**
      * Author: Grzegorz Długosz
      * Description: Creates and sends Http Request based on input body and Named Credentials.
      * @param requestBody
      *        Built earlier body of the request.
      * @return HTTPResponse element from requested service.
      */
    private HTTPResponse createRequestAndSend(String requestBody) {
        HttpRequest req = new HttpRequest();
        // Authentication - using Named Credentials; This way token should always be fresh
        req.setEndpoint('callout:Google_Analytics/reports:batchGet');
        req.setHeader('Accept', 'application/json');
        req.setHeader('Content-Type', 'application/json');
        req.setMethod('POST');
        req.setBody(requestBody);
        Http http = new Http();
        HTTPResponse response = http.send(req);

        return response;
    }

    /**
     * Author: Grzegorz Długosz
     * Description: Find and update Design records based on Google Analytics response.
     * @param respWrapper
     *        Deserialized response from Google Analytics
     * @return - List of matched and updated locally designs
     */
    public List<Design__c> matchAndUpdateRecords(ResponseWrapper respWrapper) {
        Map<String, Integer> codeToValueMap = new Map<String, Integer>(); // Code__c to new Views_Month__c value
        List<ResponseRow> rows = respWrapper.reports.get(0).data.rows;

        for (ResponseRow respRow : rows) {
            String designCode = respRow.dimensions.get(0);
            Integer designViewsPerMonth = Integer.valueOf(respRow.metrics.get(0).values.get(0));

            codeToValueMap.put(designCode, designViewsPerMonth);
        }

        List<Design__c> designToUpdateList = new List<Design__c>();
        for (Design__c design : [SELECT Code__c, Views_Month__c FROM Design__c WHERE Code__c IN :codeToValueMap.keySet()]) {
            design.Views_Month__c = codeToValueMap.get(design.Code__c);
            designToUpdateList.add(design);
        }

        return designToUpdateList;
    }

    /**
     * Author: Grzegorz Długosz
     * Description: Generate body of the request.
     * @return String containing body of the request
     */
    private String getRequestBody() {
        /*Date Ranges*/
        List<DateRange> dateRangeList = new List<DateRange>();
        DateRange singleDateRange = new DateRange('30daysAgo', 'today');
        dateRangeList.add(singleDateRange);

        /*Metrics*/
        List<Metric> metricList = new List<Metric>();
        Metric singleMetric = new Metric('ga:totalEvents');
        metricList.add(singleMetric);

        /*Dimensions*/
        List<Dimension> dimensionList = new List<Dimension>();
        Dimension singleDimension = new Dimension('ga:eventLabel');
        dimensionList.add(singleDimension);

        /*Filters*/
        List<Filter> filterList = new List<Filter>();
        Filter eventActionFilter = new Filter('ga:eventAction', 'EXACT', new List<String>{'Prezentacja projektu'});
        Filter eventCategoryFilter = new Filter('ga:eventCategory', 'EXACT', new List<String>{'Projekt'});
        filterList.add(eventActionFilter);
        filterList.add(eventCategoryFilter);

        /*Dimension Filter Clauses*/
        List<DimensionFilterClause> dimensionFilterClauseList = new List<DimensionFilterClause>();
        DimensionFilterClause singleFilterClause = new DimensionFilterClause(filterList);
        dimensionFilterClauseList.add(singleFilterClause);

        /*Order By*/
        List<OrderBy> orderByList = new List<OrderBy>();
        OrderBy singleOrderBy = new OrderBy('ga:totalEvents', 'DESCENDING');
        orderByList.add(singleOrderBy);

        /*Assign parts - fill wrapper*/
        List<ReportRequest> reportRequestList = new List<ReportRequest>();
        ReportRequest singleRepRequest = new ReportRequest(VIEW_ID, dateRangeList, metricList, dimensionList, dimensionFilterClauseList, orderByList, pageToken, PAGE_SIZE);
        reportRequestList.add(singleRepRequest);

        return JSON.serialize(new RequestWrapper(reportRequestList));
    }

    /*************************** REQUEST CLASSES FOR SERIALIZATION ***************************/
    public class RequestWrapper {
        List<ReportRequest> reportRequests;

        public RequestWrapper(List<ReportRequest> reportRequests) {
            this.reportRequests = reportRequests;
        }
    }
    
    public class ReportRequest {
        String viewId;
        List<DateRange> dateRanges;
        List<Metric> metrics;
        List<Dimension> dimensions;
        List<DimensionFilterClause> dimensionFilterClauses;
        List<OrderBy> orderBys;
        String pageToken;
        String pageSize;
        
        public ReportRequest(String viewId, List<DateRange> dateRanges, List<Metric> metrics, List<Dimension> dimensions, List<DimensionFilterClause> dimensionFilterClauses,
                             List<OrderBy> orderBys, String pageToken, String pageSize) {
            this.viewId = viewId;
            this.dateRanges = dateRanges;
            this.metrics = metrics;
            this.dimensions = dimensions;
            this.dimensionFilterClauses = dimensionFilterClauses;
            this.orderBys = orderBys;
            this.pageToken = pageToken;
            this.pageSize = pageSize;
        }
    }
    
    public class OrderBy {
        String fieldName;
        String sortOrder;
        
        public OrderBy(String fieldName, String sortOrder) {
            this.fieldName = fieldName;
            this.sortOrder = sortOrder;
        }
    }
    
    public class DimensionFilterClause {
        List<Filter> filters;

        public DimensionFilterClause(List<Filter> filters) {
            this.filters = filters;
        }
    }
    
    public class Filter {
        String dimensionName;
        String operator;
        List<String> expressions;
        
        public Filter(String dimensionName, String operator, List<String> expressions) {
            this.dimensionName = dimensionName;
            this.operator = operator;
            this.expressions = expressions;
        }
    }
    
    public class Dimension {
        String name;

        public Dimension(String name) {
            this.name = name;
        }
    }
    
    public class Metric {
        String expression;

        public Metric(String expression) {
            this.expression = expression;
        }
    }
    
    public class DateRange {
        String startDate;
        String endDate;

        public DateRange(String startDate, String endDate) {
            this.startDate = startDate;
            this.endDate = endDate;
        }
    }
    /*************************** RESPONSE CLASSES FOR DESERIALIZATION ***************************/
    public class ResponseWrapper {
        List<ResponseReport> reports;
    }

    public class ResponseReport {
        ResponseColumnHeader columnHeader;
        ResponseData data;
        String nextPageToken;
    }

    public class ResponseColumnHeader {
        List<String> dimensions;
        ResponseMetricHeader metricHeader;
    }

    public class ResponseMetricHeader {
        List<ResponseMetricHeaderEntry> metricHeaderEntries;
    }

    public class ResponseMetricHeaderEntry {
        String name;
        String type;
    }

    public class ResponseData {
        List<ResponseRow> rows;
        List<ResponseTotal> totals;
        Integer rowCount;
        List<ResponseMinimum> minimums;
        List<ResponseMaximum> maximums;
    }

    public class ResponseMaximum {
        List<String> values;
    }

    public class ResponseMinimum {
        List<String> values;
    }

    public class ResponseTotal {
        List<String> values;
    }

    public class ResponseRow {
        List<String> dimensions;
        List<ResponseMetric> metrics;
    }

    public class ResponseMetric {
        List<String> values;
    }

}