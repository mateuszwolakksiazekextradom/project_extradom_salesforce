public with sharing class ReductionOrderItemWrapper {
    
    @AuraEnabled
    public String designName {get; set;}
    
    @AuraEnabled
    public ProductWrapper product {get; set;}
    
    @AuraEnabled
    public OrderItem orderItem {get; set;}
    
    @AuraEnabled
    public Integer availableQuantity {get; set;}
    
    public ReductionOrderItemWrapper (Order reductionOrder, OrderItem oi, OrderItem reductionOrderItem) {
        this.product = new ProductWrapper(oi.Product2);
        this.designName = oi.Design__r != null ? oi.Design__r.Name : '';
        Decimal actualQuantity = reductionOrderItem != null ? -reductionOrderItem.Quantity : 0;
        this.availableQuantity = reductionOrder.StatusCode != ConstUtils.ORDER_ACTIVATED_STATUS_CODE ?
            Integer.valueOf(oi.AvailableQuantity) : Integer.valueOf(oi.AvailableQuantity) + Integer.valueOf(actualQuantity);
        this.orderItem = new OrderItem (
            OrderId = reductionOrder.Id, Quantity = reductionOrderItem != null ? actualQuantity : 0,
            OriginalOrderItemId = oi.Id, PricebookEntryId = oi.PricebookEntryId, Product2Id = oi.Product2Id,
            UnitPrice = oi.UnitPrice
        );
        if (reductionOrderItem != null) {
            this.orderItem.Id = reductionOrderItem.Id;
        }
    }
        
    public class ProductWrapper {
        @AuraEnabled
        public String id {get; set;}
        
        @AuraEnabled
        public String name {get; set;}
        
        public ProductWrapper (Product2 product) {
            this.id = product.Id;
            this.name = product.Name;
        }
    }
}