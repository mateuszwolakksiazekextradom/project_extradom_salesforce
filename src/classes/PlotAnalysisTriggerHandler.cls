public without sharing class PlotAnalysisTriggerHandler extends TriggerHandler {
    public override void beforeInsert() {
        List<Plot_Analysis__c> plotAnalysis = (List<Plot_Analysis__c>) Trigger.new;
        createPlots(plotAnalysis);
    }

    private void createPlots(List<Plot_Analysis__c> plotAnalysis) {
        if (UserInfo.getUserRoleId()==null) return;
        UserRole role = [SELECT Id, Name FROM UserRole WHERE Id =: UserInfo.getUserRoleId()];
        if ((role.Name).contains('Sales') || Test.isRunningTest()) {
            List<Plot__c> plots = new List<Plot__c>{};
            List<Plot_Analysis__c> plotAnalysisToAssignPlots = new List<Plot_Analysis__c>{};
            for (Plot_Analysis__c pla : plotAnalysis) {
                if (pla.Plot__c == null) {
                    Plot__c plot = new Plot__c();
                    plot.Name = String.isBlank(pla.Plot_Name__c)?'(do uzupełnienia)':pla.Plot_Name__c;
                    plot.Address__c = pla.Plot_Address__c;
                    plot.Request_Building_Conditions_Analysis__c = true;
                    plot.Request_Building_Conditions_Analys_Date__c = system.now();
                    plots.add(plot);
                    plotAnalysisToAssignPlots.add(pla);
                }
            }
            insert plots;
            for (Integer i = 0; i < plots.size(); i++) {
                plotAnalysisToAssignPlots[i].Plot__c = plots[i].Id;
            }
        }
    }
}