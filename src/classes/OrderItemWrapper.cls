public with sharing class OrderItemWrapper {
    @AuraEnabled
    public Decimal discountPercent {get; set;}
    
    @AuraEnabled
    public Decimal totalPrice {get; set;}
    
    @AuraEnabled
    public Decimal originalListPrice {get; set;}

    @AuraEnabled
    public Decimal minPrice {get; set;}
    
    @AuraEnabled
    public Decimal unitPrice {get; set;}
    
    @AuraEnabled
    public String pricebookEntryId {get; set;}

    @AuraEnabled
    public ProductWrapper product {get; set;}

    @AuraEnabled
    public DesignWrapper design {get; set;}
    
    @AuraEnabled
    public OrderItem orderItem {get; set;}
    
    private static Map<String, String> familyPicklist = Utils.getPicklistValues(Product2.sObjectType, 'Family');
    private static Map<String, Integer> familyPicklistOrder = Utils.getPicklistOrder(Product2.sObjectType, 'Family');


    public OrderItemWrapper (String orderId, PricebookEntry pricebookEntry, Design__c design) {
        this.orderItem = new OrderItem (
            Quantity = 1, UnitPrice = pricebookEntry.UnitPrice, List_Price__c = pricebookEntry.UnitPrice,
            Product2Id = pricebookEntry.Product2.Id, Design__c = design != null ? design.Id : null, PricebookEntryId = pricebookEntry.Id,
            Changes_Agreement__c = '', Version__c = '', Description = '', Index__c = 1, OrderId = orderId, Discount_Internal__c = 0,
            Discount_Sales__c = pricebookEntry.Product2.Family == ConstUtils.PRODUCT_MISC_FAMILY ? pricebookEntry.UnitPrice : 0, Discount_Supplier__c = 0, Discount_Type__c = null
        );
        if (design != null) {
            this.design = new DesignWrapper(design);
        }
        this.product = new ProductWrapper(pricebookEntry.Product2, familyPicklist, familyPicklistOrder);

        this.pricebookEntryId = pricebookEntry.Id;
        this.totalPrice = pricebookEntry.Product2.Family == ConstUtils.PRODUCT_MISC_FAMILY ? 0 : pricebookEntry.UnitPrice;
        this.unitPrice = pricebookEntry.Product2.Family == ConstUtils.PRODUCT_MISC_FAMILY ? 0 : pricebookEntry.UnitPrice;
        this.originalListPrice = pricebookEntry.UnitPrice;
        this.discountPercent = pricebookEntry.Product2.Family == ConstUtils.PRODUCT_MISC_FAMILY ? 100.00 : 0.00;
        this.minPrice = this.product.maxDiscount != null ?
            (pricebookEntry.UnitPrice - this.product.maxDiscount * pricebookEntry.UnitPrice/100).round(System.RoundingMode.HALF_UP) : null;
    }

    public OrderItemWrapper (OrderItem oi) {
        this.orderItem = new OrderItem (
            Quantity = oi.Quantity, UnitPrice = oi.UnitPrice, List_Price__c = oi.List_Price__c,
            Product2Id = oi.Product2.Id, Design__c = oi.Design__c, Changes_Agreement__c = oi.Changes_Agreement__c,
            Version__c = oi.Version__c, Description = oi.Description, Index__c = oi.Index__c, OrderId = oi.OrderId,
            PricebookEntryId = oi.PricebookEntry.Id, Id = oi.Id,
            Discount_Internal__c = oi.Discount_Internal__c != null ? oi.Discount_Internal__c : 0,
            Discount_Sales__c = oi.Discount_Sales__c != null ? oi.Discount_Sales__c : 0,
            Discount_Supplier__c = oi.Discount_Supplier__c != null ? oi.Discount_Supplier__c : 0,
            Discount_Type__c = oi.Discount_Type__c
        );
        if (oi.Design__r != null) {
            this.design = new DesignWrapper(oi.Design__r);
        }
        this.product = new ProductWrapper(oi.Product2, familyPicklist, familyPicklistOrder);
        this.pricebookEntryId = oi.PricebookEntry.Id;
        this.totalPrice = oi.TotalPrice;
        this.unitPrice = oi.List_Price__c - this.orderItem.Discount_Sales__c;
        this.originalListPrice = oi.PricebookEntry.UnitPrice;
        this.minPrice = this.product.maxDiscount != null && oi.List_Price__c != null ?
                (oi.List_Price__c - this.product.maxDiscount * oi.List_Price__c/100).round(System.RoundingMode.HALF_UP) : null;
        this.discountPercent = oi.List_Price__c != null && oi.List_Price__c > 0 ?
            (100*this.orderItem.Discount_Sales__c/oi.List_Price__c).setScale(2, System.RoundingMode.HALF_UP) : 0;
    }

    public class DesignWrapper {
        @AuraEnabled
        public String id {get; set;}

        @AuraEnabled
        public String name {get; set;}

        @AuraEnabled
        public String thumbnail {get; set;}

        @AuraEnabled
        public String code {get; set;}

        @AuraEnabled
        public String type {get; set;}

        @AuraEnabled
        public Decimal usableArea {get; set;}

        public DesignWrapper (Design__c design) {
            this.name = design.Name;
            this.id = design.Id;
            this.thumbnail = design.Thumbnail_Full_URL__c;
            this.code = design.Code__c;
            this.usableArea = design.Usable_Area__c;
            this.type = design.Type__c;
        }
    }

    public class ProductWrapper {
        @AuraEnabled
        public String id {get; set;}

        @AuraEnabled
        public String name {get; set;}

        @AuraEnabled
        public String type {get; set;}

        @AuraEnabled
        public String typeValue {get; set;}

        @AuraEnabled
        public Integer typeOrder {get; set;}

        @AuraEnabled
        public Boolean versionRequired {get; set;}

        @AuraEnabled
        public Boolean lineDescriptionRequired {get; set;}

        @AuraEnabled
        public Boolean lineEditable {get; set;}

        @AuraEnabled
        public Boolean changesAgreementRequired {get; set;}

        @AuraEnabled
        public Integer maxDiscount {get; set;}

        public ProductWrapper (Product2 product, Map<String, String> familyPicklist, Map<String, Integer> familyPicklistOrder) {
            this.id = product.Id;
            this.name = product.Name;
            this.type = familyPicklist.get(product.Family);
            this.typeValue = product.Family;
            this.typeOrder = familyPicklistOrder.get(product.Family);
            this.versionRequired = product.Version_Required__c;
            this.lineDescriptionRequired = product.Line_Description_Required__c;
            this.changesAgreementRequired = product.Family == ConstUtils.PRODUCT_DESIGN_FAMILY;
            this.lineEditable = (product.Family != ConstUtils.PRODUCT_MISC_FAMILY && product.Family != ConstUtils.PRODUCT_GIFT_FAMILY && product.Family != ConstUtils.PRODUCT_EXTRA_FAMILY);
            this.maxDiscount = product.Max_Discount__c != null ? Integer.valueOf(product.Max_Discount__c) : null;
        }
    }
}