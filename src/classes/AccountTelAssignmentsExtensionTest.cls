@isTest
private class AccountTelAssignmentsExtensionTest {

    static testMethod void myUnitTest() {
    
    List<RecordType> locRecordTypes = [Select Id, Name From RecordType where sObjectType='Location__c' and isActive=true];
    Map<String, ID> locRecordTypeNames = new Map<String, ID>{};
    for(RecordType rt: locRecordTypes) {
        locRecordTypeNames.put(rt.Name,rt.Id);
    }
    
    Location__c loc_country = new Location__c(Name='Polska', RecordTypeId=locRecordTypeNames.get('Country'));
    insert loc_country;
    Location__c loc_state = new Location__c(Name='opolskie', RecordTypeId=locRecordTypeNames.get('State'), Location__latitude__s=50.6131, Location__longitude__s=17.92915);
    insert loc_state;
    Location__c loc_region = new Location__c(Name='nyski', RecordTypeId=locRecordTypeNames.get('Region'), Location__latitude__s=50.44138, Location__longitude__s=17.29832);
    insert loc_region;
    Location__c loc_city = new Location__c(Name='Nysa', RecordTypeId=locRecordTypeNames.get('City'), Location__latitude__s=50.471636, Location__longitude__s=17.334111);
    insert loc_city;
    
    Account a = new Account(Name='Jan Kowalski', Investment_Location__c=loc_city.Id);
    insert a;
    
    ApexPages.StandardController sc = new ApexPages.StandardController(a);
    AccountTelemarketingAssignmentsExtension ext = new AccountTelemarketingAssignmentsExtension(sc);
    
    ext.code = 'Danfoss';
    ext.getTelAssignment();
    ext.createTelemarketingLostEntry();
    ext.createTelemarketingCanceledEntry();
    ext.getTelemarketingLostReasonOptions();
    ext.createTelemarketingLostEntryOffer();
    ext.createTelemarketingDeferredEntry();
    }
    
}