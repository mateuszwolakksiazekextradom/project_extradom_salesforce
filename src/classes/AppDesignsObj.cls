@RestResource(urlMapping='/app/designs/*')
global without sharing class AppDesignsObj {
    @HttpGet
    global static ExtradomApi.Design doGet() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Pattern pt = Pattern.compile('\\/app\\/designs\\/(.{18})');
        Matcher mt = pt.matcher(req.requestURI);
        mt.find();
        ID designId = mt.group(1);
        Design__c d = [SELECT Id, Code__c, Full_Name__c, Base_Design__c, Canonical_Full_URL__c, Thumbnail_Base_URL__c, Usable_Area__c, Height__c, Count_Rooms__c, Footprint__c, Livestock_Unit__c, Capacity__c, Roof_Cover_Area__c, EUco__c, Carport__c, Attic__c, Level__c, Garage__c, Minimum_Plot_Size_Horizontal__c, Construction__c, Ridge__c, Living_Type__c, Basement__c, Shape__c, Garage_Location__c, South_Entrance__c, Is_Passive__c, Roof_slope__c, Border_Ready__c, Type__c, Gross_Price__c, Current_Price__c, Sketchfab_ID__c, KW_Model__c, Promo_Price_End_Date__c, Use_Custom_Description__c, Custom_Description__c, Automated_Description__c, Description__c, Technology__c, Additional_Info__c, Ceiling__c, Roof__c, Construction_Pricing_Estimate_Stage_0__c, Construction_Pricing_Estimate_Stage_1__c, Construction_Pricing_Estimate_Stage_2__c, Construction_Pricing_Estimate__c, Rating_Count__c, Rating_Avg__c, Stage__c, Price_per_Segment__c, (SELECT Category__c, Subcategory__c, Name, Name__c, Base_URL__c, Sum_Usable__c, Sum_Total__c, R1__c, V1__c, U1__c, R2__c, V2__c, U2__c, R3__c, V3__c, U3__c, R4__c, V4__c, U4__c, R5__c, V5__c, U5__c, R6__c, V6__c, U6__c, R7__c, V7__c, U7__c, R8__c, V8__c, U8__c, R9__c, V9__c, U9__c, R10__c, V10__c, U10__c, R11__c, V11__c, U11__c, R12__c, V12__c, U12__c, R13__c, V13__c, U13__c, R14__c, V14__c, U14__c, R15__c, V15__c, U15__c, R16__c, V16__c, U16__c, R17__c, V17__c, U17__c, R18__c, V18__c, U18__c, R19__c, V19__c, U19__c, R20__c, V20__c, U20__c, R21__c, V21__c, U21__c, R22__c, V22__c, U22__c, R23__c, V23__c, U23__c, R24__c, V24__c, U24__c, R25__c, V25__c, U25__c, R26__c, V26__c, U26__c, R27__c, V27__c, U27__c, R28__c, V28__c, U28__c, R29__c, V29__c, U29__c, R30__c, V30__c, U30__c, R31__c, V31__c, U31__c, R32__c, V32__c, U32__c, R33__c, V33__c, U33__c, R34__c, V34__c, U34__c, R35__c, V35__c, U35__c, R36__c, V36__c, U36__c, R37__c, V37__c, U37__c, R38__c, V38__c, U38__c, R39__c, V39__c, U39__c, R40__c, V40__c, U40__c, R41__c, V41__c, U41__c, R42__c, V42__c, U42__c, R43__c, V43__c, U43__c, R44__c, V44__c, U44__c, R45__c, V45__c, U45__c, R46__c, V46__c, U46__c, R47__c, V47__c, U47__c, R48__c, V48__c, U48__c, R49__c, V49__c, U49__c, R50__c, V50__c, U50__c FROM Media__r WHERE Category__c IN ('Outdoor', 'Indoor', 'Building Placement', 'Section AA', 'Front-side Elevation', 'Back-side Elevation', 'Left-side Elevation', 'Right-side Elevation', '1st Floor Plan', '2nd Attic Plan', '2nd Floor Plan', '3rd Floor Plan', 'Attic Plan', 'Basement Plan', 'Garage Plan', 'Ground Floor Plan', 'Mezzanine Plan', 'Roof Plan', '3D Rotation') ORDER BY Lp__c), (SELECT Rating__c, CreatedBy.CommunityNickname, CreatedBy.Id, CreatedBy.SmallPhotoUrl, Builder__c, CreatedDate, Body__c FROM Reviews__r WHERE Moderated__c=true ORDER BY CreatedDate DESC), Opinion_1__c, Opinion_2__c, Opinion_3__c, Opinion_4__c, Opinion_5__c, Opinion_6__c, Opinion_1_Author_Id__c, Opinion_2_Author_Id__c, Opinion_3_Author_Id__c, Opinion_4_Author_Id__c, Opinion_5_Author_Id__c, Opinion_6_Author_Id__c, Feed_Gallery_Featured__c FROM Design__c WHERE Id =: designId];
        List<Design__c> familyDesigns;
        if (String.isNotBlank(d.Base_Design__c)) {
            familyDesigns = [SELECT Id, Code__c, Full_Name__c, Type__c, Usable_Area__c, Thumbnail_Base_URL__c, Sketchfab_ID__c, KW_Model__c, Gross_Price__c, Current_Price__c FROM Design__c WHERE ((Base_Design__c = :d.Base_Design__c AND Id != :d.Id) OR Id =: d.Base_Design__c) AND Status__c = 'Active' ORDER BY Order_Score__c DESC];
        } else {
            familyDesigns = [SELECT Id, Code__c, Full_Name__c, Type__c, Usable_Area__c, Thumbnail_Base_URL__c, Sketchfab_ID__c, KW_Model__c, Gross_Price__c, Current_Price__c FROM Design__c WHERE Base_Design__c = :d.Id AND Status__c = 'Active' ORDER BY Order_Score__c DESC];
        }
        List<Product2> addOnProducts = [select id, name, productcode, description, (SELECT id, unitprice, regular_price__c FROM PricebookEntries where isactive = true) from product2 where design__c = :d.Id and family='Add-On' and isactive = true and productcode != ''];
        Decimal usable_area = 120;
        Decimal height = 8;
        Decimal roof_slope = 40;
        Decimal count_rooms = 0;
        Decimal attic = 1;
        Decimal level = 1;
        Decimal garage = 0;
        if (d.Usable_Area__c != null) usable_area = d.Usable_Area__c;
        if (d.Height__c != null) height = d.Height__c;
        if (d.Roof_slope__c != null) roof_slope = d.Roof_slope__c;
        if (d.Count_Rooms__c != null) count_rooms = d.Count_Rooms__c;
        if (d.Attic__c != null) attic = d.Attic__c;
        if (d.Level__c != null) level = d.Level__c;
        if (d.Garage__c != null) garage = d.Garage__c;
        List<Design__c> similiarDesigns = [SELECT Id, Code__c, Full_Name__c, Type__c, Usable_Area__c, Thumbnail_Base_URL__c, Sketchfab_ID__c, KW_Model__c, Gross_Price__c, Current_Price__c FROM Design__c WHERE Border_Ready__c = :d.Border_Ready__c AND Type__c = :d.Type__c AND Usable_Area__c >= :usable_area*0.9 AND Usable_Area__c <= :usable_area*1.1 AND Height__c >= :height*0.9 AND Height__c <= :height*1.1 AND Roof_slope__c >= :roof_slope-5 AND Roof_slope__c <= :roof_slope+5 AND Count_Rooms__c >= :count_rooms AND Attic__c = :attic AND Level__c = :level AND Construction__c = :d.Construction__c AND Garage__c = :garage AND Roof__c = :d.Roof__c AND Ridge__c = :d.Ridge__c AND Living_Type__c = :d.Living_Type__c AND Basement__c = :d.Basement__c AND Shape__c = :d.Shape__c AND Garage_Location__c = :d.Garage_Location__c AND South_Entrance__c = :d.South_Entrance__c AND Is_Passive__c = :d.Is_Passive__c AND Id != :d.Id AND Status__c = 'Active' AND Id NOT IN :familyDesigns ORDER BY Order_Score__c DESC NULLS LAST LIMIT 6];
        ID subId = null;
        if (UserInfo.getUserType() != 'Guest') {
            for (EntitySubscription sub : [SELECT Id FROM EntitySubscription WHERE ParentId =: d.Id AND NetworkId =: Network.getNetworkId() AND SubscriberId =: UserInfo.getUserId()]) {
                subId = sub.Id;
                break;
            }
        }
        ExtradomApi.Design design = new ExtradomApi.Design(d, familyDesigns, similiarDesigns, addOnProducts, subId);
        if (UserInfo.getUserType()=='Guest') {
            RestContext.response.addHeader('Cache-Control', 'must-revalidate,max-age=0,s-maxage=3600');
        } else {
            RestContext.response.addHeader('Cache-Control', 'no-cache,must-revalidate,max-age=0,no-store,private,s-maxage=0');
        }
        return design;
    }
}