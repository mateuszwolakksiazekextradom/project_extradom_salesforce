@IsTest
private class ProductSelectionController_Test {

    @IsTest
    private static void shouldReturnProductByPricebookAndDesign () {
        // given
        Pricebook2 pb = [SELECT Id FROM Pricebook2 WHERE Name = 'Test Pricebook 1'];
        Design__c design = [SELECT Id FROM Design__c WHERE Name = 'First Design'];

        // when
        List<PriceBookEntry> pricebookEntries = ProductSelectionController.getDesignProducts(pb.Id, design.Id);

        // then
        System.assertEquals(1, pricebookEntries.size());
        System.assertEquals('Test Product 2 Pricebook 1', pricebookEntries.get(0).Product2.Name);
    }

    @IsTest
    private static void shouldReturnProductByPricebookAndFamily () {
        // given
        Pricebook2 pb = [SELECT Id FROM Pricebook2 WHERE Name = 'Test Pricebook 1'];

        // when
        List<PriceBookEntry> pricebookEntries = ProductSelectionController.getFamilyProducts(pb.Id, ConstUtils.PRODUCT_GIFT_FAMILY);

        // then
        System.assertEquals(1, pricebookEntries.size());
        System.assertEquals('Test Product 1 Pricebook 1', pricebookEntries.get(0).Product2.Name);
    }

    @IsTest
    private static void shouldReturnProductByPricebookAndFamilyAndDesign () {
        // given
        Pricebook2 pb = [SELECT Id FROM Pricebook2 WHERE Name = 'Test Pricebook 2'];
        Design__c design = [SELECT Id FROM Design__c WHERE Name = 'Second Design'];

        // when
        List<PriceBookEntry> pricebookEntries = ProductSelectionController.getAdditionalProducts(pb.Id, design.Id, ConstUtils.PRODUCT_GIFT_FAMILY);

        // then
        System.assertEquals(1, pricebookEntries.size());
        System.assertEquals('Test Product 3 Pricebook 2', pricebookEntries.get(0).Product2.Name);
    }

    @TestSetup
    private static void setupData() {
        Supplier__c newSupplier = new Supplier__c(Name = 'Supplier', Code__c = '11', Full_Name__c = 'Great Supplier');
        insert newSupplier;

        Design__c design1 = new Design__c (Name = 'First Design', Supplier__c = newSupplier.Id, Full_Name__c = 'First Design');
        Design__c design2 = new Design__c (Name = 'Second Design', Supplier__c = newSupplier.Id, Full_Name__c = 'Second Design');
        insert new List<Design__c> {design1, design2};

        Pricebook2 pricebook1 = new Pricebook2(Name = 'Test Pricebook 1', IsActive = true);
        Pricebook2 pricebook2 = new Pricebook2(Name = 'Test Pricebook 2', IsActive = true);
        insert new List<Pricebook2> {pricebook1, pricebook2};

        Product2 giftProductWithPricebook1 = new Product2(
                Name = 'Test Product 1 Pricebook 1', Family = ConstUtils.PRODUCT_GIFT_FAMILY, IsActive = true, Design__c = design2.Id
        );
        Product2 designProductWithPricebook1 = new Product2(
                Name = 'Test Product 2 Pricebook 1', Family = ConstUtils.PRODUCT_DESIGN_FAMILY, IsActive = true, Design__c = design1.Id
        );
        Product2 giftProductWithPricebook2 = new Product2(
                Name = 'Test Product 3 Pricebook 2', Family = ConstUtils.PRODUCT_GIFT_FAMILY, IsActive = true, Design__c = design2.Id
        );
        Product2 designProductWithPricebook2 = new Product2(
                Name = 'Test Product 4 Pricebook 2', Family = ConstUtils.PRODUCT_DESIGN_FAMILY, IsActive = true, Design__c = design1.Id
        );
        insert new List<Product2> {giftProductWithPricebook1, designProductWithPricebook1, giftProductWithPricebook2, designProductWithPricebook2};

        PricebookEntry pe1 = new PricebookEntry(Product2Id = giftProductWithPricebook1.Id, IsActive = true, Pricebook2Id = pricebook1.Id, UnitPrice = 1);
        PricebookEntry spe1 = new PricebookEntry(
                Product2Id = giftProductWithPricebook1.Id, IsActive = true, Pricebook2Id = Test.getStandardPricebookId(), UnitPrice = 1
        );
        PricebookEntry pe2 = new PricebookEntry(Product2Id = designProductWithPricebook1.Id, IsActive = true, Pricebook2Id = pricebook1.Id, UnitPrice = 1);
        PricebookEntry spe2 = new PricebookEntry(
                Product2Id = designProductWithPricebook1.Id, IsActive = true, Pricebook2Id = Test.getStandardPricebookId(), UnitPrice = 1)
        ;

        PricebookEntry pe3 = new PricebookEntry(Product2Id = giftProductWithPricebook2.Id, IsActive = true, Pricebook2Id = pricebook2.Id, UnitPrice = 1);
        PricebookEntry spe3 = new PricebookEntry(
                Product2Id = giftProductWithPricebook2.Id, IsActive = true, Pricebook2Id = Test.getStandardPricebookId(), UnitPrice = 1
        );
        PricebookEntry pe4 = new PricebookEntry(Product2Id = designProductWithPricebook2.Id, IsActive = true, Pricebook2Id = pricebook2.Id, UnitPrice = 1);
        PricebookEntry spe4 = new PricebookEntry(
                Product2Id = designProductWithPricebook2.Id, IsActive = true, Pricebook2Id = Test.getStandardPricebookId(), UnitPrice = 1
        );
        insert new List<PricebookEntry> {spe1, spe2, spe3, spe4};
        insert new List<PricebookEntry> {pe1, pe2, pe3, pe4};
    }
}