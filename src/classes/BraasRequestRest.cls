@RestResource(urlMapping='/braasrequest')
global class BraasRequestRest {
    @HttpPost
    global static Map<String,String> doPost(String firstName, String lastName, String email, String phone, Boolean optInRegulations, Boolean optInPrivacyPolicy, Boolean optInMarketing, Boolean optInMarketingExtended, Boolean optInMarketingPartner, Boolean optInLeadForward, Boolean optInEmail, Boolean optInPhone, Boolean optInProfiling, Boolean optInPKO) {
        Restcontext.response.headers.put('Access-Control-Allow-Origin', '*');
        try {
            Map<String,Id> data = OrderMethods.getWebLeadContactDetails('Telemarketing', firstName, lastName, email, phone, 'Internal', false, false, 'No');
            Task t = new Task(subject='BRAAS', Type='Telemarketing: Request', WhoId=data.get('contactPhoneId'), WhatId=data.get('accountId'), ActivityDate=System.Today());
            t.Description = firstName + ' ' + lastName + ', ' + phone + ' (' + email + ')';
            List<Group> telReqGroups = [Select (Select UserOrGroupId From GroupMembers) From Group g WHERE g.Name = 'Telemarketing: Requests'];
            for (Group g : telReqGroups) {
                for (GroupMember m: g.GroupMembers) {
                    t.OwnerId = m.UserOrGroupId;
                }
            }
            insert t;
            OrderMethods.updateOptIns(email, phone, optInRegulations, optInPrivacyPolicy, optInMarketing, optInMarketingExtended, optInMarketingPartner, optInLeadForward, optInEmail, optInPhone, optInProfiling, optInPKO);
            if(Test.isRunningTest()) {
                throw new MyException();
            }
        } catch (Exception e) {
            RestResponse res = RestContext.response;
            res.statusCode = 400;
            Map<String,String> result = new Map<String,String>{'result' => 'Nieprawidłowe zapytanie'};
            return result;
        }
        Map<String,String> result = new Map<String,String>{'result' => 'Dziękujemy za zainteresowanie. Skontaktujemy się z Tobą telefonicznie niezwłocznie, aby przygotować ofertę skrojoną na Twój dach.'};
        return result;
    }
    public class MyException extends Exception {}
}