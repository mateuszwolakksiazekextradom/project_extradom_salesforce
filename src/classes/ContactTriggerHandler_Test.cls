@IsTest
private class ContactTriggerHandler_Test {

    @IsTest
    private static void shouldSetMarketingOptIn_whenOptedInContactInserted() {
        // given
        Account a = new Account(Name = 'TestName1');
        insert a;
        Contact contact = new Contact(LastName = 'Test Name Contact', AccountId = a.Id, Email = 'test@test.com');
        insert contact;
        a = [SELECT Id, Has_Opt_In_Marketing__c FROM Account WHERE Id = :a.Id];
        Boolean optInBefore = a.Has_Opt_In_Marketing__c;

        // when
        Contact contactOptIn = new Contact(LastName = 'Test Name Contact 2', AccountId = a.Id, Email = 'test2@test.com', Opt_In_Marketing__c = true);
        insert contactOptIn;
        a = [SELECT Id, Has_Opt_In_Marketing__c FROM Account WHERE Id = :a.Id];
        Boolean optInAfter = a.Has_Opt_In_Marketing__c;

        System.assertEquals(false, optInBefore);
        System.assertEquals(true, optInAfter);
    }

    @IsTest
    private static void shouldSetMarketingOptIn_whenOptedInContactUpdate() {
        // given
        Account a = new Account(Name = 'TestName1');
        insert a;
        Contact contact = new Contact(LastName = 'Test Name Contact', AccountId = a.Id, Email = 'test@test.com');
        insert contact;
        a = [SELECT Id, Has_Opt_In_Marketing__c FROM Account WHERE Id = :a.Id];
        Boolean optInBefore = a.Has_Opt_In_Marketing__c;

        // when
        contact.Opt_In_Marketing__c = true;
        update contact;
        a = [SELECT Id, Has_Opt_In_Marketing__c FROM Account WHERE Id = :a.Id];
        Boolean optInAfter = a.Has_Opt_In_Marketing__c;

        System.assertEquals(false, optInBefore);
        System.assertEquals(true, optInAfter);
    }

    @IsTest
    private static void shouldSetMarketingOptIn_whenOptedInContactDeleted() {
        // given
        Account a = new Account(Name = 'TestName1');
        insert a;
        Contact contact = new Contact(LastName = 'Test Name Contact', AccountId = a.Id, Email = 'test@test.com', Opt_In_Marketing__c = true);
        insert contact;
        a = [SELECT Id, Has_Opt_In_Marketing__c FROM Account WHERE Id = :a.Id];
        Boolean optInBefore = a.Has_Opt_In_Marketing__c;

        // when
        delete contact;
        a = [SELECT Id, Has_Opt_In_Marketing__c FROM Account WHERE Id = :a.Id];
        Boolean optInAfter = a.Has_Opt_In_Marketing__c;

        System.assertEquals(true, optInBefore);
        System.assertEquals(false, optInAfter);
    }

    @IsTest
    private static void shouldSetMarketingOptIn_whenOptedInContactChangesAccount() {
        // given
        Account a1 = new Account(Name = 'TestName1');
        Account a2 = new Account(Name = 'TestName2');
        insert new List<Account> {a1, a2};
        Contact contact = new Contact(LastName = 'Test Name Contact', AccountId = a1.Id, Email = 'test@test.com', Opt_In_Marketing__c = true);
        insert contact;
        Map<Id, Account>  accounts = new Map<Id, Account> ([SELECT Id, Has_Opt_In_Marketing__c FROM Account WHERE Id = :a1.Id OR Id = :a2.Id]);
        Boolean a1OptInBefore = accounts.get(a1.Id).Has_Opt_In_Marketing__c;
        Boolean a2OptInBefore = accounts.get(a2.Id).Has_Opt_In_Marketing__c;

        // when
        contact.AccountId = a2.Id;
        update contact;
        accounts = new Map<Id, Account> ([SELECT Id, Has_Opt_In_Marketing__c FROM Account WHERE Id = :a1.Id OR Id = :a2.Id]);
        Boolean a1OptInAfter = accounts.get(a1.Id).Has_Opt_In_Marketing__c;
        Boolean a2OptInAfter = accounts.get(a2.Id).Has_Opt_In_Marketing__c;

        System.assertEquals(true, a1OptInBefore);
        System.assertEquals(false, a1OptInAfter);
        System.assertEquals(false, a2OptInBefore);
        System.assertEquals(true, a2OptInAfter);
    }
}