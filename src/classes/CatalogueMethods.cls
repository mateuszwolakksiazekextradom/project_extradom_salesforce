global class CatalogueMethods {

		global static String getCataloguePagesXML() {
    	String xml = '<?xml version="1.0"?>';
    	xml += '\r\n' + '<Root xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">';
    	List<Design__c> designs = [SELECT code__c FROM Design__c WHERE Catalogue_Ready__c = true ORDER BY usable_area__c];
		
		/*
		
		List<Catalogue_Settings__c> catalogueSettings = [SELECT dummy_pages__c, x2_pages_designs__c FROM Catalogue_Settings__c WHERE name = 'EXD 02/2013'];
		Catalogue_Settings__c cs = catalogueSettings.get(0);
		
		Integer currentDesign = 0;
		for (Integer page=0; page<=600; page++) {
			if (page == 0) {
				xml += '\r\n' + '<Projekt />';
			} else if (cs.dummy_pages__c.contains(','+page+',')) {
				xml += '\r\n' + '<Projekt>';
				xml += '\r\n' + '<PDF href=\"file://dtp/PRESS 2011_02_02/reklamy/reklama_dummy.pdf\" />';
				xml += '\r\n' + '<Numeracja>' + page + '</Numeracja>';
				xml += '\r\n' + '</Projekt>';
		    } else {
		        if (designs.size() > 0) {
		        	Design__c d = designs.remove(0);
		            xml += '\r\n' + '<Projekt>';
					xml += '\r\n' + '<PDF href=\"file://dtp/PRESS 2011_02_02/' + d.Code__c + '/' + d.Code__c + ((Math.mod(page,2)==1)?'_P':'_L') + '.pdf\" />';
					xml += '\r\n' + '<Numeracja>' + page + '</Numeracja>';
					xml += '\r\n' + '</Projekt>';
		            if (cs.x2_pages_designs__c.contains(d.Code__c)) {
		                page++;
		                xml += '\r\n' + '<Projekt>';
						xml += '\r\n' + '<PDF href=\"file://dtp/PRESS 2011_02_02/' + d.Code__c + '/' + d.Code__c + '_wnetrze' + '.pdf\" />';
						xml += '\r\n' + '<Numeracja>' + page + '</Numeracja>';
						xml += '\r\n' + '</Projekt>';
		            }
		        } else {
		            break;
		        }
		    }
		}
		
		*/
		
		String dummyPages = ',,';
		String twoSidePages = ',,';
		
		try {
			dummyPages = ApexPages.currentPage().getParameters().get('dummyPages');
			twoSidePages = ApexPages.currentPage().getParameters().get('twoSidePages');
		} catch (Exception e) {
		}
		
		Integer currentDesign = 0;
		for (Integer page=0; page<=1000; page++) {
			if (page == 0) {
				xml += '\r\n' + '<Projekt />';
			} else if (dummyPages.contains(','+page+',')) {
				xml += '\r\n' + '<Projekt>';
				xml += '\r\n' + '<PDF href=\"file://dtp/PRESS 2011_02_02/reklamy/reklama_dummy.pdf\" />';
				xml += '\r\n' + '<Numeracja>' + page + '</Numeracja>';
				xml += '\r\n' + '</Projekt>';
		    } else {
		        if (designs.size() > 0) {
		        	Design__c d = designs.remove(0);
		            xml += '\r\n' + '<Projekt>';
					xml += '\r\n' + '<PDF href=\"file://dtp/PRESS 2011_02_02/' + d.Code__c + '/' + d.Code__c + ((Math.mod(page,2)==1)?'_P':'_L') + '.pdf\" />';
					xml += '\r\n' + '<Numeracja>' + page + '</Numeracja>';
					xml += '\r\n' + '</Projekt>';
		            if (twoSidePages.contains(d.Code__c)) {
		                page++;
		                xml += '\r\n' + '<Projekt>';
						xml += '\r\n' + '<PDF href=\"file://dtp/PRESS 2011_02_02/' + d.Code__c + '/' + d.Code__c + '_wnetrze' + '.pdf\" />';
						xml += '\r\n' + '<Numeracja>' + page + '</Numeracja>';
						xml += '\r\n' + '</Projekt>';
		            }
		        } else {
		            break;
		        }
		    }
		}
		
		xml += '\r\n' + '</Root>';
		return xml;
    }
    
	global static String getCataloguePagesCSV() {
    	String csv = '';
    	List<Design__c> designs = [SELECT Full_Name__c, Code__c, Usable_Area__c FROM Design__c WHERE Catalogue_Ready__c = true ORDER BY usable_area__c];
		
		/*
		
		List<Catalogue_Settings__c> catalogueSettings = [SELECT dummy_pages__c, x2_pages_designs__c FROM Catalogue_Settings__c WHERE name = 'EXD 02/2013'];
		Catalogue_Settings__c cs = catalogueSettings.get(0);
		
		Integer currentDesign = 0;
		for (Integer page=0; page<=600; page++) {
			if (page == 0) {
				xml += '\r\n' + '<Projekt />';
			} else if (cs.dummy_pages__c.contains(','+page+',')) {
				xml += '\r\n' + '<Projekt>';
				xml += '\r\n' + '<PDF href=\"file://dtp/PRESS 2011_02_02/reklamy/reklama_dummy.pdf\" />';
				xml += '\r\n' + '<Numeracja>' + page + '</Numeracja>';
				xml += '\r\n' + '</Projekt>';
		    } else {
		        if (designs.size() > 0) {
		        	Design__c d = designs.remove(0);
		            xml += '\r\n' + '<Projekt>';
					xml += '\r\n' + '<PDF href=\"file://dtp/PRESS 2011_02_02/' + d.Code__c + '/' + d.Code__c + ((Math.mod(page,2)==1)?'_P':'_L') + '.pdf\" />';
					xml += '\r\n' + '<Numeracja>' + page + '</Numeracja>';
					xml += '\r\n' + '</Projekt>';
		            if (cs.x2_pages_designs__c.contains(d.Code__c)) {
		                page++;
		                xml += '\r\n' + '<Projekt>';
						xml += '\r\n' + '<PDF href=\"file://dtp/PRESS 2011_02_02/' + d.Code__c + '/' + d.Code__c + '_wnetrze' + '.pdf\" />';
						xml += '\r\n' + '<Numeracja>' + page + '</Numeracja>';
						xml += '\r\n' + '</Projekt>';
		            }
		        } else {
		            break;
		        }
		    }
		}
		
		*/
		
		String dummyPages = '';
		String twoSidePages = '';
		
		try {
			dummyPages = ApexPages.currentPage().getParameters().get('dummyPages');
			twoSidePages = ApexPages.currentPage().getParameters().get('twoSidePages');
		} catch (Exception e) {
			System.debug('Error');
		} finally {
			if (dummyPages==null) {
				dummyPages = '';
				twoSidePages = '';
			}
		}
		
		Integer currentDesign = 0;
		for (Integer page=0; page<=1000; page++) {
			if (page == 0) {
			} else if (dummyPages.contains(','+page+',')) {
		    } else {
		        if (designs.size() > 0) {
		        	Design__c d = designs.remove(0);
		            csv += d.Full_Name__c + '\t' + d.Code__c + '\t' + d.Usable_Area__c + '\t' + page + '\r\n';
		            if (twoSidePages.contains(d.Code__c)) {
		                page++;
		            }
		        } else {
		            break;
		        }
		    }
		}
		
		return csv;
    }

}