public class TsvProducts {
    public TsvProducts () {
    }
    
    public void getResultFile() {
        TsvProducts.saveResultFile();
    }
    
    @Future
    public static void saveResultFile() {
        List<Document> documents = [SELECT Id FROM Document WHERE Name = 'Text.csv'];
        String s = 'id\ttitle\tlink\timage link\tcondition\tavailability\tproduct type\tprice\tsale price\tsale price effective date\tcustom_label_0\tcustom_label_1\tcustom_label_2\tcustom_label_3\tcustom_label_4\n';
        for (List<Design__c> designs : [SELECT Code__c, Full_Name__c, Canonical_Full_URL__c, Thumbnail_Full_URL__c, Product_Type__c, Gross_Price_ISO_4217__c, Promo_Price_ISO_4217__c, Promo_Price_End_Date_ISO_4217__c, Supplier_Code__c, Usable_Area_Segment__c, Show_Estimates__c, Show_Sketches__c, Merchant_Priority__c FROM Design__c WHERE Status__c = 'Active' AND Gross_Price__c > 0]) {
            for (Design__c d : designs) {
                s += d.Code__c + '\t' + d.Full_Name__c.normalizeSpace() + '\t' + d.Canonical_Full_URL__c + '\t' + d.Thumbnail_Full_URL__c + '\tnew\tin stock\t' + d.Product_Type__c + '\t' + d.Gross_Price_ISO_4217__c + '\t' + (d.Promo_Price_ISO_4217__c!=null?d.Promo_Price_ISO_4217__c:'') + '\t' + (d.Promo_Price_End_Date_ISO_4217__c!=null?d.Promo_Price_End_Date_ISO_4217__c:'') + '\t' + d.Supplier_Code__c + '\t' + (d.Usable_Area_Segment__c!=null?d.Usable_Area_Segment__c:'') + '\t' + d.Show_Estimates__c + '\t' + d.Show_Sketches__c + '\t' + (d.Merchant_Priority__c!=null?d.Merchant_Priority__c:'') + '\n';
            }
        }
        if (documents.size() > 0) {
            Document d = new Document (Id = documents.get(0).Id, Body = Blob.valueOf(s));
            update d;
        }
    }
    @TestVisible
    private void getResultFileSup() {
        String s = 'id\tdescription\n';
        for (List<Design__c> designs : [SELECT Code__c, Description__c FROM Design__c WHERE Status__c = 'Active' AND Gross_Price__c > 0]) {
            for (Design__c d: designs) {
	            s += d.Code__c + '\t' + (d.Description__c!=null?d.Description__c.normalizeSpace():'') + '\n';
            }
        }
    }
}