/**
 * Created by mateusz.wolak on 10.06.2019.
 */

@isTest
global class AiquaIntegration_MockImpl implements HttpCalloutMock {

    global HttpResponse respond(HttpRequest req) {

        System.assertNotEquals(null, req.getEndpoint());
        System.assertEquals('POST', req.getMethod());

        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setStatusCode(200);
        return res;

    }

}