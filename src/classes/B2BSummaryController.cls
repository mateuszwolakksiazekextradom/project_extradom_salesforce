public without sharing class B2BSummaryController {
    
    public B2BSummaryController() {}
    
    private static TV__c tv = TV__c.getInstance();
    public String toExplain {get {return tv.To_Explain__c;}}
    public String bearings {get {return tv.Bearings__c;}}
    public String message {get {return tv.Message__c;}}
    public Decimal orderObjective {get {return tv.Order_Objective__c;}}
    
    public Decimal getInvestorOrders() {
      List<AggregateResult> results = [SELECT Sum(AvailableQuantity) ile FROM OrderItem WHERE Order.PoDate = THIS_MONTH AND Order.StatusCode = 'Activated' AND Product2.Family = 'Plan Set' AND AvailableQuantity > 0 AND Order.Account.Type = 'Investor'];
      return (Decimal)(results[0].get('ile'));

    }
    
    public Decimal getDealerOrders() {
        List<AggregateResult> results = [SELECT Sum(AvailableQuantity) ile FROM OrderItem WHERE Order.PoDate = THIS_MONTH AND Order.StatusCode = 'Activated' AND Product2.Family = 'Plan Set' AND AvailableQuantity > 0 AND Order.Account.Type = 'Dealer'];
        return (Decimal)results[0].get('ile');

    }
    
    public Decimal getSumB2BOrders() { 
        return (Decimal)([SELECT Sum(Total_Sales_Billing_Price__c) ile FROM OrderItem WHERE Order.StatusCode = 'Activated' AND Order.Order_Owner_Department__c = 'B2B' AND Order.PoDate = THIS_MONTH][0].get('ile'));
    }
    
    public List<B2BUser> getB2BUsers() {
        List<User> users = [SELECT Id, Alias, Name FROM User WHERE Department = 'B2B' AND isActive = true ORDER BY LastName];
        Map<Id,AggregateResult> countDraftOrders = new Map<Id,AggregateResult>([SELECT Count(Id) ile, OwnerId Id FROM Order WHERE OwnerId IN :users AND Status = 'Draft' AND isReductionOrder = false AND Has_Catalogues_Only__c = false GROUP BY OwnerId]);
        Map<Id,AggregateResult> countCallRequests = new Map<Id,AggregateResult>([SELECT Count(Id) ile, OwnerId Id FROM Task WHERE OwnerId IN :users AND Type = 'Call Request' AND isClosed = false GROUP BY OwnerId]);
        Map<Id,AggregateResult> countUnansweredCalls = new Map<Id,AggregateResult>([SELECT Count(Id) ile, OwnerId Id FROM Task WHERE OwnerId IN :users AND Type = 'Unanswered Call' AND isClosed = false AND (NOT Subject LIKE '%na numer 83') AND (NOT Subject LIKE '%na numer 85') AND (NOT Subject LIKE '%na numer 77') AND (NOT Subject LIKE '%na numer 87') AND (NOT Subject LIKE '%na numer 89') AND (NOT Subject LIKE '%na numer 61') AND (NOT Subject LIKE '%na numer 90') AND (NOT Subject LIKE '%na numer 98') AND (NOT Subject LIKE '%na numer 81') AND (NOT Subject LIKE '%na numer 06') AND (NOT Subject LIKE '%na numer 08') GROUP BY OwnerId]);
        List<B2BUser> b2bUsers = new List<B2BUser>();
        
        for (User u : users) {
            B2BUser user = new B2BUser();
            user.alias = u.Alias;
            user.fullName = u.Name;
            user.countDraftOrder = countDraftOrders.containsKey(u.Id) ? ((Integer)countDraftOrders.get(u.id).get('ile')) : 0;
            user.countCallRequest = countCallRequests.containsKey(u.Id) ? ((Integer)countCallRequests.get(u.id).get('ile')) : 0;
            user.countUnansweredCall = countUnansweredCalls.containsKey(u.Id) ? ((Integer)countUnansweredCalls.get(u.id).get('ile')) : 0;
            b2bUsers.add(user);   
        }
        
        return b2bUsers;              
    }
    
    public List<TelemarketingUser> getTelemarketingUsers() {
        List<User> telemarketingUsers = [SELECT Id, Alias, Name, Calls_Objective__c, Call_Attempts_Objective__c FROM User WHERE Department = 'Telemarketing' AND isActive = true];
        
        Map<Id, AggregateResult> calls = new Map<Id, AggregateResult>([SELECT Count(Id) ile, OwnerId Id FROM Task WHERE OwnerId IN :telemarketingUsers AND CreatedDate = THIS_MONTH AND CallType != '' AND CallDurationInSeconds > 25 GROUP BY OwnerId]);
        Map<Id, AggregateResult> callAttempts = new Map<Id, AggregateResult>([SELECT Count(Id) ile, OwnerId Id FROM Task WHERE OwnerId IN :telemarketingUsers AND CreatedDate = THIS_MONTH AND CallType != '' GROUP BY OwnerId]);
        
        List<TelemarketingUser> users = new List<TelemarketingUser>();
        
        for (User u : telemarketingUsers) {
            TelemarketingUser tu = new TelemarketingUser();
            tu.alias = u.alias;
            tu.fullName = u.Name;
            tu.countCall = calls.containsKey(u.Id) ? ((Integer)calls.get(u.id).get('ile')) : 0;
            tu.countCallAttempts = callAttempts.containsKey(u.Id) ? ((Integer)callAttempts.get(u.id).get('ile')) : 0;
            tu.callObjective = u.Calls_Objective__c;
            tu.callAttemptsObjective = u.Call_Attempts_Objective__c;
            users.add(tu);
        }
        return users;
    }
    
    public List<TelemarketingUser> getTelemarketingTodayUsers() {
        List<User> todaysTelemarketingUsers = [SELECT Id, Alias, Name FROM User WHERE Id IN (SELECT userid FROM LoginHistory WHERE logintime = TODAY) AND Department = 'Telemarketing' AND isActive = true];          
        Map<Id, AggregateResult> unansweredCalls = new Map<Id,AggregateResult>([SELECT Count(Id) ile, OwnerId Id FROM Task WHERE OwnerId IN :todaysTelemarketingUsers AND Type = 'Unanswered Call' AND isClosed = false GROUP BY OwnerId]);
        Map<Id, AggregateResult> calls = new Map<Id, AggregateResult>([SELECT Count(Id) ile, OwnerId Id FROM Task WHERE OwnerId IN :todaysTelemarketingUsers AND CreatedDate = TODAY AND CallType != '' AND CallDurationInSeconds > 25 GROUP BY OwnerId]);
        Map<Id, AggregateResult> callAttempts = new Map<Id, AggregateResult>([SELECT Count(Id) ile, OwnerId Id FROM Task WHERE OwnerId IN :todaysTelemarketingUsers AND CreatedDate = TODAY AND CallType != '' GROUP BY OwnerId]);
        
        List<AggregateResult> architectTelemarketings = [SELECT Count(Id) ile, Count_Distinct(Account__r.Id) ilek, OwnerId Id, Status__c FROM Telemarketing__c WHERE OwnerId IN :todaysTelemarketingUsers AND Status__c IN ('Won','Lost') AND Lost_Reason__c != 'Inna technologia' AND Close_Date__c = THIS_MONTH AND Name = 'Przekazanie danych: adaptacje projektów' GROUP BY OwnerId, Status__c];
        List<AggregateResult> notusTelemarketing = [SELECT Count(Id) ile, Count_Distinct(Account__r.Id) ilek, OwnerId Id, Status__c FROM Telemarketing__c WHERE OwnerId IN :todaysTelemarketingUsers AND Status__c IN ('Won','Lost') AND Lost_Reason__c != 'Inna technologia' AND Close_Date__c = THIS_MONTH AND Name = 'DK Notus' GROUP BY OwnerId, Status__c];
        List<AggregateResult> xellaTelemarketing = [SELECT Count(Id) ile, Count_Distinct(Account__r.Id) ilek, OwnerId Id, Status__c FROM Telemarketing__c WHERE OwnerId IN :todaysTelemarketingUsers AND Status__c IN ('Won','Lost') AND Lost_Reason__c != 'Inna technologia' AND Close_Date__c = THIS_MONTH AND Name = 'Przekazanie danych: materiały budowlane' GROUP BY OwnerId, Status__c];
        List<TelemarketingUser> telUsers = new List<TelemarketingUser>();
        
        
        
        for (User u : todaysTelemarketingUsers) {
            TelemarketingUser tu = new TelemarketingUser();
            tu.alias = u.alias;
            tu.fullName = u.Name;
            tu.countUnansweredCall = unansweredCalls.containsKey(u.Id) ? ((Integer)unansweredCalls.get(u.id).get('ile')) : 0;
            tu.countCall = calls.containsKey(u.Id) ? ((Integer)calls.get(u.id).get('ile')) : 0;
            tu.countCallAttempts = callAttempts.containsKey(u.Id) ? ((Integer)callAttempts.get(u.id).get('ile')) : 0;
            
            Decimal effectiveness = 0;
            Decimal won = 0;
            Decimal lost = 0;
            
            for (AggregateResult ar : architectTelemarketings) {
                if (ar.get('Id') == u.Id) {                   
                    if (ar.get('Status__c') == 'Won') {
                        won = (Integer)ar.get('ilek');    
                    } else if (ar.get('Status__c') == 'Lost') {
                        lost = (Integer)ar.get('ilek');
                    }                   
                }
            }           
            effectiveness = effectiveness + (won + lost == 0 ? 0 : won / (won + lost));
            won = 0;
            lost = 0;
            
            for (AggregateResult ar : notusTelemarketing) {
                if (ar.get('Id') == u.Id) {                    
                    if (ar.get('Status__c') == 'Won') {
                        won = (Integer)ar.get('ile');    
                    } else if (ar.get('Status__c') == 'Lost') {
                        lost = (Integer)ar.get('ile');
                    }
                }               
            }
            tu.won = won;
            tu.lost = lost;
            
            effectiveness = effectiveness + (won + lost == 0 ? 0 : won / (won + lost));
            won = 0;
            lost = 0;
            
            for (AggregateResult ar : xellaTelemarketing) {
                if (ar.get('Id') == u.Id) {                  
                    if (ar.get('Status__c') == 'Won') {
                        won = (Integer)ar.get('ile');    
                    } else if (ar.get('Status__c') == 'Lost') {
                        lost = (Integer)ar.get('ile');
                    }
                }
            }
            effectiveness = effectiveness + (won + lost == 0 ? 0 : won / (won + lost)); 
            tu.effectiveness = effectiveness / 3 * 100;
            
            telUsers.add(tu);        
        }                                            
        return telUsers;
    }
    
    class B2BUser {
        public String alias {get;set;}
        public String fullName {get;set;}
        public Integer countDraftOrder {get;set;}
        public Integer countCallRequest {get;set;}
        public Integer countUnansweredCall {get;set;}
    }
    
    class TelemarketingUser {
        public String alias {get;set;}
        public String fullName {get;set;}
        public Decimal countUnansweredCall {get;set;}
        public Decimal countCall {get;set;}
        public Decimal countCallAttempts {get;set;}
        public Decimal effectiveness {get;set;}
        public Decimal callObjective {get;set;}
        public Decimal callAttemptsObjective {get;set;}
        public Decimal won {get;set;}
        public Decimal lost {get;set;}
    }
}