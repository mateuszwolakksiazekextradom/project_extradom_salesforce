public without sharing class OrderEmailBasicInfoController {
    public Id orderId;
    public OrderWrapper orderData {private set; get;}
    public void setOrderId (Id orderId) {
        orderId = orderId;
        if (String.isNotBlank(orderId)) {
            List<Order> orders = [
                SELECT Id, TotalAmount, Total_Amount__c, Shipping_Cost__c, toLabel(Shipping_Method__c), toLabel(Payment_Method__c ),
                       OrderNumber, Status, 
                    (
                        SELECT Id, UnitPrice, Quantity, Product2.Name, Product2.Design__r.Name,
                                Product2.Design__r.Thumbnail_Full_URL__c, toLabel(Version__c), TotalPrice,
                                Confirmation_Price__c, Total_Confirmation_Price__c, Design__r.Name, Description,
                                Product2.Family
                        FROM OrderItems WHERE Hide_from_Emails__c = false ORDER BY Index__c
                    )
                FROM Order
                WHERE Id = :orderId
            ];
            if (orders.size() > 0) {
                orderData = new OrderWrapper(orders.get(0));
            }
        }
    }
    public String getOrderId () {
        return orderId;
    }
    
    public class OrderWrapper {
        public Decimal totalAmount {get; set;}
        public Decimal shippingCost {get; set;}
        public String shippingMethod {get; set;}
        public String paymentMethod {get; set;}
        public Decimal customTotalAmount {get; set;}
        public List<OrderItemWrapper> orderItems {get; set;}
        
        public OrderWrapper(Order o) {
            this.totalAmount = o.TotalAmount;
            this.shippingCost = o.Shipping_Cost__c;
            this.shippingMethod = o.Shipping_Method__c;
            this.paymentMethod = o.Payment_Method__c;
            this.customTotalAmount = o.Total_Amount__c;
            this.orderItems = new List<OrderItemWrapper> (); 
            for (OrderItem oi : o.OrderItems) {
                this.orderItems.add(new OrderItemWrapper(oi));
            }
        }
    }
    
    public class OrderItemWrapper {
        public String productName {get; set;}
        public Integer quantity {get; set;}
        public Decimal unitPrice {get; set;}
        public Decimal totalPrice {get; set;}
        public String designUrl {get; set;}
        public String designName {get; set;}
        public String version {get; set;}
        public Decimal confirmationPrice {get;set;}
        public Decimal totalConfirmationPrice {get;set;}
        public String lineDescription {get; set;}
        public String designName2 {get; set;}
        public String productFamily {get; set;}
        
        public OrderItemWrapper(OrderItem oi) {
            this.productName = oi.Product2.Name;
            this.quantity = Integer.valueOf(oi.Quantity);
            this.unitPrice = oi.UnitPrice;
            this.totalPrice = oi.TotalPrice;
            this.designURL = oi.Product2.Design__r.Thumbnail_Full_URL__c;
            this.designName = oi.Product2.Design__r.Name;
            this.version = oi.Version__c;
            this.confirmationPrice = oi.Confirmation_Price__c;
            this.totalConfirmationPrice = oi.Total_Confirmation_Price__c;
            this.lineDescription = oi.Description;
            this.designName2 = oi.Design__r.Name;
            this.productFamily = oi.Product2.Family;
        }
    }
}