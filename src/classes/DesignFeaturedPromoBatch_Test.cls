@IsTest
private class DesignFeaturedPromoBatch_Test {
    private static final Integer NUM_OF_DESIGNS = 50;

    @IsTest
    private static void shouldProcessFeaturedDesigns() {
        // given
        Supplier__c newSupplier = new Supplier__c(Name = 'Supplier', Code__c = '11', Full_Name__c = 'Great Supplier');
        insert newSupplier;
        final String DESIGN_FEATURED_NPENDING_PREFIX = 'Design Featured Promo Not Pending ';
        final String DESIGN_NFEATURED_PENDING_PREFIX = 'Design Not Featured Promo But Pending ';
        final String DESIGN_FEATURED_PENDING_PREFIX = 'Design Featured Promo Pending ';
        final String DESIGN_NFEATURED_NPENDING_PREFIX = 'Design Not Featured Promo Not Pending ';
        List<Design__c> designs = new List<Design__c> ();
        for (Integer i = 0; i < NUM_OF_DESIGNS; i++) {
            designs.add(
                    new Design__c (
                            Name = DESIGN_FEATURED_NPENDING_PREFIX + String.valueOf(i), Full_Name__c = DESIGN_FEATURED_NPENDING_PREFIX + String.valueOf(i),
                            Type__c = ConstUtils.DESIGN_HOUSE_TYPE, Featured_Promo__c = 10.0, Pending_Featured_Promo__c = null, Supplier__c = newSupplier.Id
                    )
            );
        }
        for (Integer i = 0; i < NUM_OF_DESIGNS; i++) {
            designs.add(
                    new Design__c (
                            Name = DESIGN_NFEATURED_PENDING_PREFIX + String.valueOf(i), Full_Name__c = DESIGN_NFEATURED_PENDING_PREFIX + String.valueOf(i),
                            Type__c = ConstUtils.DESIGN_HOUSE_TYPE, Featured_Promo__c = null, Pending_Featured_Promo__c = 10.0, Supplier__c = newSupplier.Id
                    )
            );
        }
        for (Integer i = 0; i < NUM_OF_DESIGNS; i++) {
            designs.add(
                    new Design__c (
                            Name = DESIGN_FEATURED_PENDING_PREFIX + String.valueOf(i), Full_Name__c = DESIGN_FEATURED_PENDING_PREFIX + String.valueOf(i),
                            Type__c = ConstUtils.DESIGN_HOUSE_TYPE, Featured_Promo__c = 20.0, Pending_Featured_Promo__c = 30.0, Supplier__c = newSupplier.Id
                    )
            );
        }
        for (Integer i = 0; i < NUM_OF_DESIGNS; i++) {
            designs.add(
                    new Design__c (
                            Name = DESIGN_NFEATURED_NPENDING_PREFIX + String.valueOf(i), Full_Name__c = DESIGN_NFEATURED_NPENDING_PREFIX + String.valueOf(i),
                            Type__c = ConstUtils.DESIGN_HOUSE_TYPE, Featured_Promo__c = null, Pending_Featured_Promo__c = null, Supplier__c = newSupplier.Id
                    )
            );
        }
        insert designs;

        // when
        Test.startTest();
        Database.executeBatch(new DesignFeaturedPromoBatch());
        Test.stopTest();
        List<Design__c> processedDesigns = [SELECT Id, Featured_Promo__c, Pending_Featured_Promo__c, Name FROM Design__c];

        // then
        for (Design__c processedDesign : processedDesigns) {
            if (processedDesign.Name.startsWith(DESIGN_FEATURED_NPENDING_PREFIX) || processedDesign.Name.startsWith(DESIGN_NFEATURED_NPENDING_PREFIX)) {
                System.assertEquals(null, processedDesign.Featured_Promo__c);
            } else if (processedDesign.Name.startsWith(DESIGN_NFEATURED_PENDING_PREFIX)) {
                System.assertEquals(10.0, processedDesign.Featured_Promo__c);
            } else if (processedDesign.Name.startsWith(DESIGN_FEATURED_PENDING_PREFIX)) {
                System.assertEquals(30.0, processedDesign.Featured_Promo__c);
            }
        }
    }
}