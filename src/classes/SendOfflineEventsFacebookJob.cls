public with sharing class SendOfflineEventsFacebookJob implements Schedulable {
    
    private static final String TOKEN = 'EAAE1WZCmjqpsBAHD9FZCJ2RZABJAAFTk0tgQvfcZCMFuoAnetQZCAQ9SrAXQA9aHN6KAtJT06HRg7wv3ToOOlN7y2292QEpfU2CReEM7QvkjpV5WdcExiKi79Ss9akkmKZBo5WaXZAdMnT4lSXXBQYdBUeqvAwIsDNnVtMR8VNj1cVsKSt9jFb0';
    private static final String EVENT_SET_ID = 'EAAE1WZCmjqpsBAHD9FZCJ2RZABJAAFTk0tgQvfcZCMFuoAnetQZCAQ9SrAXQA9aHN6KAtJT06HRg7wv3ToOOlN7y2292QEpfU2CReEM7QvkjpV5WdcExiKi79Ss9akkmKZBo5WaXZAdMnT4lSXXBQYdBUeqvAwIsDNnVtMR8VNj1cVsKSt9jFb0';
    
    public void execute(SchedulableContext sc) {
        Map<String,String> params = new Map<String,String>();
        params.put('access_token', 'EAAE1WZCmjqpsBAHD9FZCJ2RZABJAAFTk0tgQvfcZCMFuoAnetQZCAQ9SrAXQA9aHN6KAtJT06HRg7wv3ToOOlN7y2292QEpfU2CReEM7QvkjpV5WdcExiKi79Ss9akkmKZBo5WaXZAdMnT4lSXXBQYdBUeqvAwIsDNnVtMR8VNj1cVsKSt9jFb0');
        params.put('upload_tag', 'test');
        List<Order> orders = [SELECT Id, BillingEmail__c, BillingPhone__c, (SELECT Total_Price_Net_est__c FROM OrderItems) FROM Order WHERE Status = 'Sent' AND Sent_Date__c = YESTERDAY];
        
        List<Conversion> data = new List<Conversion>{};
        
        Conversion conv = new Conversion();
        conv.curr = 'PLN';
        conv.value = 0.01;
        conv.event_name = 'Purchase';
        conv.event_time = Math.round(System.now().getTime()/1000);
        MatchKeys match_keys = new MatchKeys();
        match_keys.phone = new List<String>{ EncodingUtil.convertToHex(Crypto.generateDigest('SHA-256', Blob.valueOf('604651831'))) };
        conv.match_keys = match_keys;
        
        data.add(conv);
        System.debug(data);
        params.put('data', JSON.serialize(data).replaceAll('curr','currency'));
        
        String endpoint = 'https://graph.facebook.com/v3.2/1886710501438599/events';
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpoint);
        req.setTimeOut(120000);
        req.setMethod('POST');
        req.setBody(convertParamMaptoString(params));
    
        HttpResponse res = h.send(req);
        String response = res.getBody();
        System.debug(response);
    }
    
    private static String convertParamMaptoString(Map<String,String> params){
        String returned = '';
        for(String key : params.keySet()){
            if(returned == ''){
                //returned += ‘?’;
            }
            else {
                returned += '&';
            }
            returned += key+'='+EncodingUtil.urlEncode(params.get(key), 'UTF-8');
        }
        return returned;
    }
    
    public class MatchKeys {
        public List<String> email;
        public List<String> phone;
    }
    public class Conversion {
        public MatchKeys match_keys;
        public String curr;
        public Decimal value;
        public String event_name;
        public Integer event_time;
    }
    
}