public with sharing class ProductSelectionController {

    @AuraEnabled
    public static List<PriceBookEntry> getDesignProducts(String pricebookId, String designId) {
        List<PricebookEntry> designProducts = new List<PricebookEntry> ();
        pricebookId = OrderUtils.getPricebookId(pricebookId);
        if (String.isNotBlank(designId)) {
            designProducts = [
                SELECT Id, UnitPrice, Product2.Id, Product2.Name, Product2.Design__r.Name, Product2.Family FROM PriceBookEntry
                WHERE Product2.Design__c = :designId AND Product2.Family = :ConstUtils.PRODUCT_DESIGN_FAMILY AND Pricebook2Id = :pricebookId
                	AND Product2.IsActive = true AND IsActive = true
            ];
        }
        return designProducts;
    }
    
    @AuraEnabled
    public static List<PricebookEntry> getAdditionalProducts(String pricebookId, String designId, String family) {
        pricebookId = OrderUtils.getPricebookId(pricebookId);
        List<PricebookEntry> additionalProducts = [
            SELECT Id, UnitPrice, Product2.Id, Product2.Name, Product2.Design__r.Name, Product2.Family FROM PricebookEntry
            WHERE (Product2.Design__c = :designId OR Product2.Design__c = null) AND Product2.Family = :family
            	AND Product2.IsActive = true AND Pricebook2Id = :pricebookId AND IsActive = true
            ORDER BY Product2.Name
        ];
		return additionalProducts;
    }
    
    @AuraEnabled
    public static List<PricebookEntry> getFamilyProducts(String pricebookId, String family) {
        pricebookId = OrderUtils.getPricebookId(pricebookId);
        List<PricebookEntry> familyProducts = [
            SELECT Id, UnitPrice, Product2.Id, Product2.Name, Product2.Design__r.Name, Product2.Family FROM PricebookEntry
            WHERE Product2.Family = :family AND Product2.IsActive = true AND Pricebook2Id = :pricebookId
            	AND IsActive = true
            ORDER BY Product2.Name
        ];
		return familyProducts;
    }
}