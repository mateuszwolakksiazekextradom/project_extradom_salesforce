public without sharing class ExtradomSummaryController {

    public ExtradomSummaryController() {}

    public List<ConnectApi.User> getLiveUsers() {
        Datetime live = System.now().addMinutes(-3);
        List<ID> ids = new List<ID>{};
        for (Chatter_Custom_Settings__c ccs : [SELECT SetupOwnerId FROM Chatter_Custom_Settings__c WHERE LastModifiedDate > :live ORDER BY LastModifiedDate DESC LIMIT 500]) {
            ids.add(ccs.SetupOwnerId);
        }
        List<ConnectApi.User> users = new List<ConnectApi.User>{};
        for (ConnectApi.BatchResult result : ConnectApi.ChatterUsers.getUserBatch(Network.getNetworkId(), ids)) {
            if (result.isSuccess()) {
                users.add((ConnectApi.User) result.getResult());
            }
        }
        return users;
    }
    
    public ConnectApi.FeedElement getLastCommentedFeedElement() {
        Datetime live = System.now().addHours(-2);
        for (FeedComment comment : [SELECT FeedItemId, CreatedDate FROM FeedComment WHERE CreatedDate > :live ORDER BY CreatedDate DESC LIMIT 100]) {
            return ConnectApi.ChatterFeeds.getFeedElement(Network.getNetworkId(), comment.FeedItemId);
        }
        return null;
    }
    
    public Integer getCountTotalMembers() {
        return [SELECT count() FROM User WHERE ProfileId = '00eb0000000IgKb'];
    }
    
    public Integer getCountTotalVerifiedMembers() {
        return [SELECT count() FROM User WHERE ProfileId = '00eb0000000IgKb' AND Phone_verification__c = true];
    }
    
    public Integer getCountTotalConstructions() {
        return [SELECT count() FROM Construction__c];
    }
    
    public Integer getCountTotalLocalizedConstructions() {
        return [SELECT count() FROM Construction__c WHERE Location__latitude__s != null];
    }
    
    public Integer getCountTodaysNewMembers() {
        return [SELECT count() FROM NetworkMember WHERE CreatedDate = TODAY AND NetworkId = :Network.getNetworkId()];
    }
    
    public Integer getCountTodaysUniqueMembers() {
        return [SELECT count() FROM Chatter_Custom_Settings__c WHERE LastModifiedDate = TODAY];
    }
    
    public Integer getCountTodaysComments() {
        return [SELECT count() FROM FeedComment WHERE CreatedDate = TODAY];
    }
    
    public Integer getCountTodaysPosts() {
        return [SELECT count() FROM FeedItem WHERE CreatedDate = TODAY];
    }
    
}