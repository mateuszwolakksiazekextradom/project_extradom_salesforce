public with sharing class ConstructionMapViewController {

    public String phone { get; set; }
    public String verificationCode { get; set; }
    public String designId { get; set; }

    public Boolean isExactReady {
        get {
            for (Construction__c c : [SELECT Id, Location_Visibility__c FROM Construction__c WHERE OwnerId = :UserInfo.getUserId() AND (Plot__c != null OR Location__longitude__s != null)]) {
                if (c.Location_Visibility__c == 'Default') {
                    return true;
                }
            }
            return false;
        }
    }
    
    public String mapAccess {
        
        get {
            String access = 'No Access';
            for (Construction__c c : [SELECT Id, Location_Visibility__c FROM Construction__c WHERE OwnerId = :UserInfo.getUserId() AND (Plot__c != null OR Location__longitude__s != null)]) {
                if (c.Location_Visibility__c == 'Default') {
                    access = 'Default';
                    return access;
                }
                access = 'Region';
            }
            return access;
        }
    }

    public ConstructionMapViewController() {
        try {
            this.designId = ApexPages.currentPage().getParameters().get('designid');
        } catch (Exception e) {
        }
    }
    
    public List<Construction__c> getPublicConstructions() {
        return [SELECT Id, Owner.Id, Owner.Name, Stage__c, Location__latitude__s, Location__longitude__s, Location_Visibility__c, CreatedDate, Design__r.Id, Design__r.Full_Name__c, Design__r.Code__c, Design__r.Thumbnail_Base_URL__c, Design__r.Status__c, Plot__r.Id, Plot__r.Name, Plot__r.Area__c, Plot__r.Center__latitude__s, Plot__r.Center__longitude__s, Plot__r.Location__r.Parent_Location__r.Parent_Location__r.Location__latitude__s, Plot__r.Location__r.Parent_Location__r.Parent_Location__r.Location__longitude__s, Plot__r.Location__r.Parent_Location__r.Parent_Location__r.Name FROM Construction__c WHERE (Plot__c != null OR Location__longitude__s != null) LIMIT 1000];
    }
    
    public Construction__c getMyConstructionWithLocation() {
        for (Construction__c c : [SELECT Id, Location__latitude__s, Location__longitude__s FROM Construction__c WHERE OwnerId = :UserInfo.getUserId() AND Location__longitude__s != null]) {
            return c;
        }
        return null;
    }
    
    public List<Construction__c> getPublicConstructionsNearMe() {
        Construction__c c = this.getMyConstructionWithLocation();
        if (c != null) {
            return [SELECT Id, Owner.Id, Owner.Name, Stage__c, Location__latitude__s, Location__longitude__s, Location_Visibility__c, CreatedDate, Design__r.Id, Design__r.Full_Name__c, Design__r.Code__c, Design__r.Thumbnail_Base_URL__c, Design__r.Status__c, Shared_Location__r.Name, Shared_Location__r.RecordType.Name, Shared_Location__r.Location__latitude__s, Shared_Location__r.Location__longitude__s FROM Construction__c WHERE DISTANCE(Location__c, GEOLOCATION(:c.Location__latitude__s, :c.Location__longitude__s), 'km') < 200 ORDER BY Design_Status_Order__c, Sharing_Order__c LIMIT 1000];
        }
        return new List<Construction__c>{};
    }
    
    public List<Construction__c> getPublicConstructionsForDesign() {
        return [SELECT Id, Owner.Id, Owner.Name, Stage__c, Location__latitude__s, Location__longitude__s, Location_Visibility__c, CreatedDate, Design__r.Id, Design__r.Full_Name__c, Design__r.Code__c, Design__r.Thumbnail_Base_URL__c, Design__r.Status__c, Shared_Location__r.Name, Shared_Location__r.RecordType.Name, Shared_Location__r.Location__latitude__s, Shared_Location__r.Location__longitude__s FROM Construction__c WHERE Design__c = :this.designId ORDER BY Design_Status_Order__c, Sharing_Order__c LIMIT 1000];
    }
    
    public PageReference verifyPhone() {
        User u = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        u.Phone = this.phone;
        u.Phone_Verification_Request_Date__c = System.now();
        update u;
        return null;
    }
    
    public PageReference verifyRequestCode() {
        User u = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        u.Phone_Verification_Response_Code__c = this.verificationCode;
        update u;
        return null;
    }
    
    public User getUserPhoneVerification() {
        return [SELECT Id, Phone, Phone_Verification_Request_Date__c, Phone_Verification__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
    }

}