public with sharing class PageDesignResultsExtension {
    
    private final Page__c p;
    public Integer minUsableArea{get;set;}
    public Integer maxUsableArea{get;set;}
    public Integer maxPlotSizeHorizontal{get;set;}
    public Integer maxHeight{get;set;}
    public Integer maxFootprint{get;set;}
    public Integer minRoofSlope{get;set;}
    public Integer maxRoofSlope{get;set;}
    public Integer rooms{get;set;}
    public Integer catFlight{get;set;}
    public Integer catGarage{get;set;}
    public Integer catBasement{get;set;}
    public Integer catRoof{get;set;}
    public Integer catRidge{get;set;}
    public Integer catEntrance{get;set;}
    public Integer catGarageLocation{get;set;}
    public Integer catLivingType{get;set;}
    public Integer catStyle{get;set;}
    public Integer catConstruction{get;set;}
    public Integer catEco{get;set;}
    public Integer catVeranda{get;set;}
    public Integer catWinterGarden{get;set;}
    public Integer catTerrace{get;set;}
    public Integer catOriel{get;set;}
    public Integer catSouthEntrance{get;set;}
    public List<Design__c> designsList{get;set;}
    
    public PageDesignResultsExtension(ApexPages.StandardController stdController) {
        if (!Test.isRunningTest()) {
            stdController.addFields(new List<String>{'Roof_Query__c', 'Search_Text_Query__c'});
        }
        p = (Page__c)stdController.getRecord();
        String query = 'SELECT Full_Name__c, Current_Price__c, Gross_Price__c, Thumbnail_Base_URL__c, Code__c, Height__c, Usable_Area__c, Footprint__c, Capacity__c, Roof_slope__c, Count_Rooms__c, Minimum_Plot_Size_Horizontal__c, Minimum_Plot_Size_Vertical__c FROM Design__c WHERE Status__c = \'Active\'';
        // Usable Area
        if (ApexPages.currentPage().getParameters().containsKey('Area.From')) {
            this.minUsableArea = Integer.valueOf(ApexPages.currentPage().getParameters().get('Area.From'));
            query += ' AND Usable_Area__c >= '+String.valueOf(this.minUsableArea);
        }
        if (ApexPages.currentPage().getParameters().containsKey('Area.To')) {
            this.maxUsableArea = Integer.valueOf(ApexPages.currentPage().getParameters().get('Area.To'));
            query += ' AND Usable_Area__c <= '+String.valueOf(this.maxUsableArea);
        }
        // Plot Size
        if (ApexPages.currentPage().getParameters().containsKey('AllotmentMinWidth.Value')) {
            this.maxPlotSizeHorizontal = Integer.valueOf(ApexPages.currentPage().getParameters().get('AllotmentMinWidth.Value'));
            query += ' AND Minimum_Plot_Size_Horizontal__c <= '+String.valueOf(this.maxPlotSizeHorizontal);
        }
        // Height
        if (ApexPages.currentPage().getParameters().containsKey('TotalHeight.Value')) {
            this.maxHeight = Integer.valueOf(ApexPages.currentPage().getParameters().get('TotalHeight.Value'));
            query += ' AND Height__c <= '+String.valueOf(this.maxHeight);
        }
        // Footprint
        if (ApexPages.currentPage().getParameters().containsKey('BuildingArea.Value')) {
            this.maxFootprint = Integer.valueOf(ApexPages.currentPage().getParameters().get('BuildingArea.Value'));
            query += ' AND Footprint__c <= '+String.valueOf(this.maxFootprint);
        }
        // Roof Slope
        if (ApexPages.currentPage().getParameters().containsKey('RoofSlope.From')) {
            this.minRoofSlope = Integer.valueOf(ApexPages.currentPage().getParameters().get('RoofSlope.From'));
            query += ' AND Roof_slope__c >= '+String.valueOf(this.minRoofSlope);
        }
        if (ApexPages.currentPage().getParameters().containsKey('RoofSlope.To')) {
            this.maxRoofSlope = Integer.valueOf(ApexPages.currentPage().getParameters().get('RoofSlope.To'));
            query += ' AND Roof_slope__c <= '+String.valueOf(this.maxRoofSlope);
        }
        // Count Rooms
        if (ApexPages.currentPage().getParameters().containsKey('Rooms.Value')) {
            this.rooms = Integer.valueOf(ApexPages.currentPage().getParameters().get('Rooms.Value'));
            query += ' AND Count_Rooms__c = '+String.valueOf(this.rooms);
        }
        // Flight
        if (ApexPages.currentPage().getParameters().containsKey('Flight.47')) {
            this.catFlight = 47;
            query += ' AND Category_Keys__c like \'%[47]%\'';
        } else if (ApexPages.currentPage().getParameters().containsKey('Flight.48')) {
            this.catFlight = 48;
            query += ' AND Category_Keys__c like \'%[48]%\'';
        } else if (ApexPages.currentPage().getParameters().containsKey('Flight.63')) {
            this.catFlight = 63;
            query += ' AND Category_Keys__c like \'%[63]%\'';
        }
        // Garage
        if (ApexPages.currentPage().getParameters().containsKey('Garage.79')) {
            this.catGarage = 79;
            query += ' AND Category_Keys__c like \'%[79]%\'';
        } else if (ApexPages.currentPage().getParameters().containsKey('Garage.50')) {
            this.catGarage = 50;
            query += ' AND Category_Keys__c like \'%[50]%\'';
        } else if (ApexPages.currentPage().getParameters().containsKey('Garage.52')) {
            this.catGarage = 52;
            query += ' AND Category_Keys__c like \'%[52]%\'';
        } else if (ApexPages.currentPage().getParameters().containsKey('Garage.111')) {
            this.catGarage = 111;
            query += ' AND Category_Keys__c like \'%[111]%\'';
        }
        // Roof
        if (ApexPages.currentPage().getParameters().containsKey('Roof.4')) {
            this.catRoof = 4;
            query += ' AND Category_Keys__c like \'%[4]%\'';
        } else if (ApexPages.currentPage().getParameters().containsKey('Roof.5')) {
            this.catRoof = 5;
            query += ' AND Category_Keys__c like \'%[5]%\'';
        } else if (ApexPages.currentPage().getParameters().containsKey('Roof.12')) {
            this.catRoof = 12;
            query += ' AND Category_Keys__c like \'%[12]%\'';
        } else if (ApexPages.currentPage().getParameters().containsKey('Roof.13')) {
            this.catRoof = 13;
            query += ' AND Category_Keys__c like \'%[13]%\'';
        } else if (ApexPages.currentPage().getParameters().containsKey('Roof.7')) {
            this.catRoof = 7;
            query += ' AND Category_Keys__c like \'%[7]%\'';
        } else if (ApexPages.currentPage().getParameters().containsKey('Roof.9')) {
            this.catRoof = 9;
            query += ' AND Category_Keys__c like \'%[9]%\'';
        } else if (ApexPages.currentPage().getParameters().containsKey('Roof.3')) {
            this.catRoof = 3;
            query += ' AND Category_Keys__c like \'%[3]%\'';
        } else if (ApexPages.currentPage().getParameters().containsKey('Roof.6')) {
            this.catRoof = 6;
            query += ' AND Category_Keys__c like \'%[6]%\'';
        } else if (ApexPages.currentPage().getParameters().containsKey('Roof.8')) {
            this.catRoof = 8;
            query += ' AND Category_Keys__c like \'%[8]%\'';
        }
        // Living Type
        if (ApexPages.currentPage().getParameters().containsKey('LivingType.49')) {
            this.catLivingType = 49;
            query += ' AND Category_Keys__c like \'%[49]%\'';
        } else if (ApexPages.currentPage().getParameters().containsKey('LivingType.43')) {
            this.catLivingType = 43;
            query += ' AND Category_Keys__c like \'%[43]%\'';
        } else if (ApexPages.currentPage().getParameters().containsKey('LivingType.42')) {
            this.catLivingType = 42;
            query += ' AND Category_Keys__c like \'%[42]%\'';
        }
        // Style
        if (ApexPages.currentPage().getParameters().containsKey('Style.81')) {
            this.catStyle = 81;
            query += ' AND Category_Keys__c like \'%[81]%\'';
        } else if (ApexPages.currentPage().getParameters().containsKey('Style.77')) {
            this.catStyle = 77;
            query += ' AND Category_Keys__c like \'%[77]%\'';
        } else if (ApexPages.currentPage().getParameters().containsKey('Style.90')) {
            this.catStyle = 90;
            query += ' AND Category_Keys__c like \'%[90]%\'';
        } else if (ApexPages.currentPage().getParameters().containsKey('Style.98')) {
            this.catStyle = 98;
            query += ' AND Category_Keys__c like \'%[98]%\'';
        } else if (ApexPages.currentPage().getParameters().containsKey('Style.104')) {
            this.catStyle = 104;
            query += ' AND Category_Keys__c like \'%[104]%\'';
        }
        // Construcion
        if (ApexPages.currentPage().getParameters().containsKey('Construction.82')) {
            this.catConstruction = 82;
            query += ' AND Category_Keys__c like \'%[82]%\'';
        } else if (ApexPages.currentPage().getParameters().containsKey('Construction.41')) {
            this.catConstruction = 41;
            query += ' AND Category_Keys__c like \'%[41]%\'';
        } else if (ApexPages.currentPage().getParameters().containsKey('Construction.39')) {
            this.catConstruction = 39;
            query += ' AND Category_Keys__c like \'%[39]%\'';
        } else if (ApexPages.currentPage().getParameters().containsKey('Construction.38')) {
            this.catConstruction = 38;
            query += ' AND Category_Keys__c like \'%[38]%\'';
        } else if (ApexPages.currentPage().getParameters().containsKey('Construction.40')) {
            this.catConstruction = 40;
            query += ' AND Category_Keys__c like \'%[40]%\'';
        }
        // Eco
        if (ApexPages.currentPage().getParameters().containsKey('Eco.83')) {
            this.catEco = 83;
            query += ' AND Category_Keys__c like \'%[83]%\'';
        } else if (ApexPages.currentPage().getParameters().containsKey('Eco.84')) {
            this.catEco = 84;
            query += ' AND Category_Keys__c like \'%[84]%\'';
        }
        // Garage Location
        if (ApexPages.currentPage().getParameters().containsKey('GarageLocation.66')) {
            this.catGarageLocation = 66;
            query += ' AND Category_Keys__c like \'%[66]%\'';
        } else if (ApexPages.currentPage().getParameters().containsKey('GarageLocation.67')) {
            this.catGarageLocation = 67;
            query += ' AND Category_Keys__c like \'%[67]%\'';
        } else if (ApexPages.currentPage().getParameters().containsKey('GarageLocation.73')) {
            this.catGarageLocation = 73;
            query += ' AND Category_Keys__c like \'%[73]%\'';
        } else if (ApexPages.currentPage().getParameters().containsKey('GarageLocation.70')) {
            this.catGarageLocation = 70;
            query += ' AND Category_Keys__c like \'%[70]%\'';
        } else if (ApexPages.currentPage().getParameters().containsKey('GarageLocation.78')) {
            this.catGarageLocation = 78;
            query += ' AND Category_Keys__c like \'%[78]%\'';
        }
        // Basement
        if (ApexPages.currentPage().getParameters().containsKey('Basement.46')) {
            this.catBasement = 46;
            query += ' AND Category_Keys__c like \'%[46]%\'';
        } else if (ApexPages.currentPage().getParameters().containsKey('Basement.72')) {
            this.catBasement = 72;
            query += ' AND Category_Keys__c like \'%[72]%\'';
        }
        // Ridge
        if (ApexPages.currentPage().getParameters().containsKey('Crest.106')) {
            this.catRidge = 106;
            query += ' AND Category_Keys__c like \'%[106]%\'';
        } else if (ApexPages.currentPage().getParameters().containsKey('Crest.105')) {
            this.catRidge = 105;
            query += ' AND Category_Keys__c like \'%[105]%\'';
        }
        query += ' ORDER BY Page_Score_Unique__c DESC LIMIT 24';
        this.designsList = (List<Design__c>)Database.query(query);
    }
    
    public List<Design__c> getDesignResults() {
        return this.designsList;
    }

}