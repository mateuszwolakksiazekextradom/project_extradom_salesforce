public without sharing class CatalogueOrderController {

    private String code;
    public String billingFirstName {get;set;}
    public String billingLastName {get;set;}
    public String billingAddressStreetName {get;set;}
    public String billingAddressStreetNo {get;set;}
    public String billingAddressStreetFlat {get;set;}
    public String billingPostalCodePart1 {get;set;}
    public String billingPostalCodePart2 {get;set;}
    public String billingCity {get;set;}
    public String billingEmail {get;set;}
    public String billingPhone {get;set;}
    public String questionDesignOrderEstimate {get;set;}
    public String questionStartOfInvestmentEstimate {get;set;}
    public String questionHasPlan {get;set;}
    public String shippingMethod {get;set;}
    public Boolean optInRegulations {get;set;}
    public Boolean optInOrder {get;set;}
    public Boolean optIn {get;set;}
    public Boolean optInForward {get;set;}
    
    public CatalogueOrderController() {
        this.optInRegulations = true;
        this.optInOrder = true;
        this.optIn = true;
        this.optInForward = false;
        this.shippingMethod = 'Regular (Prepayment)';
    }
    
    public PageReference save() {
        Order__c o = new Order__c();
        o.Add_Ons__c = 'Catalogue';
        o.Billing_First_Name__c = this.billingFirstName;
        o.Billing_Last_Name__c = this.billingLastName;
        o.Billing_Address_Street_Name__c = this.billingAddressStreetName;
        o.Billing_Address_Street_No__c = this.billingAddressStreetNo;
        o.Billing_Address_Street_Flat__c = this.billingAddressStreetFlat;
        o.Billing_Postal_Code_Part_1__c = this.billingPostalCodePart1;
        o.Billing_Postal_Code_Part_2__c = this.billingPostalCodePart2;
        o.Billing_City__c = this.billingCity;
        o.Billing_Email__c = this.billingEmail;
        o.Billing_Phone__c = this.billingPhone;
        o.Question_Design_Order_Estimate__c = this.questionDesignOrderEstimate;
        o.Question_Start_of_Investment_Estimate__c = this.questionStartOfInvestmentEstimate;
        o.Question_Has_Plan__c = this.questionHasPlan;
        o.Shipping_Method__c = this.shippingMethod;
        o.Opt_In_Regulations__c = this.optInRegulations;
        o.Opt_In_Order__c = this.optInOrder;
        o.Opt_In__c = this.optIn;
        o.Opt_In_Forward__c = this.optInForward;
        List<Contact> contactsBillingEmail = [SELECT id, accountId, salutation, firstname, lastname, phone, email, account.ownerId FROM Contact WHERE EmailId__c = :this.billingEmail AND EmailId__c != '' LIMIT 1];
        List<Contact> contactsBillingPhone = [SELECT id, accountId, salutation, firstname, lastname, phone, email, account.ownerId FROM Contact WHERE PhoneId__c = :this.billingPhone AND PhoneId__c != '' LIMIT 1];
        List<Contact> contacts = new List<Contact>{};
        contacts.addAll(contactsBillingEmail);
        contacts.addAll(contactsBillingPhone);
        
        Id orderAgentId = this.getNewAccountOwnerId();
        String accountName = this.BillingFirstName + ' ' + this.BillingLastName;
        
            if (contacts.isEmpty()) {
                Account a = new Account(Name=accountName, OwnerId=orderAgentId, AccountSource='Extradom.pl (Catalogue)');
                insert a;
                Contact c = new Contact(FirstName=this.BillingFirstName, LastName=this.BillingLastName, AccountId=a.Id, Phone=this.BillingPhone, Email=this.BillingEmail, OwnerId=orderAgentId, Phone_Opt_In__c=this.optIn);
                insert c;
                o.Account__c = a.Id;
            } else if (!contactsBillingEmail.isEmpty() && contactsBillingPhone.isEmpty()) {
                Contact c = contactsBillingEmail.get(0);
                o.Account__c = c.AccountId;
                if (c.Account.OwnerId=='005b0000000RxOP') {
                    c.Account.OwnerId = orderAgentId;
                    c.OwnerId = orderAgentId;
                    update c.Account;
                }
                if (String.isBlank(c.Phone)) {
                    c.Phone = this.BillingPhone;
                    c.Phone_Opt_In__c=this.optIn;
                } else {
                    Contact c1 = new Contact(FirstName=this.BillingFirstName, LastName=this.BillingLastName, AccountId=c.AccountId, Phone=this.BillingPhone, OwnerId=c.Account.OwnerId, Phone_Opt_In__c=this.optIn);
                    insert c1;
                }
                if (this.optIn) {
                    c.HasOptedOutOfEmail = false;
                }
                update c;
            } else if (contactsBillingEmail.isEmpty() && !contactsBillingPhone.isEmpty()) {
                Contact c = contactsBillingPhone.get(0);
                o.Account__c = c.AccountId;
                if (c.Account.OwnerId=='005b0000000RxOP') {
                    c.Account.OwnerId = orderAgentId;
                    c.OwnerId = orderAgentId;
                    update c.Account;
                }
                if (String.isBlank(c.Email)) {
                    c.Email = this.BillingEmail;
                } else {
                    Contact c1 = new Contact(FirstName=this.BillingFirstName, LastName=this.BillingLastName, AccountId=c.AccountId, Email=this.BillingEmail, OwnerId=c.Account.OwnerId);
                    insert c1;
                }
                if (this.optIn) {
                    c.Phone_Opt_In__c = true;
                }
                update c;
            } else {
                Contact c = contactsBillingEmail.get(0);
                o.Account__c = c.AccountId;
                if (c.Account.OwnerId=='005b0000000RxOP') {
                    c.Account.OwnerId = orderAgentId;
                    c.OwnerId = orderAgentId;
                    update c.Account;
                }
                if (this.optIn) {
                    c.HasOptedOutOfEmail = false;
                }
                if (contactsBillingEmail.get(0).id==contactsBillingPhone.get(0).id) {
                    if (this.optIn) {
                        c.Phone_Opt_In__c = true;
                    }
                }
                update c;
            }
        
        insert o;
        PageReference pageref = Page.CatalogueOrderViewComplete;
        pageref.setRedirect(true);
        return pageref;
    }
    
    private Id getNewAccountOwnerId() {
        List<AggregateResult> usersLogin = [SELECT userid, min(logintime) firstLogin FROM LoginHistory WHERE logintime = TODAY GROUP BY userid];
        List<GroupMember> members = [SELECT UserOrGroupId FROM GroupMember WHERE GroupId = '00Gb0000000Qpp7'];
        List<GroupMember> lastSeenMembers = New List<GroupMember>();
        
        List<AggregateResult> days = [Select DAY_IN_MONTH(loginTime) day FROM LoginHistory WHERE LoginTime = THIS_MONTH AND LoginTime <> TODAY AND SourceIp = '195.94.192.98' AND UserId IN (Select Id From User WHERE IsActive = true AND ProfileId = '00eb0000000I9GZ') GROUP BY DAY_IN_MONTH(loginTime) HAVING count_distinct(userId) > 1];
        
        Integer[] daysArray = new Integer[]{};
        for (AggregateResult day : days) {
            daysArray.add((Integer)day.get('day'));
        }
        
        List<AggregateResult> activeUsers = [Select userId FROM LoginHistory WHERE LoginTime = THIS_MONTH AND LoginTime <> TODAY AND SourceIp = '195.94.192.98' AND UserId IN (Select Id From User WHERE IsActive = true AND ProfileId = '00eb0000000I9GZ') GROUP BY UserId, day_in_month(loginTime)];
        
        String orderAgentId = '';
        Double orderAgentRate = 0;
        Integer orderAgentCount = 0;
        
        for (AggregateResult ul : usersLogin) {
           for (GroupMember m : members) {
                if (ul.get('userid')==(String)m.UserOrGroupId) {
                    DateTime qStart = (DateTime)ul.get('firstLogin');
                    Double qHours = (System.now().getTime()-qStart.getTime())/1000.0/60.0/60.0;
                    Double qTotalHours = qHours;
                    for (AggregateResult activeUser : activeUsers) {
                        if (activeUser.get('userId')==(String)m.UserOrGroupId) {
                            qTotalHours += 8.0;
                        }
                    }
                    lastSeenMembers.add(m);
                    if (qHours < 8.0) {
                        Integer qCount = 0;
                        qCount = [SELECT count() FROM Account WHERE OwnerId = :m.UserOrGroupId AND SPAM__c = false AND AccountSource like '%(Catalogue)' AND CreatedDate = THIS_MONTH];
                        Double qRate = qCount / qTotalHours;
                        if (qRate <= orderAgentRate || orderAgentId == '') {
                            orderAgentId = (String)m.UserOrGroupId;
                            orderAgentRate = qRate;
                            orderAgentCount = qCount;
                        }
                    }
                }
            }
        }
        if (orderAgentId == '') {
            orderAgentId = '005b0000000RxOP';
        }
        return orderAgentId;
    }

}