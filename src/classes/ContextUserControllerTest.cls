@IsTest(SeeAllData=true)
private class ContextUserControllerTest {

    static testMethod void myUnitTest() {
    
        ConnectApi.FeedElementPage testPage = new ConnectApi.FeedElementPage();
        List<ConnectApi.FeedItem> testItemList = new List<ConnectApi.FeedItem>();
        testItemList.add(new ConnectApi.FeedItem());
        testPage.elements = testItemList;
        
        ConnectApi.TopicPage testTopicPage = new ConnectApi.TopicPage();
        List<ConnectApi.Topic> testTopicList = new List<ConnectApi.Topic>();
        testTopicPage.topics = testTopicList;
        
        ConnectApi.ChatterFeeds.setTestGetFeedElementsFromFeed(null, ConnectApi.FeedType.News, 'me', null, null, null, null, ConnectApi.FeedSortOrder.LastModifiedDateDesc, testPage);
        ConnectApi.ChatterFeeds.setTestGetFeedElementsUpdatedSince(Network.getNetworkId(), ConnectApi.FeedType.News, 'me', null, null, null, null, null, testPage);
        
        ConnectApi.ChatterFeeds.setTestGetFeedElementsUpdatedSince(null, ConnectApi.FeedType.News, 'me', null, null, null, null, null, testPage);
        ConnectApi.ChatterFeeds.setTestGetFeedElementsUpdatedSince(null, ConnectApi.FeedType.Files, 'me', null, null, null, null, null, testPage);
        ConnectApi.ChatterFeeds.setTestGetFeedElementsFromFeed(null, ConnectApi.FeedType.Home, testPage);
        ConnectApi.ChatterFeeds.setTestGetFeedElementsFromFeed(null, ConnectApi.FeedType.Home, null, null, null, null, null, ConnectApi.FeedFilter.SolvedQuestions, testPage);
        ConnectApi.ChatterFeeds.setTestGetFeedElementsFromFeed(null, ConnectApi.FeedType.News, 'me', null, null, null, null, ConnectApi.FeedSortOrder.LastModifiedDateDesc, ConnectApi.FeedFilter.AllQuestions, testPage);
        ConnectApi.ChatterFeeds.setTestGetFeedElementsFromFeed(Network.getNetworkId(), ConnectApi.FeedType.Bookmarks, 'me', testPage);
        
        ConnectApi.Topics.setTestGetTrendingTopics(null, testTopicPage);
        
        ContextUserController cuc = New ContextUserController();
        cuc.doNewsRefreshToken();
        cuc.getFeedElementsFromFeedNews();
        cuc.getFeedElementsFromFeedHome();
        cuc.getFeedElementsFromFeedSolvedQuestions();
        cuc.getFeedElementsFromFeedBookmarks();
        cuc.getUser();
        cuc.getUnreadCount();
        cuc.doIsNewsModified();
        cuc.getConversations();
        cuc.getTrendingTopics();
        cuc.getMyFollowingPlots();
        cuc.getPlotsForMyConstructions();
        cuc.getFollowingsDesigns();
        
        ConnectApi.UserPage testUserPage = new ConnectApi.UserPage();
        List<ConnectApi.UserDetail> testUserList = new List<ConnectApi.UserDetail>();
        testUserList.add(new ConnectApi.UserDetail());
        testUserPage.users = testUserList;
        
        ConnectApi.ChatterGroupPage testGroupPage = new ConnectApi.ChatterGroupPage();
        List<ConnectApi.ChatterGroupDetail> testGroupList = new List<ConnectApi.ChatterGroupDetail>();
        testGroupList.add(new ConnectApi.ChatterGroupDetail());
        testGroupPage.groups = testGroupList;
        ConnectApi.ChatterGroups.setTestSearchGroups(null, '"Porady Ekspertów"', null, 1, testGroupPage);
        
        ConnectApi.ChatterUsers.setTestSearchUsers(null, 'test*', null, 5, testUserPage);
        ConnectApi.ChatterFeeds.setTestSearchFeedElements(null, 'test*', null, 5, ConnectApi.FeedSortOrder.LastModifiedDateDesc, testPage);
        ConnectApi.ChatterFeeds.setTestSearchFeedElementsInFeed(null, ConnectApi.FeedType.Record, 'me', null, 5, ConnectApi.FeedSortOrder.LastModifiedDateDesc, 'test*', testPage);
        ConnectApi.ChatterFeeds.setTestSearchFeedElements(null, '"Porady Ekspertów" test*', null, 5, ConnectApi.FeedSortOrder.LastModifiedDateDesc, testPage);

        cuc = New ContextUserController();
        PageReference pageRef = new PageReference('http://extradom.pl/');
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('q', 'test');
        
        cuc.getTopics();
        cuc.getSearchUsers();
        cuc.getSearchFeedElements();
        cuc.getSearchFeedElementsInExpertGroupFeed();
        cuc.getLatestAllNetworksPosts();
        cuc.getFeaturedFeedElement();
        cuc.getSavedDesignSets();
        
        ConnectApi.FeedElement fi = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), 'me', ConnectApi.FeedElementType.FeedItem, 'test');
        ConnectApi.ChatterFeeds.setTestSearchFeedElementsInFeed(Network.getNetworkId(), ConnectApi.FeedType.Record, fi.parent.id, null, 3, ConnectApi.FeedSortOrder.LastModifiedDateDesc, null, testPage);
        
        pageRef = new PageReference('http://extradom.pl/');
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', fi.id);
        
        FeedElementViewController controller = New FeedElementViewController();
        controller.getFeedElement();
        controller.getCommentsForFeedElement();
        controller.getSimiliarFeedElements();
        controller.doBookmark();
        
        ConnectApi.ChatterFeeds.setTestGetFeedElementsFromFeed(Network.getNetworkId(), ConnectApi.FeedType.UserProfile, 'me', null, null, null, testPage);
        
        ExtradomSummaryController extradom_summary_controller = new ExtradomSummaryController();
        
        pageRef = new PageReference('http://extradom.pl/');
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('type', 'userprofile');
        ApexPages.currentPage().getParameters().put('subjectId', 'me');
        
        FeedViewController fv_controller = New FeedViewController();
        fv_controller.getFeedElementsFromFeed();
        
        ApexPages.currentPage().getParameters().put('type', 'qa-all');
        fv_controller.getFeedElementsFromFeed();
        
        ApexPages.currentPage().getParameters().put('type', 'news');
        fv_controller.getFeedElementsFromFeed();
        
        ConnectApi.FeedElement element = FeedExtension.postUpdate('me', 'test');
        element = FeedExtension.postQuestion('me', 'title', 'test');
        FeedExtension.updateFeedElementBookmarks(element.id, true);
        FeedExtension.reassignTopics(element.id, null);
        ConnectApi.ChatterLike fi_like = FeedExtension.likeFeedElement(element.id);
        FeedExtension.unlikeFeedElement(fi_like.id);
        ConnectApi.Comment comment = FeedExtension.postCommentToFeedElement(element.id, 'test');
        
        extradom_summary_controller.getLiveUsers();
        extradom_summary_controller.getLastCommentedFeedElement();
        extradom_summary_controller.getCountTodaysComments();
        extradom_summary_controller.getCountTodaysNewMembers();
        extradom_summary_controller.getCountTodaysPosts();
        extradom_summary_controller.getCountTodaysUniqueMembers();
        extradom_summary_controller.getCountTotalVerifiedMembers();
        //extradom_summary_controller.getCountTotalLocalizedConstructions();
        
        FeedExtension.bestAnswer(element.id, comment.id);
        FeedExtension.removeBestAnswer(element.id);
        ConnectApi.ChatterLike comment_like = FeedExtension.likeComment(comment.id);
        FeedExtension.unlikeComment(comment_like.id);
        FeedExtension.deleteComment(comment.id);
        
        FeedExtension feedext = new FeedExtension();
        feedext.commentElementId = element.id;
        feedext.commentImageData = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC0AAAAtCAMAAAANxBKoAAACEFBMVEU/g63M4fBTodJRoNHO4/FSoNDV6/nb8f/e8/9Sn89Mm8vd8/9Onc3L4e/Y7fvU6fdNnMza7/3P5vXX7ftBha+82evM4e/L4O7S5/Xc8f/Z7/1Qnc1Pns7J4fFWo9JWotFUodK61eZUotJXoM3T6fe13PCixdtbpdRNmMfR5fNJmMjR5/W62/DB4/R9ut6MttG71+tUodFQmslXo9N+ut1LmcpQn89Rj7VEk8NGlcVLi7JSodHJ4O+p1eyOvt2XyOVxpMSHs89ytNqczedcpdFkqtbF5vXK3+1kptBho86u0OWozOR3qMdNmcmix95OjbSx2e9JirKJvd5ZlLicwdhWkreYxOB+tNeQwN+Tw+GRudOz1exFh7DP6vrE3OtapdNfqNWcxuHL6Pik0urU7/ptosNgmLyUvNaRxeRKlsVtqtJroMLH3u+jy+Z9ttprqdGry9+51eeFsc1Vncit1e1oq9SLw+KqzOLO6fmXvth3tdqBsM274PKw0uimyeDM5PNNjLOs0OhSnMuexNzG5faVyeVfp9S31eqcyOXg9f97uN3Q6Pddl7qfz+nJ4/Ou1e7O5fNIlseJtM9HiLHR7PqWvNWDsc5hpdCx1e1ep9HJ3/GFs8+tzeG/2+7O5fVYpdO/2eqJttLE5PVHlsZuq9JWnchel7palbmLt9JKmcnF3e1hqdVgm71ordVSm8iXyefxLSx3AAAB6klEQVR4Xu3K07ckMRDA4UqbY/PSNpa2bdu2bds2/sVN+sztdE/6nj3ztA+7v5eqVD5YVUp/Xf/XEw6OOD6xufpGS+2f9dhqXVF0nHJl/biLxfqVq9p1ui4PllTut7i/3fpbs5LEjHr9QueQevXbZNCdrP9ycuik5WbLqeKC8lWHcOou2ceWWjuEPp/yeSRvzlGds+sKhklTH56TyMy++XyTTN+KvE2obt/vk3AzAeAZni97AWaFyCV11EOLhyXSBojAAJ4nIRKBQ0SHR+VZPT7sJx3D+juebYCLkot0S2T1D6mKlN0Gn6Jk2TTn3hHr4m9idf6AlPCuqkm1dUWh9if+Ru8Sj9VBRHX6Wr13icusrkgvqxdwrRvHPB9Oltc106/vIUvjTg+dmRsi6CMARAYW9C4H3HyiQ5Uiq8XKUwJCqHz0aSg0b6+JD8KOAMdqLrBEiFmV1yy92/pg90rrgW4/Um0DZXbq2RcoxiQs1jibODSXnpyNxYtCZ2aoZYwmidrWp/FJrmInOtKcQ3OO+O4pURdHa+oM3gEsTblWN3K7WaBxdOn9HYqpptxo6Fg07atpmqht4a7+7gzBNBjmjs9oDf19Pz/s29L37ovRU/Rra+p7jICmaQEjwxPAajaeSrcuJeBL6d/QvwGwr5cCcDmEjwAAAABJRU5ErkJggg==';
        feedext.commentContentType = 'image/png';
        feedext.commentFilename = 'filename';
        feedext.commentDescription = 'test';
        feedext.submitCommentImage();
        feedext.getMyDesign();
        
        FeedExtension.deleteFeedElement(element.id);
        FeedExtension.deleteMyProfilePhoto();
        
        LiveExtension.unreadRecalc();
        LiveExtension.verifyPhone('', false);
        LiveExtension.verifyRequestCode('');
        LiveExtension.cancelVerification();
        LiveExtension.updateGaClientId('test');
        
        ChatterMethods.topUserProfileContentPostIds(UserInfo.getUserId());
        ChatterMethods.countUserProfileContentPosts(UserInfo.getUserId());
        
        Construction__c c = new Construction__c(Stage__c='Paperwork', Location__latitude__s=0.00, Location__longitude__s=0.00, Location_Visibility__c='Region');
        insert c;
        
        Construction__c c2 = [SELECT Stage__c, Location__latitude__s, Location__longitude__s, Location_Visibility__c, Plot_Name__c FROM Construction__c WHERE Id = :c.Id LIMIT 1];
        
        System.assertEquals(c2.Stage__c, 'Paperwork');
        System.assertEquals(c2.Location__latitude__s, 0.00);
        System.assertEquals(c2.Location__longitude__s, 0.00);
        System.assertEquals(c2.Location_Visibility__c, 'Region');
        
        SettingsViewController settingsViewController = new SettingsViewController();
        settingsViewController.quicksave();
        
        DesignSearchExtension dsext = new DesignSearchExtension();
        dsext.getMyFollowingDesigns();
        dsext.getMyFollowingDesignsCompare();
        
    }
}