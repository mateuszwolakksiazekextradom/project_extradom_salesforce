public class MapTools {

    
    public static Id plotIdForGivenPoint(List<Decimal> point, String state, String region, String city) {
    
        Decimal tercNumber;
        try {
            TERC__c terc = Terc__c.getInstance(state.left(5)+','+region.left(15).toLowerCase());
            System.debug(terc);
            System.debug(state.left(5)+','+region.left(15));
            tercNumber = terc.TERC__c;
        } catch (Exception e) {
            return null;
        }
        AWSConnector aws = new AWSConnector('AKIAJO25LHDRMTH7SLBA','zjzdMrMfTi3i26wIqHEKAwvZkjFBN0qJmGE4sH8n');
        aws.region = 'eu-west-1';
        aws.service = 'dynamodb';
        Map<String,String> headers = new Map<String,String>{};
        headers.put('x-amz-target','DynamoDB_20120810.Query');
        String body = '{ "TableName": "Plot", "IndexName": "TERC-Lat-key-index", "ExpressionAttributeNames": { "#TERC": "TERC", "#Name": "Name", "#Lon": "Lon", "#Lat": "Lat", "#Area": "Area", "#Polygon": "Polygon" }, "ProjectionExpression": "#Name, #Area, #Polygon, #Lat, #Lon", "KeyConditionExpression": "#TERC = :terc AND #Lat BETWEEN :lat_min AND :lat_max", "FilterExpression": "#Lon BETWEEN :lon_min AND :lon_max", "ExpressionAttributeValues": { ":terc": {"N": "'+tercNumber.intValue()+'"}, ":lat_min": {"N": "'+(point[0]-0.0017)+'"}, ":lat_max": {"N": "'+(point[0]+0.0017)+'"}, ":lon_min": {"N": "'+(point[1]-0.0017)+'"}, ":lon_max": {"N": "'+(point[1]+0.0017)+'"} } }';
        System.debug(body);
        HttpRequest req = aws.signedRequestForDynamoDB('POST', new Url('https://dynamodb.eu-west-1.amazonaws.com/'), headers, Blob.valueOf(body));
        Http http = new Http();
        HttpResponse res = http.send(req);
        try {
            Map<String, Object> result = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
            System.debug(res.getBody());
            for (Object plotResult : (List<Object>)result.get('Items')) {
                Object plotNameObject = ((Map<String, Object>)plotResult).get('Name');
                String plotName = (String)((Map<String, Object>)plotNameObject).get('S');
                Object polygonObject = ((Map<String, Object>)plotResult).get('Polygon');
                String polygonString = (String)((Map<String, Object>)polygonObject).get('S');
                List<List<Decimal>> polygon = new List<List<Decimal>>{};
                for (String polygonPointString : polygonString.split(';')) {
                    polygon.add(new List<Decimal>{Decimal.valueOf(polygonPointString.split(',')[0]), Decimal.valueOf(polygonPointString.split(',')[1])});
                }
                System.debug(plotName);
                if (isPointInsidePolygon(point, polygon)) {
                    for (Plot__c plot : [SELECT Id FROM Plot__c WHERE ID_Plot__c = :plotName LIMIT 1]) {
                        return plot.Id;
                    }
                    Object pointLatObject = ((Map<String, Object>)plotResult).get('Lat');
                    Decimal pointLat = Decimal.valueOf((String)((Map<String, Object>)pointLatObject).get('N'));
                    Object pointLonObject = ((Map<String, Object>)plotResult).get('Lon');
                    Decimal pointLon = Decimal.valueOf((String)((Map<String, Object>)pointLonObject).get('N'));
                    Object areaObject = ((Map<String, Object>)plotResult).get('Area');
                    Decimal area = Decimal.valueOf((String)((Map<String, Object>)areaObject).get('N'));
                    Plot__c plot = new Plot__c(Name=plotName, ID_Plot__c=plotName, Geometry_building_MAIL__c=polygonString.replaceAll(';','|'), Center__latitude__s=pointLat, Center__longitude__s=pointLon, Area__c=area);
                    String largestLocationQuery = 'SELECT Id, Name, Population__c FROM Location__c WHERE RecordType.Name = \'City\' AND Population__c > 1000 AND DISTANCE(Location__c, GEOLOCATION('+pointLat+', '+pointLon+'), \'km\') < 50 ORDER BY Population__c DESC LIMIT 1';
                    String city1 = '';
                    String city2 = '';
                    for (Location__c l : (List<Location__c>)Database.query(largestLocationQuery)) {
                        city1 = l.Name;
                    }
                    String closestLocationQuery = 'SELECT Id, Name, Population__c FROM Location__c WHERE RecordType.Name = \'City\' AND Population__c > 1000 AND DISTANCE(Location__c, GEOLOCATION('+pointLat+', '+pointLon+'), \'km\') < 50 ORDER BY DISTANCE(Location__c, GEOLOCATION('+pointLat+', '+pointLon+'), \'km\') LIMIT 2';
                    for (Location__c l : (List<Location__c>)Database.query(closestLocationQuery)) {
                        System.debug(l.Name);
                        if (l.Name!=city1) {
                            city2 = l.Name;
                            break;
                        }
                    }
                    for (Location__c l : [SELECT Id, Wind_Zone_Est__c, Snow_Zone_Est__c, Frost_Zone_Est__c FROM Location__c WHERE RecordType.Name = 'City' AND Name = :city AND Parent_Location__r.Parent_Location__r.Name = :region AND Parent_Location__r.Parent_Location__r.Parent_Location__r.Name = :state LIMIT 1]) {
                        plot.Wind_zone__c = l.Wind_Zone_Est__c;
                        plot.Frost_zone__c = l.Frost_Zone_Est__c;
                        plot.Snow_zone__c = l.Snow_Zone_Est__c;
                        plot.Location__c = l.Id;
                    }
                    plot.Near_City1__c = city1;
                    plot.Near_City2__c = city2;
                    insert plot;
                    return plot.Id;
                }
            }
        } catch (Exception e) {
        }
        return null;
        
    }
    
    public static Plot__c draftPlotForGivenPoint(List<Decimal> point, String state, String region) {
    
        Decimal tercNumber;
        try {
            TERC__c terc = Terc__c.getInstance(state.left(5)+','+region.left(15).toLowerCase());
            System.debug(terc);
            System.debug(state.left(5)+','+region.left(15));
            tercNumber = terc.TERC__c;
        } catch (Exception e) {
            return null;
        }
        AWSConnector aws = new AWSConnector('AKIAJO25LHDRMTH7SLBA','zjzdMrMfTi3i26wIqHEKAwvZkjFBN0qJmGE4sH8n');
        aws.region = 'eu-west-1';
        aws.service = 'dynamodb';
        Map<String,String> headers = new Map<String,String>{};
        headers.put('x-amz-target','DynamoDB_20120810.Query');
        String body = '{ "TableName": "Plot", "IndexName": "TERC-Lat-key-index", "ExpressionAttributeNames": { "#TERC": "TERC", "#Name": "Name", "#Lon": "Lon", "#Lat": "Lat", "#Area": "Area", "#Polygon": "Polygon" }, "ProjectionExpression": "#Name, #Area, #Polygon, #Lat, #Lon", "KeyConditionExpression": "#TERC = :terc AND #Lat BETWEEN :lat_min AND :lat_max", "FilterExpression": "#Lon BETWEEN :lon_min AND :lon_max", "ExpressionAttributeValues": { ":terc": {"N": "'+tercNumber.intValue()+'"}, ":lat_min": {"N": "'+(point[0]-0.0017)+'"}, ":lat_max": {"N": "'+(point[0]+0.0017)+'"}, ":lon_min": {"N": "'+(point[1]-0.0017)+'"}, ":lon_max": {"N": "'+(point[1]+0.0017)+'"} } }';
        System.debug(body);
        HttpRequest req = aws.signedRequestForDynamoDB('POST', new Url('https://dynamodb.eu-west-1.amazonaws.com/'), headers, Blob.valueOf(body));
        Http http = new Http();
        HttpResponse res = http.send(req);
        try {
            Map<String, Object> result = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
            System.debug(res.getBody());
            for (Object plotResult : (List<Object>)result.get('Items')) {
                Object plotNameObject = ((Map<String, Object>)plotResult).get('Name');
                String plotName = (String)((Map<String, Object>)plotNameObject).get('S');
                Object polygonObject = ((Map<String, Object>)plotResult).get('Polygon');
                String polygonString = (String)((Map<String, Object>)polygonObject).get('S');
                List<List<Decimal>> polygon = new List<List<Decimal>>{};
                for (String polygonPointString : polygonString.split(';')) {
                    polygon.add(new List<Decimal>{Decimal.valueOf(polygonPointString.split(',')[0]), Decimal.valueOf(polygonPointString.split(',')[1])});
                }
                System.debug(plotName);
                if (isPointInsidePolygon(point, polygon)) {
                    for (Plot__c plot : [SELECT Id, Name, ID_Plot__c, Geometry_building_MAIL__c, Center__latitude__s, Center__longitude__s, Area__c FROM Plot__c WHERE ID_Plot__c = :plotName LIMIT 1]) {
                        return plot;
                    }
                    Object pointLatObject = ((Map<String, Object>)plotResult).get('Lat');
                    Decimal pointLat = Decimal.valueOf((String)((Map<String, Object>)pointLatObject).get('N'));
                    Object pointLonObject = ((Map<String, Object>)plotResult).get('Lon');
                    Decimal pointLon = Decimal.valueOf((String)((Map<String, Object>)pointLonObject).get('N'));
                    Object areaObject = ((Map<String, Object>)plotResult).get('Area');
                    Decimal area = Decimal.valueOf((String)((Map<String, Object>)areaObject).get('N'));
                    Plot__c plot = new Plot__c(Name=plotName, ID_Plot__c=plotName, Geometry_building_MAIL__c=polygonString.replaceAll(';','|'), Center__latitude__s=pointLat, Center__longitude__s=pointLon, Area__c=area);
                    return plot;
                }
            }
        } catch (Exception e) {
        }
        return null;
        
    }
    
    public static Boolean isPointInsidePolygon(List<Decimal> point, List<List<Decimal>> vs) {
        Decimal x = point[0];
        Decimal y = point[1];
        Boolean inside = false;
        for (Integer i = 0, j = vs.size()-1; i<vs.size(); j=i++) {
            Decimal xi = vs[i][0];
            Decimal yi = vs[i][1];
            Decimal xj = vs[j][0];
            Decimal yj = vs[j][1];
            Boolean intersect = ((yi > y) != (yj > y)) && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
            if (intersect) inside = !inside;
        }
        return inside;
    }
    
    public static Id getLocationIdByTERC(String TERC) {
        for (Location__c l : [SELECT Id FROM Location__c WHERE TERC__c = :TERC LIMIT 1]) {
            return l.Id;
        }
        return null;
    }
    
}