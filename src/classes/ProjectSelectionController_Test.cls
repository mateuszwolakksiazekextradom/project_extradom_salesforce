@IsTest
private class ProjectSelectionController_Test {

    @IsTest
    private static void shouldReturnDesignsByName() {
        // given
        Supplier__c newSupplier = new Supplier__c(Name = 'Supplier', Code__c = '11', Full_Name__c = 'Great Supplier');
        insert newSupplier;
        Design__c design1 = new Design__c (Name = 'First Design', Supplier__c = newSupplier.Id, Full_Name__c = 'First Design');
        Design__c design2 = new Design__c (Name = 'Second Design', Supplier__c = newSupplier.Id, Full_Name__c = 'Second Design');
        insert new List<Design__c> {design1, design2};

        // when
        List<Design__c> foundDesigns = ProjectSelectionController.getDesigns('First');

        // then
        System.assertEquals(1, foundDesigns.size());
        System.assertEquals(design1.Id, foundDesigns.get(0).Id);
    }
}