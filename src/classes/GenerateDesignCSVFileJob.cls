public with sharing class GenerateDesignCSVFileJob implements Schedulable {
    
    private static final String PRIMARY_FEED_HEADER = 'id;level;usableArea;garage;roof;construction;livingtype;basement;style;plotsize;rooms;roofslope;eco;shape;garagelocation;ridge;ceiling;footprint;height;iDesigner;Panorama;3D;interiorPhotos;features\n';
    private static final String PRIMARY_FEED_FILENAME = 'design-features.csv';
    
    public void execute(SchedulableContext sc) {
        savePrimaryFeed();
    }   
    
    @Future
    public static void savePrimaryFeed() {
        List<Document> documents = [SELECT Id FROM Document WHERE Name = :PRIMARY_FEED_FILENAME];
        transient String s = PRIMARY_FEED_HEADER;
        for (List<Design__c> designs : [
                SELECT Code__c, Level__c, Attic__c, Adaptable_Attic__c , Type__c, Usable_Area__c,
                        Garage__c, Carport__c, Construction__c, Roof__c, Living_Type__c, Basement__c,
                        Minimum_Plot_Size_Horizontal__c, Count_Rooms__c, Roof_slope__c,
                        eco_84__c, eco_83__c, Shape__c, Garage_Location__c, Ridge__c,
                        Ceiling__c, Footprint__c, Height__c, Has_iDesigner__c, Has_Panorama__c,
                        Sketchfab_ID__c, Count_Interior_Media__c, eaves_124__c, entrance_45__c,
                        veranda_68__c, pool_71__c, wintergarden_76__c, terrace_69__c, mezzanine_94__c,
                        cost_102__c, border_118__c, scarp_121__c, Style__c, Name,
                        fireplace_123__c, oriel_64__c, bullseye_122__c
                FROM Design__c WHERE Status__c = 'Active' AND Gross_Price__c > 0
            ]) {
            for (Design__c d : designs) {
                s += d.Code__c + ';' + (d.Level__c > 1 ? 'piętrowe' : (d.Level__c == 1 ? (d.Attic__c == 1 ? 'z poddaszem użytkowym' : (d.Adaptable_Attic__c == 1 ? (d.Type__c == 'Dom' ? 'z poddaszem do adaptacji,parterowe' : 'z poddaszem użytkowym') : 'parterowe')) : '')) + ';'  
                    + (d.Usable_Area__c == null ? '' : String.valueOf(d.Usable_Area__c)) + ';' + String.valueOf(d.Garage__c+d.Carport__c) + ';' + (d.Roof__c == 'czterospadowy' ? 'czterospadowy,wielospadowy' : (d.Roof__c == 'dwuspadowy' ? 'dwuspadowy' : (d.Roof__c == 'jednospadowy' ? 'jednospadowy' : (d.Roof__c == 'kopertowy' ? 'kopertowy,namiotowy,naczółkowy' : (d.Roof__c == 'mansardowy' ? 'mansardowy' : (d.Roof__c == 'naczółkowy' ? 'naczółkowy' : (d.Roof__c == 'namiotowy' ? 'namiotowy,czterospadowy,wielospadowy' : (d.Roof__c == 'płaski' ? 'płaski' : (d.Roof__c == 'wielospadowy' ? 'wielospadowy' : ''))))))))) + ';'
                    + (d.Construction__c == 'murowana' ? 'murowane' : (d.Construction__c == 'szkieletowa' ? 'szkieletowe,drewniane' : (d.Construction__c == 'z bali' ? 'z bali,drewniane' : (d.Construction__c == 'blaszana' ? 'blaszane' : (d.Construction__c == 'stalowa' ? 'stalowe' : ''))))) + ';'
                    + d.Living_Type__c + ';' + (d.Basement__c == 1 ? 'z piwnicą' : (d.Basement__c == 2 ? 'z piwnicą,z sutereną' : 'bez piwnicy')) + ';'
                    + ((d.Style__c == 'dworkowy' ? 'dworkowy' : (d.Style__c == 'góralski' ? 'góralski' : (d.Style__c == 'nowoczesny' ? 'nowoczesne' : (d.Style__c == 'tradycyjny' ? 'tradycyjne' : (d.Style__c == 'mur pruski / szachulcowy' ? 'mur pruski / szachulec' : ''))))) + ((d.Name.contains('Willa') || (d.Type__c == 'Dom' && d.Usable_Area__c != null && d.Usable_Area__c >=200 && d.Footprint__c != null && d.Footprint__c <= 250)) ? ',willa' : '') + ((d.Name.contains('ezydencja') || (d.Type__c == 'Dom' && d.Usable_Area__c != null && d.Usable_Area__c >= 250)) ? ',rezydencja' : '')).removeStart(',') + ';'
                    + (d.Minimum_Plot_Size_Horizontal__c == null ? '' : String.valueOf(d.Minimum_Plot_Size_Horizontal__c)) + ';' + String.valueOf(d.Count_Rooms__c) + ';' + (d.Roof_slope__c == null ? '' : String.valueOf(d.Roof_slope__c)) + ';'
                    + (d.eco_84__c == 1 ? 'pasywne,energooszczędne' : (d.eco_83__c == 1 ? 'energooszczędne' : '')) + ';' + (d.Shape__c == 'litera L' ? 'litera L' : (d.Shape__c == 'z atrium' ? 'z atrium' : (d.Shape__c == 'nietypowy' ? 'nietypowy' : ''))) + ';'
                    + (d.Garage_Location__c == 'w bryle budynku' ? 'w bryle budynku' : (d.Garage_Location__c == 'wysunięty do przodu' ? 'wysunięty do przodu' : (d.Garage_Location__c == 'w linii budynku' ? 'w linii budynku' : (d.Garage_Location__c == 'cofnięty' ? 'cofnięty' : (d.Garage_Location__c == 'wolnostojący' ? 'wolnostojący' : ''))))) + ';'
                    + (d.Ridge__c == 'równoległa do drogi' ? 'równoległa do drogi' : (d.Ridge__c == 'prostopadła do drogi' ? 'prostopadła do drogi' : '')) + ';'
                    + (d.Ceiling__c == 'drewniany' ? 'drewniany' : (d.Ceiling__c == 'gęstożebrowy' ? 'gęstożebrowy' : (d.Ceiling__c == 'żelbetowy' ? 'żelbetowy / monolityczny' : (d.Ceiling__c == 'wiązary' ? 'wiązary' : '')))) + ';'
                    + (d.Footprint__c == null ? '' : String.valueOf(d.Footprint__c)) + ';'
                    + (d.Height__c == null ? '' : String.valueOf(d.Height__c)) + ';'
                    + (d.Has_iDesigner__c ? '1' : '0') + ';'
                    + (d.Has_Panorama__c ? '1' : '0') + ';'
                    + (d.Sketchfab_ID__c == null ? '0' : '1') + ';'
                    + (d.Count_Interior_Media__c>0 ? '1' : '0') + ';'
                    + ((d.eaves_124__c == 1 ? 'bez okapu,' : '') + (d.entrance_45__c == 1 ? 'wejście od południa,' : '') + (d.veranda_68__c == 1 ? 'weranda,' : '')
                         + (d.pool_71__c == 1 ? 'basen,' : '') + (d.wintergarden_76__c == 1 ? 'ogród zimowy,' : '') + (d.terrace_69__c == 1 ? 'taras,' : '') 
                         + (d.fireplace_123__c == 1 ? 'kominek,' : '') + (d.oriel_64__c == 1 ? 'wykusz,' : '') + (d.bullseye_122__c == 1 ? 'wole oko,' : '')
                         + (d.mezzanine_94__c == 1 ? 'antresola,' : '') + (d.cost_102__c == 1 ? 'tani w budowie,' : '') + (d.border_118__c == 1 ? 'na granicy działki,' : '')
                         + (d.scarp_121__c == 1 ? 'na skarpę / stok,' : '')).removeEnd(',')+ ';'
                    + '\n';
            }
        }
        if (documents.size() > 0) {
            Document d = new Document (Id = documents.get(0).Id, Body = Blob.valueOf(s));
            update d;
        }
    }
    
    public class GenerateDesignCSVFileJobException extends Exception {
        
    }
}