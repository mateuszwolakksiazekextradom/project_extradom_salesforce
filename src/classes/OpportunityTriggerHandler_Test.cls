@IsTest
private class OpportunityTriggerHandler_Test {
    
    @IsTest
    private static void shouldCloseFollowUpCallTasks_whenClosedLost() {
        //given
        Account acc = new Account(Name = 'Test');
        insert acc;
        Opportunity opp = new Opportunity(AccountId = acc.Id, Name = 'Test', CloseDate = system.today(), StageName = ConstUtils.OPPORTUNITY_SOLUTION_PROPOSAL_STAGE);
        insert opp;
        Task tsk = new Task (Subject = 'Task1', Type = ConstUtils.TASK_FOLLOW_UP_TYPE, Status = ConstUtils.TASK_NOT_STARTED_STATUS, WhatId = acc.Id);
        insert tsk;
        
        //when
        opp.StageName = ConstUtils.OPPORTUNITY_CLOSED_DUPLICATE_STAGE;
        update opp;
        tsk = [SELECT Id, Status FROM Task WHERE Id = :tsk.Id];
        
        //then
        System.assertEquals(ConstUtils.TASK_COMPLETED_STATUS,tsk.Status);
    }

}