@RestResource(urlMapping='/estimatesrequest')
global class EstimatesRequestRest {
    @HttpPost
    global static Map<String,String> doPost(String email, String phone, String code, String comments, Boolean optInRegulations, Boolean optInPrivacyPolicy, Boolean optInMarketing, Boolean optInMarketingExtended, Boolean optInMarketingPartner, Boolean optInLeadForward, String gclid, String hasPlot, String hasMpzp, String constructionPlan, String hasApp, Boolean optInEmail, Boolean optInPhone, Boolean optInProfiling, Boolean optInPKO)  {
        Map<String,Id> data = OrderMethods.getWebLeadContactDetails('Estimates', '', email, email, phone, 'Internal', false, false, 'No',hasPlot, hasMpzp, constructionPlan, hasApp);
        if (String.isNotBlank(gclid)) {
            OrderMethods.updateGclid(data.get('accountId'), gclid);
        }
        RestRequest req = RestContext.request;
        if (req.headers.containsKey('gaClientId')) {
            OrderMethods.updateGaClientId(data.get('accountId'), req.headers.get('gaClientId'));
        }
        OrderMethods.updateOptIns(email, phone, optInRegulations, optInPrivacyPolicy, optInMarketing, optInMarketingExtended, optInMarketingPartner, optInLeadForward, optInEmail, optInPhone, optInProfiling, optInPKO);
        OrderMethods.markSalesReadyFromResources(data.get('accountId'), constructionPlan);
        Design__c d = [SELECT Id, Estimates_html__c, Documents_estimates__c, Construction_Pricing_Estimate__c, Supplier_Code__c FROM Design__c WHERE Code__c != '' AND Code__c = :code];
        Boolean hasHtml = string.isBlank(d.Estimates_html__c) ? false : true;
        Boolean hasDoc = string.isBlank(d.Documents_estimates__c) ? false : true;
        String status = 'Not Started';
        
        If (string.isBlank(comments) && (hasDoc || hasHtml)) {
            status = 'Completed';
        }
        
        Task t = new Task(subject='Zamówienie kosztorysu', Description=comments, Type='Estimates Request', WhoId=data.get('contactEmailId'), WhatId=d.Id, OwnerId=data.get('ownerId'), ActivityDate=System.Today(), Status=status, Design_Has_Estimates_Html__c=hasHtml);
        insert t;
        Map<String,String> result = new Map<String,String>{'result' => 'Dziękujemy za zainteresowanie. Kosztorys zostanie przesłany na podany adres poczty e-mail.'};
        return result;
    }
    global class OverLimitOrderException extends Exception {}
}