global with sharing class FeedExtension {

    public FeedExtension(GroupViewController controller) { }
    public FeedExtension(QAViewController controller) { }
    
    public FeedExtension() { }


    public String subjectId { get; set; }
    

    public Blob fileBody { get; set; }
    
    public String filename { get; set; }
    public String contentType { get; set; }
    public string imageData { get; set; }
    public string description { get; set; }
    public string fileUploaderId { get; set; }
    public string elementId { get; set; }
    public String contentSubjectId { get; set; }
    
    public String commentFilename { get; set; }
    public String commentContentType { get; set; }
    public string commentImageData { get; set; }
    public string commentDescription { get; set; }
    public string commentId { get; set; }
    public string commentElementId { get; set; }
    
    public String username { get; set; }
    public String password { get; set; }

    public PageReference submitImage() {
        ConnectApi.FeedItemInput input = new ConnectApi.FeedItemInput();
        
        ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
        ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
        messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
        textSegmentInput.text = this.description;
        messageBodyInput.messageSegments.add(textSegmentInput);
        input.body = messageBodyInput;
        
        input.subjectId = this.contentSubjectId;
        ConnectApi.ContentCapabilityInput contentInput = new ConnectApi.ContentCapabilityInput();
        contentInput.title = this.fileName;
        ConnectApi.FeedElementCapabilitiesInput capabilities = new ConnectApi.FeedElementCapabilitiesInput();
        capabilities.content = contentInput;
        input.capabilities = capabilities;
        ConnectApi.BinaryInput binInput = new ConnectApi.BinaryInput(EncodingUtil.base64Decode(this.imageData.substring(this.imageData.indexOf('base64,')+1+6)), this.contentType, this.fileName);
        ConnectApi.FeedElement element = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), input, binInput);
        if (this.fileName=='kreator3d.png') {
            ConnectApi.Topics.assignTopicByName(Network.getNetworkId(), element.id, 'Kreator 3D');
        }
        //AWSTools.uploadFileBase64('extradom.media', UserInfo.getOrganizationId()+'-'+element.capabilities.content.id+'/source', this.imageData.substring(this.imageData.indexOf('base64,')+1+6), this.contentType, 'AKIAJO25LHDRMTH7SLBA', 'zjzdMrMfTi3i26wIqHEKAwvZkjFBN0qJmGE4sH8n');
        this.elementId = element.id;
        this.fileBody = null;
        this.filename = null;
        this.contentType = null;
        this.imageData = null;
        this.contentSubjectId = null;
        return null;
    }
    
    public PageReference submitCommentImage() {
        ConnectApi.CommentInput commentInput = new ConnectApi.CommentInput();
        ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
        ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
        textSegmentInput.text = this.commentDescription;
        messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
        messageBodyInput.messageSegments.add(textSegmentInput);
        commentInput.body = messageBodyInput;
        ConnectApi.CommentCapabilitiesInput commentCapabilitiesInput = new ConnectApi.CommentCapabilitiesInput();
        ConnectApi.ContentCapabilityInput contentCapabilityInput = new ConnectApi.ContentCapabilityInput();
        commentCapabilitiesInput.content = contentCapabilityInput;
        contentCapabilityInput.title = this.commentFilename;
        commentInput.capabilities = commentCapabilitiesInput;
        ConnectApi.BinaryInput binInput = new ConnectApi.BinaryInput(EncodingUtil.base64Decode(this.commentImageData.substring(this.commentImageData.indexOf('base64,')+1+6)), this.commentContentType, this.commentFilename);
        ConnectApi.Comment comment = ConnectApi.ChatterFeeds.postCommentToFeedElement(Network.getNetworkId(), this.commentElementId, commentInput, binInput);
        if ((comment.capabilities.content.fileExtension=='jpg' || comment.capabilities.content.fileExtension=='png' || comment.capabilities.content.fileExtension=='jpeg') && this.commentImageData!=null && Network.getNetworkId()!=null) { AWSTools.uploadFileBase64('extradom.media', UserInfo.getOrganizationId()+'-'+comment.capabilities.content.versionId+'/source', this.commentImageData.substring(this.commentImageData.indexOf('base64,')+1+6), (comment.capabilities.content.fileExtension=='jpg' || comment.capabilities.content.fileExtension=='jpeg')?'image/jpeg':'image/png', 'AKIAJO25LHDRMTH7SLBA', 'zjzdMrMfTi3i26wIqHEKAwvZkjFBN0qJmGE4sH8n'); }
        this.commentId = comment.id;
        this.commentFilename = null;
        this.commentContentType = null;
        this.commentImageData = null;
        this.commentElementId = comment.feedElement.id;
        return null;
    }
    
    public Design__c getMyDesign() {
        for (Construction__c c : [SELECT Id, Design__c FROM Construction__c WHERE OwnerId = :UserInfo.getUserId() AND Design__c != null]) {
            return [SELECT Id, Full_Name__c, Thumbnail_Base_URL__c FROM Design__c WHERE Id = :c.Design__c];
        }
        return null;
    }

    public FeedExtension(ContextUserController controller) {

    }


    public FeedExtension(TopicViewController controller) {

    }


    public FeedExtension(UserViewController controller) {

    }


    public FeedExtension(FeedElementViewController controller) {

    }


    public FeedExtension(ConnectApiToolsForVF controller) {

    }


    public FeedExtension(ApexPages.StandardController sc) { }
    
    @RemoteAction
    global static ConnectApi.Photo setMyProfilePhoto(String imageData, String contentType, String filename, Integer cropX, Integer cropY, Integer cropSize) {
        ConnectApi.PhotoInput input = new ConnectApi.PhotoInput();
        input.cropSize = cropSize;
        input.cropX = cropX;
        input.cropY = cropY;
        ConnectApi.BinaryInput binInput = new ConnectApi.BinaryInput(EncodingUtil.base64Decode(imageData.substring(imageData.indexOf('base64,')+1+6)), contentType, filename);
        return ConnectApi.UserProfiles.setPhotoWithAttributes(Network.getNetworkId(), 'me', input, binInput);
    }
    
    @RemoteAction
    global static ConnectApi.Photo deleteMyProfilePhoto() {
        ConnectApi.UserProfiles.deletePhoto(Network.getNetworkId(), 'me');
        return ConnectApi.UserProfiles.getPhoto(Network.getNetworkId(), 'me');
    }

    @RemoteAction
    global static ConnectApi.ChatterLike likeFeedElement(ID elementId) {
        return ConnectApi.ChatterFeeds.likeFeedElement(Network.getNetworkId(), elementId);
    }
    
    @RemoteAction
    global static Void unlikeFeedElement(ID likeId) {
        ConnectApi.ChatterFeeds.deleteLike(Network.getNetworkId(), likeId);
    }
    
    @RemoteAction
    global static ConnectApi.ChatterLike likeComment(ID commentId) {
        return ConnectApi.ChatterFeeds.likeComment(Network.getNetworkId(), commentId);
    }
    
    @RemoteAction
    global static Void unlikeComment(ID likeId) {
        ConnectApi.ChatterFeeds.deleteLike(Network.getNetworkId(), likeId);
    }
    
    @RemoteAction
    global static ConnectApi.QuestionAndAnswersCapability bestAnswer(ID elementId, ID commentId) {
        ConnectApi.QuestionAndAnswersCapabilityInput qaInput = new ConnectApi.QuestionAndAnswersCapabilityInput();
        qaInput.bestAnswerId = commentId;
        return ConnectApi.QuestionAndAnswers.updateQuestionAndAnswers(Network.getNetworkId(), elementId, qaInput);
    }
    
    @RemoteAction
    global static ConnectApi.QuestionAndAnswersCapability removeBestAnswer(ID elementId) {
        ConnectApi.QuestionAndAnswersCapabilityInput qaInput = new ConnectApi.QuestionAndAnswersCapabilityInput();
        qaInput.bestAnswerId = null;
        return ConnectApi.QuestionAndAnswers.updateQuestionAndAnswers(Network.getNetworkId(), elementId, qaInput);
    }
    
    @RemoteAction
    global static ConnectApi.Comment postCommentToFeedElement(ID elementId, String text) {
        return ConnectApi.ChatterFeeds.postCommentToFeedElement(Network.getNetworkId(), elementId, text);
    }
    
    @RemoteAction
    global static String postCommentToFeedElementContent(ID commentId) {
        PageReference pr = Page.SingleCommentView;
        Map<string, string> params = pr.getParameters();
        params.put('id', commentId);
        return pr.getContent().toString();
    }
    
    @RemoteAction
    global static ConnectApi.FeedElement postUpdate(String subjectId, String text) {
        return ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), subjectId, ConnectApi.FeedElementType.FeedItem, text);
    }
    
    @RemoteAction
    global static String getSingleFeedElementViewContent(ID elementId) {
        PageReference pr = Page.SingleFeedElementView;
        Map<string, string> params = pr.getParameters();
        params.put('id', elementId);
        return pr.getContent().toString();
    }
    
    @RemoteAction
    global static String getSingleBestAnswerViewContent(ID elementId) {
        PageReference pr = Page.SingleBestAnswerView;
        Map<string, string> params = pr.getParameters();
        params.put('id', elementId);
        return pr.getContent().toString();
    }
    
    @RemoteAction
    global static Object getMyFiles() {
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(System.URL.getSalesforceBaseUrl().toExternalForm()+'/services/data/v34.0/connect/communities/'+Network.getNetworkId()+'/chatter/users/me/files/');
        req.setMethod('GET');
        req.setHeader('Authorization', 'OAuth ' + UserInfo.getSessionId());
        req.setHeader('Content-Type', 'application/json');
        return JSON.deserializeUntyped(h.send(req).getBody());
    }
    
    @RemoteAction
    global static ConnectApi.FeedElement postQuestion(String subjectId, String title, String text) {
        ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
        feedItemInput.subjectId = subjectId;
        ConnectApi.FeedElementCapabilitiesInput feedElementCapabilitiesInput = new ConnectApi.FeedElementCapabilitiesInput();
        ConnectApi.QuestionAndAnswersCapabilityInput questionAndAnswersCapabilityInput = new ConnectApi.QuestionAndAnswersCapabilityInput();
        ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
        ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
        messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
        textSegmentInput.text = text;
        messageBodyInput.messageSegments.add(textSegmentInput);
        feedItemInput.body = messageBodyInput;
        feedItemInput.capabilities = feedElementCapabilitiesInput;
        feedElementCapabilitiesInput.questionAndAnswers = questionAndAnswersCapabilityInput;
        questionAndAnswersCapabilityInput.questionTitle = title;
        return ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput, null);
    }
    
    @RemoteAction
    global static ConnectApi.ChatterMessage sendMessage(String userId, String text) {
        return ConnectApi.ChatterMessages.sendMessage(Network.getNetworkId(), text, userId);
    }
    
    public PageReference loginInline() {
        return Site.login(this.username, this.password, '');
    }
    
    @RemoteAction
    global static String getSingleMessageViewContent(ID messageId) {
        PageReference pr = Page.SingleMessageView;
        Map<string, string> params = pr.getParameters();
        params.put('id', messageId);
        return pr.getContent().toString();
    }
    
    @RemoteAction
    global static String getSingleCommentsViewContent(ID elementId, String pageParam) {
        PageReference pr = Page.SingleCommentsView;
        Map<string, string> params = pr.getParameters();
        params.put('id', elementId);
        params.put('pageParam', pageParam);
        return pr.getContent().toString();
    }
    
    @RemoteAction
    global static String getSingleFeedElementsViewContent(String type, String subjectId, String pageParam) {
        PageReference pr = Page.SingleFeedElementsView;
        Map<string, string> params = pr.getParameters();
        if (type=='news') {
            params.put('order', 'lastmodifieddatedesc');
        }
        params.put('type', type);
        params.put('subjectId', subjectId);
        params.put('pageParam', pageParam);
        return pr.getContent().toString();
    }
    
    @RemoteAction
    global static String getSingleFeedElementsGalleryViewContent(String type, String subjectId, String pageParam) {
        PageReference pr = Page.SingleFeedElementsView;
        Map<string, string> params = pr.getParameters();
        params.put('type', type);
        params.put('subjectId', subjectId);
        params.put('pageParam', pageParam);
        params.put('role', 'gallery');
        return pr.getContent().toString();
    }
    
    @RemoteAction
    global static String getSingleFeedElementModalViewContent(ID elementId) {
        PageReference pr = Page.SingleFeedElementModalView;
        Map<string, string> params = pr.getParameters();
        params.put('id', elementId);
        return pr.getContent().toString();
    }
    
    @RemoteAction
    global static Void deleteFeedElement(ID elementId) {
        ConnectApi.ChatterFeeds.deleteFeedElement(Network.getNetworkId(), elementId);
    }
    
    @RemoteAction
    global static Void deleteComment(ID commentId) {
        ConnectApi.ChatterFeeds.deleteComment(Network.getNetworkId(), commentId);
    }
    
    @RemoteAction
    global static ConnectApi.BookmarksCapability updateFeedElementBookmarks(ID elementId, Boolean isBookmarked) {
        return ConnectApi.ChatterFeeds.updateFeedElementBookmarks(Network.getNetworkId(), elementId, isBookmarked);
    }
    
    @RemoteAction
    global static String showTopicsEdit(ID elementId) {
        PageReference pr = Page.SingleFeedElementTopicsEditView;
        Map<string, string> params = pr.getParameters();
        params.put('id', elementId);
        return pr.getContent().toString();
    }
    
    @RemoteAction
    global static Void reassignTopics(ID elementId, List<String> topicNames) {
        if (topicNames == null) {
            topicNames = new List<String>{};
        }
        ConnectApi.TopicNamesInput input = new ConnectApi.TopicNamesInput();
        input.topicNames = topicNames;
        ConnectApi.Topics.reassignTopicsByName(Network.getNetworkId(), elementId, input);
    }
    
    @RemoteAction
    global static String showTopics(ID elementId) {
        PageReference pr = Page.SingleFeedElementTopicsView;
        Map<string, string> params = pr.getParameters();
        params.put('id', elementId);
        return pr.getContent().toString();
    }
    
    @RemoteAction
    global static String loadConstructionEdit(ID constructionId) {
        PageReference pr = Page.SingleConstructionEditView;
        Map<string, string> params = pr.getParameters();
        params.put('id', constructionId);
        return pr.getContent().toString();
    }
    
    @RemoteAction
    global static Construction__c editConstruction(ID constructionId, String stage, Decimal location_latitude, Decimal location_longitude, String state, String region, String city, String location_visibility) {
        Construction__c c = [SELECT Stage__c, Location__latitude__s, Location__longitude__s, Location_Visibility__c, Plot_Name__c FROM Construction__c WHERE Id = :constructionId LIMIT 1];
        c.Stage__c = stage;
        c.Location__latitude__s = location_latitude;
        c.Location__longitude__s = location_longitude;
        c.Location_Visibility__c = location_visibility;
        c.Plot__c = MapTools.plotIdForGivenPoint(new List<Decimal>{location_latitude, location_longitude}, state, region, city);
        if (c.Plot__c != null) {
            Plot__c p = [SELECT Id, Name FROM Plot__c WHERE Id = :c.Plot__c LIMIT 1];
            c.Shared_Location__c = MapTools.getLocationIdByTERC(p.Name.left(4));
        }
        update c;
        return c;
    }
    
    @RemoteAction
    global static Construction__c editConstructionWithDesign(ID constructionId, String stage, ID designId, Decimal location_latitude, Decimal location_longitude, String state, String region, String city, String location_visibility) {
        Construction__c c = [SELECT Stage__c, Design__c, Location__latitude__s, Location__longitude__s, Location_Visibility__c, Plot_Name__c FROM Construction__c WHERE Id = :constructionId LIMIT 1];
        c.Stage__c = stage;
        c.Location__latitude__s = location_latitude;
        c.Location__longitude__s = location_longitude;
        c.Location_Visibility__c = location_visibility;
        c.Design__c = designId;
        c.Plot__c = MapTools.plotIdForGivenPoint(new List<Decimal>{location_latitude, location_longitude}, state, region, city);
        if (c.Plot__c != null) {
            Plot__c p = [SELECT Id, Name FROM Plot__c WHERE Id = :c.Plot__c LIMIT 1];
            c.Shared_Location__c = MapTools.getLocationIdByTERC(p.Name.left(4));
        }
        update c;
        return c;
    }
    
    @RemoteAction
    global static String loadConstructionCreate(ID designId) {
        PageReference pr = Page.SingleConstructionCreateView;
        Map<string, string> params = pr.getParameters();
        params.put('id', designId);
        return pr.getContent().toString();
    }
    
    @RemoteAction
    global static Construction__c createConstruction(ID designId, String stage) {
        Construction__c c = new Construction__c();
        c.Stage__c = stage;
        c.Design__c = designId;
        insert c;
        return c;
    }
    
    @RemoteAction
    global static String loadMoreConstructionOwnersForDesign(ID designId) {
        PageReference pr = Page.SingleDesignSocialConstructionOwnersView;
        Map<string, string> params = pr.getParameters();
        params.put('id', designId);
        return pr.getContent().toString();
    }
    
    @RemoteAction
    global static ConnectApi.Comment postCommentToFeedElementWithNewFile(ID elementId, String text, String imageData, String contentType, String filename) {
        ConnectApi.CommentInput commentInput = new ConnectApi.CommentInput();
        ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
        ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
        textSegmentInput.text = text;
        messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
        messageBodyInput.messageSegments.add(textSegmentInput);
        commentInput.body = messageBodyInput;
        ConnectApi.CommentCapabilitiesInput commentCapabilitiesInput = new ConnectApi.CommentCapabilitiesInput();
        ConnectApi.ContentCapabilityInput contentCapabilityInput = new ConnectApi.ContentCapabilityInput();
        commentCapabilitiesInput.content = contentCapabilityInput;
        contentCapabilityInput.title = filename;
        commentInput.capabilities = commentCapabilitiesInput;
        ConnectApi.BinaryInput binInput = new ConnectApi.BinaryInput(EncodingUtil.base64Decode(imageData.substring(imageData.indexOf('base64,')+1+6)), contentType, filename);
        ConnectApi.Comment comment = ConnectApi.ChatterFeeds.postCommentToFeedElement(Network.getNetworkId(), elementId, commentInput, binInput);
        if ((comment.capabilities.content.fileExtension=='jpg' || comment.capabilities.content.fileExtension=='png' || comment.capabilities.content.fileExtension=='jpeg') && imageData!=null && Network.getNetworkId()!=null) { AWSTools.uploadFileBase64('extradom.media', UserInfo.getOrganizationId()+'-'+comment.capabilities.content.versionId+'/source', imageData.substring(imageData.indexOf('base64,')+1+6), (comment.capabilities.content.fileExtension=='jpg' || comment.capabilities.content.fileExtension=='jpeg')?'image/jpeg':'image/png', 'AKIAJO25LHDRMTH7SLBA', 'zjzdMrMfTi3i26wIqHEKAwvZkjFBN0qJmGE4sH8n'); }
        return comment;
    }
    
    @RemoteAction
    global static Plot__c showPlotForGivenPoint(Decimal location_latitude, Decimal location_longitude, String state, String region) {
        return MapTools.draftPlotForGivenPoint(new List<Decimal>{location_latitude, location_longitude}, state, region);
    }
    
    @RemoteAction
    global static Plot__c followPlotForGivenPoint(Decimal location_latitude, Decimal location_longitude, String state, String region, String city) {
        ID plotId = MapTools.plotIdForGivenPoint(new List<Decimal>{location_latitude, location_longitude}, state, region, city);
        FollowExtension.follow(plotId);
        return [SELECT Id, Name, Geometry_building_MAIL__c, Area__c, Center__latitude__s, Center__longitude__s FROM Plot__c WHERE Id = :plotId LIMIT 1];
    }
    
    @RemoteAction
    global static Boolean requestBuildingConditionsAnalysis(ID plotId, String description, Boolean optInRegulations, Boolean optInPrivacyPolicy, Boolean optInMarketing, String hasPlot, String hasMpzp, String constructionPlan, String hasApp) {
        User u = [SELECT Id, Free_Plot_Analysis_Usage__c, Contact.Email FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        if (u.Free_Plot_Analysis_Usage__c) {
            return false;
        } else {
            u.Free_Plot_Analysis_Usage__c = true;
        }
        Plot__c p = [SELECT Id FROM Plot__c WHERE Id = :plotId LIMIT 1];
        p.Request_Building_Conditions_Analysis__c = true;
        p.Request_Description__c = description;
        update p;
        update u;
        Map<String,Id> data = OrderMethods.getWebLeadContactDetails('Plot Analysis', '', u.Contact.Email, u.Contact.Email, '', 'Internal', false, false, 'No', hasPlot, hasMpzp, constructionPlan, hasApp);
        OrderMethods.updateOptIns(u.Contact.Email, '', optInRegulations, optInPrivacyPolicy, optInMarketing, false, false, false);
        return true;
    }
    
    @RemoteAction
    global static Review__c createReview(ID designId, Integer rating, String body) {
        Review__c r = new Review__c();
        r.Design__c = designId;
        r.Rating__c = rating;
        r.Body__c = body;
        insert r;
        return r;
    }
    
    @RemoteAction
    global static ConnectApi.PollCapability votePoll(ID elementId, ID choiceId) {
        return ConnectApi.ChatterFeeds.voteOnFeedElementPoll(Network.getNetworkId(), elementId, choiceId);
    }
    
}