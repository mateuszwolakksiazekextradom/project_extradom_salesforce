@IsTest
public class TestUtils {
    public static User prepareTestUser(Boolean shouldInsert, String profileName, String department, String Name) {
        Id profileId = [SELECT Id FROM Profile where Profile.Name like :profileName][0].Id;
        String username = 'TestName' + Name + '@' + UserInfo.getUserName().substringAfter('@');
        User u = new User();
        u.FirstName = 'Test User Name';
        u.LastName = 'Surname Test';
        u.Username = username;
        u.Email = username.substringBefore('@') + '@testorg.com';
        u.Alias = 'tTest';
        u.CommunityNickname = username + 'nick';
        u.TimeZoneSidKey = 'GMT';
        u.LocaleSidKey = 'en_US';
        u.EmailEncodingKey = 'UTF-8';
        u.ProfileId = profileId;
        u.LanguageLocaleKey = 'en_US';
        u.Department = department;

        if (shouldInsert) {
            insert u;
        }
        return u;
    }
    
    public static User prepareTestUser(Boolean shouldInsert, String profileName, String department) {
        return prepareTestUser(shouldInsert, profileName, department, '');    
    }

    /********************************** Test Data Factory **********************************/
    /*This factory contains methods for creating either single or multiple records of given object type
    * The records have default field values but can be overwritten by values from template passed from
    * method invocation. You can search methods by object API Name or Label */
    /***************************************************************************************
    Description: FACTORY FOR Design__c - Design
    ****************************************************************************************/
    public static Design__c newDesign(Design__c designTemplate, Boolean isInsert) {
        return newDesigns(designTemplate, 1, isInsert)[0];
    }

    public static List<Design__c> newDesigns(Design__c designTemplate, Integer count, Boolean isInsert) {
        List<Design__c> result = new List<Design__c>();

        Supplier__c supplier;
        if (designTemplate.Supplier__c == null) {
            supplier = newSupplier(new Supplier__c(), true);
        }

        for (Integer i = 0; i < count; i++) {
            Design__c design = designTemplate.clone(false, true);

            if (designTemplate.Supplier__c == null)    design.Supplier__c = supplier.Id;
            if (designTemplate.Name == null)           design.Name = 'Test Design ' + String.valueOf(i);
            if (designTemplate.Full_Name__c == null)   design.Full_Name__c = 'Test Design ' + String.valueOf(i);
            if (designTemplate.Type__c == null)        design.Type__c = ConstUtils.DESIGN_HOUSE_TYPE;
            if (designTemplate.Order_Score__c == null) design.Order_Score__c = 1;
            if (designTemplate.Status__c == null)      design.Status__c = ConstUtils.DESIGN_ACTIVE_STATUS;
            if (designTemplate.Sent_Month__c == null)  design.Sent_Month__c = (i+1) * 10;
            if (designTemplate.Code__c == null)        design.Code__c = 'tst_' + String.valueOf(i);
            result.add(design);
        }

        if (isInsert) insert result;
        return result;
    }

    /***************************************************************************************
    Description: FACTORY FOR Product2 - Product
    ****************************************************************************************/
    public static Product2 newProduct2(Product2 product2Template, Boolean isInsert) {
        return newProducts2(product2Template, 1, isInsert)[0];
    }

    public static List<Product2> newProducts2(Product2 product2Template, Integer count, Boolean isInsert) {
        List<Product2> result = new List<Product2>();

        for (Integer i = 0; i < count; i++) {
            Product2 prod2 = product2Template.clone(false, true);
            if (product2Template.Name == null) prod2.Name = 'Test Product2 ' + String.valueOf(i);
            result.add(prod2);
        }

        if (isInsert) insert result;
        return result;
    }

    /***************************************************************************************
    Description: FACTORY FOR Order - Order
    ****************************************************************************************/
    public static Order newOrder(Order orderTemplate, Boolean isInsert) {
        return newOrders(orderTemplate, 1, isInsert)[0];
    }

    public static List<Order> newOrders(Order orderTemplate, Integer count, Boolean isInsert) {
        List<Order> result = new List<Order>();

        for (Integer i = 0; i < count; i++) {
            Order ordr = orderTemplate.clone(false, true);
            if (orderTemplate.Name == null)         ordr.Name = 'Test Order ' + String.valueOf(i);
            if (orderTemplate.RecordTypeId == null) ordr.RecordTypeId = ConstUtils.ORDER_REGULAR_RT_ID;
            if (orderTemplate.Status == null)       ordr.Status = ConstUtils.ORDER_DRAFT_STATUS;
            if (orderTemplate.PoDate == null)       ordr.PoDate = System.today().addMonths(-1);
            if (orderTemplate.EffectiveDate == null)ordr.EffectiveDate = System.today().addMonths(-1);
            if (orderTemplate.Pricebook2Id == null) ordr.Pricebook2Id = Test.getStandardPricebookId();
            result.add(ordr);
        }

        if (isInsert) insert result;
        return result;
    }

    /***************************************************************************************
   Description: FACTORY FOR OrderItem - OrderItem
   ****************************************************************************************/
    public static OrderItem newOrderItem(OrderItem orderItemTemplate, Boolean isInsert) {
        return newOrderItems(orderItemTemplate, 1, isInsert)[0];
    }

    public static List<OrderItem> newOrderItems(OrderItem orderItemTemplate, Integer count, Boolean isInsert) {
        List<OrderItem> result = new List<OrderItem>();


        for (Integer i = 0; i < count; i++) {
            OrderItem oi = orderItemTemplate != null ? orderItemTemplate.clone(false, true) : new OrderItem();

            if (oi.OrderId == null) oi.OrderId = newOrder(new Order(), true).Id;
            if (oi.UnitPrice == null) oi.UnitPrice = 100 * i;
            if (oi.List_Price__c == null) oi.List_Price__c = 200;
            if (oi.Product_Family__c == null) oi.Product_Family__c = ConstUtils.PRODUCT_DESIGN_FAMILY;
            if (oi.Quantity == null) oi.Quantity = i + 1;
            if (oi.PricebookEntryId == null) oi.PricebookEntryId = TestUtils.newPriceBookEntry(new PricebookEntry(), true).Id;

           result.add(oi);
        }

        if (isInsert) insert result;
        return result;
    }

    /***************************************************************************************
    Description: FACTORY FOR Account - Account
    ****************************************************************************************/
    public static Account newAccount(Account accountTemplate, Boolean isInsert) {
        return newAccounts(accountTemplate, 1, isInsert)[0];
    }

    public static List<Account> newAccounts(Account accountTemplate, Integer count, Boolean isInsert) {
        List<Account> result = new List<Account>();

        for (Integer i = 0; i < count; i++) {
            Account acc = accountTemplate.clone(false, true);
            if (accountTemplate.Name == null)         acc.Name = 'Test Account ' + String.valueOf(i);
            if (accountTemplate.RecordTypeId == null) acc.RecordTypeId = ConstUtils.ACCOUNT_CUSTOMER_RT_ID;

            result.add(acc);
        }

        if (isInsert) insert result;
        return result;
    }

    /***************************************************************************************
    Description: FACTORY FOR Supplier - Supplier__c
    ****************************************************************************************/
    public static Supplier__c newSupplier(Supplier__c supplierTemplate, Boolean isInsert) {
        return newSuppliers(supplierTemplate, 1, isInsert)[0];
    }

    public static List<Supplier__c> newSuppliers(Supplier__c supplierTemplate, Integer count, Boolean isInsert) {
        List<Supplier__c> result = new List<Supplier__c>();

        for (Integer i = 0; i < count; i++) {
            Supplier__c supplier = supplierTemplate.clone(false, true);
            if (supplierTemplate.Name == null)         supplier.Name = 'Test Supplier ' + String.valueOf(i);
            if (supplierTemplate.Code__c == null)      supplier.Code__c = '11';
            if (supplierTemplate.Full_Name__c == null) supplier.Full_Name__c = 'Test FullName ' + String.valueOf(i);
            result.add(supplier);
        }

        if (isInsert) insert result;
        return result;
    }

    /***************************************************************************************
    Description: FACTORY FOR PricebookEntry - PricebookEntry
    ****************************************************************************************/
    public static PricebookEntry newPriceBookEntry(PricebookEntry pbeTemplate, Boolean isInsert) {
        return newPricebookEntries(pbeTemplate, 1, isInsert)[0];
    }

    public static List<PricebookEntry> newPricebookEntries(PricebookEntry pbeTemplate, Integer count, Boolean isInsert) {
        List<PricebookEntry> result = new List<PricebookEntry>();

        for (Integer i = 0; i < count; i++) {
            PricebookEntry pbe = pbeTemplate.clone(false, true);

            //Product2Id
            if (pbeTemplate.IsActive == null)     pbe.IsActive = true;
            if (pbeTemplate.Pricebook2Id == null) pbe.Pricebook2Id = Test.getStandardPricebookId();
            if (pbeTemplate.UnitPrice == null)    pbe.UnitPrice = 100;
            if (pbeTemplate.Product2Id == null)   pbe.Product2Id = newProduct2(new Product2(), true).Id;
            result.add(pbe);
        }

        if (isInsert) insert result;
        return result;
    }

    /***************************************************************************************
    Description: FACTORY FOR Contact - Contact
    ****************************************************************************************/
    public static Contact newContact(Contact contactTemplate, Boolean isInsert) {
        return newContacts(contactTemplate, 1, isInsert)[0];
    }

    public static List<Contact> newContacts(Contact contactTemplate, Integer count, Boolean isInsert) {
        List<Contact> result = new List<Contact>();

        Id accId;
        if (contactTemplate.AccountId == null) {
            accId = newAccount(new Account(), true).Id;
        } else {
            accId = contactTemplate.AccountId;
        }

        for (Integer i = 0; i < count; i++) {
            Contact cnt = contactTemplate.clone(false, true);

            if (contactTemplate.AccountId == null)  cnt.AccountId = accId;
            if (contactTemplate.Email == null)      cnt.Email = 'testEmailtest' + String.valueOf(i) + '@testcode.com';
            if (contactTemplate.LastName == null)   cnt.LastName = 'TestLastName' + String.valueOf(i);
            result.add(cnt);
        }

        if (isInsert) insert result;
        return result;
    }

    /***************************************************************************************
    Description: FACTORY FOR Campaign - Campaign
    ****************************************************************************************/
    public static Campaign newCampaign(Campaign campaignTemplate, Boolean isInsert) {
        return newCampaigns(campaignTemplate, 1, isInsert)[0];
    }

    public static List<Campaign> newCampaigns(Campaign campaignTemplate, Integer count, Boolean isInsert) {
        List<Campaign> result = new List<Campaign>();

        for (Integer i = 0; i < count; i++) {
            Campaign cmp = campaignTemplate.clone(false, true);

            if (campaignTemplate.Name == null)      cmp.Name = 'TestCampaign' + String.valueOf(i);
            if (campaignTemplate.IsActive == null)  cmp.IsActive = true;
            result.add(cmp);
        }

        if (isInsert) insert result;
        return result;
    }

    /***************************************************************************************
    Description: FACTORY FOR CampaignMember - CampaignMember
    ****************************************************************************************/
    public static CampaignMember newCampaignMember(CampaignMember campaignMemberTemplate, Boolean isInsert) {
        return newCampaignMembers(campaignMemberTemplate, 1, isInsert)[0];
    }

    public static List<CampaignMember> newCampaignMembers(CampaignMember campaignMemberTemplate, Integer count, Boolean isInsert) {
        List<CampaignMember> result = new List<CampaignMember>();

        Id cntId;
        Id campaignId;
        if (campaignMemberTemplate.ContactId == null) {
            cntId = newContact(new Contact(), true).Id;
        } else {
            cntId = campaignMemberTemplate.ContactId;
        }
        if (campaignMemberTemplate.campaignId == null) {
            campaignId = newCampaign(new Campaign(), true).Id;
        } else {
            campaignId = campaignMemberTemplate.campaignId;
        }

        for (Integer i = 0; i < count; i++) {
            CampaignMember cmpMemb = campaignMemberTemplate.clone(false, true);

            if (campaignMemberTemplate.ContactId == null)       cmpMemb.ContactId = cntId;
            if (campaignMemberTemplate.CampaignId == null)      cmpMemb.campaignId = campaignId;
            result.add(cmpMemb);
        }

        if (isInsert) insert result;
        return result;
    }

    /***************************************************************************************
    Description: FACTORY FOR Aukcja - Offer__c
    ****************************************************************************************/
    public static Offer__c newOffer(Offer__c offerTemplate, Boolean isInsert) {
        return newOffers(offerTemplate, 1, isInsert)[0];
    }

    public static List<Offer__c> newOffers(Offer__c offerTemplate, Integer count, Boolean isInsert) {
        List<Offer__c> result = new List<Offer__c>();

        for (Integer i = 0; i < count; i++) {
            Offer__c offer = offerTemplate.clone(false, true);

            if (offerTemplate.Name == null)                 offer.Name = 'Offer' + String.valueOf(i);
            if (offerTemplate.Rules__c == null)             offer.Rules__c = 'Some Rules ' + String.valueOf(i);
            if (offerTemplate.Minimum_Value__c == null)     offer.Minimum_Value__c = 1600;
            if (offerTemplate.End_Date__c == null)          offer.End_Date__c = System.now().addDays(2);
            result.add(offer);
        }

        if (isInsert) insert result;
        return result;
    }

    /***************************************************************************************
    Description: FACTORY FOR Oferta - Bid__c
    ****************************************************************************************/
    public static Bid__c newBid(Bid__c bidTemplate, Boolean isInsert) {
        return newBids(bidTemplate, 1, isInsert)[0];
    }

    public static List<Bid__c> newBids(Bid__c bidTemplate, Integer count, Boolean isInsert) {
        List<Bid__c> result = new List<Bid__c>();

        Id offerId;
        if (bidTemplate.Offer__c == null) {
            offerId = newOffer(new Offer__c(), true).Id;
        } else {
            offerId = bidTemplate.Offer__c;
        }

        for (Integer i = 0; i < count; i++) {
            Bid__c bid = bidTemplate.clone(false, true);

            if (bidTemplate.Offer__c == null)       bid.Offer__c = offerId;
            if (bidTemplate.Value__c == null)       bid.Value__c = 1700 + (100 * i);
            result.add(bid);
        }

        if (isInsert) insert result;
        return result;
    }
}