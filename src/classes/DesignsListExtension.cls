public with sharing class DesignsListExtension {
    
    private final Page__c p;
    private Map<String,String> filtersMap;
    
    // filters
    
    public Integer minarea{get;set;}
    public Integer maxarea{get;set;}
    public Integer maxplotsize{get;set;}
    public Integer maxheight{get;set;}
    public Integer maxfootprint{get;set;}
    public Integer minroofslope{get;set;}
    public Integer maxroofslope{get;set;}
    public Integer rooms{get;set;}
    public Integer level{get;set;}
    public Integer garage{get;set;}
    public Integer basement{get;set;}
    public Integer livingtype{get;set;}
    public Integer roof{get;set;}
    public Integer ridge{get;set;}
    public Integer construction{get;set;}
    public Integer ceiling{get;set;}
    public Integer eco{get;set;}
    public Integer garagelocation{get;set;}
    public Integer style{get;set;}
    public Integer shape{get;set;}
    public Integer veranda{get;set;}
    public Integer pool{get;set;}
    public Integer wintergarden{get;set;}
    public Integer terrace{get;set;}
    public Integer oriel{get;set;}
    public Integer mezzanine{get;set;}
    public Integer entrance{get;set;}
    public Integer border{get;set;}
    public Integer carport{get;set;}
    public Integer cost{get;set;}
    public Integer gfroom{get;set;}
    public Integer eaves{get;set;}
    public Integer scarp{get;set;}
    public Integer fireplace{get;set;}
    public Integer bullseye{get;set;}
    
    public Integer count_basement_46 { get { return this.getCountDesignResultsAfterValueChange('basement', 46); } }
    public Integer count_basement_72 { get { return this.getCountDesignResultsAfterValueChange('basement', 72); } }
    public Integer count_basement_114 { get { return this.getCountDesignResultsAfterValueChange('basement', 114); } }
    public Integer count_border_118 { get { return this.getCountDesignResultsAfterValueChange('border', 118); } }
    public Integer count_bullseye_122 { get { return this.getCountDesignResultsAfterValueChange('bullseye', 122); } }
    public Integer count_carport_80 { get { return this.getCountDesignResultsAfterValueChange('carport', 80); } }
    public Integer count_ceiling_95 { get { return this.getCountDesignResultsAfterValueChange('ceiling', 95); } }
    public Integer count_ceiling_96 { get { return this.getCountDesignResultsAfterValueChange('ceiling', 96); } }
    public Integer count_ceiling_97 { get { return this.getCountDesignResultsAfterValueChange('ceiling', 97); } }
    public Integer count_ceiling_99 { get { return this.getCountDesignResultsAfterValueChange('ceiling', 99); } }
    public Integer count_construction_38 { get { return this.getCountDesignResultsAfterValueChange('construction', 38); } }
    public Integer count_construction_39 { get { return this.getCountDesignResultsAfterValueChange('construction', 39); } }
    public Integer count_construction_40 { get { return this.getCountDesignResultsAfterValueChange('construction', 40); } }
    public Integer count_construction_41 { get { return this.getCountDesignResultsAfterValueChange('construction', 41); } }
    public Integer count_construction_82 { get { return this.getCountDesignResultsAfterValueChange('construction', 82); } }
    public Integer count_cost_102 { get { return this.getCountDesignResultsAfterValueChange('cost', 102); } }
    public Integer count_eaves_124 { get { return this.getCountDesignResultsAfterValueChange('eaves', 124); } }
    public Integer count_eco_83 { get { return this.getCountDesignResultsAfterValueChange('eco', 83); } }
    public Integer count_eco_84 { get { return this.getCountDesignResultsAfterValueChange('eco', 84); } }
    public Integer count_entrance_45 { get { return this.getCountDesignResultsAfterValueChange('entrance', 45); } }
    public Integer count_fireplace_123 { get { return this.getCountDesignResultsAfterValueChange('fireplace', 123); } }
    public Integer count_garage_111 { get { return this.getCountDesignResultsAfterValueChange('garage', 111); } }
    public Integer count_garage_50 { get { return this.getCountDesignResultsAfterValueChange('garage', 50); } }
    public Integer count_garage_79 { get { return this.getCountDesignResultsAfterValueChange('garage', 79); } }
    public Integer count_garage_52 { get { return this.getCountDesignResultsAfterValueChange('garage', 52); } }
    public Integer count_garage_65 { get { return this.getCountDesignResultsAfterValueChange('garage', 65); } }
    public Integer count_garagelocation_66 { get { return this.getCountDesignResultsAfterValueChange('garagelocation', 66); } }
    public Integer count_garagelocation_67 { get { return this.getCountDesignResultsAfterValueChange('garagelocation', 67); } }
    public Integer count_garagelocation_70 { get { return this.getCountDesignResultsAfterValueChange('garagelocation', 70); } }
    public Integer count_garagelocation_73 { get { return this.getCountDesignResultsAfterValueChange('garagelocation', 73); } }
    public Integer count_garagelocation_78 { get { return this.getCountDesignResultsAfterValueChange('garagelocation', 78); } }
    public Integer count_gfroom_119 { get { return this.getCountDesignResultsAfterValueChange('gfroom', 119); } }
    public Integer count_level_47 { get { return this.getCountDesignResultsAfterValueChange('level', 47); } }
    public Integer count_level_48 { get { return this.getCountDesignResultsAfterValueChange('level', 48); } }
    public Integer count_level_63 { get { return this.getCountDesignResultsAfterValueChange('level', 63); } }
    public Integer count_livingtype_42 { get { return this.getCountDesignResultsAfterValueChange('livingtype', 42); } }
    public Integer count_livingtype_43 { get { return this.getCountDesignResultsAfterValueChange('livingtype', 43); } }
    public Integer count_livingtype_49 { get { return this.getCountDesignResultsAfterValueChange('livingtype', 49); } }
    public Integer count_mezzanine_94 { get { return this.getCountDesignResultsAfterValueChange('mezzanine', 94); } }
    public Integer count_oriel_64 { get { return this.getCountDesignResultsAfterValueChange('oriel', 64); } }
    public Integer count_pool_71 { get { return this.getCountDesignResultsAfterValueChange('pool', 71); } }
    public Integer count_ridge_105 { get { return this.getCountDesignResultsAfterValueChange('ridge', 105); } }
    public Integer count_ridge_106 { get { return this.getCountDesignResultsAfterValueChange('ridge', 106); } }
    public Integer count_roof_12 { get { return this.getCountDesignResultsAfterValueChange('roof', 12); } }
    public Integer count_roof_13 { get { return this.getCountDesignResultsAfterValueChange('roof', 13); } }
    public Integer count_roof_3 { get { return this.getCountDesignResultsAfterValueChange('roof', 3); } }
    public Integer count_roof_4 { get { return this.getCountDesignResultsAfterValueChange('roof', 4); } }
    public Integer count_roof_5 { get { return this.getCountDesignResultsAfterValueChange('roof', 5); } }
    public Integer count_roof_6 { get { return this.getCountDesignResultsAfterValueChange('roof', 6); } }
    public Integer count_roof_7 { get { return this.getCountDesignResultsAfterValueChange('roof', 7); } }
    public Integer count_roof_8 { get { return this.getCountDesignResultsAfterValueChange('roof', 8); } }
    public Integer count_roof_9 { get { return this.getCountDesignResultsAfterValueChange('roof', 9); } }
    public Integer count_scarp_121 { get { return this.getCountDesignResultsAfterValueChange('scarp', 121); } }
    public Integer count_shape_44 { get { return this.getCountDesignResultsAfterValueChange('shape', 44); } }
    public Integer count_shape_56 { get { return this.getCountDesignResultsAfterValueChange('shape', 56); } }
    public Integer count_shape_57 { get { return this.getCountDesignResultsAfterValueChange('shape', 57); } }
    public Integer count_shape_61 { get { return this.getCountDesignResultsAfterValueChange('shape', 61); } }
    public Integer count_style_77 { get { return this.getCountDesignResultsAfterValueChange('style', 77); } }
    public Integer count_style_81 { get { return this.getCountDesignResultsAfterValueChange('style', 81); } }
    public Integer count_style_90 { get { return this.getCountDesignResultsAfterValueChange('style', 90); } }
    public Integer count_style_98 { get { return this.getCountDesignResultsAfterValueChange('style', 98); } }
    public Integer count_style_104 { get { return this.getCountDesignResultsAfterValueChange('style', 104); } }
    public Integer count_style_92 { get { return this.getCountDesignResultsAfterValueChange('style', 92); } }
    public Integer count_style_91 { get { return this.getCountDesignResultsAfterValueChange('style', 91); } }
    public Integer count_style_120 { get { return this.getCountDesignResultsAfterValueChange('style', 120); } }
    public Integer count_terrace_69 { get { return this.getCountDesignResultsAfterValueChange('terrace', 69); } }
    public Integer count_veranda_68 { get { return this.getCountDesignResultsAfterValueChange('veranda', 68); } }
    public Integer count_wintergarden_76 { get { return this.getCountDesignResultsAfterValueChange('wintergarden', 76); } }    
    
    public DesignsListExtension() {
    }
    
    public DesignsListExtension(ApexPages.StandardController stdController) {
        this.filtersMap = new Map<String,String>(ApexPages.currentPage().getParameters());
        this.setFiltersAsActive(this.filtersMap);
    }
    
    public List<Design__c> getDesignResults() {
        String query = 'SELECT Full_Name__c, Current_Price__c, Gross_Price__c, Thumbnail_Base_URL__c, Code__c, KW_Model__c, KW_Model_Preparation__c, Height__c, Usable_Area__c, Footprint__c, Capacity__c, Roof_slope__c, Count_Rooms__c, Minimum_Plot_Size_Horizontal__c, Minimum_Plot_Size_Vertical__c FROM Design__c';
        query += ' WHERE ' + String.join(getQueryFilters(ApexPages.currentPage().getParameters()),' AND ');
        query += ' ORDER BY Page_Score_Unique__c DESC LIMIT 24';
        return (List<Design__c>)Database.query(query);
    }
    
    public Integer getCountDesignResults() {
        String query = 'SELECT Count() FROM Design__c';
        query += ' WHERE ' + String.join(getQueryFilters(ApexPages.currentPage().getParameters()),' AND ');
        return Database.countQuery(query);
    }
    
    private Integer getCountDesignResultsAfterValueChange(String key, Integer value) {
        String query = 'SELECT Count() FROM Design__c';
        Map<String,String> filtersMap = new Map<String,String>(this.filtersMap);
        filtersMap.put(key,String.valueOf(value));
        query += ' WHERE ' + String.join(getQueryFilters(filtersMap),' AND ');
        return Database.countQuery(query);
    }
    
    private void setFiltersAsActive(Map<String,String> filtersMap) {
        // Usable Area
        if (filtersMap.containsKey('minarea')) {
            this.minarea = Integer.valueOf(filtersMap.get('minarea'));
        }
        if (filtersMap.containsKey('maxarea')) {
            this.maxarea = Integer.valueOf(filtersMap.get('maxarea'));
        }
        // Plot Size
        if (filtersMap.containsKey('maxplotsize')) {
            this.maxplotsize = Integer.valueOf(filtersMap.get('maxplotsize'));
        }
        // Height
        if (filtersMap.containsKey('maxheight')) {
            this.maxheight = Integer.valueOf(filtersMap.get('maxheight'));
        }
        // Footprint
        if (filtersMap.containsKey('maxfootprint')) {
            this.maxfootprint = Integer.valueOf(filtersMap.get('maxfootprint'));
        }
        // Roof Slope
        if (filtersMap.containsKey('minroofslope')) {
            this.minroofslope = Integer.valueOf(filtersMap.get('minroofslope'));
        }
        if (filtersMap.containsKey('maxroofslope')) {
            this.maxroofslope = Integer.valueOf(filtersMap.get('maxroofslope'));
        }
        // Count Rooms
        if (filtersMap.containsKey('rooms')) {
            this.rooms = Integer.valueOf(filtersMap.get('rooms'));
        }
        // Level
        if (filtersMap.containsKey('level')) {
            this.level = Integer.valueOf(filtersMap.get('level'));
        }
        // Garage
        if (filtersMap.containsKey('garage')) {
            this.garage = Integer.valueOf(filtersMap.get('garage'));
        }
        // Roof
        if (filtersMap.containsKey('roof')) {
            this.roof = Integer.valueOf(filtersMap.get('roof'));
        }
        // Living Type
        if (filtersMap.containsKey('livingtype')) {
            this.livingtype = Integer.valueOf(filtersMap.get('livingtype'));
        }
        // Style
        if (filtersMap.containsKey('style')) {
            this.style = Integer.valueOf(filtersMap.get('style'));
        }
        // Construcion
        if (filtersMap.containsKey('construction')) {
            this.construction = Integer.valueOf(filtersMap.get('construction'));
        }
        // Eco
        if (filtersMap.containsKey('eco')) {
            this.eco = Integer.valueOf(filtersMap.get('eco'));
        }
        // Garage Location
        if (filtersMap.containsKey('garagelocation')) {
            this.garagelocation = Integer.valueOf(filtersMap.get('garagelocation'));
        }
        // Basement
        if (filtersMap.containsKey('basement')) {
            this.basement = Integer.valueOf(filtersMap.get('basement'));
        }
        // Ridge
        if (filtersMap.containsKey('ridge')) {
            this.ridge = Integer.valueOf(filtersMap.get('ridge'));
        }
        // Shape
        if (filtersMap.containsKey('shape')) {
            this.shape = Integer.valueOf(filtersMap.get('shape'));
        }
        // Veranda
        if (filtersMap.containsKey('veranda')) {
            this.veranda = Integer.valueOf(filtersMap.get('veranda'));
        }
        // Pool
        if (filtersMap.containsKey('pool')) {
            this.pool = Integer.valueOf(filtersMap.get('pool'));
        }
        // Winter Garden
        if (filtersMap.containsKey('wintergarden')) {
            this.wintergarden= Integer.valueOf(filtersMap.get('wintergarden'));
        }
        // Terrace
        if (filtersMap.containsKey('terrace')) {
            this.terrace = Integer.valueOf(filtersMap.get('terrace'));
        }
        // Oriel
        if (filtersMap.containsKey('oriel')) {
            this.oriel = Integer.valueOf(filtersMap.get('oriel'));
        }
        // Mezzanine
        if (filtersMap.containsKey('mezzanine')) {
            this.mezzanine = Integer.valueOf(filtersMap.get('mezzanine'));
        }
        // Entrance
        if (filtersMap.containsKey('entrance')) {
            this.entrance = Integer.valueOf(filtersMap.get('entrance'));
        }
        // Border
        if (filtersMap.containsKey('border')) {
            this.border = Integer.valueOf(filtersMap.get('border'));
        }
        // Border
        if (filtersMap.containsKey('carport')) {
            this.carport = Integer.valueOf(filtersMap.get('carport'));
        }
        // Cost
        if (filtersMap.containsKey('cost')) {
            this.cost = Integer.valueOf(filtersMap.get('cost'));
        }
        // Ground Floor Room
        if (filtersMap.containsKey('gfroom')) {
            this.gfroom = Integer.valueOf(filtersMap.get('gfroom'));
        }
        // Ceiling
        if (filtersMap.containsKey('ceiling')) {
            this.ceiling = Integer.valueOf(filtersMap.get('ceiling'));
        }
        // Eaves
        if (filtersMap.containsKey('eaves')) {
            this.eaves = Integer.valueOf(filtersMap.get('eaves'));
        }
        // Scarp
        if (filtersMap.containsKey('scarp')) {
            this.scarp = Integer.valueOf(filtersMap.get('scarp'));
        }
        // Fireplace
        if (filtersMap.containsKey('fireplace')) {
            this.fireplace = Integer.valueOf(filtersMap.get('fireplace'));
        }
        // Bull's Eye
        if (filtersMap.containsKey('bullseye')) {
            this.bullseye = Integer.valueOf(filtersMap.get('bullseye'));
        }
    }
    
    public List<String> getQueryFilters(Map<String,String> filtersMap) {
    
        Integer minarea;
        Integer maxarea;
        Integer maxplotsize;
        Integer maxheight;
        Integer maxfootprint;
        Integer minroofslope;
        Integer maxroofslope;
        Integer rooms;
        Integer level;
        Integer garage;
        Integer basement;
        Integer livingtype;
        Integer roof;
        Integer ridge;
        Integer construction;
        Integer ceiling;
        Integer eco;
        Integer garagelocation;
        Integer style;
        Integer shape;
        Integer veranda;
        Integer pool;
        Integer wintergarden;
        Integer terrace;
        Integer oriel;
        Integer mezzanine;
        Integer entrance;
        Integer border;
        Integer carport;
        Integer cost;
        Integer gfroom;
        Integer eaves;
        Integer scarp;
        Integer fireplace;
        Integer bullseye;
        
        List<String> filters = new List<String>{'Status__c = \'Active\''};
        
        // Usable Area
        if (filtersMap.containsKey('minarea')) {
            minarea = Integer.valueOf(filtersMap.get('minarea'));
            filters.add('Usable_Area__c >= '+String.valueOf(minarea));
        }
        if (filtersMap.containsKey('maxarea')) {
            maxarea = Integer.valueOf(filtersMap.get('maxarea'));
            filters.add('Usable_Area__c <= '+String.valueOf(maxarea));
        }
        // Plot Size
        if (filtersMap.containsKey('maxplotsize')) {
            maxplotsize = Integer.valueOf(filtersMap.get('maxplotsize'));
            filters.add('Minimum_Plot_Size_Horizontal__c <= '+String.valueOf(maxplotsize));
        }
        // Height
        if (filtersMap.containsKey('maxheight')) {
            maxheight = Integer.valueOf(filtersMap.get('maxheight'));
            filters.add('Height__c <= '+String.valueOf(maxheight));
        }
        // Footprint
        if (filtersMap.containsKey('maxfootprint')) {
            maxfootprint = Integer.valueOf(filtersMap.get('maxfootprint'));
            filters.add('Footprint__c <= '+String.valueOf(maxfootprint));
        }
        // Roof Slope
        if (filtersMap.containsKey('minroofslope')) {
            minroofslope = Integer.valueOf(filtersMap.get('minroofslope'));
            filters.add('Roof_slope__c >= '+String.valueOf(minroofslope));
        }
        if (filtersMap.containsKey('maxroofslope')) {
            maxroofslope = Integer.valueOf(filtersMap.get('maxroofslope'));
            filters.add('Roof_slope__c <= '+String.valueOf(maxroofslope));
        }
        // Count Rooms
        if (filtersMap.containsKey('rooms')) {
            rooms = Integer.valueOf(filtersMap.get('rooms'));
            filters.add('Count_Rooms__c = '+String.valueOf(rooms));
        }
        // Level
        if (filtersMap.containsKey('level')) {
            level = Integer.valueOf(filtersMap.get('level'));
            filters.add('Category_Keys__c like \'%['+level+']%\'');
        }
        // Garage
        if (filtersMap.containsKey('garage')) {
            garage = Integer.valueOf(filtersMap.get('garage'));
            filters.add('Category_Keys__c like \'%['+garage+']%\'');
        }
        // Roof
        if (filtersMap.containsKey('roof')) {
            roof = Integer.valueOf(filtersMap.get('roof'));
            filters.add('Category_Keys__c like \'%['+roof+']%\'');
        }
        // Living Type
        if (filtersMap.containsKey('livingtype')) {
            livingtype = Integer.valueOf(filtersMap.get('livingtype'));
            filters.add('Category_Keys__c like \'%['+livingtype+']%\'');
        }
        // Style
        if (filtersMap.containsKey('style')) {
            style = Integer.valueOf(filtersMap.get('style'));
            filters.add('Category_Keys__c like \'%['+style+']%\'');
        }
        // Construcion
        if (filtersMap.containsKey('construction')) {
            construction = Integer.valueOf(filtersMap.get('construction'));
            filters.add('Category_Keys__c like \'%['+construction+']%\'');
        }
        // Eco
        if (filtersMap.containsKey('eco')) {
            eco = Integer.valueOf(filtersMap.get('eco'));
            filters.add('Category_Keys__c like \'%['+eco+']%\'');
        }
        // Garage Location
        if (filtersMap.containsKey('garagelocation')) {
            garagelocation = Integer.valueOf(filtersMap.get('garagelocation'));
            filters.add('Category_Keys__c like \'%['+garagelocation+']%\'');
        }
        // Basement
        if (filtersMap.containsKey('basement')) {
            basement = Integer.valueOf(filtersMap.get('basement'));
            filters.add('Category_Keys__c like \'%['+basement+']%\'');
        }
        // Ridge
        if (filtersMap.containsKey('ridge')) {
            ridge = Integer.valueOf(filtersMap.get('ridge'));
            filters.add('Category_Keys__c like \'%['+ridge+']%\'');
        }
        // Shape
        if (filtersMap.containsKey('shape')) {
            shape = Integer.valueOf(filtersMap.get('shape'));
            filters.add('Category_Keys__c like \'%['+shape+']%\'');
        }
        // Veranda
        if (filtersMap.containsKey('veranda')) {
            veranda = Integer.valueOf(filtersMap.get('veranda'));
            filters.add('Category_Keys__c like \'%['+veranda+']%\'');
        }
        // Pool
        if (filtersMap.containsKey('pool')) {
            pool = Integer.valueOf(filtersMap.get('pool'));
            filters.add('Category_Keys__c like \'%['+pool+']%\'');
        }
        // Winter Garden
        if (filtersMap.containsKey('wintergarden')) {
            wintergarden= Integer.valueOf(filtersMap.get('wintergarden'));
            filters.add('Category_Keys__c like \'%['+wintergarden+']%\'');
        }
        // Terrace
        if (filtersMap.containsKey('terrace')) {
            terrace = Integer.valueOf(filtersMap.get('terrace'));
            filters.add('Category_Keys__c like \'%['+terrace+']%\'');
        }
        // Oriel
        if (filtersMap.containsKey('oriel')) {
            oriel = Integer.valueOf(filtersMap.get('oriel'));
            filters.add('Category_Keys__c like \'%['+oriel+']%\'');
        }
        // Mezzanine
        if (filtersMap.containsKey('mezzanine')) {
            mezzanine = Integer.valueOf(filtersMap.get('mezzanine'));
            filters.add('Category_Keys__c like \'%['+mezzanine+']%\'');
        }
        // Entrance
        if (filtersMap.containsKey('entrance')) {
            entrance = Integer.valueOf(filtersMap.get('entrance'));
            filters.add('Category_Keys__c like \'%['+entrance+']%\'');
        }
        // Border
        if (filtersMap.containsKey('border')) {
            border = Integer.valueOf(filtersMap.get('border'));
            filters.add('Category_Keys__c like \'%['+border+']%\'');
        }
        // Border
        if (filtersMap.containsKey('carport')) {
            carport = Integer.valueOf(filtersMap.get('carport'));
            filters.add('Category_Keys__c like \'%['+carport+']%\'');
        }
        // Cost
        if (filtersMap.containsKey('cost')) {
            cost = Integer.valueOf(filtersMap.get('cost'));
            filters.add('Category_Keys__c like \'%['+cost+']%\'');
        }
        // Ground Floor Room
        if (filtersMap.containsKey('gfroom')) {
            gfroom = Integer.valueOf(filtersMap.get('gfroom'));
            filters.add('Category_Keys__c like \'%['+gfroom+']%\'');
        }
        // Ceiling
        if (filtersMap.containsKey('ceiling')) {
            ceiling = Integer.valueOf(filtersMap.get('ceiling'));
            filters.add('Category_Keys__c like \'%['+ceiling+']%\'');
        }
        // Eaves
        if (filtersMap.containsKey('eaves')) {
            eaves = Integer.valueOf(filtersMap.get('eaves'));
            filters.add('Category_Keys__c like \'%['+eaves+']%\'');
        }
        // Scarp
        if (filtersMap.containsKey('scarp')) {
            scarp = Integer.valueOf(filtersMap.get('scarp'));
            filters.add('Category_Keys__c like \'%['+scarp+']%\'');
        }
        // Fireplace
        if (filtersMap.containsKey('fireplace')) {
            fireplace = Integer.valueOf(filtersMap.get('fireplace'));
            filters.add('Category_Keys__c like \'%['+fireplace+']%\'');
        }
        // Bull's Eye
        if (filtersMap.containsKey('bullseye')) {
            bullseye = Integer.valueOf(filtersMap.get('bullseye'));
            filters.add('Category_Keys__c like \'%['+bullseye+']%\'');
        }
        
        return filters;
        
    }

}