public without sharing class OrderControllerWithPlanSet {
    private String code;
    private String ver;
    public String designVersion {get;set;}
    public String billingFirstName {get;set;}
    public String billingLastName {get;set;}
    public String billingAddressStreetName {get;set;}
    public String billingAddressStreetNo {get;set;}
    public String billingAddressStreetFlat {get;set;}
    public String billingPostalCodePart1 {get;set;}
    public String billingPostalCodePart2 {get;set;}
    public String billingCity {get;set;}
    public String billingEmail {get;set;}
    public String billingPhone {get;set;}
    public String billingCompany {get;set;}
    public String billingTaxNumber {get;set;}
    public String shippingFirstName {get;set;}
    public String shippingLastName {get;set;}
    public String shippingAddressStreetName {get;set;}
    public String shippingAddressStreetNo {get;set;}
    public String shippingAddressStreetFlat {get;set;}
    public String shippingPostalCodePart1 {get;set;}
    public String shippingPostalCodePart2 {get;set;}
    public String shippingCity {get;set;}
    public String shippingPhone {get;set;}
    public String addOnsDetails {get;set;}
    public String addOns {get;set;}
    public String shippingMethod {get;set;}
    public Boolean optInRegulations {get;set;}
    public Boolean optInOrder {get;set;}
    public Boolean optIn {get;set;}
    public Boolean optInForward {get;set;}
    
    public OrderControllerWithPlanSet() {
        this.code = ApexPages.currentPage().getParameters().get('code');
        this.ver = ApexPages.currentPage().getParameters().get('ver');
        this.optInRegulations = true;
        this.optInOrder = true;
        this.optIn = false;
        this.optInForward = false;
        this.shippingMethod = 'Regular (Prepayment)';
        this.designVersion = 'Basic';
        if (this.ver=='mirrored') this.designVersion = 'Mirrored';
    }
    
    public Design__c getDesign() {
        return [SELECT Id, Full_Name__c, Code__c, Mirror__c, Gross_Price__c, Current_Price__c, Thumbnail_Base_URL__c, Type__c, Return_Conditions__c, Swap_Conditions__c FROM Design__c WHERE Code__c != '' AND Code__c = :this.code];
    }
    
    public PageReference save() {
        Order o = new Order();
        //o.Design__c = this.getDesign().id;
        //o.Quantity__c = 1;
        //o.Design_Version__c = this.designVersion;
        o.BillingFirstName__c = this.billingFirstName;
        o.BillingLastName__c = this.billingLastName;
        o.BillingStreet = this.billingAddressStreetName;
        if (String.isNotBlank(this.billingAddressStreetNo)) {
            o.BillingStreet += ' ' + this.billingAddressStreetNo;
        }
        if (String.isNotBlank(this.billingAddressStreetFlat)) {
            o.BillingStreet += '/' + this.billingAddressStreetFlat;
        }
        o.BillingPostalCode = this.billingPostalCodePart1+'-'+this.billingPostalCodePart2;
        o.BillingCity = this.billingCity;
        o.BillingEmail__c = this.billingEmail;
        o.BillingPhone__c = this.billingPhone;
        o.BillingCompany__c = this.billingCompany;
        o.BillingTaxNumber__c = this.billingTaxNumber;
        o.ShippingFirstName__c = this.shippingFirstName;
        o.ShippingLastName__c = this.shippingLastName;
        o.ShippingStreet = this.shippingAddressStreetName;
        if (String.isNotBlank(this.shippingAddressStreetNo)) {
            o.ShippingStreet += ' ' + this.shippingAddressStreetNo;
        }
        if (String.isNotBlank(this.shippingAddressStreetFlat)) {
            o.ShippingStreet += '/' + this.shippingAddressStreetFlat;
        }
        o.ShippingPostalCode = this.shippingPostalCodePart1+'-'+this.shippingPostalCodePart2;
        o.ShippingCity = this.shippingCity;
        o.ShippingPhone__c = this.shippingPhone;
        
        //o.Add_Ons_Details__c = this.addOnsDetails;
        //o.Add_Ons__c = this.addOns;
        //o.Shipping_Method__c = this.shippingMethod;
        o.optInRegulations__c = this.optInRegulations;
        o.optInOrder__c = this.optInOrder;
        o.optIn__c = this.optIn;
        List<Contact> contactsBillingEmail = [SELECT Id, AccountId FROM Contact WHERE EmailId__c = :this.billingEmail AND EmailId__c != '' LIMIT 1];
        List<Contact> contactsBillingPhone = [SELECT Id, AccountId FROM Contact WHERE PhoneId__c = :this.billingPhone AND PhoneId__c != '' LIMIT 1];
        List<Contact> contacts = new List<Contact>{};
        contacts.addAll(contactsBillingEmail);
        contacts.addAll(contactsBillingPhone);
        if (contacts.size()>0) {
            o.AccountId = contacts[0].AccountId;
        } else {
            Account a = new Account();
            if (String.isNotBlank(this.billingCompany)) {
                a.Name = this.billingCompany;
            } else {
                a.Name = this.billingFirstName + ' ' + this.billingLastName;
            }
            a.OwnerId = this.getNewAccountOwnerId();
            a.AccountSource = 'Extradom.pl (Design)';
            insert a;
            Contact c = new Contact(AccountId=a.Id,FirstName=this.billingFirstName,LastName=this.billingLastName,Email=this.billingEmail,Phone=this.billingPhone);
            c.OwnerId = a.OwnerId;
            insert c;
            o.AccountId = a.Id;
        }
        insert o;
        
        try {
            if (this.shippingMethod.contains('(Online)')) {
                Order__c order = [SELECT Id, Pay_URL__c FROM Order__c WHERE Id = :o.Id];
                PageReference payPageRef = new PageReference(order.Pay_URL__c);
                payPageRef.setRedirect(true);
                return payPageRef;
            }
        } catch(Exception e) {
        }
        
        PageReference retPageRef = Page.DesignOrderViewComplete;
        retPageRef.getParameters().put('code',this.getDesign().Code__c);
        if (this.designVersion == 'Mirrored') retPageRef.getParameters().put('ver','mirrored');
        retPageRef.setRedirect(true);
        return retPageRef;
    }
    
    private Id getNewAccountOwnerId() {
        List<AggregateResult> usersLogin = [SELECT userid, min(logintime) firstLogin FROM LoginHistory WHERE logintime = TODAY GROUP BY userid];
        List<GroupMember> members = [SELECT UserOrGroupId FROM GroupMember WHERE GroupId = '00Gb0000000Qpp7'];
        List<GroupMember> lastSeenMembers = New List<GroupMember>();
        
        List<AggregateResult> days = [Select DAY_IN_MONTH(loginTime) day FROM LoginHistory WHERE LoginTime = THIS_MONTH AND LoginTime <> TODAY AND SourceIp = '195.94.192.98' AND UserId IN (Select Id From User WHERE IsActive = true AND ProfileId = '00eb0000000I9GZ') GROUP BY DAY_IN_MONTH(loginTime) HAVING count_distinct(userId) > 1];
        
        Integer[] daysArray = new Integer[]{};
        for (AggregateResult day : days) {
            daysArray.add((Integer)day.get('day'));
        }
        
        List<AggregateResult> activeUsers = [Select userId FROM LoginHistory WHERE LoginTime = THIS_MONTH AND LoginTime <> TODAY AND SourceIp = '195.94.192.98' AND UserId IN (Select Id From User WHERE IsActive = true AND ProfileId = '00eb0000000I9GZ') GROUP BY UserId, day_in_month(loginTime)];
        
        String orderAgentId = '';
        Double orderAgentRate = 0;
        Integer orderAgentCount = 0;
        
        for (AggregateResult ul : usersLogin) {
           for (GroupMember m : members) {
                if (ul.get('userid')==(String)m.UserOrGroupId) {
                    DateTime qStart = (DateTime)ul.get('firstLogin');
                    Double qHours = (System.now().getTime()-qStart.getTime())/1000.0/60.0/60.0;
                    Double qTotalHours = qHours;
                    for (AggregateResult activeUser : activeUsers) {
                        if (activeUser.get('userId')==(String)m.UserOrGroupId) {
                            qTotalHours += 8.0;
                        }
                    }
                    lastSeenMembers.add(m);
                    if (qHours < 8.0) {
                        Integer qCount = 0;
                        qCount = [SELECT count() FROM Account WHERE OwnerId = :m.UserOrGroupId AND SPAM__c = false AND AccountSource like '%(Design)' AND CreatedDate = THIS_MONTH];
                        Double qRate = qCount / qTotalHours;
                        if (qRate <= orderAgentRate || orderAgentId == '') {
                            orderAgentId = (String)m.UserOrGroupId;
                            orderAgentRate = qRate;
                            orderAgentCount = qCount;
                        }
                    }
                }
            }
        }
        if (orderAgentId == '') {
            orderAgentId = '005b0000000RxOP';
        }
        return orderAgentId;
    }

}