public without sharing class OptInExtension {

    public OptInExtension() {
    }

    public Contact getOptInSettings() {
        try {
            Contact c = [SELECT Id, Opt_In_Marketing__c FROM Contact WHERE EmailId__c = :UserInfo.getUserEmail()];
            return c;
        } catch(Exception e) {
            return null;
        }
    }
    
}