@isTest
private class AccountExtensionTest {
    private static testMethod void myUnitTest() {
        Account a = new Account();
        a.Name = 'Test';
        a.Partner_Architect__c = true;  
        a.Transfer_Email__c = 'bartosz.lukjanski@extradom.pl';
        insert a;
        
        Telemarketing__c t = new Telemarketing__c();
        t.Name = 'Przekazanie danych: adaptacje projektów';
        t.Status__c = 'Won';
        t.Post_Conversion_Stage__c = 'In Progress';
        t.Transfer_Account__c = a.Id;
        insert t;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(a);
        AccountExtension ae = new AccountExtension(sc);
        
        Set<Id> ids = AccountExtension.getArchitectIdsForEmail();
        List<Telemarketing__c> ts = ae.getTelemarketingLeads();
        AccountExtension.sendArchitectEmail(new Set<Id>{a.Id});
    
    }
}