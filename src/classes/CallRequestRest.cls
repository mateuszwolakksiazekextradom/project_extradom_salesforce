@RestResource(urlMapping='/callrequest')
global class CallRequestRest {
    @HttpPost
    global static Map<String,String> doPost(String email, String phone, Boolean optInRegulations, Boolean optInPrivacyPolicy, Boolean optInMarketing, Boolean optInMarketingExtended, Boolean optInMarketingPartner, Boolean optInLeadForward, Boolean optInEmail, Boolean optInPhone, Boolean optInProfiling, Boolean optInPKO) {
        Restcontext.response.headers.put('Access-Control-Allow-Origin', '*');
        try {
            Map<String,Id> data = OrderMethods.getWebLeadContactDetails('Call Request', '', phone, email, phone, 'Internal', false, false, 'No');
            Task t = new Task(subject='Zamówienie rozmowy telefonicznej ze strony', Type='Call Request', WhoId=data.get('contactPhoneId'), WhatId=data.get('accountId'), OwnerId=data.get('ownerId'), ActivityDate=System.Today());
            insert t;
            RestRequest req = RestContext.request;
            if (req.headers.containsKey('gaClientId')) {
                OrderMethods.updateGaClientId(data.get('accountId'), req.headers.get('gaClientId'));
            }
            OrderMethods.updateOptIns(email, phone, optInRegulations, optInPrivacyPolicy, optInMarketing, optInMarketingExtended, optInMarketingPartner, optInLeadForward, optInEmail, optInPhone, optInProfiling, optInPKO);
            OrderMethods.markSalesReady(data.get('accountId'));
            if(Test.isRunningTest()) {
                throw new MyException();
            }
        } catch (Exception e) {
            RestResponse res = RestContext.response;
            res.statusCode = 400;
            Map<String,String> result = new Map<String,String>{'result' => 'Nieprawidłowe zapytanie'};
            return result;           
        }
        Map<String,String> result = new Map<String,String>{'result' => 'Dziękujemy za zainteresowanie. Twój Opiekun skontaktuje się z Tobą telefonicznie niezwłocznie, aby pomóc w wyborze projektu domu.'};
        return result;
    }
    
    public class MyException extends Exception {}

}