global class DesignUpdateBatch implements Database.Batchable<Design__c>, Database.AllowsCallouts {

Design__c[] designsToUpdate = [Select id, id__c, onet_id__c, Groups_Yes_Groups__c, LastModifiedDate, full_name__c, code__c, master_design__r.code__c, status__c, supplier_code__c, supplier_name__c, usable_area__c, net_floor_area__c, footprint__c, capacity__c, Minimum_Plot_Size_Horizontal__c, Minimum_Plot_Size_Vertical__c, height__c, roof_slope__c, description__c, technology__c from Design__c where code__c='WRC2429' or code__c='WAJ2532'];

global Iterable<Design__c> start(database.batchablecontext BC) {
    return (designsToUpdate);
}

global void execute(Database.BatchableContext BC, List<Design__c> scope) {
}

global void finish(Database.BatchableContext info) {
}

}