public with sharing class PageExtension {
	
	private final Page__c p;
    public PageExtension(ApexPages.StandardController stdController) {
        this.p = (Page__c)stdController.getRecord();
    }
    
    private ApexPages.StandardSetController stdSetController;
    public PageExtension(ApexPages.StandardSetController stdSetController)
    {
        this.stdSetController = stdSetController;
    }
    
    public PageReference massProcessPagesWithDesigns()
    {
        List<Page__c> selectedPages = (List<Page__c>) stdSetController.getSelected();
        List<ID> selectedPageIds = new List<ID>{};
        for (Page__c p: selectedPages) {
        	selectedPageIds.add(p.Id);
        	if (selectedPageIds.size()==1) {
        		ContactProfiler.processPagesWithDesigns(selectedPageIds);
        		selectedPageIds = new List<ID>();
        	}
        }
        if (!selectedPageIds.isEmpty()) {
        	ContactProfiler.processPagesWithDesigns(selectedPageIds);
        }
        return stdSetController.cancel();
    }

}