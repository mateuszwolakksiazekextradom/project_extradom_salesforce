@isTest
private class DesignExtensionTest {

    static testMethod void myUnitTest() {
        
        test.startTest();

        Supplier__c s = new Supplier__c(Name='Test Supplier ABC', Full_Name__c='Test Supplier', Code__c='ABC');
        insert s;
        Design__c d = new Design__c(Name='D1', Full_Name__c='D1', Code__c='ABC1000', Supplier__c=s.Id, Status__c='Active', Catalogue_Ready__c=true, Category_Keys__c='[1][2][3][4][5][6][7][8][9][10][11][12][13][14][15][16][17][18][19][20][21][22][23][24][25][26][27][28][29][30][31][32][33][34][35][36][37][38][39][40][41][42][43][44][45][46][47][48][49][50][51][52][53]', Is_Base_Design__c=True);
        d.Footprint_inc__c = 120.25;
        d.Height_inc__c = 6.30;
        d.Roof_slope_inc__c = 28;
        d.Construction_Pricing_Estimate_inc__c = 250000;
        d.Roof_Cover_Area_inc__c = 230;
        d.Usable_Area_inc__c = 140.25;
        d.Capacity_inc__c = 405.90;
        d.Minimum_Plot_Size_Horizontal_inc__c = 16.25;
        d.Minimum_Plot_Size_Vertical_inc__c = 20.30;
        d.EUco_inc__c = 37.52;
        d.Description_inc__c = 'Description...';
        d.Description_Preview_inc__c = 'Description...';
        d.Technology_inc__c = 'Technology...';
        d.Technology_Preview_inc__c = 'Technology...';
        d.Ceiling_inc__c = 'monolityczny';
        d.Canonical_URL__c = '/projekt-domu-d1-ABC1000';
        insert d;
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('responseDesignInvalidation');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'text/xml');
        Test.setMock(HttpCalloutMock.class, mock);
        d.Category_Keys__c = '[1][4]';
        d.Update_Date__c = System.now();
        update d;
        Media__c m = new Media__c(Design__c=d.Id, Category__c='Attic Plan', Media_Name_ID__c='abc: d1: attic plan');
        insert m;

        
        
        PageReference pageRef = new PageReference('http://extradom.pl/');
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('dummyPages', ',5,');
        ApexPages.currentPage().getParameters().put('twoSidePages', '');
        
        ApexPages.StandardController sc = new ApexPages.StandardController(d);
        DesignExtension dext = new DesignExtension(sc);
        dext.autoSyncDesyncedIncomingValues();
        System.assertEquals(dext.getFreeCode(),'ABC1001');
        
        CatalogueMethods.getCataloguePagesCSV();
        CatalogueMethods.getCataloguePagesXML();

        System.assertEquals(d.Usable_Area__c, 140.25); 
        
        Design__c d2 = new Design__c(Name='D2', Full_Name__c='D2', Code__c='ABC1001', Supplier__c=s.Id, Status__c='Redirected', Master_Design__c = d.Id);
        insert d2;
        Media__c m2 = new Media__c(Design__c=d2.Id, Category__c='Attic Plan', Media_Name_ID__c='abc: d2: attic plan');
        insert m2;
        
        //Design__c d3 = new Design__c(Name='D3', Full_Name__c='D3', Supplier__c=s.Id);
        //insert d3;
        //delete d3;
        
        Design__c d4 = new Design__c(Name='D4', Full_Name__c='D4', Code__c='ABC1002', Supplier__c=s.Id, Status__c='Active', Category_Keys__c='[54][55][56][57][58][59][60][61][62][63][64][65][66][67][68][69][70][71][72][73][74][75][76][77][78][79][80][81][82][83][84][85][86][87][88][89][90][91][92][93][94][95][96][97][98][99][100][101][102][103][104][105][106][107]', Base_Design__c=d.Id);
        insert d4;
        try {
            delete d4;
        } catch (DmlException e) {
            System.assert(e.getMessage().contains('You cannot delete the record since Design Code is assigned'), e.getMessage());
        }
        

        AmazonSqsSender.genChangesXMLDoc(datetime.now(),datetime.now());
        
        Lead l = new Lead(FirstName='A', LastName='B', Company='Company AB', Phone='604651831', Code__c=d.Code__c);
        insert l;
        List<Order__c> orders = [SELECT id, design__c FROM Order__c WHERE design__c = :d.id];
        Order__c o = orders.get(0);
        

        
        System.schedule('Pending Sales User Update Hours', '0 5 8-20 * * ?', New SalesUserUpdateHoursSchedulerContext());
        
        Page__c p = new Page__c(Name='Projekty domów D', Search_Text__c='d', Type__c='Text');
        insert p;
        update p;
        
        test.stopTest();

    }

    static testMethod void shouldCreatePricebookEntries() {
        // given
        Supplier__c s = new Supplier__c(Name='Test Supplier ABC', Full_Name__c='Test Supplier', Code__c='ABC');
        insert s;
        Design__c d = new Design__c(Name='D1', Full_Name__c='D1', Code__c='ABC1000', Supplier__c=s.Id, Status__c='Active', Catalogue_Ready__c=true, Category_Keys__c='[1][2][3][4][5][6][7][8][9][10][11][12][13][14][15][16][17][18][19][20][21][22][23][24][25][26][27][28][29][30][31][32][33][34][35][36][37][38][39][40][41][42][43][44][45][46][47][48][49][50][51][52][53]', Is_Base_Design__c=True);
        insert d;

        // when
        ApexPages.StandardController sc = new ApexPages.StandardController(d);
        DesignExtension dext = new DesignExtension(sc);
        dext.save();
        List<Product2> products = [
                SELECT Id, Name, ProductCode, Design__c, Family, Max_Discount__c, Version_Required__c
                FROM Product2 WHERE Design__c = :d.Id
        ];
        List<PricebookEntry> pes = [SELECT Id, UnitPrice, IsActive FROM PricebookEntry WHERE Product2.Design__c = :d.Id];

        // then
        System.assertEquals(1, products.size());
        System.assertEquals(d.Code__c, products.get(0).ProductCode);
        System.assertEquals(ConstUtils.PRODUCT_DESIGN_FAMILY, products.get(0).Family);
        System.assertEquals(15, products.get(0).Max_Discount__c);
        System.assertEquals(true, products.get(0).Version_Required__c);
        System.assertEquals(1, pes.size());
        System.assertEquals(false, pes.get(0).IsActive);
        System.assertEquals(0, pes.get(0).UnitPrice);
    }
    
    static testMethod void shouldSyncPrices() {
        Supplier__c s = new Supplier__c(Name='Test Supplier ABC', Full_Name__c='Test Supplier', Code__c='ABC');
        insert s;
        Design__c d = new Design__c(Name='D1', Full_Name__c='D1', Code__c='ABC1000', Supplier__c=s.Id, Status__c='Active', Is_Base_Design__c=True, Gross_Price_inc__c = 1000);       
        insert d;
        List<Design__c> ds =  new List<Design__c>{d};

        test.startTest();
            ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(ds);
            stdSetController.setSelected(ds);
            DesignExtension ext = new DesignExtension(stdSetController);
            ext.massSyncPrices();
        test.stopTest();
        
        Design__c d2 = [SELECT Id, Gross_Price__c, Gross_Price_inc__c FROM Design__c WHERE Id = :d.Id];
        System.assertEquals(d2.Gross_Price__c,d2.Gross_Price_inc__c);
    }
    
    static testMethod void shouldMassProcessFeaturedDesignsBatched() {
        Supplier__c s = new Supplier__c(Name='Test Supplier ABC', Full_Name__c='Test Supplier', Code__c='ABC');
        insert s;
        Design__c d = new Design__c(Name='D1', Full_Name__c='D1', Code__c='ABC1000', Supplier__c=s.Id, Status__c='Active', Is_Base_Design__c=True, Sent_month__c = 1, Views_Month__c = 1);       
        insert d;
        List<Design__c> ds =  new List<Design__c>{d};

        test.startTest();
            ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(ds);
            stdSetController.setSelected(ds);
            DesignExtension ext = new DesignExtension(stdSetController);
            ext.massProcessFeaturedDesignsBatched();
        test.stopTest();
    }

    static testMethod void shouldMassProcessFeaturedDesigns() {
        Supplier__c s = new Supplier__c(Name='Test Supplier ABC', Full_Name__c='Test Supplier', Code__c='ABC');
        insert s;
        Design__c d = new Design__c(Name='D1', Full_Name__c='D1', Code__c='ABC1000', Supplier__c=s.Id, Status__c='Active', Is_Base_Design__c=True, Sent_month__c = 1, Views_Month__c = 1);       
        insert d;
        List<Design__c> ds =  new List<Design__c>{d};

        test.startTest();
            ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(ds);
            stdSetController.setSelected(ds);
            DesignExtension ext = new DesignExtension(stdSetController);
            ext.massProcessFeaturedDesigns();
        test.stopTest();
    }
}