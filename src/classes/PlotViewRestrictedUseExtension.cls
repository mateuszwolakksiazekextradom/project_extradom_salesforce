public without sharing class PlotViewRestrictedUseExtension {

    public String taskDescription {get;set;}
    
    private final Plot__c p;
    public PlotViewRestrictedUseExtension(ApexPages.StandardController stdController) {
        this.p = (Plot__c)stdController.getRecord();
    }
    
    public ApexPages.PageReference orderPlotAnalysis() {
        PageReference pageRef = new PageReference('/PlotView?id='+p.Id);
        pageRef.setRedirect(true);
        pageRef.setAnchor('analiza-dzialki-zamowiona');
        try {
            User u = [SELECT ContactId, Free_Plot_Analysis_Usage__c FROM User WHERE Id = :UserInfo.getUserId()];
            Task t = New Task(Subject = 'Analiza działki: '+p.Name, Type = 'Plot Analysis', ActivityDate = System.today(), Priority = 'Normal', Status = u.Free_Plot_Analysis_Usage__c?'Ordered - Waiting for Payment':'Not Started', WhoId = u.ContactId, WhatId = p.Id, Description = taskDescription);
            insert t;
            u.Free_Plot_Analysis_Usage__c = true;
            update u;
            Plot__c plot = [SELECT Id, Request_Building_Conditions_Analysis__c FROM Plot__c WHERE Id = :p.Id];
            plot.Request_Building_Conditions_Analysis__c = true;
            update plot;
        } catch (Exception e) {
        }
        return pageRef;
    }

}