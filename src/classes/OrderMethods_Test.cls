@IsTest
private class OrderMethods_Test {
    @IsTest
    private static void shouldAssignSalesReadySource() {
        // 1
        Id accountId = OrderMethods.getAccountId('Anna', 'Nowak', 'test2@extradom.pl', '717152061', 'domiporta', 'Design', true, true, null);
        Account a_after = [SELECT Id, Enter_Source_Sales_Ready__c FROM Account WHERE Id = :accountId];
        System.assertEquals('Order', a_after.Enter_Source_Sales_Ready__c);
    }
    @IsTest
    private static void shouldAssignSalesReadySource2() {
        // 2
        Account a2 = new Account(Name='Anna 2', Lead_Status__c='Marketing Qualified');
        insert a2;
        Contact c2 = new Contact(LastName='Nowak 2', Phone='717152061', AccountId=a2.Id);
        insert c2;
        Id account2Id = OrderMethods.getAccountId('Anna 2', 'Nowak 2', 'test2@extradom.pl', '717152061', 'domiporta', 'Design', true, true, null);
        Contact c2_after = [SELECT Id, Email FROM Contact WHERE Id =: c2.Id];
        System.assertEquals('test2@extradom.pl', c2_after.Email);
    }
    @IsTest
    private static void shouldAssignSalesReadySource2a() {
        // 2
        Account a2 = new Account(Name='Anna 2', Lead_Status__c='Marketing Qualified');
        insert a2;
        Contact c2 = new Contact(LastName='Nowak 2', Phone='717152061', Email='test@extradom.pl', AccountId=a2.Id);
        insert c2;
        Id account2Id = OrderMethods.getAccountId('Anna', 'Nowak', 'test2@extradom.pl', '717152061', 'domiporta', 'Design', true, true, null);
        Contact c2_after = [SELECT Id, Email FROM Contact WHERE Id =: c2.Id];
        System.assertEquals('test@extradom.pl', c2_after.Email);
    }
    @IsTest
    private static void shouldAssignSalesReadySource3() {
        // 3
        Account a2 = new Account(Name='Anna 2', Lead_Status__c='Marketing Qualified');
        insert a2;
        Contact c2 = new Contact(LastName='Nowak 2', Email='test2@extradom.pl', AccountId=a2.Id);
        insert c2;
        Id account2Id = OrderMethods.getAccountId('Anna 2', 'Nowak 2', 'test2@extradom.pl', '717152061', 'domiporta', 'Design', true, true, null);
        Contact c2_after = [SELECT Id, Phone FROM Contact WHERE Id =: c2.Id];
        System.assertEquals('717152061', c2_after.Phone);
    }
    @IsTest
    private static void shouldAssignSalesReadySource3a() {
        // 3
        Account a2 = new Account(Name='Anna 2', Lead_Status__c='Marketing Qualified');
        insert a2;
        Contact c2 = new Contact(LastName='Nowak 2', Email='test2@extradom.pl', Phone='717152060', AccountId=a2.Id);
        insert c2;
        Id account2Id = OrderMethods.getAccountId('Anna 2', 'Nowak 2', 'test2@extradom.pl', '717152061', 'domiporta', 'Design', true, true, null);
        Contact c2_after = [SELECT Id, Phone FROM Contact WHERE Id =: c2.Id];
        System.assertEquals('717152060', c2_after.Phone);
    }
    @IsTest
    private static void shouldAssignSalesReadySource4() {
        // 4
        Account a2 = new Account(Name='Anna 2', Lead_Status__c='Marketing Qualified');
        insert a2;
        Contact c2 = new Contact(LastName='Nowak 2', Email='test2@extradom.pl', Phone='717152061', AccountId=a2.Id);
        insert c2;
        Id account2Id = OrderMethods.getAccountId('Anna', 'Nowak', 'test2@extradom.pl', '717152061', 'domiporta', 'Design', true, true, null);
    }
    @IsTest
    private static void shouldAssignAccountSource() {
        // 1
        Map<String,Id> result = OrderMethods.getWebLeadContactDetails('Call Request', 'Anna', 'Nowak', 'test@extradom.pl', '717152060', 'domiporta', true, true, null, 'Yes', 'No', 'powyżej 24 miesięcy', null);
        Account a_after = [SELECT Id, AccountSource FROM Account WHERE Id = :result.get('accountId')];
        System.assertEquals('domiporta (Call Request)', a_after.AccountSource);
    }
    @IsTest
    private static void shouldAssignAccountSource2() {
        // 2
        Account a2 = new Account(Name='Anna 2', Lead_Status__c='Marketing Qualified');
        insert a2;
        Contact c2 = new Contact(LastName='Nowak 2', Phone='717152061', AccountId=a2.Id);
        insert c2;
        Map<String,Id> result = OrderMethods.getWebLeadContactDetails('Call Request', 'Anna', 'Nowak', 'test2@extradom.pl', '717152061', 'domiporta', true, true, null, 'Yes', 'No', 'powyżej 24 miesięcy', null);
        Contact c2_after = [SELECT Id, Email FROM Contact WHERE Id =: c2.Id];
        System.assertEquals('test2@extradom.pl', c2_after.Email);
    }
    @IsTest
    private static void shouldAssignAccountSource2a() {
        // 2
        Account a2 = new Account(Name='Anna 2', Lead_Status__c='Marketing Qualified');
        insert a2;
        Contact c2 = new Contact(LastName='Nowak 2', Phone='717152061', Email='test@extradom.pl', AccountId=a2.Id);
        insert c2;
        Map<String,Id> result = OrderMethods.getWebLeadContactDetails('Call Request', 'Anna', 'Nowak', 'test2@extradom.pl', '717152061', 'domiporta', true, true, null, 'Yes', 'No', 'powyżej 24 miesięcy', null);
        Contact c2_after = [SELECT Id, Email FROM Contact WHERE Id =: c2.Id];
        System.assertEquals('test@extradom.pl', c2_after.Email);
    }
    @IsTest
    private static void shouldAssignAccountSource3() {
        // 3
        Account a2 = new Account(Name='Anna 2', Lead_Status__c='Marketing Qualified');
        insert a2;
        Contact c2 = new Contact(LastName='Nowak 2', Email='test2@extradom.pl', AccountId=a2.Id);
        insert c2;
        Map<String,Id> result = OrderMethods.getWebLeadContactDetails('Call Request', 'Anna', 'Nowak', 'test2@extradom.pl', '717152061', 'domiporta', true, true, null, 'Yes', 'No', 'powyżej 24 miesięcy', null);
        Contact c2_after = [SELECT Id, Phone FROM Contact WHERE Id =: c2.Id];
        System.assertEquals('717152061', c2_after.Phone);
    }
    @IsTest
    private static void shouldAssignAccountSource3a() {
        // 3
        Account a2 = new Account(Name='Anna 2', Lead_Status__c='Marketing Qualified');
        insert a2;
        Contact c2 = new Contact(LastName='Nowak 2', Email='test2@extradom.pl', Phone='717152060', AccountId=a2.Id);
        insert c2;
        Map<String,Id> result = OrderMethods.getWebLeadContactDetails('Call Request', 'Anna', 'Nowak', 'test2@extradom.pl', '717152061', 'domiporta', true, true, null, 'Yes', 'No', 'powyżej 24 miesięcy', null);
        Contact c2_after = [SELECT Id, Phone FROM Contact WHERE Id =: c2.Id];
        System.assertEquals('717152060', c2_after.Phone);
    }
    @IsTest
    private static void shouldAssignAccountSource4() {
        // 4
        Account a2 = new Account(Name='Anna 2', Lead_Status__c='Marketing Qualified');
        insert a2;
        Contact c2 = new Contact(LastName='Nowak 2', Email='test2@extradom.pl', Phone='717152061', AccountId=a2.Id);
        insert c2;
        Map<String,Id> result = OrderMethods.getWebLeadContactDetails('Call Request', 'Anna', 'Nowak', 'test2@extradom.pl', '717152061', 'domiporta', true, true, null, 'Yes', 'No', 'powyżej 24 miesięcy', null);
    }
    @IsTest
    private static void shouldAssignAccountSource5() {
        // 4
        Account a2 = new Account(Name='Anna 2', Lead_Status__c='Marketing Qualified');
        insert a2;
        Contact c2 = new Contact(LastName='Nowak 2', Email='test2@extradom.pl', Phone='717152061', AccountId=a2.Id);
        insert c2;
        Map<String,Id> result = OrderMethods.getWebLeadContactDetails('Test Request', 'Anna', 'Nowak', 'test2@extradom.pl', '717152061', 'domiporta', true, true, null);
    }
    @IsTest
    private static void shouldAssignLeadSourceOrderYes() {
        // 2
        Account a2 = new Account(Name='Anna 2', Lead_Status__c='Marketing Qualified');
        insert a2;
        Contact c2 = new Contact(LastName='Nowak 2', Phone='717152061', Email='test@extradom.pl', AccountId=a2.Id);
        insert c2;
        Id account2Id = OrderMethods.getAccountId('Anna', 'Nowak', 'test2@extradom.pl', '717152061', 'domiporta', 'Design', true, true, null);
        Account acc_after = [SELECT Id, Lead_Source__c FROM Account WHERE Id =: account2Id];
        System.assertEquals('Order', acc_after.Lead_Source__c);
    }
    @IsTest
    private static void shouldAssignLeadSourceOrderNo() {
        // 2
        Account a2 = new Account(Name='Anna 2', Lead_Status__c='Sales Ready', Lead_Source__c='Call Request');
        insert a2;
        Contact c2 = new Contact(LastName='Nowak 2', Phone='717152061', Email='test@extradom.pl', AccountId=a2.Id);
        insert c2;
        Id account2Id = OrderMethods.getAccountId('Anna', 'Nowak', 'test2@extradom.pl', '717152061', 'domiporta', 'Design', true, true, null);
        Account acc_after = [SELECT Id, Lead_Source__c FROM Account WHERE Id =: account2Id];
        System.assertNotEquals('Order', acc_after.Lead_Source__c);
    }
    @IsTest
    private static void shouldAssignLeadSourceCallRequestYes() {
        // 4
        Account a2 = new Account(Name='Anna 2', Lead_Status__c='Marketing Qualified');
        insert a2;
        Contact c2 = new Contact(LastName='Nowak 2', Email='test2@extradom.pl', Phone='717152061', AccountId=a2.Id);
        insert c2;
        Map<String,Id> result = OrderMethods.getWebLeadContactDetails('Call Request', 'Anna', 'Nowak', 'test2@extradom.pl', '717152061', 'domiporta', true, true, null);
        Account acc_after = [SELECT Id, Lead_Source__c FROM Account WHERE Id =: result.get('accountId')];
        System.assertEquals('Call Request', acc_after.Lead_Source__c);
    }
    @IsTest
    private static void shouldAssignLeadSourceCallRequestNo() {
        // 4
        Account a2 = new Account(Name='Anna 2', Lead_Status__c='Sales Ready', Lead_Source__c='Sketches');
        insert a2;
        Contact c2 = new Contact(LastName='Nowak 2', Email='test2@extradom.pl', Phone='717152061', AccountId=a2.Id);
        insert c2;
        Map<String,Id> result = OrderMethods.getWebLeadContactDetails('Call Request', 'Anna', 'Nowak', 'test2@extradom.pl', '717152061', 'domiporta', true, true, null);
        Account acc_after = [SELECT Id, Lead_Source__c FROM Account WHERE Id =: result.get('accountId')];
        System.assertNotEquals('Call Request', acc_after.Lead_Source__c);
    }
    @IsTest
    private static void shouldUpdateOptIns() {
        Account a = new Account(Name='Anna 2', Lead_Status__c='Marketing Qualified');
        insert a;
        Contact c = new Contact(LastName='Nowak 2', Email='test2@extradom.pl', Phone='717152061', AccountId=a.Id);
        insert c;
        OrderMethods.updateOptIns(c.Email, c.Phone, true, true, true, true, true, true);
    }
    @IsTest
    private static void shouldUpdateOptIns2() {
        Account a = new Account(Name='Anna 2', Lead_Status__c='Marketing Qualified');
        insert a;
        Contact c = new Contact(LastName='Nowak 2', Email='test2@extradom.pl', Phone='717152061', AccountId=a.Id);
        insert c;
        OrderMethods.updateOptIns(c.Email, c.Phone, true, true, true, true, true, true, true, true, true, true);
    }
    @IsTest
    private static void shouldMarkSalesReady() {
        Account a = new Account(Name='Anna', Lead_Status__c='', Type='Investor');
        insert a;
        OrderMethods.markSalesReady(a.Id);
    }
    
    @IsTest
    private static void shouldOrganizeOpportunity() {
        Account a = new Account(Name='Anna', Lead_Status__c='', Type='Investor');
        insert a;
        OrderMethods.organizeOpportunity(a.Id);
        Opportunity o = [SELECT Id, StageName FROM Opportunity WHERE AccountId = :a.Id][0];
        o.StageName = 'Closed Won';
        update o;
        OrderMethods.organizeOpportunity(a.Id);
        Opportunity o2 = [SELECT Id, StageName FROM Opportunity WHERE AccountId = :a.Id AND isClosed = false][0];
        o2.StageName = 'Closed Lost / Marketing Opt-Out';
        OrderMethods.organizeOpportunity(a.Id);
        OrderMethods.organizeOpportunity(a.Id);
    }
    
    @IsTest
    private static void shouldUpdateAccountFilters() {
        Account a = new Account(Name='Anna', Lead_Status__c='', Type='Investor');
        insert a;
        
        OrderMethods.updateAccountFilters(a.id, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }
}