public without sharing class OpportunityTriggerHandler extends TriggerHandler {

    public override void afterUpdate() {
        List<Opportunity> opportunities = (List<Opportunity>) Trigger.new;
        Map<Id, Opportunity> oldOpporunitiesMap = (Map<Id, Opportunity>) Trigger.oldMap;
        closeOpenFollowUpCallTasks(opportunities,oldOpporunitiesMap);
    }
    
    private void closeOpenFollowUpCallTasks(List<Opportunity> opporutnities, Map<Id, Opportunity> oldOpportunitiesMap) {
        Set<Id> accountsToClose = new Set<Id>();
        for (Opportunity o : opporutnities) {
            if (oldOpportunitiesMap != null && oldOpportunitiesMap.containsKey(o.Id) && o.isClosed && !o.isWon && (oldOpportunitiesMap.get(o.Id).isWon != o.isWon || oldOpportunitiesMap.get(o.Id).isClosed != o.isClosed)) {
                accountsToClose.add(o.AccountId);
            }
        }
        for (Opportunity o : [SELECT Id, AccountId FROM Opportunity WHERE AccountId IN :accountsToClose AND isClosed = false]) {
            accountsToClose.remove(o.AccountId);
        }
        
        List<Task> tasksToClose = new List<Task>();
        for (Task t : [SELECT Id, Status FROM Task WHERE AccountId IN :accountsToClose AND Type = :ConstUtils.TASK_FOLLOW_UP_TYPE AND Status = :ConstUtils.TASK_NOT_STARTED_STATUS]) {
            t.status = ConstUtils.TASK_COMPLETED_STATUS;
            tasksToClose.add(t);
        }
        update tasksToClose;
    }

}