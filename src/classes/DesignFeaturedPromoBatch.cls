public with sharing class DesignFeaturedPromoBatch implements Database.Batchable<sObject> {
    public Database.querylocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
                SELECT Id, Featured_Promo__c, Pending_Featured_Promo__c
                FROM Design__c
                WHERE Featured_Promo__c != null OR Pending_Featured_Promo__c != null
        ]);
    }

    public void execute(Database.BatchableContext BC, List<sObject> scope) {
        List<Design__c> designs = (List<Design__c> ) scope;
        for (Design__c design : designs) {
            if (design.Pending_Featured_Promo__c != null) {
                design.Featured_Promo__c = design.Pending_Featured_Promo__c;
            } else {
                design.Featured_Promo__c = null;
            }
        }
        DesignTriggerHandler.BYPASS_INVALIDATE_DESIGNS = true;
        update designs;
        DesignTriggerHandler.BYPASS_INVALIDATE_DESIGNS = false;
    }

    public void finish(Database.BatchableContext BC) {

    }
}