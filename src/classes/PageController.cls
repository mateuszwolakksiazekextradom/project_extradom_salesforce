public class PageController {
	List<Page__c> collections;
	public List<Page__c> getCollections() {
		if(collections == null) {
			collections = [SELECT Name, URL__c, Roof__c, Design_Type__c, Categories__c, Meta_Description__c, Query__c, Collection_Placement__c, Collection_Group__c, Collection_Group_Score__c, Main_Category__c, Main_Category_Key__c, Building_Cost__c, Building_Cost_Key__c, Technology__c, Technology_Key__c, Style__c, Style_Key__c, Living_Type__c, Living_Type_Key__c, Add_Ons__c, Add_Ons_Key__c, Search_Text__c, Usable_Area_min__c, Usable_Area_max__c, Plot_H_min__c, Height_max__c, Footprint_max__c, Show_Levels__c FROM Page__c WHERE Type__c = 'Collection' AND Status__c = 'Active' ORDER BY Collection_Order__c ASC];
		}
		return collections;
	}
}