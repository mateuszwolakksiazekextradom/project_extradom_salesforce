global with sharing class LiveExtension {

    public LiveExtension() {
    }

    public LiveExtension(UserViewController controller) {
    }

    public LiveExtension(TopicViewController controller) {
    }

    public LiveExtension(ApexPages.StandardController sc) { }

    @RemoteAction
    global static Map<String,Integer> unreadRecalc() {
        Map<String,Integer> result = new Map<String,Integer>();
        Integer unreadNewsFeed = 0;
        result.put('unreadMessages',ConnectApi.ChatterMessages.getUnreadCount(Network.getNetworkId()).unreadCount);
        try {
            Chatter_Custom_Settings__c ccs = Chatter_Custom_Settings__c.getInstance();
            Datetime nextRefresh = ccs.LastModifiedDate.addMinutes(2);
            if (nextRefresh<=System.now()) {
                ConnectApi.FeedElementPage feedElementPage = ConnectApi.ChatterFeeds.getFeedElementsUpdatedSince(Network.getNetworkId(), ConnectApi.FeedType.News, 'me', null, null, null, null, ccs.Last_News_Feed_Refresh_Token__c);
                ccs.Last_News_Feed_Refresh_Count_Unread__c = feedElementPage.elements.size();
                upsert ccs;
            }
            unreadNewsFeed = ccs.Last_News_Feed_Refresh_Count_Unread__c.intValue();
        } catch (Exception e) {
        }
        result.put('unreadNewsFeed',unreadNewsFeed);
        return result;
    }
    
    @RemoteAction
    global static Void verifyPhone(String phone, Boolean optInMarketing) {
        User u = [SELECT Id, ContactId FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        u.Phone = phone;
        u.Phone_Verification_Request_Date__c = System.now();
        update u;
        if (String.isNotBlank(phone) && String.isNotBlank(u.ContactId)) {
            OrderMethods.addPhoneToContact(u.ContactId, phone, optInMarketing);
        }
    }
    
    @RemoteAction
    global static Void cancelVerification() {
        User u = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        u.Phone_Verification_Request_Date__c = null;
        u.Phone_Verification_Response_Code__c = null;
        u.Phone = null;
        update u;
    }
    
    @RemoteAction
    global static Void verifyRequestCode(String verificationCode) {
        User u = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        u.Phone_Verification_Response_Code__c = verificationCode;
        update u;
    }
    
    @RemoteAction
    global static Void updateGaClientId(String gaClientId) {
        User u = [SELECT Id, AccountId FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        if (String.isNotBlank(u.AccountId) && String.isNotBlank(gaClientId)) {
          OrderMethods.updateGaClientId(u.accountId, gaClientId);
        }
    }
    
}