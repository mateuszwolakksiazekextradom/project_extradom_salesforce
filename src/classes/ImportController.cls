public with sharing class ImportController {

    public class Change {
        public final Object before {get;set;}
        public final Object after {get;set;}
        public Change(Object after, Object before) {
            this.after = after;
            this.before = before;
        }
    }

    public ApexPages.StandardSetController setController{get;set;}
    transient public String jsondata{get;set;}
    public List<String> vars{get;set;}
    transient public List<Map<String,Change>> changes{get;set;}
    public String log {get; set;}
    
    public ImportController() {
        this.setController = new ApexPages.StandardSetController(new List<Design__c>());
    }
    
    public PageReference processJson() {
        this.changes = new List<Map<String,Change>>();
        List<Design__c> importDesigns = (List<Design__c>)JSON.deserialize(jsondata, List<Design__c>.class);
        vars = new List<String>(importDesigns[0].getPopulatedFieldsAsMap().keySet());
        List<String> nameIds = new List<String>();
        List<String> externalWebIds = new List<String>();
        for (Design__c d : importDesigns) {
            d.Design_Name_ID__c = d.External_Web_ID__c.left(5)+d.Full_Name_inc__c.toLowerCase();
            nameIds.add(d.Design_Name_ID__c);
            externalWebIds.add(d.External_Web_ID__c);
        }
        List<Design__c> existingDesigns = (List<Design__c>)Database.query('SELECT Id, Design_Name_ID__c, Name, Full_Name__c, '+String.join(vars,', ')+' FROM Design__c WHERE External_Web_ID__c IN :externalWebIds OR Design_Name_ID__c IN :nameIds');
        Map<String,Design__c> existingDesignsMapByName = new Map<String,Design__c>();
        Map<String,Design__c> existingDesignsMapByExt = new Map<String,Design__c>();
        for (Design__c d : existingDesigns) {
            existingDesignsMapByName.put(d.Design_Name_ID__c,d);
            if (String.isNotBlank(d.External_Web_ID__c)) {
                existingDesignsMapByExt.put(d.External_Web_ID__c,d);
            }
        }
        List<Design__c> selectedDesigns = new List<Design__c>();
        for (Design__c importDesign : importDesigns) {
            if (existingDesignsMapByName.containsKey(importDesign.Design_Name_ID__c) || existingDesignsMapByExt.containsKey(importDesign.External_Web_ID__c)) {
                // existing designs potentially for update (matched by name)
                Design__c existingDesign;
                if (existingDesignsMapByName.containsKey(importDesign.Design_Name_ID__c)) {
                    existingDesign = existingDesignsMapByName.get(importDesign.Design_Name_ID__c);
                } else {
                    existingDesign = existingDesignsMapByExt.get(importDesign.External_Web_ID__c);
                }
                Design__c changeDesign = new Design__c();
                Boolean testChange = false;
                Map<String,Change> designChanges = new Map<String,Change>();
                for (String var : vars) {
                    Object importDesignFieldObj = importDesign.get(var);
                    if (importDesignFieldObj instanceof String) {
                        importDesignFieldObj = ((String)importDesignFieldObj).trim();
                        if (String.isBlank((String)importDesignFieldObj)) {
                            importDesignFieldObj = null;
                        }
                    }
                    if (importDesignFieldObj != existingDesign.get(var)) {
                        changeDesign.put(var, importDesignFieldObj);
                        designChanges.put(var, new Change(importDesignFieldObj, existingDesign.get(var)));
                        changeDesign.Id = existingDesign.Id;
                        changeDesign.Name = existingDesign.Name;
                        designChanges.put('Id', new Change(changeDesign.Id, existingDesign.Id));
                        designChanges.put('Name', new Change(changeDesign.Name, existingDesign.Name));
                        testChange = true;
                    }
                }
                if (testChange) {
                    // changes for this design contains 2 maps - first is pending, second is previous (both contains the same keys)
                    changes.add(designChanges);
                    selectedDesigns.add(changeDesign);
                }
            } else {
                // new designs for insert
                importDesign.Full_Name__c = importDesign.Full_Name_inc__c;
                importDesign.Name = importDesign.Full_Name__c;
                // changes for this design contains only 1 map - indicates that it is new entry
                Map<String,Change> designChanges = new Map<String,Change>();
                Map<String,Object> importFieldsAsMap = importDesign.getPopulatedFieldsAsMap();
                for (String var : importFieldsAsMap.keySet()) {
                    designChanges.put(var, new Change(importFieldsAsMap.get(var), null));
                }
                changes.add(designChanges);
                selectedDesigns.add(importDesign);
            }
        }
        this.setController = new ApexPages.StandardSetController(selectedDesigns);
        this.setController.setSelected(selectedDesigns);
        return null;
    }
    
    public Integer getCountChanges() {
        return this.setController.getSelected().size();
    }
    
    public List<Design__c> getSelected() {
        return this.setController.getSelected();
    }
    
    public List<Map<String,Object>> getChangesList() {
        List<Map<String,Object>> changes = new List<Map<String,Object>>{};
        for (Integer i=0; i<Math.min(this.getCountChanges(),1000); i++) {
            changes.add(((Design__c)this.setController.getSelected()[i]).getPopulatedFieldsAsMap());
        }
        return changes;
    }
    
    @future
    public static void processLastSeenBot(String jsondata)
    {
        List<Design__c> importDesigns = (List<Design__c>)JSON.deserialize(jsondata, List<Design__c>.class);
        Map<String,Design__c> importDesignsMap = new Map<String,Design__c>();
        for (Design__c d : importDesigns) {
            importDesignsMap.put(d.External_Web_ID__c,d);
        }
        List<String> externalWebIds = new List<String>(importDesignsMap.keySet());
        List<Design__c> existingDesigns = [SELECT Id, Supplier__r.Id FROM Design__c WHERE External_Web_ID__c IN :externalWebIds];
        Set<Id> supplierIds = new Set<Id>();
        for (Design__c d : existingDesigns) {
            d.Last_Seen_Bot__c = System.now();
            supplierIds.add(d.Supplier__r.Id);
        }
        List<Supplier__c> suppsToUpdate = [SELECT Id, Last_Seen_Bot__c FROM Supplier__c WHERE Id IN :supplierIds];
        for (Supplier__c s : suppsToUpdate) {
            s.Last_Seen_Bot__c = System.now();
        }
        update existingDesigns;
        update suppsToUpdate;
    }
    
    public PageReference save(){
        setController.save();
        this.processJson();
        ImportController.processLastSeenBot(jsondata);
        return null;
    }
}