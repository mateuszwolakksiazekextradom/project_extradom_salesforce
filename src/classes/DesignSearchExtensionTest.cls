@IsTest(SeeAllData=true)
private class DesignSearchExtensionTest {

    static testMethod void myUnitTest() {
        
        test.startTest();
        
        Supplier__c s = new Supplier__c(Name='Test Supplier ABC', Full_Name__c='Test Supplier', Code__c='ABC');
        insert s;
        
        Design__c d1 = new Design__c(Name='Test Design', Full_Name__c='Test Design', Supplier__c=s.Id);
        insert d1;
        d1.Type__c = 'Dom';
        d1.Secondary_Type__c = 'Sklep';
        d1.Living_Type__c = 'wolnostojąca';
        d1.Construction__c = 'murowana';
        d1.Roof__c = 'dwuspadowy';
        d1.Oriel__c = 'tak';
        d1.Terrace__c = 'tak';
        d1.Veranda__c = 'tak';
        d1.Mezzanine__c = 'tak';
        d1.Winter_Garden__c = 'tak';
        d1.Pool__c = 'tak';
        d1.Style__c = 'dworkowy';
        d1.Shape__c = 'litera L';
        d1.Ridge__c = 'równoległa do drogi';
        d1.Border_Ready__c = 'tak';
        d1.South_Entrance__c = 'tak';
        d1.Garage_Location__c = 'w bryle budynku';
        update d1;
        
        Page__c p;
        p = new Page__c(Name='Projekty domów T', Type__c='Collection', Design_Type__c = 'domy');
        insert p;
        
        PageReference pageRef = new PageReference('http://extradom.pl/projekty-domow');
        Test.setCurrentPage(pageRef);
        
        pageRef = new PageReference('http://extradom.pl/projekty-domow');
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('p', '100');
        ApexPages.currentPage().getParameters().put('type', '1');
        ApexPages.currentPage().getParameters().put('sup', 'abc');
        ApexPages.currentPage().getParameters().put('ver', 'test');
        ApexPages.currentPage().getParameters().put('minarea', '50');
        ApexPages.currentPage().getParameters().put('maxarea', '150');
        ApexPages.currentPage().getParameters().put('maxplotsize', '27');
        ApexPages.currentPage().getParameters().put('maxheight', '8');
        ApexPages.currentPage().getParameters().put('maxfootprint', '250');
        ApexPages.currentPage().getParameters().put('minroofslope', '30');
        ApexPages.currentPage().getParameters().put('maxroofslope', '40');
        ApexPages.currentPage().getParameters().put('rooms', '5');
        ApexPages.currentPage().getParameters().put('level', '47');
        ApexPages.currentPage().getParameters().put('garage', '79');
        ApexPages.currentPage().getParameters().put('basement', '72');
        ApexPages.currentPage().getParameters().put('livingtype', '49');
        ApexPages.currentPage().getParameters().put('roof', '4');
        ApexPages.currentPage().getParameters().put('ridge', '105');
        ApexPages.currentPage().getParameters().put('construction', '82');
        ApexPages.currentPage().getParameters().put('eco', '83');
        ApexPages.currentPage().getParameters().put('garagelocation', '66');
        ApexPages.currentPage().getParameters().put('style', '77');
        ApexPages.currentPage().getParameters().put('shape', '44');
        ApexPages.currentPage().getParameters().put('ceiling', '96');
        ApexPages.currentPage().getParameters().put('veranda', '68');
        ApexPages.currentPage().getParameters().put('pool', '71');
        ApexPages.currentPage().getParameters().put('wintergarden', '76');
        ApexPages.currentPage().getParameters().put('terrace', '69');
        ApexPages.currentPage().getParameters().put('oriel', '64');
        ApexPages.currentPage().getParameters().put('mezzanine', '94');
        ApexPages.currentPage().getParameters().put('entrance', '45');
        ApexPages.currentPage().getParameters().put('border', '118');
        ApexPages.currentPage().getParameters().put('carport', '80');
        ApexPages.currentPage().getParameters().put('cost', '102');
        ApexPages.currentPage().getParameters().put('gfroom', '119');
        ApexPages.currentPage().getParameters().put('bullseye', '122');
        ApexPages.currentPage().getParameters().put('fireplace', '123');
        ApexPages.currentPage().getParameters().put('scarp', '121');
        ApexPages.currentPage().getParameters().put('eaves', '124');
        
        test.stopTest();
    
    }
        
    static testMethod void myUnitTest2() {
        
        test.startTest();
        
        Page__c p;
        p = new Page__c(Name='Projekty domów T2', Type__c='Collection', Design_Type__c = 'domy');
        insert p;
        
        ApexPages.StandardController scpage = new ApexPages.StandardController(p);
        DesignSearchExtension ext = new DesignSearchExtension(scpage);
        ext.getDesignResults();
        
        System.debug(ext.count);
        System.debug(ext.sum_basement_46);
        System.debug(ext.sum_basement_72);
        System.debug(ext.sum_basement_114);
        System.debug(ext.sum_border_118);
        System.debug(ext.sum_bullseye_122);
        System.debug(ext.sum_carport_80);
        System.debug(ext.sum_ceiling_95);
        System.debug(ext.sum_ceiling_96);
        System.debug(ext.sum_ceiling_97);
        System.debug(ext.sum_ceiling_99);
        System.debug(ext.sum_construction_38);
        System.debug(ext.sum_construction_39);
        System.debug(ext.sum_construction_40);
        System.debug(ext.sum_construction_41);
        System.debug(ext.sum_construction_82);
        System.debug(ext.sum_construction_125);
        System.debug(ext.sum_cost_102);
        System.debug(ext.sum_eaves_124);
        System.debug(ext.sum_eco_83);
        System.debug(ext.sum_eco_84);
        System.debug(ext.sum_entrance_45);
        System.debug(ext.sum_fireplace_123);
        System.debug(ext.sum_garage_111);
        System.debug(ext.sum_garage_50);
        System.debug(ext.sum_garage_79);
        System.debug(ext.sum_garage_52);
        System.debug(ext.sum_garage_65);
        System.debug(ext.sum_garagelocation_66);
        System.debug(ext.sum_garagelocation_67);
        System.debug(ext.sum_garagelocation_70);
        System.debug(ext.sum_garagelocation_73);
        System.debug(ext.sum_garagelocation_78);
        System.debug(ext.sum_gfroom_119);
        System.debug(ext.sum_level_47);
        System.debug(ext.sum_level_48);
        System.debug(ext.sum_level_63);
        System.debug(ext.sum_livingtype_42);
        System.debug(ext.sum_livingtype_43);
        System.debug(ext.sum_livingtype_49);
        System.debug(ext.sum_mezzanine_94);
        System.debug(ext.sum_oriel_64);
        System.debug(ext.sum_pool_71);
        System.debug(ext.sum_ridge_105);
        System.debug(ext.sum_ridge_106);
        System.debug(ext.sum_roof_12);
        System.debug(ext.sum_roof_13);
        System.debug(ext.sum_roof_3);
        System.debug(ext.sum_roof_4);
        System.debug(ext.sum_roof_5);
        System.debug(ext.sum_roof_6);
        System.debug(ext.sum_roof_7);
        System.debug(ext.sum_roof_8);
        System.debug(ext.sum_roof_9);
        System.debug(ext.sum_scarp_121);
        System.debug(ext.sum_shape_44);
        System.debug(ext.sum_shape_56);
        System.debug(ext.sum_shape_57);
        System.debug(ext.sum_shape_61);
        System.debug(ext.sum_style_77);
        System.debug(ext.sum_style_81);
        System.debug(ext.sum_style_90);
        System.debug(ext.sum_style_98);
        System.debug(ext.sum_style_104);
        System.debug(ext.sum_style_92);
        System.debug(ext.sum_style_91);
        System.debug(ext.sum_terrace_69);
        /* System.debug(ext.sum_type_1);
        System.debug(ext.sum_type_2);
        System.debug(ext.sum_type_10);
        System.debug(ext.sum_type_11);
        System.debug(ext.sum_type_14);
        System.debug(ext.sum_type_15);
        System.debug(ext.sum_type_16);
        System.debug(ext.sum_type_17);
        System.debug(ext.sum_type_18);
        System.debug(ext.sum_type_19);
        System.debug(ext.sum_type_20);
        System.debug(ext.sum_type_21);
        System.debug(ext.sum_type_22);
        System.debug(ext.sum_type_23);
        System.debug(ext.sum_type_24);
        System.debug(ext.sum_type_25);
        System.debug(ext.sum_type_26);
        System.debug(ext.sum_type_27);
        System.debug(ext.sum_type_28);
        System.debug(ext.sum_type_29);
        System.debug(ext.sum_type_30);
        System.debug(ext.sum_type_31);
        System.debug(ext.sum_type_32);
        System.debug(ext.sum_type_33);
        System.debug(ext.sum_type_34);
        System.debug(ext.sum_type_36);
        System.debug(ext.sum_type_37);
        System.debug(ext.sum_type_53);
        System.debug(ext.sum_type_54);
        System.debug(ext.sum_type_58);
        System.debug(ext.sum_type_75);
        System.debug(ext.sum_type_85);
        System.debug(ext.sum_type_86);
        System.debug(ext.sum_type_87);
        System.debug(ext.sum_type_88);
        System.debug(ext.sum_type_89);
        System.debug(ext.sum_type_93);
        System.debug(ext.sum_type_100);
        System.debug(ext.sum_type_101);
        System.debug(ext.sum_type_103);
        System.debug(ext.sum_type_108);
        System.debug(ext.sum_type_109);
        System.debug(ext.sum_type_110);
        System.debug(ext.sum_type_116);
        System.debug(ext.sum_type_117); */
        System.debug(ext.sum_veranda_68);
        System.debug(ext.sum_wintergarden_76);
        
        test.stopTest();
    
    }
        
    static testMethod void myUnitTest3() {
        
        test.startTest();
        
        Page__c p;
        p = new Page__c(Name='Projekty domów T3', Type__c='Collection', Design_Type__c = 'domy');
        insert p;
        
        Page__c p2;
        p2 = new Page__c(Name='Projekty domów Test', Type__c='Design Versions', Filter_Code__c='test', Filter_Name__c='Test');
        insert p2;
        
        Page__c p3;
        p3 = new Page__c(Name='Test', q_basement__c=72, q_carport__c=80, q_ceiling__c=96, q_construction__c=82, q_cost__c=102, q_eco__c=83, q_entrance__c=45, q_garage__c=79, q_garagelocation__c=66, q_level__c=47, q_livingtype__c=49, q_maxarea__c=150, q_maxfootprint__c=250, q_maxheight__c=8, q_maxplotsize__c=27, q_maxroofslope__c=40, q_mezzanine__c=94, q_minarea__c=50, q_minroofslope__c=30, q_oriel__c=64, q_pool__c=71, q_ridge__c=105, q_roof__c=4, q_rooms__c=5, q_shape__c=44, q_style__c=77, q_terrace__c=69, q_type__c=1, q_veranda__c=68, q_wintergarden__c=76);
        insert p3;
        
        // no filters selected
        
        PageReference pageRef = new PageReference('http://extradom.pl/projekty-domow');
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController scpage = new ApexPages.StandardController(p);
        DesignSearchExtension ext = new DesignSearchExtension(scpage);
        ext.getDesignResults();
        
        System.debug(ext.count);
        System.debug(ext.sum_basement_46);
        System.debug(ext.sum_basement_72);
        System.debug(ext.sum_basement_114);
        System.debug(ext.sum_border_118);
        System.debug(ext.sum_bullseye_122);
        System.debug(ext.sum_carport_80);
        System.debug(ext.sum_ceiling_95);
        System.debug(ext.sum_ceiling_96);
        System.debug(ext.sum_ceiling_97);
        System.debug(ext.sum_ceiling_99);
        System.debug(ext.sum_construction_38);
        System.debug(ext.sum_construction_39);
        System.debug(ext.sum_construction_40);
        System.debug(ext.sum_construction_41);
        System.debug(ext.sum_construction_82);
        System.debug(ext.sum_construction_125);
        System.debug(ext.sum_cost_102);
        System.debug(ext.sum_eaves_124);
        System.debug(ext.sum_eco_83);
        System.debug(ext.sum_eco_84);
        System.debug(ext.sum_entrance_45);
        System.debug(ext.sum_fireplace_123);
        System.debug(ext.sum_garage_111);
        System.debug(ext.sum_garage_50);
        System.debug(ext.sum_garage_79);
        System.debug(ext.sum_garage_52);
        System.debug(ext.sum_garage_65);
        System.debug(ext.sum_garagelocation_66);
        System.debug(ext.sum_garagelocation_67);
        System.debug(ext.sum_garagelocation_70);
        System.debug(ext.sum_garagelocation_73);
        System.debug(ext.sum_garagelocation_78);
        System.debug(ext.sum_gfroom_119);
        System.debug(ext.sum_level_47);
        System.debug(ext.sum_level_48);
        System.debug(ext.sum_level_63);
        System.debug(ext.sum_livingtype_42);
        System.debug(ext.sum_livingtype_43);
        System.debug(ext.sum_livingtype_49);
        System.debug(ext.sum_mezzanine_94);
        System.debug(ext.sum_oriel_64);
        System.debug(ext.sum_pool_71);
        System.debug(ext.sum_ridge_105);
        System.debug(ext.sum_ridge_106);
        System.debug(ext.sum_roof_12);
        System.debug(ext.sum_roof_13);
        System.debug(ext.sum_roof_3);
        System.debug(ext.sum_roof_4);
        System.debug(ext.sum_roof_5);
        System.debug(ext.sum_roof_6);
        System.debug(ext.sum_roof_7);
        System.debug(ext.sum_roof_8);
        System.debug(ext.sum_roof_9);
        System.debug(ext.sum_scarp_121);
        System.debug(ext.sum_shape_44);
        System.debug(ext.sum_shape_56);
        System.debug(ext.sum_shape_57);
        System.debug(ext.sum_shape_61);
        System.debug(ext.sum_style_77);
        System.debug(ext.sum_style_81);
        System.debug(ext.sum_style_90);
        System.debug(ext.sum_style_98);
        System.debug(ext.sum_style_104);
        System.debug(ext.sum_style_92);
        System.debug(ext.sum_style_91);
        System.debug(ext.sum_terrace_69);
        /* System.debug(ext.sum_type_1);
        System.debug(ext.sum_type_2);
        System.debug(ext.sum_type_10);
        System.debug(ext.sum_type_11);
        System.debug(ext.sum_type_14);
        System.debug(ext.sum_type_15);
        System.debug(ext.sum_type_16);
        System.debug(ext.sum_type_17);
        System.debug(ext.sum_type_18);
        System.debug(ext.sum_type_19);
        System.debug(ext.sum_type_20);
        System.debug(ext.sum_type_21);
        System.debug(ext.sum_type_22);
        System.debug(ext.sum_type_23);
        System.debug(ext.sum_type_24);
        System.debug(ext.sum_type_25);
        System.debug(ext.sum_type_26);
        System.debug(ext.sum_type_27);
        System.debug(ext.sum_type_28);
        System.debug(ext.sum_type_29);
        System.debug(ext.sum_type_30);
        System.debug(ext.sum_type_31);
        System.debug(ext.sum_type_32);
        System.debug(ext.sum_type_33);
        System.debug(ext.sum_type_34);
        System.debug(ext.sum_type_36);
        System.debug(ext.sum_type_37);
        System.debug(ext.sum_type_53);
        System.debug(ext.sum_type_54);
        System.debug(ext.sum_type_58);
        System.debug(ext.sum_type_75);
        System.debug(ext.sum_type_85);
        System.debug(ext.sum_type_86);
        System.debug(ext.sum_type_87);
        System.debug(ext.sum_type_88);
        System.debug(ext.sum_type_89);
        System.debug(ext.sum_type_93);
        System.debug(ext.sum_type_100);
        System.debug(ext.sum_type_101);
        System.debug(ext.sum_type_103);
        System.debug(ext.sum_type_108);
        System.debug(ext.sum_type_109);
        System.debug(ext.sum_type_110);
        System.debug(ext.sum_type_116);
        System.debug(ext.sum_type_117); */
        System.debug(ext.sum_veranda_68);
        System.debug(ext.sum_wintergarden_76);
        
        // filters selected from Page__c
        
        pageRef = new PageReference('http://extradom.pl/projekty-domow');
        Test.setCurrentPage(pageRef);
        
        scpage = new ApexPages.StandardController(p3);
        ext = new DesignSearchExtension(scpage);
        ext.getDesignResults();
        ext.getMyFollowingDesigns();
        ext.getMyFollowingDesignsCompare();
        
        test.stopTest();
        
    }
    
    static testMethod void myUnitTestUpdatedDesignSearchViewExtension() {
        
        test.startTest();
        
        Page__c p;
        p = new Page__c(Name='Projekty domów T3', Type__c='Collection', Design_Type__c = 'domy');
        insert p;
        
        Page__c p3;
        p3 = new Page__c(Name='Test', q_basement__c=72, q_carport__c=80, q_ceiling__c=96, q_construction__c=82, q_cost__c=102, q_eco__c=83, q_entrance__c=45, q_garage__c=79, q_garagelocation__c=66, q_level__c=47, q_livingtype__c=49, q_maxarea__c=150, q_maxfootprint__c=250, q_maxheight__c=8, q_maxplotsize__c=27, q_maxroofslope__c=40, q_mezzanine__c=94, q_minarea__c=50, q_minroofslope__c=30, q_oriel__c=64, q_pool__c=71, q_ridge__c=105, q_roof__c=4, q_rooms__c=5, q_shape__c=44, q_style__c=77, q_terrace__c=69, q_type__c=1, q_veranda__c=68, q_wintergarden__c=76);
        insert p3;
        
        // no filters selected
        
        PageReference pageRef = new PageReference('http://extradom.pl/projekty-domow');
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController scpage = new ApexPages.StandardController(p);
        DesignSearchViewExtension ext = new DesignSearchViewExtension(scpage);
        ext.getDesignResults();
        System.debug(ext.count);
        
        // filters selected from Page__c
        
        pageRef = new PageReference('http://extradom.pl/projekty-domow');
        Test.setCurrentPage(pageRef);
        
        scpage = new ApexPages.StandardController(p3);
        ext = new DesignSearchViewExtension(scpage);
        ext.getDesignResults();
        ext.getMyFollowingDesigns();
        ext.getMyFollowingDesignsCompare();
        ext.getMySubscriptions();
        ext.getTop404Designs();
        ext.getPromo404Designs();
        
        System.debug(ext.sum_basement_46);
        System.debug(ext.sum_basement_72);
        System.debug(ext.sum_basement_114);
        System.debug(ext.sum_border_118);
        System.debug(ext.sum_bullseye_122);
        System.debug(ext.sum_carport_80);
        System.debug(ext.sum_ceiling_95);
        System.debug(ext.sum_ceiling_96);
        System.debug(ext.sum_ceiling_97);
        System.debug(ext.sum_ceiling_99);
        System.debug(ext.sum_construction_38);
        System.debug(ext.sum_construction_39);
        System.debug(ext.sum_construction_40);
        System.debug(ext.sum_construction_41);
        System.debug(ext.sum_construction_82);
        System.debug(ext.sum_construction_125);
        System.debug(ext.sum_cost_102);
        System.debug(ext.sum_eaves_124);
        System.debug(ext.sum_eco_83);
        System.debug(ext.sum_eco_84);
        System.debug(ext.sum_entrance_45);
        System.debug(ext.sum_fireplace_123);
        System.debug(ext.sum_garage_111);
        System.debug(ext.sum_garage_50);
        System.debug(ext.sum_garage_79);
        System.debug(ext.sum_garage_52);
        System.debug(ext.sum_garage_65);
        System.debug(ext.sum_garagelocation_66);
        System.debug(ext.sum_garagelocation_67);
        System.debug(ext.sum_garagelocation_70);
        System.debug(ext.sum_garagelocation_73);
        System.debug(ext.sum_garagelocation_78);
        System.debug(ext.sum_gfroom_119);
        System.debug(ext.sum_level_47);
        System.debug(ext.sum_level_48);
        System.debug(ext.sum_level_63);
        System.debug(ext.sum_livingtype_42);
        System.debug(ext.sum_livingtype_43);
        System.debug(ext.sum_livingtype_49);
        System.debug(ext.sum_mezzanine_94);
        System.debug(ext.sum_oriel_64);
        System.debug(ext.sum_pool_71);
        System.debug(ext.sum_ridge_105);
        System.debug(ext.sum_ridge_106);
        System.debug(ext.sum_roof_12);
        System.debug(ext.sum_roof_13);
        System.debug(ext.sum_roof_3);
        System.debug(ext.sum_roof_4);
        System.debug(ext.sum_roof_5);
        System.debug(ext.sum_roof_6);
        System.debug(ext.sum_roof_7);
        System.debug(ext.sum_roof_8);
        System.debug(ext.sum_roof_9);
        System.debug(ext.sum_scarp_121);
        System.debug(ext.sum_shape_44);
        System.debug(ext.sum_shape_56);
        System.debug(ext.sum_shape_57);
        System.debug(ext.sum_shape_61);
        System.debug(ext.sum_style_77);
        System.debug(ext.sum_style_81);
        System.debug(ext.sum_style_90);
        System.debug(ext.sum_style_98);
        System.debug(ext.sum_style_104);
        System.debug(ext.sum_style_92);
        System.debug(ext.sum_style_91);
        System.debug(ext.sum_terrace_69);
        System.debug(ext.sum_veranda_68);
        System.debug(ext.sum_wintergarden_76);
 
        // filters from query string
        
        pageRef = new PageReference('http://extradom.pl/projekty-domow');
        pageRef.getParameters().put('type', '1');
        pageRef.getParameters().put('minarea', '50');
        pageRef.getParameters().put('maxarea', '150');
        pageRef.getParameters().put('maxplotsize', '27');
        pageRef.getParameters().put('maxheight', '8');
        pageRef.getParameters().put('maxfootprint', '250');
        pageRef.getParameters().put('minroofslope', '30');
        pageRef.getParameters().put('maxroofslope', '40');
        pageRef.getParameters().put('rooms', '5');
        pageRef.getParameters().put('level', '47');
        pageRef.getParameters().put('garage', '79');
        pageRef.getParameters().put('basement', '72');
        pageRef.getParameters().put('livingtype', '49');
        pageRef.getParameters().put('roof', '4');
        pageRef.getParameters().put('ridge', '105');
        pageRef.getParameters().put('construction', '82');
        pageRef.getParameters().put('eco', '83');
        pageRef.getParameters().put('garagelocation', '66');
        pageRef.getParameters().put('style', '77');
        pageRef.getParameters().put('shape', '44');
        pageRef.getParameters().put('ceiling', '96');
        pageRef.getParameters().put('veranda', '68');
        pageRef.getParameters().put('pool', '71');
        pageRef.getParameters().put('wintergarden', '76');
        pageRef.getParameters().put('terrace', '69');
        pageRef.getParameters().put('oriel', '64');
        pageRef.getParameters().put('mezzanine', '94');
        pageRef.getParameters().put('entrance', '45');
        pageRef.getParameters().put('border', '118');
        pageRef.getParameters().put('carport', '80');
        pageRef.getParameters().put('cost', '102');
        pageRef.getParameters().put('gfroom', '119');
        pageRef.getParameters().put('bullseye', '122');
        pageRef.getParameters().put('fireplace', '123');
        pageRef.getParameters().put('scarp', '121');
        pageRef.getParameters().put('eaves', '124');
        Test.setCurrentPage(pageRef);
        
        
        scpage = new ApexPages.StandardController(new Page__c());
        ext = new DesignSearchViewExtension(scpage);
        
        ext.getFilters();
        ext.getCurrentDesignSets();
        ext.getIdesignerDesigns();
        ext.getWRCDesigns();
        
        DesignSearchViewExtension.Filters filters = ext.getFiltersObject();
        ExtradomApi.DesignSet ds = DesignSearchViewExtension.saveDesignSet(filters);
        DesignSearchViewExtension.deleteDesignSet(ds.id);
        
        test.stopTest();
        
    }
    
}