global class ConnectApiToolsForVF {

    public ID referenceId { get; set; }
    public String bodyText { get; set; }
    public String test2 { get; set; }
    
    global static ConnectApi.UserDetail userMe;
    static {
        userMe = ConnectApi.ChatterUsers.getUser(Network.getNetworkId(), 'me');
    }
    
    public ConnectApiToolsForVF() {
    }
    
    public ConnectApiToolsForVF(ApexPages.StandardController controller) {
    }
    
    public Object TextSegmentType { get { return ConnectApi.MessageSegmentType.Text; } }
    public Object HashtagSegmentType { get { return ConnectApi.MessageSegmentType.Hashtag; } }
    public Object MentionSegmentType { get { return ConnectApi.MessageSegmentType.Mention; } }
    public Object LinkSegmentType { get { return ConnectApi.MessageSegmentType.Link; } }
    public Object EntityLinkSegmentType { get { return ConnectApi.MessageSegmentType.EntityLink; } }
    public Object InlineImageSegmentType { get { return ConnectApi.MessageSegmentType.InlineImage; } }
    public Object MarkupBeginSegmentType { get { return ConnectApi.MessageSegmentType.MarkupBegin; } }
    public Object MarkupEndSegmentType { get { return ConnectApi.MessageSegmentType.MarkupEnd; } }
    public Object FieldChangeSegmentType { get { return ConnectApi.MessageSegmentType.FieldChange; } }
    public Object FieldChangeNameSegmentType { get { return ConnectApi.MessageSegmentType.FieldChangeName; } }
    public Object FieldChangeValueSegmentType { get { return ConnectApi.MessageSegmentType.FieldChangeValue; } }
    public Object ResourceLinkSegmentType { get { return ConnectApi.MessageSegmentType.ResourceLink; } }
    public Object MoreChangesSegmentType { get { return ConnectApi.MessageSegmentType.MoreChanges; } }
    
    public PageReference likeFeedElement() {
        ConnectApi.ChatterFeeds.likeFeedElement(Network.getNetworkId(), this.referenceId);
        return null;
    }
    
    public PageReference likeComment() {
        ConnectApi.ChatterFeeds.likeComment(Network.getNetworkId(), this.referenceId);
        return null;
    }
    
    public PageReference unlike() {
        ConnectApi.ChatterFeeds.deleteLike(Network.getNetworkId(), this.referenceId);
        return null;
    }
    
    public static ConnectApi.UserDetail getMe() {
        return userMe;
    }
    
    public PageReference postCommentToFeedElement() {
        System.debug(this.referenceId);
        System.debug(this.bodyText);
        System.debug(this.test2);
        ConnectApi.ChatterFeeds.postCommentToFeedElement(Network.getNetworkId(), this.referenceId, this.bodyText);
        return null;
    }
    
}