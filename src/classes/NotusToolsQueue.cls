global class NotusToolsQueue implements Queueable, Database.AllowsCallouts {

    private static Notus_Defaults__c nd = Notus_Defaults__c.getInstance(); 
    public static boolean firstRun = true; 
    public List<Telemarketing__c> tels;
    
    public NotusToolsQueue(List<Telemarketing__c> tels) {
        this.tels = tels;
    }
    
///////////////////////////////////////////////////////////////////////////////////////////////
//****************** QUEUEABLE SECTION ******************//
///////////////////////////////////////////////////////////////////////////////////////////////
    
    public void execute(QueueableContext context) {
        
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        Http http = new Http();
        HTTPResponse res = new HTTPResponse();
        
        for(Telemarketing__c tel : tels) { 
            //Create new objects to JSON serialize (new notus lead)        
            List<Phone> phones = new List<Phone>{new Phone()};
            phones[0].number_x=tel.Phone__c;
            List<Email> emails = new List<Email>{new Email()};
            emails[0].email=tel.Email__c;
            NewLeadRequest leadRequest = new NewLeadRequest();
            leadRequest.firstName=tel.first_name__c;
            leadRequest.lastName=tel.last_name__c;
            leadRequest.phones=phones;
            leadRequest.emails=emails;
            leadRequest.mediatorHash=nd.Mediator_Hash__c;
            leadRequest.sourceHash=nd.Source_Hash__c;
            leadRequest.foreignUserId=tel.Id;
            
            //Serialize object to JSON
            String requestBody = JSON.serialize(leadRequest);
            //Number is reserved by salesforce. Replace to match JSON model
            requestBody = requestBody.replaceAll('\"number_x\":', '\"number\":');

            //Create new HttpRequest and HTTPResponse objects                       
            req.setEndpoint('callout:notus/hamster/v1/proposalLeads');
            req.setBody(requestBody);
            req.setHeader('Content-Type','application/json');
            
            //Callout and deserialize response into Response object
            Response newLead = new Response();
            
            try {
                res = http.send(req);
                newLead = (Response)JSON.deserializeStrict(res.getBody(), Response.class);
                system.debug(newLead);
            } catch (exception e) {
                system.debug('Notus New Lead Callout Exception: ' + e.getMessage());
            }
         
            //Create new object to JSON serialize (case request)
            NewCaseRequest caseRequest = new NewCaseRequest();
            caseRequest.mediatorHash = nd.Mediator_Hash__c;
            caseRequest.proposalLeadHash = newLead.Hash;
            caseRequest.sourceHash = nd.Source_Hash__c;
            caseRequest.foreignUserId = tel.Id;
            caseRequest.productHash = nd.Product_Hash__c;
            
            //Serialize object to JSON
            requestBody = JSON.serialize(caseRequest);
            
            //Edit HttpRequest properties
            req.setEndpoint('callout:notus/hamster/v1/proposalCases');
            req.setBody(requestBody);
            req.setHeader('Content-Type','application/json');
            
            //Callout and deserialize response into NewLeadResponse object
            Response newCase = new Response();
            try {
                res = http.send(req);
                newCase = (Response)JSON.deserializeStrict(res.getBody(), Response.class);
                system.debug(newCase);
                
            } catch (exception e) {
                system.debug('Notus New Case Callout Exception: ' + e.getMessage());
            }
            
            tel.Notus_Id__c = newCase.hash; 
            tel.Notus_Status__c = newCase.statusSystemName;
            tel.Notus_Stage__c = newCase.stage;             
        } 
        update tels;                                  
    }          
        
////////////////////////////////////////////////////////////////////////////////////////////
//****************** INTERNAL CLASSES ******************//
////////////////////////////////////////////////////////////////////////////////////////////  
       
    public class NewLeadRequest {        
        String firstName;
        String lastName;
        List<Phone> phones;
        List<Email> emails;
        String mediatorHash;
        String sourceHash;
        String foreignUserId;                
    }
    
    public class NewCaseRequest {
        String mediatorHash;
        String proposalLeadHash;
        String sourceHash;
        String foreignUserId;
        String productHash;
    }
    
    public class Phone {  
        String number_x;      
    }
    
    public class Email {      
        String email;
    }
    
    public class Response {        
        String href;
        String hash;
        String statusSystemName;
        String stage;
        String message;
        String statusDateTime;
        String proposalLeadHash;
        String status;
    }
}