@IsTest(SeeAllData=true)
private class DirectoryViewControllerTest {

    static testMethod void myUnitTest() {
        PageReference pageRef = new PageReference('http://extradom.pl/DirectoryView');
        Test.setCurrentPage(pageRef);
        DirectoryViewController controller = new DirectoryViewController();
        controller.getDesigns();
        controller.getUsers();
        
        pageRef = new PageReference('http://extradom.pl/DirectoryView');
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('col', 'pozostale');
        controller = new DirectoryViewController();
        controller.getDesigns();
        controller.getUsers();
        
        pageRef = new PageReference('http://extradom.pl/DirectoryView');
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('col', 't');
        controller = new DirectoryViewController();
        controller.getDesigns();
        controller.getUsers();
    }
    
}