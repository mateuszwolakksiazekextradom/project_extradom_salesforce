@IsTest
private class OrderEmailBasicInfoController_Test {

    @IsTest
    private static void shouldReturnOrderData() {
        // given
        Account acc = new Account (Name = 'Test Name');
        insert acc;
        Order o = new Order (
                AccountId = acc.Id, EffectiveDate = System.now().date(), Status = 'Draft', Pricebook2Id = Test.getStandardPricebookId(),
                RecordTypeId = ConstUtils.ORDER_REGULAR_RT_ID
        );
        insert o;
        List<PricebookEntry> productEntries = [
                SELECT Id, Product2.Design__r.Type__c, Product2.Design__c, Product2.Family, Product2.Name FROM PricebookEntry WHERE Pricebook2Id = :Test.getStandardPricebookId()
        ];
        List<OrderItem> orderItems = new List<OrderItem> ();
        for (PricebookEntry productEntry : productEntries) {
            orderItems.add(new OrderItem(PricebookEntryId = productEntry.Id, Quantity = 1, Discount_Sales__c = 0, List_Price__c = 100, OrderId = o.Id, UnitPrice = 10));
        }
        insert orderItems;

        // when
        OrderEmailBasicInfoController ctrl = new OrderEmailBasicInfoController();
        ctrl.setOrderId(o.Id);
        OrderEmailBasicInfoController.OrderWrapper orderWrapper = ctrl.orderData;

        // then
        System.assertNotEquals(null, orderWrapper);
        System.assert(orderWrapper.totalAmount > 0);
        System.assertEquals(productEntries.size(), orderWrapper.orderItems.size());
        for (OrderEmailBasicInfoController.OrderItemWrapper oiw : orderWrapper.orderItems) {
            System.assert(oiw.totalPrice > 0);
        }
    }

    @TestSetup
    private static void setupData() {

        Product2 product1 = new Product2(Name = 'Test Product 1', Family = ConstUtils.PRODUCT_GIFT_FAMILY, IsActive = true);
        Product2 product2 = new Product2(Name = 'Test Product 2', Family = ConstUtils.PRODUCT_DESIGN_FAMILY, IsActive = true);
        insert new List<Product2> {product1, product2};

        PricebookEntry spe1 = new PricebookEntry(
                Product2Id = product1.Id, IsActive = true, Pricebook2Id = Test.getStandardPricebookId(), UnitPrice = 100
        );
        PricebookEntry spe2 = new PricebookEntry(
                Product2Id = product2.Id, IsActive = true, Pricebook2Id = Test.getStandardPricebookId(), UnitPrice = 200
        );
        insert new List<PricebookEntry> {spe1, spe2};
    }
}