public with sharing class DuplicateQuery { 
    public List<AggregateResult> Records {get; set;}
    public DuplicateQuery(){
        Records = [SELECT name, count(id) ile FROM account WHERE name LIKE '%@%' GROUP BY name HAVING COUNT(id) = 2 AND SUM(has_contact_with_phone_int__c) > 0 LIMIT 100];
    }
}