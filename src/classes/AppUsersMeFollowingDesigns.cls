@RestResource(urlMapping='/app/users/me/following/designs')
global class AppUsersMeFollowingDesigns {
    @HttpGet
    global static ExtradomApi.DesignResultPage doGet() {
        Integer p = Integer.valueOf(RestContext.request.params.get('p'));
        List<Id> ids = new List<Id>{};
        ConnectApi.FollowingPage followingPage = ConnectApi.ChatterUsers.getFollowings(Network.getNetworkId(), 'me', 'a06', p-1, 24);
        for(ConnectApi.Subscription sub : followingPage.following) {
            ids.add(sub.subject.id);
        }
        ExtradomApi.DesignResultPage designResultPage = new ExtradomApi.DesignResultPage([SELECT Id, Code__c, Full_Name__c, Type__c, Gross_Price__c, Current_Price__c, Thumbnail_Base_URL__c, Usable_Area__c, Sketchfab_ID__c, KW_Model__c FROM Design__c WHERE id IN :ids AND Status__c = 'Active']);
        if (followingPage.nextPageUrl!=null) {
            designResultPage.nextPageUrl = '/app/users/me/following/designs?p='+String.valueOf(p+1);
        }
        designResultPage.currentPageUrl = '/app/users/me/following/designs?p='+String.valueOf(p);
        
        if (UserInfo.getUserType()=='Guest') {
            RestContext.response.addHeader('Cache-Control', 'must-revalidate, max-age=0, s-maxage=3600');
        } else {
            RestContext.response.addHeader('Cache-Control', 'no-cache, must-revalidate, max-age=0, no-store, private, s-maxage=0');
        }
        
        return designResultPage;
    }
}