public with sharing class ConversationViewController {

    private final String conversationId;
    
    public ConversationViewController() {
        this.conversationId = ApexPages.currentPage().getParameters().get('id');
    }
    
    public void markConversationRead() {
        ConnectApi.ChatterMessages.markConversationRead(Network.getNetworkId(), this.conversationId, true);
    }
    
    public ConnectApi.ChatterConversation getConversation() {
        return ConnectApi.ChatterMessages.getConversation(Network.getNetworkId(), this.conversationId);
    }
    
    @RemoteAction
    public static ConnectApi.ChatterMessage replyToMessage(String text, String inReplyTo) {
        return ConnectApi.ChatterMessages.replyToMessage(Network.getNetworkId(), text, inReplyTo);
    }
    
    @RemoteAction
    public static String getSingleMessageConversationViewContent(ID messageId) {
        PageReference pr = Page.SingleMessageConversationView;
        Map<string, string> params = pr.getParameters();
        params.put('id', messageId);
        return pr.getContent().toString();
    }

}