public without sharing class RegisterViewController {

    public String email {get; set;}
    public String password {get; set {password = value == null ? value : value.trim(); } }
    public String confirmPassword {get; set { confirmPassword = value == null ? value : value.trim(); } }
    public String communityNickname {get; set { communityNickname = value == null ? value : value.trim(); } }
    public Boolean optInRegulations {get; set;}
    public Boolean optInMarketing {get; set;}
    public Boolean optInMarketingExtended {get; set;}
    public Boolean optInMarketingPartner {get; set;}
    public Boolean optInEmail {get; set;}
    public Boolean optInPhone {get; set;}
    public Boolean optInProfiling {get; set;}
    
    private String startUrl;
    private final String source;
    
    public RegisterViewController() {
        this.source = (System.currentPageReference().getParameters().get('source')!=null) ? System.currentPageReference().getParameters().get('source') : 'Community';
        if (System.currentPageReference().getParameters().get('startURL')!=null) {
            this.startUrl = System.currentPageReference().getParameters().get('startURL');
        } else if (System.currentPageReference().getHeaders().get('Referer')!=null && System.currentPageReference().getHeaders().get('Host')!=null) {
            String referer = System.currentPageReference().getHeaders().get('Referer').replaceFirst('(http|https)://', '');
            if (referer.startsWith(System.currentPageReference().getHeaders().get('Host'))) {
                String relativeRef = referer.replaceFirst(System.currentPageReference().getHeaders().get('Host'), '');
                if (!(relativeRef.startsWith('/LoginView') || relativeRef.startsWith('/RegisterView') || relativeRef.startsWith('/secur/logout.jsp'))) {
                    this.startUrl = relativeRef;
                }
            }
        }
        this.startUrl = this.startUrl==null?'/':this.startUrl;
    }
    
    public String getActiveStartUrl() {
        return this.startUrl;
    }
    
    public String getActiveSource() {
        return this.source;
    }

    public PageReference registerUser() {

        String profileId = null; // To be filled in by customer.
        String roleEnum = null; // To be filled in by customer.
        String accountId = ''; // To be filled in by customer.
        
        List<User> usersNickname = [SELECT Id FROM User WHERE communityNickname = :this.communityNickname LIMIT 1];
        if (usersNickname.size()>0) {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Wybrany nick jest już zajęty. Wybierz inny.');
            ApexPages.addMessage(msg);
            return null;
        }
        
        String userName = this.email;
        
        List<User> usersName = [SELECT Id FROM User WHERE username = :userName LIMIT 1];
        if (usersName.size()>0) {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Adres e-mail jest już powiązany z innym kontem użytkownika. Wybierz inny lub zaloguj się istniejącym kontem.');
            ApexPages.addMessage(msg);
            return null;
        }
        
        if (!Site.isValidUsername(userName)) {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Podany adres e-mail nie może zostać użyty. Wybierz inny.');
            ApexPages.addMessage(msg);
            return null;
        }
        
        if (this.password.length()<8) {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Hasło musi mieć co najmniej 8 znaków.');
            ApexPages.addMessage(msg);
            return null;
        }
        
        Boolean testalpha = false;
        Boolean testnumeric = false;
        for (String ch : password.split('')) {
            if (ch.isAlpha()) {
                testalpha = true;
            }
            if (ch.isNumeric()) {
                testnumeric = true;
            }
        }
        
        if (!(testalpha && testnumeric)) {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Hasło musi zawierać litery i cyfry');
            ApexPages.addMessage(msg);
            return null;
        }
        
        if (!Site.isValidUsername(userName)) {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Adres e-mail jest już powiązany z innym kontem użytkownika. Wybierz inny lub zaloguj się istniejącym kontem.');
            ApexPages.addMessage(msg);
            return null;
        }

        User u = new User();
        u.Username = userName;
        u.Email = email;
        u.LastName = communityNickname;
        u.CommunityNickname = communityNickname;
        u.ProfileId = profileId;
        
        // Site.validatePassword(u, password, confirmPassword);
        
        List<Contact> contacts = [SELECT Id, AccountId FROM Contact WHERE EmailId__c = :this.email AND EmailId__c != '' LIMIT 1];
        if (contacts.size()>0) {
            Contact c = contacts[0];
            accountId = c.AccountId;
            u.ContactId = c.Id;
        } else {
            Account a = new Account();
            a.Name = email;
            a.OwnerId = '005b0000000RxOP';
            a.AccountSource = 'Extradom.pl ('+this.source+')';
            a.Registration_Start_URL__c = this.startUrl.left(255);
            insert a;
            accountId = a.Id;
            Contact c = new Contact(FirstName='', LastName=email, Email=email, AccountId=a.Id, OwnerId='005b0000000RxOP');
            if (optInMarketing==true) {
                c.Opt_In_Marketing__c = true;
            }
            if (optInMarketingExtended==true) {
                c.Opt_In_Marketing_Extended__c = true;
            }
            if (optInMarketingPartner==true) {
                c.Opt_In_Marketing_Partner__c = true;
            }
            if (optInEmail==true) {
                c.Opt_In_Email__c = true;
                c.Opt_In_Email_Partner__c = true;
            }
            if (optInPhone==true) {
                c.Opt_In_Phone__c = true;
                c.Opt_In_Phone_Partner__c = true;
            }
            if (optInProfiling==true) {
                c.Opt_In_Profiling__c = true;
            }
            if (optInRegulations==true) {
                c.Opt_In_Regulations__c = true;
                c.Opt_In_Privacy_Policy__c = true;
            }
            insert c;
            u.ContactId = c.Id;
        }
        
        String userId = Site.createPortalUser(u, accountId, password);
        
        if (ApexPages.currentPage().getHeaders().containsKey('gaClientId')) {
            OrderMethods.updateGaClientId(accountId, ApexPages.currentPage().getHeaders().get('gaClientId'));
        }
        
        try {
            EntitySubscription sub = new EntitySubscription(NetworkId=Network.getNetworkId(), SubscriberId=userId, ParentId='005b00000031pACAAY');
            insert sub;
        } catch (Exception e) {
        }
        
        if (userId != null) {
            if (password != null && password.length() > 1) {
                return Site.login(userName, password, this.startUrl + (this.startUrl.contains('?') ? '&reg=true' : '?reg=true') + '&e=' + EncodingUtil.convertToHex(Crypto.generateDigest('MD5', Blob.valueOf(this.Email))));
            }
            else {
                PageReference page = System.Page.CommunitiesSelfRegConfirm;
                page.setRedirect(true);
                page.getParameters().put('reg','true');
                page.getParameters().put('e', EncodingUtil.convertToHex(Crypto.generateDigest('MD5', Blob.valueOf(this.Email))));
                return page;
            }
        }
        return null;
    }

}