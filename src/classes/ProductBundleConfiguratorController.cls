public with sharing class ProductBundleConfiguratorController {

    @AuraEnabled
    public static List<SelectOptionWrapper> getVersionValues() {
        List<SelectOptionWrapper> selectOptions = new List<SelectOptionWrapper> ();
        Map<String, String> picklists = Utils.getPicklistValues(OrderItem.sObjectType, 'Version__c');
        for (String value : picklists.keySet()) {
            selectOptions.add(new SelectOptionWrapper(value, picklists.get(value)));
        }
        return selectOptions;
    }
    
    @AuraEnabled
    public static List<SelectOptionWrapper> getDiscountTypes() {
        List<SelectOptionWrapper> selectOptions = new List<SelectOptionWrapper> ();
        Map<String, String> picklists = Utils.getPicklistValues(OrderItem.sObjectType, 'Discount_Type__c');
        for (String value : picklists.keySet()) {
            selectOptions.add(new SelectOptionWrapper(value, picklists.get(value)));
        }
        return selectOptions;
    }

    @AuraEnabled
    public static List<OrderItemWrapper> getOrderItems(String orderId) {
        List<OrderItemWrapper> orderItems = new List<OrderItemWrapper> ();
        Order o = getOrder(orderId);
        if (o != null && o.OrderItems.size() > 0) {
            for (OrderItem oi : o.OrderItems) {
                orderItems.add(new OrderItemWrapper(oi));
            }
        }
        return orderItems;
    }

    @AuraEnabled
    public static List<OrderItemWrapper> prepareOrderItem(String orderId, String pricebookEntryId, String designId) {
        List<OrderItemWrapper> orderItemWrappers = new List<OrderItemWrapper> ();
        PricebookEntry pe = getPricebookData(pricebookEntryId);
        Design__c design = getDesignData(designId);
        OrderItemWrapper oiw = new OrderItemWrapper(orderId, pe, design);
        orderItemWrappers.add(oiw);
        orderItemWrappers.addAll(getRelatedOrderItems(pe.Pricebook2Id, oiw));
        return orderItemWrappers;
    }

    @AuraEnabled
    public static String saveOrderItems(String orderId, String orderProducts) {
        Savepoint sp = Database.setSavepoint();
        try {
            List<OrderItem> orderItems = (List<OrderItem>) JSON.deserialize(orderProducts, List<OrderItem>.class);
            Order o = getOrder(orderId);
            if (o != null) {
                Map<Id, OrderItem> existingOIs = OrderUtils.getExistingOrderItems(orderItems);
                List<OrderItem> newOIs = OrderUtils.getNewOrderItems(orderItems);
                List<OrderItem> OIsToDelete = OrderUtils.getDeleteOrderItems(o.OrderItems, existingOIs);
                if (o.Pricebook2Id == null) {
                    o.Pricebook2Id = OrderUtils.getPricebookId(null);
                    update o;
                }
                delete OIsToDelete;
                update existingOIs.values();
                insert newOIs;
            }
        } catch (Exception e) {
            Database.rollback(sp);
            return e.getMessage();
        }
        return null;
    }
    
    public static Order getOrder(String orderId) {
        Order o = null;
        if (String.isNotBlank(orderId)) {
            List<Order> orders = [SELECT Id, Pricebook2Id,
                (
                        SELECT Id, Quantity, Product2.Id, Product2.Name, Product2.Family,
                                Product2.Max_Discount__c, Product2.Line_Description_Required__c,
                                Product2.Version_Required__c, Design__c,Design__r.Thumbnail_Full_URL__c,
                                Design__r.Usable_Area__c, Design__r.Code__c, Design__r.Type__c,
                                Design__r.Name, Changes_Agreement__c, Version__c, Description, Index__c,
                                TotalPrice, PricebookEntry.Id, PricebookEntry.UnitPrice,
                                UnitPrice, List_Price__c, OrderId, PricebookEntryId, Discount_Internal__c,
                                Discount_Sales__c, Discount_Supplier__c, Discount_Type__c
                        FROM OrderItems ORDER BY Index__c
                )
            FROM Order WHERE Id = :orderId];
            if (orders.size() > 0) {
                o = orders.get(0);
            }
        }
        return o;
    }

    public static PricebookEntry getPricebookData(String pricebookEntryId) {
        PricebookEntry pe = getPricebookEntriesData(new Set<String> {pricebookEntryId}).get(pricebookEntryId);
        return pe;
    }

    public static Map<Id, PricebookEntry> getPricebookEntriesData (Set<String> pricebookEntriesIds) {
        Map<Id, PricebookEntry> pes = new Map<Id, PricebookEntry> ();
        if (pricebookEntriesIds != null && pricebookEntriesIds.size() > 0) {
            pes = new Map<Id, PricebookEntry> ([
                    SELECT Id, UnitPrice, Product2.Id, Product2.Name, Product2.Family, Product2.Max_Discount__c,
                            Product2.Line_Description_Required__c, Product2.Version_Required__c, Pricebook2Id
                    FROM PriceBookEntry
                    WHERE Id in :pricebookEntriesIds AND Product2.IsActive = true AND IsActive = true
            ]);
        }
        return pes;
    }
    
    public static Design__c getDesignData(String designId) {
        Design__c design = getDesignsData(new Set<String> {designId}).get(designId);
        return design;
    }

    public static Map<Id, Design__c> getDesignsData(Set<String> designIds) {
        Map<Id, Design__c> designs = new Map<Id, Design__c> ();
        if (designIds != null && designIds.size() > 0) {
            designs = new Map<Id, Design__c> ([SELECT Id, Name, Preview__c, Thumbnail_Full_URL__c, Usable_Area__c, Code__c, Type__c FROM Design__c WHERE Id in: designIds]);
        }
        return designs;
    }

    public static List<OrderItemWrapper> getRelatedOrderItems(Id pricebookId, OrderItemWrapper oiw) {
        List<OrderItemWrapper> orderItemWrappers = new List<OrderItemWrapper> ();
        if (oiw.product.typeValue == ConstUtils.PRODUCT_DESIGN_FAMILY && oiw.design != null && String.isNotBlank(oiw.design.type)) {
            List<PricebookEntry> productEntries = [
                    SELECT Id, UnitPrice, Product2.Id, Product2.Name, Product2.Family, Product2.Max_Discount__c,
                            Product2.Line_Description_Required__c, Product2.Version_Required__c, Pricebook2Id
                    FROM PriceBookEntry
                    WHERE Product2.IsActive = true AND IsActive = true AND Product2.Default_Extra__c = true AND
                        Product2.Default_Extra_by_Design_Type__c INCLUDES (:oiw.design.type) AND Product2.Default_Extra_by_List_Price__c <= :oiw.originalListPrice AND
                        (Product2.Family = :ConstUtils.PRODUCT_GIFT_FAMILY OR Product2.Family = :ConstUtils.PRODUCT_EXTRA_FAMILY) AND
                        Pricebook2Id = :pricebookId
            ];
            for (PricebookEntry priceBookEntry : productEntries) {
                orderItemWrappers.add(new OrderItemWrapper(oiw.orderItem.OrderId, priceBookEntry, null));
            }
        }
        // start BSA
        if (oiw.product.typeValue == ConstUtils.PRODUCT_DESIGN_FAMILY && oiw.design != null && String.isNotBlank(oiw.design.type) && oiw.design.code != null && oiw.design.code.startsWith('WOK1023') && oiw.design.type == 'Dom') {
            List<PricebookEntry> productEntries = [
                    SELECT Id, UnitPrice, Product2.Id, Product2.Name, Product2.Family, Product2.Max_Discount__c,
                            Product2.Line_Description_Required__c, Product2.Version_Required__c, Pricebook2Id
                    FROM PriceBookEntry
                    WHERE Product2.IsActive = true AND IsActive = true AND Product2.ProductCode = 'Estimates' AND Pricebook2Id = :pricebookId
            ];
            for (PricebookEntry priceBookEntry : productEntries) {
                orderItemWrappers.add(new OrderItemWrapper(oiw.orderItem.OrderId, priceBookEntry, getDesignData(oiw.design.id)));
            }
        }
        // end BSA
        return orderItemWrappers;
    }
}