global class CallTaskProcessing {

    @future
    public static void processRecords(List<ID> taskIds)
    {
      List<Task> tasks = [SELECT Id, Caller_Called_Phone__c, WhoId FROM Task WHERE Id IN :taskIds];
      List<Task> tasksToUpdate = new List<Task>{};
      List<String> phonesToCheck = new List<String>{};
      for (Task t : tasks) {
          if (String.isNotBlank(t.Caller_Called_Phone__c) && String.isBlank(t.WhoId)) {
              phonesToCheck.add(t.Caller_Called_Phone__c.deleteWhitespace());
          }
      }
      if (!phonesToCheck.isEmpty()) {
          Map<String,Id> phonesContactMap = new Map<String,Id>{};
          for (Contact c : [SELECT Id, PhoneId__c FROM Contact WHERE PhoneId__c IN :phonesToCheck AND PhoneId__c != '']) {
              phonesContactMap.put(c.PhoneId__c, c.Id);
          }
          for (Task t : tasks) {
              if (String.isNotBlank(t.Caller_Called_Phone__c) && String.isBlank(t.WhoId)) {
                  if (phonesContactMap.containsKey(t.Caller_Called_Phone__c.deleteWhitespace())) {
                      t.WhoId = phonesContactMap.get(t.Caller_Called_Phone__c.deleteWhitespace());
                      tasksToUpdate.add(t);
                  }
              }
          }
          if (!tasksToUpdate.isEmpty()) {
              update tasksToUpdate;
          }
      }
    }
    
}