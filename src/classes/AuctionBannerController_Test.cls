/**
 * Created by grzegorz.dlugosz on 28.05.2019.
 */

@isTest
private class AuctionBannerController_Test {

    // single auction match
    @isTest
    private static void getSingleAuctionForBanner() {
        Offer__c offer = TestUtils.newOffer(new Offer__c(), true);

        Test.startTest();
        Offer__c returnedOffer = AuctionBannerController.getAuctionInfoSrv(offer.Id);
        Test.stopTest();

        System.assert(returnedOffer != null);
    }

    // no auction match
    @isTest
    private static void getNoAuctionForBanner() {
        Offer__c offer = TestUtils.newOffer(new Offer__c(), true);

        Test.startTest();
        Offer__c returnedOffer = AuctionBannerController.getAuctionInfoSrv(null);
        Test.stopTest();

        System.assertEquals(null, returnedOffer);
    }
}