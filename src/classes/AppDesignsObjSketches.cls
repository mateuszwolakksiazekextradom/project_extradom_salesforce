@RestResource(urlMapping='/app/designs/*/sketches')
global class AppDesignsObjSketches {
    @HttpPost
    global static Map<String,String> doPost(String email, String comments) {
        Pattern designIdPattern = Pattern.compile('/app/designs/([a-zA-Z0-9]+)/sketches');
        Matcher match = designIdPattern.matcher(RestContext.request.requestURI);
        match.matches();
        ID designId = match.group(1);
        Design__c d = [SELECT Id, Has_Only_Estimate_Materials__c FROM Design__c WHERE Id = :designId];
        String status = 'Not Started';
        if (d.Has_Only_Estimate_Materials__c==false && String.isBlank(comments)) {
            status = 'Completed';
        }
        Map<String,Id> data = OrderMethods.getWebLeadContactDetails('Sketches', '', email, email, null, 'Internal', true, true, 'No');
        Task t = new Task(subject='Zamówienie rysunków szczegółowych', Status=status, Design_Has_Only_Estimates__c=d.Has_Only_Estimate_Materials__c, Description=comments, Type='Question', WhoId=data.get('contactEmailId'), WhatId=d.Id, OwnerId=data.get('ownerId'), ActivityDate=System.Today());
        insert t;
        Map<String,String> result = new Map<String,String>{'result' => 'Dziękujemy za zainteresowanie. Rysunki zostaną przesłane na podany adres poczty e-mail.'};
        return result;
    }
}