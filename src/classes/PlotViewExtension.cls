public with sharing class PlotViewExtension {

    private final Plot__c p;
    public PlotViewExtension(ApexPages.StandardController stdController) {
        this.p = (Plot__c)stdController.getRecord();
    }
    
    public ConnectApi.Reference getMySubscriptionReference() {
        ConnectApi.FollowerPage followerPage = ConnectApi.Chatter.getFollowers(Network.getNetworkId(), this.p.Id);
        for (ConnectApi.Subscription subscription : followerPage.followers) {
            if (subscription.subject.mySubscription != null) {
                return subscription.subject.mySubscription;
            }
        }
        return null;
    }
    
    public ConnectApi.FollowerPage getFollowerPage() {
        return ConnectApi.Chatter.getFollowers(Network.getNetworkId(), this.p.Id);
    }

}