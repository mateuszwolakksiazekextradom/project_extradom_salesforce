public with sharing class ConstructionViewExtension {

    private final Construction__c c;
    public ConstructionViewExtension(ApexPages.StandardController stdController) {
        this.c = (Construction__c)stdController.getRecord();
    }
    
    public ConnectApi.FeedElementPage getFeedElementPage() {
        return ConnectApi.ChatterFeeds.getFeedElementsFromFeed(Network.getNetworkId(), ConnectApi.FeedType.Record, c.Id);
    }

}