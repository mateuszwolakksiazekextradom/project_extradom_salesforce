public with sharing class AccountOrderTreeController {

    @AuraEnabled
    public static List<Order> getOrders (String accountId) {
        List<Order> orders = new List<Order> ();

        if (String.isNotBlank(accountId)) {
            orders = [
                    SELECT Id, OrderNumber, toLabel(Status), Sent_Date__c, PoDate, isReductionOrder,
                    (
                        SELECT Id, Product2.Name, toLabel(Version__c), Quantity, AvailableQuantity FROM OrderItems
                    )
                    FROM Order WHERE AccountId = :accountId ORDER BY EffectiveDate DESC
            ];
        }
        return orders;
    }
}