/**
 * Created by grzegorz.dlugosz on 10.05.2019.
 */
@IsTest
private class DesignOrderSentMonthBatch_Test {

    @IsTest
    private static void runSentMonthCalculation() {
        // Prepare Test Data
        Integer quantityToInsert = 10;

        Supplier__c supplier = TestUtils.newSupplier(new Supplier__c(), true);

        List<Design__c> designs = TestUtils.newDesigns(new Design__c(Supplier__c = supplier.Id), quantityToInsert, true);
        List<Product2> product2List = new List<Product2>();
        // 1 design 1 product
        for (Integer i = 0; i < quantityToInsert; i++) {
            String prodName = 'Test Product2 ' + String.valueOf(i);
            Product2 prod2 = new Product2(Design__c = designs.get(i).Id, Name = prodName, Family = ConstUtils.PRODUCT_DESIGN_FAMILY);
            product2List.add(prod2);
        }

        insert product2List;

        List<PricebookEntry> pbeList = new List<PricebookEntry>();

        for (Integer i = 0; i < quantityToInsert; i++) {
            PricebookEntry pbe = new PricebookEntry(Product2Id = product2List.get(i).Id, IsActive = true, Pricebook2Id = Test.getStandardPricebookId(), UnitPrice = 100);
            pbeList.add(pbe);
        }

        insert pbeList;

        Account acc = TestUtils.newAccount(new Account(), true);

        List<Order> ordersList = TestUtils.newOrders(new Order(AccountId = acc.Id), quantityToInsert, true);
        List<OrderItem> orderItemList = new List<OrderItem>();

        for (Integer i = 0; i < quantityToInsert; i++) {
            OrderItem ordrItem = new OrderItem(OrderId = ordersList.get(i).Id,
                    UnitPrice = 0,
                    List_Price__c = 400,
                    Product_Family__c = ConstUtils.PRODUCT_DESIGN_FAMILY,
                    Quantity = (i+1),
                    PricebookEntryId = pbeList.get(i).Id);
            orderItemList.add(ordrItem);
        }

        insert orderItemList;

        // activate orders
        for (Order ordr : ordersList) {
            ordr.Status = ConstUtils.ORDER_ACCEPTED_STATUS;
        }

        update ordersList;

        // Run the batch
        Test.startTest();
        DesignOrderSentMonthBatch btch = new DesignOrderSentMonthBatch();
        Database.executeBatch(btch);
        Test.stopTest();

        List<Design__c> designsProcessed = [SELECT Id, Sent_Month__c, Name FROM Design__c ORDER BY Sent_Month__c];

        // Assert results
        for (Integer i = 0; i < quantityToInsert; i++) {
            Decimal expectedSentPerMonth = (i+1) / Decimal.valueOf(24);
            System.assertEquals(designsProcessed.get(i).Sent_Month__c, expectedSentPerMonth);
        }
    }

    @IsTest
    private static void runSentMonthCalculationQuantityZero() {
        // Prepare Test Data
        Integer quantityToInsert = 1;

        Supplier__c supplier = TestUtils.newSupplier(new Supplier__c(), true);

        Design__c design = TestUtils.newDesign(new Design__c(Supplier__c = supplier.Id), true);
        List<Product2> prod2List = TestUtils.newProducts2(new Product2(Design__c = design.Id, Name = 'Test Product 2', Family = ConstUtils.PRODUCT_DESIGN_FAMILY), 2, true);

        PricebookEntry pbe1 = new PricebookEntry(Product2Id = prod2List.get(0).Id, IsActive = true, Pricebook2Id = Test.getStandardPricebookId(), UnitPrice = 100);
        List<PricebookEntry> pbeList = new List<PricebookEntry>{pbe1};
        insert pbeList;

        Account acc = TestUtils.newAccount(new Account(), true);

        Order ordr1 = new Order(Name = 'Test Order 1', RecordTypeId = ConstUtils.ORDER_REGULAR_RT_ID, Status = ConstUtils.ORDER_DRAFT_STATUS, AccountId = acc.Id,
                                PoDate = System.today().addMonths(-1), EffectiveDate = System.today().addMonths(-1), Pricebook2Id = Test.getStandardPricebookId());
        // prepare order 1
        insert ordr1;
        OrderItem ordrItem1 = new OrderItem(OrderId = ordr1.Id,
                UnitPrice = 0,
                List_Price__c = 400,
                Product_Family__c = ConstUtils.PRODUCT_DESIGN_FAMILY,
                Quantity = 1,
                PricebookEntryId = pbeList.get(0).Id);

        insert ordrItem1;
        // order 1 activated
        ordr1.Status = ConstUtils.ORDER_ACCEPTED_STATUS;
        update ordr1;

        // prepare order 2 - reduction for order 1
        Order ordr2 = new Order(Name = 'Test Order 2', RecordTypeId = ConstUtils.ORDER_REDUCTION_RT_ID, Status = ConstUtils.ORDER_DRAFT_STATUS, AccountId = acc.Id,
                PoDate = System.today().addMonths(-1), EffectiveDate = System.today().addMonths(-1), Pricebook2Id = Test.getStandardPricebookId(),
                IsReductionOrder = true, OriginalOrderId = ordr1.Id);
        insert ordr2;

        OrderItem ordrItem2 = new OrderItem(OrderId = ordr2.Id,
                Product_Family__c = ConstUtils.PRODUCT_DESIGN_FAMILY,
                Quantity = -1,
                OriginalOrderItemId = ordrItem1.Id);
        Insert ordrItem2;
        // activate reduction order
        ordr2.Status = ConstUtils.ORDER_ACCEPTED_STATUS;
        update ordr2;

        // Run the batch
        Test.startTest();
        DesignOrderSentMonthBatch btch = new DesignOrderSentMonthBatch();
        Database.executeBatch(btch);
        Test.stopTest();

        List<Design__c> designsProcessed = [SELECT Id, Sent_Month__c, Name FROM Design__c ORDER BY Sent_Month__c];

        // Assert results
        System.assertEquals(designsProcessed.get(0).Sent_Month__c, 0); // sum of quantity should now be 0 since we add 1 product in 1st order and then reduce it by 1 in second order
    }

    @IsTest
    private static void executeScheduler() {
        Test.startTest();
        DesignOrderSentMonthBatch btchToSchedule = new DesignOrderSentMonthBatch();
        String CRON_EXP = '0 0 23 * * ?';
        String jobId = System.schedule('testSchedulerOfBatch', CRON_EXP, btchToSchedule);
        Test.stopTest();

        CronTrigger ct = [SELECT id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        System.assert(ct != null);
    }
}