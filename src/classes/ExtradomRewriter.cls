global with sharing class ExtradomRewriter implements Site.UrlRewriter {

    global PageReference mapRequestUrl(PageReference friendlyUrl) {

        String url = friendlyUrl.getUrl();
        Map<String, String> params = friendlyUrl.getParameters().clone();
        friendlyUrl.getParameters().clear();
        String path = friendlyUrl.getUrl();
        
        Map<String,String> staticRefs = new Map<String,String>{'/projekty-energo-plus' => '/EnergoPlusView', '/wiosenna-wyprzedaz' => '/PromocjaWiosenna', '/zamow-prezentacje-projektow' => '/PresentationOrderView', '/zamow-rysunki-i-kosztorysy' => '/SketchesEstimatesView', '/zmiany-w-projekcie' => '/AdaptationView', '/aplikacja-projekty-domow' => '/LandingExtradomApp', '/extra-projekty' => '/FollowingDesignsView', '/konkurs-malarski' => '/CompetitionView', '/kod-promocyjny-regulamin' => '/PageView?id=a0Gb000000CW2MS', '/praktyczny-poradnik' => '/PageView?id=a0Gb000000CSTUQ', '/regulamin-promocji-letniej' => '/PageView?id=a0Gb000000AYAGq', '/promocja-letnia' => '/PageView?id=a0Gb000000AYAGg', '/promocja-pilkarska' => '/PageView?id=a0Gb000000AWphA', '/regulamin-promocji-pilkarskiej' => '/PageView?id=a0Gb000000AWphF', '/regulamin-akcja-promocyjna-500' => '/PageView?id=a0Gb000000AWWAf', '/regulamin-akcji-promocyjnej-500' => '/PageView?id=a0Gb000000AEfZJ', '/regulamin-promocji-na-dzien-dziecka' => '/PageView?id=a0Gb000000AW10p', '/promocja-na-dzien-dziecka' => '/PageView?id=a0Gb000000AW10f', '/regulamin-konkursu-rodzina' => '/PageView?id=a0Gb000000AVEAH', '/firmy' => '/CompaniesView', '/porownywarka-projektow' => '/DesignComparisonView', '/dzialki' => '/PlotMapView', '/zostan-naszym-partnerem' => '/PageView?id=a0Gb000000ATNSa', '/promocja-majowa' => '/PageView?id=a0Gb000000ATfej', '/regulamin-promocji-majowej' => '/PageView?id=a0Gb000000ATffw', '/mapa' => '/ConstructionMapView', '/adaptacja-projektu' => '/PageView?id=a0Gb00000048trA', '/ustawienia' => '/SettingsView', '/spolecznosc' => '/HomeView', '/pytania-i-odpowiedzi' => '/QAView', '/porady-ekspertow' => '/GroupView?id=0F9b0000000LcabCAC', '/tagi' => '/DirectoryViewTopic', '/powiadomienia' => '/NewsView', '/powiadomienia/do-mnie' => '/NewsToMeView', '/powiadomienia/pytania' => '/NewsQuestionsView', '/powiadomienia/galeria' => '/NewsGalleryView', '/powiadomienia/zakladki' => '/BookmarksView', '/powiadomienia/galeria-zakladek' => '/BookmarksGalleryView', '/poczta' => '/ConversationsView', '/o-nas' => '/PageView?id=a0Gb000000499vP', '/dla-specjalistow' => '/PageView?id=a0Gb000000AEfaO', '/regulamin' => '/PageView?id=a0Gb0000005sXtp', '/polityka-prywatnosci' => '/PageView?id=a0Gb000000AEEZF', '/pomoc' => '/HelpView', '/opinia' => '/PageView?id=a0Gb000000Ac1jB', '/mapa-strony' => '/DirectoryView', '/kontakt' => '/ContactView', '/projekty-osiedli' => '/PageView?id=a0Gb000000AaFg1', '/starter-do-projektu-domu' => '/PageView?id=a0Gb000000499oP', '/bezplatne-projekty' => '/PageView?id=a0Gb0000006LVwJ', '/jak-zamowic-projekt-domu' => '/PageView?id=a0Gb0000005sXbI', '/katalog' => '/CatalogueOrderView', '/rekomendowany-przez-najlepszych-architektow' => '/PageView?id=a0Gb000000AEaVZ', '/kup-projekt-10-x-korzystniej' => '/PageView?id=a0Gb000000499vZ', '/zmiany-w-projekcie' => '/DesignChangeView', '/kolekcje-projektow' => '/PageView?id=a0Gb000000499vj', '/widget-house-plans-6-e-projekty-pl' => '/widget_house_plans_6_e_projekty_pl', '/promocja-wielkanocna' => '/PageView?id=a0Gb000000B69lR', '/godziny-otwarcia' => '/PageView?id=a0Gb000000B69lM', '/regulamin-promocji-wielkanocnej' => '/PageView?id=a0Gb000000B69vL', '/pewny-dach' => '/BrassLeadView', '/pewny-dach-dziekujemy' => '/BrassLeadViewComplete', '/promocja-wiosenna' => '/PageView?id=a0Gb000000B6ySw', '/regulamin-promocji-wiosennej' => '/PageView?id=a0Gb000000B6yT1', '/poradnik-ogrodowy' => '/PageView?id=a0Gb000000B6yTp', '/promocja-weekendowa' => '/PromocjaWeekendowa', '/promocja-czerwiec-bbq' => '/PromocjaCzerwiecBBQ', '/promocja-czerwcowa-z-hamakami' => '/PromocjaCzerwcowaZHamakami', '/promocja-z-walizkami-lipiec' => '/PromocjaZWalizkamiLipiec', '/promocja-lipcowa-z-mopami' => '/PromocjaLipcowaZMopami', '/konkurs-wygraj-wentylacje' => '/PromocjaWygrajWentylacje', '/regulamin-promocji-weekendowej' => '/PageView?id=a0Gb000000B7mDk'};
        
        if (staticRefs.containsKey(path)) {
            // static
            return new PageReference(staticRefs.get(path));
        } else if (url.startsWith('/znajdz-projekt?')) {
            return new PageReference(url.replaceFirst('/znajdz-projekt','/DesignSearchView'));
        } else if (url.startsWith('/zamowienie?')) {
            return new PageReference(url.replaceFirst('/zamowienie','/DesignOrderView'));
        } else if (url.startsWith('/budowy')) {
            return new PageReference(url.replaceFirst('/budowy','/ConstructionMapView'));
        } else if (url.startsWith('/porada-')) {
            // FeedElementExpertGroupView
            String id = url.replaceFirst('/porada-.*?(?=0D5[a-zA-Z0-9]{15})','').replaceFirst('\\?.*','');
            if (id.length()==18) {
                return new PageReference('/FeedElementExpertGroupView?id=' + id);
            } else {
                return new PageReference('/feedelementexpertgroupview_not_found');
            }
        } else if (url.startsWith('/post-')) {
            // FeedElementView
            String id = url.replaceFirst('/post-.*?(?=0D5[a-zA-Z0-9]{15})','').replaceFirst('\\?.*','');
            if (id.length()==18) {
                return new PageReference('/FeedElementView?id=' + id);
            } else {
                return new PageReference('/feedelementview_not_found');
            }
        } else if (url.startsWith('/pytanie-')) {
            // FeedElementQuestionView
            String id = url.replaceFirst('/pytanie-.*?(?=0D5[a-zA-Z0-9]{15})','').replaceFirst('\\?.*','');
            if (id.length()==18) {
                return new PageReference('/FeedElementQuestionView?id=' + id);
            } else {
                return new PageReference('/feedelementquestionview_not_found');
            }
        } else if (url.startsWith('/rozmowa-')) {
            // ConversationView
            String id = url.replaceFirst('/rozmowa-.*?(?=03M[a-zA-Z0-9]{15})','').replaceFirst('\\?.*','');
            if (id.length()==18) {
                return new PageReference('/ConversationView?id=' + id);
            } else {
                return new PageReference('/conversationview_not_found');
            }
        } else if (url.startsWith('/projekt-')) {
            // DesignView
            String code = url.replaceFirst('/projekt-.*?(?=[a-zA-Z]{3}[1-9][0-9]{3})','').replaceFirst('\\?.*','');
            if (code.length()==7) {
                for (Design__c d : [SELECT Id, Master_Design__c, Status__c FROM Design__c WHERE Code__c = :code LIMIT 1]) {
                    if (d.Status__c == 'Redirected') {
                        return new PageReference('/DesignView?id=' + d.Master_Design__c);
                    } else {
                        return new PageReference('/DesignView?id=' + d.Id);
                    }                   
                }
                return new PageReference('/designview_not_found');
            } else {
                return new PageReference('/designview_not_found');
            }
        } else if (url.startsWith('/jsondesignviewbycode')) {
            // jsondesignview
            if (params.containsKey('code')) {
                String code = params.get('code');
                if (code.length()==7) {
                    for (Design__c d : [SELECT Id FROM Design__c WHERE Code__c = :code LIMIT 1]) {
                        return new PageReference('/jsondesignview?id=' + d.Id);
                    }
                    return new PageReference('/jsondesignviewbycode_not_found');
                } else {
                    return new PageReference('/jsondesignviewbycode_invalid_code');
                }
            } else {
                return new PageReference('/jsondesignviewbycode_code_not_specified');
            }
        } else if (path.startsWith('/projekty-') || path.startsWith('/najnowsze-projekty-') || path == '/promocje' || path == '/top40') {
            // DesignSearchView
            for (Page__c p : [SELECT Id FROM Page__c WHERE URL__c = :path LIMIT 1]) {
                return new PageReference('/DesignSearchView?id=' + p.Id);
            }
            return new PageReference('/designsearchview_not_found');
        } else if (url.startsWith('/w-budowie-projekt-')) {
            // DesignSocialView
            String code = url.replaceFirst('/w-budowie-projekt-.*?(?=[a-zA-Z]{3}[1-9][0-9]{3})','').replaceFirst('\\?.*','');
            if (code.length()==7) {
                for (Design__c d : [SELECT Id FROM Design__c WHERE Code__c = :code LIMIT 1]) {
                    return new PageReference('/DesignSocialView?id=' + d.Id);
                }
                return new PageReference('/designsocialview_not_found');
            } else {
                return new PageReference('/designsocialview_not_found');
            }
        } else if (url.startsWith('/tag-')) {
            // TopicView
            String topicslug = url.replaceFirst('/tag-','').replaceFirst('\\?.*','');
            Map<String,String> topics = new Map<String,String>{};
            for (Topic t : [SELECT Id, Name FROM Topic WHERE NetworkId = :Network.getNetworkId() LIMIT 200]) {
                String slug = t.Name;
                slug = slug.toLowerCase('pl').replaceAll('ą','a').replaceAll('ć','c').replaceAll('ę','e').replaceAll('ł','l').replaceAll('ń','n').replaceAll('ó','o').replaceAll('ś','s').replaceAll('ż','z').replaceAll('ź','z').replaceAll(',','').replaceAll(' ','-');
                topics.put(slug,t.Id);
            }
            if (topics.containsKey(topicslug)) {
                return new PageReference('/TopicView?id=' + topics.get(topicslug));
            } else {
                return new PageReference('/topicview_not_found');
            }
        } else if (url.startsWith('/zdjecia-taga-')) {
            // TopicGalleryView
            String topicslug = url.replaceFirst('/zdjecia-taga-','').replaceFirst('\\?.*','');
            Map<String,String> topics = new Map<String,String>{};
            for (Topic t : [SELECT Id, Name FROM Topic WHERE NetworkId = :Network.getNetworkId() LIMIT 200]) {
                String slug = t.Name;
                slug = slug.toLowerCase('pl').replaceAll('ą','a').replaceAll('ć','c').replaceAll('ę','e').replaceAll('ł','l').replaceAll('ń','n').replaceAll('ó','o').replaceAll('ś','s').replaceAll('ż','z').replaceAll('ź','z').replaceAll(',','').replaceAll(' ','-');
                topics.put(slug,t.Id);
            }
            if (topics.containsKey(topicslug)) {
                return new PageReference('/TopicGalleryView?id=' + topics.get(topicslug));
            } else {
                return new PageReference('/topicgalleryview_not_found');
            }
        } else if (url.startsWith('/zdjecia-projektu-')) {
            // DesignSocialGalleryView
            String code = url.replaceFirst('/zdjecia-projektu-.*?(?=[a-zA-Z]{3}[1-9][0-9]{3})','').replaceFirst('\\?.*','');
            if (code.length()==7) {
                for (Design__c d : [SELECT Id FROM Design__c WHERE Code__c = :code LIMIT 1]) {
                    return new PageReference('/DesignSocialGalleryView?id=' + d.Id);
                }
                return new PageReference('/designsocialgalleryview_not_found');
            } else {
                return new PageReference('/designsocialgalleryview_not_found');
            }
        } else if (url.startsWith('/profil-')) {
            // ProfileView
            String nicknameEncoded = url.replaceFirst('/profil-','').replaceFirst('\\?.*','');
            if (nicknameEncoded.length()>0) {
                for (User u : [SELECT Id, Nickname_Encoded__c FROM User WHERE Nickname_Encoded__c = :nicknameEncoded AND IsActive = true LIMIT 1]) {
                    return new PageReference('/ProfileView?id=' + u.Id);
                }
                return new PageReference('/profileview_not_found');
            } else {
                return new PageReference('/profileview_not_found');
            }
        } else if (url.startsWith('/zdjecia-profilu-')) {
            // ProfileGalleryView
            String nicknameEncoded = url.replaceFirst('/zdjecia-profilu-','').replaceFirst('\\?.*','');
            if (nicknameEncoded.length()>0) {
                for (User u : [SELECT Id, Nickname_Encoded__c FROM User WHERE Nickname_Encoded__c = :nicknameEncoded AND IsActive = true LIMIT 1]) {
                    return new PageReference('/ProfileGalleryView?id=' + u.Id);
                }
                return new PageReference('/profilegalleryview_not_found');
            } else {
                return new PageReference('/profilegalleryview_not_found');
            }
        } else if (url.startsWith('/dzialka-')) {
            // PlotView
            String plotname = url.replaceFirst('/dzialka-','').replaceFirst('\\?.*','');
            if (plotname.length()>0) {
                for (Plot__c p : [SELECT Id FROM Plot__c WHERE Canonical_Name__c = :plotname LIMIT 1]) {
                    return new PageReference('/PlotView?id=' + p.Id);
                }
                return new PageReference('/plotview_not_found');
            } else {
                return new PageReference('/plotview_not_found');
            }
        } else if (url.startsWith('/wizualizacja-3d-projektu-')) {
            // ModelView
            String id = url.replaceFirst('/wizualizacja-3d-projektu-.*?(?=a0z[a-zA-Z0-9]{15})','').replaceFirst('\\?.*','');
            if (id.length()==18) {
                return new PageReference('/ModelView?id=' + id);
            } else {
                return new PageReference('/modelview_not_found');
            }
        } else if (url.startsWith('/katalog-projektow-')) {
            // DirectoryViewDesign
            String col = url.replaceFirst('/katalog-projektow-','').replaceFirst('\\?.*','');
            if ((col.isAsciiPrintable() && col.length()==1) || col=='a-2' || col=='c-2' || col=='e-2' || col=='l-2' || col=='n-2' || col=='o-2' || col=='s-2' || col=='z-2' || col=='z-3' || col=='pozostale') {
                return new PageReference('/DirectoryViewDesign?col=' + col);
            } else {
                return new PageReference('/directoryviewdesign_not_found');
            }
        } else if (url.startsWithIgnoreCase('/ProfileView?') || url.startsWithIgnoreCase('/ProfileGalleryView?')) {
            if (params.containsKey('id')) {
                String userid = params.get('id');
                for (User u : [SELECT Id FROM User WHERE Id = :userid AND IsActive=false LIMIT 1]) {
                    return new PageReference('/user_not_found_inactive');
                }
            }
        } else {
            // PageView
            for (Page__c p : [SELECT Id FROM Page__c WHERE URL__c = :path LIMIT 1]) {
                return new PageReference('/PageView?id=' + p.Id);
            }
        }
        
        return null;
        
    }

    global List<PageReference> generateUrlFor(List<PageReference> salesforceUrls){

        List<PageReference> friendlyUrls = new List<PageReference>();
        List<Id> designIds = new List<Id>();
        List<Id> topicIds = new List<Id>();
        List<Id> userIds = new List<Id>();
        List<Id> plotIds = new List<Id>();
        List<Id> modelIds = new List<Id>();
        List<String> pageUrls = new List<String>();
        
        for (PageReference salesforceUrl : salesforceUrls){
            String url = salesforceUrl.getUrl();
            Map<String,String> params = salesforceUrl.getParameters();
            params.remove('firstParam');
            if ((url.startsWith('/DesignView?') || url.startsWith('/DesignSocialView?') || url.startsWith('/DesignSocialGalleryView?')) && params.containsKey('id')) {
                String id = params.get('id');
                try {
                    designIds.add(id);
                } catch(Exception e) {
                }
            } else if ((url.startsWith('/TopicView?') || url.startsWith('/TopicGalleryView?')) && params.containsKey('id')) {
                String id = params.get('id');
                try {
                    topicIds.add(id);
                } catch(Exception e) {
                }
            } else if ((url.startsWith('/ProfileView?') || url.startsWith('/ProfileGalleryView?')) && params.containsKey('id')) {
                String id = params.get('id');
                try {
                    userIds.add(id);
                } catch(Exception e) {
                }
            } else if (url.startsWith('/PlotView?') && params.containsKey('id')) {
                String id = params.get('id');
                try {
                    plotIds.add(id);
                } catch(Exception e) {
                }
            } else if (url.startsWith('/ModelView?') && params.containsKey('id')) {
                String id = params.get('id');
                try {
                    modelIds.add(id);
                } catch(Exception e) {
                }
            } else if (url.startsWith('/DesignSearchView')) {
                pageUrls.add(url);
            }
        }

        Map<Id,Design__c> designsMap = new Map<Id,Design__c>();
        Map<Id,Topic> topicsMap = new Map<Id,Topic>();
        Map<Id,User> usersMap = new Map<Id,User>();
        Map<Id,Plot__c> plotsMap = new Map<Id,Plot__c>();
        Map<Id,Model__c> modelsMap = new Map<Id,Model__c>();
        Map<String,Page__c> pagesMap = new Map<String,Page__c>();
        
        if (!designIds.isEmpty()) {
            designsMap = new Map<Id,Design__c>([SELECT Id, Canonical_URL__c, Code__c FROM Design__c WHERE Id IN :designIds]);
        }
        if (!topicIds.isEmpty()) {
            topicsMap = new Map<Id,Topic>([SELECT Id, Name FROM Topic WHERE Id IN :topicIds]);
        }
        if (!userIds.isEmpty()) {
            usersMap = new Map<Id,User>([SELECT Id, Nickname_Encoded__c FROM User WHERE Id IN :userIds]);
        }
        if (!plotIds.isEmpty()) {
            plotsMap = new Map<Id,Plot__c>([SELECT Id, Canonical_Name__c FROM Plot__c WHERE Id IN :plotIds]);
        }
        if (!modelIds.isEmpty()) {
            modelsMap = new Map<Id,Model__c>([SELECT Id, Design__r.Canonical_URL__c FROM Model__c WHERE Id IN :modelIds]);
        }
        if (!pageUrls.isEmpty()) {
            for (Page__c p : [SELECT Id, VF_URL__c, URL__c FROM Page__c WHERE VF_URL__c IN :pageUrls]) {
                pagesMap.put(p.VF_URL__c,p);
            }
        }
        
        Map<String,String> staticRefs = new Map<String,String>{'/DesignSearchView?type=1' => '/', '/ConstructionMapView' => '/mapa', '/SettingsView' => '/ustawienia', '/HomeView' => '/spolecznosc', '/QAView' => '/pytania-i-odpowiedzi', '/GroupView?id=0F9b0000000LcabCAC' => '/porady-ekspertow', '/DirectoryViewTopic' => '/tagi', '/NewsView' => '/powiadomienia', '/NewsToMeView' => '/powiadomienia/do-mnie', '/NewsQuestionsView' => '/powiadomienia/pytania', '/NewsGalleryView' => '/powiadomienia/galeria', '/BookmarksView' => '/powiadomienia/zakladki', '/BookmarksGalleryView' => '/powiadomienia/galeria-zakladek', '/ConversationsView' => '/poczta', '/DesignSearchView' => '/', '/PageView?id=a0Gb000000499vP' => '/o-nas', '/PageView?id=a0Gb000000AEfaO' => '/dla-specjalistow', '/PageView?id=a0Gb0000005sXtp' => '/regulamin', '/PageView?id=a0Gb000000AEEZF' => '/polityka-prywatnosci', '/HelpView' => '/pomoc', '/PageView?id=a0Gb000000Ac1jB' => '/opinia', '/DirectoryView' => '/mapa-strony', '/PageView?id=a0Gb000000AaFg1' => '/projekty-osiedli', '/PageView?id=a0Gb000000499oP' => '/starter-do-projektu-domu', '/PageView?id=a0Gb0000006LVwJ' => '/bezplatne-projekty', '/PageView?id=a0Gb0000005sXbI' => '/jak-zamowic-projekt-domu', '/CatalogueOrderView' => '/katalog', '/PageView?id=a0Gb000000AEaVZ' => '/rekomendowany-przez-najlepszych-architektow', '/PageView?id=a0Gb000000499vZ' => '/kup-projekt-10-x-korzystniej', '/DesignChangeView' => '/zmiany-w-projekcie'};
    
        for (PageReference salesforceUrl : salesforceUrls) {
            String url = salesforceUrl.getUrl();
            Map<String,String> params = salesforceUrl.getParameters();
            params.remove('firstParam');
            if (staticRefs.containsKey(url)) {
                // static
                friendlyUrls.add(new PageReference(staticRefs.get(url)));
            } else if (url.startsWith('/DesignOrderView?')) {
                // /zamowienie?kod=
                friendlyUrls.add(new PageReference(url.replaceFirst('/DesignOrderView','/zamowienie')));
            } else if (url.startsWith('/FeedElementExpertGroupView?') && params.containsKey('id')) {
                // /porada-*
                String id = params.get('id');
                friendlyUrls.add(new PageReference('/porada-'+id));
            } else if (url.startsWith('/FeedElementView?') && params.containsKey('id')) {
                // /post-*
                String id = params.get('id');
                friendlyUrls.add(new PageReference('/post-'+id));
            } else if (url.startsWith('/FeedElementQuestionView?') && params.containsKey('id')) {
                // /pytanie-*
                String id = params.get('id');
                friendlyUrls.add(new PageReference('/pytanie-'+id));
            } else if (url.startsWith('/ConversationView?') && params.containsKey('id')) {
                // /rozmowa-*
                String id = params.get('id');
                friendlyUrls.add(new PageReference('/rozmowa-'+id));
            } else if (url.startsWith('/TopicView?') && params.containsKey('id')) {
                // /tag-
                String id = params.get('id');
                try {
                    if (topicsMap.containsKey(id)) {
                        Topic t = topicsMap.get(id);
                        String slug = t.Name;
                        slug = slug.toLowerCase('pl').replaceAll('ą','a').replaceAll('ć','c').replaceAll('ę','e').replaceAll('ł','l').replaceAll('ń','n').replaceAll('ó','o').replaceAll('ś','s').replaceAll('ż','z').replaceAll('ź','z').replaceAll(',','').replaceAll(' ','-');
                        friendlyUrls.add(new PageReference('/tag-'+slug));
                    } else {
                        friendlyUrls.add(salesforceUrl);
                    }
                } catch (Exception e) {
                    friendlyUrls.add(salesforceUrl);
                }
            } else if (url.startsWith('/TopicGalleryView?') && params.containsKey('id')) {
                // /zdjecia-taga-
                String id = params.get('id');
                try {
                    if (topicsMap.containsKey(id)) {
                        Topic t = topicsMap.get(id);
                        String slug = t.Name;
                        slug = slug.toLowerCase('pl').replaceAll('ą','a').replaceAll('ć','c').replaceAll('ę','e').replaceAll('ł','l').replaceAll('ń','n').replaceAll('ó','o').replaceAll('ś','s').replaceAll('ż','z').replaceAll('ź','z').replaceAll(',','').replaceAll(' ','-');
                        friendlyUrls.add(new PageReference('/zdjecia-taga-'+slug));
                    } else {
                        friendlyUrls.add(salesforceUrl);
                    }
                } catch (Exception e) {
                    friendlyUrls.add(salesforceUrl);
                }
            } else if (url.startsWith('/DesignView?') && params.containsKey('id')) {
                // /projekt-
                String id = params.get('id');
                try {
                    if (designsMap.containsKey(id)) {
                        Design__c d = designsMap.get(id);
                        if (String.isNotBlank(d.Canonical_URL__c)) {
                            friendlyUrls.add(new PageReference(d.Canonical_URL__c));
                        } else {
                            friendlyUrls.add(new PageReference('/projekt-'+d.Code__c));
                        }
                    } else {
                        friendlyUrls.add(salesforceUrl);
                    }
                } catch (Exception e) {
                    friendlyUrls.add(salesforceUrl);
                }
            } else if (url.startsWith('/DesignSocialView?') && params.containsKey('id')) {
                // /w-budowie-projekt-
                String id = params.get('id');
                try {
                    if (designsMap.containsKey(id)) {
                        Design__c d = designsMap.get(id);
                        if (String.isNotBlank(d.Canonical_URL__c)) {
                            friendlyUrls.add(new PageReference(d.Canonical_URL__c.replaceFirst('/projekt-','/w-budowie-projekt-')));
                        } else {
                            friendlyUrls.add(new PageReference('/w-budowie-projekt-'+d.Code__c));
                        }
                    } else {
                        friendlyUrls.add(salesforceUrl);
                    }
                } catch (Exception e) {
                    friendlyUrls.add(salesforceUrl);
                }
            } else if (url.startsWith('/DesignSocialGalleryView?') && params.containsKey('id')) {
                // /zdjecia-projektu-
                String id = params.get('id');
                try {
                    if (designsMap.containsKey(id)) {
                        Design__c d = designsMap.get(id);
                        if (String.isNotBlank(d.Canonical_URL__c)) {
                            friendlyUrls.add(new PageReference(d.Canonical_URL__c.replaceFirst('/projekt-','/zdjecia-projektu-')));
                        } else {
                            friendlyUrls.add(new PageReference('/zdjecia-projektu-'+d.Code__c));
                        }
                    } else {
                        friendlyUrls.add(salesforceUrl);
                    }
                } catch (Exception e) {
                    friendlyUrls.add(salesforceUrl);
                }
            } else if (url.startsWith('/ProfileView?') && params.containsKey('id')) {
                // /profil-
                String id = params.get('id');
                try {
                    if (usersMap.containsKey(id)) {
                        User u = usersMap.get(id);
                        if (String.isNotBlank(u.Nickname_Encoded__c)) {
                            friendlyUrls.add(new PageReference('/profil-'+u.Nickname_Encoded__c));
                        } else {
                            friendlyUrls.add(salesforceUrl);
                        }
                    } else {
                        friendlyUrls.add(salesforceUrl);
                    }
                } catch (Exception e) {
                    friendlyUrls.add(salesforceUrl);
                }
            } else if (url.startsWith('/ProfileGalleryView?') && params.containsKey('id')) {
                // /zdjecia-profilu-
                String id = params.get('id');
                try {
                    if (usersMap.containsKey(id)) {
                        User u = usersMap.get(id);
                        if (String.isNotBlank(u.Nickname_Encoded__c)) {
                            friendlyUrls.add(new PageReference('/zdjecia-profilu-'+u.Nickname_Encoded__c));
                        } else {
                            friendlyUrls.add(salesforceUrl);
                        }
                    } else {
                        friendlyUrls.add(salesforceUrl);
                    }
                } catch (Exception e) {
                    friendlyUrls.add(salesforceUrl);
                }
            } else if (url.startsWith('/PlotView?') && params.containsKey('id')) {
                // /zdjecia-profilu-
                String id = params.get('id');
                try {
                    if (plotsMap.containsKey(id)) {
                        Plot__c p = plotsMap.get(id);
                        if (String.isNotBlank(p.Canonical_Name__c)) {
                            friendlyUrls.add(new PageReference('/dzialka-'+p.Canonical_Name__c));
                        } else {
                            friendlyUrls.add(salesforceUrl);
                        }
                    } else {
                        friendlyUrls.add(salesforceUrl);
                    }
                } catch (Exception e) {
                    friendlyUrls.add(salesforceUrl);
                }
            } else if (url.startsWith('/ModelView?') && params.containsKey('id')) {
                // /wizualizacja-3d-projektu-
                String id = params.get('id');
                try {
                    if (modelsMap.containsKey(id)) {
                        Model__c m = modelsMap.get(id);
                        if (String.isNotBlank(m.Design__r.Canonical_URL__c)) {
                            friendlyUrls.add(new PageReference(m.Design__r.Canonical_URL__c.replaceFirst('/projekt-','/wizualizacja-3d-projektu-')+'-'+id));
                        } else {
                            friendlyUrls.add(new PageReference('/wizualizacja-3d-projektu-'+id));
                        }
                    } else {
                        friendlyUrls.add(salesforceUrl);
                    }
                } catch (Exception e) {
                    friendlyUrls.add(salesforceUrl);
                }
            } else if (url.startsWith('/DesignSearchView')) {
                // /projekty-
                try {
                    if (pagesMap.containsKey(url)) {
                        Page__c p = pagesMap.get(url);
                        if (String.isNotBlank(p.URL__c)) {
                            friendlyUrls.add(new PageReference(p.URL__c));
                        } else {
                            friendlyUrls.add(salesforceUrl);
                        }
                    } else {
                        friendlyUrls.add(salesforceUrl);
                    }
                } catch (Exception e) {
                    friendlyUrls.add(salesforceUrl);
                }
            } else if (url.startsWith('/DirectoryViewDesign?') && params.containsKey('col')) {
                // /katalog-projektow-*
                String col = params.get('col');
                friendlyUrls.add(new PageReference('/katalog-projektow-'+col));
            } else {
                friendlyUrls.add(salesforceUrl);
            }
        }
        
        return friendlyUrls;
        
    }

}