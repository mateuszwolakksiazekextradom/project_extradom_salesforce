@isTest
private class TsvProductsTest {
    @isTest
    static void generate() {
        Supplier__c s = new Supplier__c(Name='Test Supplier ABC', Full_Name__c='Test Supplier', Code__c='ABC');
        insert s;
        Design__c d = new Design__c(Name='D1', Full_Name__c='D1', Code__c='ABC1000', Supplier__c=s.Id, Status__c='Active', Sent_Month__c=0.5, Views_Month__c = 1000, Gross_Price__c = 2500);
        insert d;
        new TsvProducts();
        new TsvProducts().getResultFileSup();
    }
}