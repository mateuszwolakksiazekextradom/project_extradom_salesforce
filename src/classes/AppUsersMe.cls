@RestResource(urlMapping='/app/users/me')
global without sharing class AppUsersMe {
    @HttpGet
    global static ExtradomApi.UserProfile doGet() {
        return new ExtradomApi.UserProfile(ConnectApi.UserProfiles.getUserProfile(Network.getNetworkId(), 'me'));
    }
    @HttpPatch
    global static void doPatch(String name, String username, String aboutMe) {
        
        RestContext.response.addHeader('Cache-Control', 'no-cache, must-revalidate, max-age=0, no-store, private, s-maxage=0');
        RestContext.response.statusCode = 204;
        
        User u = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId() AND UserType = 'CspLitePortal'];
        
        if (name != null) {
            if (name=='') {
                ExtradomApi.FormValidationError validationError = new ExtradomApi.FormValidationError('Nie można zaktualizować profilu');
                validationError.addFieldError('name', 'Pseudonim nie może być pusty');
                RestContext.response.statusCode = 400;
                RestContext.response.responseBody = Blob.valueOf(JSON.serializePretty(validationError));
                return;
            }
            u.communityNickname = name;
            for (User user : [SELECT Id FROM User WHERE communityNickname = :name AND Id != :UserInfo.getUserId() LIMIT 1]) {
                ExtradomApi.FormValidationError validationError = new ExtradomApi.FormValidationError('Nie można zaktualizować profilu');
                validationError.addFieldError('name', 'Ten pseudonim jest już zajęty');
                RestContext.response.statusCode = 400;
                RestContext.response.responseBody = Blob.valueOf(JSON.serializePretty(validationError));
                return;
            }
        }
        if (username != null) {
            u.username = username;
            u.email = username;
            if (!Site.isValidUsername(username)) {
                ExtradomApi.FormValidationError validationError = new ExtradomApi.FormValidationError('Nie można zaktualizować profilu');
                validationError.addFieldError('username', 'Nieprawidłowy email');
                RestContext.response.statusCode = 400;
                RestContext.response.responseBody = Blob.valueOf(JSON.serializePretty(validationError));
                return;
            }
            for (User user : [SELECT Id FROM User WHERE Username = :username AND Id != :UserInfo.getUserId() LIMIT 1]) {
                ExtradomApi.FormValidationError validationError = new ExtradomApi.FormValidationError('Nie można zaktualizować profilu');
                validationError.addFieldError('username', 'Ten email jest użyty przez innego użytkownika');
                RestContext.response.statusCode = 400;
                RestContext.response.responseBody = Blob.valueOf(JSON.serializePretty(validationError));
                return;
            }
        }
        if (aboutMe != null) {
            u.aboutMe = aboutMe;
        }
        
        update u;
        
    }
}