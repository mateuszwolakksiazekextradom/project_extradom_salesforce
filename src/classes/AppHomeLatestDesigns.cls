@RestResource(urlMapping='/app/home/latest/designs')
global class AppHomeLatestDesigns {
    @HttpGet
    global static ExtradomApi.DesignResultPage doGet() {
        String designIds = RestContext.request.params.get('ids');
        ExtradomApi.DesignResultPage designResultPage;
        List<Id> ids = designIds.split(',');
        Map<Id,Design__c> designsMap = new Map<Id,Design__c>([SELECT Id, Code__c, Full_Name__c, Type__c, Gross_Price__c, Current_Price__c, Thumbnail_Base_URL__c, Usable_Area__c, Sketchfab_ID__c, KW_Model__c FROM Design__c WHERE Id IN :ids AND Status__c = 'Active']);
        List<Design__c> designs = new List<Design__c>();
        for (String id : ids) {
            if (designsMap.containsKey(id)) {
                designs.add(designsMap.get(id));
            }
        }
        designResultPage = new ExtradomApi.DesignResultPage(designs);
        designResultPage.currentPageUrl = '/app/home/latest/designs?ids='+designIds;
        if (UserInfo.getUserType()=='Guest') {
            RestContext.response.addHeader('Cache-Control', 'must-revalidate, max-age=0, s-maxage=3600');
        } else {
            RestContext.response.addHeader('Cache-Control', 'no-cache, must-revalidate, max-age=0, no-store, private, s-maxage=0');
        }
        return designResultPage;
    }
}