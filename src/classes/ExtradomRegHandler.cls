global class ExtradomRegHandler implements Auth.RegistrationHandler{
    class RegHandlerException extends Exception {}
    global User createUser(Id portalId, Auth.UserData data) {
        User u;
        if (data.email==null) {
            throw new RegHandlerException('Aby zalogować się do Społeczności Extradom, wymagane jest udostępnienie maila. Wróć do poprzedniej strony i zezwól aplikacji Extradom na dostęp do maila.');
        }
        Boolean blockUser = false;
        try {
            u = [SELECT Id, UserType FROM User WHERE Username = :data.email];
            if (u.UserType != 'CspLitePortal') {
                blockUser = true;
            }
        } catch (Exception e) {
            String nickname = data.firstName+String.valueOf(100+Math.round(Math.random()*10000));
            for (User user : [SELECT Id FROM User WHERE Name = :nickname LIMIT 1]) {
                throw new RegHandlerException('Wystąpił mały problem z dostępem do bazy. Cofnij się i zaloguj ponownie.');
            }
            u = new User();
            Profile p = [SELECT Id FROM Profile WHERE name='Extradom CC User'];
            u.username = data.email;
            u.email = data.email;
            u.lastName = nickname;
            u.communityNickname = nickname;
            String alias = nickname;
            if (alias.length() > 8) {
                alias = alias.substring(0, 8);
            }
            u.alias = alias;
            u.languagelocalekey = 'pl';
            u.localesidkey = 'pl_PL';
            u.emailEncodingKey = 'UTF-8';
            u.timeZoneSidKey = 'Europe/Berlin';
            u.profileId = p.Id;
            
            List<Contact> contacts = [SELECT Id FROM Contact WHERE EmailId__c = :data.email AND EmailId__c != '' LIMIT 1];
            if (contacts.size()>0) {
                Contact c = contacts[0];
                u.ContactId = c.Id;
            } else {
                Account a = new Account();
                String lastName = data.lastName;
                if (String.isBlank(data.firstName) || String.isBlank(data.lastName)) {
                    a.Name = data.email;
                    lastName = data.email;
                } else {
                    a.Name = data.firstName+' '+data.lastName;
                }
                a.AccountSource = 'Extradom.pl (AuthProvider)';
                insert a;
                Contact c = new Contact(FirstName=data.firstName, LastName=lastName, Email=data.email, AccountId=a.Id);
                insert c;
                u.ContactId = c.Id;
            }                                                                                                                                                                                                                                    
        }
        if (blockUser) {
            throw new RegHandlerException('Twój adres e-mail jest niedozwolony do logowania się z użyciem innych systemów. Skontaktuj się administratorem.');
        }
        return u;
    }
    global void updateUser(Id userId, Id portalId, Auth.UserData data) {
    }
}