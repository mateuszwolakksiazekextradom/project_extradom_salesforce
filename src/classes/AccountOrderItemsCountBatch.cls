public with sharing class AccountOrderItemsCountBatch implements Database.Batchable<sObject> {
    public Database.querylocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
                SELECT Id, AccountId FROM Order WHERE StatusCode = :ConstUtils.ORDER_ACTIVATED_STATUS_CODE
                ORDER BY AccountId
        ]);
    }

    public void execute(Database.BatchableContext BC, List<sObject> scope) {
        List<Order> orders = (List<Order> ) scope;
        Set<String> accountIds = new Set<String> ();
        for (Order o : orders) {
            accountIds.add(o.AccountId);
        }
        OrderUtils.calculateAccountOrderItems(accountIds);
    }

    public void finish(Database.BatchableContext BC) {

    }
}