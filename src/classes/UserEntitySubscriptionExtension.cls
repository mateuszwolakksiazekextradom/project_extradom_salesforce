public class UserEntitySubscriptionExtension {   

    private List<Id> subscriptionList = new List<Id>();
    private ApexPages.StandardController controller {get; set;}
    private User u {get; set;}
    
    public UserEntitySubscriptionExtension(ApexPages.StandardController controller) {
        this.controller = controller;
        this.u = (User)controller.getRecord();
        for (EntitySubscription es : [SELECT ParentId FROM EntitySubscription WHERE subscriberId = :this.u.Id]) {
            subscriptionList.add(es.ParentId);
        }
    }
      
    public List<Design__c> getDesigns() {
        return [SELECT Id, Name FROM Design__c WHERE Id IN :subscriptionList];
    } 
    
    public List<User> getUsers() {
        return [SELECT Id, Name FROM User WHERE Id IN :subscriptionList];
    }

    public List<Topic> getTopics() {
        return [SELECT Id, Name FROM Topic WHERE Id IN :subscriptionList];
    }
    
    public List<Plot__c> getPlots() {
        return [SELECT Id, Name FROM Plot__c WHERE Id IN :subscriptionList];
    }        
    
}