public with sharing class UserViewController {

    public String getFileBody() {
        return null;
    }


    public String getFilename() {
        return null;
    }


    public String getContentType() {
        return null;
    }
    
    


    private final String userId;
    public String message { get; set; }
    public String aboutMe { get; set; }
    public ConnectApi.ChatterMessage resultMessage { get; set; }
    
    public UserViewController() {
        this.userId = ApexPages.currentPage().getParameters().get('id');
        if (this.userId==UserInfo.getUserId()) {
            this.aboutMe = getMe().aboutMe;
        }
        if (!this.getUser().isActive) {
            throw new NoSuchElementException();
        }
    }
    
    public ConnectApi.UserDetail getMe() {
        return ConnectApi.ChatterUsers.getUser(Network.getNetworkId(), 'me');
    }
    
    public PageReference updateMe() {
        ConnectApi.UserInput ui = new ConnectApi.UserInput();
        ui.aboutMe = this.aboutMe;
        ConnectApi.ChatterUsers.updateUser(Network.getNetworkId(), 'me', ui);
        return null;
    }
    
    public ConnectApi.FollowerPage getFollowers() {
        return ConnectApi.ChatterUsers.getFollowers(Network.getNetworkId(), userId);
    }
    
    public PageReference follow() {
        ConnectApi.ChatterUsers.follow(Network.getNetworkId(), 'me', userId);
        return null;
    }
    
    public PageReference unfollow() {
        ConnectApi.UserDetail userToUnfollow = ConnectApi.ChatterUsers.getUser(Network.getNetworkId(), userId);
        if (userToUnfollow.mySubscription != null) {
            ConnectApi.Chatter.deleteSubscription(Network.getNetworkId(), userToUnfollow.mySubscription.id);
        }
        return null;
    }
    
    public ConnectApi.FeedElementPage getFeedElementsFromFeed() {
        return ConnectApi.ChatterFeeds.getFeedElementsFromFeed(Network.getNetworkId(), ConnectApi.FeedType.UserProfile, userId);
    }
    
    public ConnectApi.UserDetail getUser() {
        return ConnectApi.ChatterUsers.getUser(Network.getNetworkId(), userId);
    }
    
    public User getSFUser() {
        return [SELECT Id, Partner_Type__c, Location__latitude__s, Location__longitude__s, VAT_No__c, REGON__c, KRS__c, IBAN__c, fixedSearchParams__c, Feed_News__c, Feed_Offer__c, Feed_About__c, Tab_Loans__c, Hide_Author__c FROM User WHERE Id = :userId];
    }
    
    public ConnectApi.FollowingPage getFollowings() {
        return ConnectApi.ChatterUsers.getFollowings(Network.getNetworkId(), userId);
    }
    
    public ConnectApi.FollowingPage getFollowingsUsers() {
        return ConnectApi.ChatterUsers.getFollowings(Network.getNetworkId(), userId, '005', null, 96);
    }
    
    public ConnectApi.FollowingPage getFollowingsTopics() {
        return ConnectApi.ChatterUsers.getFollowings(Network.getNetworkId(), userId, '0TO', null, 96);
    }
    
    public ConnectApi.FollowingPage getFollowingsFiles() {
        return ConnectApi.ChatterUsers.getFollowings(Network.getNetworkId(), userId, '069', null, 96);
    }
    
    public ConnectApi.FollowingPage getFollowingsDesigns() {
        return ConnectApi.ChatterUsers.getFollowings(Network.getNetworkId(), userId, 'a06', null, 96);
    }
    
    public ConnectApi.TopicPage getRecentlyTalkingAboutTopics() {
        return ConnectApi.Topics.getRecentlyTalkingAboutTopicsForUser(Network.getNetworkId(), userId);
    }
    
    public ConnectApi.FeedElementPage getFeedElementsFromFeedFiles() {
        return ConnectApi.ChatterFeeds.getFeedElementsFromFeed(Network.getNetworkId(), ConnectApi.FeedType.Files, userId);
    }
    
    public List<ConnectApi.FeedElement> getTopContentPosts() {
        List<Id> ids = ChatterMethods.topUserProfileContentPostIds(this.userId);
        List<ConnectApi.FeedElement> feedElements = new List<ConnectApi.FeedElement>{};
        ConnectApi.BatchResult[] batchResults = ConnectApi.ChatterFeeds.getFeedElementBatch(Network.getNetworkId(), ids);
        for (ConnectApi.BatchResult batchResult : batchResults) {
            if (batchResult.isSuccess()) {
                ConnectApi.FeedElement element;
                if (batchResult.getResult() instanceof ConnectApi.FeedElement) {
                    element = (ConnectApi.FeedElement) batchResult.getResult();
                    feedElements.add(element);
                }
            }
        }
        return feedElements;
    }
    
    public List<ConnectApi.FeedElement> getContentPosts() {
        List<Id> ids = ChatterMethods.userProfileContentPostIds(this.userId);
        List<ConnectApi.FeedElement> feedElements = new List<ConnectApi.FeedElement>{};
        ConnectApi.BatchResult[] batchResults = ConnectApi.ChatterFeeds.getFeedElementBatch(Network.getNetworkId(), ids);
        for (ConnectApi.BatchResult batchResult : batchResults) {
            if (batchResult.isSuccess()) {
                ConnectApi.FeedElement element;
                if (batchResult.getResult() instanceof ConnectApi.FeedElement) {
                    element = (ConnectApi.FeedElement) batchResult.getResult();
                    feedElements.add(element);
                }
            }
        }
        return feedElements;
    }
    
    public List<ConnectApi.FeedElement> getSFUserNews() {
        return ChatterMethods.getFeedElementsFromStringList(this.getSFUser().Feed_News__c);
    }
    
    public List<ConnectApi.FeedElement> getSFUserOffer() {
        return ChatterMethods.getFeedElementsFromStringList(this.getSFUser().Feed_Offer__c);
    }
    
    public List<ConnectApi.FeedElement> getSFUserAbout() {
        return ChatterMethods.getFeedElementsFromStringList(this.getSFUser().Feed_About__c);
    }
    
    public Integer getCountContentPosts() {
        return ChatterMethods.countUserProfileContentPosts(this.userId);
    }
    
    public List<Construction__c> getConstructions() {
        return [SELECT Id, Stage__c, CreatedDate, Design__r.Id, Design__r.Full_Name__c, Design__r.Code__c, Design__r.Thumbnail_Base_URL__c, Design__r.Status__c, Plot__r.Id, Plot__r.Name, Plot__r.Done__c, Shared_Location__r.Parent_Location__r.Name, Shared_Location__r.Name, Plot__r.Area__c FROM Construction__c WHERE OwnerId = :this.userId];
    }
    
    public Map<String,String> getHeaders() {
        return ApexPages.currentPage().getHeaders();
    }

}