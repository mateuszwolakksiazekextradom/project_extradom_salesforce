@IsTest
private class ProductSelectionNavController_Test {

    @IsTest
    private static void shouldReturnNavigationTabs() {
        // given
        List<Product_Selection_Navigation__mdt> navTabs = [
                SELECT MasterLabel, Product_Family__c, Order__c FROM Product_Selection_Navigation__mdt
        ];

        // when
        List<Product_Selection_Navigation__mdt> navTabsRetrieved = ProductSelectionNavController.getNavigationTabs();

        // then
        System.assertEquals(navTabs.size(), navTabsRetrieved.size());
    }
}