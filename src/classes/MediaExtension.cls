public class MediaExtension {
    
    public static boolean pdfs = false;

    private final Media__c m;
    public MediaExtension(ApexPages.StandardController stdController) {
       
        this.m = (Media__c)stdController.getRecord();
    }
    
    @future
    public static void fillDocuments(Set<Id> ids) {
        
        String document, estimates, secondary, materials = '';
        List<Design__c> ds = [SELECT Id, Name, Documents__c, Documents_Secondary__c, Documents_Estimates__c, Documents_Materials__c, (SELECT Id, Label__c, Subcategory__c, Document_URL__c, Design__c FROM Media__r WHERE Category__c='Document PDF') FROM Design__c WHERE Id IN :ids];
        MediaExtension.pdfs = true;

        for(Design__c d : ds) {
            document = estimates = secondary = materials = '';
            for(Media__c m : d.Media__r) {
                if (m.Subcategory__c == 'Plan' || m.Subcategory__c == 'Section') {
                    document += '<a href=\"' + m.Document_URL__c + '\">' + d.Name + ' - ' + m.Label__c + '</a><br/>';
                } else if (m.Subcategory__c == 'Estimates') {
                    estimates += '<a href=\"' + m.Document_URL__c + '\">' + d.Name + ' - ' + m.Label__c + '</a><br/>'; 
                } else if (m.Subcategory__c == 'Materials') {
                    materials += '<a href=\"' + m.Document_URL__c + '\">' + d.Name + ' - ' + m.Label__c + '</a><br/>';
                } else {
                    secondary += '<a href=\"' + m.Document_URL__c + '\">' + d.Name + ' - ' + m.Label__c + '</a><br/>';
                }
            }
            d.Documents__c = document;
            d.Documents_Estimates__c = estimates;
            d.Documents_Materials__c = materials;
            d.Documents_Secondary__c = secondary;
        }
        update ds;
    }
}