@isTest
private class CommunityMemberTest {

    static testMethod void myUnitTest() {
        
        Community_Member__c cm = new Community_Member__c(Name='test', Membercode__c='aBc~~0123xyZ', ID__c=1, Business_Type__c='Service');
        insert cm;
        
        Community_Member__c cm2 = new Community_Member__c(Name='test', Membercode__c='aBc~~0123xyW', ID__c=2, Business_Type__c='Brand');
        insert cm2;
        
        List<RecordType> locationRecordTypes = [Select Id, Name From RecordType where sObjectType='Location__c' and isActive=true];
        Map<String, ID> locationRecordTypeNames = new Map<String, ID>{};
		for(RecordType rt: locationRecordTypes) {
			locationRecordTypeNames.put(rt.Name,rt.Id);
		}
		
		List<RecordType> cmdRecordTypes = [Select Id, Name From RecordType where sObjectType='Community_Member_Detail__c' and isActive=true];
        Map<String, ID> cmdRecordTypeNames = new Map<String, ID>{};
		for(RecordType rt: cmdRecordTypes) {
			cmdRecordTypeNames.put(rt.Name,rt.Id);
		}
		
		List<RecordType> ctagRecordTypes = [Select Id, Name From RecordType where sObjectType='Community_Tag__c' and isActive=true];
        Map<String, ID> ctagRecordTypeNames = new Map<String, ID>{};
		for(RecordType rt: ctagRecordTypes) {
			ctagRecordTypeNames.put(rt.Name,rt.Id);
		}
		
		Community_Tag__c product = new Community_Tag__c(Name='dachówki', RecordTypeId=ctagRecordTypeNames.get('Product'));
		insert product;
		Community_Tag__c service = new Community_Tag__c(Name='dekarz', RecordTypeId=ctagRecordTypeNames.get('Service'));
		insert service;
		
		Community_Member_Detail__c cmd10 = new Community_Member_Detail__c(Community_Member__c=cm2.Id, Community_Tag__c=product.Id, RecordTypeId=cmdRecordTypeNames.get('Community Tag'));
		insert cmd10;
		
		Community_Member_Detail__c cmd11 = new Community_Member_Detail__c(Community_Member__c=cm.Id, Community_Tag__c=service.Id, RecordTypeId=cmdRecordTypeNames.get('Community Tag'));
		insert cmd11;
		
		
		Community_Member_Detail__c cmd20 = new Community_Member_Detail__c(Community_Member__c=cm.Id, RecordTypeId=cmdRecordTypeNames.get('Business Address'));
		insert cmd20;
		
		Community_Member_Detail__c cmd21 = new Community_Member_Detail__c(Community_Member__c=cm.Id, Related_Address__c=cmd20.Id, RecordTypeId=cmdRecordTypeNames.get('Business Address Detail'));
		insert cmd21;
		
		Community_Member_Detail__c cmd22 = new Community_Member_Detail__c(Community_Member__c=cm.Id, RecordTypeId=cmdRecordTypeNames.get('Business Address Detail'));
		insert cmd22;
		
		update cm;
		
		
		Location__c country = new Location__c(Name='Cała Polska', RecordTypeId=locationRecordTypeNames.get('Country'));
		insert country;
		Location__c state = new Location__c(Name='dolnośląskie', Parent_Location__c=country.Id, RecordTypeId=locationRecordTypeNames.get('State'));
		insert state;
		Location__c region = new Location__c(Name='m. Wrocław', Parent_Location__c=state.Id, RecordTypeId=locationRecordTypeNames.get('Region'));
		insert region;
		
		Community_Member_Detail__c cmd1 = new Community_Member_Detail__c(Community_Member__c=cm.Id, Location__c=region.Id, RecordTypeId=cmdRecordTypeNames.get('Business Activity Location'));
		insert cmd1;
		
		Community_Member_Detail__c cmd2 = new Community_Member_Detail__c(Community_Member__c=cm.Id, Location__c=state.Id, RecordTypeId=cmdRecordTypeNames.get('Business Activity Location'));
		insert cmd2;
		
		Community_Member_Detail__c cmd3 = new Community_Member_Detail__c(Community_Member__c=cm.Id, Location__c=country.Id, RecordTypeId=cmdRecordTypeNames.get('Business Activity Location'));
		insert cmd3;
		
		Community_Member__c cm11 = [SELECT Id FROM Community_Member__c WHERE ID__c=1];
    	cm11.Type__c = 'Brand';
    	update cm11;
    	
    	Community_Member_Detail__c cmd4 = new Community_Member_Detail__c(Community_Member__c=cm.Id, Role__c='Organization', RecordTypeId=cmdRecordTypeNames.get('Role'));
		insert cmd4;
		
		Community_Member_Detail__c cmd5 = new Community_Member_Detail__c(Community_Member__c=cm2.Id, Role__c='Organization', RecordTypeId=cmdRecordTypeNames.get('Role'));
		insert cmd5;
        
    }
    
}