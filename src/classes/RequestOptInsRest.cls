@RestResource(urlMapping='/optins')
global class RequestOptInsRest {
    @HttpPost
    global static Map<String,String> doPost(Id contactId, Boolean optInMarketing) {
        Restcontext.response.headers.put('Access-Control-Allow-Origin', '*');
        try {
            Contact c = new Contact(Id=contactId);
            if (optInMarketing) {
                c.Opt_In_Marketing__c = true;
            }
            update c;
        } catch (Exception e) {
            RestResponse res = RestContext.response;
            res.statusCode = 400;
            Map<String,String> result = new Map<String,String>{'result' => 'Nieprawidłowe zapytanie'};
            return result;
        }
        Map<String,String> result = new Map<String,String>{'result' => 'Dziękujemy za zainteresowanie.'};
        return result;
    }
}