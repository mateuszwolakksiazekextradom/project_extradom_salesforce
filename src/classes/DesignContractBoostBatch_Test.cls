/**
 * Created by grzegorz.dlugosz on 17.05.2019.
 */
@isTest
private class DesignContractBoostBatch_Test {
    @IsTest
    private static void runMultipleSupplierDesigns() {
        // Positive Boost should be calculated
        // Prepare Test Data
        Supplier__c supplier = TestUtils.newSupplier(new Supplier__c(), true);

        Design__c design1 = TestUtils.newDesign(new Design__c(Supplier__c = supplier.Id, Featured__c = 1, Gross_Price__c = 1000, Views_Month__c = 100,
                                                                Name = 'tst1', Full_Name__c = 'tst1', Code__c = 'tst1', Pending_Promo_Price__c = 1000,
                                                                Promo_Price__c = 1000, VAT_Rate__c = 0.23), false);
        Design__c design2 = TestUtils.newDesign(new Design__c(Supplier__c = supplier.Id, Featured__c = 2, Gross_Price__c = 2000, Views_Month__c = 200,
                                                                Name = 'tst2', Full_Name__c = 'tst2', Code__c = 'tst2', Pending_Promo_Price__c = 2000,
                                                                Promo_Price__c = 2000, VAT_Rate__c = 0.23), false);
        Design__c design3 = TestUtils.newDesign(new Design__c(Supplier__c = supplier.Id, Featured__c = 3, Gross_Price__c = 3000, Views_Month__c = 300,
                                                                Name = 'tst3', Full_Name__c = 'tst3', Code__c = 'tst3', Pending_Promo_Price__c = 3000,
                                                                Promo_Price__c = 3000, VAT_Rate__c = 0.23), false);
        Design__c design4 = TestUtils.newDesign(new Design__c(Supplier__c = supplier.Id, Featured__c = 4, Gross_Price__c = 4000, Views_Month__c = 400,
                                                                Name = 'tst4', Full_Name__c = 'tst4', Code__c = 'tst4', Pending_Promo_Price__c = 4000,
                                                                Promo_Price__c = 4000, VAT_Rate__c = 0.23), false);
        Design__c design5 = TestUtils.newDesign(new Design__c(Supplier__c = supplier.Id, Featured__c = 4, Gross_Price__c = 5000, Views_Month__c = 500,
                                                                Name = 'tst5', Full_Name__c = 'tst5', Code__c = 'tst5', Pending_Promo_Price__c = 5000,
                                                                Promo_Price__c = 5000, VAT_Rate__c = 0.23), false);
        Design__c design6 = TestUtils.newDesign(new Design__c(Supplier__c = supplier.Id, Featured__c = 5, Gross_Price__c = 6000, Views_Month__c = 600,
                                                                Name = 'tst6', Full_Name__c = 'tst6', Code__c = 'tst6', Pending_Promo_Price__c = 6000,
                                                                Promo_Price__c = 6000, VAT_Rate__c = 0.23), false);
        List<Design__c> designToInsertList = new List<Design__c>{design1, design2, design3, design4, design5, design6};
        insert designToInsertList;

        // Run the batch
        Test.startTest();
        DesignContractBoostBatch btch = new DesignContractBoostBatch();
        Database.executeBatch(btch);
        Test.stopTest();

        /* Verify results */
        /*List<Design__c> designsAfterBatch = [
                SELECT Id, Featured__c, Page_Value__c, Contract_Boost__c
                FROM Design__c
                WHERE Featured__c <= 4 AND Id IN :designToInsertList
                ORDER BY Featured__c DESC, Page_Value__c DESC];*/

        // Boost calculated manually - compare in Asserts
        /*List<Decimal> expectedBoostList = new List<Decimal>{1.69797, 1.13198, 0.75465, 0.50310, 0.33540};
        for (Integer i = 0; i < designsAfterBatch.size(); i++) {
            System.assertEquals(expectedBoostList.get(i), designsAfterBatch.get(i).Contract_Boost__c);
        }*/
    }

    @IsTest
    private static void runSingleSupplierDesignMatch() {
        // This will calculate negative Contract Boost, so it should be set to 0
        // Prepare Test Data
        Supplier__c supplier = TestUtils.newSupplier(new Supplier__c(), true);

        Design__c design = TestUtils.newDesign(new Design__c(Supplier__c = supplier.Id, Featured__c = 1, Gross_Price__c = 1000, Views_Month__c = 100,
                Name = 'tst1', Full_Name__c = 'tst1', Code__c = 'tst1', Pending_Promo_Price__c = 1000,
                Promo_Price__c = 1000, VAT_Rate__c = 0.23), true);

        // Run the batch
        Test.startTest();
        DesignContractBoostBatch btch = new DesignContractBoostBatch();
        Database.executeBatch(btch);
        Test.stopTest();

        /* Verify Test Results */
        /*List<Design__c> designsAfterBatch = [
                SELECT Id, Featured__c, Page_Value__c, Contract_Boost__c
                FROM Design__c
                WHERE Featured__c <= 4
                ORDER BY Featured__c DESC, Page_Value__c DESC];*/

        // This one should have negative Contract Boost, so we set it to 0.
        //System.assertEquals(0, designsAfterBatch.get(0).Contract_Boost__c);
    }

    @IsTest
    private static void runSupplierNoDesigns() {
        // This is just to see if no error will be thrown in case of no designs under supplier
        // Prepare Test Data
        Supplier__c supplier = TestUtils.newSupplier(new Supplier__c(), true);

        // Run the batch
        Test.startTest();
        DesignContractBoostBatch btch = new DesignContractBoostBatch();
        Database.executeBatch(btch);
        Test.stopTest();

        /* Verify Test Results */
        /*List<Design__c> designsAfterBatch = [
                SELECT Id, Featured__c, Page_Value__c, Contract_Boost__c
                FROM Design__c
                WHERE Featured__c <= 4
                ORDER BY Featured__c DESC, Page_Value__c DESC];*/

        //System.assertEquals(0, designsAfterBatch.size());
    }
}