public with sharing class DesignFeaturedGarageBatch implements Database.Batchable<sObject> {
    public Database.querylocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
                SELECT Id, Featured_Garage__c, Pending_Featured_Garage__c
                FROM Design__c
                WHERE Featured_Garage__c != null OR Pending_Featured_Garage__c != null
        ]);
    }

    public void execute(Database.BatchableContext BC, List<sObject> scope) {
        List<Design__c> designs = (List<Design__c> ) scope;
        for (Design__c design : designs) {
            if (design.Pending_Featured_Garage__c != null) {
                design.Featured_Garage__c = design.Pending_Featured_Garage__c;
            } else {
                design.Featured_Garage__c = null;
            }
        }
        DesignTriggerHandler.BYPASS_INVALIDATE_DESIGNS = true;
        update designs;
        DesignTriggerHandler.BYPASS_INVALIDATE_DESIGNS = false;
    }

    public void finish(Database.BatchableContext BC) {
        if (!Test.isRunningTest()) {
            Database.executeBatch(new DesignFeaturedPromoBatch());
        }
    }
}