public with sharing class TopicViewController {

    private final String topicId;
    
    public TopicViewController() {
        this.topicId = ApexPages.currentPage().getParameters().get('id');
    }
    
    public ConnectApi.FollowerPage getFollowers() {
        return ConnectApi.Chatter.getFollowers(Network.getNetworkId(), topicId);
    }
    
    public ConnectApi.Reference getMySubscriptionReference() {
        ConnectApi.FollowerPage followerPage = ConnectApi.Chatter.getFollowers(Network.getNetworkId(), topicId);
        for (ConnectApi.Subscription subscription : followerPage.followers) {
            if (subscription.subject.mySubscription != null) {
                return subscription.subject.mySubscription;
            }
        }
        return null;
    }
    
    public PageReference follow() {
        ConnectApi.ChatterUsers.follow(Network.getNetworkId(), 'me', topicId);
        return null;
    }
    
    public PageReference unfollow() {
        ConnectApi.FollowerPage followerPage = ConnectApi.Chatter.getFollowers(Network.getNetworkId(), topicId);
        for (ConnectApi.Subscription subscription : followerPage.followers) {
            if (subscription.subject.mySubscription != null) {
                ConnectApi.Chatter.deleteSubscription(Network.getNetworkId(), subscription.subject.mySubscription.id);
                break;
            }
        }
        return null;
    }
    
    public ConnectApi.FeedElementPage getFeedElementsFromFeed() {
        return ConnectApi.ChatterFeeds.getFeedElementsFromFeed(Network.getNetworkId(), ConnectApi.FeedType.Topics, topicId);
    }
    
    public ConnectApi.Topic getTopic() {
        return ConnectApi.Topics.getTopic(Network.getNetworkId(), topicId);
    }
    
    public ConnectApi.TopicPage getRelatedTopics() {
        return ConnectApi.Topics.getRelatedTopics(Network.getNetworkId(), topicId);
    }
    
    public List<ConnectApi.FeedElement> getTopContentPosts() {
        List<ConnectApi.FeedElement> feedElements = new List<ConnectApi.FeedElement>{};
        ConnectApi.FeedElementPage feedElementPage = this.getFeedElementsFromFeed();
        List<ConnectApi.FeedElement> sortedFeedElements = new List<ConnectApi.FeedElement>{};
        List<ConnectApi.FeedElement> copyOfSortedFeedElements = new List<ConnectApi.FeedElement>{};
        Integer max = 0;
        if (feedElementPage.elements!=null) {
            for (ConnectApi.FeedElement fe : feedElementPage.elements) {
                if (fe.capabilities.content!=null && !(fe.parent instanceof ConnectApi.ChatterGroupSummary)) {
                    if (fe.capabilities.chatterLikes.page.total<=max) {
                        sortedFeedElements.add(fe);
                    } else {
                        copyOfSortedFeedElements.clear();
                        copyOfSortedFeedElements.addAll(sortedFeedElements);
                        sortedFeedElements.clear();
                        sortedFeedElements.add(fe);
                        sortedFeedElements.addAll(copyOfSortedFeedElements);
                        max = fe.capabilities.chatterLikes.page.total;
                    }
                }
            }
            if (sortedFeedElements.size()>6) {
                return new List<ConnectApi.FeedElement>{sortedFeedElements[0], sortedFeedElements[1], sortedFeedElements[2], sortedFeedElements[3], sortedFeedElements[4], sortedFeedElements[5]};
            }
        }
        return sortedFeedElements;
    }

}