global class PDFDocumentReport {
   
    public List<MediaReport> getMediaOlderThanPrimaryChange(){
        List<Media__c> ms = [SELECT Id, Design__r.Id, Design__r.Name, Design__r.Sent_Month__c, LastModifiedDate, Document_URL__c, Label__c FROM Media__c WHERE Category__c = 'Document PDF' AND Label__c LIKE 'Rzut parteru%' AND Design__r.Status__c = 'Active' AND Design__r.Stage__c = ''];
        List<Id> ids = new List<Id>();
        
        for (Media__c m : ms) {
            ids.add(m.Design__r.Id);    
        }        
        
        Map<Id,AggregateResult> dhs = new Map<id,AggregateResult>([SELECT Design__r.Id Id, MAX(LastModifiedDate) Date FROM Media__c WHERE (Category__c LIKE '%Plan' OR Category__c LIKE 'Rzut%') AND Design__r.Id IN :ids GROUP BY Design__r.Id]);
        List<MediaReport> mrs = new List<MediaReport>();
        
        for (Media__c m : ms) {
            if(dhs.containsKey(m.Design__r.Id) && (DateTime)dhs.get(m.Design__r.Id).get('Date') > m.LastModifiedDate) {
                MediaReport mr = new MediaReport();
                mr.mediaId=m.Id;
                mr.designId=m.Design__r.Id;
                mr.designName=m.Design__r.Name;
                mr.mediaURL=m.Document_URL__c;
                mr.mediaLabel=m.Label__c;
                mr.designSentMonth=m.Design__r.Sent_Month__c;
                mr.mediaUpdateDate=m.LastModifiedDate; 
                mr.lastUpdate=(DateTime)dhs.get(m.Design__r.Id).get('Date');
                mrs.add(mr);
            }
        }       
        return mrs;       
    }
    
    public class MediaReport {
        public String mediaId {get;set;}
        public String designId {get;set;}
        public String designName {get;set;}
        public String mediaURL {get;set;}
        public String mediaLabel {get;set;}
        public Double designSentMonth {get;set;}
        public Datetime mediaUpdateDate {get;set;}
        public Datetime lastUpdate {get;set;}       
    }
}