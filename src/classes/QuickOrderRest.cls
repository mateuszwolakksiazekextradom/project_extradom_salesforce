@RestResource(urlMapping='/quickorder')
global class QuickOrderRest {
    @HttpPost
    global static Map<String,String> doPost(String email, String phone, String firstName, String lastName, String streetName, String streetNumber, String streetFlatNumber, String zip, String city, String productId, Boolean optInRegulations, Boolean optInPrivacyPolicy, Boolean optInMarketing, Boolean optInMarketingExtended, Boolean optInMarketingPartner, Boolean optInLeadForward, String gclid, String hasPlot, String hasMpzp, String constructionPlan, String hasApp, Boolean optInEmail, Boolean optInPhone, Boolean optInProfiling, Boolean optInPKO) {
        Map<String,Id> data = OrderMethods.getWebLeadContactDetails('Catalogue', '', email, email, phone, 'Internal', false, false, 'No',hasPlot, hasMpzp, constructionPlan, hasApp);
        if (String.isNotBlank(gclid)) {
            OrderMethods.updateGclid(data.get('accountId'), gclid);
        }
        RestRequest req = RestContext.request;
        if (req.headers.containsKey('gaClientId')) {
            OrderMethods.updateGaClientId(data.get('accountId'), req.headers.get('gaClientId'));
        }
        OrderMethods.updateOptIns(email, phone, optInRegulations, optInPrivacyPolicy, optInMarketing, optInMarketingExtended, optInMarketingPartner, optInLeadForward, optInEmail, optInPhone, optInProfiling, optInPKO);
        Order o = new Order(AccountId=data.get('accountId'), OwnerId=data.get('ownerId'), EffectiveDate=System.today(), Default_Product__c=productId, Status='Draft', Shipping_Method__c = 'Regular', Shipping_Cost__c = 0.0);
        o.ShippingFirstName__c = firstName;
        o.ShippingLastName__c = lastName;
        String street = streetName;
        street += String.isNotBlank(streetNumber) ? ' ' + streetNumber : '';
        street += String.isNotBlank(streetNumber) && String.isNotBlank(streetFlatNumber) ? '/' : '';
        street += String.isNotBlank(streetFlatNumber) ? streetFlatNumber : '';
        o.ShippingStreet = street;
        o.ShippingPostalCode = zip;
        o.ShippingCity = city;
        o.ShippingPhone__c = phone;
        insert o;
        Map<String,String> result = new Map<String,String>{'result' => 'Dziękujemy za zainteresowanie. Katalog zostanie przesłane na podany adres najszybciej jak to możliwe.'};
        return result;
    }
}