@isTest
private class PDFDocumentReportTest {
    
    static testMethod void myTest() {
        PDFDocumentReport pdfdr = new PDFDocumentReport();
        
        Supplier__c s = new Supplier__c(Code__c='TST',Name='TST',Full_Name__c='TST');
        insert s;
        Design__c d = new Design__c(Supplier__c=s.Id,Code__c='TST1234',status__c='Active',Full_Name__c='TST',Name='TST');
        insert d;
        Design__c d2 = [SELECT Id, Primary_Data_Changes__c FROM Design__c WHERE Id = :d.id];
        d2.Primary_Data_Changes__c = 'pow. użytkowa';
        update d2;
        Media__c m = new Media__c(Design__c=d.Id,Category__c='Document PDF',Label__c='Rzut Parteru',Media_Name_Id__c='TST');
        insert m;
        
        List<PDFDocumentReport.MediaReport> mrs = pdfdr.getMediaOlderThanPrimaryChange();
    }
}