@isTest
private class ImportControllerTest {
    
    static testMethod void designImporterFromJsonTest() {
        
        test.startTest();

        Supplier__c s = new Supplier__c(Name='Test Supplier ABC', Full_Name__c='Test Supplier', Code__c='ABC');
        insert s;
        
        ImportController ic = new ImportController();
        ic.jsondata = '[ { "Supplier__c": "'+s.Id+'", "External_Web_ID__c": "abc: ex1", "Full_Name_inc__c": "Example 1", "Usable_Area_inc__c": 120.23, "Description_inc__c": " Lorem ipsum" } ]';
        ic.processJson();
        ic.getSelected();
        ic.save();
        
        ic.jsondata = '[ { "Supplier__c": "'+s.Id+'", "External_Web_ID__c": "abc: ex1", "Full_Name_inc__c": "Example 1", "Usable_Area_inc__c": 120.2, "Description_inc__c": "" }, { "Supplier__c": "'+s.Id+'", "External_Web_ID__c": "abc: ex2", "Full_Name_inc__c": "Example 2", "Usable_Area_inc__c": 85.44, "Description_inc__c": "Lorem ipsum" } ]';
        ic.processJson();
        ic.save();
        
        ic.jsondata = '[ { "Supplier__c": "'+s.Id+'", "External_Web_ID__c": "abc: ex1", "Full_Name_inc__c": "Example 1", "Usable_Area_inc__c": null, "Description_inc__c": " Lorem ipsum" } ]';
        ic.processJson();
        ic.getChangesList();
        ic.save();
        
        test.stopTest();
        
    }
}