@RestResource(urlMapping='/app/filter/items')
global class AppFilterItems {
    @HttpGet
    global static ExtradomApi.ItemResultPage doGet() {
        
        if (UserInfo.getUserType()=='Guest') {
            RestContext.response.addHeader('Cache-Control', 'must-revalidate, max-age=0, s-maxage=3600');
        } else {
            RestContext.response.addHeader('Cache-Control', 'no-cache, must-revalidate, max-age=0, no-store, private, s-maxage=0');
        }
        
        Integer p = Integer.valueOf(RestContext.request.params.get('p'));
        String type = RestContext.request.params.get('type');
        String department = RestContext.request.params.containsKey('department')?RestContext.request.params.get('department'):null;
        String category = RestContext.request.params.containsKey('category')?RestContext.request.params.get('category'):null;
        String location = RestContext.request.params.containsKey('location')?String.escapeSingleQuotes(RestContext.request.params.get('location')):null;
        Map<String,List<String>> filtersMap = new Map<String,List<String>>{};
        if (RestContext.request.params.containsKey('styles')) {
            filtersMap.put('styles',String.escapeSingleQuotes(RestContext.request.params.get('styles')).split(','));
        }
        if (RestContext.request.params.containsKey('colors')) {
            filtersMap.put('colors',String.escapeSingleQuotes(RestContext.request.params.get('colors')).split(','));
        }
        if (RestContext.request.params.containsKey('materials')) {
            filtersMap.put('materials',String.escapeSingleQuotes(RestContext.request.params.get('materials')).split(','));
        }
        
        return ExtradomApi.getItemResults(p, type, department, category, location, filtersMap);
        
    }
}