/**
* Created by grzegorz.dlugosz on 20.05.2019.
* This Batch is used to clear (set to null) Contract Boost on all designs.
* After the Boost on all designs is cleared, the next batch that calculates
* new value is fired.
* */

global class DesignContractBoostClearBatch implements Database.Batchable<sObject> {
    String query;

    global DesignContractBoostClearBatch() {
        query = 'SELECT Contract_Boost__c FROM Design__c';
    }

    global Database.QueryLocator start(Database.BatchableContext param1) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext param1, List<Design__c> designList) {
        for (Design__c design : designList) {
            design.Contract_Boost__c = null;
        }

        DesignTriggerHandler.BYPASS_INVALIDATE_DESIGNS = true;
        update designList;
        DesignTriggerHandler.BYPASS_INVALIDATE_DESIGNS = false;
    }

    global void finish(Database.BatchableContext bc) {
        // Fire Batch with scope = 1, so it considers only single Supplier per execution.
        // DO NOT change the scope to higher value
        if (!Test.isRunningTest()) {
            // this is tested in separate Test class.
            DesignContractBoostBatch btch = new DesignContractBoostBatch();
            Database.executeBatch(btch, 1);
        }
    }
}