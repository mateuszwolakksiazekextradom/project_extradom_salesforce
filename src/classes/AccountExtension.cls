public class AccountExtension {

    Account acc;
    public AccountExtension(ApexPages.StandardController stdController) {
        this.acc = (Account)stdController.getRecord();
    }
           
    public List<Telemarketing__c> getTelemarketingLeads() {
        return [SELECT Id, First_Name__c, Last_Name__c, Full_Location__c, Account_Latest_Design__c FROM Telemarketing__c WHERE Transfer_Account__c = :acc.Id AND Name = 'Przekazanie danych: adaptacje projektów' AND Status__c = 'Won' AND Post_Conversion_Stage__c = 'In Progress' ORDER BY CreatedDate];
    }
    
    public static Set<id> getArchitectIdsForEmail() {
        Set<Id> ids = new Set<Id>();        
        for (AggregateResult t : [SELECT Count(Id), Transfer_Account__r.Id Id FROM Telemarketing__c WHERE Name = 'Przekazanie danych: adaptacje projektów' AND Status__c = 'Won' AND Post_Conversion_Stage__c = 'In Progress' AND Transfer_Account__r.Partner_Architect__c = true AND Transfer_Account__r.Account_Community_Member_Email__c != '' AND Transfer_Account__r.Id NOT IN ('001b000000k0HBr','001b000000XHfPA','001b000001NdAmB','001b000001NdAx9','001b000001NdArp','001b000002M62R9','001b000000DpFnt') GROUP BY Transfer_Account__r.Id]) {
           ids.add(t.Id);
        }
        return ids;
    }
    
    public static void sendArchitectEmail(Set<Id> ids) {
         
        List<EmailTemplate> et = [SELECT Id FROM EmailTemplate WHERE Name='Architekci - status klientów' LIMIT 1];        
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage>();      
        
        for (Account acc : [SELECT Id, Account_Community_Member_Email__c FROM Account WHERE Id IN :ids]) {
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.toAddresses = new String[] {acc.Account_Community_Member_Email__c};
            message.setTemplateId (et[0].Id);
            message.setTargetObjectId(UserInfo.getUserId());
            message.setSaveAsActivity(false);
            message.setTreatTargetObjectAsRecipient(false);
            message.setReplyTo('marta.miecznikowska@extradom.pl');
            message.setSenderDisplayName('marta.miecznikowska@extradom.pl');         
                                      
            PageReference pdf = Page.ArchitectsPDFAttachment;
            pdf.getParameters().put('id',acc.Id);
                        
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            Blob b = Test.isRunningTest() ? Blob.valueOf('test') : pdf.getContent();
            efa.setFileName('Lista klientów przekazanych do adaptacji - Extradom.pdf');
            efa.setBody(b);
            message.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
            
            messages.add(message);
        }
        
        Messaging.sendEmail(messages);
    }
}