/**
 * Created by grzegorz.dlugosz on 20.05.2019.
 */
public without sharing class CampaignMemberTriggerHandler extends TriggerHandler {

    public override void afterInsert() {
        Map<Id, CampaignMember> cmNewMap = (Map<Id, CampaignMember>) Trigger.newMap;
        
        // update info about existing PKO Campaign Member associated with Account
        updateAccountInfo(cmNewMap);
        // send campaign member information to Total Money. Only for PKO campaign
        sendCampaignMemberToTotal(cmNewMap);
    }
    
    private void updateAccountInfo(Map<Id, CampaignMember> cmNewMap) {
        List<Id> contactIds = new List<Id>();
        Campaign_Settings__c cs = Campaign_Settings__c.getInstance();
        
        if (String.isNotBlank(cs.PKO_Campaign_Id__c)) {
            for (CampaignMember cm : cmNewMap.values()) {
                // if its member of PKO campaign
                if (cm.CampaignId == cs.PKO_Campaign_Id__c) {
                    contactIds.add(cm.ContactId);
                }
            }
        }
        
        if (!contactIds.isEmpty()) {
            List<Account> accs = [SELECT Id, Has_PKO_Campaign_Member__c FROM Account WHERE Id IN (SELECT AccountId FROM Contact WHERE Id IN :contactIds)];            
            for (Account a : accs) {
                a.Has_PKO_Campaign_Member__c = true;
            }
            update accs;
        }   
    }
    
    private void sendCampaignMemberToTotal(Map<Id, CampaignMember> cmNewMap) {
        List<CampaignMember> campaignMemberToSendList = new List<CampaignMember>();
        Campaign_Settings__c cs = Campaign_Settings__c.getInstance();

        if (String.isNotBlank(cs.PKO_Campaign_Id__c)) {
            for (CampaignMember cm : cmNewMap.values()) {
                // if its member of PKO campaign
                if (cm.CampaignId == cs.PKO_Campaign_Id__c) {
                    campaignMemberToSendList.add(cm);
                }
            }
        }

        if (!campaignMemberToSendList.isEmpty()) {
            // TODO: send only 1st element for now
            // @future method
            if (!system.isBatch() && !system.isFuture()) {
                sendToTotalMoney(campaignMemberToSendList.get(0).Id);
            }
        }
    }

    @future(callout=true)
    private static void sendToTotalMoney(Id cmId) {
        List<CampaignMember> cmToSendList = [SELECT IDMultiFormData__c, First_Name__c, Last_Name__c, Email__c, Phone__c, Postal_Code__c FROM CampaignMember WHERE Id = :cmId];
        CampaignMember cmToSend = cmToSendList.get(0);

        Http http = new Http();
        HttpRequest request = new HttpRequest();
        String reqUrl = 'https://direct.money.pl/o/extradom.php';
        request.setEndpoint(reqUrl);
        request.setMethod('POST');
        request.setHeader('Content-Type','application/x-www-form-urlencoded');

        TotalMoneyDataWrapper dataWrapper = new TotalMoneyDataWrapper(cmToSend.First_Name__c, cmToSend.Last_Name__c,
                                                                        cmToSend.Email__c, cmToSend.Postal_Code__c, cmToSend.Phone__c);
        Map<String, String> mapToSend = new Map<String, String>();
        mapToSend.put('data', JSON.serialize(dataWrapper));
        String payLoad = urlEncode(mapToSend);
        request.setBody(payLoad);
        System.HttpResponse response = new System.Http().send(request);
        if (response.getStatusCode() == 200) {
            try {
                ResponseWrapper respWrapper = (ResponseWrapper) JSON.deserialize(response.getBody(), ResponseWrapper.class);
                if (respWrapper.status == 'ok' && respWrapper.msg != null && respWrapper.msg.IDMultiFormData != null) {
                    cmToSend.IDMultiFormData__c = String.valueOf(respWrapper.msg.IDMultiFormData);

                    update cmToSendList;
                } else {
                    System.debug('@@ TOTAL MONEY INTEGRATION ERROR MESSAGE: ' + response.getBody());
                }
            } catch(Exception e) {
                System.debug('@@ TOTAL MONEY INTEGRATION EXCEPTION: ' + e.getMessage());
                System.debug('@@ TOTAL MONEY INTEGRATION Body: ' + response.getBody());
            }
        }
    }

    /**
     * Author: Grzegorz Długosz
     * Description: Convert Map of key <-> value To application/x-www-form-urlencoded form
     * @param mapToConvert
     *          Map of key <-> value pairs that will be converted to x-www-form-urlencoded using UTF8
     * @return - String in x-www-form-urlencoded form, ready to set as a request body.
     */
    public static String urlEncode(Map<String, String> mapToConvert) {
        String result = '';
        for(String thisKey : mapToConvert.keySet()) {
            result += EncodingUtil.urlEncode(thisKey, 'UTF-8') + '=' + EncodingUtil.urlEncode(mapToConvert.get(thisKey), 'UTF-8') + '&';
        }
        return result.removeEnd('&');
    }

    /* Wrapper Used To create body of request that will be send to Total Money */
    public class TotalMoneyDataWrapper {
        public String name;
        public String surname;
        public String email;
        public String zip_code;
        public String phone;

        TotalMoneyDataWrapper(String name, String surname, String email, String zip_code, String phone) {
            this.name = name;
            this.surname = surname;
            this.email = email;
            this.zip_code = zip_code;
            this.phone = phone;
        }
    }

    /* Wrapper for deserialization of Total Money Response */
    public class ResponseWrapper {
        String status;
        MessageWrapper msg;
    }

    public class MessageWrapper {
        Integer IDMultiForm;
        Integer IDMultiFormData;
        // Offerers not needed
        Integer IDDistributor;
        String formName;
        String utm_source;
    }
}