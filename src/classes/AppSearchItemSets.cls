@RestResource(urlMapping='/app/search/itemsets')
global class AppSearchItemSets {
    @HttpGet
    global static ExtradomApi.ItemSearchSet doGet() {
    
        String q = RestContext.request.params.get('q');
        String type = RestContext.request.params.get('type');
        String department = RestContext.request.params.containsKey('department')?RestContext.request.params.get('department'):null;
        String category = RestContext.request.params.containsKey('category')?RestContext.request.params.get('category'):null;
        
        if (UserInfo.getUserType()=='Guest') {
            RestContext.response.addHeader('Cache-Control', 'must-revalidate, max-age=0, s-maxage=21600');
        } else {
            RestContext.response.addHeader('Cache-Control', 'no-cache, must-revalidate, max-age=0, no-store, private, s-maxage=0');
        }
        
        return ExtradomApi.getItemSearchSet(q, type, department, category);
    }
}