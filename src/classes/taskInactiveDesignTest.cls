@isTest
private class taskInactiveDesignTest {
     static testMethod void myTest(){
         
         Supplier__c testSupplier = new Supplier__c (Name='Test', Full_name__c='Test TST', Code__c='TST');
         insert testSupplier;
         Design__c invalidTestDesign = new Design__c (Name='Test', Full_name__c='Test TST1001', Code__c='TST1001', Status__c='Rejected', Supplier__c=testSupplier.Id);
         insert invalidTestDesign;
         Exception testException;
         
         try {
             Task invalidTestTask = new Task (Subject='invalidTestTask', WhatId=invalidTestDesign.Id);
             insert invalidTestTask;            
         }
         catch (exception e) {
            testException = e;
         }
         
         System.Assert(testException.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'));
         System.Assert(testException.getMessage().contains('WhatId'));
         System.Assert(testException.getMessage().contains('Wybrany projekt jest wycofany z oferty'));
         
         Design__c correctTestDesign = new Design__c (Name='Test', Full_name__c='Test TST1002', Code__c='TST1002', Status__c='Active', Supplier__c=testSupplier.Id);
         insert correctTestDesign;
         
         Task correctTestTask = new Task (Subject='correctTestTask', WhatId=correctTestDesign.Id);
         insert correctTestTask;
         
         Task afterInsertCorrectTestTask = [SELECT Id, WhatId FROM Task WHERE Id = :correctTestTask.Id];
         System.AssertEquals(afterInsertCorrectTestTask.WhatId, correctTestDesign.Id);
     }
}