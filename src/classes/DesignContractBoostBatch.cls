/**
* Created by grzegorz.dlugosz on 17.05.2019.
* This Batch Calculates new Contract Boost. It is Scheduled to run every day. Every executions ron for single Supplier!.
* To calculate new Contract Boost, It takes all Designs under Supplier with Featured <= 4.
* It calculated Sum of Product (Featured * Page Value) of all children Designs
* Then it counts Logarithm(10) from that sum and assigns to first Design (Ordered by featured, page value)
* The next Design boost takes 2/3 from previous result etc...
* */
global class DesignContractBoostBatch implements Database.Batchable<sObject> {

    String query;

    global DesignContractBoostBatch() {
        // select all suppliers that have Design connected
        query = 'SELECT Id FROM Supplier__c WHERE Id IN (SELECT Supplier__c FROM Design__c)';
    }

    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext bc, List<Supplier__c> supplierList) {
        Map<Id, Design__c> designToUpdateMap = new Map<Id, Design__c>();

        // Sum of Featured x Page Views from all connected designs with featured <= 4 for this supplier
        Decimal suppFeatPageViewSum = 0;

        // query and filter related Designs - done for 1 supplier
        for (Design__c design : [SELECT Supplier__c, Featured__c, Page_Value__c, Contract_Boost__c FROM Design__c
                                WHERE Supplier__c IN :supplierList AND Featured__c <= 4 AND Featured__c != NULL AND Page_Value__c != NULL ORDER BY Featured__c DESC, Page_Value__c DESC]) {

            suppFeatPageViewSum += design.Featured__c * design.Page_Value__c;
            designToUpdateMap.put(design.Id, design);
        }

        // Calculate Logarithm(10) of the calculated sum. If its < 0, then set it to 0.
        suppFeatPageViewSum = suppFeatPageViewSum == 0 ? 0 : Math.log10(suppFeatPageViewSum);
        suppFeatPageViewSum = suppFeatPageViewSum >= 0 ? suppFeatPageViewSum : 0;
        suppFeatPageViewSum = suppFeatPageViewSum == 0 ? 0 : suppFeatPageViewSum.setScale(5, System.RoundingMode.HALF_UP);

        // Assign correct Boost to Designs. 1st design in the list gets highest boost.
        for (Id designId : designToUpdateMap.keySet()) {
            Design__c design = designToUpdateMap.get(designId);

            design.Contract_Boost__c = suppFeatPageViewSum;
            suppFeatPageViewSum = (suppFeatPageViewSum * (Decimal.valueOf(2)/Decimal.valueOf(3))).setScale(5, System.RoundingMode.HALF_UP);
        }

        // update all the designs
        DesignTriggerHandler.BYPASS_INVALIDATE_DESIGNS = true;
        update designToUpdateMap.values();
        DesignTriggerHandler.BYPASS_INVALIDATE_DESIGNS = false;
    }

    public void finish(Database.BatchableContext bc) {
        if (!Test.isRunningTest()) {
            Database.executeBatch(new DesignOrderScoreBatch());
        }
    }
}