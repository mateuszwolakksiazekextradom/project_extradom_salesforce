public with sharing class GenerateGoogleMerchantFilesJob implements Schedulable {
    
    private static final String PRIMARY_FEED_HEADER = 'id\ttitle\tlink\timage link\tcondition\tavailability\tproduct type\tprice\tsale price\tsale price effective date\tcustom_label_0\tcustom_label_1\tcustom_label_2\tcustom_label_3\tcustom_label_4\n';
    private static final String PRIMARY_FEED_FILENAME = 'google-merchant-primary-feed.tsv';
    private static final String SUPPLEMENTAL_FEED_HEADER = 'id\tdescription\n';
    private static final String SUPPLEMENTAL_FEED_FILENAME = 'google-merchant-supplemental-feed';
    private static final String SUPPLEMENTAL_FEED_DEVNAME = 'google_merchant_supplemental_feed';
    private static final String FOLDER_NAME = 'Public Products';
    private static final String FOLDER_DEVNAME = 'Public_Products';
    
    public void execute(SchedulableContext sc) {
        String folderId = createFolder();
        savePrimaryFeed();
        saveSupplementalFeed(folderId);
    }
    
    private static String createFolder() {
        String folderId = null;
        List<Folder> folders = [SELECT Id FROM Folder WHERE DeveloperName = :FOLDER_DEVNAME AND Type = 'Document'];
        if (folders.size() > 0) {
            folderId = folders.get(0).Id;
        }
        return folderId;
    }
    
    @Future
    public static void savePrimaryFeed() {
        List<Document> documents = [SELECT Id FROM Document WHERE Name = :PRIMARY_FEED_FILENAME];
        transient String s = PRIMARY_FEED_HEADER;
        for (List<Design__c> designs : [
                SELECT Code__c, Full_Name__c, Canonical_Full_URL__c, Thumbnail_Full_URL__c, Product_Type__c, 
                       Gross_Price_ISO_4217__c, Promo_Price_ISO_4217__c, Promo_Price_End_Date_ISO_4217__c, 
                       Supplier_Code__c, Usable_Area_Segment__c, Show_Estimates__c, Show_Sketches__c, 
                       Merchant_Priority__c 
                FROM Design__c WHERE Status__c = 'Active' AND Gross_Price__c > 0
            ]) {
            for (Design__c d : designs) {
                s += d.Code__c + '\t' + d.Full_Name__c.normalizeSpace() + '\t' + d.Canonical_Full_URL__c + '\t' 
                    + d.Thumbnail_Full_URL__c + '\tnew\tin stock\t' + d.Product_Type__c + '\t' 
                    + d.Gross_Price_ISO_4217__c + '\t' + (d.Promo_Price_ISO_4217__c!=null?d.Promo_Price_ISO_4217__c:'') + '\t'
                    + (d.Promo_Price_End_Date_ISO_4217__c!=null?d.Promo_Price_End_Date_ISO_4217__c:'') + '\t' 
                    + d.Supplier_Code__c + '\t' + (d.Usable_Area_Segment__c!=null?d.Usable_Area_Segment__c:'') + '\t' 
                    + d.Show_Estimates__c + '\t' + d.Show_Sketches__c + '\t' + (d.Merchant_Priority__c!=null ? d.Merchant_Priority__c:'') + '\n';
            }
        }
        if (documents.size() > 0) {
            Document d = new Document (Id = documents.get(0).Id, Body = Blob.valueOf(s));
            update d;
        }
    }
    
    @Future
    public static void saveSupplementalFeed(String folderId) {
        String documentsName = SUPPLEMENTAL_FEED_FILENAME + '%';
        List<Document> documents = [SELECT Id, Name FROM Document WHERE Name like :documentsName ORDER BY DeveloperName];
        system.debug(documents);
        Map<String, String> documentMap = new Map<String, String> ();
        for (Document doc : documents) {
            documentMap.put(doc.Name, doc.Id);
        }
        
        String s = SUPPLEMENTAL_FEED_HEADER;
        Integer i = 1;
        for (List<Design__c> designs : [SELECT Code__c, Description__c, Use_Custom_Description__c, Automated_Description__c, Custom_Description__c FROM Design__c WHERE Status__c = 'Active' AND Gross_Price__c > 0]) {
            for (Design__c d : designs) {
                s += d.Code__c + '\t' + (d.Use_Custom_Description__c ? (d.Custom_Description__c != null ? d.Custom_Description__c.normalizeSpace().replaceAll('"','') : '') : (d.Automated_Description__c != null && d.Automated_Description__c != '' ? d.Automated_Description__c.normalizeSpace().replaceAll('"','') : (d.Description__c != null ? d.Description__c.normalizeSpace().replaceAll('"','') : ''))) + '\n';
                if (s.length() > 3500000) {
                    Document doc = getSupplementalDocument(documentMap, i, folderId);
                    doc.Body = Blob.valueOf(s);
                    upsert doc;
                    s = SUPPLEMENTAL_FEED_HEADER;
                    system.debug(i);
                    i++;
                }
            }
        }
        
        Document doc = getSupplementalDocument(documentMap, i, folderId);
        doc.Body = Blob.valueOf(s);
        upsert doc;
    }
    
    private static Document getSupplementalDocument(Map<String, String> documentMap, Integer index, String folderId) {
        String name = SUPPLEMENTAL_FEED_FILENAME + String.valueOf(index) + '.tsv';
        Document d = null;
        if (documentMap.containsKey(name)) {
            d = new Document (Id = documentMap.get(name));
        } else {
            d = new Document(
                Name = name, DeveloperName = SUPPLEMENTAL_FEED_DEVNAME + String.valueOf(index) + '_tsv',
                FolderId = folderId, ContentType = 'text/plain'
            );
        }
        return d;
    }
    
    public class GenerateGoogleMerchantFilesJobException extends Exception {
        
    }
}