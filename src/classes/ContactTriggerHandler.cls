public without sharing class ContactTriggerHandler extends TriggerHandler {
    private static final String EMPTY_EMAIL_ERROR = 'Cannot add Contact with no Account and Email specified';

    public override void afterUpdate() {
        List<Contact> contacts = (List<Contact>) Trigger.new;
        Map<Id, Contact> oldContacts = (Map<Id, Contact>) Trigger.oldMap;
        verifyOptInMarketing(contacts, oldContacts);
        updateAccounts(contacts, oldContacts);
    }

    public override void beforeInsert() {
        List<Contact> contacts = (List<Contact>) Trigger.new;
        assignAccounts(contacts);
        setCommunityMembers(contacts);
    }

    public override void beforeUpdate() {
        List<Contact> contacts = (List<Contact>) Trigger.new;
        setCommunityMembers(contacts);
    }

    public override void afterInsert() {
        List<Contact> contacts = (List<Contact>) Trigger.new;
        Map<Id, Contact> contactsMap = (Map<Id, Contact>) Trigger.newMap;
        Map<Id, Contact> oldContactsMap = (Map<Id, Contact>) Trigger.oldMap;
        verifyOptInMarketing(contacts, null);
        assignTasks(contactsMap);
        updateAccounts(contacts, oldContactsMap);
    }

    public override void afterDelete() {
        List<Contact> contacts = (List<Contact>) Trigger.old;
        verifyOptInMarketing(contacts, null);
        updateAccounts(contacts, null);
    }

    private void verifyOptInMarketing(List<Contact> contacts, Map<Id, Contact> oldContacts) {
        Set<String> accountsToVerify = new Set<String> ();
        for (Contact c : contacts) {
            if (((oldContacts == null && c.Opt_In_Marketing__c) || (oldContacts != null && oldContacts.get(c.Id).Opt_In_Marketing__c != c.Opt_In_Marketing__c))
                    || ((oldContacts == null && c.Opt_In_Marketing_Extended__c)
                    || (oldContacts != null && oldContacts.get(c.Id).Opt_In_Marketing_Extended__c != c.Opt_In_Marketing_Extended__c))
                    || ((oldContacts == null && c.Opt_In_Marketing_Partner__c)
                    || (oldContacts != null && oldContacts.get(c.Id).Opt_In_Marketing_Partner__c != c.Opt_In_Marketing_Partner__c))
                    || ((oldContacts == null && c.Opt_In_Lead_Forward__c)
                    || (oldContacts != null && oldContacts.get(c.Id).Opt_In_Lead_Forward__c != c.Opt_In_Lead_Forward__c))
                    || ((oldContacts == null && c.Opt_Ins_Asked__c)
                    || (oldContacts != null && oldContacts.get(c.Id).Opt_Ins_Asked__c != c.Opt_Ins_Asked__c))
                    || ((oldContacts == null && c.Opt_In_Phone_Partner__c)
                    || (oldContacts != null && oldContacts.get(c.Id).Opt_In_Phone_Partner__c != c.Opt_In_Phone_Partner__c))
                    || (oldContacts != null && oldContacts.get(c.Id).AccountId != c.AccountId)) {
                accountsToVerify.add(c.AccountId);
            }

            if (oldContacts != null && c.AccountId != oldContacts.get(c.Id).AccountId) {
                accountsToVerify.add(c.AccountId);
                accountsToVerify.add(oldContacts.get(c.Id).AccountId);
            }
        }

        if (accountsToVerify.size() > 0) {
            List<Account> accounts = [
                    SELECT Id, Has_Opt_In_Marketing__c, Has_Opt_In_Marketing_Extended__c, Has_Opt_In_Marketing_Partner__c,
                            Has_Opt_In_Lead_Forward__c, Has_Opt_Ins_Asked__c, Has_Opt_In_Phone_Partner__c,
                    (
                            SELECT Opt_In_Marketing__c, Opt_In_Marketing_Extended__c, Opt_In_Marketing_Partner__c,
                                    Opt_In_Lead_Forward__c, Opt_Ins_Asked__c, Opt_In_Phone_Partner__c
                            FROM Contacts WHERE Opt_In_Marketing__c = true OR Opt_In_Marketing_Extended__c = true
                            OR Opt_In_Marketing_Partner__c = true OR Opt_In_Lead_Forward__c = true OR Opt_Ins_Asked__c = true
                            OR Opt_In_Phone_Partner__c = true
                    )
                    FROM Account WHERE Id in :accountsToVerify
            ];
            Set<Id> optInMarketing = new Set<Id> ();
            Set<Id> optInMarketingPartner = new Set<Id> ();
            Set<Id> optInMarketingExtended = new Set<Id> ();
            Set<Id> optInLeadForward = new Set<Id> ();
            Set<Id> optInAsked = new Set<Id> ();
            Set<Id> optInPhonePartner = new Set<Id> ();
            for (Account acc : accounts) {
                if (acc.Contacts != null && acc.Contacts.size() > 0) {
                    for (Contact c : acc.Contacts) {
                        if (c.Opt_In_Marketing__c) {
                            optInMarketing.add(acc.Id);
                        }
                        if (c.Opt_In_Marketing_Partner__c) {
                            optInMarketingPartner.add(acc.Id);
                        }
                        if (c.Opt_In_Marketing_Extended__c) {
                            optInMarketingExtended.add(acc.Id);
                        }
                        if (c.Opt_In_Lead_Forward__c) {
                            optInLeadForward.add(acc.Id);
                        }
                        if (c.Opt_Ins_Asked__c) {
                            optInAsked.add(acc.Id);
                        }
                        if (c.Opt_In_Phone_Partner__c) {
                            optInPhonePartner.add(acc.Id);
                        }
                    }
                }
            }
            for (Account acc : accounts) {
                acc.Has_Opt_In_Marketing__c = optInMarketing.contains(acc.Id);
                acc.Has_Opt_In_Marketing_Extended__c = optInMarketingExtended.contains(acc.Id);
                acc.Has_Opt_In_Marketing_Partner__c = optInMarketingPartner.contains(acc.Id);
                acc.Has_Opt_In_Lead_Forward__c = optInLeadForward.contains(acc.Id);
                acc.Has_Opt_Ins_Asked__c = optInAsked.contains(acc.Id);
                acc.Has_Opt_In_Phone_Partner__c = optInPhonePartner.contains(acc.Id);
            }
            update accounts;
        }
    }

    private void assignAccounts(List<Contact> contacts) {
        for (Contact c : contacts) {
            if (String.isBlank(c.AccountId)) {
                if (String.isNotBlank(c.Phone)) {
                    c.FirstName = '';
                    c.LastName = c.Phone;
                    Account a = new Account(Name = c.LastName);
                    a.AccountSource = 'Inbound Call';
                    insert a;
                    c.AccountId = a.Id;
                } else if (String.isBlank(c.Email)) {
                    c.AddError(EMPTY_EMAIL_ERROR);
                } else {
                    c.LastName = String.isBlank(c.LastName) ? c.Email : c.LastName;
                    Account a = new Account(Name = c.LastName);
                    a.AccountSource = String.isBlank(c.Community_Email__c) ? ConstUtils.ACC_IMPORT_SOURCE : ConstUtils.ACC_EXTRADOM_SOURCE;
                    insert a;
                    c.AccountId = a.Id;
                }
            }
        }
    }

    private void setCommunityMembers(List<Contact> contacts) {
        List<String> emails = new List<String>{};
        for (Contact c : contacts) {
            if (String.isNotBlank(c.Email) && c.Community_Member__c == null) {
                emails.add(c.Email);
            }
        }

        if (emails.size() > 0) {
            List<Community_Member__c> cms = [SELECT Id, Email__c FROM Community_Member__c WHERE Email__c != '' AND Email__c IN :emails];
            Map<String,ID> emailsCms = new Map<String,ID>{};
            for (Community_Member__c cm : cms) {
                emailsCms.put(cm.Email__c.toLowerCase(), cm.Id);
            }

            for (Contact c : contacts) {
                if (String.isNotBlank(c.Email) && c.Community_Member__c == null) {
                    if (emailsCms.containsKey(c.Email.toLowerCase())) {
                        c.Community_Member__c = emailsCms.get(c.Email.toLowerCase());
                    }
                }
                if (String.isBlank(c.Salutation) && String.isNotBlank(c.FirstName)) {
                    try {
                        FirstName__c fn = FirstName__c.getInstance(c.FirstName);
                        if (fn!=null) {
                            c.Salutation = fn.Salutation__c;
                        }
                    } catch (Exception e) {
                    }
                }
            }
        }
    }

    private void assignTasks(Map<Id, Contact> contactsMap) {
        Map<Id, Id> ids = new Map<Id, Id> ();
        Map<String, Id> ms = new Map<String,Id> ();

        for (Contact c : [SELECT Id, Phone FROM Contact WHERE Id IN :contactsMap.keySet()]) {
            ms.put(c.Phone, c.Id);
        }

        List<Task> inboundCalls = [
                SELECT Id, Inbound_Number__c FROM Task
                WHERE Type = :ConstUtils.TASK_CALL_TYPE AND CreatedDate = today AND WhoId = ''
                AND CallType = :ConstUtils.TASK_INBOUND_CALL_TYPE AND Inbound_Number__c IN :ms.keySet()
        ];

        for (Task t : inboundCalls) {
            if (ms.containsKey(t.Inbound_Number__c)) {
                ids.put(t.Id, ms.get(t.Inbound_Number__c));
            }
        }

        if (ids.size() > 0) {
            List<Task> toUpdate = [SELECT Id, WhoId FROM Task WHERE Id IN :ids.keySet()];

            for (Task t : toUpdate) {
                t.WhoId = ids.get(t.Id);
            }

            update toUpdate;
        }
    }

    private void updateAccounts(List<Contact> contacts, Map<Id, Contact> oldContacts) {
        Set<String> accountIds = new Set<String> ();
        for (Contact c : contacts) {
            if ((c.Phone != null && oldContacts == null) || (c.Email != null && oldContacts == null)
                    || (oldContacts != null &&
                            (
                                    (String.isBlank(c.Email) && String.isNotBlank(oldContacts.get(c.Id).Email))
                                    || (String.isNotBlank(c.Email) && String.isBlank(oldContacts.get(c.Id).Email))
                                    || (String.isBlank(c.Phone) && String.isNotBlank(oldContacts.get(c.Id).Phone))
                                    || (String.isNotBlank(c.Phone) && String.isBlank(oldContacts.get(c.Id).Phone))
                                    || oldContacts.get(c.Id).AccountId != c.AccountId
                            )
                       )
                ) {
                    if (c.AccountId != null) {
                        accountIds.add(c.AccountId);
                    }
            }
        }

        if (accountIds.size() > 0) {
            List<Account> accounts = [
                    SELECT Id, Has_Contact_with_Phone__c, Has_Contact_with_Email__c,
                            (SELECT Email, Phone FROM Contacts WHERE Email != null OR Phone != null)
                    FROM Account WHERE Id in :accountIds
            ];
            for (Account a : accounts) {
                Boolean hasPhone = false;
                Boolean hasEmail = false;
                if (a.Contacts != null && a.Contacts.size() > 0) {
                    for (Contact c : a.Contacts) {
                        if (String.isNotBlank(c.Phone)) {
                            hasPhone = true;
                        }
                        if (String.isNotBlank(c.Email)) {
                            hasEmail = true;
                        }
                    }
                }
                a.Has_Contact_with_Email__c = hasEmail;
                a.Has_Contact_with_Phone__c = hasPhone;
            }
            update accounts;
        }
    }
}