@isTest
private class ContentVersionTest {

    static testMethod void myUnitTest() {
        ContentVersion cv = new ContentVersion();
        cv.VersionData = EncodingUtil.base64Decode('iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABD0lEQVR42q2RyU6EQBCGff/ncJam2UaQKB5kLuITcBy5EgIJSyAsyW9R0c5EQh+c+ZNKr/X1X9UPAG6KuwMwTRM+4hjSsrEXBoRpITqf0XUd/moFmOcZT76PHSXujoLjkWJZm7aNpm31gDj+pMsCBymRJAlaSkjTdHHDkDB80wN+LnLytbIsYyd7Q4JcbgOoZga0a6s4SpMhZVluA6hhXPfl8oVrNU0DgvPZMAzbgPco4ldM20Ge5yBx406ex878IND3gL5qaZjq/kGaNDd4Tnt8VlXVFoCD6395DZcEFd5z8PsTPNZ1vQlQGscRRVGg73te08vKnYKsAXopiBCwXVcH0EMsx4Xjnv4BWOt2wDd5rWWmTXIYTQAAAABJRU5ErkJggg==');
        cv.Title = 'test';
        cv.PathOnClient ='/test.png';
        insert cv;
    }
    
}