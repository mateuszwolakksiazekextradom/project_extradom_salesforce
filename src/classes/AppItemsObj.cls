@RestResource(urlMapping='/app/items/*')
global class AppItemsObj {
    @HttpGet
    global static ExtradomApi.Item doGet() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        ID itemId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        
        if (UserInfo.getUserType()=='Guest') {
            RestContext.response.addHeader('Cache-Control', 'must-revalidate, max-age=0, s-maxage=300');
        } else {
            RestContext.response.addHeader('Cache-Control', 'no-cache, must-revalidate, max-age=0, no-store, private, s-maxage=0');
        }
        
        return ExtradomApi.getItem(itemId, true, true);
        
    }
}