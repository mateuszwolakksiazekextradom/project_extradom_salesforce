@isTest
private class contactUpdateAccountInfoTest {
    static testMethod void myTest(){
        
        Account testAccount = new Account(Name='Test Account');
        insert testAccount;
        
        List<Contact> testContactsPhone = new List<Contact> {new Contact(LastName='Test1', Phone='100200300', AccountId=testAccount.Id), new Contact(LastName='Test2', AccountId=testAccount.Id)};
        insert testContactsPhone;
     
        List<Contact> testContactsEmail = new List<Contact> {new Contact(LastName='Test3', Email='test@test.pl', AccountId=testAccount.Id), new Contact(LastName='Test4', AccountId=testAccount.Id)};
        insert testContactsEmail;
        
        Account testAfterAccount = [SELECT Id, Has_Contact_with_Phone__c, Has_Contact_with_Email__c FROM Account WHERE Id = :testAccount.Id]; 
                
        System.assertEquals(testAfterAccount.Has_Contact_with_Email__c, true);
        System.assertEquals(testAfterAccount.Has_Contact_with_Phone__c, true);
    }
}