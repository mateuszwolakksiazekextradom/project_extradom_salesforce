@IsTest(SeeAllData=true)
private class TopicViewControllerTest {

    static testMethod void myUnitTest() {
    
        Account a = new Account(Name='test account');
        insert a;
        
        ConnectApi.Topic topic = ConnectApi.Topics.assignTopicByName(null, a.Id, 'test');
    
        ConnectApi.FeedElementPage testPage = new ConnectApi.FeedElementPage();    
        ConnectApi.ChatterFeeds.setTestGetFeedElementsFromFeed(null, ConnectApi.FeedType.Topics, topic.id, null, 100, null, testPage);
        ConnectApi.ChatterFeeds.setTestGetFeedElementsFromFeed(null, ConnectApi.FeedType.Topics, topic.id, testPage);
        
        ConnectApi.TopicPage testTopicPage = new ConnectApi.TopicPage(); 
        ConnectApi.Topics.setTestGetRelatedTopics(null, topic.id, testTopicPage);
        
        PageReference pageRef = new PageReference('http://extradom.pl/topic');
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', topic.id);
        
        TopicViewController tvc = New TopicViewController();
        tvc.getFeedElementsFromFeed();
        tvc.getFollowers();
        tvc.getTopic();
        tvc.getRelatedTopics();
        tvc.getTopContentPosts();
        
    }
}