@RestResource(urlMapping='/collectionrequest')
global class CollectionRequestRest {
    @HttpPost
    global static Map<String,String> doPost(String email, String phone, Boolean optInRegulations, Boolean optInPrivacyPolicy, Boolean optInMarketing, Boolean optInMarketingExtended, Boolean optInMarketingPartner, Boolean optInLeadForward, String gclid, Integer minArea, Integer maxArea, Integer plotSize, Integer minRooms, Integer maxRooms, String level, String roof, String garage, String description, Boolean optInEmail, Boolean optInPhone, Boolean optInProfiling, Boolean optInPKO)  {        
        Map<String,Id> data = OrderMethods.getWebLeadContactDetails('Design Collections', '', email, email, phone, 'Internal', false, false, 'No');
               
        if (String.isNotBlank(gclid)) {
            OrderMethods.updateGclid(data.get('accountId'), gclid);
        }
        RestRequest req = RestContext.request;
        if (req.headers.containsKey('gaClientId')) {
            OrderMethods.updateGaClientId(data.get('accountId'), req.headers.get('gaClientId'));
        }
        
        OrderMethods.updateOptIns(email, phone, optInRegulations, optInPrivacyPolicy, optInMarketing, false, false, false, optInEmail, optInPhone, optInProfiling, optInPKO);
        
        Id oppId = OrderMethods.organizeOpportunity(data.get('accountId'));
        OrderMethods.updateAccountFilters(data.get('accountId'), minArea, maxArea, plotSize, minRooms, maxRooms, level, roof, garage, description, null, null, null, null, null, null, null, null, null, null, null);
        
        Queues__c q = Queues__c.getInstance(); 
        Id queueId = Test.isRunningTest() ? '005b0000000RxOP' : [select Id, Name from Group where Type = 'Queue' AND Name = :q.Design_Collection__c][0].Id;
        
        RecordTypeIds__c rti = RecordTypeIds__c.getInstance();
        Design_Collection__c dc = new Design_Collection__c(Name='Prezentacja projektów', RecordTypeId  = rti.CollectionRecordId__c, Contact__c=data.get('contactEmailId'), Account__c=data.get('accountId'), OwnerId=queueId, Status__c = 'Draft', Opportunity__c = oppId);
        insert dc;
        
        if(data.get('accountId') != data.get('account2Id')) {
            Task t = new Task(Type='Possible Duplicate', OwnerId=data.get('ownerId'), WhatId = data.get('accountId'), Subject='Klient podał numer: ' + phone + ' na formularzu. Prawdopodobnie do scalenia', ActivityDate = system.today());
            insert t;       
        }
        
        //Result dla page
        Map<String,String> result = new Map<String,String>{'result' => 'Dziękujemy za zainteresowanie. Prezentacja zostanie przesłana na podany adres poczty e-mail.'};
        return result;
    }
    global class OverLimitOrderException extends Exception {}
}