@isTest
private class B2BSummaryControllerTest {
    
    @isTest 
    static void unitTests() {
        B2BSummaryController bsc = new B2BSummaryController();
        Telemarketing__c t1 = new Telemarketing__c(Name='Przekazanie danych: adaptacje projektów', OwnerId = '005b0000005dyCz');
        Telemarketing__c t2 = new Telemarketing__c(Name='DK Notus', OwnerId = '005b0000005dyCz');
        Telemarketing__c t3 = new Telemarketing__c(Name='Przekazanie danych: materiały budowlane', OwnerId = '005b0000005dyCz');
        insert new List<Telemarketing__c>{t1,t2,t3};
        
        String test1 = bsc.toExplain;
        String test2 = bsc.bearings;
        String test3 = bsc.message;
        Decimal test4 = bsc.orderObjective;
        bsc.getInvestorOrders();
        bsc.getDealerOrders();
        bsc.getSumB2BOrders();
        bsc.getTelemarketingUsers();
        bsc.getTelemarketingTodayUsers();
        bsc.getB2BUsers();
    }

}