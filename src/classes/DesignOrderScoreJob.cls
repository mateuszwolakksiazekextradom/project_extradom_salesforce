global without sharing class DesignOrderScoreJob implements Schedulable {
    public void execute(SchedulableContext sc) {
        Database.executeBatch(new DesignOrderScoreBatch());
    }
}