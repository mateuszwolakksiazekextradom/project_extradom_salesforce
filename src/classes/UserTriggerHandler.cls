/**
 * Created by mateusz.wolak on 05.06.2019.
 */

public without sharing class UserTriggerHandler extends TriggerHandler {

    public override void afterUpdate() {
        Map<Id, User> oldUserMap = (Map<Id, User>) Trigger.oldMap;
        List<User> newUserList = (List<User>) Trigger.new;

        assignPermissionSet(oldUserMap, newUserList);
    }


    public override void afterInsert() {
        List<User> newUserList = (List<User>) Trigger.new;
        assignPermissionSet(null, newUserList);
    }



    /**
     * @author: Mateusz Wolak-Książęk
     * if it's supliers customer community user (user with "Suppliers CC User" profile) then assign permission set, which gives access to supliers community
     *
     * @param 1: old users map or null (if it's before insert context)
     * @param 2: new users map
     **/
    private void assignPermissionSet( Map<Id, User> oldUserMap, List<User> newUserList) {
        List<User> userWithSuppliersCcUserProfile = new List<User>();

        //gather users with such profile id
        if(oldUserMap != null && !oldUserMap.isEmpty()) {

            for(User userN : newUserList) {
                User userO = oldUserMap.get(userN.Id);

                if(
                    userO.ProfileId != userN.ProfileId &&
                    userN.ProfileId == ConstUtils.getProfileId(ConstUtils.USER_PROFILE_SUPPLIERS_CC_USER)
                ) {
                    userWithSuppliersCcUserProfile.add(userN);
                }

            }

        } else {

            for(User userN : newUserList) {

                if(
                    userN.ProfileId == ConstUtils.getProfileId(ConstUtils.USER_PROFILE_SUPPLIERS_CC_USER)
                ) {
                    userWithSuppliersCcUserProfile.add(userN);
                }

            }

        }

         //do the work - assigne permision set
        if(!userWithSuppliersCcUserProfile.isEmpty()) {
            List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();

            for(User u : userWithSuppliersCcUserProfile) {

                if(u.ContactId != null) {
                    psaList.add(
                        new PermissionSetAssignment (
                            PermissionSetId = ConstUtils.getPermissionSetId(ConstUtils.USER_PERMISSION_SET_SUPPLIERS_COMMUNITY_MEMBER),
                            AssigneeId = u.Id
                        )
                    );
                }

            }

            if(!psaList.isEmpty()) {
                try {
                    insert psaList;
                } catch(Exception exc) {
                    System.debug(LoggingLevel.ERROR, 'Error during permission set assigment ' + exc);
                }

            }

        }

    }


}