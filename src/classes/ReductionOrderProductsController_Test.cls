@IsTest
private class ReductionOrderProductsController_Test {
    @IsTest
    private static void shouldReturnAllRegularOrderItems_whenNewReductionOrderCreated() {
        // given
        Order regularOrder = [SELECT Id, Pricebook2Id, EffectiveDate, AccountId from Order LIMIT 1];
        Map<Id, OrderItem> exisitingItems = new Map<Id, OrderItem> ([SELECT Id, AvailableQuantity FROM OrderItem WHERE OrderId = :regularOrder.Id]);
        Order reductionOrder = new Order(IsReductionOrder = true, OriginalOrderId = regularOrder.Id, Pricebook2Id = regularOrder.Pricebook2Id,
                EffectiveDate = regularOrder.EffectiveDate, Status = ConstUtils.ORDER_DRAFT_STATUS, AccountId = regularOrder.AccountId
        );
        insert reductionOrder;

        // when
        List<ReductionOrderItemWrapper> reductionItems = ReductionOrderProductsController.getReductionOrderItems(reductionOrder.Id);

        // then
        System.assertEquals(2, reductionItems.size());
        for (ReductionOrderItemWrapper roi : reductionItems) {
            System.assertNotEquals(null, roi.orderItem.OriginalOrderItemId);
            System.assertEquals(roi.availableQuantity, exisitingItems.get(roi.orderItem.OriginalOrderItemId).AvailableQuantity);
            System.assertEquals(0, roi.orderItem.Quantity);
        }
    }

    @IsTest
    private static void shouldReturnUpdatedRegularOrderItems_whenReductionOrderRetrieved() {
        // given
        Order regularOrder = [SELECT Id, Pricebook2Id, EffectiveDate, AccountId from Order LIMIT 1];
        List<OrderItem> exisitingItems = [SELECT Id, AvailableQuantity, PricebookEntryId FROM OrderItem WHERE OrderId = :regularOrder.Id];
        Map<Id, OrderItem> exisitingItemsMap = new Map<Id, OrderItem> (exisitingItems);
        Order reductionOrder = new Order(IsReductionOrder = true, OriginalOrderId = regularOrder.Id, Pricebook2Id = regularOrder.Pricebook2Id,
                EffectiveDate = regularOrder.EffectiveDate, Status = ConstUtils.ORDER_DRAFT_STATUS, AccountId = regularOrder.AccountId
        );
        insert reductionOrder;
        OrderItem oi = new OrderItem(
                OriginalOrderItemId = exisitingItems.get(0).Id, Quantity = -exisitingItems.get(0).AvailableQuantity,
                PricebookEntryId = exisitingItems.get(0).PricebookEntryId, OrderId = reductionOrder.Id
        );
        insert oi;

        // when
        List<ReductionOrderItemWrapper> reductionItems = ReductionOrderProductsController.getReductionOrderItems(reductionOrder.Id);

        // then
        System.assertEquals(2, reductionItems.size());
        for (ReductionOrderItemWrapper roi : reductionItems) {
            if (roi.orderItem.Id == oi.Id) {
                System.assertEquals(oi.OriginalOrderItemId, roi.orderItem.OriginalOrderItemId);
                System.assertEquals(-oi.Quantity, roi.orderItem.Quantity);
            } else {
                System.assertNotEquals(null, roi.orderItem.OriginalOrderItemId);
                System.assertEquals(roi.availableQuantity, exisitingItemsMap.get(roi.orderItem.OriginalOrderItemId).AvailableQuantity);
                System.assertEquals(0, roi.orderItem.Quantity);
            }
        }
    }

    @IsTest
    private static void shouldReturnFilteredOrderItems_whenReductionOrderActivated() {
        // given
        Order regularOrder = [SELECT Id, Pricebook2Id, EffectiveDate, AccountId from Order LIMIT 1];
        List<OrderItem> exisitingItems = [SELECT Id, AvailableQuantity, PricebookEntryId FROM OrderItem WHERE OrderId = :regularOrder.Id];
        Map<Id, OrderItem> exisitingItemsMap = new Map<Id, OrderItem> (exisitingItems);
        Order reductionOrder = new Order(IsReductionOrder = true, OriginalOrderId = regularOrder.Id, Pricebook2Id = regularOrder.Pricebook2Id,
                EffectiveDate = regularOrder.EffectiveDate, Status = ConstUtils.ORDER_DRAFT_STATUS, AccountId = regularOrder.AccountId
        );
        insert reductionOrder;
        OrderItem oi = new OrderItem(
                OriginalOrderItemId = exisitingItems.get(0).Id, Quantity = -exisitingItems.get(0).AvailableQuantity,
                PricebookEntryId = exisitingItems.get(0).PricebookEntryId, OrderId = reductionOrder.Id
        );
        insert oi;
        reductionOrder.Status = ConstUtils.ORDER_ACCEPTED_STATUS;
        update reductionOrder;

        // when
        List<ReductionOrderItemWrapper> reductionItems = ReductionOrderProductsController.getReductionOrderItems(reductionOrder.Id);

        // then
        System.assertEquals(1, reductionItems.size());
        System.assertEquals(oi.Id, reductionItems.get(0).orderItem.Id);
        System.assertEquals(oi.OriginalOrderItemId, reductionItems.get(0).orderItem.OriginalOrderItemId);
        System.assertEquals(-oi.Quantity, reductionItems.get(0).orderItem.Quantity);
    }

    @IsTest
    private static void shouldSaveOrderItems() {
        // given
        Order regularOrder = [SELECT Id, Pricebook2Id, EffectiveDate, AccountId from Order LIMIT 1];
        Order reductionOrder = new Order(IsReductionOrder = true, OriginalOrderId = regularOrder.Id, Pricebook2Id = regularOrder.Pricebook2Id,
                EffectiveDate = regularOrder.EffectiveDate, Status = ConstUtils.ORDER_DRAFT_STATUS, AccountId = regularOrder.AccountId
        );
        insert reductionOrder;
        List<ReductionOrderItemWrapper> reductionItems = ReductionOrderProductsController.getReductionOrderItems(reductionOrder.Id);

        // when
        List<OrderItem> orderItemsToSave = new List<OrderItem> ();
        reductionItems.get(0).orderItem.Quantity = -reductionItems.get(0).availableQuantity;
        orderItemsToSave.add(reductionItems.get(0).orderItem);
        reductionItems.get(1).orderItem.Quantity = -reductionItems.get(1).availableQuantity;
        orderItemsToSave.add(reductionItems.get(1).orderItem);
        String result = ReductionOrderProductsController.saveReductionOrderItems(reductionOrder.Id, JSON.serialize(orderItemsToSave));
        List<OrderItem> orderItems = [SELECT Id, Quantity, PricebookEntryId FROM OrderItem WHERE OrderId = :reductionOrder.Id];

        // then
        System.assertEquals(null, result);
        System.assertEquals(2, orderItems.size());
    }

    @TestSetup
    private static void setupData() {
        Pricebook2 pricebook = new Pricebook2(Name = 'Test Pricebook', IsActive = true);
        insert pricebook;

        Product2 product1 = new Product2(
            Name = 'Test Product 1', Family = ConstUtils.PRODUCT_GIFT_FAMILY, IsActive = true
        );
        Product2 product2 = new Product2(
            Name = 'Test Product 2', Family = ConstUtils.PRODUCT_DESIGN_FAMILY, IsActive = true
        );
        insert new List<Product2> {product1, product2};

        PricebookEntry pe1 = new PricebookEntry(Product2Id = product1.Id, IsActive = true, Pricebook2Id = pricebook.Id, UnitPrice = 100);
        PricebookEntry spe1 = new PricebookEntry(
            Product2Id = product1.Id, IsActive = true, Pricebook2Id = Test.getStandardPricebookId(), UnitPrice = 100
        );
        PricebookEntry pe2 = new PricebookEntry(Product2Id = product2.Id, IsActive = true, Pricebook2Id = pricebook.Id, UnitPrice = 200);
        PricebookEntry spe2 = new PricebookEntry(
            Product2Id = product2.Id, IsActive = true, Pricebook2Id = Test.getStandardPricebookId(), UnitPrice = 200
        );
        insert new List<PricebookEntry> {spe1, spe2};
        insert new List<PricebookEntry> {pe1, pe2};

        Account acc = new Account (Name = 'Test Name');
        insert acc;
        Order order = new Order (
                AccountId = acc.Id, EffectiveDate = System.now().date(), Status = ConstUtils.ORDER_DRAFT_STATUS, Pricebook2Id = pricebook.Id,
                RecordTypeId = ConstUtils.ORDER_REGULAR_RT_ID
        );
        insert order;
        OrderItem oi1 = new OrderItem(
                PricebookEntryId = pe1.Id, Quantity = 2, UnitPrice = 100,
                List_Price__c = 400, OrderId = order.Id
        );
        OrderItem oi2 = new OrderItem(
                PricebookEntryId = pe2.Id, Quantity = 1, UnitPrice = 100,
                List_Price__c = 100, OrderId = order.Id
        );
        insert new List<OrderItem>{oi1, oi2};

        order.Status = ConstUtils.ORDER_ACCEPTED_STATUS;
        update order;
    }
}