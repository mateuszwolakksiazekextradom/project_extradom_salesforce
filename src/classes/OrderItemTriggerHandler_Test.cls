@IsTest
private class OrderItemTriggerHandler_Test {

    @IsTest
    private static void shouldAssignAccountId () {
        // given
        Account acc = new Account (Name = 'Test Name');
        insert acc;
        Design__c design = [SELECT Id, Name FROM Design__c WHERE Type__c = :ConstUtils.DESIGN_HOUSE_TYPE LIMIT 1];
        PricebookEntry pricebookEntry = [SELECT Id FROM PricebookEntry WHERE Product2.Family = :ConstUtils.PRODUCT_GIFT_FAMILY LIMIT 1];
        Order o = new Order (
                AccountId = acc.Id, EffectiveDate = System.now().date(), Status = ConstUtils.ORDER_DRAFT_STATUS,
                Pricebook2Id = Test.getStandardPricebookId(), RecordTypeId = ConstUtils.ORDER_REGULAR_RT_ID
        );
        insert o;

        // when
        OrderItem oi = new OrderItem(
                PricebookEntryId = pricebookEntry.Id, Quantity = 2, Discount_Sales__c = 300,
                List_Price__c = 400, Design__c = design.Id, OrderId = o.Id, UnitPrice = 0
        );
        insert oi;
        oi = [SELECT Account__c FROM OrderItem WHERE Id = :oi.Id];

        // then
        System.assertEquals(acc.Id, oi.Account__c);
    }

    @IsTest
    private static void shouldAssignDesignTypeAndProductFamily_whenOrderItemCreated () {
        // given
        Account acc = new Account (Name = 'Test Name');
        insert acc;
        Design__c design = [SELECT Id, Name FROM Design__c WHERE Type__c = :ConstUtils.DESIGN_HOUSE_TYPE LIMIT 1];
        PricebookEntry pricebookEntry = [SELECT Id FROM PricebookEntry WHERE Product2.Family = :ConstUtils.PRODUCT_GIFT_FAMILY LIMIT 1];
        Order o = new Order (
                AccountId = acc.Id, EffectiveDate = System.now().date(), Status = ConstUtils.ORDER_DRAFT_STATUS,
                Pricebook2Id = Test.getStandardPricebookId(), RecordTypeId = ConstUtils.ORDER_REGULAR_RT_ID
        );
        insert o;

        // when
        OrderItem oi = new OrderItem(
                PricebookEntryId = pricebookEntry.Id, Quantity = 2, Discount_Sales__c = 300,
                List_Price__c = 400, Design__c = design.Id, OrderId = o.Id, UnitPrice = 0
        );
        insert oi;
        oi = [SELECT Design_Type__c, Product_Family__c FROM OrderItem WHERE Id = :oi.Id];

        // then
        System.assertEquals(ConstUtils.DESIGN_HOUSE_TYPE, oi.Design_Type__c);
        System.assertEquals(ConstUtils.PRODUCT_GIFT_FAMILY, oi.Product_Family__c);
    }

    @IsTest
    private static void shouldAssignDesignTypeAndProductFamily_whenOrderItemUpdated () {
        // given
        Account acc = new Account (Name = 'Test Name');
        insert acc;
        Design__c houseDesign = [SELECT Id, Name FROM Design__c WHERE Type__c = :ConstUtils.DESIGN_HOUSE_TYPE LIMIT 1];
        Design__c garageDesign = [SELECT Id, Name FROM Design__c WHERE Type__c = :ConstUtils.DESIGN_GARAGE_TYPE LIMIT 1];
        PricebookEntry giftPe = [SELECT Id FROM PricebookEntry WHERE Product2.Family = :ConstUtils.PRODUCT_GIFT_FAMILY LIMIT 1];
        PricebookEntry designPe = [SELECT Id FROM PricebookEntry WHERE Product2.Family = :ConstUtils.PRODUCT_DESIGN_FAMILY LIMIT 1];
        Order o = new Order (
                AccountId = acc.Id, EffectiveDate = System.now().date(), Status = ConstUtils.ORDER_DRAFT_STATUS,
                Pricebook2Id = Test.getStandardPricebookId(), RecordTypeId = ConstUtils.ORDER_REGULAR_RT_ID
        );
        insert o;
        OrderItem oi = new OrderItem(
                PricebookEntryId = giftPe.Id, Quantity = 2, Discount_Sales__c = 300,
                List_Price__c = 400, Design__c = houseDesign.Id, OrderId = o.Id, UnitPrice = 0
        );
        insert oi;

        // when
        oi.Design__c = garageDesign.Id;
        update oi;
        oi = [SELECT Design_Type__c, Product_Family__c FROM OrderItem WHERE Id = :oi.Id];

        // then
        System.assertEquals(ConstUtils.DESIGN_GARAGE_TYPE, oi.Design_Type__c);
    }

    @IsTest
    private static void shouldRecalculateUpsellProducts_whenOrderItemsQuantityChanged () {
        Account acc = new Account (Name = 'Test Name');
        insert acc;
        Design__c design = [SELECT Id, Name FROM Design__c WHERE Type__c = :ConstUtils.DESIGN_HOUSE_TYPE LIMIT 1];
        List<PricebookEntry> pricebookEntries = [SELECT Id FROM PricebookEntry WHERE Product2.Family = :ConstUtils.PRODUCT_UPSELL_FAMILY];
        Order o = new Order (
                AccountId = acc.Id, EffectiveDate = System.now().date(), Status = ConstUtils.ORDER_DRAFT_STATUS,
                Pricebook2Id = Test.getStandardPricebookId(), RecordTypeId = ConstUtils.ORDER_REGULAR_RT_ID
        );
        insert o;

        // when
        List<OrderItem> orderItems = new List<OrderItem> ();
        for (PricebookEntry pe : pricebookEntries) {
            OrderItem oi = new OrderItem(
                    PricebookEntryId = pe.Id, Quantity = 2, Discount_Sales__c = 300,
                    List_Price__c = 400, Design__c = design.Id, OrderId = o.Id, UnitPrice = 0
            );
            orderItems.add(oi);
        }
        insert orderItems;
        o.Status = ConstUtils.ORDER_ACCEPTED_STATUS;
        update o;
        acc = [SELECT Upsell_Products__c FROM Account WHERE Id =: acc.Id];
        Decimal upsellProductsAfterInsert = acc.Upsell_Products__c;
        for (OrderItem oi : orderItems) {
            oi.Quantity = 1;
        }
        update orderItems;
        acc = [SELECT Upsell_Products__c FROM Account WHERE Id =: acc.Id];
        Decimal upsellProductsAfterUpdate = acc.Upsell_Products__c;

        // then
        System.assertEquals(4, upsellProductsAfterInsert);
        System.assertEquals(2, upsellProductsAfterUpdate);
    }

    @IsTest
    private static void shouldRecalculatePlanSetProducts_whenOrderItemsQuantityChanged () {
        Account acc = new Account (Name = 'Test Name');
        insert acc;
        Design__c design = [SELECT Id, Name FROM Design__c WHERE Type__c = :ConstUtils.DESIGN_HOUSE_TYPE LIMIT 1];
        List<PricebookEntry> pricebookEntries = [SELECT Id FROM PricebookEntry WHERE Product2.Family = :ConstUtils.PRODUCT_DESIGN_FAMILY];
        Order o = new Order (
                AccountId = acc.Id, EffectiveDate = System.now().date(), Status = ConstUtils.ORDER_DRAFT_STATUS,
                Pricebook2Id = Test.getStandardPricebookId(), RecordTypeId = ConstUtils.ORDER_REGULAR_RT_ID
        );
        insert o;

        // when
        List<OrderItem> orderItems = new List<OrderItem> ();
        for (PricebookEntry pe : pricebookEntries) {
            OrderItem oi = new OrderItem(
                    PricebookEntryId = pe.Id, Quantity = 2, Discount_Sales__c = 300,
                    List_Price__c = 400, Design__c = design.Id, OrderId = o.Id, UnitPrice = 0
            );
            orderItems.add(oi);
        }
        insert orderItems;
        o.Status = ConstUtils.ORDER_ACCEPTED_STATUS;
        update o;
        acc = [SELECT Count_Plan_Sets__c FROM Account WHERE Id =: acc.Id];
        Decimal planSetProductsAfterInsert = acc.Count_Plan_Sets__c;
        for (OrderItem oi : orderItems) {
            oi.Quantity = 1;
        }
        update orderItems;
        acc = [SELECT Count_Plan_Sets__c FROM Account WHERE Id =: acc.Id];
        Decimal planSetProductsAfterUpdate = acc.Count_Plan_Sets__c;

        // then
        System.assertEquals(4, planSetProductsAfterInsert);
        System.assertEquals(2, planSetProductsAfterUpdate);
    }


    @TestSetup
    private static void setupData() {
        Supplier__c newSupplier = new Supplier__c(Name = 'Supplier', Code__c = '11', Full_Name__c = 'Great Supplier');
        insert newSupplier;

        Design__c design1 = new Design__c (Name = 'First Design', Supplier__c = newSupplier.Id, Full_Name__c = 'First Design', Type__c = ConstUtils.DESIGN_HOUSE_TYPE);
        Design__c design2 = new Design__c (Name = 'Second Design', Supplier__c = newSupplier.Id, Full_Name__c = 'Second Design', Type__c = ConstUtils.DESIGN_GARAGE_TYPE);
        insert new List<Design__c> {design1, design2};

        Product2 product1 = new Product2(
                Name = 'Test Product 1', Family = ConstUtils.PRODUCT_GIFT_FAMILY, IsActive = true, Design__c = null,
                Version_Required__c = false, Line_Description_Required__c = false, Max_Discount__c = null
        );
        Product2 product2 = new Product2(
                Name = 'Test Product 2', Family = ConstUtils.PRODUCT_DESIGN_FAMILY, IsActive = true, Design__c = null,
                Version_Required__c = false, Line_Description_Required__c = false, Max_Discount__c = null
        );
        Product2 product3 = new Product2(
                Name = 'Test Product 3', Family = ConstUtils.PRODUCT_DESIGN_FAMILY, IsActive = true, Design__c = null,
                Version_Required__c = false, Line_Description_Required__c = false, Max_Discount__c = null
        );
        Product2 product4 = new Product2(
                Name = 'Test Product 4', Family = ConstUtils.PRODUCT_UPSELL_FAMILY, IsActive = true, Design__c = null,
                Version_Required__c = false, Line_Description_Required__c = false, Max_Discount__c = null
        );
        Product2 product5 = new Product2(
                Name = 'Test Product 5', Family = ConstUtils.PRODUCT_UPSELL_FAMILY, IsActive = true, Design__c = null,
                Version_Required__c = false, Line_Description_Required__c = false, Max_Discount__c = null
        );
        insert new List<Product2> {product1, product2, product3, product4, product5};

        PricebookEntry spe1 = new PricebookEntry(
                Product2Id = product1.Id, IsActive = true, Pricebook2Id = Test.getStandardPricebookId(), UnitPrice = 100
        );
        PricebookEntry spe2 = new PricebookEntry(
                Product2Id = product2.Id, IsActive = true, Pricebook2Id = Test.getStandardPricebookId(), UnitPrice = 100
        );
        PricebookEntry spe3 = new PricebookEntry(
                Product2Id = product3.Id, IsActive = true, Pricebook2Id = Test.getStandardPricebookId(), UnitPrice = 100
        );
        PricebookEntry spe4 = new PricebookEntry(
                Product2Id = product4.Id, IsActive = true, Pricebook2Id = Test.getStandardPricebookId(), UnitPrice = 100
        );
        PricebookEntry spe5 = new PricebookEntry(
                Product2Id = product5.Id, IsActive = true, Pricebook2Id = Test.getStandardPricebookId(), UnitPrice = 100
        );
        insert new List<PricebookEntry> {spe1, spe2, spe3, spe4, spe5};
    }
}