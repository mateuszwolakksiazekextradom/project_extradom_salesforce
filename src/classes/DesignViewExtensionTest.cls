@IsTest(SeeAllData=true)
private class DesignViewExtensionTest {

    static testMethod void myUnitTest() {
        
        test.startTest();
        
        Supplier__c s = new Supplier__c(Name='Test Supplier ABC', Full_Name__c='Test Supplier', Code__c='ABC');
        insert s;
        
        Design__c d1 = new Design__c(Name='Test Design', Full_Name__c='Test Design', Supplier__c=s.Id, Code__c='ABC1001');
        insert d1;
        
        Page__c p = new Page__c(Name='Projekty domów Test', Type__c='Collection', Design_Type__c = 'domy');
        insert p;
        
        
        PageReference pageRef = new PageReference('http://extradom.pl/projekty-domow');
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Area.From', '50');
        ApexPages.currentPage().getParameters().put('Area.To', '150');
        ApexPages.currentPage().getParameters().put('RoofSlope.From', '30');
        ApexPages.currentPage().getParameters().put('RoofSlope.To', '40');
        ApexPages.currentPage().getParameters().put('AllotmentMinWidth.Value', '60');
        ApexPages.currentPage().getParameters().put('TotalHeight.Value', '10');
        ApexPages.currentPage().getParameters().put('BuildingArea.Value', '200');
        ApexPages.currentPage().getParameters().put('Rooms.Value', '5');
        //
        ApexPages.currentPage().getParameters().put('Garage.111', 'on');
        //ApexPages.currentPage().getParameters().put('Garage.79', 'on');
        //ApexPages.currentPage().getParameters().put('Garage.50', 'on');
        //ApexPages.currentPage().getParameters().put('Garage.52', 'on');
        ApexPages.currentPage().getParameters().put('Garage.111', 'on');
        //ApexPages.currentPage().getParameters().put('Roof.4', 'on');
        //ApexPages.currentPage().getParameters().put('Roof.5', 'on');
        //ApexPages.currentPage().getParameters().put('Roof.12', 'on');
        //ApexPages.currentPage().getParameters().put('Roof.13', 'on');
        //ApexPages.currentPage().getParameters().put('Roof.7', 'on');
        //ApexPages.currentPage().getParameters().put('Roof.9', 'on');
        //ApexPages.currentPage().getParameters().put('Roof.3', 'on');
        //ApexPages.currentPage().getParameters().put('Roof.6', 'on');
        ApexPages.currentPage().getParameters().put('Roof.8', 'on');
        //ApexPages.currentPage().getParameters().put('LivingType.49', 'on');
        //ApexPages.currentPage().getParameters().put('LivingType.43', 'on');
        ApexPages.currentPage().getParameters().put('LivingType.42', 'on');
        //ApexPages.currentPage().getParameters().put('Style.81', 'on');
        //ApexPages.currentPage().getParameters().put('Style.77', 'on');
        //ApexPages.currentPage().getParameters().put('Style.90', 'on');
        //ApexPages.currentPage().getParameters().put('Style.98', 'on');
        ApexPages.currentPage().getParameters().put('Style.104', 'on');
        //ApexPages.currentPage().getParameters().put('Construction.82', 'on');
        //ApexPages.currentPage().getParameters().put('Construction.41', 'on');
        //ApexPages.currentPage().getParameters().put('Construction.39', 'on');
        //ApexPages.currentPage().getParameters().put('Construction.38', 'on');
        ApexPages.currentPage().getParameters().put('Construction.40', 'on');
        //ApexPages.currentPage().getParameters().put('Eco.83', 'on');
        ApexPages.currentPage().getParameters().put('Eco.84', 'on');
        //ApexPages.currentPage().getParameters().put('GarageLocation.66', 'on');
        //ApexPages.currentPage().getParameters().put('GarageLocation.67', 'on');
        //ApexPages.currentPage().getParameters().put('GarageLocation.73', 'on');
        //ApexPages.currentPage().getParameters().put('GarageLocation.70', 'on');
        ApexPages.currentPage().getParameters().put('GarageLocation.78', 'on');
        ApexPages.currentPage().getParameters().put('Basement.46', 'on');
        //ApexPages.currentPage().getParameters().put('Basement.72', 'on');
        ApexPages.currentPage().getParameters().put('Crest.106', 'on');
        ApexPages.currentPage().getParameters().put('Crest.105', 'on');
        
        ApexPages.StandardController scp = new ApexPages.StandardController(p);
        PageDesignResultsExtension extp = new PageDesignResultsExtension(scp);
        extp.getDesignResults();
        
        
        pageRef = new PageReference('http://extradom.pl/projekty-domow');
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('minarea', '50');
        ApexPages.currentPage().getParameters().put('maxarea', '150');
        ApexPages.currentPage().getParameters().put('maxplotsize', '27');
        ApexPages.currentPage().getParameters().put('maxheight', '8');
        ApexPages.currentPage().getParameters().put('maxfootprint', '250');
        ApexPages.currentPage().getParameters().put('minroofslope', '30');
        ApexPages.currentPage().getParameters().put('maxroofslope', '40');
        ApexPages.currentPage().getParameters().put('rooms', '5');
        ApexPages.currentPage().getParameters().put('level', '47');
        ApexPages.currentPage().getParameters().put('garage', '79');
        ApexPages.currentPage().getParameters().put('basement', '72');
        ApexPages.currentPage().getParameters().put('livingtype', '49');
        ApexPages.currentPage().getParameters().put('roof', '4');
        ApexPages.currentPage().getParameters().put('ridge', '105');
        ApexPages.currentPage().getParameters().put('construction', '82');
        ApexPages.currentPage().getParameters().put('eco', '83');
        ApexPages.currentPage().getParameters().put('garagelocation', '66');
        ApexPages.currentPage().getParameters().put('style', '77');
        ApexPages.currentPage().getParameters().put('shape', '44');
        ApexPages.currentPage().getParameters().put('ceiling', '96');
        ApexPages.currentPage().getParameters().put('veranda', '68');
        ApexPages.currentPage().getParameters().put('pool', '71');
        ApexPages.currentPage().getParameters().put('wintergarden', '76');
        ApexPages.currentPage().getParameters().put('terrace', '69');
        ApexPages.currentPage().getParameters().put('oriel', '64');
        ApexPages.currentPage().getParameters().put('mezzanine', '94');
        ApexPages.currentPage().getParameters().put('entrance', '45');
        ApexPages.currentPage().getParameters().put('border', '118');
        ApexPages.currentPage().getParameters().put('carport', '80');
        ApexPages.currentPage().getParameters().put('cost', '102');
        ApexPages.currentPage().getParameters().put('gfroom', '119');
        ApexPages.currentPage().getParameters().put('bullseye', '122');
        ApexPages.currentPage().getParameters().put('fireplace', '123');
        ApexPages.currentPage().getParameters().put('scarp', '121');
        ApexPages.currentPage().getParameters().put('eaves', '124');
        
        DesignsListExtension ext2 = new DesignsListExtension(scp);
        ext2.getDesignResults();
        ext2.getCountDesignResults();
        
        System.debug(ext2.count_basement_46);
        System.debug(ext2.count_basement_72);
        System.debug(ext2.count_basement_114);
        System.debug(ext2.count_border_118);
        System.debug(ext2.count_bullseye_122);
        System.debug(ext2.count_carport_80);
        System.debug(ext2.count_ceiling_95);
        System.debug(ext2.count_ceiling_96);
        System.debug(ext2.count_ceiling_97);
        System.debug(ext2.count_ceiling_99);
        System.debug(ext2.count_construction_38);
        System.debug(ext2.count_construction_39);
        System.debug(ext2.count_construction_40);
        System.debug(ext2.count_construction_41);
        System.debug(ext2.count_construction_82);
        System.debug(ext2.count_cost_102);
        System.debug(ext2.count_eaves_124);
        System.debug(ext2.count_eco_83);
        System.debug(ext2.count_eco_84);
        System.debug(ext2.count_entrance_45);
        System.debug(ext2.count_fireplace_123);
        System.debug(ext2.count_garage_111);
        System.debug(ext2.count_garage_50);
        System.debug(ext2.count_garage_79);
        System.debug(ext2.count_garage_52);
        System.debug(ext2.count_garage_65);
        System.debug(ext2.count_garagelocation_66);
        System.debug(ext2.count_garagelocation_67);
        System.debug(ext2.count_garagelocation_70);
        System.debug(ext2.count_garagelocation_73);
        System.debug(ext2.count_garagelocation_78);
        System.debug(ext2.count_gfroom_119);
        System.debug(ext2.count_level_47);
        System.debug(ext2.count_level_48);
        System.debug(ext2.count_level_63);
        System.debug(ext2.count_livingtype_42);
        System.debug(ext2.count_livingtype_43);
        System.debug(ext2.count_livingtype_49);
        System.debug(ext2.count_mezzanine_94);
        System.debug(ext2.count_oriel_64);
        System.debug(ext2.count_pool_71);
        System.debug(ext2.count_ridge_105);
        System.debug(ext2.count_ridge_106);
        System.debug(ext2.count_roof_12);
        System.debug(ext2.count_roof_13);
        System.debug(ext2.count_roof_3);
        System.debug(ext2.count_roof_4);
        System.debug(ext2.count_roof_5);
        System.debug(ext2.count_roof_6);
        System.debug(ext2.count_roof_7);
        System.debug(ext2.count_roof_8);
        System.debug(ext2.count_roof_9);
        System.debug(ext2.count_scarp_121);
        System.debug(ext2.count_shape_44);
        System.debug(ext2.count_shape_56);
        System.debug(ext2.count_shape_57);
        System.debug(ext2.count_shape_61);
        System.debug(ext2.count_style_77);
        System.debug(ext2.count_style_81);
        System.debug(ext2.count_style_90);
        System.debug(ext2.count_style_98);
        System.debug(ext2.count_style_104);
        System.debug(ext2.count_style_92);
        System.debug(ext2.count_style_91);
        System.debug(ext2.count_style_120);
        System.debug(ext2.count_terrace_69);
        System.debug(ext2.count_veranda_68);
        System.debug(ext2.count_wintergarden_76);
        
        SearchBarExtension sbext = new SearchBarExtension();
        sbext.getPageResults();
        sbext.getDesignResults();
        sbext.getQueryText();
       
        
        test.stopTest();
        
    }
    
    static testMethod void myUnitTest2() {
        
        test.startTest();
        
        Supplier__c s = new Supplier__c(Name='Test Supplier ABC', Full_Name__c='Test Supplier', Code__c='ABC');
        insert s;
        
        Design__c d1 = new Design__c(Name='Test Design', Full_Name__c='Test Design', Supplier__c=s.Id, Code__c='ABC1001');
        insert d1;
        
        FeedExtension.createConstruction(d1.Id, 'Paperwork');
        FollowExtension.follow(d1.Id);
        FollowExtension.unfollowBySubjectId(d1.Id);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(d1);
        DesignViewExtension ext = new DesignViewExtension(sc);
        ext.getBaseDesignVersions();
        ext.getDesignVersions();
        ext.getSimiliarDesigns();
        ext.getPlans();
        ext.getImages();
        ext.getGalleryContentVersionThumbId();
        ext.getCountContentPosts();
        ext.getCountPosts();
        ext.getCountComments();
        ext.getFollowingPlots();
        ext.getFollowingPlotsWithModel();
        ext.getCountConstructionOwners();
        ext.follow();
        ext.getMySubscriptionReference();
        ext.getFollowerPage();
        ext.unfollow();
        ext.getReviewer1();
        ext.getReviewer2();
        ext.getReviewer3();
        ext.getReviewer4();
        ext.getReviewer5();
        ext.getReviewer6();
        ext.getTopContentPosts();
        ext.getContentPosts();
        ext.getFeaturedContentPosts();
        System.assertEquals(ext.getConstructionOwners().size(),1);
        ext.getConstructionOwnersMore();
        System.assertEquals(ext.getCountMyConstructions(),1);
        System.assertEquals(ext.getCountMyConstructionsForDesign(),1);
        ext.updateDesignCommunityDetails();
        ext.getPlotsForMyConstructions();
        ext.getPlotsWithModelForMyConstructions();
        Model__c model1 = DesignViewExtension.saveModel(null, d1.Id, null, false, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
        System.assertEquals(model1.Design__c, d1.Id);
        System.assertNotEquals(model1.Id, null);
        Model__c model2 = DesignViewExtension.saveModel(model1.Id, d1.Id, null, false, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
        System.assertEquals(model1.Id, model2.Id);
        Model__c model3 = DesignViewExtension.saveNamedModel(model1.Id, d1.Id, null, 'example model', false, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
        System.assertEquals(model3.Name, 'example model');
        DesignViewExtension.renameModel(model3.Id, 'example model 2');
        DesignViewExtension.updateModelWithThumbnail(model3.Id, 'data:image/png;base64,sampledata');
        DesignViewExtension.deleteModel(model3.Id);
        ext.getModelReadyDesigns();
        ext.getMyDefaultModel();
        ext.getMyModels();
        ext.getMyAllModels();
        ext.getMyDesignAllModels();
        FeedExtension.createReview(d1.Id,5,'testowa recenzja');
        ext.getAddons();
        
        FeedItem fi = new FeedItem(ParentId=UserInfo.getUserId(), Body='test');
        insert fi;
        
        sc = new ApexPages.StandardController(fi);
        FeedItemExtension fiext = new FeedItemExtension(sc);
        fiext.parentId = d1.Id;
        fiext.reparent();
        
        test.stopTest();
        
    }
}