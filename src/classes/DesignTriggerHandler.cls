public without sharing class DesignTriggerHandler extends TriggerHandler {
    public static Boolean BYPASS_INVALIDATE_DESIGNS;
    public override void beforeInsert() {
        List<Design__c> designs = (List<Design__c>) Trigger.new;
        setCategories(designs);
        normalizeTextFields(designs);
    }

    public override void beforeUpdate() {
        Map<Id, Design__c> designsMap = (Map<Id, Design__c>) Trigger.newMap;
        setCategories(designsMap.values());
        countPosts(designsMap);
        countConstructionOwners(designsMap);
        normalizeTextFields(designsMap.values());
        countMediaRooms(designsMap);
    }

    public override void beforeDelete() {
        List<Design__c> designs = (List<Design__c>) Trigger.old;
        verifyDesignDeletion(designs);
    }

    public override void afterInsert() {
        Map<Id, Design__c> designMap = (Map<Id, Design__c>) Trigger.newMap;
        updateRelatedDesigns(designMap.values(), null);
        updateRelatedMedias(designMap.keySet());
    }

    public override void afterUpdate() {
        Map<Id, Design__c> designMap = (Map<Id, Design__c>) Trigger.newMap;
        Map<Id, Design__c> oldDesignsMap = (Map<Id, Design__c>) Trigger.oldMap;
        updateRelatedDesigns(designMap.values(), oldDesignsMap);
        updateRelatedMedias(designMap.keySet());
        updateRelatedProductPrices(designMap, oldDesignsMap);
        if (BYPASS_INVALIDATE_DESIGNS == null || BYPASS_INVALIDATE_DESIGNS == false) {
            invalidateDesigns(designMap.values(), oldDesignsMap);
        }
    }

    private void verifyDesignDeletion(List<Design__c> oldDesigns) {
        for (Design__c d : oldDesigns) {
            if (String.isNotBlank(d.Code__c)) {
                d.addError(ConstUtils.DESIGN_DELETE_ERROR);
            }
        }
    }

    private void countPosts(Map<Id, Design__c> designsMap) {
        try {
            Map<Id, Integer> countContentPosts = new Map<Id, Integer>();
            for (AggregateResult ar : [
                    SELECT ParentId, count(Id)contentPosts
                    FROM Design__Feed
                    WHERE ParentId IN :designsMap.keySet() AND Type = 'ContentPost' AND Visibility = 'AllUsers'
                    GROUP BY ParentId
            ]) {
                countContentPosts.put((Id) ar.get('ParentId'), (Integer) ar.get('contentPosts'));
            }
            for (Design__c d: designsMap.values()) {
                d.Community_Images__c = countContentPosts.containsKey(d.Id) ? countContentPosts.get(d.Id) : 0;
            }
        } catch (Exception e) {

        }
    }
    
    private void countMediaRooms(Map<Id, Design__c> designs) {
        List<Design__c> ds = [SELECT Id, Pantry__c, Wardrobe__c, (SELECT Id, Design__c, R1__c, R2__c, R3__c, R4__c, R5__c, R6__c, R7__c, R8__c, R9__c, R10__c, R11__c, R12__c, R13__c, R14__c, R15__c, R16__c, R17__c, R18__c, R19__c, R20__c, R21__c, R22__c, R23__c, R24__c, R25__c, R26__c, R27__c, R28__c, R29__c, R30__c, R31__c, R32__c, R33__c, R34__c, R35__c, R36__c, R37__c, R38__c, R39__c, R40__c, R41__c, R42__c, R43__c, R44__c, R45__c, R46__c, R47__c, R48__c, R49__c, R50__c FROM Media__r) mediaList FROM Design__c WHERE id IN :designs.keySet()];
        
        for (Design__c d : ds) {
            Integer pantry = 0, wardrobe = 0;           
            for (Media__c m : d.Media__r) {
                for (String fieldName : m.getPopulatedFieldsAsMap().keySet()) {                          
                    if(string.valueOf(m.get(fieldName)).containsIgnoreCase('spiżarnia')) {                             
                        pantry += 1;
                    }
                    if(string.valueOf(m.get(fieldName)).containsIgnoreCase('garderoba')) {                             
                        wardrobe += 1;
                    }
                }
            }          
            designs.get(d.Id).Pantry__c = pantry;
            designs.get(d.Id).Wardrobe__c = wardrobe;
        }
    }

    private void countConstructionOwners(Map<Id, Design__c> designsMap) {
        try {
            Map<Id, Integer> countConstructionOwners = new Map<Id, Integer>();
            for (AggregateResult ar : [
                    SELECT Design__c, count(Id)constructionOwners
                    FROM Construction__c
                    WHERE Design__c IN :designsMap.keySet()
                    GROUP BY Design__c
            ]) {
                countConstructionOwners.put((Id) ar.get('Design__c'), (Integer) ar.get('constructionOwners'));
            }
            for (Design__c d: designsMap.values()) {
                d.Community_Builders__c = countConstructionOwners.containsKey(d.Id) ? countConstructionOwners.get(d.Id) : 0;
            }
        } catch (Exception e) {

        }
    }

    private void updateRelatedDesigns(List<Design__c> designs, Map<Id, Design__c> oldDesignsMap) {
        Set<ID> baseDesignIdsToUpdate = new Set<ID>();
        for (Design__c d: designs) {
            ID oldBaseDesignId = oldDesignsMap != null ? oldDesignsMap.get(d.Id).Base_Design__c : null;
            ID newBaseDesignId = d.Base_Design__c;
            if (oldBaseDesignId != newBaseDesignId) {
                if (oldBaseDesignId != null) {
                    baseDesignIdsToUpdate.add(oldBaseDesignId);
                }
                if (newBaseDesignId != null) {
                    baseDesignIdsToUpdate.add(newBaseDesignId);
                }
            }
        }

        if (baseDesignIdsToUpdate.size() > 0) {
            List<Design__c> designsToUpdate = [SELECT Id FROM Design__c WHERE Id IN :baseDesignIdsToUpdate OR Base_Design__c IN :baseDesignIdsToUpdate];
            for (Design__c d: designsToUpdate) {
                d.Update_Date__c = System.now();
            }
            if (!designsToUpdate.isEmpty()) {
                update designsToUpdate;
            }
        }
    }

    private void invalidateDesigns(List<Design__c> designs, Map<Id, Design__c> oldDesignsMap) {
        List<Id> designToInvalidate = new List<Id>();

        for (Design__c d: designs) {
            if (d.Update_Date__c != oldDesignsMap.get(d.Id).Update_Date__c) {
                designToInvalidate.add(d.Id);
            }
        }

        if (designToInvalidate.size() > 0 && !MediaExtension.pdfs && !Test.isRunningTest()) {
            DesignExtension.invalidateDesigns(designToInvalidate);
        }
    }

    private void updateRelatedMedias(Set<Id> designIDs) {
        // This queries all Media related to the incoming Design records in a single SOQL query.
        List<Design__c> designsWithMedia = [
                SELECT id, Design_Name_ID__c, (SELECT id, Media_Name_ID__c, Media_Name_ID_Postfix__c from Media__r)
                FROM Design__c WHERE Id IN :designIDs
        ];

        List<Media__c> mediaToUpdate = new List<Media__c>{};
        // For loop to iterate through all the queried Design records
        for (Design__c d: designsWithMedia) {
            // Use the child relationships dot syntax to access the related Media
            for (Media__c m: d.Media__r) {
                if(m.Media_Name_ID__c != d.Design_Name_ID__c + ': ' + m.Media_Name_ID_Postfix__c) {
                    m.Media_Name_ID__c = d.Design_Name_ID__c + ': ' + m.Media_Name_ID_Postfix__c;
                    mediaToUpdate.add(m);
                }
            }

        }

        //Now outside the FOR Loop, perform a single Update DML statement.
        update mediaToUpdate;
    }

    private void updateRelatedProductPrices(Map<Id, Design__c> designs, Map<Id, Design__c> oldDesigns) {
        Set<Id> changedDesigns = new Set<Id> ();
        for (Design__c design : designs.values()) {
            Design__c oldDesign = oldDesigns.containsKey(design.Id) ? oldDesigns.get(design.Id) : null;
            if (oldDesign == null || design.Status__c != oldDesign.Status__c || design.Gross_Price__c != oldDesign.Gross_Price__c
                    || design.Promo_Price__c != oldDesign.Promo_Price__c) {
                changedDesigns.add(design.Id);
            }
        }

        if (changedDesigns.size() > 0) {
            List<PricebookEntry> pricebookEntries = [
                    SELECT Id, IsActive, UnitPrice, Regular_Price__c, Product2.Design__c, Pricebook2.IsStandard
                    FROM PricebookEntry
                    WHERE Product2.Design__c in :changedDesigns AND Product2.Family = :ConstUtils.PRODUCT_DESIGN_FAMILY
            ];
            for (PricebookEntry pe : pricebookEntries) {
                if (pe.Pricebook2.IsStandard || Test.isRunningTest()) {
                    Design__c design = designs.get(pe.Product2.Design__c);
                    pe.Regular_Price__c = design.Gross_Price__c != null ? design.Gross_Price__c : 0;
                    pe.UnitPrice = design.Promo_Price__c != null ? design.Promo_Price__c : pe.Regular_Price__c;
                    pe.IsActive = pe.UnitPrice > 0 &&
                            (design.Status__c == ConstUtils.DESIGN_ACTIVE_STATUS || design.Status__c == ConstUtils.DESIGN_VERSION_STATUS);
                }
            }
            update pricebookEntries;
        }
    }
    
    private void normalizeTextFields(List<Design__c> designs) {
        for (Design__c d : designs) {
            d.Full_Name__c = d.Full_Name__c.normalizeSpace();
            if (String.isNotBlank(d.Full_Name_inc__c)) {
                d.Full_Name_inc__c = d.Full_Name_inc__c.normalizeSpace();
            }
            
            if (String.isNotBlank(d.Design_Availability__c)) { 
                d.Design_Availability__c = d.Design_Availability__c.normalizeSpace();
            }
            
            if (String.isNotBlank(d.Design_Availability_inc__c)) { 
                d.Design_Availability_inc__c = d.Design_Availability_inc__c.normalizeSpace();
            }
        }
    }

    private void setCategories(List<Design__c> designs) {
        for (Design__c d : designs) {
            if (String.isNotBlank(d.Category_Keys__c)) {
                if (d.Category_Keys__c.contains('[46]')) { d.basement_46__c = 1; } else { d.basement_46__c = 0; }
                if (d.Category_Keys__c.contains('[72]')) { d.basement_72__c = 1; } else { d.basement_72__c = 0; }
                if (d.Category_Keys__c.contains('[114]')) { d.basement_114__c = 1; } else { d.basement_114__c = 0; }
                if (d.Border_Ready__c=='tak') { d.border_118__c = 1; } else { d.border_118__c = 0; }
                if (d.Bullseye__c=='tak') { d.bullseye_122__c = 1; } else { d.bullseye_122__c = 0; }
                if (d.Category_Keys__c.contains('[80]')) { d.carport_80__c = 1; } else { d.carport_80__c = 0; }
                if (d.Category_Keys__c.contains('[95]')) { d.ceiling_95__c = 1; } else { d.ceiling_95__c = 0; }
                if (d.Category_Keys__c.contains('[96]')) { d.ceiling_96__c = 1; } else { d.ceiling_96__c = 0; }
                if (d.Category_Keys__c.contains('[97]')) { d.ceiling_97__c = 1; } else { d.ceiling_97__c = 0; }
                if (d.Category_Keys__c.contains('[99]')) { d.ceiling_99__c = 1; } else { d.ceiling_99__c = 0; }
                if (d.Category_Keys__c.contains('[38]')) { d.construction_38__c = 1; } else { d.construction_38__c = 0; }
                if (d.Category_Keys__c.contains('[39]')) { d.construction_39__c = 1; } else { d.construction_39__c = 0; }
                if (d.Category_Keys__c.contains('[40]')) { d.construction_40__c = 1; } else { d.construction_40__c = 0; }
                if (d.Category_Keys__c.contains('[41]')) { d.construction_41__c = 1; } else { d.construction_41__c = 0; }
                if (d.Category_Keys__c.contains('[82]')) { d.construction_82__c = 1; } else { d.construction_82__c = 0; }
                if (d.Category_Keys__c.contains('[125]')) { d.construction_125__c = 1; } else { d.construction_125__c = 0; }
                if (d.Ridge__c=='równoległa do drogi') { d.ridge_105__c = 1; } else { d.ridge_105__c = 0; }
                if (d.Ridge__c=='prostopadła do drogi') { d.ridge_106__c = 1; } else { d.ridge_106__c = 0; }
                if (d.Category_Keys__c.contains('[102]')) { d.cost_102__c = 1; } else { d.cost_102__c = 0; }
                if (d.Eaves__c=='nie') { d.eaves_124__c = 1; } else { d.eaves_124__c = 0; }
                if (d.Category_Keys__c.contains('[83]')) { d.eco_83__c = 1; } else { d.eco_83__c = 0; }
                if (d.Category_Keys__c.contains('[84]')) { d.eco_84__c = 1; } else { d.eco_84__c = 0; }
                if (d.Category_Keys__c.contains('[45]')) { d.entrance_45__c = 1; } else { d.entrance_45__c = 0; }
                if (d.Fireplace__c=='tak') { d.fireplace_123__c = 1; } else { d.fireplace_123__c = 0; }
                if (d.Category_Keys__c.contains('[111]')) { d.garage_111__c = 1; } else { d.garage_111__c = 0; }
                if (d.Category_Keys__c.contains('[50]')) { d.garage_50__c = 1; } else { d.garage_50__c = 0; }
                if (d.Category_Keys__c.contains('[79]')) { d.garage_79__c = 1; } else { d.garage_79__c = 0; }
                if (d.Category_Keys__c.contains('[52]')) { d.garage_52__c = 1; } else { d.garage_52__c = 0; }
                if (d.Category_Keys__c.contains('[65]')) { d.garage_65__c = 1; } else { d.garage_65__c = 0; }
                if (d.Category_Keys__c.contains('[66]')) { d.garagelocation_66__c = 1; } else { d.garagelocation_66__c = 0; }
                if (d.Category_Keys__c.contains('[67]')) { d.garagelocation_67__c = 1; } else { d.garagelocation_67__c = 0; }
                if (d.Category_Keys__c.contains('[70]')) { d.garagelocation_70__c = 1; } else { d.garagelocation_70__c = 0; }
                if (d.Category_Keys__c.contains('[73]')) { d.garagelocation_73__c = 1; } else { d.garagelocation_73__c = 0; }
                if (d.Category_Keys__c.contains('[78]')) { d.garagelocation_78__c = 1; } else { d.garagelocation_78__c = 0; }
                if (d.Garage_Location__c == 'garaż podziemny') { d.garagelocation_126__c = 1; } else { d.garagelocation_126__c = 0; }
                if (d.Category_Keys__c.contains('[119]')) { d.gfroom_119__c = 1; } else { d.gfroom_119__c = 0; }
                if (d.Category_Keys__c.contains('[47]')) { d.level_47__c = 1; } else { d.level_47__c = 0; }
                if (d.Category_Keys__c.contains('[48]')) { d.level_48__c = 1; } else { d.level_48__c = 0; }
                if (d.Category_Keys__c.contains('[63]')) { d.level_63__c = 1; } else { d.level_63__c = 0; }
                if (d.Category_Keys__c.contains('[127]')) { d.level_127__c = 1; } else { d.level_127__c = 0; }
                if (d.Category_Keys__c.contains('[42]')) { d.livingtype_42__c = 1; } else { d.livingtype_42__c = 0; }
                if (d.Category_Keys__c.contains('[43]')) { d.livingtype_43__c = 1; } else { d.livingtype_43__c = 0; }
                if (d.Category_Keys__c.contains('[49]')) { d.livingtype_49__c = 1; } else { d.livingtype_49__c = 0; }
                if (d.Category_Keys__c.contains('[113]')) { d.livingtype_113__c = 1; } else { d.livingtype_113__c = 0; }
                if (d.Category_Keys__c.contains('[94]')) { d.mezzanine_94__c = 1; } else { d.mezzanine_94__c = 0; }
                if (d.Category_Keys__c.contains('[64]')) { d.oriel_64__c = 1; } else { d.oriel_64__c = 0; }
                if (d.Category_Keys__c.contains('[71]')) { d.pool_71__c = 1; } else { d.pool_71__c = 0; }
                if (d.Category_Keys__c.contains('[12]')) { d.roof_12__c = 1; } else { d.roof_12__c = 0; }
                if (d.Category_Keys__c.contains('[13]')) { d.roof_13__c = 1; } else { d.roof_13__c = 0; }
                if (d.Category_Keys__c.contains('[3]')) { d.roof_3__c = 1; } else { d.roof_3__c = 0; }
                if (d.Category_Keys__c.contains('[4]')) { d.roof_4__c = 1; } else { d.roof_4__c = 0; }
                if (d.Category_Keys__c.contains('[5]')) { d.roof_5__c = 1; } else { d.roof_5__c = 0; }
                if (d.Category_Keys__c.contains('[6]')) { d.roof_6__c = 1; } else { d.roof_6__c = 0; }
                if (d.Category_Keys__c.contains('[7]')) { d.roof_7__c = 1; } else { d.roof_7__c = 0; }
                if (d.Category_Keys__c.contains('[8]')) { d.roof_8__c = 1; } else { d.roof_8__c = 0; }
                if (d.Category_Keys__c.contains('[9]')) { d.roof_9__c = 1; } else { d.roof_9__c = 0; }
                if (d.Scarp__c=='tak') { d.scarp_121__c = 1; } else { d.scarp_121__c = 0; }
                if (d.Category_Keys__c.contains('[44]')) { d.shape_44__c = 1; } else { d.shape_44__c = 0; }
                if (d.Category_Keys__c.contains('[56]')) { d.shape_56__c = 1; } else { d.shape_56__c = 0; }
                if (d.Category_Keys__c.contains('[57]')) { d.shape_57__c = 1; } else { d.shape_57__c = 0; }
                if (d.Category_Keys__c.contains('[77]')) { d.style_77__c = 1; } else { d.style_77__c = 0; }
                if (d.Category_Keys__c.contains('[81]')) { d.style_81__c = 1; } else { d.style_81__c = 0; }
                if (d.Category_Keys__c.contains('[90]')) { d.style_90__c = 1; } else { d.style_90__c = 0; }
                if (d.Category_Keys__c.contains('[98]')) { d.style_98__c = 1; } else { d.style_98__c = 0; }
                if (d.Category_Keys__c.contains('[104]')) { d.style_104__c = 1; } else { d.style_104__c = 0; }
                if (d.Category_Keys__c.contains('[92]')) { d.style_92__c = 1; } else { d.style_92__c = 0; }
                if (d.Category_Keys__c.contains('[91]')) { d.style_91__c = 1; } else { d.style_91__c = 0; }
                if (d.Category_Keys__c.contains('[120]')) { d.style_120__c = 1; } else { d.style_120__c = 0; }
                if (d.Category_Keys__c.contains('[69]')) { d.terrace_69__c = 1; } else { d.terrace_69__c = 0; }
                if (d.Category_Keys__c.contains('[1]')) { d.type_1__c = 1; } else { d.type_1__c = 0; }
                if (d.Category_Keys__c.contains('[2]')) { d.type_2__c = 1; } else { d.type_2__c = 0; }
                if (d.Category_Keys__c.contains('[10]')) { d.type_10__c = 1; } else { d.type_10__c = 0; }
                if (d.Category_Keys__c.contains('[11]')) { d.type_11__c = 1; } else { d.type_11__c = 0; }
                if (d.Category_Keys__c.contains('[14]')) { d.type_14__c = 1; } else { d.type_14__c = 0; }
                if (d.Category_Keys__c.contains('[15]')) { d.type_15__c = 1; } else { d.type_15__c = 0; }
                if (d.Category_Keys__c.contains('[16]')) { d.type_16__c = 1; } else { d.type_16__c = 0; }
                if (d.Category_Keys__c.contains('[17]')) { d.type_17__c = 1; } else { d.type_17__c = 0; }
                if (d.Category_Keys__c.contains('[18]')) { d.type_18__c = 1; } else { d.type_18__c = 0; }
                if (d.Category_Keys__c.contains('[19]')) { d.type_19__c = 1; } else { d.type_19__c = 0; }
                if (d.Category_Keys__c.contains('[20]')) { d.type_20__c = 1; } else { d.type_20__c = 0; }
                if (d.Category_Keys__c.contains('[21]')) { d.type_21__c = 1; } else { d.type_21__c = 0; }
                if (d.Category_Keys__c.contains('[22]')) { d.type_22__c = 1; } else { d.type_22__c = 0; }
                if (d.Category_Keys__c.contains('[23]')) { d.type_23__c = 1; } else { d.type_23__c = 0; }
                if (d.Category_Keys__c.contains('[24]')) { d.type_24__c = 1; } else { d.type_24__c = 0; }
                if (d.Category_Keys__c.contains('[25]')) { d.type_25__c = 1; } else { d.type_25__c = 0; }
                if (d.Category_Keys__c.contains('[26]')) { d.type_26__c = 1; } else { d.type_26__c = 0; }
                if (d.Category_Keys__c.contains('[27]')) { d.type_27__c = 1; } else { d.type_27__c = 0; }
                if (d.Category_Keys__c.contains('[28]')) { d.type_28__c = 1; } else { d.type_28__c = 0; }
                if (d.Category_Keys__c.contains('[29]')) { d.type_29__c = 1; } else { d.type_29__c = 0; }
                if (d.Category_Keys__c.contains('[30]')) { d.type_30__c = 1; } else { d.type_30__c = 0; }
                if (d.Category_Keys__c.contains('[31]')) { d.type_31__c = 1; } else { d.type_31__c = 0; }
                if (d.Category_Keys__c.contains('[32]')) { d.type_32__c = 1; } else { d.type_32__c = 0; }
                if (d.Category_Keys__c.contains('[33]')) { d.type_33__c = 1; } else { d.type_33__c = 0; }
                if (d.Category_Keys__c.contains('[34]')) { d.type_34__c = 1; } else { d.type_34__c = 0; }
                if (d.Category_Keys__c.contains('[36]')) { d.type_36__c = 1; } else { d.type_36__c = 0; }
                if (d.Category_Keys__c.contains('[37]')) { d.type_37__c = 1; } else { d.type_37__c = 0; }
                if (d.Category_Keys__c.contains('[53]')) { d.type_53__c = 1; } else { d.type_53__c = 0; }
                if (d.Category_Keys__c.contains('[54]')) { d.type_54__c = 1; } else { d.type_54__c = 0; }
                if (d.Category_Keys__c.contains('[58]')) { d.type_58__c = 1; } else { d.type_58__c = 0; }
                if (d.Category_Keys__c.contains('[75]')) { d.type_75__c = 1; } else { d.type_75__c = 0; }
                if (d.Category_Keys__c.contains('[85]')) { d.type_85__c = 1; } else { d.type_85__c = 0; }
                if (d.Category_Keys__c.contains('[86]')) { d.type_86__c = 1; } else { d.type_86__c = 0; }
                if (d.Category_Keys__c.contains('[87]')) { d.type_87__c = 1; } else { d.type_87__c = 0; }
                if (d.Category_Keys__c.contains('[88]')) { d.type_88__c = 1; } else { d.type_88__c = 0; }
                if (d.Category_Keys__c.contains('[89]')) { d.type_89__c = 1; } else { d.type_89__c = 0; }
                if (d.Category_Keys__c.contains('[93]')) { d.type_93__c = 1; } else { d.type_93__c = 0; }
                if (d.Category_Keys__c.contains('[100]')) { d.type_100__c = 1; } else { d.type_100__c = 0; }
                if (d.Category_Keys__c.contains('[101]')) { d.type_101__c = 1; } else { d.type_101__c = 0; }
                if (d.Category_Keys__c.contains('[103]')) { d.type_103__c = 1; } else { d.type_103__c = 0; }
                if (d.Category_Keys__c.contains('[108]')) { d.type_108__c = 1; } else { d.type_108__c = 0; }
                if (d.Category_Keys__c.contains('[109]')) { d.type_109__c = 1; } else { d.type_109__c = 0; }
                if (d.Category_Keys__c.contains('[110]')) { d.type_110__c = 1; } else { d.type_110__c = 0; }
                if (d.Category_Keys__c.contains('[116]')) { d.type_116__c = 1; } else { d.type_116__c = 0; }
                if (d.Category_Keys__c.contains('[117]')) { d.type_117__c = 1; } else { d.type_117__c = 0; }
                if (d.Category_Keys__c.contains('[68]')) { d.veranda_68__c = 1; } else { d.veranda_68__c = 0; }
                if (d.Category_Keys__c.contains('[76]')) { d.wintergarden_76__c = 1; } else { d.wintergarden_76__c = 0; }
            }
        }
    }
}