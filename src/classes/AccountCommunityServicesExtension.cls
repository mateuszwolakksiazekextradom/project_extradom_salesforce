public without sharing class AccountCommunityServicesExtension {
  
  public String tag{get; set;}
  public String lostReason{get; set;}
  public String comments{get; set;}
  private final Account acc;
  public Id partnerAccId{get; set;}
  public String firstName{get; set;}
  public String lastName{get; set;}
  public String phone{get; set;}
  public String email{get; set;}
  public Boolean nonHouse{get; set;}
  public List<Community_Member__c> members;
  public List<Telemarketing__c> tels;

    //test 223
  
    public AccountCommunityServicesExtension(ApexPages.StandardController stdController) {
        this.acc = (Account)stdController.getRecord();
    }
    
    public List<Community_Member__c> getClosestArchitectByPlotAdaptationMembers() {
        if (String.isBlank(this.tag)) {
            return new List<Community_Member__c>{};
        }
        Community_Tag__c ctag = [SELECT Id, Name, Distance__c FROM Community_Tag__c WHERE Name = :tag];
        Account a = [SELECT Name, Investment_Location__c, Investment_Location__r.Parent_Location__c, Investment_Location__r.Parent_Location__r.Parent_Location__c, Investment_Location__r.Parent_Location__r.Parent_Location__r.Parent_Location__c, Investment_Location__r.Parent_Location__r.Parent_Location__r.Parent_Location__r.Parent_Location__c, Investment_Location__r.Location__longitude__s, Investment_Location__r.Location__latitude__s FROM Account WHERE Id = :acc.Id];
        List<Community_Member_Detail__c> cmds_by_valid_tag;
        if (this.tag == 'adaptacje projektów') {
            cmds_by_valid_tag = [SELECT Community_Member__c FROM Community_Member_Detail__c WHERE RecordType.Name = 'Community Tag' AND Community_Tag__c = :ctag.Id AND Is_Architect__c = true AND Community_Member__r.Business_Location__latitude__s != null AND Community_Member__r.Business_Location__longitude__s != null];
        } else {
            cmds_by_valid_tag = [SELECT Community_Member__c FROM Community_Member_Detail__c WHERE RecordType.Name = 'Community Tag' AND Community_Tag__c = :ctag.Id AND Is_Telemarketing_Partner__c = true AND Community_Member__r.Business_Location__latitude__s != null AND Community_Member__r.Business_Location__longitude__s != null];
        }
        Set<Id> ids_cm_by_valid_tag = new Set<Id>{};
        for (Community_Member_Detail__c cmd : cmds_by_valid_tag) {
            ids_cm_by_valid_tag.add(cmd.Community_Member__c);
        }
        List<Community_Member_Detail__c> cmds_by_valid_main_location = [SELECT Community_Member__c FROM Community_Member_Detail__c WHERE Main__c = true AND Is_Architect__c = true AND Community_Member__r.Business_Location__latitude__s != null AND Community_Member__r.Business_Location__longitude__s != null AND RecordType.Name = 'Business Activity Location' AND (Location__c = :a.Investment_Location__c OR Location__c = :a.Investment_Location__r.Parent_Location__c OR Location__c = :a.Investment_Location__r.Parent_Location__r.Parent_Location__c OR Location__c = :a.Investment_Location__r.Parent_Location__r.Parent_Location__r.Parent_Location__c OR Location__c = :a.Investment_Location__r.Parent_Location__r.Parent_Location__r.Parent_Location__r.Parent_Location__c)];
        List<Community_Member_Detail__c> cmds_by_valid_location = [SELECT Community_Member__c FROM Community_Member_Detail__c WHERE Is_Architect__c = true AND Community_Member__r.Business_Location__latitude__s != null AND Community_Member__r.Business_Location__longitude__s != null AND RecordType.Name = 'Business Activity Location' AND (Location__c = :a.Investment_Location__c OR Location__c = :a.Investment_Location__r.Parent_Location__c OR Location__c = :a.Investment_Location__r.Parent_Location__r.Parent_Location__c OR Location__c = :a.Investment_Location__r.Parent_Location__r.Parent_Location__r.Parent_Location__c OR Location__c = :a.Investment_Location__r.Parent_Location__r.Parent_Location__r.Parent_Location__r.Parent_Location__c)];
        
        Set<Id> ids_cm_by_valid_main_location = new Set<Id>{};
        Set<Id> ids_cm_by_valid_location = new Set<Id>{};
        
        for (Community_Member_Detail__c cmd : cmds_by_valid_main_location) {
            ids_cm_by_valid_main_location.add(cmd.Community_Member__c);
        }
        for (Community_Member_Detail__c cmd : cmds_by_valid_location) {
            ids_cm_by_valid_location.add(cmd.Community_Member__c);
        }
        
        Map<Id,Community_Member__c> members_map = new Map<Id,Community_Member__c>([SELECT Id FROM Community_Member__c WHERE Id IN :ids_cm_by_valid_tag AND Id IN :ids_cm_by_valid_location]);
        Map<Id,Community_Member__c> members_main_map = new Map<Id,Community_Member__c>([SELECT Id FROM Community_Member__c WHERE Id IN :ids_cm_by_valid_tag AND Id IN :ids_cm_by_valid_main_location]);
        
        String query = 'SELECT Id, Partner_Account__c, Business_Location__latitude__s, Business_Location__longitude__s FROM Community_Member__c WHERE Id IN (\''+String.join(new List<Id>(members_main_map.keySet()), '\',\'')+'\') AND DISTANCE(Business_Location__c, GEOLOCATION('+a.Investment_Location__r.Location__latitude__s+', '+a.Investment_Location__r.Location__longitude__s+'), \'km\') < '+ctag.Distance__c+' ORDER BY DISTANCE(Business_Location__c, GEOLOCATION('+a.Investment_Location__r.Location__latitude__s+', '+a.Investment_Location__r.Location__longitude__s+'), \'km\') LIMIT 3';
        System.debug(query);
        this.members = (List<Community_Member__c>)Database.query(query);
        if (members.size() < 3) {
            query = 'SELECT Id, Partner_Account__c, Business_Location__latitude__s, Business_Location__longitude__s FROM Community_Member__c WHERE Id IN (\''+String.join(new List<Id>(members_map.keySet()), '\',\'')+'\') AND Id NOT IN (\''+String.join(new List<Id>(members_main_map.keySet()), '\',\'')+'\') AND DISTANCE(Business_Location__c, GEOLOCATION('+a.Investment_Location__r.Location__latitude__s+', '+a.Investment_Location__r.Location__longitude__s+'), \'km\') < '+ctag.Distance__c+' ORDER BY DISTANCE(Business_Location__c, GEOLOCATION('+a.Investment_Location__r.Location__latitude__s+', '+a.Investment_Location__r.Location__longitude__s+'), \'km\') LIMIT ' + String.valueOf(3 - members.size());
            for (Community_Member__c cm : Database.query(query)) {
                members.add(cm);
            }
        }
        System.debug(this.members);
        return this.members;
    }

    public PageReference createTelemarketingEntries() {
        List<Telemarketing__c> telsToCreate = new List<Telemarketing__c>();
        string telName = 'Przekazanie danych: '+this.tag;
        List<Telemarketing__c> telsToDelete = [SELECT Id FROM Telemarketing__c WHERE Account__r.Id=:this.acc.Id AND Name=:telName AND Status__c='Deferred'];
        if (!telsToDelete.isEmpty()) {
            delete telsToDelete;
        }
        for (Community_Member__c cm: this.members) {
            Telemarketing__c tel = new Telemarketing__c(Name='Przekazanie danych: '+this.tag, Account__c=this.acc.Id, Transfer_Account__c=cm.Partner_Account__c, First_Name__c=this.firstName, Last_Name__c=this.lastName, Phone__c=this.phone, Email__c=this.email, Region__c=this.acc.Investment_Location__c, Status__c='Won', Comments__c=this.comments, Post_Conversion_Stage__c='Waiting', Non_House_Design__c=this.nonHouse);
            telsToCreate.add(tel);
        }
        insert telsToCreate;
        this.tels = telsToCreate;
        return null;
    }
    
    public PageReference createTelemarketingLostEntry() {
        List<Telemarketing__c> telsToCreate = new List<Telemarketing__c>();
        Telemarketing__c tel = new Telemarketing__c(Name='Przekazanie danych: '+this.tag, Account__c=this.acc.Id, Status__c='Lost', Lost_Reason__c=this.lostReason, Non_House_Design__c=this.nonHouse);
        string telName = 'Przekazanie danych: '+this.tag;
        List<Telemarketing__c> telsToDelete = [SELECT Id FROM Telemarketing__c WHERE Account__r.Id=:this.acc.Id AND Name=:telName AND Status__c='Deferred'];
        if (!telsToDelete.isEmpty()) {
            delete telsToDelete;
        }
        telsToCreate.add(tel);
        insert telsToCreate;
        this.tels = telsToCreate;
        return null;
    }
    
    public PageReference createTelemarketingDeferredEntry() {
        List<Telemarketing__c> telsToCreate = new List<Telemarketing__c>();
        Telemarketing__c tel = new Telemarketing__c(Name='Przekazanie danych: '+this.tag, Account__c=this.acc.Id, Status__c='Deferred', Non_House_Design__c=this.nonHouse);
        telsToCreate.add(tel);
        insert telsToCreate;
        this.tels = telsToCreate;
        return null;
    }
    
    public List<Telemarketing__c> getTelResults() {
        return this.tels;
    }
    
    public List<SelectOption> getTelemarketingLostReasonOptions() {
       List<SelectOption> options = new List<SelectOption>();
       for (Schema.PicklistEntry ple : Telemarketing__c.fields.Lost_Reason__c.getDescribe().getpicklistvalues()) {
          options.add(new SelectOption(ple.getLabel(), ple.getValue()));
       }
       return options;
    }
    
    public PageReference createBraasTelemarketing() {
      List<Telemarketing__c> telsToCreate = new List<Telemarketing__c>();
      Map<String,Id> partnerAreas = new Map<String,Id>{};
      Map<String,Id> locationIds = new Map<String,Id>{};
      List<Location__c> regions = [SELECT Name, Parent_Location__r.Name, Braas_Account_Id__c FROM Location__c WHERE RecordType.Name = 'Region' AND Braas_Account_Id__c != null];
      for (Location__c region: regions) {
        partnerAreas.put(region.Parent_Location__r.Name+': '+region.Name, region.Braas_Account_Id__c);
        locationIds.put(region.Parent_Location__r.Name+': '+region.Name, region.Id);
      }
      if (partnerAreas.containsKey(this.acc.Investment_State__c+': '+this.acc.Investment_Region__c)) {
        Telemarketing__c tel = new Telemarketing__c(Name='Umówienie spotkania ws. produktów Braas', Account__c=this.acc.Id, Transfer_Account__c=partnerAreas.get(this.acc.Investment_State__c+': '+this.acc.Investment_Region__c), Region__c=locationIds.get(this.acc.Investment_State__c+': '+this.acc.Investment_Region__c), Status__c='New', RecordTypeId=Schema.SObjectType.Telemarketing__c.RecordTypeInfosByName.get('Braas').RecordTypeId, Non_House_Design__c=this.nonHouse);
        telsToCreate.add(tel);
      }
      insert telsToCreate;
      this.tels = telsToCreate;
      return null;
    }
    
    public PageReference createCustomTelemarketingEntry() {
        List<Telemarketing__c> telsToCreate = new List<Telemarketing__c>();
        Telemarketing__c tel = new Telemarketing__c(Name='Przekazanie danych: '+this.tag, Account__c=this.acc.Id, Transfer_Account__c=this.partnerAccId, First_Name__c=this.firstName, Last_Name__c=this.lastName, Phone__c=this.phone, Email__c=this.email, Region__c=this.acc.Investment_Location__c, Status__c='Won', Comments__c=this.comments, Post_Conversion_Stage__c='Waiting', Non_House_Design__c=this.nonHouse);
        string telName = 'Przekazanie danych: '+this.tag;
        List<Telemarketing__c> telsToDelete = [SELECT Id FROM Telemarketing__c WHERE Account__r.Id=:this.acc.Id AND Name=:telName AND Status__c='Deferred'];
        if (!telsToDelete.isEmpty()) {
            delete telsToDelete;
        }
        telsToCreate.add(tel);
        insert telsToCreate;
        this.tels = telsToCreate;
        return null;
    }
}