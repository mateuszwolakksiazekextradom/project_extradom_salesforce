@IsTest global with sharing class LoginControllerTest {
    @IsTest(SeeAllData=true) 
    global static void testLoginController () {
       LoginController controller = new LoginController();
       System.assertEquals(null, controller.loginInline());
       PageReference pageRef = new PageReference('http://www.extradom.pl/passwordRecoveryInline');
       Test.setCurrentPage(pageRef);
       ApexPages.currentPage().getParameters().put('username', 'testpasswordrecovery');
       pageRef = new PageReference('http://www.extradom.pl/LoginView?startURL=%2Fprojekt-ABC1001');
       Test.setCurrentPage(pageRef);
       controller = new LoginController();
       controller.autoPasswordRecovery();
       controller = new LoginController();
       ApexPages.currentPage().getParameters().put('username', 'testpasswordrecovery@extradom.pl');
       controller.autoPasswordRecovery();
       ApexPages.currentPage().getParameters().put('username', 'testpasswordrecovery@extradom.pl');
       ApexPages.currentPage().getParameters().put('password', 'abc123456');
       controller.getActiveStartUrl();
       controller.getActiveSource();
       controller.getReferer();
       controller.autoLogin();
       String a = controller.recoveryUsername;
    }
}