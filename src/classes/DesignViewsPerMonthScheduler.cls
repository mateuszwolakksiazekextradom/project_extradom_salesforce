/**
 * Created by grzegorz.dlugosz on 15.05.2019.
 */
public with sharing class DesignViewsPerMonthScheduler implements Schedulable{
    public void execute(SchedulableContext sc) {
        // Enqueue Design Views Per Month update on Designs, start from the beggining - 0,
        // This job requests fresh data from Google Analytics and updates Design__c - Views_Month__c with new value
        System.enqueueJob(new DesignViewsPerMonthQueueable('0'));
    }
}