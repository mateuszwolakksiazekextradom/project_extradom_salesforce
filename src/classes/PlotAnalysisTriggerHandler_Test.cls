@isTest
private class PlotAnalysisTriggerHandler_Test {
    @isTest
    private static void shouldAutocreatePlot_whenCreated() {
        // when
        Plot__c plot = new Plot__c();
        plot.Name = 'NR';
        insert plot;
        
        Plot_Analysis__c pla = new Plot_Analysis__c();
        pla.Name = 'Analiza działki';
        pla.Plot__c = plot.Id;
        
        Plot_Analysis__c pla1 = new Plot_Analysis__c();
        pla1.Name = 'Analiza działki';
        pla1.Plot_Address__c = 'Adres testowy';
        pla1.Plot_Name__c = 'Numer tesowy';
        
        Plot_Analysis__c pla2 = new Plot_Analysis__c();
        pla2.Name = 'Analiza działki';
        
        insert new List<Plot_Analysis__c> { pla, pla1, pla2 };
        
        Plot_Analysis__c pla_after = [SELECT Id, Plot__c, Plot__r.Name, Plot__r.Address__c FROM Plot_Analysis__c WHERE Id =: pla.Id];
        Plot_Analysis__c pla1_after = [SELECT Id, Plot__c, Plot__r.Name, Plot__r.Address__c FROM Plot_Analysis__c WHERE Id =: pla1.Id];
        Plot_Analysis__c pla2_after = [SELECT Id, Plot__c, Plot__r.Name, Plot__r.Address__c FROM Plot_Analysis__c WHERE Id =: pla2.Id];

        // then
        System.assertEquals(plot.Id, pla_after.Plot__c);
        System.assertNotEquals(null, pla1_after.Plot__c);
        System.assertNotEquals(null, pla2_after.Plot__c);
        System.assertEquals('Numer tesowy', pla1_after.Plot__r.Name);
        System.assertEquals('(do uzupełnienia)', pla2_after.Plot__r.Name);
        System.assertEquals('Adres testowy', pla1_after.Plot__r.Address__c);
        System.assert(String.isBlank(pla2_after.Plot__r.Address__c));
        
    }

}