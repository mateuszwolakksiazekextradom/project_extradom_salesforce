@RestResource(urlMapping='/app/filter/designsets')
global class AppFilterDesignSets {
    @HttpGet
    global static ExtradomApi.DesignSet doGet() {
    
        Integer total;
    
        String type;
        String mask;
        
        Integer minarea;
        Integer maxarea;
        Integer maxplotsize;
        Integer maxheight;
        Integer maxfootprint;
        Integer minroofslope;
        Integer maxroofslope;
        Integer rooms;
        String level;
        String garage;
        String basement;
        String livingtype;
        String roof;
        String ridge;
        String construction;
        String ceiling;
        String eco;
        String garagelocation;
        String style;
        String shape;
        String veranda;
        String pool;
        String wintergarden;
        String terrace;
        String oriel;
        String mezzanine;
        String entrance;
        String border;
        String carport;
        String cost;
        String gfroom;
        String eaves;
        String scarp;
        String fireplace;
        String bullseye;
        
        String title = 'Projekty domów';
        List<ExtradomApi.FilterReset> filterResets = new List<ExtradomApi.FilterReset>{};
        
        Map<String,String> shortNames = new Map<String,String>{ '46' => 'z piwnicą', '72' => 'bez piwnicy', '114' => 'z sutereną', '118' => 'na granicy działki', '122' => 'z wolim okiem', '80' => 'z wiatą garażową', '95' => 'strop drewniany', '96' => 'strop gęstożebrowy', '97' => 'strop żelbetowy', '99' => 'wiązary', '38' => 'drewniane', '39' => 'z bali', '40' => 'blaszane', '41' => 'szkieletowe', '82' => 'murowane', '125' => 'stalowa', '102' => 'tani w budowie', '124' => 'bez okapu', '83' => 'energooszczędne', '84' => 'pasywne', '45' => 'wejście od południa', '123' => 'z kominkiem', '50' => 'garaż jedno miejsce', '52' => 'garaż dwa miejsca', '65' => 'bez garażu', '79' => 'z garażem', '111' => 'garaż trzy lub więcej miejsc', '66' => 'garaż w bryle budynku', '67' => 'garaż wysunięty do przodu', '70' => 'garaż poza bryłą, w linii wejścia', '73' => 'garaż cofnięty', '78' => 'garaż wolnostojący', '126' => 'garaż podziemny', '119' => 'dodatkowy pokój', '47' => 'parterowe', '48' => 'z poddaszem użytkowym', '127' => 'z poddaszem do adaptacji', '63' => 'piętrowe', '42' => 'zabudowa bliźniacza', '43' => 'zabudowa szeregowa', '49' => 'zabudowa wolnostojąca', '94' => 'z antresolą', '64' => 'z wykuszem', '71' => 'z basenem', '105' => 'kalenica równoległa do drogi', '106' => 'kalenica prostopadła do drogi', '3' => 'dach mansardowy', '4' => 'dach dwuspadowy', '5' => 'dach czterospadowy', '6' => 'dach wielospadowy', '7' => 'dach kopertowy', '8' => 'dach namiotowy', '9' => 'dach naczółkowy', '12' => 'dach płaski', '13' => 'dach jednospadowy', '121' => 'na skarpę', '44' => 'litera L', '56' => 'z atrium', '57' => 'nietypowa', '61' => 'kształt klasyczny', '77' => 'styl tradycyjny', '81' => 'nowoczesne', '90' => 'styl dworkowy', '91' => 'wille', '92' => 'rezydencje', '98' => 'góralski', '104' => 'mur pruski / szachulec', '69' => 'z tarasem', '68' => 'z werandą', '76' => 'z ogrodem zimowym', '35' => 'bez basenu', '51' => 'bez werandy', '55' => 'bez tarasu', '59' => 'salon w głębi', '60' => 'bez antresoli', '62' => 'bez wiaty garażowej', '74' => 'bez wykusza', '107' => 'bez ogrodu zimowego', '112' => 'jednorodzinny', '113' => 'zabudowa dwurodzinna' };
        Map<String,String> maskNames = new Map<String,String>{ 'promo' => 'promocje', 'latest' => 'ostatnio dodane', 'top' => 'najczęściej budowane', '3d' => '3D', 'interior' => 'z wnętrzami' };
        Map<String,String> titleNames = new Map<String,String>{ '1' => 'Projekty domów', '2' => 'Projekty garaży', '10' => 'Projekty dodatkowe' };
        Map<String,String> categoryNames = new Map<String,String>{ '1' => 'dom', '2' => 'garaż', '10' => 'dodatkowy' };
        
        List<String> filters = new List<String>{'Status__c = \'Active\''};
        Map<String,String> canonicalParams = new Map<String,String>{};
        List<ID> internalTopDesignIds = new List<ID>();
        
        // Type
        if (RestContext.request.params.containsKey('type')) {
            type = RestContext.request.params.get('type');
            filters.add('type_'+type+'__c=1');
        } else {
            type = '1';
            filters.add('type_1__c=1');
        }
        canonicalParams.put('type',String.valueOf(type));
        // mask
        if (RestContext.request.params.containsKey('mask')) {
            mask = RestContext.request.params.get('mask');
            canonicalParams.put('mask',RestContext.request.params.get('mask'));
            if (mask == 'latest') {
                filters.add('(Activation_Date__c = LAST_N_DAYS:90 OR Featured_Recently_Added__c > 0)');
            } else if (mask == 'promo') {
                filters.add('Promo_Price__c > 0');
            } else if (mask == 'top') {
                Map<Id,AggregateResult> topDesignsResults = new Map<Id,AggregateResult>([SELECT ParentId Id FROM Design__Feed WHERE Parent.Status__c = 'Active' AND Parent.Featured_Top_Built__c = null AND Parent.Top_Ready__c = true AND CreatedDate = last_n_days:30 AND Type = 'ContentPost' AND Visibility = 'AllUsers' GROUP BY ParentId ORDER BY SUM(LikeCount) DESC LIMIT 31]);
                internalTopDesignIds = new List<Id>(topDesignsResults.keySet());
                filters.add('(Featured_Top_Built__c > 0 OR Id IN :internalTopDesignIds)');
            }
            filterResets.add(new ExtradomApi.FilterReset(maskNames.get(mask),new List<String>{'mask'}));
        }
        // Usable Area
        if (RestContext.request.params.containsKey('minarea')) {
            minarea = Integer.valueOf(RestContext.request.params.get('minarea'));
            filters.add('Usable_Area__c >= '+String.valueOf(minarea));
            canonicalParams.put('minarea',RestContext.request.params.get('minarea'));
        }
        if (RestContext.request.params.containsKey('maxarea')) {
            maxarea = Integer.valueOf(RestContext.request.params.get('maxarea'));
            filters.add('Usable_Area__c <= '+String.valueOf(maxarea));
            canonicalParams.put('maxarea',RestContext.request.params.get('maxarea'));
        }
        if (minarea!=null || maxarea!=null) {
            String label;
            if (minarea!=null && maxarea!=null) {
                if (minarea == maxarea) {
                    label = String.valueOf(minarea)+' m²';
                } else {
                    label = String.valueOf(minarea)+'-'+String.valueOf(maxarea)+' m²';
                }
            } else if (minarea==null && maxarea!=null) {
                label = 'do '+String.valueOf(maxarea)+' m²';
            } else {
                label = String.valueOf(minarea)+'+ m²';
            }
            filterResets.add(new ExtradomApi.FilterReset(label,new List<String>{'minarea','maxarea'}));
        }
        // Plot Size
        if (RestContext.request.params.containsKey('maxplotsize')) {
            maxplotsize = Integer.valueOf(RestContext.request.params.get('maxplotsize'));
            filters.add('Minimum_Plot_Size_Horizontal__c <= '+String.valueOf(maxplotsize));
            canonicalParams.put('maxplotsize',RestContext.request.params.get('maxplotsize'));
            filterResets.add(new ExtradomApi.FilterReset('działka do '+String.valueOf(maxplotsize)+' m',new List<String>{'maxplotsize'}));
        }
        // Height
        if (RestContext.request.params.containsKey('maxheight')) {
            maxheight = Integer.valueOf(RestContext.request.params.get('maxheight'));
            filters.add('Height__c <= '+String.valueOf(maxheight));
            canonicalParams.put('maxheight',RestContext.request.params.get('maxheight'));
            filterResets.add(new ExtradomApi.FilterReset('wysokość do '+String.valueOf(maxheight)+' m',new List<String>{'maxheight'}));
        }
        // Footprint
        if (RestContext.request.params.containsKey('maxfootprint')) {
            maxfootprint = Integer.valueOf(RestContext.request.params.get('maxfootprint'));
            filters.add('Footprint__c <= '+String.valueOf(maxfootprint));
            canonicalParams.put('maxfootprint',RestContext.request.params.get('maxfootprint'));
            filterResets.add(new ExtradomApi.FilterReset('zabudowa do '+String.valueOf(maxfootprint)+' m²',new List<String>{'maxfootprint'}));
        }
        // Roof Slope
        if (RestContext.request.params.containsKey('minroofslope')) {
            minroofslope = Integer.valueOf(RestContext.request.params.get('minroofslope'));
            filters.add('Roof_slope__c >= '+String.valueOf(minroofslope));
            canonicalParams.put('minroofslope',RestContext.request.params.get('minroofslope'));
        }
        if (RestContext.request.params.containsKey('maxroofslope')) {
            maxroofslope = Integer.valueOf(RestContext.request.params.get('maxroofslope'));
            filters.add('Roof_slope__c <= '+String.valueOf(maxroofslope));
            canonicalParams.put('maxroofslope',RestContext.request.params.get('maxroofslope'));
        }
        if (minroofslope!=null || maxroofslope!=null) {
            String label;
            if (minroofslope!=null && maxroofslope!=null) {
                if (minroofslope == maxroofslope) {
                    label = String.valueOf(minroofslope)+'°';
                } else {
                    label = String.valueOf(minroofslope)+'-'+String.valueOf(maxroofslope)+'°';
                }
            } else if (minroofslope==null && maxroofslope!=null) {
                label = 'max. '+String.valueOf(maxroofslope)+'°';
            } else {
                label = 'min. '+String.valueOf(minroofslope)+'°';
            }
            filterResets.add(new ExtradomApi.FilterReset(label,new List<String>{'minroofslope','maxroofslope'}));
        }
        // Count Rooms
        if (RestContext.request.params.containsKey('rooms')) {
            rooms = Integer.valueOf(RestContext.request.params.get('rooms'));
            filters.add('Count_Rooms__c = '+String.valueOf(rooms));
            canonicalParams.put('rooms',RestContext.request.params.get('rooms'));
            String label;
            if (rooms>=10) {
                label = String.valueOf(rooms)+'+ pokoi';
            } else if (rooms>=5) {
                label = String.valueOf(rooms)+' pokoi';
            } else if (rooms>=2) {
                label = String.valueOf(rooms)+' pokoje';
            } else {
                label = String.valueOf(rooms)+' pokój';
            }
            filterResets.add(new ExtradomApi.FilterReset(label,new List<String>{'rooms'}));
        }
        // Level
        if (RestContext.request.params.containsKey('level')) {
            level = RestContext.request.params.get('level');
            filters.add('level_'+String.valueOf(level)+'__c=1');
            canonicalParams.put('level',RestContext.request.params.get('level'));
            filterResets.add(new ExtradomApi.FilterReset(shortNames.get(level),new List<String>{'level'}));
        }
        // Garage
        if (RestContext.request.params.containsKey('garage')) {
            garage = RestContext.request.params.get('garage');
            filters.add('garage_'+String.valueOf(garage)+'__c=1');
            canonicalParams.put('garage',RestContext.request.params.get('garage'));
            filterResets.add(new ExtradomApi.FilterReset(shortNames.get(garage),new List<String>{'garage'}));
        }
        // Roof
        if (RestContext.request.params.containsKey('roof')) {
            roof = RestContext.request.params.get('roof');
            filters.add('roof_'+String.valueOf(roof)+'__c=1');
            canonicalParams.put('roof',RestContext.request.params.get('roof'));
            filterResets.add(new ExtradomApi.FilterReset(shortNames.get(roof),new List<String>{'roof'}));
        }
        // Living Type
        if (RestContext.request.params.containsKey('livingtype')) {
            livingtype = RestContext.request.params.get('livingtype');
            filters.add('livingtype_'+String.valueOf(livingtype)+'__c=1');
            canonicalParams.put('livingtype',RestContext.request.params.get('livingtype'));
            filterResets.add(new ExtradomApi.FilterReset(shortNames.get(livingtype),new List<String>{'livingtype'}));
        }
        // Style
        if (RestContext.request.params.containsKey('style')) {
            style = RestContext.request.params.get('style');
            filters.add('style_'+String.valueOf(style)+'__c=1');
            canonicalParams.put('style',RestContext.request.params.get('style'));
            filterResets.add(new ExtradomApi.FilterReset(shortNames.get(style),new List<String>{'style'}));
        }
        // Construcion
        if (RestContext.request.params.containsKey('construction')) {
            construction = RestContext.request.params.get('construction');
            filters.add('construction_'+String.valueOf(construction)+'__c=1');
            canonicalParams.put('construction',RestContext.request.params.get('construction'));
            filterResets.add(new ExtradomApi.FilterReset(shortNames.get(construction),new List<String>{'construction'}));
        }
        // Eco
        if (RestContext.request.params.containsKey('eco')) {
            eco = RestContext.request.params.get('eco');
            filters.add('eco_'+String.valueOf(eco)+'__c=1');
            canonicalParams.put('eco',RestContext.request.params.get('eco'));
            filterResets.add(new ExtradomApi.FilterReset(shortNames.get(eco),new List<String>{'eco'}));
        }
        // Garage Location
        if (RestContext.request.params.containsKey('garagelocation')) {
            garagelocation = RestContext.request.params.get('garagelocation');
            filters.add('garagelocation_'+String.valueOf(garagelocation)+'__c=1');
            canonicalParams.put('garagelocation',RestContext.request.params.get('garagelocation'));
            filterResets.add(new ExtradomApi.FilterReset(shortNames.get(garagelocation),new List<String>{'garagelocation'}));
        }
        // Basement
        if (RestContext.request.params.containsKey('basement')) {
            basement = RestContext.request.params.get('basement');
            filters.add('basement_'+String.valueOf(basement)+'__c=1');
            canonicalParams.put('basement',RestContext.request.params.get('basement'));
            filterResets.add(new ExtradomApi.FilterReset(shortNames.get(basement),new List<String>{'basement'}));
        }
        // Ridge
        if (RestContext.request.params.containsKey('ridge')) {
            ridge = RestContext.request.params.get('ridge');
            filters.add('ridge_'+String.valueOf(ridge)+'__c=1');
            canonicalParams.put('ridge',RestContext.request.params.get('ridge'));
            filterResets.add(new ExtradomApi.FilterReset(shortNames.get(ridge),new List<String>{'ridge'}));
        }
        // Shape
        if (RestContext.request.params.containsKey('shape')) {
            shape = RestContext.request.params.get('shape');
            filters.add('shape_'+String.valueOf(shape)+'__c=1');
            canonicalParams.put('shape',RestContext.request.params.get('shape'));
            filterResets.add(new ExtradomApi.FilterReset(shortNames.get(shape),new List<String>{'shape'}));
        }
        // Veranda
        if (RestContext.request.params.containsKey('veranda')) {
            veranda = RestContext.request.params.get('veranda');
            filters.add('veranda_'+String.valueOf(veranda)+'__c=1');
            canonicalParams.put('veranda',RestContext.request.params.get('veranda'));
            filterResets.add(new ExtradomApi.FilterReset(shortNames.get(veranda),new List<String>{'veranda'}));
        }
        // Pool
        if (RestContext.request.params.containsKey('pool')) {
            pool = RestContext.request.params.get('pool');
            filters.add('pool_'+String.valueOf(pool)+'__c=1');
            canonicalParams.put('pool',RestContext.request.params.get('pool'));
            filterResets.add(new ExtradomApi.FilterReset(shortNames.get(pool),new List<String>{'pool'}));
        }
        // Winter Garden
        if (RestContext.request.params.containsKey('wintergarden')) {
            wintergarden= RestContext.request.params.get('wintergarden');
            filters.add('wintergarden_'+String.valueOf(wintergarden)+'__c=1');
            canonicalParams.put('wintergarden',RestContext.request.params.get('wintergarden'));
            filterResets.add(new ExtradomApi.FilterReset(shortNames.get(wintergarden),new List<String>{'wintergarden'}));
        }
        // Terrace
        if (RestContext.request.params.containsKey('terrace')) {
            terrace = RestContext.request.params.get('terrace');
            filters.add('terrace_'+String.valueOf(terrace)+'__c=1');
            canonicalParams.put('terrace',RestContext.request.params.get('terrace'));
            filterResets.add(new ExtradomApi.FilterReset(shortNames.get(terrace),new List<String>{'terrace'}));
        }
        // Oriel
        if (RestContext.request.params.containsKey('oriel')) {
            oriel = RestContext.request.params.get('oriel');
            filters.add('oriel_'+String.valueOf(oriel)+'__c=1');
            canonicalParams.put('oriel',RestContext.request.params.get('oriel'));
            filterResets.add(new ExtradomApi.FilterReset(shortNames.get(oriel),new List<String>{'oriel'}));
        }
        // Mezzanine
        if (RestContext.request.params.containsKey('mezzanine')) {
            mezzanine = RestContext.request.params.get('mezzanine');
            filters.add('mezzanine_'+String.valueOf(mezzanine)+'__c=1');
            canonicalParams.put('mezzanine',RestContext.request.params.get('mezzanine'));
            filterResets.add(new ExtradomApi.FilterReset(shortNames.get(mezzanine),new List<String>{'mezzanine'}));
        }
        // Entrance
        if (RestContext.request.params.containsKey('entrance')) {
            entrance = RestContext.request.params.get('entrance');
            filters.add('entrance_'+String.valueOf(entrance)+'__c=1');
            canonicalParams.put('entrance',RestContext.request.params.get('entrance'));
            filterResets.add(new ExtradomApi.FilterReset(shortNames.get(entrance),new List<String>{'entrance'}));
        }
        // Border
        if (RestContext.request.params.containsKey('border')) {
            border = RestContext.request.params.get('border');
            filters.add('border_'+String.valueOf(border)+'__c=1');
            canonicalParams.put('border',RestContext.request.params.get('border'));
            filterResets.add(new ExtradomApi.FilterReset(shortNames.get(border),new List<String>{'border'}));
        }
        // Carport
        if (RestContext.request.params.containsKey('carport')) {
            carport = RestContext.request.params.get('carport');
            filters.add('carport_'+String.valueOf(carport)+'__c=1');
            canonicalParams.put('carport',RestContext.request.params.get('carport'));
            filterResets.add(new ExtradomApi.FilterReset(shortNames.get(carport),new List<String>{'carport'}));
        }
        // Cost
        if (RestContext.request.params.containsKey('cost')) {
            cost = RestContext.request.params.get('cost');
            filters.add('cost_'+String.valueOf(cost)+'__c=1');
            canonicalParams.put('cost',RestContext.request.params.get('cost'));
            filterResets.add(new ExtradomApi.FilterReset(shortNames.get(cost),new List<String>{'cost'}));
        }
        // Ground Floor Room
        if (RestContext.request.params.containsKey('gfroom')) {
            gfroom = RestContext.request.params.get('gfroom');
            filters.add('gfroom_'+String.valueOf(gfroom)+'__c=1');
            canonicalParams.put('gfroom',RestContext.request.params.get('gfroom'));
            filterResets.add(new ExtradomApi.FilterReset(shortNames.get(gfroom),new List<String>{'gfroom'}));
        }
        // Ceiling
        if (RestContext.request.params.containsKey('ceiling')) {
            ceiling = RestContext.request.params.get('ceiling');
            filters.add('ceiling_'+String.valueOf(ceiling)+'__c=1');
            canonicalParams.put('ceiling',RestContext.request.params.get('ceiling'));
            filterResets.add(new ExtradomApi.FilterReset(shortNames.get(ceiling),new List<String>{'ceiling'}));
        }
        // Eaves
        if (RestContext.request.params.containsKey('eaves')) {
            eaves = RestContext.request.params.get('eaves');
            filters.add('eaves_'+String.valueOf(eaves)+'__c=1');
            canonicalParams.put('eaves',RestContext.request.params.get('eaves'));
            filterResets.add(new ExtradomApi.FilterReset(shortNames.get(eaves),new List<String>{'eaves'}));
        }
        // Scarp
        if (RestContext.request.params.containsKey('scarp')) {
            scarp = RestContext.request.params.get('scarp');
            filters.add('scarp_'+String.valueOf(scarp)+'__c=1');
            canonicalParams.put('scarp',RestContext.request.params.get('scarp'));
            filterResets.add(new ExtradomApi.FilterReset(shortNames.get(scarp),new List<String>{'scarp'}));
        }
        // Fireplace
        if (RestContext.request.params.containsKey('fireplace')) {
            fireplace = RestContext.request.params.get('fireplace');
            filters.add('fireplace_'+String.valueOf(fireplace)+'__c=1');
            canonicalParams.put('fireplace',RestContext.request.params.get('fireplace'));
            filterResets.add(new ExtradomApi.FilterReset(shortNames.get(fireplace),new List<String>{'fireplace'}));
        }
        // Bull's Eye
        if (RestContext.request.params.containsKey('bullseye')) {
            bullseye = RestContext.request.params.get('bullseye');
            filters.add('bullseye_'+String.valueOf(bullseye)+'__c=1');
            canonicalParams.put('bullseye',RestContext.request.params.get('bullseye'));
            filterResets.add(new ExtradomApi.FilterReset(shortNames.get(bullseye),new List<String>{'bullseye'}));
        }
        
        String queryCount = 'SELECT Count(Id) FROM Design__c WHERE ' + String.join(filters,' AND ');
        AggregateResult ar = (AggregateResult)Database.query(queryCount);
        total = Integer.valueOf(ar.get('expr0'));
        
        
        
        PageReference firstPage = new PageReference('/app/filter/designs');
        firstPage.getParameters().putAll(canonicalParams);
        
        ExtradomApi.DesignSet designSet;
        designSet = new ExtradomApi.DesignSet();
        designSet.firstPageUrl = firstPage.getUrl();
        designSet.title = titleNames.get(type);
        designSet.label = titleNames.get(type);
        designSet.category = categoryNames.get(type);
        for (ExtradomApi.FilterReset filterReset : filterResets) {
            designSet.label += ' '+filterReset.label;
        }
        designSet.total = total;
        designSet.filterResets = filterResets;
        
        if (UserInfo.getUserType()=='Guest') {
            RestContext.response.addHeader('Cache-Control', 'must-revalidate, max-age=0, s-maxage=21600');
        } else {
            for (Set__c s : [SELECT Id FROM Set__c WHERE OwnerId = :UserInfo.getUserId() AND type__c =: type AND mask__c =: mask AND minarea__c =: minarea AND maxarea__c =: maxarea AND maxplotsize__c =: maxplotsize AND maxheight__c =: maxheight AND maxfootprint__c =: maxfootprint AND minroofslope__c =: minroofslope AND maxroofslope__c =: maxroofslope AND rooms__c =: rooms AND level__c =: level AND garage__c =: garage AND basement__c =: basement AND livingtype__c =: livingtype AND roof__c =: roof AND ridge__c =: ridge AND construction__c =: construction AND ceiling__c =: ceiling AND eco__c =: eco AND garagelocation__c =: garagelocation AND style__c =: style AND shape__c =: shape AND veranda__c =: veranda AND pool__c =: pool AND wintergarden__c =: wintergarden AND terrace__c =: terrace AND oriel__c =: oriel AND mezzanine__c =: mezzanine AND entrance__c =: entrance AND border__c =: border AND carport__c =: carport AND cost__c =: cost AND gfroom__c =: gfroom AND eaves__c =: eaves AND scarp__c =: scarp AND fireplace__c =: fireplace AND bullseye__c =: bullseye LIMIT 1]) {
                designSet.id = s.Id;
            }
            RestContext.response.addHeader('Cache-Control', 'no-cache, must-revalidate, max-age=0, no-store, private, s-maxage=0');
        }
        
        return designSet;
    }
}