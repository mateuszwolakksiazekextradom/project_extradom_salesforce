@isTest
private class ContactTest {

    static testMethod void myUnitTest() {
        
        Community_Member__c cm = new Community_Member__c(Name='test', Email__c='test@extradom.pl');
		insert cm;
		
		Community_Member__c cm2 = new Community_Member__c(Name='test2', Email__c='test2@extradom.pl');
		insert cm2;
		
		Community_Member_Detail__c cmd1 = new Community_Member_Detail__c(Community_Member__c=cm.Id, Related_Community_Member__c=cm2.Id);
		insert cmd1;
		
		Community_Member_Detail__c cmd2 = new Community_Member_Detail__c(Community_Member__c=cm2.Id, Related_Community_Member__c=cm.Id);
		insert cmd2;
		
		cm.Business_Type__c = 'Brand';
		cm.Org_Member_End_Date__c = System.today();
		cm2.Business_Type__c = 'Service';
		
		update cm;
		update cm2;
		
		
		Case cs = New Case(Description='extradom', SuppliedEmail='test@extradom.pl', SuppliedName='Test', Origin='Web', Subject='Olark chat with ...');
		insert cs;
        Account a = new Account(Name='Test');
        insert a;
        Contact c = New Contact(AccountId=a.Id, Email='test2@extradom.pl', LastName='Test2');
        insert c;
        Case cs2 = New Case(ContactId=c.Id, Description='extradom', SuppliedEmail='test2@extradom.pl', SuppliedName='Test2', Origin='Web', Subject='Olark chat with ...');
        insert cs2;
        
        QueueTools.getAccountNextUser('Inbound Calls');
		
        
    }
}