@IsTest
private class TaskTriggerHandler_Test {
    @IsTest
    private static void shouldCopyAccountId() {
        // given
        Account a1 = new Account(Name = 'TestName1');
        Account a2 = new Account(Name = 'TestName2');
        insert new List<Account> {a1, a2};
        Contact contact = new Contact(LastName = 'Test Name Contact', AccountId = a1.Id, Email = 'test@test.com');
        insert contact;
        Case c = new Case(Subject = 'Test subject', ContactId = contact.Id, SuppliedName = 'TestName');
        insert c;

        // when
        String taskAccAfterInsert = null;
        String taskCaseAfterInsert = null;
        String taskAccAfterUpdate = null;
        Task t1 = new Task(WhatId = a1.Id, Subject = 'Test Subject 1');
        Task t2 = new Task(WhatId = c.Id, Subject = 'Test Subject 2');
        insert new List<Task> {t1, t2};
        Map<Id, Task> tasks = new Map<Id, Task> ([SELECT Id, Account__c, WhatId FROM Task]);
        taskAccAfterInsert = tasks.get(t1.Id).Account__c;
        taskCaseAfterInsert = tasks.get(t2.Id).Account__c;
        t1.WhatId = a2.Id;
        update t1;
        tasks = new Map<Id, Task> ([SELECT Id, Account__c, WhatId FROM Task]);
        taskAccAfterUpdate = tasks.get(t1.Id).Account__c;

        // then
        System.assertEquals(a1.Id, taskAccAfterInsert);
        System.assertEquals(null, taskCaseAfterInsert);
        System.assertEquals(a2.Id, taskAccAfterUpdate);
    }
    
    @IsTest
    private static void shouldCloseFollowUpCallTasks_whenFollowUpTaskInsert() {
        //given
        Account acc = new Account (Name = 'Test');
        insert acc;
        Task oldTask = new Task(Subject = 'Task1', Type = ConstUtils.TASK_FOLLOW_UP_TYPE, Status = ConstUtils.TASK_NOT_STARTED_STATUS, WhatId = acc.Id);
        insert oldTask;
        
        //when
        Task newTask = new Task(Subject = 'Task2', Type = ConstUtils.TASK_FOLLOW_UP_TYPE, Status = ConstUtils.TASK_NOT_STARTED_STATUS, WhatId = acc.Id);
        insert newTask;
        oldTask = [SELECT Id, Status FROM Task WHERE Id = :oldTask.Id];
        
        //then
        System.assertEquals(ConstUtils.TASK_COMPLETED_STATUS, oldTask.Status);
    }
    
    @IsTest
    private static void shouldCloseFollowUpCallTasks_whenFollowUpTaskReopen() {
        //given
        Account acc = new Account (Name = 'Test');
        insert acc;
        Task task1 = new Task(Subject = 'Task1', Type = ConstUtils.TASK_FOLLOW_UP_TYPE, Status = ConstUtils.TASK_NOT_STARTED_STATUS, WhatId = acc.Id);
        Task task2 = new Task(Subject = 'Task2', Type = ConstUtils.TASK_FOLLOW_UP_TYPE, Status = ConstUtils.TASK_COMPLETED_STATUS, WhatId = acc.Id);      
        insert new List<Task>{task1, task2};
        
        //when
        task2.Status = ConstUtils.TASK_NOT_STARTED_STATUS;
        update task2;        
        task1 = [SELECT Id, Status FROM Task WHERE Id = :task1.Id];
        
        //then
        System.assertEquals(ConstUtils.TASK_COMPLETED_STATUS, task1.Status);
    }
    
    @IsTest
    private static void shouldSetTeaser_whenNewCallTaskCreated() {
        // given
        final String DESCRIPTION = 'Test Description';
        User cokUser = TestUtils.prepareTestUser(true, ConstUtils.USER_ADMIN_PROFILE, ConstUtils.USER_COK_DEPARTMENT);
        System.runAs(cokUser) {
            Account a = new Account(Name = 'TestName');
            insert a;
            Task t1 = new Task(WhatId = a.Id, Subject = 'Test Subject 1', Type = ConstUtils.TASK_FOLLOW_UP_TYPE,
                               ActivityDate = system.now().addDays(1).date(), Status = ConstUtils.TASK_NOT_STARTED_STATUS);
            Task t2 = new Task(WhatId = a.Id, Subject = 'Test Subject 2', Type = ConstUtils.TASK_FOLLOW_UP_TYPE,
                               ActivityDate = system.now().addDays(1).date(), Status = ConstUtils.TASK_COMPLETED_STATUS);
            insert new List<Task> {t1, t2};
            Task callTask = new Task(
                WhatId = a.Id, Subject = 'Call Task', Type = ConstUtils.TASK_CALL_TYPE, CallDurationInSeconds = 10,
                CallType = ConstUtils.TASK_INBOUND_CALL_TYPE, Status = ConstUtils.TASK_COMPLETED_STATUS,
                Description = DESCRIPTION
            );
        
        // when
            insert callTask;
            Set<String> taskIds = new Set<String> {t1.Id, t2.Id};
            Map<Id, Task> tasksMap = new Map<Id, Task>([SELECT Id, Teaser__c FROM Task WHERE Id in :taskIds]);
            
        // then
            system.assertEquals(2, tasksMap.size());
            system.assertEquals(DESCRIPTION, tasksMap.get(t1.Id).Teaser__c);
            system.assertEquals(null, tasksMap.get(t2.Id).Teaser__c);
        }
    }
    
    @IsTest
    private static void shouldSetTeaser_whenCallTaskUpdated() {
        // given
        final String DESCRIPTION = 'Test Description';
        User cokUser = TestUtils.prepareTestUser(true, ConstUtils.USER_ADMIN_PROFILE, ConstUtils.USER_COK_DEPARTMENT);
        System.runAs(cokUser) {
            Account a = new Account(Name = 'TestName');
            insert a;
            Task t1 = new Task(WhatId = a.Id, Subject = 'Test Subject 1', Type = ConstUtils.TASK_FOLLOW_UP_TYPE,
                               ActivityDate = system.now().addDays(1).date(), Status = ConstUtils.TASK_NOT_STARTED_STATUS);
            Task t2 = new Task(WhatId = a.Id, Subject = 'Test Subject 2', Type = ConstUtils.TASK_FOLLOW_UP_TYPE,
                               ActivityDate = system.now().addDays(1).date(), Status = ConstUtils.TASK_COMPLETED_STATUS);
            insert new List<Task> {t1, t2};
            Task callTask = new Task(
                WhatId = a.Id, Subject = 'Call Task', Type = ConstUtils.TASK_CALL_TYPE, CallDurationInSeconds = 10,
                CallType = ConstUtils.TASK_INBOUND_CALL_TYPE, Status = ConstUtils.TASK_COMPLETED_STATUS,
                Description = null
            );
            insert callTask;
        
        // when
            callTask.Description = DESCRIPTION;
            update callTask;
            Set<String> taskIds = new Set<String> {t1.Id, t2.Id};
            Map<Id, Task> tasksMap = new Map<Id, Task>([SELECT Id, Teaser__c FROM Task WHERE Id in :taskIds]);
            
        // then
            system.assertEquals(2, tasksMap.size());
            system.assertEquals(DESCRIPTION, tasksMap.get(t1.Id).Teaser__c);
            system.assertEquals(null, tasksMap.get(t2.Id).Teaser__c);
        }
    }
    
    @IsTest
    private static void shouldSetTeaser_whenFollowUpTaskCreated() {
        // given
        final String DESCRIPTION = 'Test Description';
        User cokUser = TestUtils.prepareTestUser(true, ConstUtils.USER_ADMIN_PROFILE, ConstUtils.USER_COK_DEPARTMENT);
        System.runAs(cokUser) {
            Account a = new Account(Name = 'TestName');
            insert a;
            Task callTask = new Task(
                WhatId = a.Id, Subject = 'Call Task', Type = ConstUtils.TASK_CALL_TYPE, CallDurationInSeconds = 10,
                CallType = ConstUtils.TASK_INBOUND_CALL_TYPE, Status = ConstUtils.TASK_COMPLETED_STATUS,
                Description = DESCRIPTION
            );
            insert callTask;
        
        // when
            Task followUpTask = new Task(WhatId = a.Id, Subject = 'Test Subject', Type = ConstUtils.TASK_FOLLOW_UP_TYPE,
                               ActivityDate = system.now().addDays(1).date(), Status = ConstUtils.TASK_NOT_STARTED_STATUS);
            insert followUpTask;
            followUpTask = [SELECT Id, Teaser__c FROM Task WHERE Id = :followUpTask.Id LIMIT 1];
            
        // then
            system.assertEquals(DESCRIPTION, followUpTask.Teaser__c);
        }
    }
    
    @IsTest
    private static void shouldCreateDealerAlertTask() {
        //given
        Account a = new Account(Name = 'TestName', Type = ConstUtils.ACCOUNT_DEALER_TYPE);
        insert a;
        
        //when
        Task t = new Task(Status = ConstUtils.TASK_COMPLETED_STATUS, Type = ConstUtils.TASK_QUESTION_TYPE);
        insert t;
    }
    
    @IsTest
    private static void shouldSetTeaser_whenFollowUpTaskUpdated() {
        // given
        final String DESCRIPTION = 'Test Description';
        User cokUser = TestUtils.prepareTestUser(true, ConstUtils.USER_ADMIN_PROFILE, ConstUtils.USER_COK_DEPARTMENT);
        System.runAs(cokUser) {
            Account a = new Account(Name = 'TestName');
            insert a;
            Task callTask = new Task(
                WhatId = a.Id, Subject = 'Call Task', Type = ConstUtils.TASK_CALL_TYPE, CallDurationInSeconds = 10,
                CallType = ConstUtils.TASK_INBOUND_CALL_TYPE, Status = ConstUtils.TASK_COMPLETED_STATUS,
                Description = DESCRIPTION
            );
            insert callTask;
            Task followUpTask = new Task(WhatId = a.Id, Subject = 'Test Subject', Type = ConstUtils.TASK_FOLLOW_UP_TYPE,
                ActivityDate = system.now().addDays(1).date(), Status = ConstUtils.TASK_COMPLETED_STATUS);
            insert followUpTask;
            Task followUpTaskBefore = [SELECT Id, Teaser__c FROM Task WHERE Id = :followUpTask.Id LIMIT 1];
        
        // when
            followUpTask.Status = ConstUtils.TASK_NOT_STARTED_STATUS;
            update followUpTask;
            Task followUpTaskAfter = [SELECT Id, Teaser__c FROM Task WHERE Id = :followUpTask.Id LIMIT 1];
            
        // then
            system.assertEquals(null, followUpTaskBefore.Teaser__c);
            system.assertEquals(DESCRIPTION, followUpTaskAfter.Teaser__c);
        }
    }
}