@isTest
private class taskCallTriggerTest {

    static testMethod void myUnitTest() {
        
        test.startTest();
        
        Profile p = [SELECT Id From Profile WHERE Name='Standard User'];
        User u = new User( Alias = 'newUser1' ,
                           Email ='newuser123@testorg1.com',
                           EmailEncodingKey = 'UTF-8',
                           LastName = 'Testing',
                           LanguageLocaleKey='en_US',
                           LocaleSidKey='en_US', 
                           UserName='newuser123@testorg1.com',
                           ProfileId=p.Id,
                           Department='COK',
                           TimeZoneSidKey = 'America/Los_Angeles' );
         Insert u;

         System.runAs(u) {
            
            Account a = new Account();
            a.Name = 'Jan Kowalski';
            insert a;
            
            Contact c = new Contact();
            c.LastName = 'Kowalski';
            c.AccountId = a.Id;
            c.Phone = '604651831';
            insert c;
            
            Task t = new Task();
            t.WhoId = c.Id;
            t.Subject = 'Porzucone połączenie';
            t.Status = 'Not Started';
            t.Type = 'Unanswered Call';
            insert t;
            
            Task ft = new Task();
            ft.WhoId = c.Id;
            ft.Subject = 'Następny kontakt telefoniczny';
            ft.Status = 'Not Started';
            ft.Type = 'Follow Up Call';
            insert ft;
            
            /*
            Task t2 = new Task();
            t2.WhoId = c.Id;
            t2.Subject = 'Połączenie wychodzące';
            t2.Status = 'Completed';
            t2.CallType = 'Outbound';
            t2.CallDurationInSeconds = 40;
            insert t2;
            */
            
            /*
            Task ft2 = new Task();
            ft2.WhoId = c.Id;
            ft2.Subject = 'Następny kontakt telefoniczny';
            ft2.Status = 'Not Started';
            ft2.Type = 'Follow Up Call';
            insert ft2;
            */
            
            
            Task t4 = new Task();
            t4.WhoId = c.Id;
            t4.Subject = 'Połączenie wychodzące';
            t4.Status = 'Completed';
            t4.CallType = 'Outbound';
            t4.CallDurationInSeconds = 40;
            t4.Description = 'test';
            insert t4;
            
            for (Task t3 : [SELECT Id, IsClosed FROM Task WHERE Id = :t.Id]) {
                System.assert(t3.IsClosed);
            }
            
            update a;
            
            CallTaskProcessing.processRecords(new List<Id>{t4.Id});
            
         }
        
        test.stopTest();
        
    }
}