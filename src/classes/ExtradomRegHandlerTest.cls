@isTest
private class ExtradomRegHandlerTest {
    static testMethod void testCreateAndUpdateUser() {
        ExtradomRegHandler handler = new ExtradomRegHandler();
        Auth.UserData sampleData = new Auth.UserData('testId', 'testFirst', 'testLast',
            'testFirst testLast', 'testuser@example.org', null, 'testuserlong', 'pl_PL', 'facebook',
            null, new Map<String, String>{'language' => 'pl_PL'});
        User u = handler.createUser(null, sampleData);
        System.assertEquals('testuser@example.org', u.Username);
        System.assertEquals('testuser@example.org', u.Email);
        insert(u);
        String uid = u.id;
        
        sampleData = new Auth.UserData('testNewId', 'testNewFirst', 'testNewLast',
            'testNewFirst testNewLast', 'testnewuser@example.org', null, 'testnewuserlong', 'pl_PL', 'facebook',
            null, new Map<String, String>{});
        handler.updateUser(uid, null, sampleData);
    }
}