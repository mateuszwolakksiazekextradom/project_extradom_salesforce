/**
 * Created by mateusz.wolak on 10.06.2019.
 * Test class for AiquaIntegration
 **/

@isTest
private class AiquaIntegration_Test {


    @isTest
    static void sendCustomersWhoAreAlreadyBuyers_test() {
        Account account = TestUtils.newAccount(new Account(), true);

        Order newOrder = TestUtils.newOrder(new Order(AccountId = account.Id), true);
        OrderItem newOrderItem = TestUtils.newOrderItem(new OrderItem(OrderId = newOrder.Id), true);


        Test.startTest();

            Test.setMock(HttpCalloutMock.class, new AiquaIntegration_MockImpl());

            newOrder.Status = ConstUtils.ORDER_SENT_STATUS;
            update newOrder;

        Test.stopTest();

    }


    public class AiquaIntegration_MockImpl implements HttpCalloutMock {

        public HttpResponse respond(HttpRequest req) {

            System.assertNotEquals(null, req.getEndpoint());
            System.assertEquals('POST', req.getMethod());

            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setStatusCode(200);
            return res;

        }
    }


}