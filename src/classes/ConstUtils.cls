public class ConstUtils {
    public static final String ACC_IMPORT_SOURCE = 'Import';
    public static final String ACC_EXTRADOM_SOURCE = 'Extradom.pl (Community)';

    public static final String DESIGN_HOUSE_TYPE = 'Dom';
    public static final String DESIGN_GARAGE_TYPE = 'Garaż';
    public static final String DESIGN_DELETE_ERROR = 'You cannot delete the record since Design Code is assigned';
    public static final String DESIGN_ACTIVE_STATUS = 'Active';
    public static final String DESIGN_DRAFT_STATUS = 'Draft';
    public static final String DESIGN_VERSION_STATUS = 'Version';
    public static final String DESIGN_IDEA_STAGE = 'Idea';
    public static final String DESIGN_EXPIRED_STAGE = 'Out-of-date';
    public static final String DESIGN_NEEDS_UPDATE_STAGE = 'Needs Update';

    public static final String PRODUCT_DESIGN_FAMILY = 'Plan Set';
    public static final String PRODUCT_ADD_ON_FAMILY = 'Add-On';
    public static final String PRODUCT_GIFT_FAMILY = 'Gift';
    public static final String PRODUCT_EXTRA_FAMILY = 'Extra';
    public static final String PRODUCT_UPSELL_FAMILY = 'Upsell';
    public static final String PRODUCT_MISC_FAMILY = 'Misc';
    public static final String PRODUCT_STARTER_PACK = 'Starter Pack';
    public static final String PRODUCT_GARDEN_DESIGN = 'Garden Design';

    public static final String TASK_CALL_TYPE = 'Call';
    public static final String TASK_INBOUND_CALL_TYPE = 'Inbound';
    public static final String TASK_FOLLOW_UP_TYPE = 'Follow Up Call';
    public static final String TASK_CONFLICT_TYPE = 'Conflict Task';
    public static final String TASK_COMPLETED_STATUS = 'Completed';
    public static final String TASK_NOT_STARTED_STATUS = 'Not Started';
    public static final String TASK_DEALER_MATERIALS_TYPE = 'Dealer Materials';
    public static final String TASK_ESTIMATES_TYPE = 'Estimates Request';
    public static final String TASK_QUESTION_TYPE = 'Question';
    public static final String TASK_UNANSWERED_CALL_TYPE = 'Unanswered Call';
    public static final String TASK_CALL_REQUEST_TYPE = 'Call Request';

    public static final String ACCOUNT_OBJECT = 'Account';
    public static final String ACCOUNT_NEW_LEAD_STATUS = 'New';
    public static final String ACCOUNT_CONVERTED_LEAD_STATUS = 'Converted';
    public static final String ACCOUNT_MARKETING_QUALIFIED_LEAD_STATUS = 'Marketing Qualified';
    public static final String ACCOUNT_SALES_READY_LEAD_STATUS = 'Sales Ready';
    public static final String ACCOUNT_SALES_QUALIFIED_LEAD_STATUS = 'Sales Qualified';
    public static final String ACCOUNT_UNQUALIFIED_LEAD_STATUS = 'Unqualified';
    public static final String ACCOUNT_OTHER_UNQUALIFIED_REASON = 'Other';
    public static final String ACCOUNT_DEALER_TYPE = 'Dealer';
    public static final String ACCOUNT_CUSTOMER_RT = 'Customer';
    public static final String ACCOUNT_CUSTOMER_RT_ID = RecordTypeManager.RecordTypeId(ACCOUNT_OBJECT + '.' + ACCOUNT_CUSTOMER_RT);
       
    public static final String OPPORTUNITY_CLOSED_WON_STAGE = 'Closed Won';
    public static final String OPPORTUNITY_CLOSED_DUPLICATE_STAGE = 'Closed Lost / Duplicate';
    public static final String OPPORTUNITY_CLOSED_COMPETITOR_STAGE = 'Closed Lost / Competitor';
    public static final String OPPORTUNITY_SOLUTION_PROPOSAL_STAGE = 'Solution Proposal';
    public static final String OPPORTUNITY_CLOSED_FORECAST = 'Closed';
    public static final String OPPORTUNITY_OMITTED_FORECAST = 'Omitted';

    public static final String ORDER_OBJECT = 'Order';
    public static final String ORDER_REGULAR_RT = 'Regular';
    public static final String ORDER_REDUCTION_RT = 'Regular';
    public static final String ORDER_REGULAR_RT_ID = RecordTypeManager.RecordTypeId(ORDER_OBJECT + '.' + ORDER_REGULAR_RT);
    public static final String ORDER_REDUCTION_RT_ID = RecordTypeManager.RecordTypeId(ORDER_OBJECT + '.' + ORDER_REDUCTION_RT);
    public static final String ORDER_ACTIVATED_STATUS_CODE = 'A';
    public static final String ORDER_STATUS_CODE_ACTIVATED = 'Activated';
    public static final String ORDER_DRAFT_STATUS = 'Draft';
    public static final String ORDER_SENT_STATUS = 'Sent';
    public static final String ORDER_WAITING_FOR_SHIPMENT_STATUS = 'Waiting for Shipment';
    public static final String ORDER_ACCEPTED_STATUS = 'Accepted';
    public static final String ORDER_CANCELED_STATUS = 'Canceled';
    public static final String ORDER_RETURNED_STATUS = 'Returned';
    public static final String ORDER_OPT_IN_PARAM = 'optIn';
    public static final String ORDER_OPT_IN_ORDER_PARAM = 'optInOrder';
    public static final String ORDER_OPT_IN_REGULATIONS_PARAM = 'optInRegulations';
    public static final String ORDER_OPT_IN_PRIVACY_POLICY_PARAM = 'optInPrivacyPolicy';
    public static final String ORDER_OPT_IN_MARKETING_PARAM = 'optInMarketing';
    public static final String ORDER_OPT_IN_MARKETING_EXTENDED_PARAM = 'optInMarketingExtended';
    public static final String ORDER_OPT_IN_MARKETING_PARTNER_PARAM = 'optInMarketingPartner';
    public static final String ORDER_OPT_IN_LEAD_FORWARD_PARAM = 'optInLeadForward';
    public static final String ORDER_OPT_IN_REGULATIONS_AND_PRIVACY_POLICY_PARAM = 'optInRegulationsAndPrivacyPolicy';
    public static final String ORDER_OPT_IN_EMAIL_PARAM = 'optInEmail';
    public static final String ORDER_OPT_IN_PHONE_PARAM = 'optInPhone';
    public static final String ORDER_OPT_IN_PROFILING_PARAM = 'optInProfiling';
    public static final String ORDER_OPT_IN_PKO_PARAM = 'optInPKO';
    public static final String ORDER_ONLINE_PAYMENT = 'Online';
    public static final String ORDER_BASIC_VERSION = 'Basic';
    public static final String ORDER_MIRRORED_VERSION = 'Mirrored';
    public static final String ORDER_REST_CREATE_MESSAGE = 'Dziękujemy za złożenie zamówienia. Potwierdzenie złożenia zamówienia zostało'
            + ' przesłane na podany adres poczty e-mail. Wkrótce skontaktujemy się z Państwem w celu ustalenia dogodnego terminu dostarczenia projektu.';
    public static final String ORDER_REST_CREATE_COMMENTS = 'Dostarczenie projektu następuje przeważnie w terminie 3-5 dni roboczych od potwierdzenia'
            + ' zamówienia przez konsultanta Serwisu Extradom.pl. Maksymalny czas oczekiwania to 14 dni. Stworzenie Projektu domu na podstawie jego'
            + ' Koncepcji może wydłużyć czas realizacji, o czym zostaną Państwo poinformowani telefonicznie. Konsultant Serwisu Extradom.pl uprzedzi'
            + ' o terminie dostarczenia projektu z jednodniowym wyprzedzeniem.';

    public static final String CONTACT_INVESTOR_TYPE = 'Investor';

    public static final String USER_DESIGN_SALES_AGENT_ROLE = 'Design Sales Agent';
    public static final String USER_ADMIN_PROFILE = 'System Administrator';
    public static final String USER_COK_DEPARTMENT = 'COK';
    public static final String USER_B2B_DEPARTMENT = 'B2B';
    public static final String USER_PROFILE_SUPPLIERS_CC_USER = 'Suppliers CC User';
    public static final String USER_PERMISSION_SET_SUPPLIERS_COMMUNITY_MEMBER = 'Suppliers Community Member';

    public static Map<String, Integer> LEAD_STATUS_MAP = new Map<String, Integer>{
        'Unqualified' => 0,
        'New' => 1, 
        'Marketing Qualified' => 2,
        'Sales Ready' => 3,
        'Sales Qualified' => 4,
        'Converted' => 5
    };

    public static Map<String, Id> profileName2Id = new Map<String, Id>();

    public static Id getProfileId(String profileName) {

        if(profileName2Id.containsKey(profileName)) {

            return profileName2Id.get(profileName);

        } else {

            for(Profile p : [SELECT Id, Name FROM Profile]) {
                profileName2Id.put(p.Name, p.Id);
            }

            return profileName2Id.get(profileName);
        }

    }

    public static Map<String, Id> permissionSet2Id = new Map<String, Id>();

    public static Id getPermissionSetId(String permissionSetName) {

        if(permissionSet2Id.containsKey(permissionSetName)) {

            return permissionSet2Id.get(permissionSetName);

        } else {

            for(PermissionSet p : [SELECT Id, Name FROM PermissionSet]) {
                permissionSet2Id.put(p.Name, p.Id);
            }

            return permissionSet2Id.get(permissionSetName);
        }

    }

}