public without sharing class TaskTriggerHandler extends TriggerHandler {
    
    public override void afterInsert() {
        Map<Id, Task> tasksMap = (Map<Id, Task>) Trigger.newMap;
        setAccount(tasksMap);
        closeOpenFollowUpCallTasks(tasksMap,null);
        setTeaser(tasksMap);
        createDealerMaterialsTask(tasksMap,null);
    }

    public override void afterUpdate() {
        Map<Id, Task> tasksMap = (Map<Id, Task>) Trigger.newMap;
        Map<Id, Task> oldTasksMap = (Map<Id, Task>) Trigger.oldMap;        
        setAccount(tasksMap);
        closeOpenFollowUpCallTasks(tasksMap,oldTasksMap);
        setTeaser(tasksMap);
        createDealerMaterialsTask(tasksMap,oldTasksMap);
    }
    
    public override void beforeUpdate() {
        Map<Id, Task> tasksMap = (Map<Id, Task>) Trigger.newMap;
        Map<Id, Task> oldTasksMap = (Map<Id, Task>) Trigger.oldMap;
        setCloseDate(tasksMap,oldTasksMap);           
    }
    
    private void setCloseDate(Map<Id, Task> tasksMap, Map<Id, Task> oldTasksMap) {
        for (Task t : tasksMap.values()) {
            if ((t.Type == ConstUtils.TASK_CALL_REQUEST_TYPE || t.Type == ConstUtils.TASK_UNANSWERED_CALL_TYPE) && t.Status == ConstUtils.TASK_COMPLETED_STATUS && oldTasksMap != null
            && t.Status != oldTasksMap.get(t.id).Status && t.Close_Date__c == null && UserInfo.getUserRoleId() != null && (UserInfo.getUserRoleId() == '00Eb0000000Ln3EEAS' || UserInfo.getUserRoleId() == '00Eb0000000Ln3E')) {
                t.ActivityDate = System.today();
                t.OwnerId = UserInfo.getUserId();
                t.Close_Date__c = System.now();
            }
        }
    }

    private void setAccount(Map<Id, Task> tasksMap) {
        List<Task> tasks = [SELECT Id, AccountId, Account__c FROM Task WHERE Id in :tasksMap.keySet()];
        List<Task> tasksToUpdate = new List<Task> ();
        for (Task task : tasks) {
            if (task.AccountId != null && task.AccountId.getSobjectType() == Account.getSObjectType()
                    && task.AccountId != task.Account__c) {
                task.Account__c = task.AccountId;
                tasksToUpdate.add(task);
            }
        }
        TriggerHandler.bypass('TaskTriggerHandler');
        update tasksToUpdate;
        TriggerHandler.clearBypass('TaskTriggerHandler');
    }
    
    private void closeOpenFollowUpCallTasks(Map<Id, Task> tasksMap, Map<Id, Task> oldTasksMap) {
        Set<Id> accountsToClose = new Set<Id>();
        for (Task t : tasksMap.values()) {
            if ((oldTasksMap == null && t.Type == ConstUtils.TASK_FOLLOW_UP_TYPE && t.Status == ConstUtils.TASK_NOT_STARTED_STATUS) 
                || (t.Type == ConstUtils.TASK_FOLLOW_UP_TYPE && oldTasksMap != null && oldTasksMap.containsKey(t.Id) && t.Status != oldTasksMap.get(t.id).Status && t.Status == ConstUtils.TASK_NOT_STARTED_STATUS)) {
                accountsToClose.add(t.AccountId);
            }
        }
        
        List<Task> tasksToClose = new List<Task>();
            for (Task t : [SELECT Id, Status FROM Task WHERE AccountId IN :accountsToClose AND Type = :ConstUtils.TASK_FOLLOW_UP_TYPE AND Status = :ConstUtils.TASK_NOT_STARTED_STATUS AND Id NOT IN :tasksMap.keySet()]) {
                t.Status = ConstUtils.TASK_COMPLETED_STATUS;
                tasksToClose.add(t);
            }      
        TriggerHandler.bypass('TaskTriggerHandler');
        update tasksToClose;
        TriggerHandler.clearBypass('TaskTriggerHandler');
    }
    
    private void createDealerMaterialsTask(Map<Id, Task> tasksMap, Map<Id, Task> oldTasksMap) {
        Set<Id> accountIds = new Set<Id>();       
        for (Task t : tasksMap.values()) {
            if ((oldTasksMap == null && (t.Type == ConstUtils.TASK_ESTIMATES_TYPE || t.Type == ConstUtils.TASK_QUESTION_TYPE) && t.Status == ConstUtils.TASK_COMPLETED_STATUS)
            || (oldTasksMap != null && (t.Type == ConstUtils.TASK_ESTIMATES_TYPE || t.Type == ConstUtils.TASK_QUESTION_TYPE) && t.Status == ConstUtils.TASK_COMPLETED_STATUS && oldTasksMap.get(t.Id).Status != t.Status)) {
                accountIds.add(t.AccountId);    
            }
        }
        system.debug('Ids '+accountIds);
    
        List<Task> tasks = new List<Task>();
        for (Account a : [SELECT Id, OwnerId, (SELECT Id FROM Tasks WHERE Type = :ConstUtils.TASK_DEALER_MATERIALS_TYPE AND Status = :ConstUtils.TASK_NOT_STARTED_STATUS) FROM Account WHERE Id IN :accountIds AND Type = :ConstUtils.ACCOUNT_DEALER_TYPE]) {
            system.debug('Taski ' + a.tasks);
            if (a.Tasks.isEmpty()) {
                system.debug('Weszło ' + a.tasks);
                tasks.add(new Task(Subject = 'Dealer zamówił rysunki/koszty', OwnerId = a.OwnerId, Type = ConstUtils.TASK_DEALER_MATERIALS_TYPE, Status = ConstUtils.TASK_NOT_STARTED_STATUS, WhatId = a.Id, ActivityDate = System.today()));
            }
        }       
        insert tasks;
    } 
    
    private void setTeaser(Map<Id, Task> tasksMap) {
        List<String> accountIds = new List<String> ();
        for (Task t : tasksMap.values()) {
            if (((t.Type == ConstUtils.TASK_FOLLOW_UP_TYPE && t.Status == ConstUtils.TASK_NOT_STARTED_STATUS)
                || (String.isNotBlank(t.CallType) && t.CallDurationInSeconds > 0 && t.IsClosed == true && String.isNotBlank(t.Description)))
                && t.AccountId != null) {
                accountIds.add(t.AccountId);
            }
        }
        system.debug(accountIds);
        if (accountIds.size() > 0) {
            List<Task> tasks = [
                SELECT Id, AccountId, Description, CallType, CallDurationInSeconds, CreatedBy.Department, Type, Status
                FROM Task 
                WHERE AccountId in :accountIds AND
                    ((CallType != null AND CallDurationInSeconds > 0 AND CreatedBy.Department = :ConstUtils.USER_COK_DEPARTMENT 
                    AND IsClosed = true) OR (Type = :ConstUtils.TASK_FOLLOW_UP_TYPE AND Status = :ConstUtils.TASK_NOT_STARTED_STATUS))
                ORDER BY CreatedDate ASC
            ];
            system.debug(tasks);
            Map<String, String> teaserMap = new Map<String, String> ();
            List<Task> followUpTasks = new List<Task> ();
            for (Task t : tasks) {
                if (String.isNotBlank(t.Description) && String.isNotBlank(t.CallType) && t.CallDurationInSeconds > 0
                        && t.CreatedBy.Department == ConstUtils.USER_COK_DEPARTMENT ) {
                    teaserMap.put(t.AccountId, t.Description);
                } else if (t.Type == ConstUtils.TASK_FOLLOW_UP_TYPE && t.Status == ConstUtils.TASK_NOT_STARTED_STATUS) {
                    followUpTasks.add(t);
                }
            }
            
            List<Task> tasksToUpdate = new List<Task> ();
            for (Task t : followUpTasks) {
                if (t.AccountId != null && teaserMap.containsKey(t.AccountId)) {
                    t.Teaser__c = teaserMap.get(t.AccountId);
                    tasksToUpdate.add(t);
                }
            }
            TriggerHandler.bypass('TaskTriggerHandler');
            update tasksToUpdate;
            TriggerHandler.clearBypass('TaskTriggerHandler');
        }
    }
}