@isTest
private class ConstructionViewExtensionTest {

    static testMethod void myUnitTest() {
        
        test.startTest();
        
        Construction__c c = new Construction__c(Name='Testowa budowa');
        insert c;
        
        ConnectApi.FeedElementPage testPage = new ConnectApi.FeedElementPage();
        List<ConnectApi.FeedItem> testItemList = new List<ConnectApi.FeedItem>();
        testItemList.add(new ConnectApi.FeedItem());
        testPage.elements = testItemList;
        ConnectApi.ChatterFeeds.setTestGetFeedElementsFromFeed(null, ConnectApi.FeedType.Record, c.id, testPage);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(c);
        ConstructionViewExtension ext = new ConstructionViewExtension(sc);
        ext.getFeedElementPage();
        
        test.stopTest();
        
    }
}