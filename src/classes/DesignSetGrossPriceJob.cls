global without sharing class DesignSetGrossPriceJob implements Schedulable {
    public void execute(SchedulableContext sc) {
        Database.executeBatch(new DesignSetGrossPriceBatch());
    }
}