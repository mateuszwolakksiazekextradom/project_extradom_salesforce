@RestResource(urlMapping='/app/users/password')
global without sharing class AppUsersPassword {
    @HttpDelete
    global static Map<String,String> doDelete() {
        RestContext.response.addHeader('Cache-Control', 'no-cache, must-revalidate, max-age=0, no-store, private, s-maxage=0');
        String username = RestContext.request.params.get('username');
        Map<String,String> result = new Map<String,String>{};
        PageReference pr = Page.passwordRecoveryInline;
        Map<string, string> params = pr.getParameters();
        params.put('username', username);
        Map<String, Object> jsondata = (Map<String, Object>)JSON.deserializeUntyped(pr.getContent().toString());
        String message = (String)jsondata.get('message');
        if (message == 'Instrukcja resetu hasła została wysłana') {
            RestContext.response.statusCode = 204;
            return null;
        } else {
            RestContext.response.statusCode = 400;
            return new Map<String,String>{ 'message' => message };
        }
        return null;
    }
}