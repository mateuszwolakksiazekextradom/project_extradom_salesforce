@isTest
private class ExtradomRewriterTest {
    
    @isTest
    static void testMapUrl() {
        Supplier__c s = new Supplier__c(Name='Test Supplier ABC', Full_Name__c='Test Supplier', Code__c='ABC');
        insert s;
        Design__c d = new Design__c(Name='Test Design ABC1234', Full_Name__c='Test Design', Code__c='ABC1234', Supplier__c = s.Id, Canonical_URL__c='/projekt-domu-test-design-ABC1234');
        insert d;
        Model__c m = new Model__c(Name='Example Model', Design__c=d.Id);
        insert m;
        Plot__c p = new Plot__c(Name='Example Plot', Canonical_Name__c='example-plot');
        insert p;
        Page__c page = new Page__c(Name='Example Page', URL__c='/projekty-garazy', VF_URL__c='/DesignSearchView?type=2', q_type__c=2);
        insert page;
        
        ExtradomRewriter rewriter = new ExtradomRewriter();
        
        PageReference actualResult = rewriter.mapRequestUrl(new PageReference('/projekt-domu-test-design-ABC1234'));
        System.assertEquals(actualResult.getUrl(),'/DesignView?id='+d.Id);
        
        actualResult = rewriter.mapRequestUrl(new PageReference('/wizualizacja-3d-projektu-'+m.Id));
        System.assertEquals(actualResult.getUrl(),'/ModelView?id='+m.Id);
        
        actualResult = rewriter.mapRequestUrl(new PageReference('/projekty-garazy'));
        System.assertEquals(actualResult.getUrl(),'/DesignSearchView?id='+page.Id);
        
        actualResult = rewriter.mapRequestUrl(new PageReference('/w-budowie-projekt-domu-test-design-ABC1234'));
        System.assertEquals(actualResult.getUrl(),'/DesignSocialView?id='+d.Id);
        
        actualResult = rewriter.mapRequestUrl(new PageReference('/zdjecia-projektu-domu-test-design-ABC1234'));
        System.assertEquals(actualResult.getUrl(),'/DesignSocialGalleryView?id='+d.Id);
        
        actualResult = rewriter.mapRequestUrl(new PageReference('/jsondesignviewbycode?code=ABC1234'));
        System.assertEquals(actualResult.getUrl(),'/jsondesignview?id='+d.Id);
        
        actualResult = rewriter.mapRequestUrl(new PageReference('/dzialka-example-plot'));
        System.assertEquals(actualResult.getUrl(),'/PlotView?id='+p.Id);
        rewriter.generateUrlFor(new List<PageReference>{new PageReference('/PlotView?id='+p.Id)});
        
        List<PageReference> pageRefs = new List<PageReference>{new PageReference('/DesignView?id='+d.Id), new PageReference('/DesignSocialView?id='+d.Id), new PageReference('/DesignSocialGalleryView?id='+d.Id)};
        for (PageReference pageRef : rewriter.generateUrlFor(pageRefs)) {
            System.assertEquals(pageRef.getUrl().right(7),'ABC1234');
        }
        
        pageRefs = new List<PageReference>{new PageReference('/DesignSearchView?type=2')};
        for (PageReference pageRef : rewriter.generateUrlFor(pageRefs)) {
            System.assertEquals(pageRef.getUrl(),'/projekty-garazy');
        }
        
        List<PageReference> anotherRefs = new List<PageReference>{new PageReference('/DesignView?id=badid'), new PageReference('/'), new PageReference('/projekt-domu-test-WWW1001?abc'), new PageReference('/w-budowie-projekt-domu-test-WWW1001?abc'), new PageReference('/zdjecia-projektu-domu-test-WWW1001?abc'), new PageReference('/ModelView?id=bad'), new PageReference('/ModelView?id='+m.Id)};
        rewriter.generateUrlFor(anotherRefs);
        rewriter.mapRequestUrl(new PageReference('/cosinnego'));
    }
    
    @isTest
    static void testMapNonVipUrl() {

        ExtradomRewriter rewriter = new ExtradomRewriter();
        PageReference actualResult = rewriter.mapRequestUrl(new PageReference('/projekt-domu-test-design'));
        PageReference expectedResult = new PageReference('/designview_not_found');
        System.assertEquals(actualResult.getUrl(),expectedResult.getUrl());
        
        actualResult = rewriter.mapRequestUrl(new PageReference('/w-budowie-projekt-test-design'));
        expectedResult = new PageReference('/designsocialview_not_found');
        System.assertEquals(actualResult.getUrl(),expectedResult.getUrl());
        
        actualResult = rewriter.mapRequestUrl(new PageReference('/zdjecia-projektu-test-design'));
        expectedResult = new PageReference('/designsocialgalleryview_not_found');
        System.assertEquals(actualResult.getUrl(),expectedResult.getUrl());
        
        actualResult = rewriter.mapRequestUrl(new PageReference('/tag-test'));
        expectedResult = new PageReference('/topicview_not_found');
        System.assertEquals(actualResult.getUrl(),expectedResult.getUrl());
        
        actualResult = rewriter.mapRequestUrl(new PageReference('/zdjecia-taga-test'));
        expectedResult = new PageReference('/topicgalleryview_not_found');
        System.assertEquals(actualResult.getUrl(),expectedResult.getUrl());
        
        rewriter.mapRequestUrl(new PageReference('/katalog-projektow-a'));
        
        actualResult = rewriter.mapRequestUrl(new PageReference('/profil-test'));
        expectedResult = new PageReference('/profileview_not_found');
        System.assertEquals(actualResult.getUrl(),expectedResult.getUrl());
        
        rewriter.mapRequestUrl(new PageReference('/ProfileView?id='+UserInfo.getUserId()));
    }
    
    @isTest
    static void testStaticUrls() {

        ExtradomRewriter rewriter = new ExtradomRewriter();
        
        PageReference actualResult = rewriter.mapRequestUrl(new PageReference('/spolecznosc'));
        PageReference expectedResult = new PageReference('/HomeView');
        System.assertEquals(actualResult.getUrl(),expectedResult.getUrl());
        
        actualResult = rewriter.mapRequestUrl(new PageReference('/pytania-i-odpowiedzi'));
        expectedResult = new PageReference('/QAView');
        System.assertEquals(actualResult.getUrl(),expectedResult.getUrl());
        
        actualResult = rewriter.mapRequestUrl(new PageReference('/porady-ekspertow'));
        expectedResult = new PageReference('/GroupView?id=0F9b0000000LcabCAC');
        System.assertEquals(actualResult.getUrl(),expectedResult.getUrl());
        
        actualResult = rewriter.mapRequestUrl(new PageReference('/powiadomienia'));
        expectedResult = new PageReference('/NewsView');
        System.assertEquals(actualResult.getUrl(),expectedResult.getUrl());
        
        List<PageReference> pageRefs = new List<PageReference>{new PageReference('/HomeView'), new PageReference('/QAView'), new PageReference('/GroupView?id=0F9b0000000LcabCAC'), new PageReference('/NewsView')};
        rewriter.generateUrlFor(pageRefs);
        
    }
    
    @isTest
    static void testComplexUrls() {

        ExtradomRewriter rewriter = new ExtradomRewriter();
        
        PageReference actualResult = rewriter.mapRequestUrl(new PageReference('/porada-abc-0D5b0000037Ny7ACAS'));
        PageReference expectedResult = new PageReference('/FeedElementExpertGroupView?id=0D5b0000037Ny7ACAS');
        System.assertEquals(actualResult.getUrl(),expectedResult.getUrl());
        
        actualResult = rewriter.mapRequestUrl(new PageReference('/post-abc-0D5b0000037Ny7ACAS'));
        expectedResult = new PageReference('/FeedElementView?id=0D5b0000037Ny7ACAS');
        System.assertEquals(actualResult.getUrl(),expectedResult.getUrl());
        
        actualResult = rewriter.mapRequestUrl(new PageReference('/pytanie-abc-0D5b0000037Ny7ACAS'));
        expectedResult = new PageReference('/FeedElementQuestionView?id=0D5b0000037Ny7ACAS');
        System.assertEquals(actualResult.getUrl(),expectedResult.getUrl());
        
        actualResult = rewriter.mapRequestUrl(new PageReference('/rozmowa-abc-03Mb0000000HO2FEAW'));
        expectedResult = new PageReference('/ConversationView?id=03Mb0000000HO2FEAW');
        System.assertEquals(actualResult.getUrl(),expectedResult.getUrl());
        
        actualResult = rewriter.mapRequestUrl(new PageReference('/profil-grg'));
        actualResult = rewriter.mapRequestUrl(new PageReference('/zdjecia-profilu-grg'));
        actualResult = rewriter.mapRequestUrl(new PageReference('/projekty-testowe'));
        
        Topic topic = new Topic(Name='test');
        insert topic;
        
        List<PageReference> pageRefs = new List<PageReference>{new PageReference('/FeedElementExpertGroupView?id=0D5b0000037Ny7ACAS'), new PageReference('/FeedElementQuestionView?id=0D5b0000037Ny7ACAS'), new PageReference('/FeedElementView?id=0D5b0000037Ny7ACAS'), new PageReference('/ConversationView?id=03Mb0000000HO2FEAW'), new PageReference('/ProfileView?id='+UserInfo.getUserId()), new PageReference('/ProfileGalleryView?id='+UserInfo.getUserId()), new PageReference('/TopicView?id='+topic.Id), new PageReference('/TopicGalleryView?id='+topic.Id), new PageReference('/DirectoryViewDesign?col=a')};
        rewriter.generateUrlFor(pageRefs);
        
    }

}