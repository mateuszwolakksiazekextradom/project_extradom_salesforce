/**
 * Created by grzegorz.dlugosz on 28.05.2019.
 */

@isTest
private class BidTriggerHandler_Test {

    // insert new record and check if it updates NazwaPracowni__c
    @isTest
    private static void insertSingleBidAndCheckData() {
        User usr = TestUtils.prepareTestUser(false, ConstUtils.USER_ADMIN_PROFILE, ConstUtils.USER_COK_DEPARTMENT);
        usr.CompanyName = 'TestCompanyName';

        Test.startTest();
        System.runAs(usr) {
            Bid__c bid = TestUtils.newBid(new Bid__c(), true);
        }
        Test.stopTest();

        List<Bid__c> bidAfterTriggerList = [SELECT NazwaPracowni__c FROM Bid__c];

        System.assertEquals(1, bidAfterTriggerList.size());
        System.assertEquals('TestCompanyName', bidAfterTriggerList.get(0).NazwaPracowni__c);
    }

    // update bid record and check if it updates NazwaPracowni__c
    @isTest
    private static void updateSingleBidAndCheckData() {
        User usr = TestUtils.prepareTestUser(false, ConstUtils.USER_ADMIN_PROFILE, ConstUtils.USER_COK_DEPARTMENT);
        usr.CompanyName = 'TestCompanyName';

        Bid__c bid = TestUtils.newBid(new Bid__c(), true);
        List<Bid__c> bidListBeforeTrigger = [SELECT NazwaPracowni__c FROM Bid__c];

        System.assertEquals(1, bidListBeforeTrigger.size());
        System.assert(bidListBeforeTrigger.get(0).NazwaPracowni__c != 'TestCompanyName');

        Test.startTest();
        System.runAs(usr) {
            bid.OwnerId = UserInfo.getUserId();
            update bid;
        }
        Test.stopTest();

        List<Bid__c> bidAfterTriggerList = [SELECT NazwaPracowni__c FROM Bid__c];

        System.assertEquals(1, bidAfterTriggerList.size());
        System.assertEquals('TestCompanyName', bidAfterTriggerList.get(0).NazwaPracowni__c);
    }
}