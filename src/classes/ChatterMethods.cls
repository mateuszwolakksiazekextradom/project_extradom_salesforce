public without sharing class ChatterMethods {
    
    public static List<Id> topUserProfileContentPostIds(Id userId) {
        List<Id> ids = new List<Id>{};
        for (FeedItem fi : [SELECT Id FROM FeedItem WHERE CreatedById = :userId AND Visibility = 'AllUsers' AND Type = 'ContentPost' ORDER BY LikeCount DESC, CreatedDate DESC LIMIT 6]) {
            ids.add(fi.Id);
        }
        return ids;
    }
    
    public static List<Id> userProfileContentPostIds(Id userId) {
        List<Id> ids = new List<Id>{};
        for (FeedItem fi : [SELECT Id FROM FeedItem WHERE CreatedById = :userId AND Visibility = 'AllUsers' AND Type = 'ContentPost' ORDER BY CreatedDate DESC LIMIT 500]) {
            ids.add(fi.Id);
        }
        return ids;
    }
    
    public static Integer countUserProfileContentPosts(Id userId) {
        return [SELECT count() FROM FeedItem WHERE CreatedById = :userId AND Visibility = 'AllUsers' AND Type = 'ContentPost'];
    }
    
    public static List<ConnectApi.FeedElement> getFeedElementsFromStringList(String feedstring) {
        List<ConnectApi.FeedElement> feedElements = new List<ConnectApi.FeedElement>{};
        if (String.isBlank(feedstring)) {
            return feedElements;
        }
        List<String> ids = feedstring.split(',');
        ConnectApi.BatchResult[] batchResults = ConnectApi.ChatterFeeds.getFeedElementBatch(Network.getNetworkId(), ids);
        for (ConnectApi.BatchResult batchResult : batchResults) {
            if (batchResult.isSuccess()) {
                ConnectApi.FeedElement element;
                if (batchResult.getResult() instanceof ConnectApi.FeedElement) {
                    element = (ConnectApi.FeedElement) batchResult.getResult();
                    feedElements.add(element);
                }
            }
        }
        return feedElements;
    }
    
}