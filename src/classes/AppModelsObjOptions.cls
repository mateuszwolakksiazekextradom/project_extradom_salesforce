@RestResource(urlMapping='/app/models/*/options')
global with sharing class AppModelsObjOptions {
    @HttpGet
    global static ExtradomApi.AccessLevel doGet() {
        RestRequest req = RestContext.request;
        Pattern pt = Pattern.compile('\\/app\\/models\\/(.{18})\\/options');
        Matcher mt = pt.matcher(req.requestURI);
        mt.find();
        ID modelId = mt.group(1);
        for (UserRecordAccess ura : [SELECT RecordId, MaxAccessLevel from UserRecordAccess where RecordId = :modelId and UserId =: UserInfo.getUserId()]) {
            if (ura.MaxAccessLevel == 'None') {
                for (UserRecordAccess ura2 : [SELECT RecordId, MaxAccessLevel from UserRecordAccess where RecordId = :modelId and UserId =: '005b0000002FJlBAAW']) {
                    return new ExtradomApi.AccessLevel(null);
                }
            }
            return new ExtradomApi.AccessLevel(ura);
        }
        return null;
    }
    
}