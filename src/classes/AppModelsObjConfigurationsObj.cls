@RestResource(urlMapping='/app/models/*/configurations/*')
global with sharing class AppModelsObjConfigurationsObj {
    @HttpGet
    global static ExtradomApi.ModelConfiguration doGet() {
        RestRequest req = RestContext.request;
        Pattern pt = Pattern.compile('\\/app\\/models\\/(.{18})\\/configurations\\/(.{18})');
        Matcher mt = pt.matcher(req.requestURI);
        mt.find();
        ID modelId = mt.group(1);
        ID configurationId = mt.group(2);
        UserRecordAccess ura;
        try {
            ura = [SELECT RecordId, MaxAccessLevel from UserRecordAccess where RecordId = :configurationId and UserId =: UserInfo.getUserId()];
        } catch (Exception e) {
            RestContext.response.statusCode = 404; // NOT_FOUND
            return null;
        }
        if (ura.MaxAccessLevel == 'None') {
            if ([SELECT count() from Attachment where Id = :configurationId AND IsDeleted = true ALL ROWS]==1) {
                RestContext.response.statusCode = 404; // NOT_FOUND
                return null;
            }
            if (UserInfo.getUserType()=='Guest') {
                RestContext.response.statusCode = 401; // UNAUTHORIZED
                return null;
            } else {
                RestContext.response.statusCode = 403; // FORBIDDEN
                return null;
            }
        }
        Attachment att = [SELECT Id, Name, ParentId, Description, Body, CreatedDate, LastModifiedDate FROM Attachment WHERE Id =: configurationId AND ParentId =: modelId];
        if (UserInfo.getUserType()=='Guest') {
            RestContext.response.addHeader('Cache-Control', 'must-revalidate,max-age=0,s-maxage=3600');
        } else {
            RestContext.response.addHeader('Cache-Control', 'no-cache,must-revalidate,max-age=0,no-store,private,s-maxage=0');
        }
        return new ExtradomApi.ModelConfiguration(att);
    }
    @HttpPatch
    global static ExtradomApi.ModelConfigurationResult doPatch() {
        RestRequest req = RestContext.request;
        ExtradomApi.ModelConfigurationInput input = (ExtradomApi.ModelConfigurationInput)JSON.deserialize(req.requestBody.toString(), ExtradomApi.ModelConfigurationInput.class);
        Pattern pt = Pattern.compile('\\/app\\/models\\/(.{18})\\/configurations\\/(.{18})');
        Matcher mt = pt.matcher(req.requestURI);
        mt.find();
        ID modelId = mt.group(1);
        ID configurationId = mt.group(2);
        UserRecordAccess ura;
        try {
            ura = [SELECT RecordId, MaxAccessLevel from UserRecordAccess where RecordId = :configurationId and UserId =: UserInfo.getUserId()];
        } catch (Exception e) {
            RestContext.response.statusCode = 404; // NOT_FOUND
            return null;
        }
        if (ura.MaxAccessLevel == 'None') {
            if ([SELECT count() from Attachment where Id = :configurationId AND IsDeleted = true ALL ROWS]==1) {
                RestContext.response.statusCode = 404; // NOT_FOUND
                return null;
            }
            if (UserInfo.getUserType()=='Guest') {
                RestContext.response.statusCode = 401; // UNAUTHORIZED
                return null;
            } else {
                RestContext.response.statusCode = 403; // FORBIDDEN
                return null;
            }
        } else if (ura.MaxAccessLevel == 'Read') {
            RestContext.response.statusCode = 403; // FORBIDDEN
            return null;
        }
        Attachment att = [SELECT Id, Name, ParentId, Description, CreatedDate, LastModifiedDate FROM Attachment WHERE Id =: configurationId AND ParentId =: modelId];
        if (input.name != null) {
            att.Name = input.name;
        }
        if (input.version != null) {
            att.Description = input.version;
        }
        if (input.body != null) {
            att.Body = Blob.valueOf(input.body);
        }
        update att;
        try {
            Pattern ptThumb = Pattern.compile('data:(image\\/(jpg|jpeg|png));base64,(.+)');
            Matcher mtThumb = ptThumb.matcher(input.thumbnail);
            mtThumb.find();
            String contentType = mtThumb.group(1);
            String base64Body = mtThumb.group(3);
            AWSTools.uploadFileBase64('extradom.media', UserInfo.getOrganizationId()+'-'+att.id+'-thumb/source', base64Body, contentType, 'AKIAJO25LHDRMTH7SLBA', 'zjzdMrMfTi3i26wIqHEKAwvZkjFBN0qJmGE4sH8n');
        } catch(Exception e) {
        }
        RestContext.response.addHeader('Cache-Control', 'no-cache,must-revalidate,max-age=0,no-store,private,s-maxage=0');
        return new ExtradomApi.ModelConfigurationResult(att);
    }
    @HttpDelete
    global static void doDelete() {
        RestRequest req = RestContext.request;
        Pattern pt = Pattern.compile('\\/app\\/models\\/(.{18})\\/configurations\\/(.{18})');
        Matcher mt = pt.matcher(req.requestURI);
        mt.find();
        ID modelId = mt.group(1);
        ID configurationId = mt.group(2);
        UserRecordAccess ura;
        try {
            ura = [SELECT RecordId, MaxAccessLevel from UserRecordAccess where RecordId = :configurationId and UserId =: UserInfo.getUserId()];
        } catch (Exception e) {
            RestContext.response.statusCode = 404; // NOT_FOUND
            return;
        }
        if (ura.MaxAccessLevel == 'None') {
            if ([SELECT count() from Attachment where Id = :configurationId AND IsDeleted = true ALL ROWS]==1) {
                RestContext.response.statusCode = 404; // NOT_FOUND
                return;
            }
            if (UserInfo.getUserType()=='Guest') {
                RestContext.response.statusCode = 401; // UNAUTHORIZED
                return;
            } else {
                RestContext.response.statusCode = 403; // FORBIDDEN
                return;
            }
        } else if (ura.MaxAccessLevel == 'Read') {
            RestContext.response.statusCode = 403; // FORBIDDEN
            return;
        }
        Attachment att = [SELECT Id, Name, ParentId, Description, Body, CreatedDate, LastModifiedDate FROM Attachment WHERE Id =: configurationId AND ParentId =: modelId];
        RestContext.response.addHeader('Cache-Control', 'no-cache,must-revalidate,max-age=0,no-store,private,s-maxage=0');
        delete att;
    }
}