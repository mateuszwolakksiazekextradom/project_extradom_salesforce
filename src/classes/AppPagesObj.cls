@RestResource(urlMapping='/app/pages/*')
global class AppPagesObj {
    @HttpGet
    global static ExtradomApi.StaticPage doGet() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String pageId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        Page__c p = [SELECT Id, Name, App_Body__c, App_Image__c FROM Page__c WHERE Id =: pageId];
        ExtradomApi.StaticPage staticPage = new ExtradomApi.StaticPage();
        staticPage.url = '/app/pages/'+p.Id;
        staticPage.image = new ExtradomApi.Image();
        staticPage.image.smallImageUrl = p.App_Image__c;
        staticPage.title = p.Name;
        staticPage.body = new ExtradomApi.RichText(p.App_Body__c);
        
        if (UserInfo.getUserType()=='Guest') {
            RestContext.response.addHeader('Cache-Control', 'must-revalidate, max-age=0, s-maxage=300');
        } else {
            RestContext.response.addHeader('Cache-Control', 'no-cache, must-revalidate, max-age=0, no-store, private, s-maxage=0');
        }
        
        return staticPage;
    }
}