public without sharing class DesignResourcesViewExtension {

    private final Design__c d;
    public String type { get;set; }
    
    private final String gaClientId;
    private final ID accountId;

    public DesignResourcesViewExtension(ApexPages.StandardController stdController) {
        this.d = (Design__c)stdController.getRecord();
        ID tid = ApexPages.currentPage().getParameters().get('tid');
        Task t = [SELECT Id, WhatId, AccountId, Type FROM Task WHERE Id = :tid AND WhatId =: d.Id];
        this.type = t.Type;
        this.accountId = t.AccountId;
        if (ApexPages.currentPage().getHeaders().containsKey('gaClientId')) {
            this.gaClientId = ApexPages.currentPage().getHeaders().get('gaClientId');
        }
    }
    
    public PageReference updateGaClientId() {
        if (String.isNotBlank(this.gaClientId) && String.isNotBlank(accountId)) {
            OrderMethods.updateGAConnectorDataFromHeaders(this.accountId, ApexPages.currentPage().getHeaders());
        }
        return null;
    }
    
    public List<Product2> getAddons() {
        return [SELECT Id, Name, Type__c, Version_Required__c, Description, Subtype__c, Product_Description__r.HTML_Description__c, (SELECT Id, Regular_Price__c, UnitPrice FROM PricebookEntries WHERE Pricebook2.IsActive=true AND Pricebook2.IsStandard=true) FROM Product2 WHERE Design__r.Id = :this.d.Id AND Family = 'Add-on' AND isActive = true ORDER BY Subtype__c];
    }

}