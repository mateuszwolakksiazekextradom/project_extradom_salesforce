public without sharing class OrderItemTriggerHandler extends TriggerHandler {
    public override void beforeInsert() {
        List<OrderItem> orderItems = (List<OrderItem>) Trigger.new;
        setAccount(orderItems);
        setProductFamily(orderItems, null);
        setDesignType(orderItems, null);
    }

    public override void afterInsert() {
        List<OrderItem> orderItems = (List<OrderItem>) Trigger.new;
        Set<String> accountIds = Utils.fetchSet(orderItems, 'Account__c');
        OrderUtils.calculateAccountOrderItems(accountIds);
    }

    public override void afterDelete() {
        List<OrderItem> orderItems = (List<OrderItem>) Trigger.old;
        Set<String> accountIds = Utils.fetchSet(orderItems, 'Account__c');
        OrderUtils.calculateAccountOrderItems(accountIds);
    }

    public override void beforeUpdate() {
        List<OrderItem> orderItems = (List<OrderItem>) Trigger.new;
        Map<Id, OrderItem> oldOrderItems = (Map<Id, OrderItem>) Trigger.oldMap;
        setProductFamily(orderItems, oldOrderItems);
        setDesignType(orderItems, oldOrderItems);
    }

    public override void afterUpdate() {
        List<OrderItem> orderItems = (List<OrderItem>) Trigger.new;
        Map<Id, OrderItem> oldOrderItems = (Map<Id, OrderItem>) Trigger.oldMap;
        Set<String> accountsRelated = getAccountsChanged(orderItems, oldOrderItems);
        OrderUtils.calculateAccountOrderItems(accountsRelated);
    }

    private Set<String> getAccountsChanged(List<OrderItem> orderItems, Map<Id, OrderItem> oldOrderItems) {
        Set<String> accountIds = new Set<String> ();
        for (OrderItem oi : orderItems) {
            OrderItem oldOi = oldOrderItems.get(oi.Id);
            if (oi.Account__c != oldOi.Account__c || oi.Quantity != oldOi.Quantity) {
                if (String.isNotBlank(oi.Account__c)) {
                    accountIds.add(oi.Account__c);
                }
                if (String.isNotBlank(oldOi.Account__c)) {
                    accountIds.add(oldOi.Account__c);
                }
            }
        }
        return accountIds;
    }

    private void setAccount(List<OrderItem> orderItems) {
        Set<String> orderIds = Utils.fetchSet(orderItems, 'OrderId');
        Map<Id, Order> orders = new Map<Id, Order> ([SELECT Id, AccountId FROM Order WHERE Id in :orderIds]);
        for (OrderItem oi : orderItems) {
            if (orders.containsKey(oi.OrderId)) {
                oi.Account__c = orders.get(oi.OrderId).AccountId;
            }
        }
    }

    private void setProductFamily(List<OrderItem> orderItems, Map<Id, OrderItem> oldOrderItems) {
        List<String> entriesIds = new List<String> ();
        for (OrderItem oi : orderItems) {
            if (oi.PricebookEntryId != null &&
                    (oldOrderItems == null || oldOrderItems.get(oi.Id).PricebookEntryId != oi.PricebookEntryId || String.isBlank(oi.Product_Family__c))) {
                entriesIds.add(oi.PricebookEntryId);
            }
        }

        if (entriesIds.size() > 0) {
            Map<Id, PricebookEntry> entries = new Map<Id, PricebookEntry> ([SELECT Id, Product2.Family, Product2.ProductCode FROM PricebookEntry WHERE Id in :entriesIds]);
            for (OrderItem oi : orderItems) {
                if (entries.containsKey(oi.PricebookEntryId)) {
                    oi.Product_Family__c = entries.get(oi.PricebookEntryId).Product2.Family;
                    oi.Product_Code__c = entries.get(oi.PricebookEntryId).Product2.ProductCode;
                }
            }

        }
    }

    private void setDesignType(List<OrderItem> orderItems, Map<Id, OrderItem> oldOrderItems) {
        List<String> designIDs = new List<String> ();
        for (OrderItem oi : orderItems) {
            if (oi.Design__c != null &&
                    (oldOrderItems == null || oldOrderItems.get(oi.Id).Design__c != oi.Design__c || String.isBlank(oi.Design_Type__c))) {
                designIDs.add(oi.Design__c);
            }
        }

        if (designIDs.size() > 0) {
            Map<Id, Design__c> designs = new Map<Id, Design__c> ([SELECT Id, Type__c FROM Design__c WHERE Id in :designIDs]);
            for (OrderItem oi : orderItems) {
                if (designs.containsKey(oi.Design__c)) {
                    system.debug(designs.get(oi.Design__c).Type__c);
                    oi.Design_Type__c = designs.get(oi.Design__c).Type__c;
                }
            }

        }
    }

}