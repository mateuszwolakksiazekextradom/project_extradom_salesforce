@IsTest
private class DesignResourcesViewExtensionTest {

    static testMethod void myUnitTest() {
        
        test.startTest();
        
        Account a = new Account(Name='Test');
        insert a;
        
        Contact c = new Contact(LastName='Test', AccountId=a.Id);
        insert c;
        
        Supplier__c s = new Supplier__c(Name='Test Supplier ABC', Full_Name__c='Test Supplier', Code__c='ABC');
        insert s;
        
        Design__c d = new Design__c(Name='Test Design', Full_Name__c='Test Design', Supplier__c=s.Id, Code__c='ABC1001', Status__c='Active');
        insert d;
        
        Task t = new Task(Type='Estimates Request', WhoId=c.Id, WhatId=d.Id, Status='Not Started');
        insert t;
        
        PageReference pageRef = new PageReference('http://extradom.pl/DesignResourcesView');
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', d.Id);
        ApexPages.currentPage().getParameters().put('tid', t.Id);
        ApexPages.currentPage().getHeaders().put('gaClientId', 'abc.def');
        ApexPages.currentPage().getHeaders().put('gaconnector_fc_source', 'google');
        ApexPages.currentPage().getHeaders().put('gaconnector_fc_medium', 'cpc');
        ApexPages.currentPage().getHeaders().put('gaconnector_fc_campaign', 'test_campaign');
        ApexPages.currentPage().getHeaders().put('gaconnector_fc_channel', 'Pay-Per-Click');
        ApexPages.currentPage().getHeaders().put('gaconnector_lc_source', 'bing');
        ApexPages.currentPage().getHeaders().put('gaconnector_lc_medium', 'organic');
        ApexPages.currentPage().getHeaders().put('gaconnector_lc_campaign', '-');
        ApexPages.currentPage().getHeaders().put('gaconnector_lc_channel', 'Organic');
        
        
        ApexPages.StandardController scp = new ApexPages.StandardController(d);
        DesignResourcesViewExtension ext = new DesignResourcesViewExtension(scp);
        ext.updateGaClientId();
        ext.getAddons();
        
        test.stopTest();
        
    }
}