public class OrderCreateBatch  implements Database.Batchable<sObject> {
    private static Set<String> productCodes = new Set<String> {
        	'Starter Pack', 'Investment Log', 'Investment Board', 'Garden Design',
            'Promotional Gift', 'Catalogue', 'Home Profit Card', 'Default Add On'
    };
    private Map<String, Id> ownerMap = new Map<String, Id> {
        'Administrator' => '005b0000000RxOPAA0',
        'Agnieszka Ł.' => '005b00000016HBiAAM',
        'Agnieszka M.' => '005b000000181InAAI',
        'Agnieszka W.' => '005b0000005Mez4AAC',
        'Aleksandra F.' => '005b00000018HncAAE',
        'Aleksandra R.' => '005b0000005PJNeAAO',
        'Aleksandra W.' => '005b0000002FSj9AAG',
        'Aneta B.' => '005b0000006yxpIAAQ',
        'Aneta N.' => '0050N0000075xSRQAY',
        'Anita S.' => '005b00000019LpuAAE',
        'Anna N.' => '005b0000000TlteAAC',
        'Barbara M.' => '005b0000003Q7WxAAK',
        'Dagmara J.' => '005b0000003RHDuAAO',
        'Dagmara M.' => '005b00000018W0GAAU',
        'Dagmara S.' => '005b0000003RHDuAAO',
        'Elwira S.' => '005b0000000SxoCAAS',
        'Ewa B.' => '0050N0000075xSlQAI',
        'Ewelina' => '005b0000001A0o6AAC',
        'Ewelina S.' => '005b0000001A0o6AAC',
        'Iwo J.' => '005b00000019mxrAAA',
        'Iwona L.' => '005b0000000Sxo2AAC',
        'Iwona R.' => '005b0000000SxoNAAS',
        'Izabela Z.' => '005b00000065DHPAA2',
        'Joanna J.' => '005b00000018GpDAAU',
        'Joanna K.' => '005b0000002FHJsAAO',
        'Joanna S' => '005b0000000SxnxAAC',
        'Joanna S.' => '005b0000000SxnxAAC',
        'Judyta R.' => '005b00000019LppAAE',
        'Karol W.' => '005b0000005gdeFAAQ',
        'Karolina' => '005b00000016ex1AAA',
        'Karolina B.' => '005b00000016ex1AAA',
        'Karolina L.' => '005b00000018eLqAAI',
        'Karolina S.' => '005b0000002JIoeAAG',
        'Katarzyna G.' => '005b0000000SI9YAAW',
        'Kinga O.' => '005b0000000SxoMAAS',
        'Klaudia L.' => '005b00000018pKAAAY',
        'Łukasz M.' => '005b0000005Pjh1AAC',
        'Magda' => '005b0000000RxOPAA0',
        'Magdalena O.' => '005b00000019VOfAAM',
        'Magdalena T.' => '005b00000063f5cAAA',
        'Malgorzata G.' => '005b0000000SxoHAAS',
        'Malwina K.' => '005b0000003RHDaAAO',
        'Małgorzata G.' => '005b0000000SxoHAAS',
        'Mariusz J' => '005b0000000RxOPAA0',
        'Mariusz J.' => '005b0000000RxOPAA0',
        'Marta W.' => '005b0000003PchuAAC',
        'Martyna K.' => '005b00000017pR8AAI',
        'Martyna Kucharska' => '005b00000017pR8AAI',
        'Martyna P.' => '005b00000019iwBAAQ',
        'Martyna S.' => '005b00000065DHFAA2',
        'Monika D.' => '005b00000017mINAAY',
        'Patrycja C.' => '005b00000019mNUAAY',
        'Paulina D.' => '005b00000065DH0AAM',
        'Paulina T.' => '0050N0000075xSgQAI',
        'Piotr L.' => '005b00000019fgPAAQ',
        'Przemysław G.' => '005b0000000U0ApAAK',
        'Ryszard B.' => '005b0000002FueIAAS',
        'Sylwia C.' => '005b00000016WLJAA2'
    };
    private Map<String, String> statusMap = new Map<String, String> {
        'Draft'	=> 'Draft', 'To Process' =>	'Accepted', 'Reserved' => 'Accepted',  'Waiting For Payment' =>	'Waiting for Payment',
		'Ordered' => 'Ordered', 'In Progress' => 'Ordered', 'Ready' => 'Ready', 'Sent' => 'Sent', 'To Return' => 'Sent',
		'Returned' => 'Sent', 'Canceled' =>'Canceled'
    };
    private Map<String, String> returnStatusMap = new Map<String, String> {
        'To Return' => 'Waiting for Shipment', 'Returned' => 'Returned'
    };
    public static Set<String> ids;
	public Database.querylocator start(Database.BatchableContext BC) {
        if (!Test.isRunningTest()) {
            ids = new Set<String> {'a020N000015Qd8b', 'a020N000015QdH9', 'a020N000015QdH4', 'a020N000015QdP9','a020N000015QdNh'};
        }
        List<String> statuses = new List<String> {'Sent', 'Returned', 'Canceled'};
        return Database.getQueryLocator([
                SELECT Id,Account__c,Add_Ons_Details__c,Add_Ons_Gross_Price__c,Add_ons_Price_No_HP__c,
                 Add_Ons_Summary__c,Add_Ons__c,Billing_Address__c,Billing_City__c,Billing_Company__c,Billing_Email__c,
                 Billing_First_Name__c,Billing_Last_Name__c,Billing_Name__c,Billing_Phone__c,Billing_Postal_Code__c,
            	 Billing_Tax_Number__c,Campaign_Medium__c,Campaign_Name__c,Design_Type__c,Design_Version__c,
                 Campaign_Source__c,Confirm_Agent__c,CreatedById,CreatedDate,Created_DateTime__c,
                 Design__c,Discount_Type__c,Discount__c,GCLID__c,Gross_Price__c,Gross_Shipping_Cost__c,Investment_Address__c,
                 Investment_City__c,Investment_Postal_Code__c,
                 Last_Account_Owner_Change__c,Name,Opt_in_Forward__c,Opt_in_Order__c,Opt_in_Regulations__c,Opt_in__c,
                 Order_Date__c,Order_Source__c,Partner_User__c,Pay_Id__c,Prev_Agent__c,Prev_ID__c,
                 Product__c,Quantity__c,Question_Design_Order_Estimate__c,Question_Has_Plan__c,Question_Start_of_Investment_Estimate__c,
                 RecordTypeId,Returned_Date__c,Sales_Agent_Alias__c,Sales_Agent__c,Sent_Date__c,Service_End_Date__c,Service_Start_Date__c,
                 Shipping_Address__c, Shipping_Phone__c,Shipping_Postal_Code__c,Source_Account__c,
                 Shipping_City__c,Shipping_Conditions__c,Shipping_First_Name__c,Shipping_Last_Name__c,Shipping_Method__c,Shipping_Name__c,
                 Source_URL__c,Special_Conditions__c,Status__c,Transfer_Account__c,Web_Email__c, Design__r.Code__c
                 FROM Order__c WHERE RecordType.DeveloperName != 'Product'
            		AND (NOT((Add_Ons__c = 'Modify Agreement' OR Add_Ons__c = 'Voucher' OR Add_Ons__c = 'Investment Estimate' OR Add_Ons__c = 'Modify Agreement; Investment Estimate')
                             and Gross_Total__c = 0))
            		AND (NOT(Add_Ons__c = null and Add_ons_Price_No_HP__c = 0 AND Gross_Total__c = 0))
            		AND (NOT(Design__c = null and Quantity__c >0 and Gross_Total__c > 0))
            		AND Id in :ids
            	ORDER BY Name ASC
        ]);
    }

    public void execute(Database.BatchableContext BC, List<sObject> scope) {
        List<Order__c> orders = (List<Order__c>) scope;
        Map<Id, Order__c> oldOrdersMap = new Map<Id, Order__c> (orders);
        Map<Id, Order> orderMap = new Map<Id, Order> ();
        Map<Id, List<OrderItem>> orderItemMap = new Map<Id, List<OrderItem>> ();
        Map<Id, Order> returnOrderMap = new Map<Id, Order> ();
        Map<Id, List<OrderItem>> returnOrderItemMap = new Map<Id, List<OrderItem>> ();
        String pricebookId = OrderUtils.getPricebookId(null);
        Set<String> designCodes = getCodes(orders);
        Set<String> allCodes = new Set<String> ();
        allCodes.addAll(designCodes);
        allCodes.addAll(productCodes);
        Map<String, PricebookEntry> pbEntries = OrderUtils.getProductsByCodes(allCodes);
        for (Order__c o : orders) {
            Order newOrder = getOrder(o, pricebookId, false);
            List<OrderItem> orderItems = getOrderItems(o, pbEntries);
            orderMap.put(o.Id, newOrder);
            orderItemMap.put(o.Id, orderItems);
        }
        insert orderMap.values();
        
        List<OrderItem> oisToInsert = new List<OrderItem> ();
        for (String orderId : orderItemMap.keySet()) {
            List<OrderItem> tempItems = orderItemMap.get(orderId);
            Order newO = orderMap.get(orderId);
            if (tempItems.size() == 0 && !Test.isRunningTest()) {
                system.debug(orderId);
                throw new OrderCreateBatchException(orderId);
            }
            for (OrderItem tempItem : tempItems) {
                tempItem.OrderId = newO.Id;
                oisToInsert.add(tempItem);
            }
        }
        insert oisToInsert;
        for (String oid : orderMap.keySet()) {
            Order__c oldO = oldOrdersMap.get(oId);
            if (statusMap.containsKey(oldO.Status__c)) {
                Order newO = orderMap.get(oId);
                newO.Status = statusMap.get(oldO.Status__c);
            }
        }
        update orderMap.values();
        
        for (Order__c o : orders) {
            if (returnStatusMap.containsKey(o.Status__c)) {
                Order reductionOrder = getOrder(o, pricebookId, true);
                reductionOrder.OriginalOrderId = orderMap.get(o.Id).Id;
                List<OrderItem> regularOrderItems = orderItemMap.get(o.Id);
                List<OrderItem> reductionOrderItems = new List<OrderItem> ();
                for (OrderItem oi : regularOrderItems) {
                    OrderItem roi = new OrderItem();
                    roi.OriginalOrderItemId = oi.Id;
                    Decimal quantity = -oi.Quantity;
                    roi.Quantity = quantity;
                    roi.Design__c = oi.Design__c;
                    roi.Version__c = oi.Version__c;
                    roi.PricebookEntryId = oi.PricebookEntryId;
					reductionOrderItems.add(roi);
                }
                returnOrderMap.put(o.Id, reductionOrder);
                returnOrderItemMap.put(o.Id, reductionOrderItems);
            }
        }
        insert returnOrderMap.values();
        
        List<OrderItem> oisReturnToInsert = new List<OrderItem> ();
        for (String orderId : returnOrderItemMap.keySet()) {
            List<OrderItem> tempItems = returnOrderItemMap.get(orderId);
            Order newO = returnOrderMap.get(orderId);
            for (OrderItem tempItem : tempItems) {
                tempItem.OrderId = newO.Id;
                oisReturnToInsert.add(tempItem);
            }
        }
        insert oisReturnToInsert;
        
        for (String oid : returnOrderMap.keySet()) {
            Order__c oldO = oldOrdersMap.get(oId);
            Order newO = returnOrderMap.get(oId);
            if (returnStatusMap.containsKey(oldO.Status__c)) {
                newO.Status = returnStatusMap.get(oldO.Status__c);
            }
        }
        update returnOrderMap.values();
    }
    
    private Set<String> getCodes(List<Order__c> orders) {
        Set<String> productCodes = new Set<String> ();
        for (Order__c o : orders) {
            if (String.isNotBlank(o.Design__r.Code__c)) {
                productCodes.add(o.Design__r.Code__c);
            }
        }
        return productCodes;
    }
    
    private Order getOrder(Order__c prevOrder, String pricebookId, Boolean isReduction) {
        Order o = new Order();
        o.OrderSource__c = prevOrder.Order_Source__c;
        o.Pricebook2Id = pricebookId;
        o.EffectiveDate = isReduction && prevOrder.Returned_Date__c != null && prevOrder.Returned_Date__c > prevOrder.CreatedDate.date() ? prevOrder.Returned_Date__c : prevOrder.CreatedDate.date(); // to clarify
        o.Status = ConstUtils.ORDER_DRAFT_STATUS; // to clarify
        o.BillingFirstName__c = prevOrder.Billing_First_Name__c;
        o.BillingLastName__c = prevOrder.Billing_Last_Name__c;
        o.BillingStreet = prevOrder.Billing_Address__c;
        o.BillingPostalCode = prevOrder.Billing_Postal_Code__c;
        o.BillingCity = String.isNotBlank(prevOrder.Billing_City__c) ? prevOrder.Billing_City__c.left(40) : null;
        o.BillingEmail__c = prevOrder.Billing_Email__c;
        o.BillingPhone__c = prevOrder.Billing_Phone__c;
        o.BillingCompany__c = prevOrder.Billing_Company__c;
        o.BillingTaxNumber__c = prevOrder.Billing_Tax_Number__c;
        o.Version__c = prevOrder.Design_Version__c;
        o.IsReductionOrder = isReduction;
        
        o.ShippingFirstName__c = prevOrder.Shipping_First_Name__c;
        o.ShippingLastName__c = prevOrder.Shipping_Last_Name__c;
        o.ShippingStreet = prevOrder.Shipping_Address__c;
        o.ShippingPostalCode = prevOrder.Shipping_Postal_Code__c;
        o.ShippingCity = String.isNotBlank(prevOrder.Shipping_City__c) ? prevOrder.Shipping_City__c.left(40) : null;
        o.ShippingPhone__c = prevOrder.Shipping_Phone__c;
        o.ShippingCompany__c = prevOrder.Shipping_Name__c;
        o.Shipping_Conditions__c = prevOrder.Shipping_Conditions__c;
        o.Payment_Method__c = getPaymentMethod(prevOrder.Shipping_Method__c);
        o.Shipping_Method__c = getShippingMethod(prevOrder.Shipping_Method__c);
        o.Shipping_Cost__c = prevOrder.Gross_Shipping_Cost__c;
        o.Version__c = prevOrder.Design_Version__c;
        o.AccountId = prevOrder.Account__c;
        o.Pay_Id__c = prevOrder.Pay_Id__c;
        o.Migration_Order__c = prevOrder.Id;
        o.RecordTypeId = isReduction ? RecordTypeManager.RecordTypeId('Order.Reduction') : RecordTypeManager.RecordTypeId('Order.Regular');
        o.Special_Conditions__c = prevOrder.Special_Conditions__c;
        o.OwnerId = ownerMap.containsKey(prevOrder.Sales_Agent__c) ? ownerMap.get(prevOrder.Sales_Agent__c) : '005b0000000RxOPAA0';
        o.Status = 'Draft';
        o.PoDate = isReduction && prevOrder.Returned_Date__c != null ? prevOrder.Returned_Date__c : prevOrder.Order_Date__c;
        o.Migration_Order_Number__c = prevOrder.Name;
        return o;
    }
    
    private List<OrderItem> getOrderItems(Order__c prevOrder, Map<String, PricebookEntry> pbEntries) {
        List<OrderItem> relatedItems = new List<OrderItem> ();
        if (prevOrder.Design__c != null && prevOrder.Quantity__c > 0 && prevOrder.Gross_Price__c > 0
            && pbEntries.containsKey(prevOrder.Design__r.Code__c)) {
            relatedItems.add(new OrderItem(
                Design__c = prevOrder.Design__c, Quantity = prevOrder.Quantity__c, Version__c = prevOrder.Design_Version__c,
                Changes_Agreement__c = prevOrder.Add_Ons_Details__c, PricebookEntryId = pbEntries.get(prevOrder.Design__r.Code__c).Id,
                List_Price__c = prevOrder.Gross_Price__c, 
                Discount_Sales__c = prevOrder.Discount__c != null && prevOrder.Gross_Price__c != null ? prevOrder.Discount__c * prevOrder.Gross_Price__c/100 : 0,
                UnitPrice = 0
            ));
        }
        If (String.isNotBlank(prevOrder.Add_Ons__c)) {
            List<String> codes = prevOrder.Add_Ons__c.split(';');
            for (String code : codes) {
                if (String.isNotBlank(code) && pbEntries.containsKey(code)) {
                    OrderItem oi = new OrderItem(
                        Quantity = 1, PricebookEntryId = pbEntries.get(code).Id,
                        List_Price__c = 0, Discount_Sales__c = 0, UnitPrice = 0
                    );
                    
                    if (code == 'Home Profit Card') {
                        oi.List_Price__c = 99;
                        oi.Changes_Agreement__c = prevOrder.Add_Ons_Details__c;
                    }
                    relatedItems.add(oi);
                }
            }
        }
        if (prevOrder.Add_ons_Price_No_HP__c > 0) {
            relatedItems.add(new OrderItem(
                Quantity = 1, PricebookEntryId = pbEntries.get('Default Add On').Id,
                List_Price__c = prevOrder.Add_ons_Price_No_HP__c, Discount_Sales__c = 0,
                Design__c = prevOrder.Design__c, UnitPrice = prevOrder.Add_ons_Price_No_HP__c
            ));
        }
        return relatedItems;
    }
    
    private String getPaymentMethod(String method) {
        String paymentMethod = null;
        if (String.isNotBlank(method)) {
            List<String> shipping = method.split(' ');
            if (shipping.size() == 2) {
                String payMethod = shipping.get(1).trim().replace('(', '').replace(')', '');
                if (String.isNotBlank(payMethod)) {
                    paymentMethod = payMethod;
                }
            }
        }
        return paymentMethod;
    }
    
    private String getShippingMethod(String method) {
        String shippingMethod = null;
        if (String.isNotBlank(method)) {
            List<String> shipping = method.split(' ');
            if (shipping.size() == 2) {
                String shipMethod = shipping.get(0).trim();
                if (String.isNotBlank(shipMethod)) {
                    shippingMethod = shipMethod;
                }
            }
        }
        return shippingMethod;
    }

    public void finish(Database.BatchableContext BC) {

    } 
    
    public class OrderCreateBatchException extends Exception { }
}