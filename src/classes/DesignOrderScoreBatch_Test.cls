@IsTest
private class DesignOrderScoreBatch_Test {
    private static final Integer NUM_OF_DESIGNS = 100;

    @IsTest
    private static void shouldSetOrderScoreDesigns() {
        // given
        Supplier__c newSupplier = new Supplier__c(Name = 'Supplier', Code__c = '11', Full_Name__c = 'Great Supplier');
        insert newSupplier;
        final String DESIGN_TO_PROCESS_PREFIX = 'Design To Process ';
        final String DESIGN_NOT_TO_PROCESS_PREFIX = 'Design Not To Process ';
        List<Design__c> designs = new List<Design__c> ();
        for (Integer i = 0; i < NUM_OF_DESIGNS; i++) {
            designs.add(
                    new Design__c (
                            Name = DESIGN_TO_PROCESS_PREFIX + String.valueOf(i), Supplier__c = newSupplier.Id, Full_Name__c = DESIGN_TO_PROCESS_PREFIX + String.valueOf(i),
                            Type__c = ConstUtils.DESIGN_HOUSE_TYPE, Order_Score__c = null, Status__c = ConstUtils.DESIGN_ACTIVE_STATUS
                    )
            );
        }
        for (Integer i = 0; i < NUM_OF_DESIGNS; i++) {
            designs.add(
                    new Design__c (
                            Name = DESIGN_NOT_TO_PROCESS_PREFIX + String.valueOf(i), Supplier__c = newSupplier.Id, Full_Name__c = DESIGN_NOT_TO_PROCESS_PREFIX + String.valueOf(i),
                            Type__c = ConstUtils.DESIGN_HOUSE_TYPE, Order_Score__c = null, Status__c = ConstUtils.DESIGN_DRAFT_STATUS
                    )
            );
        }
        insert designs;

        // when
        Test.startTest();
        Database.executeBatch(new DesignOrderScoreBatch());
        Test.stopTest();
        List<Design__c> designsProcessed = [SELECT Id, Order_Score__c, Page_Score__c, Name FROM Design__c];

        // then
        System.assertEquals(NUM_OF_DESIGNS*2, designs.size());
        for (Design__c design : designsProcessed) {
            if (design.Name.startsWith(DESIGN_TO_PROCESS_PREFIX)) {
                System.assertEquals(design.Page_Score__c, design.Order_Score__c);
            } else if (design.Name.startsWith(DESIGN_NOT_TO_PROCESS_PREFIX)) {
                System.assertEquals(null, design.Order_Score__c);
                System.assertNotEquals(design.Page_Score__c, design.Order_Score__c);
            }
        }
    }
}