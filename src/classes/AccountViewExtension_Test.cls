@isTest
private class AccountViewExtension_Test {
    private static testMethod void testView() {
        Account a = new Account();
        a.Name = 'Test';
        a.Transfer_Email__c = 'test@extradom.pl';
        insert a;
        
        Order o = new Order(AccountId=a.Id, EffectiveDate=System.today(), Status='Draft');
        insert o;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(a);
        AccountViewExtension ext = new AccountViewExtension(sc);
        List<Order> orders = ext.getOrdersWithOrderItems();
        System.assertEquals(orders.size(),1);
    
    }
}