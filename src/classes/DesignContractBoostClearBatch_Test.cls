/**
 * Created by grzegorz.dlugosz on 20.05.2019.
 */
@isTest
private class DesignContractBoostClearBatch_Test {

    @isTest static void clearBoostOnMultipleDesigns() {
        // Prepare Test Data
        Supplier__c supplier = TestUtils.newSupplier(new Supplier__c(), true);

        List<Design__c> designList = TestUtils.newDesigns(new Design__c(Supplier__c = supplier.Id, Featured__c = 1, Gross_Price__c = 1000, Views_Month__c = 100,
                Pending_Promo_Price__c = 1000, Promo_Price__c = 1000, VAT_Rate__c = 0.23, Contract_Boost__c = 1.00009), 5, true);

        for (Design__c design : designList) {
            System.assert(design.Contract_Boost__c != 0);
        }

        Test.startTest();
        DesignContractBoostClearBatch btch = new DesignContractBoostClearBatch();
        Database.executeBatch(btch, 100);
        Test.stopTest();

        List<Design__c> designAfterUpdateList = [SELECT Contract_Boost__c FROM Design__c];

        for (Design__c design : designAfterUpdateList) {
            System.assertEquals(null, design.Contract_Boost__c);
        }
    }
}