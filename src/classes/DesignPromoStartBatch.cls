public with sharing class DesignPromoStartBatch implements Database.Batchable<sObject> {
    public Database.querylocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
                SELECT Id, Pending_Promo_Name__c, Pending_Promo_Start_Date__c, Pending_Promo_End_Date__c, Pending_Promo_Price__c,
                        Promo_Name__c, Promo_Price__c, Promo_Price_End_Date__c, Promo_Price_Inc__c
                FROM Design__c
                WHERE Pending_Promo_Start_Date__c <= :system.now()
        ]);
    }

    public void execute(Database.BatchableContext BC, List<sObject> scope) {
        List<Design__c> designs = (List<Design__c> ) scope;
        for (Design__c design : designs) {
            design.Promo_Price__c = design.Pending_Promo_Price__c;
            design.Promo_Price_Inc__c = design.Pending_Promo_Price__c;
            design.Promo_Name__c = design.Pending_Promo_Name__c;            
            design.Promo_Price_End_Date__c = design.Pending_Promo_End_Date__c;
            design.Pending_Promo_Start_Date__c = null;
            design.Pending_Promo_Price__c = null;
            design.Pending_Promo_Name__c = null;
            design.Pending_Promo_End_Date__c = null;
        }
        DesignTriggerHandler.BYPASS_INVALIDATE_DESIGNS = true;
        update designs;
        DesignTriggerHandler.BYPASS_INVALIDATE_DESIGNS = false;
    }

    public void finish(Database.BatchableContext BC) {
        if (!Test.isRunningTest()) {
            Database.executeBatch(new DesignPromoEndBatch());
        }
    }
}