public class NotusInvocable {

    @InvocableMethod(label='Notus Callout' description='Enqueue callouts to Notus REST')
    public static void updateTelemarketing(List<Telemarketing__c> telemarketings) {
        System.enqueueJob(new NotusTools(telemarketings));
    }
}