@RestResource(urlMapping='/app/models/*')
global with sharing class AppModelsObj {
    @HttpGet
    global static ExtradomApi.Model doGet() {
        RestRequest req = RestContext.request;
        Pattern pt = Pattern.compile('\\/app\\/models\\/(.{18})');
        Matcher mt = pt.matcher(req.requestURI);
        mt.find();
        ID modelId = mt.group(1);
        Model__c m;
        UserRecordAccess ura;
        try {
            ura = [SELECT RecordId, MaxAccessLevel from UserRecordAccess where RecordId = :modelId and UserId =: UserInfo.getUserId()];
        } catch (Exception e) {
            RestContext.response.statusCode = 404; // NOT_FOUND
            return null;
        }
        if (ura.MaxAccessLevel == 'None') {
            if (UserInfo.getUserType()=='Guest') {
                RestContext.response.statusCode = 401; // UNAUTHORIZED
                return null;
            } else {
                RestContext.response.statusCode = 403; // FORBIDDEN
                return null;
            }
        }
        if (UserInfo.getUserType()=='Guest') {
            RestContext.response.addHeader('Cache-Control', 'must-revalidate,max-age=0,s-maxage=3600');
        } else {
            RestContext.response.addHeader('Cache-Control', 'no-cache,must-revalidate,max-age=0,no-store,private,s-maxage=0');
        }
        ExtradomApi.Model model;
        if (modelId.getSObjectType()==Schema.Model__c.SObjectType) {
            m = [SELECT Id, Name, Mirrored__c, Design__r.Id, Design__r.Code__c, Design__r.Full_Name__c, Design__r.Type__c, Design__r.Canonical_Full_URL__c, Design__r.Usable_Area__c, Design__r.Minimum_Plot_Size_Horizontal__c, Design__r.Roof_slope__c, Design__r.Height__c, Design__r.Footprint__c, Design__r.Capacity__c, Design__r.Roof_Cover_Area__c, Design__r.EUco__c, Design__r.Livestock_Unit__c, Design__r.Thumbnail_Base_URL__c, Design__r.Sketchfab_ID__c, Design__r.KW_Model__c, Design__r.Gross_Price__c, Design__r.Current_Price__c, (SELECT Id, Name, ParentId, Description, CreatedDate, LastModifiedDate FROM Attachments ORDER BY CreatedDate DESC) FROM Model__c WHERE Id =: modelId];
        } else if (modelId.getSObjectType()==Schema.Design__c.SObjectType) {
            Design__c d = [SELECT Id, UserRecordAccess.MaxAccessLevel, Code__c, Full_Name__c, Type__c, Canonical_Full_URL__c, Usable_Area__c, Minimum_Plot_Size_Horizontal__c, Roof_slope__c, Height__c, Footprint__c, Capacity__c, Roof_Cover_Area__c, EUco__c, Livestock_Unit__c, Thumbnail_Base_URL__c, Sketchfab_ID__c, KW_Model__c, Gross_Price__c, Current_Price__c FROM Design__c WHERE Id =: modelId];
            model = new ExtradomApi.Model(d);
            model.maxAccessLevel = ura.MaxAccessLevel;
            return model;
        }
        model = new ExtradomApi.Model(m);
        model.maxAccessLevel = ura.MaxAccessLevel;
        return model;
    }
    @HttpPost
    global static ExtradomApi.ModelConfigurationResult doPost() {
        RestRequest req = RestContext.request;
        ExtradomApi.ModelConfigurationInput input = (ExtradomApi.ModelConfigurationInput)JSON.deserialize(req.requestBody.toString(), ExtradomApi.ModelConfigurationInput.class);
        Pattern pt = Pattern.compile('\\/app\\/models\\/(.{18})');
        Matcher mt = pt.matcher(req.requestURI);
        mt.find();
        ID modelId = mt.group(1);
        UserRecordAccess ura;
        try {
            ura = [SELECT RecordId, MaxAccessLevel from UserRecordAccess where RecordId = :modelId and UserId =: UserInfo.getUserId()];
        } catch (Exception e) {
            RestContext.response.statusCode = 404; // NOT_FOUND
            return null;
        }
        if (ura.MaxAccessLevel == 'None') {
            if (UserInfo.getUserType()=='Guest') {
                RestContext.response.statusCode = 401; // UNAUTHORIZED
                return null;
            } else {
                RestContext.response.statusCode = 403; // FORBIDDEN
                return null;
            }
        } else if (ura.MaxAccessLevel == 'Read') {
            RestContext.response.statusCode = 403; // FORBIDDEN
            return null;
        }
        Model__c m = [SELECT Id FROM Model__c WHERE Id =: modelId];
        Attachment att = new Attachment(ParentId=m.Id,Name=input.name,Description=input.version,Body=Blob.valueOf(input.body));
        insert att;
        try {
            Pattern ptThumb = Pattern.compile('data:(image\\/(jpg|jpeg|png));base64,(.+)');
            Matcher mtThumb = ptThumb.matcher(input.thumbnail);
            mtThumb.find();
            String contentType = mtThumb.group(1);
            String base64Body = mtThumb.group(3);
            AWSTools.uploadFileBase64('extradom.media', UserInfo.getOrganizationId()+'-'+att.id+'-thumb/source', base64Body, contentType, 'AKIAJO25LHDRMTH7SLBA', 'zjzdMrMfTi3i26wIqHEKAwvZkjFBN0qJmGE4sH8n');
        } catch(Exception e) {
        }
        Attachment att2 = [SELECT Id, Name, ParentId, Description, CreatedDate, LastModifiedDate FROM Attachment WHERE Id =: att.Id AND ParentId =: m.Id];
        RestContext.response.addHeader('Cache-Control', 'no-cache,must-revalidate,max-age=0,no-store,private,s-maxage=0');
        return new ExtradomApi.ModelConfigurationResult(att2);
    }
    public class ApiException extends Exception {}
}