@RestResource(urlMapping='/app/users/me/notifications')
global without sharing class AppUsersMeNotifications {
    @HttpGet
    global static ExtradomApi.Notifications doGet() {
        return new ExtradomApi.Notifications([SELECT Id, PreferencesDisableAllFeedsEmail FROM NetworkMember WHERE NetworkId != null AND MemberId = :UserInfo.getUserId() LIMIT 1]);
    }
    @HttpPatch
    global static Map<String,String> doPatch(Boolean communityEmails) {
    
        RestContext.response.addHeader('Cache-Control', 'no-cache, must-revalidate, max-age=0, no-store, private, s-maxage=0');
        RestContext.response.statusCode = 204;
        
        if (communityEmails != null) {
            NetworkMember member = [SELECT Id, PreferencesDisableAllFeedsEmail FROM NetworkMember WHERE NetworkId != null AND MemberId = :UserInfo.getUserId() LIMIT 1];
            member.PreferencesDisableAllFeedsEmail = !communityEmails;
            update member;
        }
        
        return null;
        
    }
}