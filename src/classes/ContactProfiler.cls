global class ContactProfiler {
    
    @future
    global static void processContact(Id contactId) {
    }
    
    global static void processContacts(List<Contact> contactsToProcess) {
    }
    
    @future
    global static void processDesign(Id designId) {
    }
    
    @future
    global static void processDesigns(List<ID> designIds) {
    }
    
    global static Boolean compareDesignWithPage(Design__c d, Page__c p) {
        return true;
    }
    
    global static Boolean compareAnyKey(String categories, String str_keys) {
        if (String.isBlank(str_keys)) {
            return true;
        } else {
            for (String key: str_keys.split(',')) {
                if (categories.contains('['+key+']')) {
                    return true;
                }
            }
        }
        return false;
    }
    
    global static Boolean compareAllKey(String categories, String categories2) {
        if (String.isBlank(categories2)) {
            return true;
        } else {
            for (String key: categories2.replace('][', ',').replace(']', '').replace('[', '').split(',')) {
                if (!categories.contains('['+key+']')) {
                    return false;
                }
            }
            return true;
        }
        return true;
    }
    
    @future
    global static void processPagesWithDesigns(List<ID> pageIds) {
        
        List<Design__c> designs = [SELECT Name, Sent_Month__c, Views_Month__c, Total_Profit__c, Category_Keys__c, Usable_Area__c, Height__c, Minimum_Plot_Size_Horizontal__c, Footprint__c FROM Design__c WHERE Status__c = 'Active'];
        List<Page__c> pages = [SELECT Main_Category_Key__c, Building_Cost_Key__c, Technology_Key__c, Style_Key__c, Living_Type_Key__c, Add_Ons_Key__c, Roof_Key__c, Basement_Key__c, Levels_Keys__c, Garage_Keys__c, Usable_Area_min__c, Usable_Area_max__c, Search_Text__c, Plot_H_min__c, Footprint_max__c, Height_max__c, Category_Keys__c FROM Page__c WHERE Id IN :pageIds];
        
        for (Page__c p: pages) {
            Integer count = 0;
            Double sent = 0.0;
            Double views = 0.0;
            Double profit = 0.0;
            for (Design__c d: designs) {
                if (compareDesignWithPage(d, p)) {
                    count++;
                    if (d.Sent_Month__c > 0.0) {
                        sent += d.sent_month__c;
                        if (d.Total_Profit__c > 0.0) {
                            profit += d.Sent_Month__c*d.total_profit__c;
                        }
                    }
                    if (d.Views_Month__c > 0.0) {
                        views += d.views_month__c;
                    }
                }
            }
            p.Designs_Count_Active__c = count;
            p.Designs_Views_Month__c = views;
            p.Designs_Sent_Month__c = sent;
            p.Designs_Profit_Month__c = profit;
        }
        
        update pages;
        
    }

}