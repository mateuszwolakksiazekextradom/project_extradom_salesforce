public with sharing class ReductionOrderProductsController {
	
    @AuraEnabled
    public static List<ReductionOrderItemWrapper> getReductionOrderItems (String orderId) {
        List<ReductionOrderItemWrapper> orderitems = new List<ReductionOrderItemWrapper> ();
        Order reductionOrder = getReductionOrder(orderId);
        if (reductionOrder != null && String.isNotBlank(reductionOrder.OriginalOrderId)) {
            Order regularOrder = getRegularOrder(reductionOrder.OriginalOrderId);
            Map<Id, OrderItem> reductionOrderItems = getReductionOrderItemsMap(reductionOrder.OrderItems);
            if (regularOrder != null && regularOrder.OrderItems != null && regularOrder.OrderItems.size() > 0) {
                for (OrderItem oi : regularOrder.OrderItems) {
                    if (reductionOrder.StatusCode == ConstUtils.ORDER_ACTIVATED_STATUS_CODE && !reductionOrderItems.containsKey(oi.Id)) {
                        continue;
                    }
                    orderitems.add(
                        new ReductionOrderItemWrapper (reductionOrder, oi, reductionOrderItems.containsKey(oi.Id) ? reductionOrderItems.get(oi.Id) : null)
                    );
                }
            }
        }
        return orderItems;
    }
    
	@AuraEnabled
    public static String saveReductionOrderItems(String orderId, String reductionOrderItems) {
        Savepoint sp = Database.setSavepoint();
        try {
            List<OrderItem> orderItems = (List<OrderItem>) JSON.deserialize(reductionOrderItems, List<OrderItem>.class);
            Order o = getReductionOrder(orderId);
            if (o != null) {
                Map<Id, OrderItem> existingOIs = OrderUtils.getExistingOrderItems(orderItems);
                List<OrderItem> newOIs = OrderUtils.getNewOrderItems(orderItems);
                List<OrderItem> OIsToDelete = OrderUtils.getDeleteOrderItems(o.OrderItems, existingOIs);

                if (OIsToDelete.size() > 0) {
                    delete OIsToDelete;
                }
                if (existingOIs.size() > 0) {
                    update existingOIs.values();
                }
                if (newOIs.size() > 0) {
                    insert newOIs;
                }

                if (o.Status == ConstUtils.ORDER_DRAFT_STATUS &&
                        (OIsToDelete.size() > 0 || newOIs.size() > 0 || checkOrderItemsChange(o.OrderItems, existingOIs))) {
                    Order oToUpdate = new Order (Id = o.Id, Status = ConstUtils.ORDER_WAITING_FOR_SHIPMENT_STATUS);
                    update oToUpdate;
                }
            }
        } catch (Exception e) {
            Database.rollback(sp);
            return e.getMessage();
        }
        return null;
    }

    private static Boolean checkOrderItemsChange(List<OrderItem> existingOIs, Map<Id, OrderItem> updatedOIs) {
        Boolean changed = false;
        if (existingOIs != null && existingOIs.size() > 0 && updatedOIs.size() > 0) {
            for (OrderItem oiExisting : existingOIs) {
                if (updatedOIs.containsKey(oiExisting.Id)) {
                    OrderItem oiUpdated = updatedOIs.get(oiExisting.Id);
                    if (oiUpdated.Quantity != oiExisting.Quantity) {
                        changed = true;
                        break;
                    }
                }
            }
        }
        return changed;
    }

    private static Order getReductionOrder (String orderId) {
        Order reductionOrder = null;
        if (String.isNotBlank(orderId)) {
            List<Order> orders = [
                SELECT Id, OriginalOrderId, StatusCode, Status, (SELECT Id, Quantity, OriginalOrderItemId  FROM OrderItems)
                FROM Order WHERE Id = :orderId
            ];
            if (orders.size() > 0 ) {
                reductionOrder = orders.get(0);
            }
        }
        return reductionOrder;
    }
    
    private static Order getRegularOrder (String orderId) {
        Order regularOrder = null;
        if (String.isNotBlank(orderId)) {
            List<Order> orders = [
                SELECT Id,
                	(SELECT Id, AvailableQuantity, PricebookEntryId, Product2.Id, Product2.Name, Design__r.Name, UnitPrice FROM OrderItems)
                FROM Order WHERE Id = :orderId];
            if (orders.size() > 0 ) {
                regularOrder = orders.get(0);
            }
        }
        return regularOrder;
    }
    
    private static Map<Id, OrderItem> getReductionOrderItemsMap (List<OrderItem> reductionOrderItems) {
        Map<Id, OrderItem> orderItems = new Map<Id, OrderItem> ();
        if (reductionOrderItems != null && reductionOrderItems.size() > 0) {
            for (OrderItem oi : reductionOrderItems) {
                orderItems.put(oi.OriginalOrderItemId, oi);
            }
        }
        return orderItems;
    }
}