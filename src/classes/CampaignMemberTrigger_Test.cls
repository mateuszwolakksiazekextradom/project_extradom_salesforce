/**
 * Created by grzegorz.dlugosz on 21.05.2019.
 */

@isTest
private class CampaignMemberTrigger_Test {

    // PKO Campaign exists, response was 200 - ok, IDMultiFormData__c should be updated
    @isTest
    private static void sendMessageToTotalMoneySuccess() {
        Account acc = TestUtils.newAccount(new Account(), true);
        Contact cnt = TestUtils.newContact(new Contact(AccountId = acc.Id), true);
        Campaign cmp = TestUtils.newCampaign(new Campaign(Name='PKO'), true);

        Campaign_Settings__c cs = new Campaign_Settings__c(SetupOwnerId = UserInfo.getOrganizationId(), PKO_Campaign_Id__c = cmp.Id);
        insert cs;

        Test.setMock(HttpCalloutMock.class, new TotalMoneySuccessMockImpl());

        Test.startTest();
        CampaignMember cmpMemb = TestUtils.newCampaignMember(new CampaignMember(ContactId = cnt.Id, CampaignId = cmp.Id, First_Name__c = 'TestFirstName1',
                                                                                Last_Name__c = 'TestLastName1', Email__c = 'testEmailtest1@testcode.com',
                                                                                Phone__c = '123498744', Postal_Code__c = '11-121'), true);
        Test.stopTest();

        List<CampaignMember> cmpMembUpdatedList = [SELECT Id, IDMultiFormData__c FROM CampaignMember WHERE Id = :cmpMemb.Id];

        if (!cmpMembUpdatedList.isEmpty()) {
            System.assertEquals('6433901', cmpMembUpdatedList.get(0).IDMultiFormData__c);
        }
    }

    // PKO campaign does not exist, IDMultiFormData__c should be null
    @isTest
    private static void campaignMemberNoPkoSetting() {
        Account acc = TestUtils.newAccount(new Account(), true);
        Contact cnt = TestUtils.newContact(new Contact(AccountId = acc.Id), true);
        Campaign cmp = TestUtils.newCampaign(new Campaign(), true);

        Test.setMock(HttpCalloutMock.class, new TotalMoneySuccessMockImpl());

        Test.startTest();
        CampaignMember cmpMemb = TestUtils.newCampaignMember(new CampaignMember(ContactId = cnt.Id, CampaignId = cmp.Id, First_Name__c = 'TestFirstName1',
                Last_Name__c = 'TestLastName1', Email__c = 'testEmailtest1@testcode.com',
                Phone__c = '123498744', Postal_Code__c = '11-121'), true);
        Test.stopTest();

        List<CampaignMember> cmpMembUpdatedList = [SELECT Id, IDMultiFormData__c FROM CampaignMember WHERE Id = :cmpMemb.Id];

        if (!cmpMembUpdatedList.isEmpty()) {
            System.assertEquals(null, cmpMembUpdatedList.get(0).IDMultiFormData__c);
        }
    }

    // Campaign Member for different Campaign than PKO, IDMultiFormData__c should be null
    @isTest
    private static void campaignMemberDifferentCampaign() {
        Account acc = TestUtils.newAccount(new Account(), true);
        Contact cnt = TestUtils.newContact(new Contact(AccountId = acc.Id), true);
        Campaign cmp = TestUtils.newCampaign(new Campaign(), true);

        Test.setMock(HttpCalloutMock.class, new TotalMoneySuccessMockImpl());

        Test.startTest();
        CampaignMember cmpMemb = TestUtils.newCampaignMember(new CampaignMember(ContactId = cnt.Id, CampaignId = cmp.Id, First_Name__c = 'TestFirstName1',
                Last_Name__c = 'TestLastName1', Email__c = 'testEmailtest1@testcode.com',
                Phone__c = '123498744', Postal_Code__c = '11-121'), true);
        Test.stopTest();

        List<CampaignMember> cmpMembUpdatedList = [SELECT Id, IDMultiFormData__c FROM CampaignMember WHERE Id = :cmpMemb.Id];

        if (!cmpMembUpdatedList.isEmpty()) {
            System.assertEquals(null, cmpMembUpdatedList.get(0).IDMultiFormData__c);
        }
    }

    // PKO campaign does not exist, IDMultiFormData__c should be null
    @isTest
    private static void sendMessageToTotalMoneyFailure() {
        Account acc = TestUtils.newAccount(new Account(), true);
        Contact cnt = TestUtils.newContact(new Contact(AccountId = acc.Id), true);
        List<Campaign> cmpList = TestUtils.newCampaigns(new Campaign(), 2, true);

        Campaign_Settings__c cs = new Campaign_Settings__c(SetupOwnerId = UserInfo.getOrganizationId(), PKO_Campaign_Id__c = cmpList.get(0).Id);
        insert cs;

        Test.setMock(HttpCalloutMock.class, new TotalMoneyFailureMockImpl());

        Test.startTest();
        CampaignMember cmpMemb = TestUtils.newCampaignMember(new CampaignMember(ContactId = cnt.Id, CampaignId = cmpList.get(1).Id, First_Name__c = 'TestFirstName1',
                Last_Name__c = 'TestLastName1', Email__c = 'testEmailtest1@testcode.com',
                Phone__c = '123498744', Postal_Code__c = '11-121'), true);
        Test.stopTest();

        List<CampaignMember> cmpMembUpdatedList = [SELECT Id, IDMultiFormData__c FROM CampaignMember WHERE Id = :cmpMemb.Id];
        System.assertEquals(cmpMembUpdatedList.size(), 1);
        System.assertEquals(null, cmpMembUpdatedList.get(0).IDMultiFormData__c);
    }

    public class TotalMoneySuccessMockImpl implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest req) {
            HttpResponse res = new HttpResponse();
            // body is minified version of real response for 4 designs; nextPageToken is removed so the queueable won't try to chain more jobs
            String respBody = '{"status":"ok","msg":{"IDMultiForm":19,"IDMultiFormData":6433901,"Offerers":{"300":{"IDMultiFormOfferer":"300",';
            respBody += '"IDOfferer":"30","IDSaleChannel":"57","IDApplication":39908124}},"IDDistributor":266299,"formName":"extradom-pl","utm_source":"s_m2m_widget_dm"}}';
            res.setHeader('Content-Type', 'application/json');
            res.setBody(respBody);
            res.setStatusCode(200);

            return res;
        }
    }

    public class TotalMoneyFailureMockImpl implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest req) {
            HttpResponse res = new HttpResponse();
            // body is minified version of real response for 4 designs; nextPageToken is removed so the queueable won't try to chain more jobs
            String respBody = '{"status":"error","msg":"Unable to create application"}';
            res.setHeader('Content-Type', 'application/json');
            res.setBody(respBody);
            res.setStatusCode(400);

            return res;
        }
    }
}