@isTest
private class mediaAfterCreateAfterDeleteTest {
    static testMethod void myUnitTest() {
        
        Supplier__c s = new Supplier__c (Name='Test',Full_Name__c='Test TST',Code__c='TST');
        insert s;
        Design__c d = new Design__c (Name='Test TST1001',Full_name__c='Test',Supplier__c=s.Id);
        insert d;       
        List<Media__c> ms = new List<Media__c>{new Media__c(Design__c=d.Id,Media_Name_ID__c='test1',Category__c='Document PDF',Label__c='Kosztorys 1'),
            new Media__c(Design__c=d.Id,Media_Name_ID__c='test2',Category__c='',Label__c='Kosztorys2'),
            new Media__c(Design__c=d.Id,Media_Name_ID__c='test3',Category__c='Document PDF',Label__c='Test'),
            new Media__c(Design__c=d.Id,Media_Name_ID__c='test4',Category__c='3D Rotation',Label__c='Test')};
        
        insert ms;
        
        Design__c afterInsertDesign = [SELECT Id, Documents__c, Documents_estimates__c, Name FROM Design__c WHERE Id = :d.Id];
        Media__c m1 = [SELECT Id, Label__c, Document_URL__c, Design__c FROM Media__c WHERE Id = :ms.get(0).Id];
        Media__c m2 = [SELECT Id, Label__c, Document_URL__c, Design__c FROM Media__c WHERE Id = :ms.get(2).Id];
        
        delete m1;
    }
}