/**
 * Created by mateusz.wolak on 05.06.2019.
 */

@isTest
private class UserTriggerHandler_Test {

    @isTest
    private static void assignPermissionSet_test() {
        User usr = null, runAs = TestUtils.prepareTestUser(false, ConstUtils.USER_ADMIN_PROFILE, ConstUtils.USER_COK_DEPARTMENT);

        //account owner must have role assigned
        runAs.UserRoleId = [SELECT Id, name FROM UserRole WHERE Name =: ConstUtils.USER_DESIGN_SALES_AGENT_ROLE].Id;
        insert runAs;
       
        Test.startTest();
            //run as a user who has assigned role
            System.runAs(runAs) {

                Contact con = TestUtils.newContact(new Contact(), true);

                usr = TestUtils.prepareTestUser(false, ConstUtils.USER_PROFILE_SUPPLIERS_CC_USER, ConstUtils.USER_COK_DEPARTMENT);
                usr.Username = 'exp' + usr.Username;
                usr.CommunityNickname = 'exp' + usr.CommunityNickname;
                usr.CompanyName = 'Sample name';
                usr.ContactId = con.Id;

                insert usr;
            
            }

        Test.stopTest();

        //check if right permission set was assigned 
        List<Id> psIds = new List<Id>();
        for(PermissionSetAssignment psa : [SELECT PermissionSetId FROM PermissionSetAssignment WHERE AssigneeId =: usr.Id]) {
            psIds.add(psa.PermissionSetId);
        } 

        Map<Id, PermissionSet> permissionSetMap = new Map<Id, PermissionSet>([SELECT Id, Name FROM PermissionSet WHERE Id IN: psIds]);

        System.assert(permissionSetMap.containsKey(ConstUtils.getPermissionSetId(ConstUtils.USER_PERMISSION_SET_SUPPLIERS_COMMUNITY_MEMBER)));
    }

}