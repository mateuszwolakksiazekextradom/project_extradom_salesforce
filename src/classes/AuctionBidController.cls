/**
 * Created by grzegorz.dlugosz on 22.05.2019.
 */
public without sharing class AuctionBidController {

    /**
     * @author Grzegorz Długosz
     * @description Creates new Bid record under given auction / offer__c
     * @param auctionId
     *          Id of the Offer__c record
     * @return Either String = 'SUCCESS' or throws Exception
     */
    @AuraEnabled
    public static String addSecondBidSrv(Id auctionId) {
        String result = 'SUCCESS';
        Bid__c bidToCreate = new Bid__c(Offer__c = auctionId, OwnerId = UserInfo.getUserId());

        try {
            insert bidToCreate;
        } catch (Exception e) {
            String errorMessage = 'Something went wrong while creating the bid record: ' + e.getMessage();
            AuraHandledException ex = new AuraHandledException(errorMessage);
            ex.setMessage(errorMessage);
            throw ex;
        }

        return result;
    }

    /**
     * @author Grzegorz Długosz
     * @description Query the bid record of current user (owner) that is under requested offer. If there is no such record, then return null.
     * @param auctionId
     *          Id of the Offer__c record
     * @return - Wrapper containing :
     * - bidList (list of bids of current user under current auction, sorted by created date),
     * - auctionEnd (time when auction ends)
     * - currentTime (current time)
     */
    @AuraEnabled
    public static UserBidsWrapper getUserBidsSrv(Id auctionId) {
        UserBidsWrapper wrapper;

        List<Bid__c> bidList = [
                SELECT Value__c, OwnerId, Offer__r.Minimum_Value__c, Offer__r.End_Date__c
                FROM Bid__c
                WHERE Offer__c = :auctionId AND OwnerId = :UserInfo.getUserId()
                ORDER BY CreatedDate
                LIMIT 2
        ];

        if (bidList != null && !bidList.isEmpty()) {
            // DateTime in contextUser locale
            Datetime endDate = bidList.get(0).Offer__r.End_Date__c;
            DateTime endDateGmt = Datetime.newInstanceGmt(endDate.year(), endDate.month(), endDate.day(), endDate.hour(), endDate.minute(), endDate.second());

            wrapper = new UserBidsWrapper(bidList, endDate, DateTime.now());
        } else {
            // if user doesn't have bids, at least query the auction and set countdown
            List<Offer__c> offerList = [
                SELECT End_Date__c
                FROM Offer__c
                WHERE Id = :auctionId
            ];
            // DateTime in contextUser locale
            if (offerList != null && !offerList.isEmpty()) {
                Datetime endDate = offerList.get(0).End_Date__c;
                DateTime endDateGmt = Datetime.newInstanceGmt(endDate.year(), endDate.month(), endDate.day(), endDate.hour(), endDate.minute(), endDate.second());

                wrapper = new UserBidsWrapper(null, endDate, DateTime.now());
            }
        }

        return wrapper;
    }

    /**
     * @author Grzegorz Długosz
     * @description Try to update given Bid__c record with new Value__c (record is already updated locally on JS side.)
     *              In case of issue with updating, throw an Exception for JS controller.
     * @param bidRecord
     *          Bid__c record, updated locally with new Value__c.
     * @return If there was no problem with updating, return String = SUCCESS, in case of error, the Exception is thrown before return statement.
     */
    @AuraEnabled
    public static String saveUserBidSrv(Bid__c bidRecord) {
        // create new object. To be sure only value field is updated.
        Bid__c bidToUpdate = new Bid__c(Id = bidRecord.Id, Value__c = bidRecord.Value__c);

        try {
            update bidToUpdate;
        } catch (Exception e) {
            String errorMessage = 'Something went wrong while updating the bid record: ' + e.getMessage();
            AuraHandledException ex = new AuraHandledException(errorMessage);
            ex.setMessage(errorMessage);
            throw ex;
        }

        return 'SUCCESS';
    }

    public class UserBidsWrapper {
        @AuraEnabled public List<Bid__c> bidList { get; set; }
        @AuraEnabled public Datetime auctionEnd { get; set; }
        @AuraEnabled public DateTime currentTime { get; set; }

        public UserBidsWrapper(List<Bid__c> bidList, Datetime auctionEnd, Datetime currentTime) {
            this.bidList = bidList;
            this.auctionEnd = auctionEnd;
            this.currentTime = currentTime;
        }
    }
}