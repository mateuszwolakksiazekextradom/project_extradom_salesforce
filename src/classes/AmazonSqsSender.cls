global class AmazonSqsSender
{

    
    global static String xmlString(String str) {
        if (str != null) {
            return str;
        }
        return '';
    }
    
    global static String xmlString(Decimal dec) {
        if (dec != null) {
            return dec.toPlainString();
        }
        return '';
    }
    
    global static String xmlStringPos(Decimal dec) {
        if (dec != null) {
            if (dec > 0) {
                return dec.toPlainString();
            }
            return '';
        }
        return '';
    }
    
    global static String xmlString(Boolean bool) {
        if (bool) {
            return 'true';
        }
        return 'false';
    }


    
    
    global static String getChangesXML(Datetime dateFrom, Datetime dateTo) {
        
        Page__c[] pages = [SELECT Name, URL__c, Main_Category_Key__c, Building_Cost_Key__c, Technology_Key__c, Style_Key__c, Living_Type_Key__c, Add_Ons_Key__c, Roof_Key__c, Basement_Key__c, Levels_Keys__c, Garage_Keys__c, Usable_Area_min__c, Usable_Area_max__c, Search_Text__c, Plot_H_min__c, Footprint_max__c, Height_max__c, Category_Keys__c FROM Page__c WHERE Type__c = 'Collection' AND AdWords_Ready__c = True ORDER BY Designs_Count_Active__c DESC];
        Design__c[] designs = [SELECT Name, Code__c, Web_Status__c, Master_Design__r.Code__c, Full_Name__c, Supplier__r.Full_Name__c, Supplier__r.Code__c, Gross_Price__c, Construction_Pricing_Estimate__c, Authors__c, Usable_Area__c, Footprint__c, Capacity__c, Minimum_Plot_Size_Horizontal__c, Minimum_Plot_Size_Vertical__c, Height__c, Roof_Slope__c, Roof_Cover_Area__c, Livestock_Unit__c, EUco__c, Garage__c, Carport__c, Level__c, Attic__c, Basement__c, Description__c, Technology__c, Category_Keys__c, (SELECT Name, Is_Plan__c, Plan_Type__c, Plan_Order__c, Image_Type__c, Order__c, Image_Bucket__c, Image_Key__c, R1__c, V1__c, U1__c, R2__c, V2__c, U2__c, R3__c, V3__c, U3__c, R4__c, V4__c, U4__c, R5__c, V5__c, U5__c, R6__c, V6__c, U6__c, R7__c, V7__c, U7__c, R8__c, V8__c, U8__c, R9__c, V9__c, U9__c, R10__c, V10__c, U10__c, R11__c, V11__c, U11__c, R12__c, V12__c, U12__c, R13__c, V13__c, U13__c, R14__c, V14__c, U14__c, R15__c, V15__c, U15__c, R16__c, V16__c, U16__c, R17__c, V17__c, U17__c, R18__c, V18__c, U18__c, R19__c, V19__c, U19__c, R20__c, V20__c, U20__c, R21__c, V21__c, U21__c, R22__c, V22__c, U22__c, R23__c, V23__c, U23__c, R24__c, V24__c, U24__c, R25__c, V25__c, U25__c, R26__c, V26__c, U26__c, R27__c, V27__c, U27__c, R28__c, V28__c, U28__c, R29__c, V29__c, U29__c, R30__c, V30__c, U30__c, R31__c, V31__c, U31__c, R32__c, V32__c, U32__c, R33__c, V33__c, U33__c, R34__c, V34__c, U34__c, R35__c, V35__c, U35__c, R36__c, V36__c, U36__c, R37__c, V37__c, U37__c, R38__c, V38__c, U38__c, R39__c, V39__c, U39__c, R40__c, V40__c, U40__c, R41__c, V41__c, U41__c, R42__c, V42__c, U42__c, R43__c, V43__c, U43__c, R44__c, V44__c, U44__c, R45__c, V45__c, U45__c, R46__c, V46__c, U46__c, R47__c, V47__c, U47__c, R48__c, V48__c, U48__c, R49__c, V49__c, U49__c, R50__c, V50__c, U50__c FROM Media__r), (SELECT Community_Membercode__c, Description FROM Tasks WHERE Type = 'Design Opinion' AND Status = 'Completed' AND Locked__c = True), Base_Design__c, Is_Base_Design__c FROM Design__c WHERE Code__c != '' AND Update_Date__c >= :dateFrom AND Update_Date__c < :dateTo];
        
        Set<ID> baseDesignIds = new Set<ID>{};
        for (Design__c d: designs) {
            if (d.Base_Design__c != null) {
                baseDesignIds.add(d.Base_Design__c);
            } else if (d.Is_Base_Design__c) {
                baseDesignIds.add(d.Id);
            }
        }
        
        Map<ID, Design__c> designVersions = new Map<ID, Design__c>([SELECT Id, Code__c, (SELECT Code__c FROM Design_Versions__r WHERE Code__c != '') FROM Design__c WHERE Id IN :baseDesignIds]);
        
        if (designs.isEmpty()) {
            designs = [SELECT Code__c, Web_Status__c, Master_Design__r.Code__c, Full_Name__c, Supplier__r.Full_Name__c, Supplier__r.Code__c, Gross_Price__c, Construction_Pricing_Estimate__c, Authors__c, Usable_Area__c, Footprint__c, Capacity__c, Minimum_Plot_Size_Horizontal__c, Minimum_Plot_Size_Vertical__c, Height__c, Roof_Slope__c, Roof_Cover_Area__c, Livestock_Unit__c, EUco__c, Garage__c, Carport__c, Level__c, Attic__c, Basement__c, Description__c, Technology__c, Category_Keys__c, (SELECT Name, Is_Plan__c, Plan_Type__c, Plan_Order__c, Image_Type__c, Order__c, Image_Bucket__c, Image_Key__c, R1__c, V1__c, U1__c, R2__c, V2__c, U2__c, R3__c, V3__c, U3__c, R4__c, V4__c, U4__c, R5__c, V5__c, U5__c, R6__c, V6__c, U6__c, R7__c, V7__c, U7__c, R8__c, V8__c, U8__c, R9__c, V9__c, U9__c, R10__c, V10__c, U10__c, R11__c, V11__c, U11__c, R12__c, V12__c, U12__c, R13__c, V13__c, U13__c, R14__c, V14__c, U14__c, R15__c, V15__c, U15__c, R16__c, V16__c, U16__c, R17__c, V17__c, U17__c, R18__c, V18__c, U18__c, R19__c, V19__c, U19__c, R20__c, V20__c, U20__c, R21__c, V21__c, U21__c, R22__c, V22__c, U22__c, R23__c, V23__c, U23__c, R24__c, V24__c, U24__c, R25__c, V25__c, U25__c, R26__c, V26__c, U26__c, R27__c, V27__c, U27__c, R28__c, V28__c, U28__c, R29__c, V29__c, U29__c, R30__c, V30__c, U30__c, R31__c, V31__c, U31__c, R32__c, V32__c, U32__c, R33__c, V33__c, U33__c, R34__c, V34__c, U34__c, R35__c, V35__c, U35__c, R36__c, V36__c, U36__c, R37__c, V37__c, U37__c, R38__c, V38__c, U38__c, R39__c, V39__c, U39__c, R40__c, V40__c, U40__c, R41__c, V41__c, U41__c, R42__c, V42__c, U42__c, R43__c, V43__c, U43__c, R44__c, V44__c, U44__c, R45__c, V45__c, U45__c, R46__c, V46__c, U46__c, R47__c, V47__c, U47__c, R48__c, V48__c, U48__c, R49__c, V49__c, U49__c, R50__c, V50__c, U50__c FROM Media__r), (SELECT Community_Membercode__c, Description FROM Tasks WHERE Type = 'Design Opinion' AND Status = 'Completed' AND Locked__c = True), Base_Design__c, Is_Base_Design__c FROM Design__c WHERE Code__c != '' LIMIT 1];
        }
        
        XmlStreamWriter xml = new XmlStreamWriter();
        
        xml.writeStartElement(null, 'designs', null); /* start designs */
        
        for (Design__c design: designs) {
            
            Design__c d = design;
            
            String masterDesignCode = '';
            if (design.Master_Design__r.Code__c!=null) {
                masterDesignCode = design.Master_Design__r.Code__c;
            }
            
            /*if (masterDesignCode!='') {
                Design__c[] masterDesigns = [SELECT Code__c, Web_Status__c, Master_Design__r.Code__c, Full_Name__c, Supplier__r.Full_Name__c, Supplier__r.Code__c, Gross_Price__c, Construction_Pricing_Estimate__c, Authors__c, Usable_Area__c, Footprint__c, Capacity__c, Minimum_Plot_Size_Horizontal__c, Minimum_Plot_Size_Vertical__c, Height__c, Roof_Slope__c, Roof_Cover_Area__c, EUco__c, Garage__c, Carport__c, Level__c, Attic__c, Basement__c, Description__c, Technology__c, Category_Keys__c, (SELECT Name, Is_Plan__c, Plan_Type__c, Plan_Order__c, Image_Type__c, Order__c, Image_Bucket__c, Image_Key__c, R1__c, V1__c, U1__c, R2__c, V2__c, U2__c, R3__c, V3__c, U3__c, R4__c, V4__c, U4__c, R5__c, V5__c, U5__c, R6__c, V6__c, U6__c, R7__c, V7__c, U7__c, R8__c, V8__c, U8__c, R9__c, V9__c, U9__c, R10__c, V10__c, U10__c, R11__c, V11__c, U11__c, R12__c, V12__c, U12__c, R13__c, V13__c, U13__c, R14__c, V14__c, U14__c, R15__c, V15__c, U15__c, R16__c, V16__c, U16__c, R17__c, V17__c, U17__c, R18__c, V18__c, U18__c, R19__c, V19__c, U19__c, R20__c, V20__c, U20__c, R21__c, V21__c, U21__c, R22__c, V22__c, U22__c, R23__c, V23__c, U23__c, R24__c, V24__c, U24__c, R25__c, V25__c, U25__c, R26__c, V26__c, U26__c, R27__c, V27__c, U27__c, R28__c, V28__c, U28__c, R29__c, V29__c, U29__c, R30__c, V30__c, U30__c, R31__c, V31__c, U31__c, R32__c, V32__c, U32__c, R33__c, V33__c, U33__c, R34__c, V34__c, U34__c, R35__c, V35__c, U35__c, R36__c, V36__c, U36__c, R37__c, V37__c, U37__c, R38__c, V38__c, U38__c, R39__c, V39__c, U39__c, R40__c, V40__c, U40__c, R41__c, V41__c, U41__c, R42__c, V42__c, U42__c, R43__c, V43__c, U43__c, R44__c, V44__c, U44__c, R45__c, V45__c, U45__c, R46__c, V46__c, U46__c, R47__c, V47__c, U47__c, R48__c, V48__c, U48__c, R49__c, V49__c, U49__c, R50__c, V50__c, U50__c FROM Media__r) FROM Design__c WHERE Code__c =: masterDesignCode];
                for (Design__c masterDesign: masterDesigns) {
                    d = masterDesign;
                }
            }*/
            
            xml.writeStartElement(null, 'design', null); /* start design */
            xml.writeAttribute(null, null, 'code', design.Code__c);
            xml.writeAttribute(null, null, 'status', design.Web_Status__c);
            xml.writeAttribute(null, null, 'master', masterDesignCode);

if (d.Web_Status__c == 'out-of-date' || d.Web_Status__c == 'active' || d.Web_Status__c == 'rejected' || d.Web_Status__c == 'shadow') { /* start if ready to display */
    
            Boolean attic = false;
            if (d.Attic__c>0) {
                attic = true;
            }
            
            Boolean basement = false;
            if (d.Basement__c>0) {
                basement = true;
            }

            xml.writeStartElement(null, 'attributes', null); /* start design > attributes */
            
                xml.writeStartElement(null, 'att', null);
                xml.writeAttribute(null, null, 'name', 'name');
                xml.writeCData(d.Full_Name__c);
                xml.writeEndElement();
        
                xml.writeStartElement(null, 'att', null);
                xml.writeAttribute(null, null, 'name', 'supplier');
                xml.writeCData(d.Supplier__r.Full_Name__c);
                xml.writeEndElement();
                
                xml.writeStartElement(null, 'att', null);
                xml.writeAttribute(null, null, 'name', 'supplier code');
                xml.writeCData(d.Supplier__r.Code__c);
                xml.writeEndElement();
        
                xml.writeStartElement(null, 'att', null);
                xml.writeAttribute(null, null, 'name', 'gross price');
                xml.writeCharacters(xmlString(d.Gross_Price__c));
                xml.writeEndElement();
        
                xml.writeStartElement(null, 'att', null);
                xml.writeAttribute(null, null, 'name', 'usable area');
                xml.writeCharacters(xmlString(d.Usable_Area__c));
                xml.writeEndElement();
        
                xml.writeStartElement(null, 'att', null);
                xml.writeAttribute(null, null, 'name', 'footprint');
                xml.writeCharacters(xmlString(d.Footprint__c));
                xml.writeEndElement();
        
                xml.writeStartElement(null, 'att', null);
                xml.writeAttribute(null, null, 'name', 'capacity');
                xml.writeCharacters(xmlString(d.Capacity__c));
                xml.writeEndElement();
        
                xml.writeStartElement(null, 'att', null);
                xml.writeAttribute(null, null, 'name', 'minimum plot size (horizontal)');
                xml.writeCharacters(xmlString(d.Minimum_Plot_Size_Horizontal__c));
                xml.writeEndElement();
        
                xml.writeStartElement(null, 'att', null);
                xml.writeAttribute(null, null, 'name', 'minimum plot size (vertical)');
                xml.writeCharacters(xmlString(d.Minimum_Plot_Size_Vertical__c));
                xml.writeEndElement();
        
                xml.writeStartElement(null, 'att', null);
                xml.writeAttribute(null, null, 'name', 'height');
                xml.writeCharacters(xmlString(d.Height__c));
                xml.writeEndElement();
        
                xml.writeStartElement(null, 'att', null);
                xml.writeAttribute(null, null, 'name', 'roof slope');
                xml.writeCharacters(xmlString(d.Roof_Slope__c));
                xml.writeEndElement();
                
                xml.writeStartElement(null, 'att', null);
                xml.writeAttribute(null, null, 'name', 'roof cover area');
                xml.writeCharacters(xmlString(d.Roof_Cover_Area__c));
                xml.writeEndElement();
                
                xml.writeStartElement(null, 'att', null);
                xml.writeAttribute(null, null, 'name', 'livestock unit');
                xml.writeCharacters(xmlString(d.Livestock_Unit__c));
                xml.writeEndElement();
        
                xml.writeStartElement(null, 'att', null);
                xml.writeAttribute(null, null, 'name', 'euco');
                xml.writeCharacters(xmlString(d.EUco__c));
                xml.writeEndElement();
        
                xml.writeStartElement(null, 'att', null);
                xml.writeAttribute(null, null, 'name', 'garage');
                xml.writeCharacters(xmlString(d.Garage__c));
                xml.writeEndElement();
        
                xml.writeStartElement(null, 'att', null);
                xml.writeAttribute(null, null, 'name', 'carport');
                xml.writeCharacters(xmlString(d.Carport__c));
                xml.writeEndElement();
        
                xml.writeStartElement(null, 'att', null);
                xml.writeAttribute(null, null, 'name', 'level');
                xml.writeCharacters(xmlString(d.Level__c));
                xml.writeEndElement();
        
                xml.writeStartElement(null, 'att', null);
                xml.writeAttribute(null, null, 'name', 'attic');
                xml.writeCharacters(xmlString(attic));
                xml.writeEndElement();
        
                xml.writeStartElement(null, 'att', null);
                xml.writeAttribute(null, null, 'name', 'basement');
                xml.writeCharacters(xmlString(basement));
                xml.writeEndElement();
        
                xml.writeStartElement(null, 'att', null);
                xml.writeAttribute(null, null, 'name', 'description');
                xml.writeCData(xmlString(d.Description__c));
                xml.writeEndElement();
        
                xml.writeStartElement(null, 'att', null);
                xml.writeAttribute(null, null, 'name', 'technology');
                xml.writeCData(xmlString(d.Technology__c));
                xml.writeEndElement();
                
                xml.writeStartElement(null, 'att', null);
                xml.writeAttribute(null, null, 'name', 'authors');
                xml.writeCData(xmlString(d.Authors__c));
                xml.writeEndElement();
                
                xml.writeStartElement(null, 'att', null);
                xml.writeAttribute(null, null, 'name', 'construction pricing estimate');
                xml.writeCharacters(xmlString(d.Construction_Pricing_Estimate__c));
                xml.writeEndElement();
        
            xml.writeEndElement(); /* end design > attributes */
            
            xml.writeStartElement(null, 'categories', null); /* start design > categories */
            
                String strCategories = d.Category_Keys__c;
                strCategories = strCategories.replace('][',',');
                strCategories = strCategories.replace('[','');
                strCategories = strCategories.replace(']','');
                List<String> categories = new List<String>(new Set<String>(strCategories.split(',',0)));
                
                for (String cat: categories) {
                    if (cat.isNumeric()) {
                        xml.writeStartElement(null, 'cat', null);
                        xml.writeCharacters(cat);
                        xml.writeEndElement();
                    }
                }
        
            xml.writeEndElement(); /* end design > categories */
            
            xml.writeStartElement(null, 'links', null); /* start design > links */
            
                /* for (Page__c p: pages) {
                    if (ContactProfiler.compareDesignWithPage(d, p)) {
                        xml.writeStartElement(null, 'link', null);
                        xml.writeAttribute(null, null, 'url', xmlString(p.URL__c));
                        xml.writeCharacters(p.Name);
                        xml.writeEndElement();
                    }
                } */
        
            xml.writeEndElement(); /* end design > links */
            
            xml.writeStartElement(null, 'versions', null); /* start design > versions */
            
                if (designVersions.containsKey(d.Id)) {
                    Design__c baseDesign = designVersions.get(d.Id);
                    for (Design__c dversion: baseDesign.Design_Versions__r) {
                        xml.writeStartElement(null, 'version', null);
                        xml.writeCharacters(dversion.Code__c);
                        xml.writeEndElement();
                    }
                } else if (designVersions.containsKey(d.Base_Design__c)) {
                    Design__c baseDesign = designVersions.get(d.Base_Design__c);
                    xml.writeStartElement(null, 'version', null);
                    xml.writeCharacters(baseDesign.Code__c);
                    xml.writeEndElement();
                    for (Design__c dversion: baseDesign.Design_Versions__r) {
                        if (dversion.Code__c != d.Code__c) {
                            xml.writeStartElement(null, 'version', null);
                            xml.writeCharacters(dversion.Code__c);
                            xml.writeEndElement();
                        }
                    }
                }
        
            xml.writeEndElement(); /* end design > versions */
            
            xml.writeStartElement(null, 'plans', null); /* start design > plans */
        
            for (Media__c m: d.Media__r) {
            
                if (m.Is_Plan__c) {
            
                    xml.writeStartElement(null, 'plan', null); /* start plan */
                    xml.writeAttribute(null, null, 'type', xmlString(m.Plan_Type__c));
                    
                    if (xmlString(m.R1__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '1'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U1__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V1__c)); xml.writeCData(xmlString(m.R1__c)); xml.writeEndElement(); }
                    if (xmlString(m.R2__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '2'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U2__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V2__c)); xml.writeCData(xmlString(m.R2__c)); xml.writeEndElement(); }
                    if (xmlString(m.R3__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '3'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U3__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V3__c)); xml.writeCData(xmlString(m.R3__c)); xml.writeEndElement(); }
                    if (xmlString(m.R4__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '4'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U4__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V4__c)); xml.writeCData(xmlString(m.R4__c)); xml.writeEndElement(); }
                    if (xmlString(m.R5__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '5'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U5__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V5__c)); xml.writeCData(xmlString(m.R5__c)); xml.writeEndElement(); }
                    if (xmlString(m.R6__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '6'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U6__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V6__c)); xml.writeCData(xmlString(m.R6__c)); xml.writeEndElement(); }
                    if (xmlString(m.R7__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '7'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U7__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V7__c)); xml.writeCData(xmlString(m.R7__c)); xml.writeEndElement(); }
                    if (xmlString(m.R8__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '8'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U8__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V8__c)); xml.writeCData(xmlString(m.R8__c)); xml.writeEndElement(); }
                    if (xmlString(m.R9__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '9'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U9__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V9__c)); xml.writeCData(xmlString(m.R9__c)); xml.writeEndElement(); }
                    if (xmlString(m.R10__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '10'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U10__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V10__c)); xml.writeCData(xmlString(m.R10__c)); xml.writeEndElement(); }
                    if (xmlString(m.R11__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '11'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U11__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V11__c)); xml.writeCData(xmlString(m.R11__c)); xml.writeEndElement(); }
                    if (xmlString(m.R12__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '12'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U12__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V12__c)); xml.writeCData(xmlString(m.R12__c)); xml.writeEndElement(); }
                    if (xmlString(m.R13__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '13'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U13__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V13__c)); xml.writeCData(xmlString(m.R13__c)); xml.writeEndElement(); }
                    if (xmlString(m.R14__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '14'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U14__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V14__c)); xml.writeCData(xmlString(m.R14__c)); xml.writeEndElement(); }
                    if (xmlString(m.R15__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '15'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U15__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V15__c)); xml.writeCData(xmlString(m.R15__c)); xml.writeEndElement(); }
                    if (xmlString(m.R16__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '16'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U16__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V16__c)); xml.writeCData(xmlString(m.R16__c)); xml.writeEndElement(); }
                    if (xmlString(m.R17__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '17'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U17__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V17__c)); xml.writeCData(xmlString(m.R17__c)); xml.writeEndElement(); }
                    if (xmlString(m.R18__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '18'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U18__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V18__c)); xml.writeCData(xmlString(m.R18__c)); xml.writeEndElement(); }
                    if (xmlString(m.R19__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '19'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U19__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V19__c)); xml.writeCData(xmlString(m.R19__c)); xml.writeEndElement(); }
                    if (xmlString(m.R20__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '20'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U20__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V20__c)); xml.writeCData(xmlString(m.R20__c)); xml.writeEndElement(); }
                    if (xmlString(m.R21__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '21'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U21__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V21__c)); xml.writeCData(xmlString(m.R21__c)); xml.writeEndElement(); }
                    if (xmlString(m.R22__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '22'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U22__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V22__c)); xml.writeCData(xmlString(m.R22__c)); xml.writeEndElement(); }
                    if (xmlString(m.R23__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '23'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U23__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V23__c)); xml.writeCData(xmlString(m.R23__c)); xml.writeEndElement(); }
                    if (xmlString(m.R24__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '24'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U24__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V24__c)); xml.writeCData(xmlString(m.R24__c)); xml.writeEndElement(); }
                    if (xmlString(m.R25__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '25'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U25__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V25__c)); xml.writeCData(xmlString(m.R25__c)); xml.writeEndElement(); }
                    if (xmlString(m.R26__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '26'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U26__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V26__c)); xml.writeCData(xmlString(m.R26__c)); xml.writeEndElement(); }
                    if (xmlString(m.R27__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '27'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U27__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V27__c)); xml.writeCData(xmlString(m.R27__c)); xml.writeEndElement(); }
                    if (xmlString(m.R28__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '28'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U28__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V28__c)); xml.writeCData(xmlString(m.R28__c)); xml.writeEndElement(); }
                    if (xmlString(m.R29__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '29'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U29__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V29__c)); xml.writeCData(xmlString(m.R29__c)); xml.writeEndElement(); }
                    if (xmlString(m.R30__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '30'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U30__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V30__c)); xml.writeCData(xmlString(m.R30__c)); xml.writeEndElement(); }
                    if (xmlString(m.R31__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '31'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U31__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V31__c)); xml.writeCData(xmlString(m.R31__c)); xml.writeEndElement(); }
                    if (xmlString(m.R32__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '32'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U32__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V32__c)); xml.writeCData(xmlString(m.R32__c)); xml.writeEndElement(); }
                    if (xmlString(m.R33__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '33'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U33__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V33__c)); xml.writeCData(xmlString(m.R33__c)); xml.writeEndElement(); }
                    if (xmlString(m.R34__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '34'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U34__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V34__c)); xml.writeCData(xmlString(m.R34__c)); xml.writeEndElement(); }
                    if (xmlString(m.R35__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '35'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U35__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V35__c)); xml.writeCData(xmlString(m.R35__c)); xml.writeEndElement(); }
                    if (xmlString(m.R36__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '36'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U36__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V36__c)); xml.writeCData(xmlString(m.R36__c)); xml.writeEndElement(); }
                    if (xmlString(m.R37__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '37'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U37__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V37__c)); xml.writeCData(xmlString(m.R37__c)); xml.writeEndElement(); }
                    if (xmlString(m.R38__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '38'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U38__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V38__c)); xml.writeCData(xmlString(m.R38__c)); xml.writeEndElement(); }
                    if (xmlString(m.R39__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '39'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U39__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V39__c)); xml.writeCData(xmlString(m.R39__c)); xml.writeEndElement(); }
                    if (xmlString(m.R40__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '40'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U40__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V40__c)); xml.writeCData(xmlString(m.R40__c)); xml.writeEndElement(); }
                    if (xmlString(m.R41__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '41'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U41__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V41__c)); xml.writeCData(xmlString(m.R41__c)); xml.writeEndElement(); }
                    if (xmlString(m.R42__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '42'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U42__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V42__c)); xml.writeCData(xmlString(m.R42__c)); xml.writeEndElement(); }
                    if (xmlString(m.R43__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '43'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U43__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V43__c)); xml.writeCData(xmlString(m.R43__c)); xml.writeEndElement(); }
                    if (xmlString(m.R44__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '44'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U44__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V44__c)); xml.writeCData(xmlString(m.R44__c)); xml.writeEndElement(); }
                    if (xmlString(m.R45__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '45'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U45__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V45__c)); xml.writeCData(xmlString(m.R45__c)); xml.writeEndElement(); }
                    if (xmlString(m.R46__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '46'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U46__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V46__c)); xml.writeCData(xmlString(m.R46__c)); xml.writeEndElement(); }
                    if (xmlString(m.R47__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '47'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U47__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V47__c)); xml.writeCData(xmlString(m.R47__c)); xml.writeEndElement(); }
                    if (xmlString(m.R48__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '48'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U48__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V48__c)); xml.writeCData(xmlString(m.R48__c)); xml.writeEndElement(); }
                    if (xmlString(m.R49__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '49'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U49__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V49__c)); xml.writeCData(xmlString(m.R49__c)); xml.writeEndElement(); }
                    if (xmlString(m.R50__c)!='') { xml.writeStartElement(null, 'room', null); xml.writeAttribute(null, null, 'order', '50'); xml.writeAttribute(null, null, 'area', xmlStringPos(m.U50__c)); xml.writeAttribute(null, null, 'usable', xmlStringPos(m.V50__c)); xml.writeCData(xmlString(m.R50__c)); xml.writeEndElement(); }
                    
                    xml.writeEndElement(); /* end plan */
                    
                }
        
            }
            
            xml.writeEndElement(); /* end design > plans */
            
            xml.writeStartElement(null, 'media', null); /* start design > media */
        
            for (Media__c m: d.Media__r) {
            
                if (xmlString(m.Image_Key__c)!='') {
            
                    xml.writeStartElement(null, 'image', null); /* start image */
                    xml.writeAttribute(null, null, 'type', xmlString(m.Image_Type__c));
                    if (xmlString(m.Plan_Type__c) != '') {
                        xml.writeAttribute(null, null, 'plan', xmlString(m.Plan_Type__c));
                    }
                    xml.writeAttribute(null, null, 'order', xmlString(m.Order__c));
                    xml.writeAttribute(null, null, 'bucket', xmlString(m.Image_Bucket__c));
                    xml.writeAttribute(null, null, 'key', xmlString(m.Image_Key__c));
                    xml.writeEndElement(); /* end image */
                    
                }
        
            }
            
            xml.writeEndElement(); /* end design > media */
            
            xml.writeStartElement(null, 'opinions', null); /* start design > opinions */
        
            for (Task t: d.Tasks) {
                
                xml.writeStartElement(null, 'opinion', null); /* start opinion */
                xml.writeAttribute(null, null, 'membercode', xmlString(t.Community_Membercode__c));
                xml.writeCData(xmlString(t.Description));
                xml.writeEndElement(); /* end opinion */
                
            }
            
            xml.writeEndElement(); /* end design > opinions */

} /* end if ready to display */
            
            xml.writeEndElement(); /* end design */
        
        }
        
        xml.writeEndElement(); /* end designs */
        
        return xml.getXmlString();
        
    }
    
    

    
    @future
    global static void genChangesXMLDoc(Datetime dateFrom, Datetime dateTo) {
        
        Document document = new Document();
        document.Body = Blob.valueOf(AmazonSqsSender.getChangesXML(dateFrom, dateTo));
        document.Name = 'designs-from-'+String.valueOf(dateFrom)+'-'+String.valueOf(dateTo)+'.xml';
        document.FolderId = '00lb0000001BCW6';
        
        insert document;
        
    }

    
    global static void genLatestChangesXMLDoc() {
        
        Datetime dateFrom = (Datetime)[SELECT MAX(CreatedDate)dateFrom FROM Document WHERE folderId = '00lb0000001BCW6'][0].get('dateFrom');
        dateFrom = dateFrom.addMinutes(-1);
        Datetime dateTo = datetime.now();
        genChangesXMLDoc(dateFrom, dateTo);
        
    }
    

}