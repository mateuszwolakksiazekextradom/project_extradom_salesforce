@IsTest(SeeAllData=true)
private class ConnectApiToolsForVFTest {

    static testMethod void myUnitTest() {
    
        ConnectApiToolsForVF tools = new ConnectApiToolsForVF();

        System.assertEquals(tools.LinkSegmentType, ConnectApi.MessageSegmentType.Link);
        System.assertEquals(tools.MentionSegmentType, ConnectApi.MessageSegmentType.Mention);
        System.assertEquals(tools.HashtagSegmentType, ConnectApi.MessageSegmentType.Hashtag);
        System.assertEquals(tools.TextSegmentType, ConnectApi.MessageSegmentType.Text);
        System.assertEquals(tools.EntityLinkSegmentType, ConnectApi.MessageSegmentType.EntityLink);
        System.assertEquals(tools.InlineImageSegmentType, ConnectApi.MessageSegmentType.InlineImage);
        System.assertEquals(tools.MarkupBeginSegmentType, ConnectApi.MessageSegmentType.MarkupBegin);
        System.assertEquals(tools.MarkupEndSegmentType, ConnectApi.MessageSegmentType.MarkupEnd);
        System.assertEquals(tools.FieldChangeSegmentType, ConnectApi.MessageSegmentType.FieldChange);
        System.assertEquals(tools.FieldChangeNameSegmentType, ConnectApi.MessageSegmentType.FieldChangeName);
        System.assertEquals(tools.FieldChangeValueSegmentType, ConnectApi.MessageSegmentType.FieldChangeValue);
        System.assertEquals(tools.ResourceLinkSegmentType, ConnectApi.MessageSegmentType.ResourceLink);
        System.assertEquals(tools.MoreChangesSegmentType, ConnectApi.MessageSegmentType.MoreChanges);
    
    }
    
}