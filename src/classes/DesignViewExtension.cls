public with sharing class DesignViewExtension {

    private final Design__c d;
    public DesignViewExtension(ApexPages.StandardController stdController) {
        if (!Test.isRunningTest()) {
            stdController.addFields(new List<String>{'Base_Design__c', 'Category_Keys__c', 'Name', 'Minimum_Plot_Size_Horizontal__c', 'Usable_Area__c', 'Footprint__c', 'Height__c', 'Opinion_1_Author_Id__c', 'Opinion_2_Author_Id__c', 'Opinion_3_Author_Id__c', 'Opinion_4_Author_Id__c', 'Opinion_5_Author_Id__c', 'Opinion_6_Author_Id__c', 'Type__c', 'Usable_Area__c', 'Roof_slope__c', 'Count_rooms__c', 'Attic__c', 'Level__c', 'Border_Ready__c', 'Construction__c', 'Garage__c', 'Roof__c', 'Ridge__c', 'Living_Type__c', 'Basement__c', 'Shape__c', 'Garage_Location__c', 'South_Entrance__c', 'Is_Passive__c', 'Feed_Gallery_Featured__c', 'Style__c', 'Adaptable_Attic__c', 'Ceiling__c', 'Has_iDesigner__c', 'Has_Panorama__c', 'Count_Interior_Media__c'});
        }
        this.d = (Design__c)stdController.getRecord();
    }         
    
    public List<Design__c> getBaseDesignVersions() {
        if (String.isNotBlank(d.Base_Design__c)) {
            return [SELECT Full_Name__c, Type__c, Sketchfab_ID__c, Current_Price__c, Gross_Price__c, Thumbnail_Base_URL__c, Code__c, KW_Model__c, KW_Model_Preparation__c, Height__c, Usable_Area__c, Footprint__c, Capacity__c, Roof_slope__c, Count_Rooms__c, Minimum_Plot_Size_Horizontal__c, (SELECT Base_URL__c FROM Media__r WHERE Category__c IN ('Building Placement', 'Ground Floor Plan') ORDER BY Category__c DESC LIMIT 1) FROM Design__c WHERE ((Base_Design__c = :d.Base_Design__c AND Id != :d.Id) OR Id =: d.Base_Design__c) AND Status__c = 'Active' ORDER BY Page_Score_Unique__c DESC];
        }
        return null;
    }
    
    public List<Design__c> getSimiliarDesigns() {
        Decimal usable_area = 120;
        Decimal height = 8;
        Decimal roof_slope = 40;
        Decimal count_rooms = 0;
        Decimal attic = 1;
        Decimal level = 1;
        Decimal garage = 0;
        if (d.Usable_Area__c != null) usable_area = d.Usable_Area__c;
        if (d.Height__c != null) height = d.Height__c;
        if (d.Roof_slope__c != null) roof_slope = d.Roof_slope__c;
        if (d.Count_Rooms__c != null) count_rooms = d.Count_Rooms__c;
        if (d.Attic__c != null) attic = d.Attic__c;
        if (d.Level__c != null) level = d.Level__c;
        if (d.Garage__c != null) garage = d.Garage__c;
        return [SELECT Full_Name__c, Type__c, Sketchfab_ID__c, Current_Price__c, Gross_Price__c, Thumbnail_Base_URL__c, Code__c, KW_Model__c, KW_Model_Preparation__c, Height__c, Usable_Area__c, Footprint__c, Capacity__c, Roof_slope__c, Count_Rooms__c, Minimum_Plot_Size_Horizontal__c, (SELECT Base_URL__c FROM Media__r WHERE Category__c IN ('Building Placement', 'Ground Floor Plan') ORDER BY Category__c DESC LIMIT 1) FROM Design__c WHERE Border_Ready__c = :d.Border_Ready__c AND Type__c = :d.Type__c AND Usable_Area__c >= :usable_area*0.9 AND Usable_Area__c <= :usable_area*1.1 AND Height__c >= :height*0.9 AND Height__c <= :height*1.1 AND Roof_slope__c >= :roof_slope-5 AND Roof_slope__c <= :roof_slope+5 AND Count_Rooms__c >= :count_rooms AND Attic__c = :attic AND Level__c = :level AND Construction__c = :d.Construction__c AND Garage__c = :garage AND Roof__c = :d.Roof__c AND Ridge__c = :d.Ridge__c AND Living_Type__c = :d.Living_Type__c AND Basement__c = :d.Basement__c AND Shape__c = :d.Shape__c AND Garage_Location__c = :d.Garage_Location__c AND South_Entrance__c = :d.South_Entrance__c AND Is_Passive__c = :d.Is_Passive__c AND Id != :d.Id AND Status__c = 'Active' AND Id NOT IN :this.getDesignVersions() ORDER BY Order_Score__c DESC NULLS LAST LIMIT 6];
    }
    
    public List<Design__c> getDesignVersions() {
        if (String.isNotBlank(d.Base_Design__c)) {
            return [SELECT Full_Name__c, Type__c, Sketchfab_ID__c, Current_Price__c, Gross_Price__c, Thumbnail_Base_URL__c, Code__c, KW_Model__c, KW_Model_Preparation__c, Height__c, Usable_Area__c, Footprint__c, Capacity__c, Roof_slope__c, Count_Rooms__c, Minimum_Plot_Size_Horizontal__c, (SELECT Base_URL__c FROM Media__r WHERE Category__c IN ('Building Placement', 'Ground Floor Plan') ORDER BY Category__c DESC LIMIT 1) FROM Design__c WHERE ((Base_Design__c = :d.Base_Design__c AND Id != :d.Id) OR Id =: d.Base_Design__c) AND Status__c = 'Active' ORDER BY Page_Score_Unique__c DESC];
        } else {
            return [SELECT Full_Name__c, Type__c, Sketchfab_ID__c, Current_Price__c, Gross_Price__c, Thumbnail_Base_URL__c, Code__c, KW_Model__c, KW_Model_Preparation__c, Height__c, Usable_Area__c, Footprint__c, Capacity__c, Roof_slope__c, Count_Rooms__c, Minimum_Plot_Size_Horizontal__c, (SELECT Base_URL__c FROM Media__r WHERE Category__c IN ('Building Placement', 'Ground Floor Plan') ORDER BY Category__c DESC LIMIT 1) FROM Design__c WHERE Base_Design__c = :d.Id AND Status__c = 'Active' ORDER BY Page_Score_Unique__c DESC];
        }
    }
    
    public ConnectApi.UserDetail getReviewer1() {
        try {
            return ConnectApi.ChatterUsers.getUser(Network.getNetworkId(), d.Opinion_1_Author_Id__c);
        } catch(Exception e) {}
        return null;
    }
    public ConnectApi.UserDetail getReviewer2() {
        try {
            return ConnectApi.ChatterUsers.getUser(Network.getNetworkId(), d.Opinion_2_Author_Id__c);
        } catch(Exception e) {}
        return null;
    }
    public ConnectApi.UserDetail getReviewer3() {
        try {
            return ConnectApi.ChatterUsers.getUser(Network.getNetworkId(), d.Opinion_3_Author_Id__c);
        } catch(Exception e) {}
        return null;
    }
    public ConnectApi.UserDetail getReviewer4() {
        try {
            return ConnectApi.ChatterUsers.getUser(Network.getNetworkId(), d.Opinion_4_Author_Id__c);
        } catch(Exception e) {}
        return null;
    }
    public ConnectApi.UserDetail getReviewer5() {
        try {
            return ConnectApi.ChatterUsers.getUser(Network.getNetworkId(), d.Opinion_5_Author_Id__c);
        } catch(Exception e) {}
        return null;
    }
    public ConnectApi.UserDetail getReviewer6() {
        try {
            return ConnectApi.ChatterUsers.getUser(Network.getNetworkId(), d.Opinion_6_Author_Id__c);
        } catch(Exception e) {}
        return null;
    }
    
    public List<Media__c> getPlans() {
        return [SELECT Category__c, Base_URL__c, Sum_Usable__c, Sum_Total__c, R1__c, V1__c, U1__c, R2__c, V2__c, U2__c, R3__c, V3__c, U3__c, R4__c, V4__c, U4__c, R5__c, V5__c, U5__c, R6__c, V6__c, U6__c, R7__c, V7__c, U7__c, R8__c, V8__c, U8__c, R9__c, V9__c, U9__c, R10__c, V10__c, U10__c, R11__c, V11__c, U11__c, R12__c, V12__c, U12__c, R13__c, V13__c, U13__c, R14__c, V14__c, U14__c, R15__c, V15__c, U15__c, R16__c, V16__c, U16__c, R17__c, V17__c, U17__c, R18__c, V18__c, U18__c, R19__c, V19__c, U19__c, R20__c, V20__c, U20__c, R21__c, V21__c, U21__c, R22__c, V22__c, U22__c, R23__c, V23__c, U23__c, R24__c, V24__c, U24__c, R25__c, V25__c, U25__c, R26__c, V26__c, U26__c, R27__c, V27__c, U27__c, R28__c, V28__c, U28__c, R29__c, V29__c, U29__c, R30__c, V30__c, U30__c, R31__c, V31__c, U31__c, R32__c, V32__c, U32__c, R33__c, V33__c, U33__c, R34__c, V34__c, U34__c, R35__c, V35__c, U35__c, R36__c, V36__c, U36__c, R37__c, V37__c, U37__c, R38__c, V38__c, U38__c, R39__c, V39__c, U39__c, R40__c, V40__c, U40__c, R41__c, V41__c, U41__c, R42__c, V42__c, U42__c, R43__c, V43__c, U43__c, R44__c, V44__c, U44__c, R45__c, V45__c, U45__c, R46__c, V46__c, U46__c, R47__c, V47__c, U47__c, R48__c, V48__c, U48__c, R49__c, V49__c, U49__c, R50__c, V50__c, U50__c FROM Media__c WHERE Design__c = :d.Id AND Category__c IN ('1st Floor Plan', '2nd Attic Plan', '2nd Floor Plan', '3rd Floor Plan', 'Attic Plan', 'Basement Plan', 'Garage Plan', 'Ground Floor Plan', 'Mezzanine Plan', 'Roof Plan') ORDER BY Lp__c];
    }
    
    public List<Media__c> getImages() {
        return [SELECT Category__c, Base_URL__c, Name, Subcategory__c, minPitch__c, maxPitch__c, minYaw__c, maxYaw__c, Disable_Zoom__c FROM Media__c WHERE Design__c = :d.Id AND Category__c IN ('Outdoor', 'Indoor', 'Building Placement', 'Section AA', 'Section BB', 'Front-side Elevation', 'Back-side Elevation', 'Left-side Elevation', 'Right-side Elevation', '3D Rotation') ORDER BY Lp__c];
    }
    
    public ConnectApi.FollowerPage getFollowerPage() {
        return ConnectApi.Chatter.getFollowers(Network.getNetworkId(), d.Id);
    }
    
    public ConnectApi.FeedElementPage getFeedElementPage() {
        return ConnectApi.ChatterFeeds.getFeedElementsFromFeed(Network.getNetworkId(), ConnectApi.FeedType.Record, d.Id);
    }
    
    public ConnectApi.Reference getMySubscriptionReference() {
        ConnectApi.FollowerPage followerPage = ConnectApi.Chatter.getFollowers(Network.getNetworkId(), d.Id);
        for (ConnectApi.Subscription subscription : followerPage.followers) {
            if (subscription.subject.mySubscription != null) {
                return subscription.subject.mySubscription;
            }
        }
        return null;
    }
    
    public PageReference follow() {
        ConnectApi.ChatterUsers.follow(Network.getNetworkId(), 'me', d.Id);
        return null;
    }
    
    public PageReference unfollow() {
        ConnectApi.FollowerPage followerPage = ConnectApi.Chatter.getFollowers(Network.getNetworkId(), d.Id);
        for (ConnectApi.Subscription subscription : followerPage.followers) {
            if (subscription.subject.mySubscription != null) {
                ConnectApi.Chatter.deleteSubscription(Network.getNetworkId(), subscription.subject.mySubscription.id);
                break;
            }
        }
        return null;
    }
    
    public Id getGalleryContentVersionThumbId() {
        for (ContentDocumentLink cdl : [SELECT Id, ContentDocument.LatestPublishedVersionId FROM ContentDocumentLink WHERE LinkedEntityId = :d.Id AND Visibility = 'AllUsers' LIMIT 1]) {
            return cdl.ContentDocument.LatestPublishedVersionId;
        }
        return null;
    }
    
    public List<ConnectApi.FeedElement> getTopContentPosts() {
        List<Id> ids = new List<Id>{};
        for (Design__Feed d_fi : [SELECT Id FROM Design__Feed WHERE ParentId = :d.Id AND Type = 'ContentPost' AND Visibility = 'AllUsers' ORDER BY LikeCount DESC, CreatedDate DESC LIMIT 100]) {
            ids.add(d_fi.Id);
        }
        List<ConnectApi.FeedElement> feedElements = new List<ConnectApi.FeedElement>{};
        ConnectApi.BatchResult[] batchResults = ConnectApi.ChatterFeeds.getFeedElementBatch(Network.getNetworkId(), ids);
        for (ConnectApi.BatchResult batchResult : batchResults) {
            if (batchResult.isSuccess()) {
                ConnectApi.FeedElement element;
                if (batchResult.getResult() instanceof ConnectApi.FeedElement) {
                    element = (ConnectApi.FeedElement) batchResult.getResult();
                    feedElements.add(element);
                }
            }
        }
        
        List<ConnectApi.FeedElement> finalFeeds = new List<ConnectApi.FeedElement>();
        List<ConnectApi.FeedElement> lockup = new List<ConnectApi.FeedElement>();
        List<ConnectApi.FeedElement> development = new List<ConnectApi.FeedElement>();
        List<ConnectApi.FeedElement> buildingShellClosed = new List<ConnectApi.FeedElement>();
        List<ConnectApi.FeedElement> buildingShellOpen = new List<ConnectApi.FeedElement>();
        List<ConnectApi.FeedElement> zero = new List<ConnectApi.FeedElement>();
        
        for(ConnectApi.FeedElement fe : feedElements) {           
            for(ConnectApi.Topic t : fe.capabilities.topics.items) {
                system.debug(t.name);
                switch on t.Name {
                    when 'Ukończona budowa' {
                        if(finalFeeds.size() < 6) {
                            finalFeeds.add(fe);
                        } else {
                            return finalFeeds;
                        }
                    }       
                    when 'Stan pod klucz' { 
                        lockup.add(fe);
                    }
                    when 'Stan deweloperski' { 
                        development.add(fe);
                    }
                    when 'Stan surowy zamknięty' { 
                        buildingShellClosed.add(fe);
                    }
                    when 'Stan surowy otwarty' { 
                        buildingShellOpen.add(fe);
                    }
                    when 'Stan zerowy' { 
                        zero.add(fe);
                    }
                }
            }
        }

        for (ConnectApi.FeedElement fe : lockup) {
            if (finalFeeds.size() < 6) {
                if(!finalFeeds.contains(fe)) {finalFeeds.add(fe);}
            } else {
                return finalFeeds;
            }
        }
        for (ConnectApi.FeedElement fe : development) {
            if (finalFeeds.size() < 6) {
                if(!finalFeeds.contains(fe)) {finalFeeds.add(fe);}
            } else {
                return finalFeeds;
            }
        }
        for (ConnectApi.FeedElement fe : buildingShellClosed) {
            if (finalFeeds.size() < 6) {
                if(!finalFeeds.contains(fe)) {finalFeeds.add(fe);}
            } else {
                return finalFeeds;
            }
        }
        for (ConnectApi.FeedElement fe : buildingShellOpen) {
            if (finalFeeds.size() < 6) {
                if(!finalFeeds.contains(fe)) {finalFeeds.add(fe);}
            } else {
                return finalFeeds;
            }
        }
        for (ConnectApi.FeedElement fe : zero) {
            if (finalFeeds.size() < 6) {
                if(!finalFeeds.contains(fe)) {finalFeeds.add(fe);}
            } else {
                return finalFeeds;
            }
        }
        for (ConnectApi.FeedElement fe : feedelements) {
            if (finalFeeds.size() < 6) {
                if(!finalFeeds.contains(fe)) {finalFeeds.add(fe);}
            } else {
                return finalFeeds;
            }
        }
        return finalFeeds;
    }
    
    public List<ConnectApi.FeedElement> getContentPosts() {
        List<Id> ids = new List<Id>{};
        for (Design__Feed d_fi : [SELECT Id FROM Design__Feed WHERE ParentId = :d.Id AND Type = 'ContentPost' AND Visibility = 'AllUsers' ORDER BY CreatedDate DESC LIMIT 450]) {
            ids.add(d_fi.Id);
        }
        List<ConnectApi.FeedElement> feedElements = new List<ConnectApi.FeedElement>{};
        ConnectApi.BatchResult[] batchResults = ConnectApi.ChatterFeeds.getFeedElementBatch(Network.getNetworkId(), ids);
        for (ConnectApi.BatchResult batchResult : batchResults) {
            if (batchResult.isSuccess()) {
                ConnectApi.FeedElement element;
                if (batchResult.getResult() instanceof ConnectApi.FeedElement) {
                    element = (ConnectApi.FeedElement) batchResult.getResult();
                    feedElements.add(element);
                }
            }
        }
        return feedElements;
    }
    
    public List<Product2> getAddons() {
        return [SELECT Id, Name, Type__c, Version_Required__c, Description, Subtype__c, Product_Description__r.HTML_Description__c, (SELECT Id, Regular_Price__c, UnitPrice FROM PricebookEntries WHERE Pricebook2.IsActive=true AND Pricebook2.IsStandard=true) FROM Product2 WHERE Design__r.Id = :this.d.Id AND Family = 'Add-on' AND isActive = true ORDER BY Subtype__c];
    }
    
    public List<ConnectApi.FeedElement> getFeaturedContentPosts() {
        return ChatterMethods.getFeedElementsFromStringList(d.Feed_Gallery_Featured__c);
    }
    
    public Integer getCountContentPosts() {
        return [SELECT count() FROM Design__Feed WHERE ParentId = :d.Id AND Type = 'ContentPost' AND Visibility = 'AllUsers'];
    }
    
    public Integer getCountPosts() {
        return [SELECT count() FROM Design__Feed WHERE ParentId = :d.Id AND Visibility = 'AllUsers'];
    }
    
    public Integer getCountComments() {
        for (AggregateResult ar : [SELECT SUM(CommentCount) FROM Design__Feed WHERE ParentId = :d.Id AND Visibility = 'AllUsers'])  {
            Decimal dec = (Decimal)ar.get('expr0');
            if (dec != null) return dec.intValue();
        }
        return 0;
    }
    
    public List<Plot__c> getFollowingPlots() {
        ConnectApi.FollowingPage page = ConnectApi.ChatterUsers.getFollowings(Network.getNetworkId(), UserInfo.getUserId(), 'a0l');
        List<ID> ids = new List<ID>{};
        for (ConnectApi.Subscription sub : page.following) {
            ids.add(sub.subject.id);
        }
        return [SELECT Id, Name, StreetView__c, Area__c, acessibility_Road__c, Media__c, Perimeter__c, Position_road__c, Frost_zone__c, Length_side__c, Type__c, Snow_zone__c, Shape__c, Wind_zone__c, Built_up_area__c, Center__latitude__s, Center__longitude__s, Geometry__c, Geometry_building__c, Elevation_X__latitude__s, Elevation_Y__latitude__s, Elevation_X2__latitude__s, Elevation_Y2__latitude__s, Elevation_X__longitude__s, Elevation_Y__longitude__s, Elevation_X2__longitude__s, Elevation_Y2__longitude__s, Geometry_building_MAIL__c, MPZP_Ancillary_buildings__c, MPZP_Area_biological_min__c, MPZP_Area_max__c, MPZP_Colour__c, MPZP_Development_intensity_max__c, MPZP_Development_intensity_min__c, MPZP_Line_construction__c, MPZP_Main_purpose_area__c, MPZP_Max_height__c, MPZP_Max_number_of_floors__c, MPZP_Max_size__c, MPZP_Min_size__c, MPZP_other__c, MPZP_Roof_Lukar__c, MPZP_Roof_Slope_max__c, MPZP_Roof_Slope_min__c, MPZP_Roof_type__c, MPZP_Skylights__c, MPZP_URL__c, MPZP_URL2__c, MPZP_Width_facade__c, MPZP_Zoning_accompanying__c, (SELECT Id, OwnerId FROM Constructions__r) FROM Plot__c WHERE Id IN :ids];
    }
    
    public List<Plot__c> getPlotsForMyConstructions() {
        List<Id> ids = new List<Id>{};
        for (Construction__c c : [SELECT Id, Plot__r.Id FROM Construction__c WHERE OwnerId = :UserInfo.getUserId() AND Plot__c != null]) {
            ids.add(c.Plot__r.Id);
        }
        return [SELECT Id, Name, StreetView__c, Area__c, acessibility_Road__c, Media__c, Perimeter__c, Position_road__c, Frost_zone__c, Length_side__c, Type__c, Snow_zone__c, Shape__c, Wind_zone__c, Built_up_area__c, Center__latitude__s, Center__longitude__s, Geometry__c, Geometry_building__c, Elevation_X__latitude__s, Elevation_Y__latitude__s, Elevation_X2__latitude__s, Elevation_Y2__latitude__s, Elevation_X__longitude__s, Elevation_Y__longitude__s, Elevation_X2__longitude__s, Elevation_Y2__longitude__s, Geometry_building_MAIL__c, MPZP_Ancillary_buildings__c, MPZP_Area_biological_min__c, MPZP_Area_max__c, MPZP_Colour__c, MPZP_Development_intensity_max__c, MPZP_Development_intensity_min__c, MPZP_Line_construction__c, MPZP_Main_purpose_area__c, MPZP_Max_height__c, MPZP_Max_number_of_floors__c, MPZP_Max_size__c, MPZP_Min_size__c, MPZP_other__c, MPZP_Roof_Lukar__c, MPZP_Roof_Slope_max__c, MPZP_Roof_Slope_min__c, MPZP_Roof_type__c, MPZP_Skylights__c, MPZP_URL__c, MPZP_URL2__c, MPZP_Width_facade__c, MPZP_Zoning_accompanying__c, (SELECT Id, OwnerId FROM Constructions__r) FROM Plot__c WHERE Id IN :ids];
    }
    
    public List<ConnectApi.User> getConstructionOwners() {
        List<ID> ids = new List<ID>{};
        for (Construction__c c : [SELECT OwnerId FROM Construction__c WHERE Design__c = :this.d.Id AND Stage__c = 'Stan pod klucz' LIMIT 24]) {
            ids.add(c.OwnerId);
        }
        for (Construction__c c : [SELECT OwnerId FROM Construction__c WHERE Design__c = :this.d.Id AND Stage__c != 'Stan pod klucz' LIMIT :(24-ids.size())]) {
            ids.add(c.OwnerId);
        }
        List<ConnectApi.User> users = new List<ConnectApi.User>{};
        for (ConnectApi.BatchResult result : ConnectApi.ChatterUsers.getUserBatch(Network.getNetworkId(), ids)) {
            if (result.isSuccess()) {
                users.add((ConnectApi.User) result.getResult());
            }
        }
        return users;
    }
    
    public List<ConnectApi.User> getConstructionOwnersMore() {
        List<ID> ids = new List<ID>{};
        for (Construction__c c : [SELECT OwnerId FROM Construction__c WHERE Design__c = :this.d.Id AND Stage__c = 'Stan pod klucz' LIMIT 500]) {
            ids.add(c.OwnerId);
        }
        for (Construction__c c : [SELECT OwnerId FROM Construction__c WHERE Design__c = :this.d.Id AND Stage__c != 'Stan pod klucz' LIMIT :(500-ids.size())]) {
            ids.add(c.OwnerId);
        }
        List<ConnectApi.User> users = new List<ConnectApi.User>{};
        for (ConnectApi.BatchResult result : ConnectApi.ChatterUsers.getUserBatch(Network.getNetworkId(), ids)) {
            if (result.isSuccess()) {
                users.add((ConnectApi.User) result.getResult());
            }
        }
        return users;
    }
    
    public Integer getCountConstructionOwners() {
        return [SELECT count() FROM Construction__c WHERE Design__c = :this.d.Id];
    }
    
    public Integer getCountMyConstructions() {
        return [SELECT count() FROM Construction__c WHERE OwnerId = :UserInfo.getUserId()];
    }
    
    public Integer getCountMyConstructionsForDesign() {
        return [SELECT count() FROM Construction__c WHERE Design__c = :this.d.Id AND OwnerId = :UserInfo.getUserId()];
    }
    
    public List<Plot__c> getFollowingPlotsWithModel() {
        ConnectApi.FollowingPage page = ConnectApi.ChatterUsers.getFollowings(Network.getNetworkId(), UserInfo.getUserId(), 'a0l');
        Set<ID> ids = new Set<ID>{};
        for (ConnectApi.Subscription sub : page.following) {
            ids.add(sub.subject.id);
        }
        for (Construction__c c : [SELECT Id, Plot__r.Id FROM Construction__c WHERE OwnerId = :UserInfo.getUserId() AND Plot__c != null]) {
            ids.remove(c.Plot__r.Id);
        }
        return [SELECT Id, Name, Center__latitude__s, Center__longitude__s, Geometry_building_MAIL__c, Area__c, (SELECT Id, Thumbnail_Base64_String__c, OwnerId, Owner.Name, Position_Configuration__c, Mirrored__c, conf1__c, conf2__c, conf3__c, conf4__c, conf5__c, conf6__c, conf7__c, conf8__c, conf9__c, conf10__c, conf11__c, conf12__c, conf13__c, conf14__c, conf15__c, conf16__c, Design__r.Id, Design__r.Name, Design__r.Sketchfab_ID__c FROM Models__r WHERE Design__c = :this.d.Id AND OwnerId =: UserInfo.getUserId()) FROM Plot__c WHERE Id IN :ids];
    }
    
    public List<Plot__c> getPlotsWithModelForMyConstructions() {
        List<Id> ids = new List<Id>{};
        for (Construction__c c : [SELECT Id, Plot__r.Id FROM Construction__c WHERE OwnerId = :UserInfo.getUserId() AND Plot__c != null]) {
            ids.add(c.Plot__r.Id);
        }
        return [SELECT Id, Name, Center__latitude__s, Center__longitude__s, Geometry_building_MAIL__c, Area__c, (SELECT Id, Thumbnail_Base64_String__c, OwnerId, Owner.Name, Position_Configuration__c, Mirrored__c, conf1__c, conf2__c, conf3__c, conf4__c, conf5__c, conf6__c, conf7__c, conf8__c, conf9__c, conf10__c, conf11__c, conf12__c, conf13__c, conf14__c, conf15__c, conf16__c, Design__r.Id, Design__r.Name, Design__r.Sketchfab_ID__c FROM Models__r WHERE Design__c = :this.d.Id AND OwnerId =: UserInfo.getUserId()) FROM Plot__c WHERE Id IN :ids];
    }
    
    public List<Model__c> getMyDefaultModel() {
        return [SELECT Id, Thumbnail_Base64_String__c, OwnerId, Owner.Name, Position_Configuration__c, Mirrored__c, conf1__c, conf2__c, conf3__c, conf4__c, conf5__c, conf6__c, conf7__c, conf8__c, conf9__c, conf10__c, conf11__c, conf12__c, conf13__c, conf14__c, conf15__c, conf16__c, Design__r.Id, Design__r.Name, Design__r.Sketchfab_ID__c, Design__r.Thumbnail_Base_URL__c FROM Model__c WHERE Design__c = :this.d.Id AND OwnerId =: UserInfo.getUserId() AND Plot__c = null LIMIT 1];
    }
    
    public List<Model__c> getMyModels() {
        return [SELECT Id, Thumbnail_Base64_String__c, Name, Plot__r.Name, Plot__r.Area__c, LastModifiedDate, OwnerId, Owner.Name, Position_Configuration__c, Mirrored__c, conf1__c, conf2__c, conf3__c, conf4__c, conf5__c, conf6__c, conf7__c, conf8__c, conf9__c, conf10__c, conf11__c, conf12__c, conf13__c, conf14__c, conf15__c, conf16__c, Design__r.Id, Design__r.Name, Design__r.Sketchfab_ID__c, Design__r.Thumbnail_Base_URL__c FROM Model__c WHERE Design__c = :this.d.Id AND OwnerId =: UserInfo.getUserId()];
    }
    
    public List<Model__c> getMyAllModels() {
        return [SELECT Id, Thumbnail_Base64_String__c, Name, Plot__r.Name, Plot__r.Area__c, LastModifiedDate, OwnerId, Owner.Name, Position_Configuration__c, Mirrored__c, conf1__c, conf2__c, conf3__c, conf4__c, conf5__c, conf6__c, conf7__c, conf8__c, conf9__c, conf10__c, conf11__c, conf12__c, conf13__c, conf14__c, conf15__c, conf16__c, Design__r.Id, Design__r.Name, Design__r.Sketchfab_ID__c, Design__r.Thumbnail_Base_URL__c FROM Model__c WHERE OwnerId =: UserInfo.getUserId()];
    }
    
    public List<Design__c> getModelReadyDesigns() {
        return [SELECT Id, Name, Status__c, Sketchfab_ID__c, KW_Model__c, KW_Model_Preparation__c, Thumbnail_Base_URL__c FROM Design__c WHERE Sketchfab_ID__c != null ORDER BY Name LIMIT 2000];
    }
    
    public List<Design__c> getMyDesignAllModels() {
        return [SELECT Id, Name, Sketchfab_ID__c, KW_Model__c, KW_Model_Preparation__c, Thumbnail_Base_URL__c FROM Design__c WHERE Id IN (SELECT Design__c FROM Model__c WHERE OwnerId =: UserInfo.getUserId())];
    }
    
    public PageReference updateDesignCommunityDetails() {
        this.d.Community_Images__c = this.getCountContentPosts();
        this.d.Community_Builders__c = this.getCountConstructionOwners();
        PageReference pageRef = new ApexPages.StandardController(d).save();
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    @RemoteAction
    public static Model__c saveModel(ID modelId, ID designId, ID plotId, Boolean mirrored, String positionConfiguration, String conf1, String conf2, String conf3, String conf4, String conf5, String conf6, String conf7, String conf8, String conf9, String conf10, String conf11, String conf12, String conf13, String conf14, String conf15, String conf16) {
        Model__c model = new Model__c(Id = modelId, Design__c = designId, Plot__c = plotId, Mirrored__c = mirrored, Position_Configuration__c = positionConfiguration, conf1__c = conf1, conf2__c = conf2, conf3__c = conf3, conf4__c = conf4, conf5__c = conf5, conf6__c = conf6, conf7__c = conf7, conf8__c = conf8, conf9__c = conf9, conf10__c = conf10, conf11__c = conf11, conf12__c = conf12, conf13__c = conf13, conf14__c = conf14, conf15__c = conf15, conf16__c = conf16);
        upsert model;
        return model;
    }
    
    @RemoteAction
    public static Model__c saveNamedModel(ID modelId, ID designId, ID plotId, String name, Boolean mirrored, String positionConfiguration, String conf1, String conf2, String conf3, String conf4, String conf5, String conf6, String conf7, String conf8, String conf9, String conf10, String conf11, String conf12, String conf13, String conf14, String conf15, String conf16) {
        Model__c model = new Model__c(Id = modelId, Design__c = designId, Plot__c = plotId, Name=name, Mirrored__c = mirrored, Position_Configuration__c = positionConfiguration, conf1__c = conf1, conf2__c = conf2, conf3__c = conf3, conf4__c = conf4, conf5__c = conf5, conf6__c = conf6, conf7__c = conf7, conf8__c = conf8, conf9__c = conf9, conf10__c = conf10, conf11__c = conf11, conf12__c = conf12, conf13__c = conf13, conf14__c = conf14, conf15__c = conf15, conf16__c = conf16);
        upsert model;
        return model;
    }
    
    @RemoteAction
    public static void renameModel(ID modelId, String name) {
        Model__c model = [SELECT Id, Name FROM Model__c WHERE Id =: modelId LIMIT 1];
        model.Name = name;
        update model;
    }
    
    @RemoteAction
    public static void updateModelWithThumbnail(ID modelId, String imageData) {
        Model__c model = [SELECT Id, Thumbnail_Base64_String__c FROM Model__c WHERE Id =: modelId LIMIT 1];
        model.Thumbnail_Base64_String__c = imageData;
        update model;
    }
    
    @RemoteAction
    public static void deleteModel(ID modelId) {
        Model__c model = [SELECT Id FROM Model__c WHERE Id =: modelId LIMIT 1];
        delete model;
    }

}