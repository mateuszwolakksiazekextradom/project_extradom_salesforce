/**
 * Created by grzegorz.dlugosz on 15.05.2019.
 */
@isTest
private class DesignViewsPerMonthQueueable_Test {

    @IsTest
    private static void singleMatchTest() {
        // Prepare Test Data
        // Single design that will be matched
        Design__c testDesign = TestUtils.newDesign(new Design__c(Code__c = 'WAH1871', Views_Month__c = 0), true);

        Test.setMock(HttpCalloutMock.class, new GoogleAnalyticsMockImpl());
        Test.startTest();
        System.enqueueJob(new DesignViewsPerMonthQueueable('0'));
        Test.stopTest();

        Design__c newDesign = [SELECT Code__c, Views_Month__c FROM Design__c];
        System.debug('design after: ' + JSON.serialize(newDesign));

        System.assertEquals(28184, newDesign.Views_Month__c);
    }

    @IsTest
    private static void multipleMatchTest() {
        // Prepare Test Data
        Supplier__c supplier = TestUtils.newSupplier(new Supplier__c(), true);

        // Multiple designs that will match (2)
        String design1Code = 'WAH1871';
        String design2Code = 'WRC2706';
        Design__c design1 = TestUtils.newDesign(new Design__c(Name = 'design1', Full_Name__c = 'design1', Code__c = design1Code, Views_Month__c = 0, Supplier__c = supplier.Id), false);
        Design__c design2 = TestUtils.newDesign(new Design__c(Name = 'design1', Full_Name__c = 'design2', Code__c = design2Code, Views_Month__c = 0, Supplier__c = supplier.Id), false);

        // few more designs that will not be matched
        List<Design__c> designToInsertList = TestUtils.newDesigns(new Design__c(Views_Month__c = 0, Supplier__c = supplier.Id), 5, false);

        designToInsertList.add(design1);
        designToInsertList.add(design2);
        insert designToInsertList;

        Test.setMock(HttpCalloutMock.class, new GoogleAnalyticsMockImpl());
        Test.startTest();
        System.enqueueJob(new DesignViewsPerMonthQueueable('0'));
        Test.stopTest();

        List<Design__c> designList = [SELECT Code__c, Views_Month__c FROM Design__c];
        System.debug('design after: ' + JSON.serialize(designList));

        for (Design__c design : designList) {
            if (design.Code__c == design1Code) {
                System.assertEquals(28184, design.Views_Month__c);
            } else if (design.Code__c == design2Code) {
                System.assertEquals(39395, design.Views_Month__c);
            } else {
                System.assertEquals(0, design.Views_Month__c);
            }
        }
    }

    @IsTest
    private static void noMatchTest() {
        // Prepare Test Data
        Supplier__c supplier = TestUtils.newSupplier(new Supplier__c(), true);

        // Create few designs with no matching code
        TestUtils.newDesigns(new Design__c(Views_Month__c = 0, Supplier__c = supplier.Id), 5, true);

        Test.setMock(HttpCalloutMock.class, new GoogleAnalyticsMockImpl());
        Test.startTest();
        System.enqueueJob(new DesignViewsPerMonthQueueable('0'));
        Test.stopTest();

        List<Design__c> designList = [SELECT Code__c, Views_Month__c FROM Design__c];
        System.debug('design after: ' + JSON.serialize(designList));

        for (Design__c design : designList) {
            System.assertEquals(0, design.Views_Month__c);
        }
    }

    @IsTest
    private static void executeScheduler() {
        Test.setMock(HttpCalloutMock.class, new GoogleAnalyticsMockImpl());
        Test.startTest();
        DesignViewsPerMonthScheduler queueableScheduler = new DesignViewsPerMonthScheduler();
        String CRON_EXP = '0 0 23 * * ?';
        String jobId = System.schedule('testSchedulerOfQueueable', CRON_EXP, queueableScheduler);
        Test.stopTest();

        CronTrigger ct = [SELECT id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        System.assert(ct != null);
    }

    public class GoogleAnalyticsMockImpl implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest req) {
            HttpResponse res = new HttpResponse();
            // body is minified version of real response for 4 designs; nextPageToken is removed so the queueable won't try to chain more jobs
            String respBody = '{"reports":[{"columnHeader":{"dimensions":["ga:eventLabel"],"metricHeader":{"metricHeaderEntries":[{"name":"ga:totalEvents","type":"INTEGER"}]}},';
            respBody += '"data":{"rows":[{"dimensions":["WRC2706"],"metrics":[{"values":["39395"]}]},{"dimensions":["WRC2689"],"metrics":[{"values":["30446"]}]},';
            respBody += '{"dimensions":["WAH1871"],"metrics":[{"values":["28184"]}]},{"dimensions":["BSA1929"],"metrics":[{"values":["20728"]}]}],"totals":[{"values":["5247714"]}],';
            respBody += '"rowCount":24467,"minimums":[{"values":["1"]}],"maximums":[{"values":["39395"]}]}}]}';
            res.setHeader('Content-Type', 'application/json');
            res.setBody(respBody);
            res.setStatusCode(200);

            return res;
        }
    }
}