public with sharing class ProductBundleAddressesController {
	@AuraEnabled
    public static List<SelectOptionWrapper> getPaymentMethods() {
        List<SelectOptionWrapper> selectOptions = new List<SelectOptionWrapper> ();
        Map<String, String> picklists = Utils.getPicklistValues(Order.sObjectType, 'Payment_Method__c');
        for (String value : picklists.keySet()) {
            selectOptions.add(new SelectOptionWrapper(value, picklists.get(value)));
        }
        return selectOptions;
    }

    @AuraEnabled
    public static List<SelectOptionWrapper> getShippingMethods() {
        List<SelectOptionWrapper> selectOptions = new List<SelectOptionWrapper> ();
        Map<String, String> picklists = Utils.getPicklistValues(Order.sObjectType, 'Shipping_Method__c');
        for (String value : picklists.keySet()) {
            selectOptions.add(new SelectOptionWrapper(value, picklists.get(value)));
        }
        return selectOptions;	
    }
    
    @AuraEnabled
    public static List<Shipping_Cost_Setting__mdt > getShippingCostSettings() {
        List<Shipping_Cost_Setting__mdt> shippingCosts = [
            SELECT Payment_Method__c, Shipping_Method__c, Shipping_Cost__c FROM Shipping_Cost_Setting__mdt
        ];
        return shippingCosts;	
    }
}