@isTest
private class PlotViewRestrictedUseExtensionTest {

    static testMethod void myUnitTest() {
    
        Plot__c p = new Plot__c(Name='Example plot 2');
        insert p;
        
        PageReference pageRef = new PageReference('https://www.extradom.pl/dzialka-example-plot-2');
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(p);
        
        PlotViewRestrictedUseExtension ext2 = new PlotViewRestrictedUseExtension(sc);
        ext2.orderPlotAnalysis();
        
    }
    
}