@isTest
private class OrderTest {

    static testMethod void myUnitTestDesign() {
        
        test.startTest();
        
        Supplier__c s = new Supplier__c(Name='Test Supplier ABC', Full_Name__c='Test Supplier', Code__c='ABC');
        insert s;
        
        Design__c d1 = new Design__c(Name='Test Design', Full_Name__c='Test Design', Supplier__c=s.Id, Code__c='ABC1001');
        insert d1;
        
        PageReference pageRef = new PageReference('http://extradom.pl/zamowienie');
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('code', 'ABC1001');
        DesignOrderController designOrderController = new DesignOrderController();
        designOrderController.getDesign();
        designOrderController.billingFirstName = 'Jan';
        designOrderController.billingLastName = 'Nowak';
        designOrderController.billingEmail = 'test@extradom.pl';
        designOrderController.billingPhone = '717152060';
        designOrderController.save();
        designOrderController.save();
        
        test.stopTest();
    }
    
    static testMethod void myUnitTestCatalogue() {
        
        test.startTest();
        
        PageReference pageRef = new PageReference('http://extradom.pl/katalog');
        Test.setCurrentPage(pageRef);
        CatalogueOrderController catalogueOrderController = new CatalogueOrderController();
        catalogueOrderController.billingFirstName = 'Anna';
        catalogueOrderController.billingLastName = 'Kowalska';
        catalogueOrderController.billingEmail = 'test2@extradom.pl';
        catalogueOrderController.billingPhone = '717152061';
        catalogueOrderController.save();
        catalogueOrderController.save();
        //catalogueOrderController.billingPhone = '717152062';
        //catalogueOrderController.save();
        //catalogueOrderController.billingEmail = 'test3@extradom.pl';
        //catalogueOrderController.save();
        
        test.stopTest();
    }
    
    static testMethod void myUnitTestQuestion() {
        
        test.startTest();
        
        Supplier__c s = new Supplier__c(Name='Test Supplier ABC', Full_Name__c='Test Supplier', Code__c='ABC');
        insert s;
        
        Design__c d1 = new Design__c(Name='Test Design', Full_Name__c='Test Design', Supplier__c=s.Id, Code__c='ABC1001', Status__c='Active');
        insert d1;
        
        Lead l = new Lead();
        l.Code__c = 'ABC1001';
        l.FirstName = 'Jan';
        l.LastName = 'Nowak';
        l.Email = 'test@extradom.pl';
        l.Phone = '717152060';
        l.Source_Type__c = 'Question';
        l.Phone_Opt_In__c = true;
        l.Email_Opt_In__c = true;
        l.LeadSource = 'Extradom.pl';
        l.Company = 'Jan Nowak';
        insert l;
        
        /*
        l = new Lead();
        l.Code__c = 'ABC1001';
        l.FirstName = 'Jan';
        l.LastName = 'Nowak';
        l.Email = 'test@extradom.pl';
        l.Phone = '717152061';
        l.Source_Type__c = 'Question';
        l.Phone_Opt_In__c = true;
        l.Email_Opt_In__c = true;
        l.LeadSource = 'Extradom.pl';
        l.Company = 'Jan Nowak';
        insert l;
        */
        
        test.stopTest();
    }
    
    static testMethod void myUnitTestCompetition() {
        
        test.startTest();
        
        Lead l = new Lead();
        l.FirstName = 'Jan';
        l.LastName = 'Nowak';
        l.Email = 'test@extradom.pl';
        l.Phone = '717152060';
        l.Source_Type__c = 'Competition';
        l.Phone_Opt_In__c = true;
        l.Email_Opt_In__c = true;
        l.LeadSource = 'Extradom.pl';
        l.Company = 'Jan Nowak';
        l.Has_Design__c = '';
        insert l;
        
        l = new Lead();
        l.FirstName = 'Jan';
        l.LastName = 'Nowak';
        l.Email = 'test@extradom.pl';
        l.Phone = '717152060';
        l.Source_Type__c = 'Competition';
        l.Phone_Opt_In__c = true;
        l.Email_Opt_In__c = true;
        l.LeadSource = 'Extradom.pl';
        l.Company = 'Jan Nowak';
        l.Has_Design__c = 'No';
        insert l;
        
        test.stopTest();
    }
    
}