/**
 * Created by grzegorz.dlugosz on 22.05.2019.
 */
@isTest
private class AuctionBidControllerTest {

    @isTest
    public static void getSingleUserBid() {
        // create offer / aukcja
        Offer__c offer = TestUtils.newOffer(new Offer__c(), true);
        // put 1 bid of current user under this offer
        Bid__c bid = TestUtils.newBid(new Bid__c(Offer__c = offer.Id), true);

        Test.startTest();
        // this should return the bid
        AuctionBidController.UserBidsWrapper usrBidWrapper = AuctionBidController.getUserBidsSrv(offer.Id);
        Test.stopTest();

        System.assert(usrBidWrapper != null);
        System.assertEquals(1 ,usrBidWrapper.bidList.size());
        System.assert(usrBidWrapper.auctionEnd != null);
        System.assert(usrBidWrapper.currentTime != null);
    }

    @isTest
    public static void getNoUserBids() {
        // create offer / aukcja
        Offer__c offer = TestUtils.newOffer(new Offer__c(), true);

        Test.startTest();
        // this should return no bids
        AuctionBidController.UserBidsWrapper usrBidWrapper = AuctionBidController.getUserBidsSrv(offer.Id);
        Test.stopTest();

        System.assert(usrBidWrapper != null);
        System.assertEquals(null ,usrBidWrapper.bidList);
        System.assert(usrBidWrapper.auctionEnd != null);
        System.assert(usrBidWrapper.currentTime != null);
    }

    @isTest
    public static void updateUserBid() {
        // create offer / aukcja
        Offer__c offer = TestUtils.newOffer(new Offer__c(), true);
        // put 1 bid of current user under this offer
        Bid__c bid = TestUtils.newBid(new Bid__c(Offer__c = offer.Id), true);

        System.assertNotEquals(5200, bid.Value__c);

        // prepare bid for new value/update
        bid.Value__c = 5200;

        Test.startTest();
        String result = AuctionBidController.saveUserBidSrv(bid);
        Test.stopTest();

        List<Bid__c> bidListAfterUpdate = [SELECT Value__c FROM Bid__c WHERE Id = :bid.Id];

        System.assertEquals('SUCCESS', result);
        System.assertEquals(5200, bidListAfterUpdate.get(0).Value__c);
    }

    @isTest
    public static void updateUserBidWithException() {
        // create offer / aukcja
        Offer__c offer = TestUtils.newOffer(new Offer__c(), true);
        // put 1 bid of current user under this offer
        Bid__c bid = TestUtils.newBid(new Bid__c(Offer__c = offer.Id), true);

        String execptionMessage;

        // make a local update to bid that will cause an exception - because of validation rule
        bid.Value__c = 100;

        Test.startTest();
        try {
            String result = AuctionBidController.saveUserBidSrv(bid);
        } catch (Exception e) {
            execptionMessage = e.getMessage();
        }
        Test.stopTest();

        List<Bid__c> bidListAfterUpdate = [SELECT Value__c FROM Bid__c WHERE Id = :bid.Id];

        System.assertNotEquals(5200, bidListAfterUpdate.get(0).Value__c);
        System.assert(String.isNotBlank(execptionMessage ) && execptionMessage.contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'));
    }

    @isTest
    public static void insertAdditionalBid() {
        // create offer / aukcja
        Offer__c offer = TestUtils.newOffer(new Offer__c(), true);
        // put 1 bid of current user under this offer
        Bid__c bid = TestUtils.newBid(new Bid__c(Offer__c = offer.Id), true);

        Test.startTest();
        // insert second bid
        String result = AuctionBidController.addSecondBidSrv(offer.Id);
        Test.stopTest();

        System.assertEquals('SUCCESS', result);
        System.assertEquals(2, [SELECT Count() FROM Bid__c WHERE Offer__c = :offer.Id]);
    }

    @isTest
    public static void insertAdditionalBidException() {
        // create offer / aukcja
        Offer__c offer = TestUtils.newOffer(new Offer__c(), true);
        // put 1 bid of current user under this offer
        Bid__c bid = TestUtils.newBid(new Bid__c(Offer__c = offer.Id), true);
        String errorMessage = '';

        Test.startTest();
        // close the offer
        offer.End_Date__c = System.now().addMinutes(-100);
        update offer;
        // try to insert second bid, it should not be possible due to validation rule
        try {
            String result = AuctionBidController.addSecondBidSrv(offer.Id);
        } catch (Exception e) {
            errorMessage = e.getMessage();
        }
        Test.stopTest();

        System.assert(errorMessage.contains('Something went wrong while creating the bid record'));
    }
}