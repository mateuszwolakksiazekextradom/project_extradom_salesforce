public class DesignSmallXMLEmail implements Database.Batchable<Design__c> {
    
    XML_Email__c xemail = XML_Email__c.getInstance();        
    List<Design__c> selected;
    
    private final Design__c d;
    public DesignSmallXMLEmail (ApexPages.StandardController stdController) {
        this.d = (Design__c)stdController.getRecord();
    }
    
    private ApexPages.StandardSetController stdSetController;
    public DesignSmallXMLEmail (ApexPages.StandardSetController stdSetController)
    {
        this.stdSetController = stdSetController;
    }
    
    public DesignSmallXMLEmail (List<Design__c> selectedDesigns) {
       this.selected = new List<Design__c>(selectedDesigns);
    }
    
    public PageReference sendSmallXMLEmail() {                       
        List<Design__c> selectedDesigns = (List<Design__c>) stdSetController.getSelected();
        List<Document> ds = [SELECT Id FROM Document WHERE Folder.Name = :xemail.XML_Folder__c];
        delete ds;
        
        Database.executebatch(new DesignSmallXMLEmail (selectedDesigns),5);
                     
        return stdSetController.save();
    }
    
    public Iterable<Design__c> start(Database.BatchableContext BC){
        return selected;             
    }
    
    public void execute(Database.BatchableContext BC, List<Design__c> scope){
        List<Document> ds = new List<Document>();
        List<Folder> fs = [SELECT Id FROM Folder WHERE Name = :xemail.XML_Folder__c];
        Id fid;
        if(!Test.isRunningTest()){
            fid=fs[0].id;
        }
        for (Design__c design : scope) {
            PageReference xml = Page.xml4dtp_small2_attachment;                                                 
            xml.getParameters().put('id',design.Id);
            Document d = new Document();
            d.Name = design.Code__c+'.xml';
            if(Test.isRunningTest()){
                d.Body = Blob.valueOf('test');
            } else {
                Blob b = xml.getContent();
                d.Body = b;
            }
            d.Type = 'xml';
            d.FolderId = fid;
            ds.add(d);         
        }
        if(!Test.isRunningTest()) {         
            insert ds;
        }
    }
    
    public void finish(Database.BatchableContext BC){
        List<Messaging.EmailFileAttachment> emailAttachments = new List<Messaging.EmailFileAttachment>();
        for (Document d : [SELECT Id, Body, Name FROM Document WHERE Folder.Name = :xemail.XML_Folder__c]) {
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            efa.setFileName(d.Name);
            efa.setBody(d.Body);            
            emailAttachments.add(efa);
        }
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();        
        message.setPlainTextBody('SmallXML');
        message.toAddresses = new String[] {xemail.Email__c==null ? 'bartosz.lukjanski@extradom.pl' : xemail.Email__c};
        message.setTargetObjectId(UserInfo.getUserId());
        message.setSaveAsActivity(false);
        message.setFileAttachments(emailAttachments);
        messages.add(message);       
        Messaging.sendEmail(messages);
    }


}