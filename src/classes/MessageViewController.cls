public with sharing class MessageViewController {

    private final String messageId;
    
    public MessageViewController() {
        this.messageId = ApexPages.currentPage().getParameters().get('id');
    }
    
    public ConnectApi.ChatterMessage getMessage() {
        return ConnectApi.ChatterMessages.getMessage(this.messageId);
    }

}