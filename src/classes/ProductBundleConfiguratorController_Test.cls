@IsTest
private class ProductBundleConfiguratorController_Test {

    @IsTest
    private static void shouldReturnVersionPicklist() {
        // given
        List<Schema.PicklistEntry> picklistEntries = OrderItem.sObjectType.getDescribe().fields.getMap().get('Version__c').getDescribe().getPickListValues();
        Map<String, String> picklists = new Map<String, String> ();
        for (Schema.PicklistEntry pe : picklistEntries) {
            picklists.put(pe.getValue(), pe.getLabel());
        }

        // when
        List<SelectOptionWrapper> versions = ProductBundleConfiguratorController.getVersionValues();

        // then
        System.assertEquals(picklists.size(), versions.size());
        for (SelectOptionWrapper version : versions) {
            System.assert(picklists.containsKey(version.value));
            System.assertEquals(picklists.get(version.value), version.label);
        }
    }

    @IsTest
    private static void shouldReturnDiscountTypesPicklist() {
        // given
        List<Schema.PicklistEntry> picklistEntries = OrderItem.sObjectType.getDescribe().fields.getMap().get('Discount_Type__c').getDescribe().getPickListValues();
        Map<String, String> picklists = new Map<String, String> ();
        for (Schema.PicklistEntry pe : picklistEntries) {
            picklists.put(pe.getValue(), pe.getLabel());
        }

        // when
        List<SelectOptionWrapper> discountTypes = ProductBundleConfiguratorController.getDiscountTypes();

        // then
        System.assertEquals(picklists.size(), discountTypes.size());
        for (SelectOptionWrapper discountType : discountTypes) {
            System.assert(picklists.containsKey(discountType.value));
            System.assertEquals(picklists.get(discountType.value), discountType.label);
        }
    }

    @IsTest
    private static void shouldPrepareOrderItem() {
        // given
        Account acc = new Account (Name = 'Test Name');
        insert acc;
        Design__c design = [SELECT Id, Name FROM Design__c LIMIT 1];
        Pricebook2 pb = [SELECT Id FROM Pricebook2 WHERE Name = 'Test Pricebook'];
        PricebookEntry pe = [SELECT Id FROM PricebookEntry WHERE Product2.Name = 'Test Product 2' LIMIT 1];
        Order order = new Order (
                AccountId = acc.Id, EffectiveDate = System.now().date(), Status = 'Draft', Pricebook2Id = pb.Id,
                RecordTypeId = ConstUtils.ORDER_REGULAR_RT_ID
        );
        insert order;

        // when
        List<OrderItemWrapper> oiws = ProductBundleConfiguratorController.prepareOrderItem(order.Id, pe.Id, design.Id);

        // then
        System.assertEquals(1, oiws.size());
        OrderItemWrapper oiw = oiws.get(0);
        System.assertEquals(200, oiw.totalPrice);
        System.assertEquals(180, oiw.minPrice);
        System.assertEquals(200, oiw.originalListPrice);
        System.assertEquals(0, oiw.discountPercent);
        System.assertEquals(0, oiw.orderItem.Discount_Sales__c);
        System.assertNotEquals(null, oiw.design);
        System.assertEquals(design.Name, oiw.design.name);
        System.assertNotEquals(null, oiw.product);
        System.assertEquals('Test Product 2', oiw.product.name);
        System.assertEquals(200, oiw.orderItem.UnitPrice);
        System.assertEquals(1, oiw.orderItem.Quantity);
        System.assertEquals(200, oiw.orderItem.List_Price__c);
    }

    @IsTest
    private static void shouldSaveOrderItems() {
        // given
        Account acc = new Account (Name = 'Test Name');
        insert acc;
        Design__c design = [SELECT Id, Name FROM Design__c LIMIT 1];
        Pricebook2 pb = [SELECT Id FROM Pricebook2 WHERE Name = 'Test Pricebook'];
        PricebookEntry pe = [SELECT Id FROM PricebookEntry WHERE Product2.Name = 'Test Product 2' AND Pricebook2Id = :pb.Id LIMIT 1];
        Order order = new Order (
                AccountId = acc.Id, EffectiveDate = System.now().date(), Status = 'Draft', Pricebook2Id = pb.Id,
                RecordTypeId = ConstUtils.ORDER_REGULAR_RT_ID
        );
        insert order;
        OrderItem oi1 = new OrderItem(
                PricebookEntryId = pe.Id, Quantity = 2, UnitPrice = 100,
                List_Price__c = 400, Design__c = design.Id, OrderId = order.Id
        );
        OrderItem oi2 = new OrderItem(
                PricebookEntryId = pe.Id, Quantity = 1, UnitPrice = 100,
                List_Price__c = 100, Design__c = null, OrderId = order.Id
        );
        insert new List<OrderItem>{oi1, oi2};


        // when
        OrderItemWrapper oiw1 = ProductBundleConfiguratorController.prepareOrderItem(order.Id, pe.Id, design.Id).get(0);
        OrderItemWrapper oiw2 = ProductBundleConfiguratorController.prepareOrderItem(order.Id, pe.Id, design.Id).get(0);
        oiw2.orderItem.Quantity = 2;
        ProductBundleConfiguratorController.saveOrderItems(order.Id, JSON.serialize(new List<OrderItem> {oiw1.orderItem, oiw2.orderItem}));
        Map<Id, OrderItem> orderItems =
                new Map<Id, OrderItem>([SELECT Id, Quantity, PricebookEntryId, UnitPrice, List_Price__c, Design__c FROM OrderItem WHERE OrderId = :order.Id]);

        // then
        System.assertEquals(2, orderItems.size());
        System.assert(!orderItems.containsKey(oi1.Id) && !orderItems.containsKey(oi2.Id));
        for (OrderItem orderItem : orderItems.values()) {
            System.assertEquals(design.Id, orderItem.Design__c);
            System.assertEquals(200, orderItem.UnitPrice);
            System.assert(orderItem.Quantity == 1 || orderItem.Quantity == 2);
            System.assertEquals(200, orderItem.List_Price__c);
            System.assertEquals(pe.Id, orderItem.PricebookEntryId);
        }
    }

    @IsTest
    private static void shouldReturnOrderItemsForOrder() {
        // given
        Account acc = new Account (Name = 'Test Name');
        insert acc;
        Design__c design = [SELECT Id, Name FROM Design__c LIMIT 1];
        Pricebook2 pb = [Select Id FROM Pricebook2 WHERE Name = 'Test Pricebook'];
        Map<Id, PricebookEntry> pricebookEntryMap = new Map<Id, PricebookEntry> ([SELECT Id, Product2.Name FROM PricebookEntry WHERE Pricebook2Id = :pb.Id]);
        Map<String, Id> productNameToPricebookId = new Map<String, Id> ();
        for (Id priceEntryId : pricebookEntryMap.keySet()) {
            productNameToPricebookId.put(pricebookEntryMap.get(priceEntryId).Product2.Name, priceEntryId);
        }
        Order o1 = new Order (
                AccountId = acc.Id, EffectiveDate = System.now().date(), Status = 'Draft', Pricebook2Id = pb.Id,
                RecordTypeId = ConstUtils.ORDER_REGULAR_RT_ID
        );
        Order o2 = new Order (
                AccountId = acc.Id, EffectiveDate = System.now().date(), Status = 'Draft', Pricebook2Id = pb.Id,
                RecordTypeId = ConstUtils.ORDER_REGULAR_RT_ID
        );
        insert new List<Order>{o1, o2};
        OrderItem oi1 = new OrderItem(
                PricebookEntryId = productNameToPricebookId.get('Test Product 2'), Quantity = 2, Discount_Sales__c = 300,
                List_Price__c = 400, Design__c = design.Id, OrderId = o1.Id, UnitPrice = 0
        );
        OrderItem oi2 = new OrderItem(
                PricebookEntryId = productNameToPricebookId.get('Test Product 1'), Quantity = 1, Discount_Sales__c = 0,
                List_Price__c = 100, Design__c = null, OrderId = o1.Id, UnitPrice = 0
        );
        OrderItem oi3 = new OrderItem(
                PricebookEntryId = productNameToPricebookId.get('Test Product 2'), Quantity = 3, Discount_Sales__c = 150,
                List_Price__c = 250, Design__c = design.Id, OrderId = o2.Id, UnitPrice = 0
        );
        OrderItem oi4 = new OrderItem(
                PricebookEntryId = productNameToPricebookId.get('Test Product 1'), Quantity = 2, Discount_Sales__c = 50,
                List_Price__c = 150, Design__c = null, OrderId = o2.Id, UnitPrice = 0
        );
        insert new List<OrderItem>{oi1, oi2, oi3, oi4};

        // when
        List<OrderItemWrapper> orderItemWrappers = ProductBundleConfiguratorController.getOrderItems(o1.Id);

        // then
        System.assertEquals(2, orderItemWrappers.size());
        for (OrderItemWrapper oiw : orderItemWrappers) {
            System.assert(oi1.PricebookEntryId == oiw.pricebookEntryId || oi2.PricebookEntryId == oiw.pricebookEntryId);
            if (oi1.PricebookEntryId == oiw.pricebookEntryId) {
                //System.assertEquals(200, oiw.totalPrice);
                System.assertEquals(360, oiw.minPrice);
                System.assertEquals(200, oiw.originalListPrice);
                System.assertEquals(75, oiw.discountPercent);
                System.assertEquals(300, oiw.orderItem.Discount_Sales__c);
                System.assertNotEquals(null, oiw.design);
                System.assertEquals(design.Name, oiw.design.name);
                System.assertNotEquals(null, oiw.product);
                System.assertEquals('Test Product 2', oiw.product.name);
                System.assertEquals(100, oiw.orderItem.UnitPrice);
                System.assertEquals(2, oiw.orderItem.Quantity);
                System.assertEquals(400, oiw.orderItem.List_Price__c);
            } else {
                System.assertEquals(100, oiw.totalPrice);
                System.assertEquals(null, oiw.minPrice);
                System.assertEquals(100, oiw.originalListPrice);
                System.assertEquals(0, oiw.discountPercent);
                System.assertEquals(0, oiw.orderItem.Discount_Sales__c);
                System.assertEquals(null, oiw.design);
                System.assertNotEquals(null, oiw.product);
                System.assertEquals('Test Product 1', oiw.product.name);
                System.assertEquals(100, oiw.orderItem.UnitPrice);
                System.assertEquals(1, oiw.orderItem.Quantity);
                System.assertEquals(100, oiw.orderItem.List_Price__c);
            }
        }
    }

    @IsTest
    private static void shouldReturnRelatedOrderItems() {
        // given
        List<PricebookEntry> existingEntries = [
                SELECT Id, Product2.Design__r.Type__c, Product2.Design__c, Product2.Family, Product2.Name FROM PricebookEntry WHERE Pricebook2Id = :Test.getStandardPricebookId()
        ];
        List<Product2> productsToInsert = new List<Product2> ();
        List<PricebookEntry> pesToInsert = new List<PricebookEntry> ();
        Map<String, PricebookEntry> pesByFamily = new Map<String, PricebookEntry> ();
        Integer i = 0;
        for (PricebookEntry existingEntry : existingEntries) {
            productsToInsert.add(new Product2(
                    IsActive = true, Family = ConstUtils.PRODUCT_GIFT_FAMILY, Default_Extra_by_List_Price__c = 15.0,
                    Default_Extra__c = true, Default_Extra_by_Design_Type__c = existingEntry.Product2.Design__r.Type__c,
                    Name = 'ValidExtraValidDesignTypeGiftFamilyValidPrice_' + String.valueOf(i)
            ));
            productsToInsert.add(new Product2(
                    IsActive = true, Family = ConstUtils.PRODUCT_GIFT_FAMILY, Default_Extra_by_List_Price__c = 15.0,
                    Default_Extra__c = false, Default_Extra_by_Design_Type__c = existingEntry.Product2.Design__r.Type__c,
                    Name = 'InvalidExtraValidDesignTypeGiftFamilyValidPrice_' + String.valueOf(i)
            ));
            productsToInsert.add(new Product2(
                    IsActive = true, Family = ConstUtils.PRODUCT_EXTRA_FAMILY, Default_Extra_by_List_Price__c = 15.0,
                    Default_Extra__c = true, Default_Extra_by_Design_Type__c = existingEntry.Product2.Design__r.Type__c,
                    Name = 'ValidExtraValidDesignTypeExtraFamilyValidPrice_' + String.valueOf(i)
            ));
            productsToInsert.add(new Product2(
                    IsActive = true, Family = ConstUtils.PRODUCT_DESIGN_FAMILY, Default_Extra_by_List_Price__c = 15.0,
                    Default_Extra__c = true, Default_Extra_by_Design_Type__c = existingEntry.Product2.Design__r.Type__c,
                    Name = 'ValidExtraValidDesignTypeDesignFamilyValidPrice_' + String.valueOf(i)
            ));
            productsToInsert.add(new Product2(
                    IsActive = true, Family = ConstUtils.PRODUCT_GIFT_FAMILY, Default_Extra_by_List_Price__c = 15.0,
                    Default_Extra__c = true, Default_Extra_by_Design_Type__c = null,
                    Name = 'ValidExtraInvalidDesignTypeGiftFamilyValidPrice_' + String.valueOf(i)
            ));
            productsToInsert.add(new Product2(
                    IsActive = true, Family = ConstUtils.PRODUCT_GIFT_FAMILY, Default_Extra_by_List_Price__c = 250.0,
                    Default_Extra__c = true, Default_Extra_by_Design_Type__c = existingEntry.Product2.Design__r.Type__c,
                    Name = 'ValidExtraValidDesignTypeGiftFamilyInvalidPrice_' + String.valueOf(i)
            ));
            pesByFamily.put(existingEntry.Product2.Family, existingEntry);
            i++;
        }
        insert productsToInsert;
        for (Product2 product : productsToInsert) {
            pesToInsert.add(new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(), UnitPrice = 5.0, Product2Id = product.Id, IsActive = true));
        }
        insert pesToInsert;
        Account acc = new Account (Name = 'Test Name');
        insert acc;
        Order o = new Order (
                AccountId = acc.Id, EffectiveDate = System.now().date(), Status = 'Draft', Pricebook2Id = Test.getStandardPricebookId(),
                RecordTypeId = ConstUtils.ORDER_REGULAR_RT_ID
        );
        insert o;

        // when
        List<OrderItemWrapper> orderItemsForDesignProduct = new List<OrderItemWrapper> ();
        List<OrderItemWrapper> orderItemsForGiftProduct = new List<OrderItemWrapper> ();
        Test.startTest();
        orderItemsForDesignProduct = ProductBundleConfiguratorController.prepareOrderItem(
                o.Id, pesByFamily.get(ConstUtils.PRODUCT_DESIGN_FAMILY).Id, pesByFamily.get(ConstUtils.PRODUCT_DESIGN_FAMILY).Product2.Design__c
        );
        orderItemsForGiftProduct = ProductBundleConfiguratorController.prepareOrderItem(
                o.Id, pesByFamily.get(ConstUtils.PRODUCT_GIFT_FAMILY).Id, pesByFamily.get(ConstUtils.PRODUCT_DESIGN_FAMILY).Product2.Design__c
        );
        Test.stopTest();

        // then
        System.assertEquals(1, orderItemsForGiftProduct.size());
        System.assertEquals(pesByFamily.get(ConstUtils.PRODUCT_GIFT_FAMILY).Id, orderItemsForGiftProduct.get(0).orderItem.PricebookEntryId);
        System.assertEquals(3, orderItemsForDesignProduct.size());
        for (OrderItemWrapper oiw : orderItemsForDesignProduct) {
            System.assert(oiw.product.Name == pesByFamily.get(ConstUtils.PRODUCT_DESIGN_FAMILY).Product2.Name
                    || oiw.product.Name.startsWith('ValidExtraValidDesignTypeExtraFamilyValidPrice_')
                    || oiw.product.Name.startsWith('ValidExtraValidDesignTypeGiftFamilyValidPrice_')
            );
        }
    }

    @TestSetup
    private static void setupData() {
        Supplier__c newSupplier = new Supplier__c(Name = 'Supplier', Code__c = '11', Full_Name__c = 'Great Supplier');
        insert newSupplier;

        Design__c design = new Design__c (Name = 'First Design', Supplier__c = newSupplier.Id, Full_Name__c = 'First Design', Type__c = ConstUtils.DESIGN_HOUSE_TYPE);
        insert design;

        Pricebook2 pricebook = new Pricebook2(Name = 'Test Pricebook', IsActive = true);
        insert pricebook;

        Product2 product1 = new Product2(
                Name = 'Test Product 1', Family = ConstUtils.PRODUCT_GIFT_FAMILY, IsActive = true, Design__c = null,
                Version_Required__c = false, Line_Description_Required__c = false, Max_Discount__c = null
        );
        Product2 product2 = new Product2(
                Name = 'Test Product 2', Family = ConstUtils.PRODUCT_DESIGN_FAMILY, IsActive = true, Design__c = design.Id,
                Version_Required__c = true, Line_Description_Required__c = true, Max_Discount__c = 10
        );
        insert new List<Product2> {product1, product2};

        PricebookEntry pe1 = new PricebookEntry(Product2Id = product1.Id, IsActive = true, Pricebook2Id = pricebook.Id, UnitPrice = 100);
        PricebookEntry spe1 = new PricebookEntry(
                Product2Id = product1.Id, IsActive = true, Pricebook2Id = Test.getStandardPricebookId(), UnitPrice = 100
        );
        PricebookEntry pe2 = new PricebookEntry(Product2Id = product2.Id, IsActive = true, Pricebook2Id = pricebook.Id, UnitPrice = 200);
        PricebookEntry spe2 = new PricebookEntry(
                Product2Id = product2.Id, IsActive = true, Pricebook2Id = Test.getStandardPricebookId(), UnitPrice = 200
        );
        insert new List<PricebookEntry> {spe1, spe2};
        insert new List<PricebookEntry> {pe1, pe2};
    }
}