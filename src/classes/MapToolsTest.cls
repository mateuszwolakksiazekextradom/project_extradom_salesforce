@IsTest
private class MapToolsTest {

    static testMethod void myUnitTest() {
    
        TERC__c terc = new TERC__c(Name='dolno,trzebnicki',TERC__c=220);
        insert terc;
        
        Test.startTest();
    
        List<Decimal> point = new List<Decimal>{0,0};
        List<List<Decimal>> polygon = new List<List<Decimal>>{new List<Decimal>{-1,-1}, new List<Decimal>{-1,1}, new List<Decimal>{1,0}};
        System.assertEquals(MapTools.isPointInsidePolygon(point, polygon), true);
        
        point = new List<Decimal>{51.287260, 17.253516};
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('queryExternalPlotResponse');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, mock);
        FeedExtension.showPlotForGivenPoint(51.287260, 17.253516, 'dolnośląskie', 'trzebnicki');
        MapTools.plotIdForGivenPoint(new List<Decimal>{51.287260, 17.253516}, 'dolnośląskie', 'trzebnicki', 'Trzebnica');
        
        MapTools.getLocationIdByTERC('0220');
        
        Test.stopTest();
        
    }
    
    static testMethod void myUnitTest2() {
    
        TERC__c terc = new TERC__c(Name='dolno,trzebnicki',TERC__c=220);
        insert terc;
        
        Test.startTest();
    
        List<Decimal> point = new List<Decimal>{0,0};
        List<List<Decimal>> polygon = new List<List<Decimal>>{new List<Decimal>{-1,-1}, new List<Decimal>{-1,1}, new List<Decimal>{1,0}};
        System.assertEquals(MapTools.isPointInsidePolygon(point, polygon), true);
        
        point = new List<Decimal>{51.287260, 17.253516};
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('queryExternalPlotResponse');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, mock);
        
        Supplier__c s = new Supplier__c(Name='Test Supplier ABC', Full_Name__c='Test Supplier', Code__c='ABC');
        insert s;
        
        Design__c d1 = new Design__c(Name='Test Design', Full_Name__c='Test Design', Supplier__c=s.Id, Code__c='ABC1001');
        insert d1;
        
        Construction__c c = FeedExtension.createConstruction(null, 'Paperwork');
        Construction__c c2 = FeedExtension.editConstructionWithDesign(c.Id, 'Paperwork', d1.Id, null, null, null, null, null, 'Default');
        System.assertEquals(c2.Design__c,d1.Id);
        
        Test.stopTest();
        
    }
    
}