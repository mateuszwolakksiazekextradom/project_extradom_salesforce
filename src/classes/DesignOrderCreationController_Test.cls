@IsTest
private class DesignOrderCreationController_Test {
    @IsTest
    private static void shouldCreateOrder() {
        // given
        Product2 product = [SELECT ID, ProductCode FROM Product2 LIMIT 1];
        PageReference designOrderPage = Page.DesignOrderView;
        Test.setCurrentPage(designOrderPage);
        ApexPages.currentPage().getParameters().put('code', product.ProductCode);
        ApexPages.currentPage().getHeaders().put('gaClientId', 'test.test');

        // when
        DesignOrderCreationController ctrl = new DesignOrderCreationController();
        ctrl.getDesign();
        ctrl.getAddOnProducts();
        ctrl.billingCompany = 'TestCompany';
        ctrl.billingLastName = 'TestName';
        ctrl.billingEmail = 'test@test.com';
        ctrl.billingPhone = '500000000';
        ctrl.shippingPhone = '500000000';
        ctrl.shippingLastName = 'TestName';
        ctrl.optInRegulations = true;
        ctrl.optInPrivacyPolicy = true;
        ctrl.optInMarketing = true;
        ctrl.optInMarketingExtended = true;
        ctrl.optInMarketingPartner = true;
        ctrl.optInLeadForward = true;
        ctrl.save();
        List<Order> orders = [SELECT Id, Account.gaClientId__c FROM Order];
        
        // then
        System.assert(orders.size() > 0);
        System.assertEquals(orders[0].Account.gaClientId__c, 'test.test');
    }
    
    @TestSetup
    private static void setupData() {
        Supplier__c newSupplier = new Supplier__c(Name = 'Supplier', Code__c = '11', Full_Name__c = 'Great Supplier');
        insert newSupplier;

        Design__c design = new Design__c (Name = 'First Design', Supplier__c = newSupplier.Id, Full_Name__c = 'First Design', Type__c = ConstUtils.DESIGN_HOUSE_TYPE);
        insert design;

        Pricebook2 pricebook = new Pricebook2(Name = 'Test Pricebook', IsActive = true);
        insert pricebook;

        Product2 product = new Product2(
                Name = 'Test Product', Family = ConstUtils.PRODUCT_DESIGN_FAMILY, IsActive = true, Design__c = design.Id,
                Version_Required__c = true, Line_Description_Required__c = true, Max_Discount__c = 10, ProductCode = 'Test'
        );
        insert product;

        PricebookEntry spe = new PricebookEntry(
                Product2Id = product.Id, IsActive = true, Pricebook2Id = Test.getStandardPricebookId(), UnitPrice = 100
        );
        insert spe;
    }
}