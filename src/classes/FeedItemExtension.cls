public without sharing class FeedItemExtension {
    private ApexPages.StandardController controller {get; set;}
    private FeedItem fi {get; set;}
    public ID parentId {get; set;}
    public FeedItemExtension(ApexPages.StandardController controller) {
        this.controller = controller;
        this.fi = (FeedItem)controller.getRecord();
    }
    public PageReference reparent() {
        FeedItem fiSource = [SELECT Id, CreatedById, CreatedDate, Body, IsRichText, LastEditById, LastEditDate, LastModifiedDate, LinkUrl, NetworkScope, ParentId, RelatedRecordId, Revision, Status, Title, Type, Visibility, (SELECT ID, FeedItemId, FeedEntityId, InsertedById, CreatedDate, CreatedById FROM FeedLikes), (SELECT Id, EntityId, NetworkId, TopicId, CreatedDate, CreatedById FROM TopicAssignments), (SELECT Id, CommentBody, CommentType, FeedItemId, IsRichText, LastEditById, LastEditDate, ParentId, RelatedRecordId, Revision, Status, CreatedById, CreatedDate FROM FeedComments) FROM FeedItem WHERE Id = :this.fi.Id];
        FeedItem fiCopy = fiSource.clone(false, true, true, true);
        fiCopy.ParentId = this.parentId;
        fiCopy.NetworkScope = 'AllNetworks';
        List<FeedLike> feedLikesCopy = fiCopy.FeedLikes.deepClone(false, true, true);
        List<FeedComment> feedCommentsCopy = fiCopy.FeedComments.deepClone(false, true, true);
        List<TopicAssignment> topicAssignmentsCopy = fiCopy.TopicAssignments.deepClone(false, true, true);
        insert fiCopy;
        for (FeedLike fl : feedLikesCopy) {
            fl.FeedEntityId = fiCopy.Id;
            fl.FeedItemId = fiCopy.Id;
        }
        insert feedLikesCopy;
        for (FeedComment fc : feedCommentsCopy) {
            fc.FeedItemId = fiCopy.Id;
        }
        insert feedCommentsCopy;
        for (TopicAssignment ta : topicAssignmentsCopy) {
            ta.EntityId = fiCopy.Id;
        }
        insert topicAssignmentsCopy;
        return null;
    }
}