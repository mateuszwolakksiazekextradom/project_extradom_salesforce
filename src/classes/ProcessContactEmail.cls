global class ProcessContactEmail implements Messaging.InboundEmailHandler {

    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        
        for (Contact c : [SELECT Id, Account.SPAM__c FROM Contact WHERE EmailId__c =: email.fromAddress AND EmailId__c != '' AND Account.SPAM__c = true]) {
            result.success = true;
            return result;
        }
        Map<String,Id> data = OrderMethods.getWebLeadContactDetails('E-mail', '', email.fromAddress, email.fromAddress, '', 'kontakt@extradom.pl', false, false, '');
        Task t = new Task(subject='kontakt@extradom.pl: ' +email.subject, Type='E-mail', Description=email.plainTextBody, WhoId=data.get('contactEmailId'), OwnerId=data.get('ownerId'), ActivityDate=System.Today(), Status='Not Started');
        insert t;
        result.success = true;
        
        if (email.binaryAttachments != null && email.binaryAttachments.size() > 0) {
            List<Attachment> attachments = new List<Attachment>{};
            for (integer i = 0 ; i < email.binaryAttachments.size() ; i++) {
                  Attachment attachment = new Attachment();
                  // attach to the newly created contact record
                  attachment.ParentId = t.Id;
                  attachment.Name = email.binaryAttachments[i].filename;
                  attachment.Body = email.binaryAttachments[i].body;
                  attachment.OwnerId = t.OwnerId;
                  attachments.add(attachment);
            }
            insert attachments;
        }
        
        return result;
    }
}