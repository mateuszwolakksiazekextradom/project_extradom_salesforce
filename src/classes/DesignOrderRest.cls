@RestResource(urlMapping='/designorder')
global class DesignOrderRest {
    @HttpPost
    global static Map<String,String> doPost(String code, String designVersion, String billingFirstName, String billingLastName, String billingAddressStreetName, String billingAddressStreetNo, String billingAddressStreetFlat, String billingPostalCode, String billingCity, String billingEmail, String billingPhone, String billingCompany, String billingTaxNumber, String shippingFirstName, String shippingLastName, String shippingAddressStreetName, String shippingAddressStreetNo, String shippingAddressStreetFlat, String shippingPostalCode, String shippingCity, String shippingPhone, String addOnsDetails, String addOns, String shippingMethod, Boolean optInRegulations, Boolean optInOrder, Boolean optIn, Boolean optInForward, String partnerId, String sourceUrl) {
        Restcontext.response.headers.put('Access-Control-Allow-Origin', '*');
        try {
            Design__c d = [SELECT Id, Full_Name__c, Code__c, Mirror__c, Gross_Price__c, Current_Price__c, Thumbnail_Base_URL__c, Type__c, Return_Conditions__c, Swap_Conditions__c FROM Design__c WHERE Code__c != '' AND Code__c = :code];
            Order__c o = new Order__c();
            o.Design__c = d.id;
            o.Quantity__c = 1;
            o.Design_Version__c = designVersion;
            o.Billing_First_Name__c = billingFirstName;
            o.Billing_Last_Name__c = billingLastName;
            o.Billing_Address_Street_Name__c = billingAddressStreetName;
            o.Billing_Address_Street_No__c = billingAddressStreetNo;
            o.Billing_Address_Street_Flat__c = billingAddressStreetFlat;
            o.Billing_Postal_Code__c = billingPostalCode;
            o.Billing_City__c = billingCity;
            o.Billing_Email__c = billingEmail;
            o.Billing_Phone__c = billingPhone;
            o.Billing_Company__c = billingCompany;
            o.Billing_Tax_Number__c = billingTaxNumber;
            o.Shipping_First_Name__c = shippingFirstName;
            o.Shipping_Last_Name__c = shippingLastName;
            o.Shipping_Address_Street_Name__c = shippingAddressStreetName;
            o.Shipping_Address_Street_No__c = shippingAddressStreetNo;
            o.Shipping_Address_Street_Flat__c = shippingAddressStreetFlat;
            o.Shipping_Postal_Code__c = shippingPostalCode;
            o.Shipping_City__c = shippingCity;
            o.Shipping_Phone__c = shippingPhone;
            o.Add_Ons_Details__c = addOnsDetails;
            o.Add_Ons__c = addOns;
            o.Shipping_Method__c = shippingMethod;
            o.Opt_In_Regulations__c = optInRegulations;
            o.Opt_In_Order__c = optInOrder;
            o.Opt_In__c = optIn;
            o.Opt_In_Forward__c = optInForward;
            String partnerName = 'Extradom.pl';
            if (String.isNotBlank(partnerId)) {
                partnerName = 'Partner';
            }
            o.Account__c = OrderMethods.getAccountId(billingFirstName, billingLastName, billingEmail, billingPhone, partnerName, 'Design', optIn, optIn, 'No');
            o.Partner_User__c = partnerId;
            o.Source_URL__c = sourceUrl;
            insert o;
        } catch (Exception e) {
            RestResponse res = RestContext.response;
            res.statusCode = 400;
            Map<String,String> result = new Map<String,String>{'result' => 'Nieprawidłowe zapytanie'};
            return result;
        }
        Map<String,String> result = new Map<String,String>{'result' => 'Dziękujemy za złożenie zamówienia'};
        return result;
    }
}