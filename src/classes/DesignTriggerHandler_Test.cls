@isTest
private class DesignTriggerHandler_Test {
    @isTest
    private static void shouldChangePricebookEntry_whenDesignStatusChanged() {
        // given
        Design__c design = [SELECT Id, Status__c FROM Design__c LIMIT 1];

        // when
        design.Status__c = ConstUtils.DESIGN_DRAFT_STATUS;
        update design;
        List<PricebookEntry> pricebookEntries = [
                SELECT Id, IsActive, Product2.Family, UnitPrice, Regular_Price__c FROM PricebookEntry
                WHERE Product2.Design__c = :design.Id AND Pricebook2Id = :Test.getStandardPricebookId()
        ];

        // then
        System.assertEquals(2, pricebookEntries.size());
        for (PricebookEntry pe : pricebookEntries) {
            if (pe.Product2.Family == ConstUtils.PRODUCT_DESIGN_FAMILY) {
                System.assertEquals(false, pe.IsActive);
                System.assertNotEquals(0, pe.UnitPrice);
                System.assertNotEquals(0, pe.Regular_Price__c);
            } else {
                System.assertEquals(true, pe.IsActive);
                System.assertEquals(0, pe.UnitPrice);
                System.assertEquals(0, pe.Regular_Price__c);
            }
        }
    }

    @isTest
    private static void shouldChangePricebookEntry_whenPromoPriceChanged() {
        // given
        Design__c design = [SELECT Id, Promo_Price__c FROM Design__c LIMIT 1];

        // when
        design.Promo_Price__c = 400;
        update design;
        List<PricebookEntry> pricebookEntries = [
                SELECT Id, IsActive, Product2.Family, UnitPrice, Regular_Price__c FROM PricebookEntry
                WHERE Product2.Design__c = :design.Id AND Pricebook2Id = :Test.getStandardPricebookId()
        ];

        // then
        System.assertEquals(2, pricebookEntries.size());
        for (PricebookEntry pe : pricebookEntries) {
            if (pe.Product2.Family == ConstUtils.PRODUCT_DESIGN_FAMILY) {
                System.assertEquals(true, pe.IsActive);
                System.assertEquals(400, pe.UnitPrice);
                System.assertNotEquals(0, pe.Regular_Price__c);
            } else {
                System.assertEquals(true, pe.IsActive);
                System.assertEquals(0, pe.UnitPrice);
                System.assertEquals(0, pe.Regular_Price__c);
            }
        }
    }

    @isTest
    private static void shouldChangePricebookEntry_whenGrossPriceChanged() {
        // given
        Design__c design = [SELECT Id, Gross_Price__c FROM Design__c LIMIT 1];

        // when
        design.Gross_Price__c = 800;
        update design;
        List<PricebookEntry> pricebookEntries = [
                SELECT Id, IsActive, Product2.Family, UnitPrice, Regular_Price__c FROM PricebookEntry
                WHERE Product2.Design__c = :design.Id AND Pricebook2Id = :Test.getStandardPricebookId()
        ];

        // then
        System.assertEquals(2, pricebookEntries.size());
        for (PricebookEntry pe : pricebookEntries) {
            if (pe.Product2.Family == ConstUtils.PRODUCT_DESIGN_FAMILY) {
                System.assertEquals(true, pe.IsActive);
                System.assertNotEquals(0, pe.UnitPrice);
                System.assertEquals(800, pe.Regular_Price__c);
            } else {
                System.assertEquals(true, pe.IsActive);
                System.assertEquals(0, pe.UnitPrice);
                System.assertEquals(0, pe.Regular_Price__c);
            }
        }
    }

    @TestSetup
    private static void setupData() {
        Supplier__c newSupplier = new Supplier__c(Name = 'Supplier', Code__c = '11', Full_Name__c = 'Great Supplier');
        insert newSupplier;

        Design__c design = new Design__c (
                Name = 'Test Design', Supplier__c = newSupplier.Id, Full_Name__c = 'First Design', Type__c = ConstUtils.DESIGN_HOUSE_TYPE,
                Status__c = ConstUtils.DESIGN_ACTIVE_STATUS, Promo_Price__c = 500, Gross_Price__c = 600
        );
        insert design;

        Product2 designProduct = new Product2(
                Name = 'Design Product', Family = ConstUtils.PRODUCT_DESIGN_FAMILY, IsActive = true, Design__c = design.Id,
                Version_Required__c = false, Line_Description_Required__c = false, Max_Discount__c = 10
        );
        Product2 giftProduct = new Product2(
                Name = 'Gift Product', Family = ConstUtils.PRODUCT_GIFT_FAMILY, IsActive = true, Design__c = design.Id,
                Version_Required__c = true, Line_Description_Required__c = true, Max_Discount__c = 10
        );
        insert new List<Product2> {designProduct, giftProduct};

        PricebookEntry spe1 = new PricebookEntry(
                Product2Id = designProduct.Id, IsActive = true, Pricebook2Id = Test.getStandardPricebookId(), UnitPrice = 0, Regular_Price__c = 0
        );
        PricebookEntry spe2 = new PricebookEntry(
                Product2Id = giftProduct.Id, IsActive = true, Pricebook2Id = Test.getStandardPricebookId(), UnitPrice = 0, Regular_Price__c = 0
        );
        insert new List<PricebookEntry> {spe1, spe2};
    }
    
    @isTest
    private static void shouldRecalculateMediaRooms_whenDesignUpdates() {
        Supplier__c newSupplier = new Supplier__c(Name = 'Supplier', Code__c = 'QQQ', Full_Name__c = 'Great Supplier');
        insert newSupplier;
    
        Design__c design = new Design__c (
                Name = 'QQQ', Supplier__c = newSupplier.Id, Full_Name__c = 'First Design', Type__c = ConstUtils.DESIGN_HOUSE_TYPE,
                Status__c = ConstUtils.DESIGN_ACTIVE_STATUS, Promo_Price__c = 500, Gross_Price__c = 600
        );
        insert design;
        
        Media__c media = new Media__c (Design__c = design.Id, R1__c = 'spiżarnia', R2__c = 'garderoba', Media_Name_ID__c = 'qqq');
        insert media;
        
        design.Name = 'ttt';
        update design;
        
        Design__c d = [SELECT Id, Pantry__c FROM Design__c WHERE Id = :design.Id];
        
        System.assertEquals(d.Pantry__c, 1);
        
    }
}