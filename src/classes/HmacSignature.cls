public class HmacSignature {
   public String relUrl {get; set;}
   public String secretKeyValue {get; set;}
    
   public String getHmacSignature() {
       relUrl = (relUrl.replace('|','%7C')).replace(',','%2C');
       Blob secretKey = EncodingUtil.base64Decode(secretKeyValue);
       Blob data = crypto.generateMac('hmacSHA1', Blob.valueOf(relUrl), secretKey);
       return EncodingUtil.base64Encode(data);
   }
}