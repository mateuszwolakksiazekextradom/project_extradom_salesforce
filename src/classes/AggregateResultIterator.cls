/**
 * Created by grzegorz.dlugosz on 10.05.2019.
 */
global class AggregateResultIterator Implements Iterator<AggregateResult>{

    AggregateResult [] results {get;set;}
    Integer index {get;set;}

    global AggregateResultIterator(String query){
        index = 0;
        results = Database.query(query);
    }

    global boolean hasNext(){
        return results !=null && !results.isEmpty() && index < results.size();
    }

    global AggregateResult next(){
        return results[index++];
    }
}