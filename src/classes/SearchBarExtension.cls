public with sharing class SearchBarExtension {
    
    private final String query;
    public SearchBarExtension() {
        try {
            query = ApexPages.currentPage().getParameters().get('q');
        } catch(Exception e) {
            query = '';
        }
    }
    
    public List<Design__c> getDesignResults() {
        List<Design__c> results = new List<Design__c>();
        // exact search
        results.addAll([SELECT Full_Name__c, Gross_Price__c, Thumbnail_Base_URL__c, Code__c, KW_Model__c, KW_Model_Preparation__c, Height__c, Usable_Area__c, Footprint__c, Capacity__c, Roof_slope__c, Minimum_Plot_Size_Horizontal__c, Minimum_Plot_Size_Vertical__c FROM Design__c WHERE Status__c = 'Active' AND Full_Name__c = :query ORDER BY Page_Score_Unique__c DESC LIMIT 10]);
        Integer searchLimit = 10-results.size();
        if (searchLimit > 0) {
            // phrase search
            String phraseQuery = '%'+query+'%';
            results.addAll([SELECT Full_Name__c, Gross_Price__c, Thumbnail_Base_URL__c, Code__c, KW_Model__c, KW_Model_Preparation__c, Height__c, Usable_Area__c, Footprint__c, Capacity__c, Roof_slope__c, Minimum_Plot_Size_Horizontal__c, Minimum_Plot_Size_Vertical__c FROM Design__c WHERE Status__c = 'Active' AND Name LIKE :phraseQuery AND Full_Name__c != :query ORDER BY Page_Score_Unique__c DESC LIMIT :searchLimit]);
        }
        return results;
    }
    
    public List<Page__c> getPageResults() {
        List<Page__c> results = new List<Page__c>();
        // exact search
        for (Keyword__c k : [SELECT Page__c FROM Keyword__c WHERE Name = :query AND Page__r.Type__c = 'Collection']) {
            results.add([SELECT Id, Name, URL__c FROM Page__c WHERE Id = :k.Page__c]);
        }
        // phrase search
        Integer searchLimit = 5-results.size();
        String phraseQuery = '%'+query+'%';
        List<ID> pageIds = new List<ID>();
        List<AggregateResult> orderedPageIds = [SELECT Page__c pageId FROM Keyword__c WHERE Name LIKE :phraseQuery AND Page__r.Type__c = 'Collection' GROUP BY Page__c HAVING SUM(Search_Score__c) >= 3 ORDER BY SUM(Search_Score__c) DESC LIMIT :searchLimit];
        for (AggregateResult ar : orderedPageIds) {
            pageIds.add((ID)ar.get('pageId'));
        }
        Map<ID,Page__c> pagesMap = new Map<ID,Page__c>([SELECT Id, Name, URL__c FROM Page__c WHERE Id IN :pageIds AND Id NOT IN :results]);
        for (AggregateResult ar : orderedPageIds) {
            results.add(pagesMap.get((ID)ar.get('pageId')));
        }
        return results;
    }
    
    public String getQueryText() {
        return query;
    }

}