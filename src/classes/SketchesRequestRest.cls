@RestResource(urlMapping='/sketchesrequest')
global class SketchesRequestRest {
    @HttpPost
    global static Map<String,String> doPost(String email, String phone, String code, String comments, Boolean optInRegulations, Boolean optInPrivacyPolicy, Boolean optInMarketing, Boolean optInMarketingExtended, Boolean optInMarketingPartner, Boolean optInLeadForward, String gclid, String hasPlot, String hasMpzp, String constructionPlan, String hasApp, Boolean optInEmail, Boolean optInPhone, Boolean optInProfiling, Boolean optInPKO) {
        Map<String,Id> data = OrderMethods.getWebLeadContactDetails('Sketches', '', email, email, phone, 'Internal', false, false, 'No',hasPlot, hasMpzp, constructionPlan, hasApp);
        if (String.isNotBlank(gclid)) {
            OrderMethods.updateGclid(data.get('accountId'), gclid);
        }
        RestRequest req = RestContext.request;
        if (req.headers.containsKey('gaClientId')) {
            OrderMethods.updateGaClientId(data.get('accountId'), req.headers.get('gaClientId'));
        }
        OrderMethods.updateOptIns(email, phone, optInRegulations, optInPrivacyPolicy, optInMarketing, optInMarketingExtended, optInMarketingPartner, optInLeadForward, optInEmail, optInPhone, optInProfiling, optInPKO);
        OrderMethods.markSalesReadyFromResources(data.get('accountId'), constructionPlan);
        Design__c d = [SELECT Id, Has_Only_Estimate_Materials__c FROM Design__c WHERE Code__c != '' AND Code__c = :code];
        String status = 'Not Started';
        if (String.isBlank(comments)) {
            status = 'Completed';
        }
        Task t = new Task(subject='Zamówienie rysunków szczegółowych', Status=status, Description=comments, Type='Question', WhoId=data.get('contactEmailId'), WhatId=d.Id, OwnerId=data.get('ownerId'), ActivityDate=System.Today());
        insert t;
        Map<String,String> result = new Map<String,String>{'result' => 'Dziękujemy za zainteresowanie. Rysunki zostaną przesłane na podany adres poczty e-mail.'};
        return result;
    }
}