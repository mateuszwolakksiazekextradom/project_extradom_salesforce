public with sharing class QAViewController {
    
    public QAViewController() {
    }
    
    public ConnectApi.FeedElementPage getFeedElementsFromFeedAllQuestions() {
        return ConnectApi.ChatterFeeds.getFeedElementsFromFeed(Network.getNetworkId(), ConnectApi.FeedType.Home, null, null, null, null, null, ConnectApi.FeedFilter.AllQuestions);
    }
    
    public ConnectApi.FeedElementPage getFeedElementsFromFeedSolvedQuestions() {
        return ConnectApi.ChatterFeeds.getFeedElementsFromFeed(Network.getNetworkId(), ConnectApi.FeedType.Home, null, null, null, null, null, ConnectApi.FeedFilter.SolvedQuestions);
    }
    
    public ConnectApi.FeedElementPage getFeedElementsFromFeedUnsolvedQuestions() {
        return ConnectApi.ChatterFeeds.getFeedElementsFromFeed(Network.getNetworkId(), ConnectApi.FeedType.Home, null, null, null, null, null, ConnectApi.FeedFilter.UnsolvedQuestions);
    }
    
    public ConnectApi.FeedElementPage getFeedElementsFromFeedUnansweredQuestions() {
        return ConnectApi.ChatterFeeds.getFeedElementsFromFeed(Network.getNetworkId(), ConnectApi.FeedType.Home, null, null, null, null, null, ConnectApi.FeedFilter.UnansweredQuestions);
    }
    
    public ConnectApi.ManagedTopicCollection getManagedTopics() {
        return ConnectApi.ManagedTopics.getManagedTopics(Network.getNetworkId(), ConnectApi.ManagedTopicType.Navigational);
    }

}