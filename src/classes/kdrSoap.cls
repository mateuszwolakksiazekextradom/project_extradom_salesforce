public class kdrSoap {

    @future(callout=true)
    public static void sendRequest() {
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        Http http = new Http();
        HTTPResponse res = new HTTPResponse();
        
        string body = '<?xml version="1.0" encoding="utf-8"?>' +
                '<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:tem="http://tempuri.org/">' +
                '<soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">' +
                '<wsa:MessageID></wsa:MessageID>'+
                '<wsa:Action>http://tempuri.org/IInfoKarta/StatusNumer</wsa:Action>' +
                '<soap:Body>' +
                '<soapenv:Body>' +
                '<tem:StatusNumer>' +
                '<tem:numerKarty>24660110003402001</tem:numerKarty>' +
                ' </tem:StatusNumer>' +
                '</soap:Body>' +
                '</soap:Envelope>';
        
       
       req.setEndpoint('https://kdr-ws2-test.mpips.gov.pl/mok/ws/infoKartaWS2');
       req.setBody(body);
       req.setHeader('Content-Type','application/soap+xml;charset=utf-8');
       
       res = http.send(req);
       system.debug(res.getBody());
    }
}