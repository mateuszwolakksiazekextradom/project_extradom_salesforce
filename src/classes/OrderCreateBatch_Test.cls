@IsTest
private class OrderCreateBatch_Test {
	@IsTest
    private static void shouldExecute() {
        Product2 product = new Product2(
                Name = 'Default Add On', Family = ConstUtils.PRODUCT_GIFT_FAMILY, IsActive = true, ProductCode = 'Default Add On'
        );
        insert product;

        PricebookEntry spe = new PricebookEntry(
                Product2Id = product.Id, IsActive = true, Pricebook2Id = Test.getStandardPricebookId(), UnitPrice = 100
        );
        insert spe;
        Account acc = new Account (Name = 'Test');
        insert acc;
        Order__c o = new Order__c (Account__c = acc.ID, Shipping_Method__c = 'Office (Online)', Add_Ons_Gross_Price__c =5, Status__c = 'Returned');
        insert o;
        try {
            OrderCreateBatch.ids = new Set<String>{o.Id};
            Database.executeBatch(new OrderCreateBatch());
        } catch (Exception e) {
            
        }
    }
}