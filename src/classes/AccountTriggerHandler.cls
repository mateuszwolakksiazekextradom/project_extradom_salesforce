public without sharing class AccountTriggerHandler extends TriggerHandler {
    public static Boolean BEFORE_DELETE_PERFORMED = false;
    public static Boolean FIRST_RUN = true;
    public static Boolean FIRST_OPP_RUN = true;
    
    public static List<Account> afterDeleteAccounts;
    public static String leadStatus;
    public static Datetime unqEnterDate;
    public static String unqReason;
    
    public override void beforeUpdate() {
        Map<Id, Account> accountsMap = (Map<Id, Account>) Trigger.newMap;
        List<Account> accountsList = Trigger.new;
        List<Account> oldAccountsList = Trigger.old;
        setContactData(accountsMap);
        // setConvertedOpportunity(accountsMap);
        onAccountMergeCOKCheck(oldAccountsList);
        onAccountMergeLeadStatus(oldAccountsList,accountsList);
    }

    public override void beforeDelete() {
        BEFORE_DELETE_PERFORMED = true;
        afterDeleteAccounts = (List<Account>)Trigger.old;
    }
    
    public override void afterDelete() {       
    }
    
    public override void afterInsert() {
        List<Account> accounts = (List<Account>) Trigger.new;
        sendGATrackingLeadStatus(accounts, null);
        FIRST_RUN = false;
    }
    
    public override void afterUpdate() {
        List<Account> accounts = (List<Account>) Trigger.new;
        Map<Id, Account> oldAccounts = (Map<Id, Account>) Trigger.oldMap;
        sendGATrackingLeadStatus(accounts, oldAccounts);
        closeOpenFollowUpCalls(accounts, oldAccounts);
        closeOpenConflictTasks(accounts, oldAccounts);
        onAccountMergeOpportunities(accounts);
        onAccountMergeReassignOrders(accounts);
        FIRST_RUN = false;
    }
    
    private void onAccountMergeReassignOrders(List<Account> newAccounts) {
        if (BEFORE_DELETE_PERFORMED) {
            system.debug(newAccounts);
            List<Order> draftOrders = [SELECT Id, OwnerId, Account.OwnerId FROM Order WHERE AccountId IN :newAccounts AND Status = :ConstUtils.ORDER_DRAFT_STATUS AND IsReductionOrder = false];
            List<Order> ordersToUpdate = new List<Order>();
            for (Order o : draftOrders) {
                system.debug(o);
                if (o.OwnerId != o.Account.OwnerId) {
                    o.OwnerId = o.Account.OwnerId;
                    ordersToUpdate.add(o);
                }
            }
            TriggerHandler.bypass('OrderTriggerHandler');
            update ordersToUpdate;
            TriggerHandler.clearBypass('OrderTriggerHandler');
        }    
    }
    
    private void onAccountMergeCOKCheck(List<Account> oldAccounts) {
        if (BEFORE_DELETE_PERFORMED) {
            Account beforeMergeMasterAccount = oldAccounts[0];
            User u = [SELECT Id, Department FROM User WHERE Id = :UserInfo.getUserId()];
                        
            if (u.Department == 'COK' && u.Id != beforeMergeMasterAccount.OwnerId) {
                beforeMergeMasterAccount.addError('Nie możesz scalić kont, jedno z kont nie było przypisane na Ciebie.');
            }
            for (Account a : afterDeleteAccounts) {
                if (u.Department == 'COK' && u.Id != a.OwnerId) {
                    beforeMergeMasterAccount.addError('Nie możesz scalić kont, jedno z kont nie było przypisane na Ciebie.');
                }
            }
        }
    }
    
    
    private void onAccountMergeLeadStatus(List<Account> oldAccounts, List<Account> newAccounts) {
        if (BEFORE_DELETE_PERFORMED) {
            leadStatus = oldAccounts[0].Lead_Status__c;
            unqEnterDate = oldAccounts[0].Enter_Date_Unqualified__c;
            for (Account a : afterDeleteAccounts) {
                if (a.Lead_Status__c != null && a.Lead_Status__c == ConstUtils.ACCOUNT_UNQUALIFIED_LEAD_STATUS) {
                    unqEnterDate = unqEnterDate == null ? a.Enter_Date_Unqualified__c : (unqEnterDate > a.Enter_Date_Unqualified__c ? a.Enter_Date_Unqualified__c : unqEnterDate);
                    unqReason = unqReason == null ? a.Unqualified_Reason__c : (unqEnterDate > a.Enter_Date_Unqualified__c ? a.Unqualified_Reason__c : unqReason);
                }               
                if (leadStatus == null || (ConstUtils.LEAD_STATUS_MAP.get(a.Lead_Status__c) > ConstUtils.LEAD_STATUS_MAP.get(leadStatus))) {
                    leadStatus = a.Lead_Status__c;
                }
            }
            
            if (leadStatus != null && leadStatus != ConstUtils.ACCOUNT_UNQUALIFIED_LEAD_STATUS && (unqEnterDate == null || leadStatus == ConstUtils.ACCOUNT_CONVERTED_LEAD_STATUS)) {
                newAccounts[0].Unqualified_Reason__c = '';
                newAccounts[0].Lead_Status__c = leadStatus;
            }          
        }
    }
    
    private void onAccountMergeOpportunities(List<Account> newAccounts) {
        if (BEFORE_DELETE_PERFORMED && FIRST_OPP_RUN) {
            FIRST_OPP_RUN = false;
            if (leadStatus == ConstUtils.ACCOUNT_CONVERTED_LEAD_STATUS) {
                List<Opportunity> accountOpportunities = [SELECT Id, isWon, isClosed, StageName, Most_Recent_Stage_Date__c FROM Opportunity WHERE AccountId IN :newAccounts];               
                List<Opportunity> oppsToUpdate = new List<Opportunity>();               
                Opportunity openOpportunity, wonOpportunity, lostOpportunity, closedOpportunity;
                               
                for (Opportunity o : accountOpportunities) {
                    if (!o.isClosed) {
                        if (openOpportunity != null && openOpportunity.Most_Recent_Stage_Date__c < o.Most_Recent_Stage_Date__c) {
                            openOpportunity.StageName = 'Closed Lost / Duplicate';
                            oppsToUpdate.add(openOpportunity);
                            openOpportunity = o;
                            system.debug(o.StageName);
                            system.debug(openOpportunity.StageName);
                        } else if (openOpportunity != null && openOpportunity.Most_Recent_Stage_Date__c > o.Most_Recent_Stage_Date__c) {
                            o.StageName = 'Closed Lost / Duplicate';
                            oppsToUpdate.add(o);                                                    
                        } else {
                            openOpportunity = o;
                        }                     
                    } else if (o.isClosed && o.isWon && (wonOpportunity == null || wonOpportunity.Most_Recent_Stage_Date__c < o.Most_Recent_Stage_Date__c)) {                       
                        wonOpportunity = o;
                    } else if (o.isClosed && !o.isWon) {                       
                        if (lostOpportunity != null && lostOpportunity.Most_Recent_Stage_Date__c < o.Most_Recent_Stage_Date__c) {
                            if (lostOpportunity.StageName != 'Closed Lost / Duplicate') {
                                lostOpportunity.StageName = 'Closed Lost / Duplicate';
                                oppsToUpdate.add(lostOpportunity);
                            }
                            lostOpportunity = o;
                        } else if (lostOpportunity != null && lostOpportunity.Most_Recent_Stage_Date__c > o.Most_Recent_Stage_Date__c) {
                            if (lostOpportunity.StageName != 'Closed Lost / Duplicate') {
                                o.StageName = 'Closed Lost / Duplicate';
                                oppsToUpdate.add(o);
                            }
                        } else {
                            lostOpportunity = o;
                        }
                    }
                }
                
                if (lostOpportunity != null && wonOpportunity != null && lostOpportunity.Most_Recent_Stage_Date__c < wonOpportunity.Most_Recent_Stage_Date__c) {
                    lostOpportunity.StageName = 'Closed Lost / Duplicate';
                    oppsToUpdate.add(lostOpportunity);
                }
                closedOpportunity = lostOpportunity == null ? wonOpportunity : (wonOpportunity == null ? lostOpportunity : (wonOpportunity.Most_Recent_Stage_Date__c > lostOpportunity.Most_Recent_Stage_Date__c ? wonOpportunity : lostOpportunity));
              
                if (openOpportunity != null) {
                    if (((closedOpportunity != null && unqEnterDate != null && closedOpportunity.Most_Recent_Stage_Date__c > unqEnterDate) || (closedOpportunity != null && unqEnterDate == null)) 
                    && closedOpportunity.Most_Recent_Stage_Date__c > openOpportunity.Most_Recent_Stage_Date__c) {
                        openOpportunity.StageName = 'Closed Lost / Duplicate';
                        oppsToUpdate.add(openOpportunity);  
                    } else if (((closedOpportunity != null && unqEnterDate != null && unqEnterDate > closedOpportunity.Most_Recent_Stage_Date__c) || (unqEnterDate != null && closedOpportunity == null)) 
                    && unqEnterDate > openOpportunity.Most_Recent_Stage_Date__c) {
                        openOpportunity.StageName = 'Closed Lost / ' + unqReason;
                        oppsToUpdate.add(openOpportunity);
                    }
                }
                
                update oppsToUpdate;
            }
        }
    }    
    
    private void sendGATrackingLeadStatus(List<Account> newAccounts, Map<Id, Account> oldAccountMap) {
        List<Id> createdAccountIds = new List<Id>{};
        List<Id> convertedAccountIds = new List<Id>{};
        for (Account a : newAccounts) {
            if (FIRST_RUN && oldAccountMap == null) {
                createdAccountIds.add(a.Id);
            }
            if ((FIRST_RUN && oldAccountMap == null && a.Lead_Status__c == ConstUtils.ACCOUNT_CONVERTED_LEAD_STATUS)
               || (FIRST_RUN && oldAccountMap != null && oldAccountMap.get(a.Id).Lead_Status__c != a.Lead_Status__c && a.Lead_Status__c == ConstUtils.ACCOUNT_CONVERTED_LEAD_STATUS)) {
                convertedAccountIds.add(a.Id);
            }
        }
        if (!createdAccountIds.isEmpty()) {
            GoogleAnalyticsTools.collectCreatedLeads(createdAccountIds);
        }
        if (!convertedAccountIds.isEmpty()) {
            GoogleAnalyticsTools.collectConvertedLeads(convertedAccountIds);
        }
    }
    
    private void closeOpenConflictTasks(List<Account> newAccounts, Map<Id, Account> oldAccountMap) {
        Set<Id> accountIds = new Set<Id> ();

        for (Account a : newAccounts) {
            if (oldAccountMap != null && oldAccountMap.containsKey(a.Id) && oldAccountMap.get(a.Id).Lead_Status__c != a.Lead_Status__c) {
                accountIds.add(a.Id);
            }
        }

        if (accountIds.size() > 0) {
            List<Task> conflictTasksToClose = [SELECT Id, Status FROM Task WHERE Type = :ConstUtils.TASK_CONFLICT_TYPE AND AccountId IN :accountIds AND isClosed = false];
            for (Task t : conflictTasksToClose) {
                t.Status = ConstUtils.TASK_COMPLETED_STATUS;
            }
            update conflictTasksToClose;
        }
    }
    /*
    private void setConvertedOpportunity(Map<Id, Account> accountsMap) {
        if (BEFORE_DELETE_PERFORMED) {
            List<Account> accountsWithOpportunities = [
                    SELECT Id,
                    (
                            SELECT Id, StageName, Probability, CreatedDate, Enter_Date_Lost__c, Enter_Date_Won__c, ForecastCategoryName
                            FROM Opportunities
                            ORDER BY Enter_Date_Won__c ASC NULLS LAST, Probability DESC NULLS LAST,
                                    Enter_Date_Lost__c DESC NULLS LAST, CreatedDate ASC
                    )
                    FROM Account WHERE Id in: accountsMap.keySet()
            ];
            for (Account acc : accountsWithOpportunities) {
                String opportunityId = null;
                if (acc.Opportunities != null && acc.Opportunities.size() > 0) {
                    for (Opportunity o : acc.Opportunities) {
                        if (o.StageName == ConstUtils.OPPORTUNITY_CLOSED_WON_STAGE && o.Enter_Date_Won__c != null) {
                            opportunityId = o.Id;
                            break;
                        }
                    }
                    if (String.isBlank(opportunityId)) {
                        for (Opportunity o : acc.Opportunities) {
                            if (o.ForecastCategoryName != ConstUtils.OPPORTUNITY_CLOSED_FORECAST
                                    && o.ForecastCategoryName != ConstUtils.OPPORTUNITY_OMITTED_FORECAST) {
                                opportunityId = o.Id;
                                break;
                            }
                        }
                    }
                    if (String.isBlank(opportunityId)) {
                        for (Opportunity o : acc.Opportunities) {
                            if (o.ForecastCategoryName == ConstUtils.OPPORTUNITY_OMITTED_FORECAST && o.Enter_Date_Lost__c != null) {
                                opportunityId = o.Id;
                                break;
                            }
                        }
                    }
                }
                accountsMap.get(acc.Id).Converted_Opportunity__c = opportunityId;
            }
        }
    }
    */
    private void closeOpenFollowUpCalls(List<Account> newAccounts, Map<Id, Account> oldAccounts) {
        List<Account> accountsToClose = new List<Account>();
        List<Task> tasksToClose = new List<Task>();
        if (BEFORE_DELETE_PERFORMED) {
            Map<Id, Account> accountsWithNewestTask = new Map<Id, Account> ([SELECT Id, (SELECT Id, CreatedDate FROM Tasks WHERE Type = :ConstUtils.TASK_FOLLOW_UP_TYPE AND Status = :ConstUtils.TASK_NOT_STARTED_STATUS ORDER BY CreatedDate DESC NULLS LAST LIMIT 1) tasks FROM Account WHERE Id = :newAccounts]);            
            Set<Id> accountsExcludedByOpp = new Map<Id, Account>([SELECT Id FROM Account WHERE Id IN :newAccounts AND Id IN (SELECT AccountId FROM Opportunity WHERE isClosed = true AND isWon = false AND AccountId IN :newAccounts) AND Id NOT IN (SELECT AccountId FROM Opportunity WHERE isClosed = false AND AccountId IN :newAccounts)]).keySet();
            Set<Id> accountsExcludedByOrder = new Set<Id>();
            for (OrderItem oi : [SELECT Order.AccountId FROM OrderItem WHERE Order.AccountId IN :newAccounts AND Order.StatusCode = :ConstUtils.ORDER_ACTIVATED_STATUS_CODE AND Quantity > 0 AND PricebookEntry.Product2.Family = :ConstUtils.PRODUCT_DESIGN_FAMILY]) {
                accountsExcludedByOrder.add(oi.Order.AccountId);
            }

            for (Task t : [SELECT Id, Status, AccountId, Account.OwnerId FROM Task WHERE AccountId IN :newAccounts AND Type = :ConstUtils.TASK_FOLLOW_UP_TYPE AND Status = :ConstUtils.TASK_NOT_STARTED_STATUS]) {
                t.OwnerId = t.Account.OwnerId;
                if (t.Id != accountsWithNewestTask.get(t.AccountId).tasks[0].Id || accountsExcludedByOpp.contains(t.AccountId) || accountsExcludedByOrder.contains(t.AccountId)) {
                    t.Status = ConstUtils.TASK_COMPLETED_STATUS;
                }
                tasksToClose.add(t);
            }
            update tasksToClose;
        } else {
            for (Account a : newAccounts) {
                if (oldAccounts != null && oldAccounts.containsKey(a.Id) && oldAccounts.get(a.Id).Lead_Status__c != a.Lead_Status__c && a.Lead_Status__c == ConstUtils.ACCOUNT_UNQUALIFIED_LEAD_STATUS) {
                    accountsToClose.add(a);   
                }
            }
            for (Task t : [SELECT Id, Status FROM Task WHERE AccountId IN :accountsToClose AND Type = :ConstUtils.TASK_FOLLOW_UP_TYPE AND Status = :ConstUtils.TASK_NOT_STARTED_STATUS]) {
                t.Status = ConstUtils.TASK_COMPLETED_STATUS;
                tasksToClose.add(t);
            }
            update tasksToClose;        
        }
    }

    private void setContactData(Map<Id, Account> accountsMap) {
        if (BEFORE_DELETE_PERFORMED) {
            Set<Id> optInMarketing = new Set<Id> ();
            Set<Id> optInMarketingPartner = new Set<Id> ();
            Set<Id> optInMarketingExtended = new Set<Id> ();
            Set<Id> optInLeadForward = new Set<Id> ();
            Set<Id> optInAsked = new Set<Id> ();
            Set<Id> phoneAccs = new Set<Id> ();
            Set<Id> emailAccs = new Set<Id> ();

            List<Account> accountsWithContacts = [
                    SELECT Id,
                    (
                            SELECT Id, Opt_In_Marketing__c, Opt_In_Marketing_Extended__c, Opt_In_Marketing_Partner__c,
                                    Opt_In_Lead_Forward__c, Opt_Ins_Asked__c, Phone, Email FROM Contacts
                    )
                    FROM Account WHERE Id in: accountsMap.keySet()
            ];

            for (Account acc : accountsWithContacts) {
                if (acc.Contacts != null && acc.Contacts.size() > 0) {
                    for (Contact c : acc.Contacts) {
                        if (c.Opt_In_Marketing__c) {
                            optInMarketing.add(acc.Id);
                        }
                        if (c.Opt_In_Marketing_Partner__c) {
                            optInMarketingPartner.add(acc.Id);
                        }
                        if (c.Opt_In_Marketing_Extended__c) {
                            optInMarketingExtended.add(acc.Id);
                        }
                        if (c.Opt_In_Lead_Forward__c) {
                            optInLeadForward.add(acc.Id);
                        }
                        if (c.Opt_Ins_Asked__c) {
                            optInAsked.add(acc.Id);
                        }
                        if (String.isNotBlank(c.Email)) {
                            emailAccs.add(acc.Id);
                        }
                        if (String.isNotBlank(c.Phone)) {
                            phoneAccs.add(acc.Id);
                        }
                    }
                }
            }

            for (Account acc : accountsMap.values()) {
                acc.Has_Opt_In_Marketing__c = optInMarketing.contains(acc.Id);
                acc.Has_Opt_In_Marketing_Extended__c = optInMarketingExtended.contains(acc.Id);
                acc.Has_Opt_In_Marketing_Partner__c = optInMarketingPartner.contains(acc.Id);
                acc.Has_Opt_In_Lead_Forward__c = optInLeadForward.contains(acc.Id);
                acc.Has_Opt_Ins_Asked__c = optInAsked.contains(acc.Id);
                acc.Has_Contact_with_Email__c = emailAccs.contains(acc.Id);
                acc.Has_Contact_with_Phone__c = phoneAccs.contains(acc.Id);
            }
        }
    }
}