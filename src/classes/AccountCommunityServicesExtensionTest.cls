@isTest
private class AccountCommunityServicesExtensionTest {

    static testMethod void myUnitTest() {
    
    List<RecordType> locRecordTypes = [Select Id, Name From RecordType where sObjectType='Location__c' and isActive=true];
    Map<String, ID> locRecordTypeNames = new Map<String, ID>{};
    for(RecordType rt: locRecordTypes) {
        locRecordTypeNames.put(rt.Name,rt.Id);
    }
    
    Location__c loc_country = new Location__c(Name='Polska', RecordTypeId=locRecordTypeNames.get('Country'));
    insert loc_country;
    Location__c loc_state = new Location__c(Name='opolskie', RecordTypeId=locRecordTypeNames.get('State'), Location__latitude__s=50.6131, Location__longitude__s=17.92915);
    insert loc_state;
    Location__c loc_region = new Location__c(Name='nyski', RecordTypeId=locRecordTypeNames.get('Region'), Location__latitude__s=50.44138, Location__longitude__s=17.29832);
    insert loc_region;
    Location__c loc_city = new Location__c(Name='Nysa', RecordTypeId=locRecordTypeNames.get('City'), Location__latitude__s=50.471636, Location__longitude__s=17.334111);
    insert loc_city;
    
    Account a = new Account(Name='Test Architect', Partner_Architect__c=true, Investment_Location__c=loc_city.Id);
    insert a;
    
    Community_Member__c cm = new Community_Member__c(Name='Test Architect', Membercode__c='aBc~~0123xyZ', ID__c=1, Business_Type__c='Service', Partner_Account__c=a.Id);
    insert cm;
    
    List<RecordType> cmdRecordTypes = [Select Id, Name From RecordType where sObjectType='Community_Member_Detail__c' and isActive=true];
    Map<String, ID> cmdRecordTypeNames = new Map<String, ID>{};
    for(RecordType rt: cmdRecordTypes) {
        cmdRecordTypeNames.put(rt.Name,rt.Id);
    }
    
    List<RecordType> ctagRecordTypes = [Select Id, Name From RecordType where sObjectType='Community_Tag__c' and isActive=true];
    Map<String, ID> ctagRecordTypeNames = new Map<String, ID>{};
    for(RecordType rt: ctagRecordTypes) {
        ctagRecordTypeNames.put(rt.Name,rt.Id);
    }
    
    Community_Tag__c ct = new Community_Tag__c(Name='adaptacje projektów', RecordTypeId=ctagRecordTypeNames.get('Service'), Distance__c=50);
    insert ct;
    
    Community_Member_Detail__c cmd_ct = new Community_Member_Detail__c(Community_Member__c=cm.Id, Community_Tag__c=ct.Id, RecordTypeId=cmdRecordTypeNames.get('Community Tag'));
    insert cmd_ct;
    
    Community_Member_Detail__c cmd_bal = new Community_Member_Detail__c(Community_Member__c=cm.Id, Location__c=loc_region.Id, RecordTypeId=cmdRecordTypeNames.get('Business Activity Location'));
    insert cmd_bal;
    
    ApexPages.StandardController sc = new ApexPages.StandardController(a);
    AccountCommunityServicesExtension ext = new AccountCommunityServicesExtension(sc);
    
    ext.tag = 'adaptacje projektów';
    
    ext.getClosestArchitectByPlotAdaptationMembers();
    
    ext.firstName = 'first name';
    ext.lastName = 'last name';
    ext.phone = '601602603';
    ext.email = 'test@extradom.pl';
    ext.nonHouse = true;
    
    ext.createTelemarketingEntries();
    ext.getTelResults();
    
    ext.getTelemarketingLostReasonOptions();
    ext.createTelemarketingLostEntry();
    ext.getTelResults();
    
    ext.createBraasTelemarketing();
    ext.createTelemarketingDeferredEntry();
    ext.createCustomTelemarketingEntry();
    
    }
    
}