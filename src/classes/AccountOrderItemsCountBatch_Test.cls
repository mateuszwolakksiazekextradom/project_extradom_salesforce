@IsTest
private class AccountOrderItemsCountBatch_Test {
    @IsTest
    private static void shouldExecute() {
        Test.setMock(HttpCalloutMock.class, new GoogleAnalyticsCollectCalloutMock());
        
        Product2 product1 = new Product2(Name = 'Test Product 3', Family = ConstUtils.PRODUCT_DESIGN_FAMILY, IsActive = true);
        Product2 product2 = new Product2(Name = 'Test Product 4', Family = ConstUtils.PRODUCT_DESIGN_FAMILY, IsActive = true);
        insert new List<Product2> {product1, product2};

        PricebookEntry spe1 = new PricebookEntry(
                Product2Id = product1.Id, IsActive = true, Pricebook2Id = Test.getStandardPricebookId(), UnitPrice = 100
        );
        PricebookEntry spe2 = new PricebookEntry(
                Product2Id = product2.Id, IsActive = true, Pricebook2Id = Test.getStandardPricebookId(), UnitPrice = 100
        );
        insert new List<PricebookEntry> {spe1, spe2};
        Account acc = new Account (Name = 'Test Name');
        insert acc;
        List<PricebookEntry> pricebookEntries = [SELECT Id FROM PricebookEntry WHERE Product2.Family = :ConstUtils.PRODUCT_DESIGN_FAMILY];
        Order o = new Order (
                AccountId = acc.Id, EffectiveDate = System.now().date(), Status = ConstUtils.ORDER_DRAFT_STATUS,
                Pricebook2Id = Test.getStandardPricebookId(), RecordTypeId = ConstUtils.ORDER_REGULAR_RT_ID
        );
        insert o;
        List<OrderItem> orderItems = new List<OrderItem> ();
        for (PricebookEntry pe : pricebookEntries) {
            OrderItem oi = new OrderItem(
                    PricebookEntryId = pe.Id, Quantity = 2, Discount_Sales__c = 300,
                    List_Price__c = 400, OrderId = o.Id, UnitPrice = 0
            );
            orderItems.add(oi);
        }
        insert orderItems;
        o.Status = ConstUtils.ORDER_ACCEPTED_STATUS;
        update o;
        acc.Count_Plan_Sets__c = 0;
        update acc;
        Test.startTest();
        Database.executeBatch(new AccountOrderItemsCountBatch());
        Test.stopTest();
        acc = [SELECT Count_Plan_Sets__c FROM Account WHERE ID = :acc.Id];

        System.assertEquals(4, acc.Count_Plan_Sets__c);
    }
}