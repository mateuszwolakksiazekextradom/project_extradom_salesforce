@RestResource(urlMapping='/app/filter/itemsets')
global class AppFilterItemSets {
    @HttpGet
    global static ExtradomApi.ItemSet doGet() {
                
        Map<String,String> canonicalParams = new Map<String,String>{};
        String type = RestContext.request.params.get('type');
        canonicalParams.put('type',type);
        String department = RestContext.request.params.containsKey('department')?RestContext.request.params.get('department'):null;
        String category = RestContext.request.params.containsKey('category')?RestContext.request.params.get('category'):null;
        String location = RestContext.request.params.containsKey('location')?String.escapeSingleQuotes(RestContext.request.params.get('location')):null;
        List<String> styles = RestContext.request.params.containsKey('styles')?String.escapeSingleQuotes(RestContext.request.params.get('styles')).split(','):null;
        List<String> colors = RestContext.request.params.containsKey('colors')?String.escapeSingleQuotes(RestContext.request.params.get('colors')).split(','):null;
        List<String> materials = RestContext.request.params.containsKey('materials')?String.escapeSingleQuotes(RestContext.request.params.get('materials')).split(','):null;
        String filterPrefix = '';
        String sObjectType = 'Item__c';
        if (materials!=null || colors!=null) {
            filterPrefix = 'Item__r.';
            sObjectType = 'Item_Color__c';
        }
        
        List<String> filters = new List<String>{filterPrefix+'RecordType.Name = :type', filterPrefix+'Main_Size_Item__c = null'};
        if (department!=null) {
            filters.add(filterPrefix+'Department__c = :department');
            canonicalParams.put('department',department);
        }
        if (category!=null) {
            filters.add(filterPrefix+'Category__c = :category');
            canonicalParams.put('category',category);
        }
        if (location!=null) {
            filters.add(filterPrefix+'Locations__c includes (\''+location+'\')');
            canonicalParams.put('location',location);
        }
        if (styles!=null) {
            filters.add(filterPrefix+'Styles__c includes (\''+String.join(styles,'\',\'')+'\')');
            canonicalParams.put('styles',String.join(styles,','));
        }
        if (colors!=null) {
            filters.add('Colors__c includes (\''+String.join(colors,'\',\'')+'\')');
            canonicalParams.put('colors',String.join(colors,','));
        }
        if (materials!=null) {
            filters.add('Materials__c includes (\''+String.join(materials,'\',\'')+'\')');
            canonicalParams.put('materials',String.join(materials,','));
        }
        
        String queryCount = 'SELECT count() FROM '+sObjectType+' WHERE ' + String.join(filters,' AND ');
        Integer total = Database.countQuery(queryCount);
        
        PageReference firstPage = new PageReference('/app/filter/items');
        firstPage.getParameters().putAll(canonicalParams);
        firstPage.getParameters().put('p','1');
        
        ExtradomApi.ItemSet itemSet = new ExtradomApi.ItemSet();
        itemSet.firstPageUrl = firstPage.getUrl();
        itemSet.total = total;
        
        if (UserInfo.getUserType()=='Guest') {
            RestContext.response.addHeader('Cache-Control', 'must-revalidate, max-age=0, s-maxage=21600');
        } else {
            RestContext.response.addHeader('Cache-Control', 'no-cache, must-revalidate, max-age=0, no-store, private, s-maxage=0');
        }
        
        return itemSet;
    }
}