@IsTest
private class AccountOrderTreeController_Test {

    @IsTest
    private static void shouldReturnAccountOrders() {
        // given
        Product2 product1 = new Product2(
                Name = 'Test Product 1', Family = ConstUtils.PRODUCT_GIFT_FAMILY, IsActive = true
        );
        Product2 product2 = new Product2(
                Name = 'Test Product 2', Family = ConstUtils.PRODUCT_DESIGN_FAMILY, IsActive = true
        );
        insert new List<Product2> {product1, product2};

        PricebookEntry spe1 = new PricebookEntry(
                Product2Id = product1.Id, IsActive = true, Pricebook2Id = Test.getStandardPricebookId(), UnitPrice = 100
        );
        PricebookEntry spe2 = new PricebookEntry(
                Product2Id = product2.Id, IsActive = true, Pricebook2Id = Test.getStandardPricebookId(), UnitPrice = 200
        );
        insert new List<PricebookEntry> {spe1, spe2};

        Account acc1 = new Account (Name = 'Test Name 1');
        Account acc2 = new Account (Name = 'Test Name 2');
        insert new List<Account> {acc1, acc2};
        Order order1 = new Order (
                AccountId = acc1.Id, EffectiveDate = System.now().date(), Status = ConstUtils.ORDER_DRAFT_STATUS,
                Pricebook2Id = Test.getStandardPricebookId(), RecordTypeId = ConstUtils.ORDER_REGULAR_RT_ID
        );
        Order order2 = new Order (
                AccountId = acc1.Id, EffectiveDate = System.now().date(), Status = ConstUtils.ORDER_DRAFT_STATUS,
                Pricebook2Id = Test.getStandardPricebookId(), RecordTypeId = ConstUtils.ORDER_REGULAR_RT_ID
        );
        Order order3 = new Order (
                AccountId = acc2.Id, EffectiveDate = System.now().date(), Status = ConstUtils.ORDER_DRAFT_STATUS,
                Pricebook2Id = Test.getStandardPricebookId(), RecordTypeId = ConstUtils.ORDER_REGULAR_RT_ID
        );
        insert new List<Order> {order1, order2, order3};
        OrderItem oi1 = new OrderItem(
                PricebookEntryId = spe1.Id, Quantity = 2, UnitPrice = 100,
                List_Price__c = 400, OrderId = order1.Id
        );
        OrderItem oi2 = new OrderItem(
                PricebookEntryId = spe2.Id, Quantity = 1, UnitPrice = 100,
                List_Price__c = 100, OrderId = order1.Id
        );
        insert new List<OrderItem>{oi1, oi2};

        // when
        List<Order> orders = AccountOrderTreeController.getOrders(acc1.Id);

        // then
        System.assertEquals(2, orders.size());
        for (Order o : orders) {
            if (o.Id == order1.Id) {
                System.assertEquals(2, o.OrderItems.size());
            }
            System.assertNotEquals(order3.Id, o.Id);
        }
    }
}