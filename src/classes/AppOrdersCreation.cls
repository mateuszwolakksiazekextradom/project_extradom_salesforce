@RestResource(urlMapping='/app/orders/create')
global class AppOrdersCreation {
    @HttpPost
    global static ExtradomApi.DesignOrderResult doPost() {
        RestRequest req = RestContext.request;
        ExtradomApi.DesignOrderInput input = (ExtradomApi.DesignOrderInput)JSON.deserialize(req.requestBody.toString(), ExtradomApi.DesignOrderInput.class);
        
        RestResponse res = RestContext.response;
        
        RestContext.response.addHeader('Cache-Control', 'no-cache, must-revalidate, max-age=0, no-store, private, s-maxage=0');
        String orderId = OrderUtils.saveOrder(input);
        
        List<OrderItem> orderItems = [
            SELECT Id, Version__c, OrderId, Order.Shipping_Method__c, Order.Payment_Method__c, Order.Pay_URL__c, Design__r.Id,
            Design__r.Code__c, Design__r.Full_Name__c, Design__r.Type__c, Design__r.Usable_Area__c, Design__r.Thumbnail_Base_URL__c,
            Design__r.Sketchfab_ID__c, Design__r.KW_Model__c, Design__r.Gross_Price__c, Design__r.Current_Price__c 
            FROM OrderItem WHERE OrderId = :orderId AND Product2.ProductCode = :input.code
        ];
        Order o = [SELECT Id, AccountId FROM Order WHERE Id = :orderId];
        if (req.headers.containsKey('gaClientId')) {
            OrderMethods.updateGaClientId(o.AccountId, req.headers.get('gaClientId'));
        }
        OrderItem oi = orderItems.size() > 0 ? orderItems.get(0) : null;
        ExtradomApi.DesignOrderResult orderres = new ExtradomApi.DesignOrderResult();
        orderres.id = oi.OrderId;
        if (oi.Design__r != null) {
            orderres.design = new ExtradomApi.DesignResult(oi.Design__r, null);
        }
        orderres.version = oi.Version__c;
        if (oi.Order.Payment_Method__c == ConstUtils.ORDER_ONLINE_PAYMENT) {
            orderres.payUrl = oi.Order.Pay_URL__c;
        }
        orderres.message = ConstUtils.ORDER_REST_CREATE_MESSAGE;
        orderres.comments = ConstUtils.ORDER_REST_CREATE_COMMENTS;
        return orderres;
    }
}