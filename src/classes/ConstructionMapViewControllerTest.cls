@IsTest
private class ConstructionMapViewControllerTest {
    static testMethod void myUnitTest() {
    
        PageReference pageRef = new PageReference('http://extradom.pl/');
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('designid', '');
    
        ConstructionMapViewController controller = New ConstructionMapViewController();
        System.assertEquals(controller.isExactReady,false);
        System.assertEquals(controller.mapAccess,'No Access');
        Construction__c c = new Construction__c(Name='Example Construction',Location__longitude__s=0.00,Location__latitude__s=0.00,Location_Visibility__c='Default');
        insert c;
        System.assertEquals(controller.isExactReady,true);
        System.assertEquals(controller.mapAccess,'Default');
        controller.getPublicConstructions();
        controller.getPublicConstructionsForDesign();
        controller.getPublicConstructionsNearMe();
        controller.getUserPhoneVerification();
        controller.verifyPhone();
        controller.verifyRequestCode();
    }
}