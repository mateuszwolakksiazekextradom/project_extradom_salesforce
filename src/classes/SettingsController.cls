global with sharing class SettingsController {

    public String oldPassword { get; set; }
    public String newPassword { get; set; }
    public String verifyNewPassword { get; set; }

    global SettingsController() { }
    
    global PageReference changePassword() {
        return Site.changePassword(this.newPassword, this.verifyNewPassword, this.oldPassword);
    }
    
}