@IsTest
private class EditOrderAddressDataController_Test {
    @IsTest
    private static void shouldReturnAccountContacts() {
        // given
        Account acc1 = new Account(Name = 'TestName 1');
        Account acc2 = new Account(Name = 'TestName 2');
        insert new List<Account> {acc1, acc2};
        Contact c1acc1 = new Contact(LastName = 'TestContact1', Email = 'testcontact1@test.pl', Phone = '500000001', AccountId = acc1.Id);
        Contact c2acc1 = new Contact(LastName = 'TestContact2', Email = 'testcontact2@test.pl', Phone = '500000002', AccountId = acc1.Id);
        Contact c1acc2 = new Contact(LastName = 'TestContact3', Email = 'testcontact3@test.pl', Phone = '500000003', AccountId = acc2.Id);
        Contact c2acc2 = new Contact(LastName = 'TestContact4', Email = 'testcontact4@test.pl', Phone = '500000004', AccountId = acc2.Id);
        insert new List<Contact> {c1acc1, c2acc1, c1acc2, c2acc2};

        // when
        List<Contact> contacts = EditOrderAddressDataController.getContacts(acc1.Id);
        Map<Id, Contact> contactMap = new Map<Id, Contact> (contacts);

        // then
        System.assertEquals(2, contacts.size());
        System.assert(contactMap.containsKey(c1acc1.Id));
        System.assert(contactMap.containsKey(c2acc1.Id));
    }
}