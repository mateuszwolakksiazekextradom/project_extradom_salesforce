public with sharing class OfferViewExtension {

    private final Offer__c o;
    public OfferViewExtension(ApexPages.StandardController stdController) {
        this.o = (Offer__c)stdController.getRecord();
    }
    
    public List<Bid__c> getTopBids() {
        return [SELECT Id, Pracownia__c, Value__c, LastModifiedDate FROM Bid__c WHERE Offer__c = :this.o.Id ORDER BY Value__c DESC NULLS LAST, LastModifiedDate ASC NULLS LAST];
    }
    
    public List<Bid__c> getTop12Bids() {
        return [SELECT Id, Pracownia__c, Value__c, LastModifiedDate FROM Bid__c WHERE Offer__c = :this.o.Id ORDER BY Value__c DESC NULLS LAST, LastModifiedDate ASC NULLS LAST LIMIT 12];
    }
    
}