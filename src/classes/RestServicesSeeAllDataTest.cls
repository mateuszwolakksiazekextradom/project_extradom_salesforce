@isTest(SeeAllData=true)
private class RestServicesSeeAllDataTest {

    static testMethod void myUnitTest() {
        
        test.startTest();

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/app/users/me/following/designs';  
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.request.addParameter('p','1');
        RestContext.response = res;
        AppUsersMeFollowingDesigns.doGet();
        
        RestRequest req2 = new RestRequest(); 
        RestResponse res2 = new RestResponse();
        req2.requestURI = '/app/users/me';  
        req2.httpMethod = 'GET';
        RestContext.request = req2;
        RestContext.response = res2;
        AppUsersMe.doGet();
        
        RestRequest req3 = new RestRequest(); 
        RestResponse res3 = new RestResponse();
        req3.requestURI = '/app/users/me/notifications';  
        req3.httpMethod = 'GET';
        RestContext.request = req3;
        RestContext.response = res3;
        AppUsersMeNotifications.doGet();
        
        RestRequest req4 = new RestRequest(); 
        RestResponse res4 = new RestResponse();
        req4.requestURI = '/app/users/me/notifications';  
        req4.httpMethod = 'PATCH';
        RestContext.request = req4;
        RestContext.response = res4;
        AppUsersMeNotifications.doPatch(false);

        test.stopTest();

    }
    
}