public without sharing class DesignAddonsBasicInfoController {
    
    public Id designId;
    public List<Product2> addons {private set; get;}
    public Design__c design {private set; get;}
    public void setdesignId (Id designId) {
        designId = designId;
        if (String.isNotBlank(designId)) {
            addons = [
                SELECT Id, Name, Type__c, Version_Required__c, Description, Subtype__c, Product_Description__r.HTML_Description__c, 
                    (
                        SELECT Id, Regular_Price__c, UnitPrice 
                        FROM PricebookEntries 
                        WHERE Pricebook2.IsActive=true AND Pricebook2.IsStandard=true
                    ) 
                FROM Product2 
                WHERE Design__r.Id = :designId 
                AND Family = 'Add-on' 
                AND isActive = true 
                ORDER BY Subtype__c
            ];
            
            design = [SELECT Id, Current_Price__c, Canonical_Full_URL__c, Stage__c FROM Design__c WHERE Id = :designId];
        }
    }
    
    
    public String getDesignId () {
        return designId;
    }
}