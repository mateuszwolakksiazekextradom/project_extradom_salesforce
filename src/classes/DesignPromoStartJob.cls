global without sharing class DesignPromoStartJob implements Schedulable {
    public void execute(SchedulableContext sc) {
        Database.executeBatch(new DesignPromoStartBatch());
    }
}