public without sharing class OptInsController {
    private final ID contactId;
    public OptInsController() {
        this.contactId = ApexPages.currentPage().getParameters().get('id');
    }
    public PageReference initSetOptInMarketing() {
        Contact c = new Contact();
        c.Id = this.contactId;
        c.Opt_In_Marketing__c = true;
        update c;
        return null;
    }
    public PageReference initSetOptInLeadForward() {
        Contact c = new Contact();
        c.Id = this.contactId;
        c.Opt_In_Lead_Forward__c = true;
        update c;
        return null;
    }
}