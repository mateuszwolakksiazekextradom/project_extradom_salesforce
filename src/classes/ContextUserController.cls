public with sharing class ContextUserController {

    private String newsPageParam;

    public ContextUserController(ApexPages.StandardController controller) {

    }

    
    public ContextUserController() {
    }
    
    public ConnectApi.FollowingPage getFollowingsDesigns() {
        return ConnectApi.ChatterUsers.getFollowings(Network.getNetworkId(), 'me', 'a06', null, 100);
    }
    
    public void doNewsRefreshToken() {
        ConnectApi.FeedElementPage feedElementPage = ConnectApi.ChatterFeeds.getFeedElementsFromFeed(Network.getNetworkId(), ConnectApi.FeedType.News, 'me', null, null, null, null, ConnectApi.FeedSortOrder.LastModifiedDateDesc);
        this.newsPageParam = feedElementPage.currentPageToken;
        try {
            Chatter_Custom_Settings__c ccs = Chatter_Custom_Settings__c.getInstance();
            ccs.Last_News_Feed_Refresh_Token__c = feedElementPage.updatesToken;
            ccs.Last_News_Feed_Refresh_Count_Unread__c = 0;
            upsert ccs;
        } catch (Exception e) {
        }
    }
    
    public ConnectApi.UserDetail getUser() {
        return ConnectApiToolsForVF.userMe;
    }
    
    public ConnectApi.FeedElementPage getFeedElementsFromFeedNews() {
        return ConnectApi.ChatterFeeds.getFeedElementsFromFeed(Network.getNetworkId(), ConnectApi.FeedType.News, 'me', null, null, this.newsPageParam, null, ConnectApi.FeedSortOrder.LastModifiedDateDesc);
    }
    
    public ConnectApi.FeedElementPage getFeedElementsFromFeedFiles() {
        return ConnectApi.ChatterFeeds.getFeedElementsFromFeed(Network.getNetworkId(), ConnectApi.FeedType.Files, 'me');
    }
    
    public ConnectApi.FeedElementPage getFeedElementsFromFeedNewsAllQuestions() {
        return ConnectApi.ChatterFeeds.getFeedElementsFromFeed(Network.getNetworkId(), ConnectApi.FeedType.News, 'me', null, null, null, null, ConnectApi.FeedSortOrder.LastModifiedDateDesc, ConnectApi.FeedFilter.AllQuestions);
    }
    
    public ConnectApi.FeedElementPage getFeedElementsFromFeedNewsToMe() {
        return ConnectApi.ChatterFeeds.getFeedElementsFromFeed(Network.getNetworkId(), ConnectApi.FeedType.To, 'me', null, null, ConnectApi.FeedSortOrder.LastModifiedDateDesc);
    }
    
    public ConnectApi.FeedElementPage getFeedElementsFromFeedHome() {
        return ConnectApi.ChatterFeeds.getFeedElementsFromFeed(Network.getNetworkId(), ConnectApi.FeedType.Home);
    }
    
    public ConnectApi.FeedElementPage getFeedElementsFromFeedBookmarksThumbs() {
        return ConnectApi.ChatterFeeds.getFeedElementsFromFeed(Network.getNetworkId(), ConnectApi.FeedType.Bookmarks, 'me', null, 6, null);
    }
    
    public ConnectApi.FeedElementPage getFeedElementsFromFeedBookmarks() {
        return ConnectApi.ChatterFeeds.getFeedElementsFromFeed(Network.getNetworkId(), ConnectApi.FeedType.Bookmarks, 'me');
    }
    
    public void doIsNewsModified() {
        try {
            Chatter_Custom_Settings__c ccs = Chatter_Custom_Settings__c.getInstance();
            Datetime nextRefresh = ccs.LastModifiedDate.addMinutes(2);
            if (nextRefresh<=System.now()) {
                ConnectApi.FeedElementPage feedElementPage = ConnectApi.ChatterFeeds.getFeedElementsUpdatedSince(Network.getNetworkId(), ConnectApi.FeedType.News, 'me', null, null, null, null, ccs.Last_News_Feed_Refresh_Token__c);
                ccs.Last_News_Feed_Refresh_Count_Unread__c = feedElementPage.elements.size();
                upsert ccs;
            }
        } catch (Exception e) {
        }
    }
    
    public ConnectApi.UnreadConversationCount getUnreadCount() {
        return ConnectApi.ChatterMessages.getUnreadCount(Network.getNetworkId());
    }
    
    public ConnectApi.ChatterConversationPage getConversations() {
        return ConnectApi.ChatterMessages.getConversations(Network.getNetworkId());
    }
    
    public ConnectApi.ChatterConversationPage getConversationsByString() {
        return ConnectApi.ChatterMessages.searchConversations(Network.getNetworkId(), Apexpages.currentPage().getParameters().get('q'));
    }
    
    public ConnectApi.TopicPage getTrendingTopics() {
        return ConnectApi.Topics.getTrendingTopics(Network.getNetworkId());
    }
    
    public ConnectApi.ManagedTopicCollection getManagedTopics() {
        return ConnectApi.ManagedTopics.getManagedTopics(Network.getNetworkId(), ConnectApi.ManagedTopicType.Navigational);
    }
    
    public List<List<SObject>> getSearchResults() {
        String searchString = Apexpages.currentPage().getParameters().get('q').replace('?','').replace('*','').normalizeSpace();
        searchString += '*';
        List<List<SObject>> results = [FIND :searchString.replace(' ','* ') IN NAME FIELDS RETURNING Design__c(Id, Full_Name__c, Code__c, Thumbnail_Base_URL__c, Usable_Area__c, Community_Builders__c WHERE Status__c = 'Active' LIMIT 50), Page__c(Id, Name, URL__c WHERE Status__c = 'Active' AND Type__c = 'Collection' AND Subtype__c = 'Categories' LIMIT 3)];
        if (results[0].size()>0) {
            List<Design__c> designResults = new List<Design__c>();
            for (Design__c d : (List<Design__c>)results[0]) {
                if (d.Full_Name__c == searchString.replace('*','') && designResults.size()<10) {
                    designResults.add(d);
                }
            }
            for (Design__c d : (List<Design__c>)results[0]) {
                if (d.Full_Name__c != searchString.replace('*','') && designResults.size()<10) {
                    designResults.add(d);
                }
            }
            results[0] = designResults;
            return results;
        }
        searchString = Apexpages.currentPage().getParameters().get('q').replace('?','').replace('*','').normalizeSpace() + ' OR ' + searchString.replace(' ', ' OR ');
        
        return [FIND :searchString IN NAME FIELDS RETURNING Design__c(Id, Full_Name__c, Code__c, Thumbnail_Base_URL__c, Usable_Area__c, Community_Builders__c WHERE Status__c = 'Active' LIMIT 10), Page__c(Id, Name, URL__c WHERE Status__c = 'Active' AND Type__c = 'Collection' AND Subtype__c = 'Categories' LIMIT 3)];
    }
    
    public ConnectApi.UserPage getSearchUsers() {
        String searchString = Apexpages.currentPage().getParameters().get('q').replace('?','').replace('*','').normalizeSpace();
        searchString += '*';
        searchString = searchString.replace(' ', ' OR ');
        return ConnectApi.ChatterUsers.searchUsers(Network.getNetworkId(), searchString, null, 5);
    }
    
    public ConnectApi.TopicPage getTopics() {
        String searchString = Apexpages.currentPage().getParameters().get('q').replace('?','').replace('*','').normalizeSpace();
        searchString += '*';
        searchString = searchString.replace(' ', ' OR ');
        return ConnectApi.Topics.getTopics(Network.getNetworkId(), searchString, null, 5);
    }
    
    public ConnectApi.FeedElementPage getSearchFeedElements() {
        String searchString = Apexpages.currentPage().getParameters().get('q').replace(' ', ' OR ')+'*';
        return ConnectApi.ChatterFeeds.searchFeedElements(Network.getNetworkId(), searchString, null, 5, ConnectApi.FeedSortOrder.LastModifiedDateDesc);
    }
    
    public ConnectApi.FeedElementPage getSearchFeedElementsInExpertGroupFeed() {
        String searchString = Apexpages.currentPage().getParameters().get('q').replace('?','').replace('*','').normalizeSpace();
        searchString += '*';
        searchString = searchString.replace(' ', ' OR ');
        ConnectApi.ChatterGroupPage groupPage = ConnectApi.ChatterGroups.searchGroups(Network.getNetworkId(), '"Porady Ekspertów"', null, 1);
        try {
            return ConnectApi.ChatterFeeds.searchFeedElementsInFeed(Network.getNetworkId(), ConnectApi.FeedType.Record, groupPage.groups[0].id, null, 5, ConnectApi.FeedSortOrder.LastModifiedDateDesc, searchString);
        } catch(Exception e) {
        }
        return ConnectApi.ChatterFeeds.searchFeedElements(Network.getNetworkId(), '"Porady Ekspertów" ' + searchString, null, 5, ConnectApi.FeedSortOrder.LastModifiedDateDesc);
    }
    
    public ConnectApi.FeedElementPage getFeedElementsFromFeedSolvedQuestions() {
        return ConnectApi.ChatterFeeds.getFeedElementsFromFeed(Network.getNetworkId(), ConnectApi.FeedType.Home, null, null, null, null, null, ConnectApi.FeedFilter.SolvedQuestions);
    }
    
    public ConnectApi.FeedElementPage getFeedElementsFromNewsFeedAllQuestions() {
        return ConnectApi.ChatterFeeds.getFeedElementsFromFeed(Network.getNetworkId(), ConnectApi.FeedType.News, null, null, null, null, null, ConnectApi.FeedFilter.AllQuestions);
    }
    
    public List<Plot__c> getMyFollowingPlots() {
        List<Id> ids = new List<Id>{};
        for(ConnectApi.Subscription sub : ConnectApi.ChatterUsers.getFollowings(Network.getNetworkId(), 'me', 'a0l', null, 100).following) {
            ids.add(sub.subject.id);
        }
        return [SELECT Id, Name, Area__c, Center__latitude__s, Center__longitude__s, Geometry_building_MAIL__c, Location__r.Parent_Location__r.Name, (SELECT Id, OwnerId FROM Constructions__r) FROM Plot__c WHERE Id IN :ids];
    }
    
    public List<Plot__c> getPlotsForMyConstructions() {
        List<Id> ids = new List<Id>{};
        for (Construction__c c : [SELECT Id, Plot__r.Id FROM Construction__c WHERE OwnerId = :UserInfo.getUserId() AND Plot__c != null]) {
            ids.add(c.Plot__r.Id);
        }
        return [SELECT Id, Name, Area__c, Center__latitude__s, Center__longitude__s, Geometry_building_MAIL__c, Location__r.Parent_Location__r.Name, (SELECT Id, OwnerId FROM Constructions__r) FROM Plot__c WHERE Id IN :ids];
    }
    
    public List<ConnectApi.FeedElement> getLatestAllNetworksPostsByString() {
        List<Id> ids = new List<Id>{};
        Integer offRows = integer.valueof(Apexpages.currentPage().getParameters().get('q'));
        for (Design__Feed d_fi : [SELECT Id FROM Design__Feed WHERE CreatedDate = LAST_N_DAYS:10 ORDER BY CreatedDate DESC LIMIT 300 OFFSET :offRows]) {
            ids.add(d_fi.Id);
        }
        List<ConnectApi.FeedElement> feedElements = new List<ConnectApi.FeedElement>{};
        ConnectApi.BatchResult[] batchResults = ConnectApi.ChatterFeeds.getFeedElementBatch(Network.getNetworkId(), ids);
        for (ConnectApi.BatchResult batchResult : batchResults) {
            if (batchResult.isSuccess()) {
                ConnectApi.FeedElement element;
                if (batchResult.getResult() instanceof ConnectApi.FeedElement) {
                    element = (ConnectApi.FeedElement) batchResult.getResult();
                    feedElements.add(element);
                }
            }
        }
        return feedElements;
    }
    
    public List<ConnectApi.FeedElement> getLatestAllNetworksPosts() {
        List<Id> ids = new List<Id>{};
        for (Design__Feed d_fi : [SELECT Id FROM Design__Feed WHERE CreatedDate = LAST_N_DAYS:10 ORDER BY CreatedDate DESC LIMIT 300]) {
            ids.add(d_fi.Id);
        }
        List<ConnectApi.FeedElement> feedElements = new List<ConnectApi.FeedElement>{};
        ConnectApi.BatchResult[] batchResults = ConnectApi.ChatterFeeds.getFeedElementBatch(Network.getNetworkId(), ids);
        for (ConnectApi.BatchResult batchResult : batchResults) {
            if (batchResult.isSuccess()) {
                ConnectApi.FeedElement element;
                if (batchResult.getResult() instanceof ConnectApi.FeedElement) {
                    element = (ConnectApi.FeedElement) batchResult.getResult();
                    feedElements.add(element);
                }
            }
        }
        return feedElements;
    }
    
    public ConnectApi.FeedElement getFeaturedFeedElement() {
        Community_Home_Feed_Settings__c chfs = Community_Home_Feed_Settings__c.getInstance();
        try {
            if (String.isNotBlank(chfs.Featured_Feed_Item__c)) {
                return ConnectApi.ChatterFeeds.getFeedElement(Network.getNetworkId(), chfs.Featured_Feed_Item__c);
            }
        } catch (Exception e) {
        }
        return null;
    }
    
    public List<Set__c> getSavedDesignSets() {
        return [SELECT Id, Name, type__c, mask__c, minarea__c, maxarea__c, maxplotsize__c, maxheight__c, maxfootprint__c, minroofslope__c, maxroofslope__c, rooms__c, level__c, garage__c, basement__c, livingtype__c, roof__c, ridge__c, construction__c, ceiling__c, eco__c, garagelocation__c, style__c, shape__c, veranda__c, pool__c, wintergarden__c, terrace__c, oriel__c, mezzanine__c, entrance__c, border__c, carport__c, cost__c, gfroom__c, eaves__c, scarp__c, fireplace__c, bullseye__c FROM Set__c WHERE OwnerId = :UserInfo.getUserId() ORDER BY CreatedDate DESC];
    }

}