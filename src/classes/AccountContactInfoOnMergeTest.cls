@isTest
private class AccountContactInfoOnMergeTest {
    static testMethod void myTest(){
           
        List<Account> testAccounts = new List<Account> {new Account(Name='Test Account'), new Account(Name='Test Account Second'), new Account(Name='Account Test'), new Account(Name='Account Test Second')};
        insert testAccounts;
               
        List<Contact> testContacts = new List<Contact>{new Contact(LastName='Test1', Phone='100200300', AccountId=testAccounts.get(0).Id), new Contact(LastName='Test1', Email='mergetest@email.com', AccountId=testAccounts.get(2).Id)};
        insert testContacts;
        
        Account firstMasterAcc = [SELECT Id FROM Account WHERE Id = :testAccounts.get(0).Id];
        Account secondMasterAcc = [SELECT Id FROM Account WHERE Id = :testAccounts.get(3).Id];
        Account firstMergeAcc = [SELECT Id FROM Account WHERE Id = :testAccounts.get(1).Id];
        Account secondMergeAcc = [SELECT Id FROM Account WHERE Id = :testAccounts.get(2).Id];
        
        try {
            merge firstMasterAcc firstMergeAcc;
        } catch (DmlException e) {
        }
        
        try {
            merge secondMasterAcc secondMergeAcc;
        } catch (DmlException e) {
        }
             
        Account testAfterMerge = [SELECT Id, Has_Contact_with_Phone__c, Has_Contact_with_Email__c FROM Account WHERE Id = :firstMasterAcc.Id];
        Account testAfterMergeTwo = [SELECT Id, Has_Contact_with_Phone__c, Has_Contact_with_Email__c FROM Account WHERE Id = :secondMasterAcc.Id];
                
        System.assertEquals(testAfterMerge.Has_Contact_with_Phone__c, true);
        System.assertEquals(testAfterMergeTwo.Has_Contact_with_Email__c, true);
    }
}