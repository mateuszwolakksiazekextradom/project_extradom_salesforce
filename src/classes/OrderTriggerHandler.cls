public without sharing class OrderTriggerHandler extends TriggerHandler {
    public override void beforeInsert() {
        List<Order> orders = (List<Order>) Trigger.new;
        cleanPhones(orders);
        attachContacts(orders);
        setShippingCosts(orders);
        setSentDate(orders, null);
        setDefaultPricebooks(orders);            
    }

    public override void afterInsert() {
        List<Order> orders = (List<Order>) Trigger.new;
        addDefaultProducts(orders);            
    }

    public override void beforeUpdate() {
        List<Order> orders = (List<Order>) Trigger.new;
        Map<Id, Order> oldOrders = (Map<Id, Order>) Trigger.oldMap;
        cleanPhones(orders);
        attachContacts(orders);
        setShippingCosts(orders);
        setSentDate(orders, oldOrders);              
    }

    public override void afterUpdate() {
        List<Order> orders = (List<Order>) Trigger.new;
        Map<Id, Order> oldOrders = (Map<Id, Order>) Trigger.oldMap;
        changeOrderItemAccountIds(orders, oldOrders);
        setLatestAccountDesigns(orders, oldOrders);
        setCampaignMembersOnActivation(orders, oldOrders);
        recalculateAccountItems(orders, oldOrders);
        setInfluenceUsers(orders, oldOrders);
        revertDealerLeadUsers(orders, oldOrders);
        revertCanceledOrdersInfluenceUsers(orders, oldOrders);
        sendGATrackingData(orders, oldOrders);
        sendNotificationToAiqua(orders, oldOrders);
        removeIdeaStage(orders, oldOrders);
        closeOpenFollowUpCalls(orders, oldOrders);            
    }
    
    private void cleanPhones(List<Order> newOrders) {
        for (Order o : newOrders) {
            o.BillingPhone__c = Utils.normalizePhone(o.BillingPhone__c);
            o.ShippingPhone__c = Utils.normalizePhone(o.ShippingPhone__c);
        }
    }

    private void setDefaultPricebooks(List<Order> newOrders) {

        ID stdPricebookId = Test.isRunningTest()?Test.getStandardPricebookId():[SELECT Id FROM Pricebook2 WHERE isStandard = true][0].Id;
        for (Order o : newOrders) {
            if (String.isBlank(o.Pricebook2Id)) {
                o.Pricebook2Id = stdPricebookId;
            }
        }

    }
    
    private void addDefaultProducts(List<Order> newOrders) {

        List<OrderItem> ois = new List<OrderItem> ();
        List<Order> os = new List<Order> ();
        Set<Id> productIds = new Set<Id> ();
        for (Order o : newOrders) {
            if (String.isNotBlank(o.Default_Product__c)) {
                productIds.add(o.Default_Product__c);
            }
        }
        List<Product2> products = [SELECT Id, (SELECT Id, Pricebook2Id, UnitPrice FROM PricebookEntries WHERE IsActive = true) FROM Product2 WHERE Id IN :productIds AND IsActive = true];
        for (Order o : newOrders) {
            if (String.isNotBlank(o.Default_Product__c)) {
                for (Product2 product : products) {
                    if (product.Id == o.Default_Product__c) {
                        for (PricebookEntry pe : product.PricebookEntries) {
                            if (pe.Pricebook2Id == o.Pricebook2Id) {
                                OrderItem oi = new OrderItem(PricebookEntryId=pe.Id, OrderId=o.Id, UnitPrice=pe.UnitPrice, Quantity=1.0, List_Price__c=pe.UnitPrice);
                                ois.add(oi);
                                Order order = new Order(Id=o.Id, Status='Accepted');
                                os.add(order);
                                break;
                            }
                        }
                        break;
                    }
                }
            }
        }
        insert ois;
        update os;

    }

    private void setSentDate(List<Order> newOrders, Map<Id, Order> oldOrderMap) {
        for (Order o : newOrders) {
            if (oldOrderMap == null ||
                    (oldOrderMap.get(o.Id).Status != ConstUtils.ORDER_SENT_STATUS && o.Status == ConstUtils.ORDER_SENT_STATUS)){
                o.Sent_Date__c = System.now().date();
            }
        }
    }
    private void setShippingCosts(List<Order> newOrders) {
        Map<String, Decimal> shippingCosts = Utils.getShippingCosts();
        for (Order o : newOrders) {
            if (String.isNotBlank(o.Payment_Method__c) && String.isNotBlank(o.Shipping_Method__c)
                && o.Shipping_Cost__c == null) {
                    if (shippingCosts.containsKey(o.Shipping_Method__c + o.Payment_Method__c)) {
                        o.Shipping_Cost__c = shippingCosts.get(o.Shipping_Method__c + o.Payment_Method__c);
                    }
            }
        }
    }

    private void changeOrderItemAccountIds(List<Order> newOrders, Map<Id, Order> oldOrderMap) {
        Set<String> orderIds = new Set<String> ();
        for (Order o : newOrders) {
            if (oldOrderMap != null && oldOrderMap.containsKey(o.Id) && oldOrderMap.get(o.Id).AccountId != o.AccountId) {
                orderIds.add(o.Id);
            }
        }

        List<OrderItem> orderItems = [SELECT Id, Account__c, Order.AccountId FROM OrderItem WHERE OrderId in :orderIds];
        for (OrderItem oi : orderItems) {
            oi.Account__c = oi.Order.AccountId;
        }
        update orderItems;
    }

    private void setLatestAccountDesigns(List<Order> newOrders, Map<Id, Order> oldOrderMap) {
        Set<String> orderIds = new Set<String> ();
        Set<String> accountIds = new Set<String> ();

        for (Order o : newOrders) {
            if (oldOrderMap != null && oldOrderMap.containsKey(o.Id) && oldOrderMap.get(o.Id).StatusCode != o.StatusCode
                    && o.StatusCode == ConstUtils.ORDER_ACTIVATED_STATUS_CODE && String.isNotBlank(o.AccountId)) {
                orderIds.add(o.Id);
                accountIds.add(o.AccountId);
            }
        }

        if (orderIds.size() > 0 && accountIds.size() > 0) {
            List<OrderItem> orderItems = [
                    SELECT Id, Design__r.Type__c, Design__c, Order.AccountId FROM OrderItem
                    WHERE OrderId in :orderIds AND Design__c != null AND Product2.Family = :ConstUtils.PRODUCT_DESIGN_FAMILY
            ];
            Map<Id, Account> accounts = new Map<Id, Account> ([SELECT Id, Latest_Design__c, Latest_Design_Non_House__c FROM Account WHERE Id in :accountIds]);
            Map<Id, Account> accountsToUpdate = new Map<Id, Account> ();
            for (OrderItem oi : orderItems) {
                if (accounts.containsKey(oi.Order.AccountId)) {
                    Account acc = accounts.get(oi.Order.AccountId);
                    if (oi.Design__r.Type__c == ConstUtils.DESIGN_HOUSE_TYPE) {
                        acc.Latest_Design__c = oi.Design__c;
                        accountsToUpdate.put(acc.Id, acc);
                    } else if (oi.Design__r.Type__c != ConstUtils.DESIGN_HOUSE_TYPE) {
                        acc.Latest_Design_Non_House__c = oi.Design__c;
                        accountsToUpdate.put(acc.Id, acc);
                    }
                }
            }
            update accountsToUpdate.values();
        }
    }
    
    private void setCampaignMembersOnActivation(List<Order> newOrders, Map<Id, Order> oldOrderMap) {
        List<CampaignMember> pkoCampaignMembers = new List<CampaignMember>{};
        Set<Id> pkoContactIds = new Set<Id>{};
        Campaign_Settings__c cs = Campaign_Settings__c.getInstance();        
        for (Order o : newOrders) {
            if (oldOrderMap != null && oldOrderMap.containsKey(o.Id) && oldOrderMap.get(o.Id).StatusCode != o.StatusCode
                    && String.isNotBlank(cs.PKO_Campaign_Id__c)
                    && o.StatusCode == ConstUtils.ORDER_ACTIVATED_STATUS_CODE && String.isNotBlank(o.AccountId)
                    && String.isNotBlank(o.BillToContactId)
                    && o.Opt_In_Phone__c == true && o.Opt_In_Email__c == true
                    && o.Opt_In_Profiling__c == true && o.Opt_In_PKO__c == true) {
                if (!pkoContactIds.contains(o.BillToContactId)) {
                    pkoCampaignMembers.add(new CampaignMember(CampaignId=cs.PKO_Campaign_Id__c, ContactId=o.BillToContactId, First_Name__c=o.BillingFirstName__c, Last_Name__c=o.BillingLastName__c, Email__c=o.BillingEmail__c, Phone__c=o.BillingPhone__c, Postal_Code__c=o.BillingPostalCode));
                    pkoContactIds.add(o.BillToContactId);
                }
            }
        }

        Set<Id> contactIds = pkoContactIds;
        List<CampaignMember> campaignMembers = pkoCampaignMembers;
        if (!campaignMembers.isEmpty()) {
            for (Contact c : [SELECT Id, (SELECT Id, CampaignId, First_Name__c, Last_Name__c, Email__c, Phone__c, Postal_Code__c FROM CampaignMembers) FROM Contact WHERE Id IN :contactIds]) {
                for (CampaignMember cm : c.CampaignMembers) {
                    for (CampaignMember cmToInsert : campaignMembers) {
                        if (cmToInsert.ContactId == c.Id && cmToInsert.CampaignId == cm.CampaignId) {
                            cmToInsert.Id = cm.Id;
                            cmToInsert.First_Name__c = cm.First_Name__c;
                            cmToInsert.Last_Name__c = cm.Last_Name__c;
                            cmToInsert.Email__c = cm.Email__c;
                            cmToInsert.Phone__c = cm.Phone__c;
                            cmToInsert.Postal_Code__c = cm.Postal_Code__c;
                        }
                    }
                }
            }
            upsert campaignMembers;            
        }
    }
    
    private void sendGATrackingData(List<Order> newOrders, Map<Id, Order> oldOrderMap) {
        for (Order o : newOrders) {
            if (oldOrderMap != null && oldOrderMap.containsKey(o.Id) && oldOrderMap.get(o.Id).Status != o.Status
                    && o.Status == ConstUtils.ORDER_SENT_STATUS && String.isNotBlank(o.AccountId) && o.TotalAmount != null && o.TotalAmount > 0) {
                GoogleAnalyticsTools.collectOrder(o.Id);
                break;
            }
        }
    }

    private void sendNotificationToAiqua(List<Order> newOrders, Map<Id, Order> oldOrderMap) {
        List<Id> accountIdsWithSentOrderds = new List<Id>();

        Map<Id, Order> newOrderMap = new Map<Id, Order>(newOrders);
        for(Id key : newOrderMap.keySet()) {
            Order newOrder = newOrderMap.get(key);
            Order oldOrder = oldOrderMap.get(key);

            if(
                oldOrder.Status != newOrder.Status &&
                newOrder.Status == ConstUtils.ORDER_SENT_STATUS &&
                String.isNotBlank(newOrder.AccountId) &&
                newOrder.TotalAmount != null &&
                newOrder.TotalAmount > 0 &&
                newOrder.AccountId != null
            ) {
                accountIdsWithSentOrderds.add(newOrder.AccountId);
            }

        }

        if(!accountIdsWithSentOrderds.isEmpty()) {
            AiquaIntegration.sendCustomersWhoAreAlreadyBuyers(accountIdsWithSentOrderds); 
        }
    }
    
    private void removeIdeaStage(List<Order> newOrders, Map<Id, Order> oldOrderMap) {
        Set<Id> orderIds = new Set<Id>();
        for (Order o : newOrders) {
            if (oldOrderMap != null && oldOrderMap.containsKey(o.Id) && oldOrderMap.get(o.Id).Status != o.Status && o.Status == ConstUtils.ORDER_SENT_STATUS) {
                orderIds.add(o.Id);
            }
        }
        
        if (orderIds.size() > 0) {
            List<Design__c> designs = new List<Design__c>();
            for (Design__c d : [SELECT Id, Stage__c FROM Design__c WHERE (Stage__c = :ConstUtils.DESIGN_IDEA_STAGE OR (Stage__c = :ConstUtils.DESIGN_NEEDS_UPDATE_STAGE AND Supplier__r.Code__c != 'WAJ')) AND Id IN (SELECT Design__c FROM OrderItem WHERE PricebookEntry.Product2.Family = :ConstUtils.PRODUCT_DESIGN_FAMILY AND OrderId IN :orderIds)]) {
                d.Stage__c = null;
                designs.add(d);
            }
            update designs;
        }
    }
    
    private void closeOpenFollowUpCalls(List<Order> newOrders, Map<Id, Order> oldOrderMap) {
        Set<Id> orderIds = new Set<Id>();
        for (Order o : newOrders) {
            if (oldOrderMap != null && oldOrderMap.containsKey(o.Id) && oldOrderMap.get(o.Id).StatusCode != o.StatusCode && o.StatusCode == ConstUtils.ORDER_ACTIVATED_STATUS_CODE && !o.isReductionOrder) {
                orderIds.add(o.Id);
            }
        }
                
        Set<Id> accountsToClose = new Set<Id>();
        for (Order o : [SELECT Id, AccountId FROM Order WHERE Id IN (SELECT OrderId FROM OrderItem WHERE Quantity > 0 AND PricebookEntry.Product2.Family = :ConstUtils.PRODUCT_DESIGN_FAMILY AND OrderId IN :orderIds)]) {
            accountsToClose.add(o.AccountId);
        }
        
        List<Task> tasksToClose = new List<Task>();
        for (Task t : [SELECT Id, Status FROM Task WHERE AccountId IN :accountsToClose AND Type = :ConstUtils.TASK_FOLLOW_UP_TYPE AND Status = :ConstUtils.TASK_NOT_STARTED_STATUS]) {
            t.Status = ConstUtils.TASK_COMPLETED_STATUS;
            tasksToClose.add(t);
        }
        update tasksToClose;
    }

    private void setInfluenceUsers(List<Order> newOrders, Map<Id, Order> oldOrderMap) {
        Set<String> orderIds = new Set<String> ();
        Set<String> accountIds = new Set<String> ();
        User u = [SELECT Id, Department FROM User WHERE Id = :UserInfo.getUserId()];

        for (Order o : newOrders) {
            if (oldOrderMap != null && oldOrderMap.containsKey(o.Id) && oldOrderMap.get(o.Id).StatusCode != o.StatusCode
                    && o.StatusCode == ConstUtils.ORDER_ACTIVATED_STATUS_CODE && String.isNotBlank(o.AccountId) && !o.IsReductionOrder) {
                orderIds.add(o.Id);
                accountIds.add(o.AccountId);
            }
        }

        if (orderIds.size() > 0 && accountIds.size() > 0) {
            List<OrderItem> orderItems = [
                    SELECT Id, Design__c, OrderId, Order.AccountId, Influence_User__c FROM OrderItem
                    WHERE OrderId in :orderIds AND Design__c != null AND Product2.Family = :ConstUtils.PRODUCT_DESIGN_FAMILY ORDER BY Quantity ASC
            ];
            Map<Id, Account> accounts = new Map<Id, Account> ([SELECT Id, Dealer_Lead_User__c, Type, (SELECT Id, OwnerId, Dealer_Alert_Design__c FROM Accounts_Dealer_Alert__r) FROM Account WHERE Id IN :accountIds ORDER BY CreatedDate ASC]);
            Map<Id, List<Task>> tasks = new Map<Id, List<Task>> {};
            for (Id id : accounts.keySet()) {
                tasks.put(id, new List<Task>{});
            }
            for (Task t : [SELECT Id, AccountId, CreatedById, WhatId FROM Task WHERE Type = 'COK to Dealer' AND CreatedDate = LAST_N_DAYS:90 AND What.Type = 'Design__c' AND AccountId IN :accountIds ORDER BY CreatedDate ASC]) {
                tasks.get(t.AccountId).add(t);
            }
            Map<Id, Account> invAccountsToUpdate = new Map<Id, Account> ();
            Map<Id, Account> dealerAccountsToUpdate = new Map<Id, Account> ();
            Map<Id, Task> tasksToUpdate = new Map<Id, Task> ();
            Map<Id, OrderItem> orderItemsToUpdate = new Map<Id, OrderItem> ();
            for (OrderItem oi : orderItems) {
                // for each order items try to influence in following order
                if (accounts.containsKey(oi.Order.AccountId)) {
                    Account acc = accounts.get(oi.Order.AccountId);
                    // 1) if dealer account has alert on investor related to it,
                    for (Account invAccount : acc.Accounts_Dealer_Alert__r) {
                        // and it reffers to product design
                        if (oi.Design__c == invAccount.Dealer_Alert_Design__c) {
                            // influence investor's account owner
                            oi.Influence_User__c = invAccount.OwnerId;
                            oi.Influence_User_Source__c = 'Investor Order';
                            oi.Influence_User_Account__c = invAccount.Id;
                            orderItemsToUpdate.put(oi.Id, oi);
                            // and clear this alert on investor account
                            invAccount.Dealer_Alert_Design__c = null;
                            invAccount.Account_Dealer_Alert__c = null;
                            invAccount.Dealer_Alert_Text__c = null;
                            invAccountsToUpdate.put(invAccount.Id, invAccount);
                            // don't look for any other alerts, if any left,
                            // it will process next time or same product design is present on another order item during this transcation
                            break;
                        }
                    }
                    // 2) else if dealer is forwarded as lead for B2B,
                    if (String.isNotBlank(acc.Dealer_Lead_User__c) && String.isBlank(oi.Influence_User__c)) {
                        // influence former investor account owner (lead user)
                        oi.Influence_User__c = acc.Dealer_Lead_User__c;
                        oi.Influence_User_Source__c = 'Dealer Lead User';
                        oi.Influence_User_Account__c = acc.Id;
                        orderItemsToUpdate.put(oi.Id, oi);
                        // and remeber this account to clear this value after all order items is processed
                        // (every order item should be influenced by this lead user if no dealer alert is found)
                        dealerAccountsToUpdate.put(acc.Id, acc);
                    }
                    // 3) else if there is COK to Dealer Note,
                    for (Task t : tasks.get(acc.Id)) {
                        // try to find in the order from the oldest one in last 90 days period from now
                        // and the note Task is related to product design
                        if (oi.Design__c == t.WhatId && String.isBlank(oi.Influence_User__c)) {
                            // influence creator of the note Task
                            oi.Influence_User__c = t.CreatedById;
                            oi.Influence_User_Source__c = 'COK Note';
                            oi.Influence_User_Account__c = acc.Id;
                            orderItemsToUpdate.put(oi.Id, oi);
                            // change relation of the Task to the Order to avoid next influences and remember where was it used
                            t.WhatId = oi.OrderId;
                            tasksToUpdate.put(t.Id, t);
                            // don't look for any other note Tasks, if any left,
                            // it will process next time or same product design is present on another order item during this transcation
                            break;
                        }
                    }
                    // 4) else if Dealer Order is activated by COK User,
                    if (acc.Type=='Dealer' && String.isBlank(oi.Influence_User__c) && u.Department=='COK') {
                        // influence activation user
                        oi.Influence_User__c = UserInfo.getUserId();
                        oi.Influence_User_Source__c = 'COK Order';
                        oi.Influence_User_Account__c = acc.Id;
                        orderItemsToUpdate.put(oi.Id, oi);
                    }
                }
            }
            // after all influences, clear dealer lead user if used on any order item
            for (Id dealerAccountId : dealerAccountsToUpdate.keySet()) {
                Account dealerAccount = dealerAccountsToUpdate.get(dealerAccountId);
                dealerAccount.Dealer_Lead_User__c = null;
                dealerAccount.Dealer_Lead_User_Date__c = null;
            }
            update orderItemsToUpdate.values();
            update invAccountsToUpdate.values();
            update dealerAccountsToUpdate.values();
            update tasksToUpdate.values();
        }
    }
    
    private void revertDealerLeadUsers(List<Order> newOrders, Map<Id, Order> oldOrderMap) {
        Set<String> orderIds = new Set<String> ();

        for (Order o : newOrders) {
            if (oldOrderMap != null && oldOrderMap.containsKey(o.Id) && oldOrderMap.get(o.Id).StatusCode != o.StatusCode
                    && o.StatusCode == ConstUtils.ORDER_ACTIVATED_STATUS_CODE && String.isNotBlank(o.AccountId) && o.IsReductionOrder) {
                orderIds.add(o.Id);
            }
        }

        if (orderIds.size() > 0) {
            List<OrderItem> orderItems = [
                    SELECT Id, OriginalOrderItem.OrderId FROM OrderItem
                    WHERE OrderId in :orderIds AND OriginalOrderItem.Influence_User_Source__c = 'Dealer Lead User' AND OriginalOrderItem.Influence_User__c != ''
            ];
            Set<String> originalOrderIds = new Set<String> {};
            for (OrderItem oi : orderItems) {
                originalOrderIds.add(oi.OriginalOrderItem.OrderId);
            }
            List<Order> originalOrders = [
                    SELECT Id, AccountId, (SELECT Id, AvailableQuantity, Influence_User__c FROM OrderItems WHERE Influence_User_Source__c = 'Dealer Lead User' AND Influence_User__c != '') FROM Order
                    WHERE Id in :originalOrderIds AND AccountId != ''
            ];
            System.debug(originalOrders);
            Map<Id, Id> accountIdsToRevertDealerLeadUser = new Map<Id, Id> {};
            for (Order o : originalOrders) {
                System.debug(o);
                if (o.OrderItems.size()>0) {
                    Double sum = 0.0;
                    Id influenceUserId = null;
                    for (OrderItem oi : o.OrderItems) {
                        sum += oi.AvailableQuantity;
                        influenceUserId = oi.Influence_User__c;
                    }
                    if (sum == 0.0) {
                        accountIdsToRevertDealerLeadUser.put(o.AccountId, influenceUserId);
                    }
                }
            }
            List<Account> accountsToRevertDealerLeadUser = new List<Account>([SELECT Id, Dealer_Lead_User__c, Dealer_Lead_User_Date__c FROM Account WHERE Id IN :accountIdsToRevertDealerLeadUser.keySet() AND Type = 'Dealer']);
            for (Account a : accountsToRevertDealerLeadUser) {
                a.Dealer_Lead_User__c = accountIdsToRevertDealerLeadUser.get(a.Id);
                a.Dealer_Lead_User_Date__c = System.now();
            }
            update accountsToRevertDealerLeadUser;
        }
    }
    
    private void revertCanceledOrdersInfluenceUsers(List<Order> newOrders, Map<Id, Order> oldOrderMap) {
        Set<String> orderIds = new Set<String> ();

        for (Order o : newOrders) {
            if (oldOrderMap != null && oldOrderMap.containsKey(o.Id) && oldOrderMap.get(o.Id).Status != o.Status
                    && o.Status == ConstUtils.ORDER_CANCELED_STATUS && String.isNotBlank(o.AccountId) && !o.IsReductionOrder) {
                orderIds.add(o.Id);
            }
        }

        if (orderIds.size() > 0) {
            List<OrderItem> orderItems = [
                    SELECT Id, Design__c, OrderId, Order.AccountId, Order.Account.Type, Influence_User__c, Influence_User_Account__c, Influence_User_Source__c FROM OrderItem
                    WHERE OrderId in :orderIds AND Design__c != null AND Product2.Family = :ConstUtils.PRODUCT_DESIGN_FAMILY
                    AND Influence_User_Account__c != '' AND Influence_User_Source__c IN ('Investor Order', 'Dealer Lead User') AND Influence_User__c != ''
            ];
            Set<Id> accountIds = new Set<Id>{};
            for (OrderItem oi : orderItems) {
                accountIds.add(oi.Influence_User_Account__c);
            }
            Map<Id, Account> accounts = new Map<Id, Account> ([SELECT Id, Dealer_Lead_User__c, Dealer_Lead_User_Date__c, Dealer_Alert_Design__c, Account_Dealer_Alert__c FROM Account WHERE Id IN :accountIds]);
            Map<Id, Account> accountsToUpdate = new Map<Id, Account> ();
            for (OrderItem oi : orderItems) {
                Account a = accounts.get(oi.Influence_User_Account__c);
                if (oi.Influence_User_Source__c == 'Investor Order' && oi.Order.Account.Type == 'Dealer') {
                    a.Dealer_Alert_Design__c = oi.Design__c;
                    a.Account_Dealer_Alert__c = oi.Order.AccountId;
                    accountsToUpdate.put(a.Id, a);
                } else if (oi.Influence_User_Source__c == 'Dealer Lead User' && oi.Order.Account.Type == 'Dealer') {
                    a.Dealer_Lead_User__c = oi.Influence_User__c;
                    a.Dealer_Lead_User_Date__c = System.now();
                    accountsToUpdate.put(a.Id, a);
                }
            }
            update accountsToUpdate.values();
        }
    }

    private void recalculateAccountItems(List<Order> newOrders, Map<Id, Order> oldOrderMap) {
        Set<String> accountIds = new Set<String>();

        for (Order o : newOrders) {
            if (oldOrderMap != null && oldOrderMap.containsKey(o.Id) && oldOrderMap.get(o.Id).StatusCode != o.StatusCode
                    && (o.StatusCode == ConstUtils.ORDER_ACTIVATED_STATUS_CODE
                        || oldOrderMap.get(o.Id).StatusCode == ConstUtils.ORDER_ACTIVATED_STATUS_CODE)
                    && String.isNotBlank(o.AccountId)) {
                accountIds.add(o.AccountId);
                accountIds.add(oldOrderMap.get(o.Id).AccountId);
            }
        }
        if (accountIds.size() > 0) {
            OrderUtils.calculateAccountOrderItems(accountIds);
        }
    }

    private static Contact prepareContact(String accountId, String phone, String email) {
        return new Contact(
                AccountId = accountId, PhoneId__c = phone, EmailId__c = email, Phone = phone, Email = email,
                LastName = String.isNotBlank(phone) ? phone : email
        );
    }

    private void attachContacts(List<Order> newOrders) {
        Set<String> emails = Utils.fetchSet(newOrders, 'BillingEmail__c');
        Set<String> phones = Utils.fetchSet(newOrders, 'ShippingPhone__c');
        phones.addAll(Utils.fetchSet(newOrders, 'BillingPhone__c'));

        Map<String, Contact> investorEmailContacts = new Map<String, Contact> ();
        Map<String, Contact> otherEmailContacts = new Map<String, Contact> ();
        Map<String, Contact> investorPhoneContacts = new Map<String, Contact> ();
        Map<String, Contact> otherPhoneContacts = new Map<String, Contact> ();
        Map<String, Contact> newContactsByPhone = new Map<String, Contact> ();
        Map<String, Contact> newContactsByEmail = new Map<String, Contact> ();
        if (!emails.isEmpty() || !phones.isEmpty()) {
            List<Contact> contacts = [
                    SELECT Id, PhoneId__c, EmailId__c, Email, Phone, Account_Type__c FROM Contact
                    WHERE EmailId__c in :emails OR PhoneId__c in :phones
            ];


            for (Contact c : contacts) {
                if (String.isNotBlank(c.EmailId__c) && c.Account_Type__c == ConstUtils.CONTACT_INVESTOR_TYPE) {
                    investorEmailContacts.put(c.EmailId__c.toLowerCase(), c);
                } else if (String.isNotBlank(c.EmailId__c)) {
                    otherEmailContacts.put(c.EmailId__c.toLowerCase(), c);
                }

                if (String.isNotBlank(c.PhoneId__c) && c.Account_Type__c == ConstUtils.CONTACT_INVESTOR_TYPE) {
                    investorPhoneContacts.put(c.PhoneId__c.toLowerCase(), c);
                } else if (String.isNotBlank(c.PhoneId__c)) {
                    otherPhoneContacts.put(c.PhoneId__c.toLowerCase(), c);
                }
            }
        }
                
        List<Contact> contactsToUpsert = new List<Contact> ();
                
        List<Id> accountIds = new List<Id>();      
        for (Order o : newOrders) {
            accountIds.add(o.AccountId);
        }
        Map<Id,Account> accountMap = new Map<Id,Account>([SELECT Id, Type FROM Account WHERE Id IN :accountIds]);
        
        Map<Id,String> orderTypeMap = new Map<Id,String>();
        for (Order o : newOrders) {
            orderTypeMap.put(o.Id, accountMap.get(o.AccountId).Type);
        }
        
        for (Order o : newOrders) {
            if (String.isNotBlank(o.BillingEmail__c) && investorEmailContacts.containsKey(o.BillingEmail__c.toLowerCase())) {
                o.BillToContactEmailId__c = investorEmailContacts.get(o.BillingEmail__c.toLowerCase()).Id;
            } else if (String.isNotBlank(o.BillingEmail__c) && otherEmailContacts.containsKey(o.BillingEmail__c.toLowerCase())) {
                o.BillToContactEmailId__c = otherEmailContacts.get(o.BillingEmail__c.toLowerCase()).Id;
            } else if (String.isNotBlank(o.BillingEmail__c)) {
                o.BillToContactEmailId__c = null;
                Contact contact = null;
                String email = o.BillingEmail__c.toLowerCase();
                String phone = String.isNotBlank(o.BillingPhone__c) ? o.BillingPhone__c.toLowerCase() : null;
                if (!newContactsByEmail.containsKey(email) && String.isNotBlank(phone) && investorPhoneContacts.containsKey(phone)
                        && String.isBlank(investorPhoneContacts.get(phone).EmailId__c)) {
                    contact = investorPhoneContacts.get(phone);
                } else if (!newContactsByEmail.containsKey(email) && String.isNotBlank(phone) && otherPhoneContacts.containsKey(phone)
                        && String.isBlank(otherPhoneContacts.get(phone).EmailId__c)) {
                    contact = otherPhoneContacts.get(phone);
                } else if (!newContactsByEmail.containsKey(email) && (String.isBlank(orderTypeMap.get(o.Id)) || orderTypeMap.get(o.Id) != 'Dealer')) {
                    contact = prepareContact(
                            o.AccountId,
                            String.isNotBlank(phone) && !investorPhoneContacts.containsKey(phone) && !otherPhoneContacts.containsKey(phone) ? phone : null,
                            email
                    );
                }
                if (contact != null) {
                    contact.Email = email;
                    contact.EmailId__c = email;
                    contactsToUpsert.add(contact);
                    newContactsByEmail.put(email, contact);
                    if (String.isNotBlank(contact.PhoneId__c)) {
                        newContactsByPhone.put(contact.PhoneId__c.toLowerCase(), contact);
                    }
                }
            } else {
                o.BillToContactEmailId__c = null;
            }

            if (String.isNotBlank(o.BillingPhone__c) && investorPhoneContacts.containsKey(o.BillingPhone__c.toLowerCase())) {
                o.BillToContactId = investorPhoneContacts.get(o.BillingPhone__c.toLowerCase()).Id;
            } else if (String.isNotBlank(o.BillingPhone__c) && otherPhoneContacts.containsKey(o.BillingPhone__c.toLowerCase())) {
                o.BillToContactId = otherPhoneContacts.get(o.BillingPhone__c.toLowerCase()).Id;
            } else if (String.isNotBlank(o.BillingPhone__c)) {
                o.BillToContactId = null;
                Contact contact = null;
                String email = String.isNotBlank(o.BillingEmail__c) ? o.BillingEmail__c.toLowerCase() : null;
                String phone = o.BillingPhone__c.toLowerCase();
                if (!newContactsByPhone.containsKey(phone) && String.isNotBlank(email) && investorEmailContacts.containsKey(email)
                        && String.isBlank(investorEmailContacts.get(email).PhoneId__c)) {
                    contact = investorEmailContacts.get(email);
                } else if (!newContactsByPhone.containsKey(phone) && String.isNotBlank(email) && otherEmailContacts.containsKey(email)
                        && String.isBlank(otherEmailContacts.get(email).PhoneId__c)) {
                    contact = otherEmailContacts.get(email);
                } else if (!newContactsByPhone.containsKey(phone) && (String.isBlank(orderTypeMap.get(o.Id)) || orderTypeMap.get(o.Id) != 'Dealer')) {
                    contact = prepareContact(
                            o.AccountId,
                            phone,
                            String.isNotBlank(email) && !investorEmailContacts.containsKey(email) && !otherEmailContacts.containsKey(email) ? email : null
                    );
                }
                if (contact != null) {
                    contact.Phone = phone;
                    contact.PhoneId__c = phone;
                    contactsToUpsert.add(contact);
                    newContactsByPhone.put(phone, contact);
                    if (String.isNotBlank(contact.EmailId__c)) {
                        newContactsByEmail.put(contact.EmailId__c.toLowerCase(), contact);
                    }
                }
            } else {
                o.BillToContactId = null;
            }

            if (String.isNotBlank(o.ShippingPhone__c) && investorPhoneContacts.containsKey(o.ShippingPhone__c.toLowerCase())) {
                o.ShipToContactId = investorPhoneContacts.get(o.ShippingPhone__c.toLowerCase()).Id;
            } else if (String.isNotBlank(o.ShippingPhone__c) && otherPhoneContacts.containsKey(o.ShippingPhone__c.toLowerCase())) {
                o.ShipToContactId = otherPhoneContacts.get(o.ShippingPhone__c.toLowerCase()).Id;
            } else if (String.isNotBlank(o.ShippingPhone__c) && !newContactsByPhone.containsKey(o.ShippingPhone__c.toLowerCase()) && (String.isBlank(orderTypeMap.get(o.Id)) || orderTypeMap.get(o.Id) != 'Dealer')) {
                o.ShipToContactId = null;
                String phone = o.ShippingPhone__c.toLowerCase();
                Contact contact = prepareContact(o.AccountId, phone, null);
                contactsToUpsert.add(contact);
                newContactsByPhone.put(phone, contact);
            } else {
                o.ShipToContactId = null;
            }
        }
        if (contactsToUpsert.size() > 0) {
            system.debug(contactsToUpsert);
            upsert contactsToUpsert;
            for (Order o : newOrders) {
                if (o.BillToContactEmailId__c == null && String.isNotBlank(o.BillingEmail__c)
                        && newContactsByEmail.containsKey(o.BillingEmail__c.toLowerCase())) {
                    o.BillToContactEmailId__c = newContactsByEmail.get(o.BillingEmail__c.toLowerCase()).Id;
                }
                if (o.BillToContactId == null && String.isNotBlank(o.BillingPhone__c)
                        && newContactsByPhone.containsKey(o.BillingPhone__c.toLowerCase())) {
                    o.BillToContactId = newContactsByPhone.get(o.BillingPhone__c.toLowerCase()).Id;
                }
                if (o.ShipToContactId == null && String.isNotBlank(o.ShippingPhone__c)
                        && newContactsByPhone.containsKey(o.ShippingPhone__c.toLowerCase())) {
                    o.ShipToContactId = newContactsByPhone.get(o.ShippingPhone__c.toLowerCase()).Id;
                }
            }
        }
    }
}