/**
 * Created by grzegorz.dlugosz on 10.05.2019.
 */
global class DesignOrderSentMonthBatch implements Database.Batchable<AggregateResult> , Schedulable {
    global Iterable<AggregateResult> start(Database.BatchableContext BC) {
        DateTime fromDate = System.today().addYears(-2);
        String dateString = fromDate.format('yyyy-MM-dd');

        String query = 'SELECT SUM(Quantity), Product2.Design__c ';
        query += 'FROM OrderItem ';
        query += 'WHERE Product2.Design__c != null AND Order.StatusCode = \'' + ConstUtils.ORDER_STATUS_CODE_ACTIVATED + '\' AND Order.PoDate >= ' + dateString;
        query += ' AND Product2.Family = \'' + ConstUtils.PRODUCT_DESIGN_FAMILY + '\'';
        query += ' GROUP BY Product2.Design__c ';

        return new AggregateResultIterable(query);
    }

    public void execute(Database.BatchableContext BC, List<sObject> scope) {
        System.debug(LoggingLevel.ERROR, 'inside execute');
        // prepare list of Designs to update
        List<Design__c> DesignListToUpdate = new List<Design__c>();

        for (sObject aggResult : scope) {
            // The query returns sum of quantity from last 2 years, so divide that by 24 to get average amount per month.
            // It doesn't matter if record was created recently. This is only used as support field in formula which takes care of that.
            Decimal newQuantitySentPerMonth;
            if (aggResult.get('expr0') != null && (Decimal)aggResult.get('expr0') != 0) {
                newQuantitySentPerMonth = ((Decimal)aggResult.get('expr0') / 24);
            } else {
                newQuantitySentPerMonth = 0;
            }
            Design__c design = new Design__c(Id = (String)aggResult.get('Design__c'), Sent_Month__c = newQuantitySentPerMonth);
            DesignListToUpdate.add(design);
        }

        DesignTriggerHandler.BYPASS_INVALIDATE_DESIGNS = true;
        update DesignListToUpdate;
        DesignTriggerHandler.BYPASS_INVALIDATE_DESIGNS = false;
    }

    public void finish(Database.BatchableContext BC) {
        if (!Test.isRunningTest()) {
            DesignContractBoostClearBatch btch = new DesignContractBoostClearBatch();
            Database.executeBatch(btch, 100);
        }
    }

    global void execute(SchedulableContext sc) {
        DesignOrderSentMonthBatch btch = new DesignOrderSentMonthBatch();
        // standard chunk is 200
        Database.executeBatch(btch);
    }
}