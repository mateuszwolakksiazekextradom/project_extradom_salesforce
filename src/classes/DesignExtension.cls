public with sharing class DesignExtension {

    private final Design__c d;
    public DesignExtension(ApexPages.StandardController stdController) {
        this.d = (Design__c)stdController.getRecord();
    }
    
    private ApexPages.StandardSetController stdSetController;
    public DesignExtension(ApexPages.StandardSetController stdSetController)
    {
        this.stdSetController = stdSetController;
    }

    public String getFreeCode() {
        List<Design__c> d1 = [SELECT Supplier_Code__c FROM Design__c WHERE Id=:d.id];
        AggregateResult[] groupedResults = [SELECT MAX(Code_Number_Only__c)maxnumber FROM Design__c WHERE Code_Prefix_Only__c=:d1.get(0).Supplier_Code__c];
        if(groupedResults.size()>0) {
            if (groupedResults[0].get('maxnumber')==null) {
                return String.ValueOf(d1.get(0).Supplier_Code__c)+'1000';
            } else {
                return String.ValueOf(d1.get(0).Supplier_Code__c)+(Integer.ValueOf(groupedResults[0].get('maxnumber'))+1);
            }
        } else {
            return String.ValueOf(d1.get(0).Supplier_Code__c)+'1000';
        }
    }
    
    public PageReference autoSyncDesyncedIncomingValues() {
        if (!d.Footprint_syn__c) {
            d.Footprint__c = d.Footprint_inc__c;
        }
        if (!d.Height_syn__c) {
            d.Height__c = d.Height_inc__c;
        }
        if (!d.Roof_slope_syn__c) {
            d.Roof_slope__c = d.Roof_slope_inc__c;
        }
        if (!d.Construction_Pricing_Estimate_syn__c) {
            d.Construction_Pricing_Estimate__c = d.Construction_Pricing_Estimate_inc__c;
        }
        if (!d.Roof_Cover_Area_syn__c) {
            d.Roof_Cover_Area__c = d.Roof_Cover_Area_inc__c;
        }
        if (!d.Usable_Area_syn__c) {
            d.Usable_Area__c = d.Usable_Area_inc__c;
        }
        if (!d.Capacity_syn__c) {
            d.Capacity__c = d.Capacity_inc__c;
        }
        if (!d.Minimum_Plot_Size_Horizontal_syn__c) {
            d.Minimum_Plot_Size_Horizontal__c = d.Minimum_Plot_Size_Horizontal_inc__c;
        }
        if (!d.Minimum_Plot_Size_Vertical_syn__c) {
            d.Minimum_Plot_Size_Vertical__c = d.Minimum_Plot_Size_Vertical_inc__c;
        }
        if (!d.EUco_syn__c) {
            d.EUco__c = d.EUco_inc__c;
        }
        if (!d.Description_syn__c) {
            d.Description__c = d.Description_inc__c;
            d.Description_dec__c = true;
        }
        if (!d.Design_Availability_syn__c) {
            d.Design_Availability__c = d.Design_Availability_inc__c;
            d.Design_Availability_dec__c = true;
        }
        if (!d.Technology_syn__c) {
            d.Technology__c = d.Technology_inc__c;
            d.Technology_dec__c = true;
        }
        if (!d.Ceiling_syn__c && String.isNotBlank(d.Ceiling_inc__c)) {
            Schema.DescribeFieldResult fieldResult = Design__c.Ceiling__c.getDescribe();
            List<Schema.PicklistEntry> ceilingPicklistEntries = fieldResult.getPicklistValues();
            for(Schema.PicklistEntry entry : ceilingPicklistEntries) {
                if (d.Ceiling_inc__c.toLowerCase().contains(entry.getValue().toLowerCase())) {
                    d.Ceiling__c = entry.getValue();
                    d.Ceiling_dec__c = true;
                    break;
                }
            }
        }
        PageReference pageRef = new ApexPages.StandardController(d).save();
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    public PageReference processFeaturedDesigns() {
        ContactProfiler.processDesign(d.Id);
        PageReference pageRef = new ApexPages.StandardController(d).cancel();
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    public PageReference massProcessFeaturedDesigns()
    {
        List<Design__c> selectedDesigns = (List<Design__c>) stdSetController.getSelected();
        for (Design__c d: selectedDesigns) {
            ContactProfiler.processDesign(d.Id);
        }
        return stdSetController.cancel();
    }
    
    public PageReference massProcessFeaturedDesignsBatched()
    {
        List<Design__c> selectedDesigns = (List<Design__c>) stdSetController.getSelected();
        List<ID> selectedDesignIds = new List<ID>{};
        for (Design__c d: selectedDesigns) {
            selectedDesignIds.add(d.Id);
            if (selectedDesignIds.size()==10) {
                ContactProfiler.processDesigns(selectedDesignIds);
                selectedDesignIds = new List<ID>();
            }
        }
        if (!selectedDesignIds.isEmpty()) {
            ContactProfiler.processDesigns(selectedDesignIds);
        }
        return stdSetController.cancel();
    }
    
    public PageReference massSyncPrices()
    {
        List<Design__c> selectedDesigns = (List<Design__c>) stdSetController.getSelected();
        for (Design__c d: selectedDesigns) {
            d.Gross_Price__c = d.Gross_Price_inc__c;
            d.Update_Date__c = System.now();
        }
        return stdSetController.save();
    }
        
    @future (callout=true)
    public static void invalidateDesigns(List<ID> ids) {
    
        if (ids == null) ids = new List<ID>();
        
        String items = '';
        List<Design__c> designsToInvalidate = [SELECT Canonical_URL__c FROM Design__c WHERE Id in :ids AND Canonical_URL__c != ''];
        for (Design__c d : designsToInvalidate) {
            items += '<Path>'+d.Canonical_URL__c+'</Path>';
        }
    
        AWSConnector aws = new AWSConnector('AKIAJO25LHDRMTH7SLBA','zjzdMrMfTi3i26wIqHEKAwvZkjFBN0qJmGE4sH8n');
        aws.region = 'us-east-1';
        aws.service = 'cloudfront';
        Map<String,String> headers = new Map<String,String>{};
        String body = '<InvalidationBatch xmlns="http://cloudfront.amazonaws.com/doc/2016-01-13/"><Paths><Quantity>'+designsToInvalidate.size()+'</Quantity><Items>'+items+'</Items></Paths><CallerReference>'+String.valueof(DateTime.now().getTime())+'</CallerReference></InvalidationBatch>';
        System.debug(body);
        HttpRequest req = aws.signedRequest('POST', new Url('https://cloudfront.amazonaws.com/2016-01-13/distribution/E8A8VXKR03ELG/invalidation'), headers, Blob.valueOf(body), 'text/xml');
        Http http = new Http();
        HttpResponse res = http.send(req);
        
    }

    public PageReference save() {
        PageReference pageRef = new ApexPages.StandardController(d).save();
        if (String.isNotBlank(d.Code__c)) {
            createProductsAndPricebooks(d);
        }
        return pageRef;
    }

    private static void createProductsAndPricebooks(Design__c design) {
        design = [SELECT Id, Name, Code__c, Gross_Price__c, Promo_Price__c, Status__c FROM Design__c WHERE Id = :design.Id];
        Product2 product = createProduct(design);
        createPricebookEntry(design, product);
    }

    private static Product2 createProduct(Design__c design) {
        List<Product2> products = [
                SELECT Id, Name, ProductCode, Design__c, Family, Max_Discount__c, Version_Required__c
                FROM Product2 WHERE Product2.Design__c = :design.Id AND Product2.Family = :ConstUtils.PRODUCT_DESIGN_FAMILY
        ];
        Product2 product = null;
        if (products.size() == 0) {
            product = new Product2(
                    Name = design.Name, ProductCode = design.Code__c, Design__c = design.Id, IsActive = true,
                    Family = ConstUtils.PRODUCT_DESIGN_FAMILY, Max_Discount__c = 15, Version_Required__c = true
            );
            insert product;
        } else {
            product = products.get(0);
        }
        return product;
    }

    private static void createPricebookEntry(Design__c design, Product2 product) {
        List<PricebookEntry> pes = [
                SELECT Id, IsActive, UnitPrice, Regular_Price__c, Product2.Design__c, Pricebook2.IsStandard
                FROM PricebookEntry
                WHERE Product2.Id = :product.Id AND Pricebook2.IsStandard = true
        ];

        PricebookEntry pe = null;
        if (pes.size() == 0) {
            String pricebookId = OrderUtils.getPricebookId(null);
            pe = new PricebookEntry();
            pe.Pricebook2Id = pricebookId;
        } else {
            pe = pes.get(0);
        }
        pe.Regular_Price__c = design.Gross_Price__c != null ? design.Gross_Price__c : 0;
        pe.UnitPrice = design.Promo_Price__c != null ? design.Promo_Price__c : pe.Regular_Price__c;
        pe.IsActive = pe.UnitPrice > 0 &&
                (design.Status__c == ConstUtils.DESIGN_ACTIVE_STATUS || design.Status__c == ConstUtils.DESIGN_VERSION_STATUS);
        pe.Product2Id = product.Id;
        upsert pe;
    }

}