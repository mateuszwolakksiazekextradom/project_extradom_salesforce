@isTest
private class emailTestClass {

    static testMethod void myUnitTest() {
        
        // Create a new email, envelope object and Attachment
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        Messaging.InboundEmail.BinaryAttachment inAtt = new Messaging.InboundEmail.BinaryAttachment();
    
        email.subject = 'test';
        email.fromname = 'User';
        email.fromAddress = 'user@acme.com';
        email.toAddresses = new List<String>{'kontakt@extradom.pl'};
        email.plainTextBody = 'test body';
        env.fromAddress = 'user@acme.com';
    
        // set the body of the attachment
        inAtt.body = blob.valueOf('test file');
        inAtt.fileName = 'my attachment name';
        inAtt.mimeTypeSubType = 'plain/txt';
    
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] {inAtt }; 
    
        // call the class and test it with the data in the testMethod
        ProcessContactEmail emailServiceObj = new ProcessContactEmail();
        emailServiceObj.handleInboundEmail(email, env);
        
        Contact c = [SELECT Id FROM Contact WHERE EmailId__c = 'user@acme.com'];
        Task t = [SELECT Id, Description FROM Task WHERE WhoId = :c.Id AND Type = 'E-mail' LIMIT 1];
        System.assertEquals('test body', t.Description);
        
    }
    
    static testMethod void myUnitTest2() {
        
        // Create a new email, envelope object and Attachment
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
    
        email.subject = 'Lost Queue Call from 604651831 on Queue 60 Extradom.pl - ID 105';
        email.plainTextBody = 'Lost Call in Queue Extradom.pl (60) from Caller ID \'604651831\'\\nQueue: 63 [Extradom.pl]\\ncaller: 604651831';
        email.fromname = 'User';
        email.fromAddress = 'user@acme.com';
        email.toAddresses = new List<String>{'3cx@extradom.pl'};
    
        // call the class and test it with the data in the testMethod
        Inbound3CXNotificationProcess emailServiceObj = new Inbound3CXNotificationProcess();
        emailServiceObj.handleInboundEmail(email, env);
        Account a = new Account(Name='Test', AccountSource = 'Inbound Call', OwnerId = '005b00000065DH0');
        insert a;
        Contact c = new Contact(Phone = '721804188', PhoneId__c = '721804188',LastName='721804188', AccountId = a.Id);
        insert c;
        emailServiceObj.handleInboundEmail(email, env); // to check if already processed contact is found
        
    }
    
    static testMethod void myUnitTest3() {
        
        // Create a new email, envelope object and Attachment
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
    
        email.subject = 'New missed call from: 604651831 at: 1/26/2015 10:14:58 AM';
        email.plainTextBody = 'You have a new missed call from 604651831 at: 1/26/2015 10:14:58 AM. Ringing time: 00:44 to extension:63 Iwona Lewandowska\nTo: "063"';
        email.fromname = 'User';
        email.fromAddress = 'user@acme.com';
        email.toAddresses = new List<String>{'3cx@extradom.pl'};
        
        Account a = new Account(Name='Test', AccountSource = 'Inbound Call', OwnerId = '005b00000065DH0', Queue__c = 'Inbound Calls');
        insert a;
    
        // call the class and test it with the data in the testMethod
        Inbound3CXNotificationProcess emailServiceObj = new Inbound3CXNotificationProcess();
        emailServiceObj.handleInboundEmail(email, env);
        emailServiceObj.handleInboundEmail(email, env); // to check if already processed contact is found
        
    }
    
    static testMethod void myUnitTest4() {
        
        // Create a new email, envelope object and Attachment
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
    
        email.subject = 'New missed call from: 604651831 at: 1/26/2015 10:14:58 AM';
        email.plainTextBody = 'You have a new missed call from 604651831 at: 1/26/2015 10:14:58 AM. Ringing time: 00:44 to extension:63 Iwona Lewandowska\nTo: "905"';
        email.fromname = 'User';
        email.fromAddress = 'user@acme.com';
        email.toAddresses = new List<String>{'3cx@extradom.pl'};
    
        // call the class and test it with the data in the testMethod
        Inbound3CXNotificationProcess emailServiceObj = new Inbound3CXNotificationProcess();
        emailServiceObj.handleInboundEmail(email, env);
        emailServiceObj.handleInboundEmail(email, env); // to check if already processed contact is found
        
    }
    
    static testMethod void myUnitTest5() {           
        QueueTools.getAccountNextUser('Inbound Calls Dealer');
        QueueTools.getAccountNextUser('Inbound Calls');              
    }
    
    static testMethod void myUnitTest6() {
        
        // Create a new email, envelope object and Attachment
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
    
        email.subject = 'New missed call from: 604651831 at: 1/26/2015 10:14:58 AM';
        email.plainTextBody = 'You have a new missed call from 604651831 at: 1/26/2015 10:14:58 AM. Ringing time: 00:44 to extension:63 Iwona Lewandowska\nTo: "176"';
        email.fromname = 'User';
        email.fromAddress = 'user@acme.com';
        email.toAddresses = new List<String>{'3cx@extradom.pl'};
    
        // call the class and test it with the data in the testMethod
        Inbound3CXNotificationProcess emailServiceObj = new Inbound3CXNotificationProcess();
        emailServiceObj.handleInboundEmail(email, env);
        emailServiceObj.handleInboundEmail(email, env); // to check if already processed contact is found       
    }
    
    static testMethod void myUnitTest7() {
        
        // Create a new email, envelope object and Attachment
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
    
        email.subject = 'New missed call from: 604651831 at: 1/26/2015 10:14:58 AM';
        email.plainTextBody = 'You have a new missed call from 721804188 at: 1/26/2015 10:14:58 AM. Ringing time: 00:44 to extension:63 Iwona Lewandowska\nTo: "063"';
        email.fromname = 'User';
        email.fromAddress = 'user@acme.com';
        email.toAddresses = new List<String>{'3cx@extradom.pl'};
        
        Account a = new Account(Name='Test', AccountSource = 'Inbound Call', OwnerId = '005b00000065DH0', Queue__c = 'Inbound Calls');
        insert a;
    
        // call the class and test it with the data in the testMethod
        Inbound3CXNotificationProcess emailServiceObj = new Inbound3CXNotificationProcess();
        emailServiceObj.handleInboundEmail(email, env);
        emailServiceObj.handleInboundEmail(email, env); // to check if already processed contact is found        
    }
    
    static testMethod void myUnitTest8() {
        
        // Create a new email, envelope object and Attachment
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
    
        email.subject = 'New missed call from: 604651831 at: 1/26/2015 10:14:58 AM';
        email.plainTextBody = 'You have a new missed call from 604651831 at: 1/26/2015 10:14:58 AM. Ringing time: 00:44 to extension:63 Iwona Lewandowska\nTo: "089"';
        email.fromname = 'User';
        email.fromAddress = 'user@acme.com';
        email.toAddresses = new List<String>{'3cx@extradom.pl'};
        
        Account a = new Account(Name='Test', AccountSource = 'Inbound Call', OwnerId = '005b00000065DH0', Queue__c = 'Inbound Calls');
        insert a;
    
        // call the class and test it with the data in the testMethod
        Inbound3CXNotificationProcess emailServiceObj = new Inbound3CXNotificationProcess();
        emailServiceObj.handleInboundEmail(email, env);
        emailServiceObj.handleInboundEmail(email, env); // to check if already processed contact is found        
    }
}