@IsTest
private class OrderSendEmailActionExtension_Test {
	@IsTest
    private static void shouldSetSendEmailAction() {
        // given
        Account acc = new Account (Name = 'Test Name');
        insert acc;
        Order o = new Order (
            AccountId = acc.Id, EffectiveDate = System.now().date(), Status = 'Draft', Pricebook2Id = Test.getStandardPricebookId(),
            RecordTypeId = ConstUtils.ORDER_REGULAR_RT_ID, Send_Email__c = false
        );
        insert o;
        
        // when
        OrderSendEmailActionExtension ext = new OrderSendEmailActionExtension(new ApexPages.StandardController(o));
        ext.setSendEmailAction();
        o = [SELECT Id, Send_Email__c FROM Order WHERE Id = :o.Id];
        
        // then
        System.assertEquals(false, o.Send_Email__c);
    }
}