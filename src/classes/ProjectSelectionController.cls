public with sharing class ProjectSelectionController {
    
    @AuraEnabled
    public static List<Design__c> getDesigns(String designName) {
        List<Design__c> designs = new List<Design__c> ();
        if (String.isNotBlank(designName)) {
            String designCondition = '%' + designName + '%';
            designs = [SELECT Id, Name, Trade_Description__c FROM Design__c WHERE Name like :designCondition LIMIT 25];
        }
        return designs;
    }
}