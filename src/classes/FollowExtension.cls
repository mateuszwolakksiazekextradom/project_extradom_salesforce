global with sharing class FollowExtension {

    public FollowExtension() { }

    public FollowExtension(UserViewController controller) {
    }
    
    public FollowExtension(ContextUserController controller) {
    }

    public FollowExtension(TopicViewController controller) {
    }

    public FollowExtension(ApexPages.StandardController sc) { }

    @RemoteAction
    global static ConnectApi.Subscription follow(ID subjectId) {
        return ConnectApi.ChatterUsers.follow(Network.getNetworkId(), 'me', subjectId);
    }
    
    @RemoteAction
    global static Void unfollow(ID subscriptionId) {
        ConnectApi.Chatter.deleteSubscription(Network.getNetworkId(), subscriptionId);
    }
    
    @RemoteAction
    global static Void unfollowBySubjectId(ID subjectId) {
        EntitySubscription sub = [SELECT Id FROM EntitySubscription WHERE NetworkId = :Network.getNetworkId() AND ParentId = :subjectId AND SubscriberId = :UserInfo.getUserId() LIMIT 1];
        ConnectApi.Chatter.deleteSubscription(Network.getNetworkId(), sub.Id);
    }
    
}