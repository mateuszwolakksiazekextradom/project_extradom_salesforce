@IsTest
private class DesignFeaturedJob_Test {
    public static String CRON_EXP = '0 0 0 15 3 ? 2080';

    @IsTest
    private static void shouldExecuteJob() {
        //given

        //when
        Test.startTest();
        String jobId = System.schedule('DesignFeaturedJob_Test', CRON_EXP, new DesignFeaturedJob());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];

        //then
        system.assertEquals(CRON_EXP, ct.CronExpression);
        system.assertEquals(0, ct.TimesTriggered);
        Test.stopTest();
    }
}