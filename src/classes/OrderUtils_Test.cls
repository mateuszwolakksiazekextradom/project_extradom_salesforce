@IsTest
private class OrderUtils_Test {
    private static final String INSERTED_NAME = 'OrderItem Insert';
    private static final Integer INSERTED_SIZE = 5;
    private static final String DESIGN_CODE = 'ABC1001';
    private static final String ADD_ON_1_CODE = 'Add On Product 1';
    private static final String ADD_ON_2_CODE = 'Add On Product 2';

    @IsTest
    private static void shouldReturnExistingOrderItems() {
        // given
        List<OrderItem> orderItems = [SELECT Id, Description FROM OrderItem];
        final Integer NEW_ORDER_ITEMS_SIZE = 3;
        final STRING NEW_ORDER_ITEMS_NAME = 'OrderItem New';
        for (Integer i = 0; i < NEW_ORDER_ITEMS_SIZE; i++){
            orderItems.add(new OrderItem(Description = NEW_ORDER_ITEMS_NAME + String.valueOf(i)));
        }

        // when
        Map<Id, OrderItem> orderItemsResult = OrderUtils.getExistingOrderItems(orderItems);

        // then
        System.assertEquals(INSERTED_SIZE, orderItemsResult.size());
        for (OrderItem oi : orderItemsResult.values()) {
            System.assert(oi.Description.startsWith(INSERTED_NAME));
        }
    }

    @IsTest
    private static void shouldReturnNewOrderItems() {
        // given
        List<OrderItem> orderItems = [SELECT Id, Description FROM OrderItem];
        final Integer NEW_ORDER_ITEMS_SIZE = 3;
        final STRING NEW_ORDER_ITEMS_NAME = 'OrderItem New';
        for (Integer i = 0; i < NEW_ORDER_ITEMS_SIZE; i++){
            orderItems.add(new OrderItem(Description = NEW_ORDER_ITEMS_NAME + String.valueOf(i)));
        }

        // when
        List<OrderItem> orderItemsResult = OrderUtils.getNewOrderItems(orderItems);

        // then
        System.assertEquals(NEW_ORDER_ITEMS_SIZE, orderItemsResult.size());
        for (OrderItem oi : orderItemsResult) {
            System.assert(oi.Description.startsWith(NEW_ORDER_ITEMS_NAME));
        }
    }

    @IsTest
    private static void shouldReturnDeletedOrderItems() {
        // given
        List<OrderItem> orderItems = [SELECT Id, Description FROM OrderItem];
        Map<Id, OrderItem> orderItemMap = new Map<Id, OrderItem> ();
        Integer i = 0;
        for (OrderItem oi : orderItems) {
            Boolean addOI = Math.mod(i, 2) == 0;
            if (addOI) {
                orderItemMap.put(oi.Id, oi);
            }
        }

        // when
        List<OrderItem> orderItemsResult = OrderUtils.getDeleteOrderItems(orderItems, orderItemMap);

        // then
        System.assertNotEquals(orderItemsResult.size(), orderItems.size());
        for (OrderItem oi : orderItemsResult) {
            System.assert(!orderItemMap.containsKey(oi.Id));
        }
    }

    /*
    @IsTest
    private static void shouldCreateOrderBasedOnOrderWrapper() {
        // given
        ExtradomApi.DesignOrderInput orderInput = new ExtradomApi.DesignOrderInput();
        orderInput.billingAddress = new ExtradomApi.AddressInput();
        orderInput.shippingAddress = new ExtradomApi.AddressInput();
        orderInput.addOns = new List<ExtradomApi.AddOnInput>();
        orderInput.optIns = new List<ExtradomApi.OptInInput>();
        orderInput.code = DESIGN_CODE;
        orderInput.version = 'Basic';
        orderInput.source = 'Extradom.pl';
        orderInput.comments = 'Test Comment';
        orderInput.paymentMethod = 'Online';
        orderInput.shippingMethod = 'Office';
        orderInput.billingAddress.company = 'Test Company';
        orderInput.billingAddress.vatNumber = '3334445';
        orderInput.billingAddress.firstName = 'Test';
        orderInput.billingAddress.lastName = 'Name';
        orderInput.billingAddress.streetName = 'Street Name';
        orderInput.billingAddress.streetNumber = '5';
        orderInput.billingAddress.streetFlatNumber = '6';
        orderInput.billingAddress.zip = '00-000';
        orderInput.billingAddress.city = 'Warsaw';
        orderInput.billingAddress.phone = '+48600000111';
        orderInput.billingAddress.email = 'test@test.com';
        orderInput.billingAddress.company = 'Test Company';
        orderInput.billingAddress.vatNumber = '3334445';
        orderInput.shippingAddress.firstName = 'Test 2';
        orderInput.shippingAddress.lastName = 'Name 2';
        orderInput.shippingAddress.streetName = 'Street Name 2';
        orderInput.shippingAddress.streetNumber = '6';
        orderInput.shippingAddress.streetFlatNumber = '7';
        orderInput.shippingAddress.zip = '00-000';
        orderInput.shippingAddress.city = 'Warsaw2';
        orderInput.shippingAddress.phone = '+48600000112';
        ExtradomApi.OptInInput optinInput = new ExtradomApi.OptInInput();
        optinInput.name = ConstUtils.ORDER_OPT_IN_REGULATIONS_PARAM;
        optinInput.value = true;
        orderInput.optIns.add(optinInput);
        ExtradomApi.OptInInput optinInput2 = new ExtradomApi.OptInInput();
        optinInput2.name = ConstUtils.ORDER_OPT_IN_PRIVACY_POLICY_PARAM;
        optinInput2.value = true;
        orderInput.optIns.add(optinInput2);
        ExtradomApi.OptInInput optinInput3 = new ExtradomApi.OptInInput();
        optinInput3.name = ConstUtils.ORDER_OPT_IN_MARKETING_PARAM;
        optinInput3.value = true;
        orderInput.optIns.add(optinInput3);
        ExtradomApi.OptInInput optinInput4 = new ExtradomApi.OptInInput();
        optinInput4.name = ConstUtils.ORDER_OPT_IN_MARKETING_EXTENDED_PARAM;
        optinInput4.value = true;
        orderInput.optIns.add(optinInput4);
        ExtradomApi.OptInInput optinInput5 = new ExtradomApi.OptInInput();
        optinInput5.name = ConstUtils.ORDER_OPT_IN_MARKETING_PARTNER_PARAM;
        optinInput5.value = true;
        orderInput.optIns.add(optinInput5);
        ExtradomApi.OptInInput optinInput6 = new ExtradomApi.OptInInput();
        optinInput6.name = ConstUtils.ORDER_OPT_IN_LEAD_FORWARD_PARAM;
        optinInput6.value = true;
        orderInput.optIns.add(optinInput6);
        ExtradomApi.AddOnInput addOnInput1 = new ExtradomApi.AddOnInput();
        addOnInput1.name = ADD_ON_1_CODE;
        orderInput.addOns.add(addOnInput1);
        ExtradomApi.AddOnInput addOnInput2 = new ExtradomApi.AddOnInput();
        addOnInput2.name = ADD_ON_2_CODE;
        orderInput.addOns.add(addOnInput2);

        // when
        String orderId = OrderUtils.saveOrder(orderInput);
        List<Order> orders = [SELECT Id FROM Order WHERE Id = :orderId];

        // then
        System.assertNotEquals(null, orderId);
        System.assertEquals(1, orders.size());
    }
	*/
    
    /*
    @IsTest
    private static void shouldCreateOrderFromRest() {
        // given
        ExtradomApi.DesignOrderInput orderInput = new ExtradomApi.DesignOrderInput();
        orderInput.billingAddress = new ExtradomApi.AddressInput();
        orderInput.shippingAddress = new ExtradomApi.AddressInput();
        orderInput.addOns = new List<ExtradomApi.AddOnInput>();
        orderInput.optIns = new List<ExtradomApi.OptInInput>();
        orderInput.code = DESIGN_CODE;
        orderInput.version = 'Basic';
        orderInput.source = 'Extradom.pl';
        orderInput.comments = 'Test Comment';
        orderInput.paymentMethod = 'Online';
        orderInput.shippingMethod = 'Office';
        orderInput.billingAddress.company = 'Test Company';
        orderInput.billingAddress.vatNumber = '3334445';
        orderInput.billingAddress.firstName = 'Test';
        orderInput.billingAddress.lastName = 'Name';
        orderInput.billingAddress.streetName = 'Street Name';
        orderInput.billingAddress.streetNumber = '5';
        orderInput.billingAddress.streetFlatNumber = '6';
        orderInput.billingAddress.zip = '00-000';
        orderInput.billingAddress.city = 'Warsaw';
        orderInput.billingAddress.phone = '+48600000111';
        orderInput.billingAddress.email = 'test@test.com';
        orderInput.billingAddress.company = 'Test Company';
        orderInput.billingAddress.vatNumber = '3334445';
        orderInput.shippingAddress.firstName = 'Test 2';
        orderInput.shippingAddress.lastName = 'Name 2';
        orderInput.shippingAddress.streetName = 'Street Name 2';
        orderInput.shippingAddress.streetNumber = '6';
        orderInput.shippingAddress.streetFlatNumber = '7';
        orderInput.shippingAddress.zip = '00-000';
        orderInput.shippingAddress.city = 'Warsaw2';
        orderInput.shippingAddress.phone = '+48600000112';
        ExtradomApi.OptInInput optinInput = new ExtradomApi.OptInInput();
        optinInput.name = ConstUtils.ORDER_OPT_IN_REGULATIONS_PARAM;
        optinInput.value = true;
        orderInput.optIns.add(optinInput);
        ExtradomApi.OptInInput optinInput2 = new ExtradomApi.OptInInput();
        optinInput2.name = ConstUtils.ORDER_OPT_IN_PRIVACY_POLICY_PARAM;
        optinInput2.value = true;
        orderInput.optIns.add(optinInput2);
        ExtradomApi.OptInInput optinInput3 = new ExtradomApi.OptInInput();
        optinInput3.name = ConstUtils.ORDER_OPT_IN_MARKETING_PARAM;
        optinInput3.value = true;
        orderInput.optIns.add(optinInput3);
        ExtradomApi.OptInInput optinInput4 = new ExtradomApi.OptInInput();
        optinInput4.name = ConstUtils.ORDER_OPT_IN_MARKETING_EXTENDED_PARAM;
        optinInput4.value = true;
        orderInput.optIns.add(optinInput4);
        ExtradomApi.OptInInput optinInput5 = new ExtradomApi.OptInInput();
        optinInput5.name = ConstUtils.ORDER_OPT_IN_MARKETING_PARTNER_PARAM;
        optinInput5.value = true;
        orderInput.optIns.add(optinInput5);
        ExtradomApi.OptInInput optinInput6 = new ExtradomApi.OptInInput();
        optinInput6.name = ConstUtils.ORDER_OPT_IN_LEAD_FORWARD_PARAM;
        optinInput6.value = true;
        orderInput.optIns.add(optinInput6);
        ExtradomApi.AddOnInput addOnInput1 = new ExtradomApi.AddOnInput();
        addOnInput1.name = ADD_ON_1_CODE;
        orderInput.addOns.add(addOnInput1);
        ExtradomApi.AddOnInput addOnInput2 = new ExtradomApi.AddOnInput();
        addOnInput2.name = ADD_ON_2_CODE;
        orderInput.addOns.add(addOnInput2);
        
        RestRequest req = new RestRequest(); 
        req.addHeader('gaClientId','testowe');
        req.requestBody = Blob.valueOf(JSON.serialize(orderInput));
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/app/orders/create';  
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;

        // when
        ExtradomApi.DesignOrderResult dor = AppOrdersCreation.doPost();

        // then
        // to complete
    }
	*/

    @TestSetup
    private static void setupData() {
        Account acc = new Account (Name = 'Test Name');
        insert acc;
        Supplier__c newSupplier = new Supplier__c(Name = 'Supplier', Code__c = 'ABC', Full_Name__c = 'Great Supplier');
        insert newSupplier;
        Design__c design = new Design__c (
                Name = 'First Design', Supplier__c = newSupplier.Id, Full_Name__c = 'First Design', Type__c = ConstUtils.DESIGN_HOUSE_TYPE,
                Code__c = DESIGN_CODE
        );
        insert design;

        Order o = new Order (
                AccountId = acc.Id, EffectiveDate = System.now().date(), Status = 'Draft', Pricebook2Id = Test.getStandardPricebookId(),
                RecordTypeId = ConstUtils.ORDER_REGULAR_RT_ID
        );
        insert o;
        Product2 designProduct = new Product2(
                Name = 'Test Design Product', Family = ConstUtils.PRODUCT_DESIGN_FAMILY, IsActive = true, ProductCode = DESIGN_CODE
        );
        Product2 addOnProduct1 = new Product2(
                Name = 'Test Add On Product 1', Family = ConstUtils.PRODUCT_EXTRA_FAMILY, IsActive = true, ProductCode = 'Add On Product 1'
        );
        Product2 addOnProduct2 = new Product2(
                Name = 'Test Add On Product 2', Family = ConstUtils.PRODUCT_EXTRA_FAMILY, IsActive = true, ProductCode = 'Add On Product 2'
        );
        insert new List<Product2> {designProduct, addOnProduct1, addOnProduct2};
        PricebookEntry designPriceBookEntry = new PricebookEntry(Product2Id = designProduct.Id, IsActive = true, Pricebook2Id = Test.getStandardPricebookId(), UnitPrice = 1000);
        PricebookEntry addOn1PriceBookEntry = new PricebookEntry(Product2Id = addOnProduct1.Id, IsActive = true, Pricebook2Id = Test.getStandardPricebookId(), UnitPrice = 100);
        PricebookEntry addOn2PriceBookEntry = new PricebookEntry(Product2Id = addOnProduct2.Id, IsActive = true, Pricebook2Id = Test.getStandardPricebookId(), UnitPrice = 150);
        insert new List<PricebookEntry> {designPriceBookEntry, addOn1PriceBookEntry, addOn2PriceBookEntry};

        List<OrderItem> orderItems = new List<OrderItem> ();
        for (Integer i = 0; i < INSERTED_SIZE; i++) {
            orderItems.add(new OrderItem(
                    PricebookEntryId = designPriceBookEntry.Id, Quantity = 1, Discount_Sales__c = 0, List_Price__c = 100,
                    OrderId = o.Id, UnitPrice = 0, Description = INSERTED_NAME + String.valueOf(i))
            );
        }
        insert orderItems;
    }

}