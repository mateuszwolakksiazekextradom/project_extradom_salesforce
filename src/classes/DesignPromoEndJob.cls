global without sharing class DesignPromoEndJob implements Schedulable {
    public void execute(SchedulableContext sc) {
        Database.executeBatch(new DesignPromoEndBatch());
    }
}