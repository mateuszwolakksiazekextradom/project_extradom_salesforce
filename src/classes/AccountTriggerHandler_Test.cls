@IsTest
private class AccountTriggerHandler_Test {
    /*@IsTest
    private static void shouldSetWonOpportunityAsConverted_whenAccountMerged() {
        // given
        final String NAME = 'OppTestName';
        Account acc1 = new Account (Name = 'Test Name 1');
        Account acc2 = new Account (Name = 'Test Name 2');
        insert new List<Account> {acc1, acc2};
        Datetime now = system.now();
        Opportunity oppAcc1WithEndDate1 = new Opportunity(
                Name = NAME + '1', StageName = ConstUtils.OPPORTUNITY_CLOSED_WON_STAGE, CloseDate = system.now().date(),
                Enter_Date_Won__c = now.addMinutes(1), AccountId = acc1.Id
        );
        Opportunity oppAcc1WithEndDate2 = new Opportunity(
                Name = NAME + '2', StageName = ConstUtils.OPPORTUNITY_CLOSED_WON_STAGE, CloseDate = system.now().date(),
                Enter_Date_Won__c = now.addMinutes(2), AccountId = acc1.Id
        );
        Opportunity oppAcc2NotEnded = new Opportunity(
                Name = NAME + '3', StageName = ConstUtils.OPPORTUNITY_SOLUTION_PROPOSAL_STAGE, CloseDate = system.now().date(),
                AccountId = acc2.Id
        );
        insert new List<Opportunity> {oppAcc1WithEndDate1, oppAcc1WithEndDate2, oppAcc2NotEnded};
        acc1.Converted_Opportunity__c = oppAcc1WithEndDate1.Id;
        acc2.Converted_Opportunity__c = oppAcc2NotEnded.Id;
        update new List<Account> {acc1, acc2};

        // when
        Database.merge(acc2, acc1.Id);
        List<Account> accs = [SELECT Id, Converted_Opportunity__c FROM Account WHERE Id = :acc1.Id OR Id = :acc2.Id];

        // then
        System.assertEquals(1, accs.size());
        System.assertEquals(acc2.Id, accs.get(0).Id);
        System.assertEquals(oppAcc1WithEndDate1.Id, accs.get(0).Converted_Opportunity__c);
    }
    */
    @IsTest
    private static void shouldCheckCOKUsers_whenAccountsMerge() {
        //given
        User cokUser1 = TestUtils.prepareTestUser(true, ConstUtils.USER_ADMIN_PROFILE, ConstUtils.USER_COK_DEPARTMENT);
        User cokUser2 = TestUtils.prepareTestUser(true, ConstUtils.USER_ADMIN_PROFILE, ConstUtils.USER_COK_DEPARTMENT,'2');
        
        Account acc1 = new Account (Name = 'Test1', ownerId = cokUser1.Id);
        Account acc2 = new Account (Name = 'Test2', ownerId = cokUser2.Id);
        insert new List<Account> {acc1, acc2};
        
        system.runAs(cokUser1) {
            try {
                merge acc1 acc2;
            } catch (Exception e) {
            }
        }
    }
    
    @IsTest
    private static void shouldCloseFollowUpCallTasks_whenAccountsMerge() {
        //given
        User cokUser1 = TestUtils.prepareTestUser(true, ConstUtils.USER_ADMIN_PROFILE, ConstUtils.USER_COK_DEPARTMENT);
        User cokUser2 = TestUtils.prepareTestUser(true, ConstUtils.USER_ADMIN_PROFILE, ConstUtils.USER_COK_DEPARTMENT,'2');
        
        Account acc1 = new Account (Name = 'Test1', ownerId = cokUser1.Id);
        Account acc2 = new Account (Name = 'Test2', ownerId = cokUser2.Id);
        insert new List<Account> {acc1, acc2};       
        
        Task tsk1 = new Task (Type = ConstUtils.TASK_FOLLOW_UP_TYPE, Status = ConstUtils.TASK_NOT_STARTED_STATUS, Subject = 'test1', WhatId = acc1.Id, OwnerId = cokUser1.Id, ActivityDate = system.Today());
        Task tsk2 = new Task (Type = ConstUtils.TASK_FOLLOW_UP_TYPE, Status = ConstUtils.TASK_NOT_STARTED_STATUS, Subject = 'test2', WhatId = acc2.Id, OwnerId = cokUser2.Id, ActivityDate = system.Today() + 1);
        insert new List<Task> {tsk1, tsk2};
        
        //when
        merge acc1 acc2;
        tsk1 = [SELECT Id, Status, OwnerId, WhatId FROM Task WHERE Id = :tsk1.Id];
        tsk2 = [SELECT Id, Status, OwnerId, WhatId FROM Task WHERE Id = :tsk2.Id];
        
        system.debug(tsk1.WhatId);
        system.debug(tsk2.WhatId);
        system.debug(tsk1.Status);
        system.debug(tsk2.Status);
        system.debug(tsk1.OwnerId);
        system.debug(tsk2.OwnerId);
        
        //then
        system.assertEquals(tsk1.Status, ConstUtils.TASK_NOT_STARTED_STATUS);
        system.assertEquals(tsk2.OwnerId, cokUser1.Id);
        system.assertEquals(tsk2.Status, ConstUtils.TASK_COMPLETED_STATUS);  
    }
      
    @IsTest
    private static void shouldSetLeadStatus_whenAccountMerged() {
        //given
        Account acc1 = new Account (Name = 'Test Name 1', Lead_Status__c = ConstUtils.ACCOUNT_NEW_LEAD_STATUS);
        Account acc2 = new Account (Name = 'Test Name 2', Lead_Status__c = ConstUtils.ACCOUNT_UNQUALIFIED_LEAD_STATUS, Unqualified_Reason__c = ConstUtils.ACCOUNT_OTHER_UNQUALIFIED_REASON);
        Account acc3 = new Account (Name = 'Test Name 3', Lead_Status__c = ConstUtils.ACCOUNT_CONVERTED_LEAD_STATUS);
        
        Account acc4 = new Account (Name = 'Test Name 4', Lead_Status__c = ConstUtils.ACCOUNT_NEW_LEAD_STATUS);
        Account acc5 = new Account (Name = 'Test Name 5', Lead_Status__c = ConstUtils.ACCOUNT_MARKETING_QUALIFIED_LEAD_STATUS);
        Account acc6 = new Account (Name = 'Test Name 6', Lead_Status__c = ConstUtils.ACCOUNT_SALES_QUALIFIED_LEAD_STATUS);
        
        insert new List<Account>{acc1,acc2,acc3,acc4,acc5,acc6};
        
        //when
        Database.merge(acc1,new List<Account>{acc2,acc3});
        Database.merge(acc4,new List<Account>{acc5,acc6});
        
        //then
        Account after1 = [SELECT Id, Lead_Status__c FROM Account WHERE Id = :acc1.Id OR Id = :acc2.Id OR Id = :acc3.Id];
        Account after2 = [SELECT Id, Lead_Status__c FROM Account WHERE Id = :acc4.Id OR Id = :acc5.Id OR Id = :acc6.Id];
        
        system.assertEquals(ConstUtils.ACCOUNT_CONVERTED_LEAD_STATUS,after1.Lead_Status__c);
        system.assertEquals(ConstUtils.ACCOUNT_SALES_QUALIFIED_LEAD_STATUS,after2.Lead_Status__c);
    }
    
    @IsTest
    private static void shouldOrganizeOpps_whenAccountMerged() {
        //given
        
        Account acc1 = new Account (Name = 'Test Name 1', Lead_Status__c = ConstUtils.ACCOUNT_CONVERTED_LEAD_STATUS);
        Account acc2 = new Account (Name = 'Test Name 2', Lead_Status__c = ConstUtils.ACCOUNT_UNQUALIFIED_LEAD_STATUS, Unqualified_Reason__c = ConstUtils.ACCOUNT_OTHER_UNQUALIFIED_REASON);
        Account acc3 = new Account (Name = 'Test Name 3', Lead_Status__c = ConstUtils.ACCOUNT_NEW_LEAD_STATUS);
        
        Account acc4 = new Account (Name = 'Test Name 4', Lead_Status__c = ConstUtils.ACCOUNT_CONVERTED_LEAD_STATUS);
        Account acc5 = new Account (Name = 'Test Name 5', Lead_Status__c = ConstUtils.ACCOUNT_UNQUALIFIED_LEAD_STATUS, Unqualified_Reason__c = ConstUtils.ACCOUNT_OTHER_UNQUALIFIED_REASON);
        Account acc6 = new Account (Name = 'Test Name 6', Lead_Status__c = ConstUtils.ACCOUNT_CONVERTED_LEAD_STATUS);
        
        Account acc7 = new Account (Name = 'Test Name 7', Lead_Status__c = ConstUtils.ACCOUNT_CONVERTED_LEAD_STATUS);
        Account acc8 = new Account (Name = 'Test Name 8', Lead_Status__c = ConstUtils.ACCOUNT_UNQUALIFIED_LEAD_STATUS, Unqualified_Reason__c = ConstUtils.ACCOUNT_OTHER_UNQUALIFIED_REASON);
        Account acc9 = new Account (Name = 'Test Name 9', Lead_Status__c = ConstUtils.ACCOUNT_CONVERTED_LEAD_STATUS);
        
        Account acc10 = new Account (Name = 'Test Name 10', Lead_Status__c = ConstUtils.ACCOUNT_CONVERTED_LEAD_STATUS);
        Account acc11 = new Account (Name = 'Test Name 11', Lead_Status__c = ConstUtils.ACCOUNT_UNQUALIFIED_LEAD_STATUS, Unqualified_Reason__c = ConstUtils.ACCOUNT_OTHER_UNQUALIFIED_REASON, Enter_Date_Unqualified__c = system.today() + 1);
        Account acc12 = new Account (Name = 'Test Name 12', Lead_Status__c = ConstUtils.ACCOUNT_CONVERTED_LEAD_STATUS);
        insert new List<Account>{acc1,acc2,acc3,acc4,acc5,acc6,acc7,acc8,acc9,acc10,acc11,acc12};
        
        Opportunity opp1 = new Opportunity (Name = 'Test', StageName = ConstUtils.OPPORTUNITY_CLOSED_WON_STAGE, CloseDate = system.now().date(), AccountId = acc1.Id, Enter_Date_Won__c = System.today() + 1);
        Opportunity opp2 = new Opportunity (Name = 'Test', StageName = ConstUtils.OPPORTUNITY_CLOSED_DUPLICATE_STAGE, CloseDate = system.now().date(), AccountId = acc2.Id, Enter_Date_Lost__c = System.today() + 1);
        Opportunity opp3 = new Opportunity (Name = 'Test', StageName = ConstUtils.OPPORTUNITY_SOLUTION_PROPOSAL_STAGE, CloseDate = system.now().date(), AccountId = acc2.Id, Enter_Date_Solution_Proposal__c = System.today() + 2);
        
        Opportunity opp4 = new Opportunity (Name = 'Test', StageName = ConstUtils.OPPORTUNITY_CLOSED_WON_STAGE, CloseDate = system.now().date(), AccountId = acc4.Id, Enter_Date_Solution_Proposal__c = System.today() + 2);
        Opportunity opp5 = new Opportunity (Name = 'Test', StageName = ConstUtils.OPPORTUNITY_CLOSED_COMPETITOR_STAGE, CloseDate = system.now().date(), AccountId = acc5.Id, Enter_Date_Lost__c = System.today() + 1);
        Opportunity opp6 = new Opportunity (Name = 'Test', StageName = ConstUtils.OPPORTUNITY_SOLUTION_PROPOSAL_STAGE, CloseDate = system.now().date(), AccountId = acc6.Id, Enter_Date_Won__c = System.today() + 1);
        
        Opportunity opp7 = new Opportunity (Name = 'Test', StageName = ConstUtils.OPPORTUNITY_CLOSED_WON_STAGE, CloseDate = system.now().date(), AccountId = acc7.Id, Enter_Date_Won__c = System.today() + 1);
        Opportunity opp8 = new Opportunity (Name = 'Test', StageName = ConstUtils.OPPORTUNITY_CLOSED_COMPETITOR_STAGE, CloseDate = system.now().date(), AccountId = acc8.Id, Enter_Date_Lost__c = System.today() + 2);
        Opportunity opp9 = new Opportunity (Name = 'Test', StageName = ConstUtils.OPPORTUNITY_SOLUTION_PROPOSAL_STAGE, CloseDate = system.now().date(), AccountId = acc9.Id, Enter_Date_Solution_Proposal__c = System.today() + 1);
        
        Opportunity opp10 = new Opportunity (Name = 'Test', StageName = ConstUtils.OPPORTUNITY_CLOSED_WON_STAGE, CloseDate = system.now().date(), AccountId = acc10.Id);
        Opportunity opp11 = new Opportunity (Name = 'Test', StageName = ConstUtils.OPPORTUNITY_CLOSED_COMPETITOR_STAGE, CloseDate = system.now().date(), AccountId = acc11.Id);
        Opportunity opp12 = new Opportunity (Name = 'Test', StageName = ConstUtils.OPPORTUNITY_SOLUTION_PROPOSAL_STAGE, CloseDate = system.now().date(), AccountId = acc12.Id);
        List<Opportunity> opps = new List<Opportunity>{opp1,opp2,opp3,opp4,opp5,opp6,opp7,opp8,opp9,opp10,opp11,opp12};
        insert opps;

        //when
        Database.merge(acc1,new List<Account>{acc2,acc3});
        Database.merge(acc4,new List<Account>{acc5,acc6});
        Database.merge(acc7,new List<Account>{acc8,acc9});
        Database.merge(acc10,new List<Account>{acc11,acc12});
        
        
    }
    
    @IsTest
    private static void shouldCloseFollowUpCallTasks_whenAccountLeadStatusChangeToUnqualified() {
        //given
        Account acc1 = new Account (Name = 'Test Name 1', Lead_Status__c = ConstUtils.ACCOUNT_NEW_LEAD_STATUS);
        Account acc2 = new Account (Name = 'Test Name 2', Lead_Status__c = ConstUtils.ACCOUNT_UNQUALIFIED_LEAD_STATUS, Unqualified_Reason__c = ConstUtils.ACCOUNT_OTHER_UNQUALIFIED_REASON);
        Account acc3 = new Account (Name = 'Test Name 2', Lead_Status__c = ConstUtils.ACCOUNT_NEW_LEAD_STATUS);
        insert new List<Account> {acc1, acc2, acc3};
                
        Task tsk1 = new Task (Type = ConstUtils.TASK_FOLLOW_UP_TYPE, Status = ConstUtils.TASK_NOT_STARTED_STATUS, Subject = 'test1', WhatId = acc1.Id);
        Task tsk2 = new Task (Type = ConstUtils.TASK_FOLLOW_UP_TYPE, Status = ConstUtils.TASK_NOT_STARTED_STATUS, Subject = 'test2', WhatId = acc2.Id);
        Task tsk3 = new Task (Type = ConstUtils.TASK_FOLLOW_UP_TYPE, Status = ConstUtils.TASK_NOT_STARTED_STATUS, Subject = 'test3', WhatId = acc3.Id);
        insert new List<Task> {tsk1, tsk2, tsk3};
        
        Map<Id, AggregateResult> countOpenTasksBeforeUpdate = new Map<Id, AggregateResult> ([SELECT AccountId Id, Count(Id) c FROM Task WHERE AccountId IN (:acc1.Id, :acc2.Id, :acc3.Id) AND Status = :ConstUtils.TASK_NOT_STARTED_STATUS AND Type = :ConstUtils.TASK_FOLLOW_UP_TYPE GROUP BY AccountId]);
        
        //when
        acc1.Lead_Status__c = ConstUtils.ACCOUNT_UNQUALIFIED_LEAD_STATUS;
        acc1.Unqualified_Reason__c = ConstUtils.ACCOUNT_OTHER_UNQUALIFIED_REASON;
        acc2.Lead_Status__c = ConstUtils.ACCOUNT_NEW_LEAD_STATUS;
        acc3.Lead_Status__c = ConstUtils.ACCOUNT_CONVERTED_LEAD_STATUS;        
        update new List<Account> {acc1, acc2, acc3};
        
        Map<Id, AggregateResult> countOpenTasksAfterUpdate = new Map<Id, AggregateResult> ([SELECT AccountId Id, Count(Id) c FROM Task WHERE AccountId IN (:acc1.Id, :acc2.Id, :acc3.Id) AND Status = :ConstUtils.TASK_NOT_STARTED_STATUS AND Type = :ConstUtils.TASK_FOLLOW_UP_TYPE GROUP BY AccountId]);
        
        //then
        System.assertEquals(null,countOpenTasksAfterUpdate.get(acc1.Id) == null ? null : Integer.valueOf(countOpenTasksAfterUpdate.get(acc1.Id).get('c')));
        System.assertEquals(1,Integer.valueOf(countOpenTasksAfterUpdate.get(acc2.Id).get('c')));
        System.assertEquals(1,Integer.valueOf(countOpenTasksAfterUpdate.get(acc3.Id).get('c')));
    }
    
    /*
    @IsTest
    private static void shouldSetOpenOpportunityAsConverted_whenAccountMergedAndNoClosedWonOpp() {
        // given
        final String NAME = 'OppTestName';
        Account acc1 = new Account (Name = 'Test Name 1');
        Account acc2 = new Account (Name = 'Test Name 2');
        insert new List<Account> {acc1, acc2};
        Opportunity oppAcc1ClosedLost = new Opportunity(
                Name = NAME + '1', StageName = ConstUtils.OPPORTUNITY_CLOSED_DUPLICATE_STAGE, CloseDate = system.now().date(),
                AccountId = acc1.Id
        );
        Opportunity oppAcc2Open = new Opportunity(
                Name = NAME + '2', StageName = ConstUtils.OPPORTUNITY_SOLUTION_PROPOSAL_STAGE, CloseDate = system.now().date(),
                AccountId = acc2.Id
        );
        Opportunity oppAcc2ClosedLost = new Opportunity(
                Name = NAME + '3', StageName = ConstUtils.OPPORTUNITY_CLOSED_DUPLICATE_STAGE, CloseDate = system.now().date(),
                AccountId = acc2.Id
        );
        insert new List<Opportunity> {oppAcc1ClosedLost, oppAcc2Open, oppAcc2ClosedLost};
        acc1.Converted_Opportunity__c = oppAcc1ClosedLost.Id;
        acc2.Converted_Opportunity__c = oppAcc2Open.Id;
        update new List<Account> {acc1, acc2};

        // when
        Database.merge(acc1, acc2.Id);
        List<Account> accs = [SELECT Id, Converted_Opportunity__c FROM Account WHERE Id = :acc1.Id OR Id = :acc2.Id];

        // then
        System.assertEquals(1, accs.size());
        System.assertEquals(acc1.Id, accs.get(0).Id);
        System.assertEquals(oppAcc2Open.Id, accs.get(0).Converted_Opportunity__c);
    }
    */
    /*
    @IsTest
    private static void shouldSetFirstOpenAsConverted_whenAccountMergedAndNoClosedWonOpp() {
        // given
        final String NAME = 'OppTestName';
        Account acc1 = new Account (Name = 'Test Name 1');
        Account acc2 = new Account (Name = 'Test Name 2');
        insert new List<Account> {acc1, acc2};
        Opportunity oppAcc1Open = new Opportunity(
                Name = NAME + '1', StageName = ConstUtils.OPPORTUNITY_SOLUTION_PROPOSAL_STAGE, CloseDate = system.now().date(),
                AccountId = acc1.Id
        );
        insert oppAcc1Open;
        Opportunity oppAcc2Open = new Opportunity(
                Name = NAME + '2', StageName = ConstUtils.OPPORTUNITY_SOLUTION_PROPOSAL_STAGE, CloseDate = system.now().date(),
                AccountId = acc2.Id
        );
        Opportunity oppAcc2ClosedLost = new Opportunity(
                Name = NAME + '3', StageName = ConstUtils.OPPORTUNITY_CLOSED_DUPLICATE_STAGE, CloseDate = system.now().date(),
                AccountId = acc2.Id
        );
        insert new List<Opportunity> {oppAcc2Open, oppAcc2ClosedLost};
        acc1.Converted_Opportunity__c = oppAcc1Open.Id;
        acc2.Converted_Opportunity__c = oppAcc2Open.Id;
        update new List<Account> {acc1, acc2};

        // when
        Database.merge(acc1, acc2.Id);
        List<Account> accs = [SELECT Id, Converted_Opportunity__c FROM Account WHERE Id = :acc1.Id OR Id = :acc2.Id];

        // then
        System.assertEquals(1, accs.size());
        System.assertEquals(acc1.Id, accs.get(0).Id);
        System.assertEquals(oppAcc1Open.Id, accs.get(0).Converted_Opportunity__c);
    }*/
    /*
    @IsTest
    private static void shouldSetFirstClosedLostAsConverted_whenAccountMergedAndOnlyClosedLostOpp() {
        // given
        final String NAME = 'OppTestName';
        Account acc1 = new Account (Name = 'Test Name 1');
        Account acc2 = new Account (Name = 'Test Name 2');
        insert new List<Account> {acc1, acc2};
        Datetime now = system.now();
        Opportunity oppAcc1ClosedLost = new Opportunity(
                Name = NAME + '1', StageName = ConstUtils.OPPORTUNITY_CLOSED_DUPLICATE_STAGE, CloseDate = system.now().date(),
                AccountId = acc1.Id, Enter_Date_Lost__c = now
        );
        Opportunity oppAcc1ClosedLost2 = new Opportunity(
                Name = NAME + '2', StageName = ConstUtils.OPPORTUNITY_CLOSED_DUPLICATE_STAGE, CloseDate = system.now().date(),
                AccountId = acc1.Id, Enter_Date_Lost__c = now.addMinutes(1)
        );
        Opportunity oppAcc2ClosedLost = new Opportunity(
                Name = NAME + '3', StageName = ConstUtils.OPPORTUNITY_CLOSED_DUPLICATE_STAGE, CloseDate = system.now().date(),
                AccountId = acc2.Id, Enter_Date_Lost__c = now.addMinutes(2)
        );
        insert new List<Opportunity> {oppAcc1ClosedLost, oppAcc1ClosedLost2, oppAcc2ClosedLost};
        acc1.Converted_Opportunity__c = oppAcc1ClosedLost2.Id;
        acc2.Converted_Opportunity__c = oppAcc2ClosedLost.Id;
        update new List<Account> {acc1, acc2};

        // when
        Database.merge(acc1, acc2.Id);
        List<Account> accs = [SELECT Id, Converted_Opportunity__c FROM Account WHERE Id = :acc1.Id OR Id = :acc2.Id];

        // then
        System.assertEquals(1, accs.size());
        System.assertEquals(acc1.Id, accs.get(0).Id);
        System.assertEquals(oppAcc2ClosedLost.Id, accs.get(0).Converted_Opportunity__c);
    }
    */
    @IsTest
    private static void shouldValidateMarketingFields_whenAccountsMerged() {
        // given
        Account acc1 = new Account (Name = 'Test Name 1');
        Account acc2 = new Account (Name = 'Test Name 2');
        insert new List<Account> {acc1, acc2};
        Contact c1 = new Contact(
                AccountId = acc1.Id, Opt_In_Marketing__c = true, Opt_In_Marketing_Partner__c = false,
                Opt_In_Marketing_Extended__c = false, Opt_In_Lead_Forward__c = false, Opt_Ins_Asked__c = false,
                Email = 'test@test.com', Phone = null, LastName = 'Test Contact 1'
        );
        Contact c2 = new Contact(
                AccountId = acc2.Id, Opt_In_Marketing__c = false, Opt_In_Marketing_Partner__c = true,
                Opt_In_Marketing_Extended__c = true, Opt_In_Lead_Forward__c = true, Opt_Ins_Asked__c = true,
                Email = null, Phone = '500232012', LastName = 'Test Contact 2'
        );
        insert new List<Contact> {c1, c2};
        Map<Id, Account> accsBeforeMerge = new Map<Id, Account> (
            [
                    SELECT Id, Has_Opt_In_Marketing__c, Has_Opt_In_Marketing_Extended__c, Has_Opt_In_Marketing_Partner__c,
                           Has_Opt_In_Lead_Forward__c, Has_Opt_Ins_Asked__c, Has_Contact_with_Email__c, Has_Contact_with_Phone__c
                    FROM Account WHERE Id =: acc1.Id OR Id =: acc2.Id
            ]
        );

        // when
        Database.merge(acc1, acc2.Id);
        List<Account> accs = [
                SELECT Id, Has_Opt_In_Marketing__c, Has_Opt_In_Marketing_Extended__c, Has_Opt_In_Marketing_Partner__c,
                       Has_Opt_In_Lead_Forward__c, Has_Opt_Ins_Asked__c, Has_Contact_with_Email__c, Has_Contact_with_Phone__c
                FROM Account WHERE Id = :acc1.Id OR Id = :acc2.Id
        ];

        // then
        System.assertEquals(2, accsBeforeMerge.size());
        System.assertEquals(true, accsBeforeMerge.get(acc1.Id).Has_Opt_In_Marketing__c);
        System.assertEquals(false, accsBeforeMerge.get(acc1.Id).Has_Opt_In_Marketing_Extended__c);
        System.assertEquals(false, accsBeforeMerge.get(acc1.Id).Has_Opt_In_Marketing_Partner__c);
        System.assertEquals(false, accsBeforeMerge.get(acc1.Id).Has_Opt_In_Lead_Forward__c);
        System.assertEquals(false, accsBeforeMerge.get(acc1.Id).Has_Opt_Ins_Asked__c);
        System.assertEquals(true, accsBeforeMerge.get(acc1.Id).Has_Contact_with_Email__c);
        System.assertEquals(false, accsBeforeMerge.get(acc1.Id).Has_Contact_with_Phone__c);
        System.assertEquals(false, accsBeforeMerge.get(acc2.Id).Has_Opt_In_Marketing__c);
        System.assertEquals(true, accsBeforeMerge.get(acc2.Id).Has_Opt_In_Marketing_Extended__c);
        System.assertEquals(true, accsBeforeMerge.get(acc2.Id).Has_Opt_In_Marketing_Partner__c);
        System.assertEquals(true, accsBeforeMerge.get(acc2.Id).Has_Opt_In_Lead_Forward__c);
        System.assertEquals(true, accsBeforeMerge.get(acc2.Id).Has_Opt_Ins_Asked__c);
        System.assertEquals(false, accsBeforeMerge.get(acc2.Id).Has_Contact_with_Email__c);
        System.assertEquals(true, accsBeforeMerge.get(acc2.Id).Has_Contact_with_Phone__c);
        System.assertEquals(1, accs.size());
        System.assertEquals(acc1.Id, accs.get(0).Id);
        System.assertEquals(true, accs.get(0).Has_Opt_In_Marketing__c);
        System.assertEquals(true, accs.get(0).Has_Opt_In_Marketing_Extended__c);
        System.assertEquals(true, accs.get(0).Has_Opt_In_Marketing_Partner__c);
        System.assertEquals(true, accs.get(0).Has_Opt_In_Lead_Forward__c);
        System.assertEquals(true, accs.get(0).Has_Opt_Ins_Asked__c);
        System.assertEquals(true, accs.get(0).Has_Contact_with_Email__c);
        System.assertEquals(true, accs.get(0).Has_Contact_with_Phone__c);
    }

}