public with sharing class DesignOrderScoreBatch implements Database.Batchable<sObject> {
    public Database.querylocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
                SELECT Id, Order_Score__c, Page_Score__c
                FROM Design__c
                WHERE Status__c = :ConstUtils.DESIGN_ACTIVE_STATUS
        ]);
    }

    public void execute(Database.BatchableContext BC, List<sObject> scope) {
        List<Design__c> designs = (List<Design__c> ) scope;
        for (Design__c design : designs) {
            design.Order_Score__c = design.Page_Score__c;
        }
        DesignTriggerHandler.BYPASS_INVALIDATE_DESIGNS = true;
        update designs;
        DesignTriggerHandler.BYPASS_INVALIDATE_DESIGNS = false;
    }

    public void finish(Database.BatchableContext BC) {

    }
}