public class ConnectApiTools {
    public static Object TextSegmentType { get { return ConnectApi.MessageSegmentType.Text; } }
    public static Object HashtagSegmentType { get { return ConnectApi.MessageSegmentType.Hashtag; } }
    public static Object MentionSegmentType { get { return ConnectApi.MessageSegmentType.Mention; } }
    public static Object LinkSegmentType { get { return ConnectApi.MessageSegmentType.Link; } }
    public DateTime dateTimeValue { get; set; }
    public Integer integerValue { get; set; }
    public Decimal decimalValue { get; set; }
    public String getTimeZoneValue() {
        if (dateTimeValue!=null) {
            String localeFormatDT = dateTimeValue.format();
            return localeFormatDT;
        }
        return null;
    }
    public String getIntegerLocaleValue() {
        if (integerValue!=null) {
            String localeFormatDT = integerValue.format();
            return localeFormatDT;
        }
        return null;
    }
    public String getDecimalLocaleValue() {
        if (decimalValue!=null) {
            String localeFormatDT = decimalValue.format();
            return localeFormatDT;
        }
        return null;
    }
}