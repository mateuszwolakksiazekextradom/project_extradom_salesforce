public with sharing class Utils {

    public static Map<String, String> getPicklistValues(SObjectType sObj, String fieldName) {
        Map<String, String> picklists = new Map<String, String> ();
        if (sObj != null && String.isNotBlank(fieldName)) {
            Map<String, Schema.SObjectField> fields = sObj.getDescribe().fields.getMap();
            List<Schema.PicklistEntry> picklistEntries = fields.get(fieldName).getDescribe().getPickListValues();
            for (Schema.PicklistEntry picklistEntry : picklistEntries) {
                picklists.put(picklistEntry.getValue(), picklistEntry.getLabel());
            }
        }
        return picklists;
    }

    public static Map<String, Integer> getPicklistOrder(SObjectType sObj, String fieldName) {
        Map<String, Integer> picklistsOrder = new Map<String, Integer> ();
        if (sObj != null && String.isNotBlank(fieldName)) {
            Map<String, Schema.SObjectField> fields = sObj.getDescribe().fields.getMap();
            List<Schema.PicklistEntry> picklistEntries = fields.get(fieldName).getDescribe().getPickListValues();
            Integer i = 1;
            for (Schema.PicklistEntry picklistEntry : picklistEntries) {
                picklistsOrder.put(picklistEntry.getValue(), i);
                i++;
            }
        }
        return picklistsOrder;
    }

    public static Set<String> fetchSet(List<sObject> objects, String fieldName) {
        Set<String> fieldValues = new Set<String>();
        for (sObject obj : objects) {
            String value = String.valueOf(obj.get(fieldName));
            if (String.isNotBlank(value)) {
                fieldValues.add(value);
            }
        }
        return fieldValues;
    }
    
    public static Map<String, Decimal> getShippingCosts() {
        Map<String, Decimal> shippingCosts = new Map<String, Decimal> ();
        List<Shipping_Cost_Setting__mdt> shippingCostsSettings = [
            SELECT Id, Shipping_Method__c, Payment_Method__c, Shipping_Cost__c FROM Shipping_Cost_Setting__mdt
        ];
        for (Shipping_Cost_Setting__mdt shippingCost : shippingCostsSettings) {
            shippingCosts.put(shippingCost.Shipping_Method__c + shippingCost.Payment_Method__c, shippingCost.Shipping_Cost__c);
        }
        return shippingCosts;
    }

    public static Map<String, Decimal> getExternalShippingCosts() {
        Map<String, Decimal> shippingCosts = new Map<String, Decimal> ();
        List<Shipping_Cost_Setting__mdt> shippingCostsSettings = [
                SELECT Id, Shipping_Method__c, Payment_Method__c, Shipping_Cost__c, Shipping_Cost_External__c FROM Shipping_Cost_Setting__mdt
        ];
        for (Shipping_Cost_Setting__mdt shippingCost : shippingCostsSettings) {
            shippingCosts.put(shippingCost.Shipping_Method__c + shippingCost.Payment_Method__c, shippingCost.Shipping_Cost_External__c);
        }
        return shippingCosts;
    }
    
    public static String normalizePhone(String phoneString) {
        String resultString = '';

        if (String.isNotBlank(phoneString)) {
            resultString = phoneString.replaceAll('[^0-9\\+]', '');
            if (resultString.startsWith('+')) {
                resultString = resultString.replaceFirst('\\+', '00').replaceAll('\\+', '');
            }
            
            if (resultString.startsWith('0')) {
                resultString = resultString.replaceFirst('^0*', '00');
            }

            if (resultString.startsWith('0048')) {
                resultString = resultString.replaceFirst('0048', '');
            }

        }
        return resultString;
    }
}