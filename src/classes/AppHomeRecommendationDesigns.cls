@RestResource(urlMapping='/app/home/recommendation/designs')
global class AppHomeRecommendationDesigns {
    @HttpGet
    global static ExtradomApi.DesignResultPage doGet() {
        String designId = RestContext.request.params.get('relatedTo');
        ExtradomApi.DesignResultPage designResultPage;
        if (String.isNotBlank(designId)) {
            Design__c d = [SELECT Id, Usable_Area__c, Height__c, Roof_slope__c, Count_Rooms__c, Attic__c, Level__c, Garage__c, Border_Ready__c, Type__c, Construction__c, Roof__c, Ridge__c, Living_Type__c, Basement__c, Shape__c, Garage_Location__c, South_Entrance__c, Is_Passive__c FROM Design__c WHERE Id = :designId];
            Decimal usable_area = 120;
            Decimal height = 8;
            Decimal roof_slope = 40;
            Decimal count_rooms = 0;
            Decimal attic = 1;
            Decimal level = 1;
            Decimal garage = 0;
            if (d.Usable_Area__c != null) usable_area = d.Usable_Area__c;
            if (d.Height__c != null) height = d.Height__c;
            if (d.Roof_slope__c != null) roof_slope = d.Roof_slope__c;
            if (d.Count_Rooms__c != null) count_rooms = d.Count_Rooms__c;
            if (d.Attic__c != null) attic = d.Attic__c;
            if (d.Level__c != null) level = d.Level__c;
            if (d.Garage__c != null) garage = d.Garage__c;
            List<Design__c> designs = [SELECT Id, Code__c, Full_Name__c, Type__c, Gross_Price__c, Current_Price__c, Thumbnail_Base_URL__c, Usable_Area__c, Sketchfab_ID__c, KW_Model__c FROM Design__c WHERE Border_Ready__c = :d.Border_Ready__c AND Type__c = :d.Type__c AND Usable_Area__c >= :usable_area*0.9 AND Usable_Area__c <= :usable_area*1.1 AND Height__c >= :height*0.9 AND Height__c <= :height*1.1 AND Roof_slope__c >= :roof_slope-5 AND Roof_slope__c <= :roof_slope+5 AND Count_Rooms__c >= :count_rooms AND Attic__c = :attic AND Level__c = :level AND Construction__c = :d.Construction__c AND Garage__c = :garage AND Roof__c = :d.Roof__c AND Ridge__c = :d.Ridge__c AND Living_Type__c = :d.Living_Type__c AND Basement__c = :d.Basement__c AND Shape__c = :d.Shape__c AND Garage_Location__c = :d.Garage_Location__c AND South_Entrance__c = :d.South_Entrance__c AND Is_Passive__c = :d.Is_Passive__c AND Id != :d.Id AND Status__c = 'Active' ORDER BY Order_Score__c DESC NULLS LAST LIMIT 6];
            if (designs.size()<6) {
                Integer globalRecsLimit = 6-designs.size();
                designs.addAll([SELECT Id, Code__c, Full_Name__c, Type__c, Gross_Price__c, Current_Price__c, Thumbnail_Base_URL__c, Usable_Area__c, Sketchfab_ID__c, KW_Model__c FROM Design__c WHERE type_1__c = 1 AND Status__c = 'Active' AND Id NOT IN :designs AND Id != :d.Id ORDER BY Order_Score__c DESC NULLS LAST LIMIT :globalRecsLimit]);
            }
            designResultPage = new ExtradomApi.DesignResultPage(designs);
            designResultPage.currentPageUrl = '/app/home/recommendation/designs?relatedTo='+d.Id;
        } else {
            designResultPage = new ExtradomApi.DesignResultPage([SELECT Id, Code__c, Full_Name__c, Type__c, Gross_Price__c, Current_Price__c, Thumbnail_Base_URL__c, Usable_Area__c, Sketchfab_ID__c, KW_Model__c FROM Design__c WHERE type_1__c = 1 AND Status__c = 'Active' ORDER BY Order_Score__c DESC NULLS LAST LIMIT 6]);
            designResultPage.currentPageUrl = '/app/home/recommendation/designs';
        }
        if (UserInfo.getUserType()=='Guest') {
            RestContext.response.addHeader('Cache-Control', 'must-revalidate, max-age=0, s-maxage=3600');
        } else {
            RestContext.response.addHeader('Cache-Control', 'no-cache, must-revalidate, max-age=0, no-store, private, s-maxage=0');
        }
        return designResultPage;
    }
}