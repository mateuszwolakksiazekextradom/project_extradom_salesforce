public without sharing class AccountTelemarketingAssignmentsExtension {
  
  private final Account acc;
  public String code{get; set;}
  public String lostReason{get; set;}
  public String comments{get; set;}
  public String firstName{get; set;}
  public String lastName{get; set;}
  public String phone{get; set;}
  public String email{get; set;}
  public List<Telemarketing__c> tels{get; set;}
  public Telemarketing_Assignment__c telAssignmentPartner;
  public Location__c accInvLoc {
      get {
          return [SELECT Id, RecordType.Name, Parent_Location__r.Parent_Location__c, Location__latitude__s, Location__longitude__s FROM Location__c WHERE Id = :this.acc.Investment_Location__c];
      }
  }
  
  public AccountTelemarketingAssignmentsExtension(ApexPages.StandardController stdController) {
      if (!Test.isRunningTest()) {
          stdController.addFields(new List<String>{'Investment_Location__c'});
      }
      this.acc = (Account)stdController.getRecord();
  }
  
  public Telemarketing_Assignment__c getTelAssignment() {
      
      ID regionId = this.accInvLoc.Id;
      if (this.accInvLoc.RecordType.Name=='City') {
          regionId = this.accInvLoc.Parent_Location__r.Parent_Location__c;
      }
      Map<ID,Telemarketing_Assignment__c> telAssignments = new Map<Id,Telemarketing_Assignment__c>([SELECT Id FROM Telemarketing_Assignment__c WHERE Code__c = :this.code AND Region_ID__c = :regionId]);
      if (telAssignments.keySet().isEmpty()) {
          String query = 'SELECT Id FROM Location__c WHERE RecordType.Name = \'Region\' AND DISTANCE(Location__c, GEOLOCATION('+this.accInvLoc.Location__latitude__s+', '+this.accInvLoc.Location__longitude__s+'), \'km\') < 80';
          Map<ID,Location__c> regions = new Map<ID,Location__c>((List<Location__c>)Database.query(query));
          telAssignments = new Map<Id,Telemarketing_Assignment__c>([SELECT Id FROM Telemarketing_Assignment__c WHERE Code__c = :this.code AND Region_ID__c IN :regions.keySet()]);
      }
      
      Map<ID,Integer> telAssignmentsCount = new Map<ID,Integer>();
      for (ID telAssId : telAssignments.keySet()) {
          telAssignmentsCount.put(telAssId,0);
      }
      
      for (AggregateResult ar: [SELECT Telemarketing_Assignment_ID__c, COUNT(Id)sum FROM Telemarketing__c WHERE Telemarketing_Assignment_ID__c IN :telAssignments.keySet() GROUP BY Telemarketing_Assignment_ID__c ORDER BY COUNT(Id)]) {
          ID telAssId = (ID)ar.get('Telemarketing_Assignment_ID__c');
          Integer telAssIdCount = (Integer)ar.get('sum');
          telAssignmentsCount.put(telAssId,telAssIdCount);
      }
      
      ID telAssIdMin;
      Integer telAssIdMinCount = 100000;
      
      for (ID telAssId : telAssignmentsCount.keySet()) {
          Integer count = telAssignmentsCount.get(telAssId);
          if (count <= telAssIdMinCount) {
              telAssIdMin = telAssId;
              telAssIdMinCount = count;
          }
      }
      
      if (telAssIdMin == null) {
          this.telAssignmentPartner = null;
          return null;
      }
      
      this.telAssignmentPartner = [SELECT Id, Name, Partner_Account_ID__c, Partner_Name__c, Partner_Phone__c, Partner_Email__c, Partner_Company__c, Region_ID__c, Partner_Address__c FROM Telemarketing_Assignment__c WHERE Id = :telAssIdMin LIMIT 1];
      return this.telAssignmentPartner;
      
  }
  
  public PageReference createTelemarketingEntries() {
        List<Telemarketing__c> telsToCreate = new List<Telemarketing__c>();
        List<Telemarketing__c> telsToDelete = [SELECT Id FROM Telemarketing__c WHERE Account__r.Id=:this.acc.Id AND Name=:this.code AND Status__c = 'Deferred'];
        if (!telsToDelete.isEmpty()) {
            delete telsToDelete;
        }
        Telemarketing__c tel = new Telemarketing__c(Name=this.code, Account__c=this.acc.Id, Transfer_Account__c=this.telAssignmentPartner.Partner_Account_ID__c, Telemarketing_Assignment_ID__c=this.telAssignmentPartner.Id, Partner_Name__c=this.telAssignmentPartner.Partner_Name__c, Partner_Email__c=this.telAssignmentPartner.Partner_Email__c, Partner_Phone__c=this.telAssignmentPartner.Partner_Phone__c, Partner_Company__c=this.telAssignmentPartner.Partner_Company__c, Partner_Address__c=this.telAssignmentPartner.Partner_Address__c, First_Name__c=this.firstName, Last_Name__c=this.lastName, Phone__c=this.phone, Email__c=this.email, Region__c=this.acc.Investment_Location__c, Status__c='Won', Comments__c=this.comments, Post_Conversion_Stage__c='Waiting');
        telsToCreate.add(tel);
        insert telsToCreate;
        this.tels = telsToCreate;
        return null;
    }
    
    public List<SelectOption> getTelemarketingLostReasonOptions() {
       List<SelectOption> options = new List<SelectOption>();
       for (Schema.PicklistEntry ple : Telemarketing__c.fields.Lost_Reason__c.getDescribe().getpicklistvalues()) {
          options.add(new SelectOption(ple.getLabel(), ple.getValue()));
       }
       return options;
    }
    
    public PageReference createTelemarketingLostEntryOffer() {
        List<Telemarketing__c> telsToCreate = new List<Telemarketing__c>();
        List<Telemarketing__c> telsToDelete = [SELECT Id FROM Telemarketing__c WHERE Account__r.Id=:this.acc.Id AND Name=:this.code AND Status__c = 'Deferred'];
        if (!telsToDelete.isEmpty()) {
            delete telsToDelete;
        }
        Telemarketing__c tel = new Telemarketing__c(Name=this.code, Account__c=this.acc.Id, Status__c='Lost', Email__c=this.email, Lost_Reason__c='Oferta na maila');
        telsToCreate.add(tel);
        insert telsToCreate;
        this.tels = telsToCreate;
        return null;
    }
    
    public PageReference createTelemarketingLostEntry() {
        List<Telemarketing__c> telsToCreate = new List<Telemarketing__c>();
        List<Telemarketing__c> telsToDelete = [SELECT Id FROM Telemarketing__c WHERE Account__r.Id=:this.acc.Id AND Name=:this.code AND Status__c = 'Deferred'];
        if (!telsToDelete.isEmpty()) {
            delete telsToDelete;
        }
        Telemarketing__c tel = new Telemarketing__c(Name=this.code, Account__c=this.acc.Id, Status__c='Lost', Lost_Reason__c=this.lostReason);
        telsToCreate.add(tel);
        insert telsToCreate;
        this.tels = telsToCreate;
        return null;
    }
    
    public PageReference createTelemarketingCanceledEntry() {
        List<Telemarketing__c> telsToCreate = new List<Telemarketing__c>();
        List<Telemarketing__c> telsToDelete = [SELECT Id FROM Telemarketing__c WHERE Account__r.Id=:this.acc.Id AND Name=:this.code AND Status__c = 'Deferred'];
        if (!telsToDelete.isEmpty()) {
            delete telsToDelete;
        }
        Telemarketing__c tel = new Telemarketing__c(Name=this.code, Account__c=this.acc.Id, Status__c='Canceled');
        telsToCreate.add(tel);
        insert telsToCreate;
        this.tels = telsToCreate;
        return null;
    }
    
    public PageReference createTelemarketingDeferredEntry() {
        List<Telemarketing__c> telsToCreate = new List<Telemarketing__c>();
        List<Telemarketing__c> telsToDelete = [SELECT Id FROM Telemarketing__c WHERE Account__r.Id=:this.acc.Id AND Name=:this.code AND Status__c = 'Deferred'];
        if (!telsToDelete.isEmpty()) {
            delete telsToDelete;
        }
        Telemarketing__c tel = new Telemarketing__c(Name=this.code, Account__c=this.acc.Id, Status__c='Deferred');
        telsToCreate.add(tel);
        insert telsToCreate;
        this.tels = telsToCreate;
        return null;
    }
   
}