@RestResource(urlMapping='/catalogueorder')
global class CatalogueOrderRest {
    @HttpPost
    global static Map<String,String> doPost(String email, String phone, String firstName, String lastName, String streetName, String streetNo, String streetFlat, String postalCode, String city, Boolean optInRegulations, Boolean optIn, String gclid, String hasPlot, String hasMpzp, String constructionPlan, String hasApp) {
        Map<String,Id> data = OrderMethods.getWebLeadContactDetails('Catalogue', firstName, lastName, email, phone, 'Extradom.pl', optIn, optIn, 'No', hasPlot, hasMpzp, constructionPlan, hasApp);
        if (String.isNotBlank(gclid)) {
            OrderMethods.updateGclid(data.get('accountId'), gclid);
        }
        OrderMethods.markSalesReady(data.get('accountId'));
        
        Order__c o = new Order__c();
        o.Add_Ons__c = 'Catalogue';
        o.Account__c = data.get('accountId');
        o.Billing_First_Name__c = firstName;
        o.Billing_Last_Name__c = lastName;
        o.Billing_Address_Street_Name__c = streetName;
        o.Billing_Address_Street_No__c = streetNo;
        o.Billing_Address_Street_Flat__c = streetFlat;
        o.Billing_Postal_Code__c = postalCode;
        o.Billing_City__c = city;
        o.Billing_Email__c = email;
        o.Billing_Phone__c = phone;
        o.Opt_In_Regulations__c = optInRegulations;
        o.Opt_In__c = optIn;
        insert o;
        Map<String,String> result = new Map<String,String>{'result' => 'Dziękujemy za zainteresowanie. Potwierdzenie zamówienia przesłaliśmy pocztą e-mail.'};
        return result;
    }
}