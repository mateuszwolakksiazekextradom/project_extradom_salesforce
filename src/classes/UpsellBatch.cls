public with sharing class UpsellBatch  implements Database.Batchable<sObject> {
    public Database.querylocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
                SELECT Id, Account__c FROM OrderItem
                WHERE PricebookEntry.Product2.Family = :ConstUtils.PRODUCT_UPSELL_FAMILY
                AND Order.Status != :ConstUtils.ORDER_CANCELED_STATUS
                Order BY Account__c
        ]);
    }

    public void execute(Database.BatchableContext BC, List<sObject> scope) {
        List<OrderItem> orderItems = (List<OrderItem> ) scope;
        Set<String> accountIds = Utils.fetchSet(orderItems, 'Account__c');
        calculateUpsellOrderItems(accountIds);
    }

    private void calculateUpsellOrderItems(Set<String> accountIds) {
        if (accountIds.size() > 0) {
            List<Account> accounts = [
                    SELECT Id, Upsell_Products__c,
                    (
                            SELECT Id, Quantity, PricebookEntry.Product2.Family FROM Order_Products__r
                            WHERE PricebookEntry.Product2.Family = :ConstUtils.PRODUCT_UPSELL_FAMILY AND Order.Status != :ConstUtils.ORDER_CANCELED_STATUS
                    )
                    FROM Account WHERE Id in :accountIds
            ];
            List<Account> accountsToUpdate = new List<Account> ();
            for (Account a : accounts) {
                Integer numOfUpsellProducts = 0;
                for (OrderItem oi : a.Order_Products__r) {
                    numOfUpsellProducts += oi.Quantity != null ? Integer.valueOf(oi.Quantity) : 0;
                }
                if (a.Upsell_Products__c != numOfUpsellProducts) {
                    a.Upsell_Products__c = numOfUpsellProducts;
                    accountsToUpdate.add(a);
                }
            }
            update accountsToUpdate;
        }
    }

    public void finish(Database.BatchableContext BC) {

    }
}