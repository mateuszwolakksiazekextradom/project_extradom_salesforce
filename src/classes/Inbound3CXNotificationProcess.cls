global class Inbound3CXNotificationProcess implements Messaging.InboundEmailHandler {

    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        

        Set<Id> queueGroupMemberIds = new Set<Id>{};
        for (GroupMember gm : [SELECT UserOrGroupId FROM GroupMember WHERE GroupId = '00Gb0000004hVadEAE']) {
            queueGroupMemberIds.add(gm.UserOrGroupId);
        }
        Map<String,Id> queuePhoneId = new Map<String,Id>();
        for (User u : [SELECT Id, Phone_Extension__c FROM User WHERE Id IN :queueGroupMemberIds]) {
            queuePhoneId.put(u.Phone_Extension__c,u.Id);  
        }
        
        Set<Id> telemarketingMemberIds = new Set<Id>{};
        for (GroupMember gm : [SELECT UserOrGroupId FROM GroupMember WHERE GroupId = '00G0N000004fbd0UAA']) {
            telemarketingMemberIds.add(gm.UserOrGroupId);
        }
        Map<String,Id> telemarketingPhoneId = new Map<String,Id>();
        for (User u : [SELECT Id, Phone_Extension__c FROM User WHERE Id IN :telemarketingMemberIds]) {
            telemarketingPhoneId.put(u.Phone_Extension__c,u.Id);  
        }
        
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        
        String body = email.plainTextBody;
        String subject = email.subject;
        
        Pattern p;
        Matcher m;
        
        String caller;
        String extension;
        
        if (subject.startsWith('Lost Queue Call')) {
            p = Pattern.compile('.*\\([0-9](\\d{2})\\) from Caller ID (\\d{9}\\d*).*');
            m = p.matcher(body);
            if (m.find()) {
                caller = m.group(2);
                extension = m.group(1);
            }
        } else if (subject.startsWith('New missed call')) {
            p = Pattern.compile('.*You have a new missed call from (\\d{9}\\d*).*');
            m = p.matcher(body);
            if (m.find()) {
                caller = m.group(1);
            }
            p = Pattern.compile('.*To: "[0-9](\\d{2})".*');
            m = p.matcher(body);
            if (m.find()) {
                extension = m.group(1);
            }
        }
        
        User extensionUser;
        List<User> userList = [SELECT Id, Profile.Name, IsActive, Is_Live__c, Department FROM User WHERE Phone_Extension__c =: extension LIMIT 1];
        If(userList != null && !userList.isEmpty()) {
            extensionUser = userList[0];
        }
        
        System.debug(body);
        System.debug(caller);
        
        ID ownerId;
        ID contactId;
        Boolean createContact = true;
        
        // Caller Contact already exists
        for (Contact c : [SELECT Id, Account.OwnerId, Account.Owner.IsActive, Account.Owner.Profile.Name, Account.Type, Account.Owner.Department FROM Contact WHERE PhoneId__c = :caller AND PhoneId__c != '' LIMIT 1]) {
            contactId = c.Id;
            if (telemarketingPhoneId.get(extension) != null) {
                ownerId = telemarketingPhoneId.get(extension);
            } else if (extension == '08') {
                ownerId = '005b00000016Ei1';
            } else if (c.Account.Type == 'Supplier' && extensionUser != null) {
                ownerId = extensionUser.Id;
            } else if (c.Account.Owner.IsActive && c.Account.Owner.Profile.Name != 'System Administrator') {
                ownerId = c.Account.OwnerId;
            } else if (c.Account.Owner.Department == 'B2B') {
                ownerId = QueueTools.getAccountNextUser('Inbound Calls Dealer');
            } else if (queuePhoneId.get(extension) != null) {
                ownerId = queuePhoneId.get(extension);
            } else {
                ownerId = QueueTools.getAccountNextUser('Inbound Calls');
            }
        }
        
        // Caller Contact does not exists - search for extension user caller drops the call + create new contact
        if (contactId==null && String.isNotBlank(caller) && String.isNotBlank(extension)) {
            Account a = new Account();
            a.Name = caller;
            if (extensionUser != null) {
                // user extension found
                if (telemarketingPhoneId.get(extension) != null) {
                    ownerId = telemarketingPhoneId.get(extension);
                    createContact = false;
                } else if (extensionUser.IsActive && extensionUser.Is_Live__c) {
                    ownerId = extensionUser.Id;                  
                } else if (extensionUser.Department == 'B2B') {
                    ownerId = QueueTools.getAccountNextUser('Inbound Calls Dealer');
                    createContact = false;
                } else if (extensionUser.Department == 'COK' || extension == '60' || extension == '30') {
                    ownerId = QueueTools.getAccountNextUser('Inbound Calls');
                    a.AccountSource = 'Inbound Call';
                } else {
                    ownerId = '005b0000000RxOP';
                    createContact = false;
                }
            }
            if (ownerId==null) {
                // user extension not found
                if (extension == '08') {
                    ownerId = '005b00000016Ei1';
                } else if (extension == '06') {
                    ownerId = '005b0000002FueI';
                } else {
                    ownerId = QueueTools.getAccountNextUser('Inbound Calls');
                }                
                a.AccountSource = 'Inbound Call';
                createContact = extension == '05';
            }
            if(createContact) {
                a.OwnerId = ownerId;
                insert a;
                Contact c = new Contact();
                c.LastName = caller;
                c.Phone = caller;
                c.AccountId = a.Id;
                c.OwnerId = ownerId;
                insert c;
                contactId = c.Id;
            }
        }
                
        
        if (((contactId != null && [SELECT Count() FROM Task WHERE WhoId = :contactId AND Type = 'Unanswered Call' AND Status = 'Not Started'] == 0) || !createContact) && ownerId != null && String.isNotBlank(caller) && String.isNotBlank(extension)) {
            Task t = new Task();
            extension = extension == '05' ? '60' : (extension == '07' ? '61' : extension);
            t.Subject = 'Porzucone połączenie klienta z numeru ' + caller + ' na numer ' + extension;
            t.WhoId = createContact ? contactId : null;
            t.Priority = 'High';
            t.ActivityDate = System.today();
            t.Status = 'Not Started';
            t.OwnerId = ownerId;
            t.Type = 'Unanswered Call';
            t.Start_Date__c = System.now();
            insert t;
        }

        return result;
    }
}