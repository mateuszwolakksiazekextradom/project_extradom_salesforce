@isTest
public class DesignSmallXMLEmailTest
{
    static testMethod void testMethod1()
    {
        
        List<Design__c> ds= new List<Design__c>();
        
        Supplier__c s = new Supplier__c(Name='Test',Full_Name__c='Test TST',Code__c='TST');
        insert s;
        
        for(Integer i=0 ; i<7; i++)
        {
            Design__c d = new Design__c(Name='Test'+i,Full_Name__c='Test TST'+i,Code__c='TST'+i,Supplier__c=s.Id);
            ds.add(d);
        }
         
        insert ds;
         
        Test.startTest();
 
            DesignSmallXMLEmail obj = new DesignSmallXMLEmail(ds);
            DataBase.executeBatch(obj);
             
        Test.stopTest();
    }
}