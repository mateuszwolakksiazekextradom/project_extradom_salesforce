public with sharing class CommentViewController {

    private final String commentId;
    
    public CommentViewController() {
        this.commentId = ApexPages.currentPage().getParameters().get('id');
    }
    
    public ConnectApi.Comment getComment() {
        return ConnectApi.ChatterFeeds.getComment(Network.getNetworkId(), this.commentId);
    }
    
    public ConnectApi.FeedElement getFeedElementFromComment() {
        return ConnectApi.ChatterFeeds.getFeedElement(Network.getNetworkId(), ConnectApi.ChatterFeeds.getComment(Network.getNetworkId(), this.commentId).feedElement.id);
    }

}