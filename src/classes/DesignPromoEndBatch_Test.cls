@IsTest
private class DesignPromoEndBatch_Test {
    private static final Integer NUM_OF_DESIGNS = 100;

    @IsTest
    private static void shouldProcessEndPromoDesigns() {
        // given
        Supplier__c newSupplier = new Supplier__c(Name = 'Supplier', Code__c = '11', Full_Name__c = 'Great Supplier');
        insert newSupplier;
        final String DESIGN_TO_PROCESS_PREFIX = 'Design End Before ';
        final String DESIGN_NOT_TO_PROCESS_PREFIX = 'Design End After ';
        List<Design__c> designs = new List<Design__c> ();
        for (Integer i = 0; i < NUM_OF_DESIGNS; i++) {
            designs.add(
                    new Design__c (
                            Name = DESIGN_TO_PROCESS_PREFIX + String.valueOf(i), Supplier__c = newSupplier.Id, Full_Name__c = DESIGN_TO_PROCESS_PREFIX + String.valueOf(i),
                            Type__c = ConstUtils.DESIGN_HOUSE_TYPE, Promo_Price_End_Date__c = system.now().addDays(-i), Promo_Price__c = 20.0, Promo_Price_inc__c = 20.0,
                            Promo_Name__c = 'Test Name'
                    )
            );
        }
        for (Integer i = 0; i < NUM_OF_DESIGNS; i++) {
            designs.add(
                    new Design__c (
                            Name = DESIGN_NOT_TO_PROCESS_PREFIX + String.valueOf(i), Supplier__c = newSupplier.Id, Full_Name__c = DESIGN_NOT_TO_PROCESS_PREFIX + String.valueOf(i),
                            Type__c = ConstUtils.DESIGN_HOUSE_TYPE, Promo_Price_End_Date__c = system.now().addDays(i+1), Promo_Price__c = 20.0, Promo_Price_inc__c = 20.0,
                            Promo_Name__c = 'Test Name'
                    )
            );
        }
        insert designs;

        // when
        Test.startTest();
        Database.executeBatch(new DesignPromoEndBatch());
        Test.stopTest();
        List<Design__c> processedDesigns = [
                SELECT Id, Name, Promo_Name__c, Promo_Price__c, Promo_Price_inc__c, Promo_Price_End_Date__c FROM Design__c
                WHERE Promo_Price_End_Date__c = null AND Promo_Price__c = null AND Promo_Price_inc__c = null AND Promo_Name__c = null
        ];

        // then
        System.assertEquals(NUM_OF_DESIGNS, processedDesigns.size());
        for (Design__c processedDesign : processedDesigns) {
            System.assert(processedDesign.Name.startsWith(DESIGN_TO_PROCESS_PREFIX));
            System.assertEquals(null, processedDesign.Promo_Price_End_Date__c);
            System.assertEquals(null, processedDesign.Promo_Name__c);
            System.assertEquals(null, processedDesign.Promo_Price__c);
            System.assertEquals(null, processedDesign.Promo_Price_inc__c);
        }
    }
}