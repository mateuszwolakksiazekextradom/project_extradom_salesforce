public class QueueTools {
  
  public static Id getAccountNextUser(String queue) {
    
    Group queueGroup = [SELECT Id FROM Group WHERE Name = :('Queue: '+queue) LIMIT 1];
    List<GroupMember> queueGroupMembers = [SELECT UserOrGroupId FROM GroupMember WHERE GroupId = :queueGroup.Id];
    Set<Id> queueGroupMemberIds = new Set<Id>{};
    for (GroupMember gm : queueGroupMembers) {
      queueGroupMemberIds.add(gm.UserOrGroupId);
    }
    
    Id nextUserId;
    Double nextQueueRate;
    Double nextQueueIdleRate;
    
    Map<Id,User> queueActiveUsers = new Map<Id,User>([SELECT Id, This_Month_Hours__c, ManagerId FROM User WHERE Id IN :queueGroupMemberIds AND IsActive = true AND Is_Live__c = false]);
    Map<Id,User> queueLiveUsers = new Map<Id,User>([SELECT Id, This_Month_Hours__c, ManagerId FROM User WHERE Id IN :queueGroupMemberIds AND IsActive = true AND Is_Live__c = true]);
    Map<Id,User> queueUsers;
    if (queueLiveUsers.isEmpty()) {
      queueUsers = queueActiveUsers;
    } else {
      queueUsers = queueLiveUsers;
    }
    
    Map<Id,Integer> qCounts = new Map<Id,Integer>{};
    for (AggregateResult ar : [SELECT OwnerId, count(Id)qCount FROM Account WHERE OwnerId IN :queueUsers.keySet() AND Account_Source_EN__c = 'Inbound Call' AND CreatedDate = THIS_MONTH GROUP BY OwnerId]) {
        qCounts.put((Id)ar.get('OwnerId'), (Integer)ar.get('qCount'));
    }
    Map<Id,Integer> teamUserCounts = new Map<Id,Integer>{};
    Map<Id,Integer> teamQCounts = new Map<Id,Integer>{};
    for (User m : queueUsers.values()) {
        if (!qCounts.containsKey(m.Id)) {
            qCounts.put(m.Id, 0);
        }
        Integer qCount = qCounts.get(m.Id);
        if (String.isNotBlank(m.ManagerId)) {
            if (teamUserCounts.containsKey(m.ManagerId)) {
                Integer teamUserCount = teamUserCounts.get(m.ManagerId) + 1;
                Integer teamQCount = teamQCounts.get(m.ManagerId) + qCount;
                teamUserCounts.put(m.ManagerId, teamUserCount);
                teamQCounts.put(m.ManagerId, teamQCount);
            } else {
                teamUserCounts.put(m.ManagerId, 1);
                teamQCounts.put(m.ManagerId, qCount);
            }
        }
    }
    System.debug(teamQCounts);
    System.debug(teamUserCounts);
    String orderManagerId = '';
    Double orderManagerRate = 0;
    Integer orderManagerCount = 0;
    for (Id managerId : teamUserCounts.keySet()) {
        Integer qCount = teamQCounts.get(managerId);
        Double userCount = (Double)teamUserCounts.get(managerId);
        Double qRate = qCount / userCount;
        System.debug(managerId);
        System.debug(qRate);
        if (qRate <= orderManagerRate || orderManagerId == '') {
            orderManagerId = (String)managerId;
            orderManagerRate = qRate;
            orderManagerCount = qCount;
        }
    }
    if (queue != 'Inbound Calls' || (queue == 'Inbound Calls' && !queueLiveUsers.isEmpty()) || Test.isRunningTest()) {
        Set<Id> processedUsers = new Set<Id>();
        List<AggregateResult> queueResults = [SELECT OwnerId userId, COUNT(Id) countAccounts FROM Account WHERE CreatedDate = THIS_MONTH AND Account_Source_EN__c = 'Inbound Call' AND OwnerId IN :queueUsers.keySet() GROUP BY OwnerId];
        // calculate rates for owners with at least 1 assignment this month
        for (User m : queueUsers.values()) {
            for (AggregateResult ar : queueResults) {
              if (ar.get('userid')==(String)m.Id && orderManagerId==(String)m.ManagerId) {
                  Id userId = (Id)ar.get('userId');
                  processedUsers.add(userId);
                  Integer countAccounts = (Integer)ar.get('countAccounts');
                  Double thisMonthHours = (Double)(((User)queueUsers.get(userId)).This_Month_Hours__c);
                  Double queueRate = countAccounts / Math.max(thisMonthHours, 0.001);
                  if (nextUserId == null || queueRate < nextQueueRate) {
                    nextQueueRate = queueRate;
                    nextUserId = userId;
                  }
              }
            }
        }
        // if there is at least 1 user with no queue assignment (rate=0), select the one with greatest idle time
        for (User u : queueUsers.values()) {
          if (!processedUsers.contains(u.Id) && orderManagerId==(String)u.ManagerId) {
            if (nextQueueIdleRate == null || u.This_Month_Hours__c > nextQueueIdleRate) {
              nextQueueIdleRate = u.This_Month_Hours__c;
              nextUserId = u.Id;
            }
          }
        }
    }
   
    if (nextUserId == null || Test.isRunningTest()) {
      nextUserId = '005b0000000RxOP';
    }

    return nextUserId;
  }

}