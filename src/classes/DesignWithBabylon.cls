public class DesignWithBabylon {

    public List<Design__c> getDesigns () {
        Set<Id> ids = new Set<Id>();
        Set<Id> dids = new Set<id>();

        For (Attachment a : ([SELECT Id, ParentId FROM Attachment WHERE Parent.Type = 'Media__c' AND Name LIKE '%babylon%'])) {
           ids.add(a.ParentId); 
        }
        
        For (Media__c m : [SELECT Id, Design__r.Id FROM Media__c WHERE Id IN :ids]) {
           dids.add(m.Design__r.Id); 
        }
        
        return [SELECT Id, Name FROM Design__c WHERE Id IN :dids];
    }
}