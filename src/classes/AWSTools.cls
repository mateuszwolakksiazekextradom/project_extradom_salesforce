public class AWSTools {

@Future(callout=true)
public static void uploadFileBase64(String bucketname, String filekey, String binaryFileString, String contentType, String key, String secret) {
        String formattedDateString = Datetime.now().formatGMT('EEE, dd MMM yyyy HH:mm:ss +0000');
        HttpRequest req = new HttpRequest();
        req.setMethod('PUT');
        req.setHeader('Host','s3-eu-west-1.amazonaws.com');
        req.setEndpoint('https://s3-eu-west-1.amazonaws.com/'+ bucketName + '/' + filekey);
        req.setHeader('Content-Length', string.valueOf(binaryFileString.length()));
        //req.setHeader('Content-Encoding', 'base64');
        req.setHeader('Content-Type', contentType);
        req.setHeader('Date', formattedDateString);

        //get signature string
        String stringToSign = 'PUT\n\n'+contentType+'\n'+formattedDateString+'\n/'+bucketname+'/'+filekey;
        String signed = createSignature(stringToSign, secret);
        String authHeader = 'AWS' + ' ' + key + ':' + signed;
        req.setHeader('Authorization',authHeader);
        Blob file = EncodingUtil.base64Decode(binaryFileString);
        req.setBodyAsBlob(file);
        Http http = new Http();

        try {
            //Execute web service call
            HTTPResponse res = http.send(req);
            System.debug('RESPONSE STRING: ' + res.toString());
            System.debug('RESPONSE STATUS: '+res.getStatus());
            System.debug('STATUS_CODE: '+res.getStatusCode());

        } catch(System.CalloutException e) {
            system.debug('AWS Service Callout Exception: ' + e.getMessage());
        }

}

public static String createSignature(String canonicalBuffer, String secret){
        Blob mac = Crypto.generateMac('HMACSHA1', Blob.valueOf(canonicalBuffer), Blob.valueOf(secret));
        return EncodingUtil.base64Encode(mac);
}

}