@RestResource(urlMapping='/app/list/items')
global class AppListItems {
    @HttpGet
    global static List<ExtradomApi.ItemResult> doGet() {
        
        if (UserInfo.getUserType()=='Guest') {
            RestContext.response.addHeader('Cache-Control', 'must-revalidate, max-age=0, s-maxage=3600');
        } else {
            RestContext.response.addHeader('Cache-Control', 'no-cache, must-revalidate, max-age=0, no-store, private, s-maxage=0');
        }
        
        String role = RestContext.request.params.get('role');
        return ExtradomApi.getListItemResults(role);
        
    }
}