@IsTest
private class Utils_Test {

    @IsTest
    static void shouldReturnPicklistValues () {
        // given
        SObjectType oppType = Opportunity.sObjectType;
        final String stageField = 'StageName';
        List<Schema.PicklistEntry> picklistEntries = oppType.getDescribe().fields.getMap().get(stageField).getDescribe().getPickListValues();

        // when
        Map<String, String> oppStages = Utils.getPicklistValues(oppType, stageField);

        // then
        System.assertEquals(picklistEntries.size(), oppStages.size());
        for (PicklistEntry entry : picklistEntries) {
            System.assert(oppStages.containsKey(entry.getValue()));
            System.assertEquals(entry.getLabel(), oppStages.get(entry.getValue()));
        }
    }

    @IsTest
    static void shouldReturnPicklistOrder () {
        // given
        SObjectType oppType = Opportunity.sObjectType;
        final String stageField = 'StageName';
        List<Schema.PicklistEntry> picklistEntries = oppType.getDescribe().fields.getMap().get(stageField).getDescribe().getPickListValues();

        // when
        Map<String, Integer> oppStages = Utils.getPicklistOrder(oppType, stageField);

        // then
        System.assertEquals(picklistEntries.size(), oppStages.size());
        Integer i = 1;
        for (PicklistEntry entry : picklistEntries) {
            System.assertEquals(i, oppStages.get(entry.getValue()));
            i++;
        }
    }

    @isTest
    public static void shouldFetchFieldAsASetInSObjectList() {
        //given
        Case c1 = new Case(Subject = 'subject1', Description = 'Description1');
        Case c2 = new Case(Subject = 'subject2', Description = 'Description2');

        //when
        Set<String> values = Utils.fetchSet(new List<sObject>{
                c1, c2
        }, 'Subject');

        //then
        system.assertEquals(2, values.size());
        system.assert(values.contains('subject1'));
        system.assert(values.contains('subject2'));
    }
}