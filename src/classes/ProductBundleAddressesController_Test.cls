@IsTest
private class ProductBundleAddressesController_Test {
    @IsTest
    private static void shouldReturnPaymentMethodsPicklist() {
        // given
        List<Schema.PicklistEntry> picklistEntries = Order.sObjectType.getDescribe().fields.getMap().get('Payment_Method__c').getDescribe().getPickListValues();
        Map<String, String> picklists = new Map<String, String> ();
        for (Schema.PicklistEntry pe : picklistEntries) {
            picklists.put(pe.getValue(), pe.getLabel());
        }

        // when
        List<SelectOptionWrapper> paymentMethods = ProductBundleAddressesController.getPaymentMethods();

        // then
        System.assertEquals(picklists.size(), paymentMethods.size());
        for (SelectOptionWrapper paymentMethod : paymentMethods) {
            System.assert(picklists.containsKey(paymentMethod.value));
            System.assertEquals(picklists.get(paymentMethod.value), paymentMethod.label);
        }
    }

    @IsTest
    private static void shouldReturnShippingMethodsPicklist() {
        // given
        List<Schema.PicklistEntry> picklistEntries = Order.sObjectType.getDescribe().fields.getMap().get('Shipping_Method__c').getDescribe().getPickListValues();
        Map<String, String> picklists = new Map<String, String> ();
        for (Schema.PicklistEntry pe : picklistEntries) {
            picklists.put(pe.getValue(), pe.getLabel());
        }

        // when
        List<SelectOptionWrapper> shippingMethods = ProductBundleAddressesController.getShippingMethods();

        // then
        System.assertEquals(picklists.size(), shippingMethods.size());
        for (SelectOptionWrapper shippingMethod : shippingMethods) {
            System.assert(picklists.containsKey(shippingMethod.value));
            System.assertEquals(picklists.get(shippingMethod.value), shippingMethod.label);
        }
    }

    @IsTest
    private static void shouldReturnShippingCostsSettings() {
        // given
        List<Shipping_Cost_Setting__mdt> shippingCostSettings = [SELECT Payment_Method__c, Shipping_Method__c, Shipping_Cost__c FROM Shipping_Cost_Setting__mdt];

        // when
        List<Shipping_Cost_Setting__mdt> shippingCostSettingsRetrieved = ProductBundleAddressesController.getShippingCostSettings();

        // then
        System.assertEquals(shippingCostSettings.size(), shippingCostSettingsRetrieved.size());
    }
}