@RestResource(urlMapping='/crm/route')
global without sharing class CrmRoute {
    @HttpPost
    global static void doPost(String inbound, String caller, String callid) {
        RestContext.response.addHeader('Cache-Control', 'no-cache, must-revalidate, max-age=0, no-store, private, s-maxage=0');
        RestContext.response.statusCode = 404;
        String ext = inbound.right(2);
        if (ext == '88') {
            ext = '60';
        }
        User u = [SELECT Id, Department, Is_Live__c, Manager.Team_Extension__c FROM User WHERE Phone_Extension__c =: ext];
        Contact c;
        try {
            c = [SELECT Id, Account.Id, Account.Type, Account.Lead_Status__c, Account.Lead_Source__c, Account.Owner.Id, Account.Owner.Department, Account.Owner.Phone_Extension__c, Account.Owner.Manager.Team_Extension__c, Account.Owner.Is_Live__c FROM Contact WHERE PhoneId__c =: caller];
            Boolean accountChanged = false;
            if (c.Account.Type == 'Investor' && (String.isBlank(c.Account.Lead_Status__c) || c.Account.Lead_Status__c == 'New' || c.Account.Lead_Status__c == 'Marketing Qualified')) {
                Account a = c.Account;
                a.Lead_Status__c = 'Sales Ready';
                accountChanged = true;
            }
            if (String.isBlank(c.Account.Lead_Source__c)) {
                c.Account.Lead_Source__c = 'Inbound Call';
                c.Account.Lead_Source_Date__c = System.now();
                accountChanged = true;
            }
            if (accountChanged) {
                update c.Account;
            }
        } catch (Exception e) {
            Account a = new Account(Name=caller, OwnerId='005b0000000RxOP', Type='Investor', AccountSource='Inbound Call', Lead_Status__c='Sales Ready', Lead_Source__c='Inbound Call', Lead_Source_Date__c=System.now());
            insert a;
            c = new Contact(AccountId=a.Id, OwnerId='005b0000000RxOP', Phone=caller, LastName=caller);
            insert c;
            c = [SELECT Id, Account.Type, Account.Owner.Id, Account.Owner.Department, Account.Owner.Phone_Extension__c, Account.Owner.Manager.Team_Extension__c, Account.Owner.Is_Live__c FROM Contact WHERE PhoneId__c =: caller];
        }
        if (c.Account.Type == 'Supplier') {
            if (u.Department == 'B2B') {
                RestContext.response.responseBody = Blob.valueOf('1'+ext);
            } else {
                RestContext.response.responseBody = Blob.valueOf('2'+ext);
            }
            RestContext.response.statusCode = 201;
        } else if ((c.Account.Type == 'Dealer' || c.Account.Type == 'Company' || u.Department == 'B2B') && String.isNotBlank(c.Account.Owner.Phone_Extension__c)) {
            try {
                BusinessHours bh = [select id, MondayEndTime from businesshours where name = 'B2B'];
                if (Datetime.now().time()>=bh.MondayEndTime || Datetime.now().format('EEEE')=='Saturday' || Datetime.now().format('EEEE')=='Sunday' || Test.isRunningTest()) {
                    if (c.Account.Owner.Is_Live__c && c.Account.Owner.Department == 'COK') {
                        RestContext.response.responseBody = Blob.valueOf('2'+c.Account.Owner.Phone_Extension__c);
                    } else {
                        RestContext.response.responseBody = Blob.valueOf('260');
                    }
                    RestContext.response.statusCode = 201;
                }
            } catch (Exception e) {
            }
            if (RestContext.response.statusCode != 201 || Test.isRunningTest()) {
                if (c.Account.Type == 'Company') {
                    RestContext.response.responseBody = Blob.valueOf('3'+c.Account.Owner.Phone_Extension__c);
                    RestContext.response.statusCode = 201;
                } else if (c.Account.Type == 'Dealer') {
                    RestContext.response.responseBody = Blob.valueOf('1'+c.Account.Owner.Phone_Extension__c);
                    RestContext.response.statusCode = 201;
                } else {
                    RestContext.response.responseBody = Blob.valueOf('1'+ext);
                    RestContext.response.statusCode = 201;
                }
            }
        } else if ((u.Department == 'COK' || ext == '60') && String.isNotBlank(c.Account.Owner.Phone_Extension__c)) {
            if (c.Account.Owner.Phone_Extension__c=='60') {
                Map<String,Integer> countUsers = new Map<String,Integer>{};
                Map<String,Integer> qCounts = new Map<String,Integer>{};
                for (AggregateResult ar : [SELECT Manager.Team_Extension__c teamext, count(Id) countusers FROM User WHERE Department = 'COK' AND ManagerId != '' AND Manager.Team_Extension__c != '' GROUP BY Manager.Team_Extension__c]) {
                    countUsers.put((String)ar.get('teamext'), (Integer)ar.get('countusers'));
                    qCounts.put((String)ar.get('teamext'), 0);
                }
                for (AggregateResult ar : [SELECT Owner.Manager.Team_Extension__c teamext, count(Id) qcount FROM Account WHERE Type = 'Investor' AND SPAM__c = false AND Lead_Source__c = 'Inbound Call' AND CreatedDate = THIS_MONTH AND Owner.Department = 'COK' AND Owner.ManagerId != '' AND Owner.Manager.Team_Extension__c != '' GROUP BY Owner.Manager.Team_Extension__c]){
                    Integer qcount = qCounts.get((String)ar.get('teamext'));
                    qCounts.put((String)ar.get('teamext'), qcount + (Integer)ar.get('qcount'));
                }
                String selectedteamext = '60';
                Double selectedqrate = 100000000.0;
                for (String teamext : countUsers.keySet()) {
                    Integer countUser = countUsers.get(teamext);
                    Integer qcount = qCounts.get(teamext);
                    Double qrate = qcount / (Double)countUser;
                    if (qrate <= selectedqrate) {
                        selectedqrate = qrate;
                        selectedteamext = teamext;
                    }
                }
                RestContext.response.responseBody = Blob.valueOf('1'+selectedteamext);
            } else {
                if (c.Account.Owner.Is_Live__c) {
                    RestContext.response.responseBody = Blob.valueOf('2'+c.Account.Owner.Phone_Extension__c);
                } else if (String.isNotBlank(c.Account.Owner.Manager.Team_Extension__c)) {
                    RestContext.response.responseBody = Blob.valueOf('2'+c.Account.Owner.Manager.Team_Extension__c);
                } else {
                    RestContext.response.responseBody = Blob.valueOf('260');
                }
            }
            RestContext.response.statusCode = 201;
        } else {
            RestContext.response.responseBody = Blob.valueOf('260');
            RestContext.response.statusCode = 201;
        }
    }
}