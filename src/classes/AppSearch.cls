@RestResource(urlMapping='/app/search')
global class AppSearch {
    @HttpGet
    global static ExtradomApi.SearchResult doGet() {
    
        if (UserInfo.getUserType()=='Guest') {
            RestContext.response.addHeader('Cache-Control', 'must-revalidate, max-age=0, s-maxage=21600');
        } else {
            RestContext.response.addHeader('Cache-Control', 'no-cache, must-revalidate, max-age=0, no-store, private, s-maxage=0');
        }
    
        ExtradomApi.SearchResult searchResult;
        
        String searchString = RestContext.request.params.get('q').replace('?','').replace('*','').normalizeSpace();
        searchString += '*';
        List<List<SObject>> results = [FIND :searchString.replace(' ','* ') IN NAME FIELDS RETURNING Design__c(Id, Code__c, Full_Name__c, Type__c, Gross_Price__c, Current_Price__c, Thumbnail_Base_URL__c, Usable_Area__c, Sketchfab_ID__c, KW_Model__c WHERE Status__c = 'Active' LIMIT 50), Page__c(Id, Name, API_URL__c WHERE Status__c = 'Active' AND Type__c = 'Collection' AND Subtype__c = 'Categories' AND API_URL__c != '' LIMIT 3)];
        if (results[0].size()>0) {
            List<Design__c> designResults = new List<Design__c>();
            for (Design__c d : (List<Design__c>)results[0]) {
                if (d.Full_Name__c == searchString.replace('*','') && designResults.size()<10) {
                    designResults.add(d);
                }
            }
            for (Design__c d : (List<Design__c>)results[0]) {
                if (d.Full_Name__c != searchString.replace('*','') && designResults.size()<10) {
                    designResults.add(d);
                }
            }
            results[0] = designResults;
            searchResult = new ExtradomApi.SearchResult(results[0], results[1]);
            return searchResult;
        }
        searchString = RestContext.request.params.get('q').replace('?','').replace('*','').normalizeSpace() + ' OR ' + searchString.replace(' ', ' OR ');
        results = [FIND :searchString IN NAME FIELDS RETURNING Design__c(Id, Code__c, Full_Name__c, Type__c, Gross_Price__c, Current_Price__c, Thumbnail_Base_URL__c, Usable_Area__c, Sketchfab_ID__c, KW_Model__c WHERE Status__c = 'Active' LIMIT 10), Page__c(Id, Name, API_URL__c WHERE Status__c = 'Active' AND Type__c = 'Collection' AND Subtype__c = 'Categories' AND API_URL__c != '' LIMIT 3)];
        
        searchResult = new ExtradomApi.SearchResult(results[0], results[1]);
        return searchResult;
    }
}