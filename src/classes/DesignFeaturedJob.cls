global without sharing class DesignFeaturedJob implements Schedulable {
    public void execute(SchedulableContext sc) {
        Database.executeBatch(new DesignFeaturedBatch());
    }
}