global class DesignUpdateSchedulerContext implements Schedulable {
    global DesignUpdateSchedulerContext() {}
    global void execute(SchedulableContext ctx) {
        DesignUpdateBatch b = new DesignUpdateBatch();
        database.executebatch(b, 1);
    }
}