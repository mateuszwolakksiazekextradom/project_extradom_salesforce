@IsTest
private class COKBonus_Test {
    
    @IsTest
    private static void shouldCalculateBOKBonus() {
        // given
        Date startDate = Date.today().toStartOfMonth();
        
        // when
        COKBonus.processOrderProducts(startDate.year(), startDate.month());
        
        // then
        String name = 'COK Bonus '+String.valueOf(startDate);
        Document doc = [SELECT Id, Name FROM Document WHERE FolderId = :UserInfo.getUserId() AND Name = :name];
        System.assertEquals(name, doc.Name);
    }
    
    @TestSetup
    private static void setupData() {
        Profile pf= [Select Id from profile where Name= :ConstUtils.USER_ADMIN_PROFILE];
        UserRole ur = [SELECT ID FROM UserRole WHERE Name = :ConstUtils.USER_DESIGN_SALES_AGENT_ROLE LIMIT 1];
        String username = 'TestName' + '@' + UserInfo.getUserName().substringAfter('@');
        User u = new User(
            FirstName = 'ABC', LastName = 'XYZ', email = username.substringBefore('@') + '@testorg.com', 
            Username = username, EmailEncodingKey = 'ISO-8859-1', 
            Alias = username.left(8), TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', 
            LanguageLocaleKey = 'en_US', ProfileId = pf.Id,  UserRoleId = ur.Id, COK_Target__c = 100
        ); 

        System.runAs(u) {
            Account acc = new Account (Name = 'Test Name');
            insert acc;
            Supplier__c newSupplier = new Supplier__c(Name = 'Supplier', Code__c = '11', Full_Name__c = 'Great Supplier');
            insert newSupplier;
                    
            Product2 designProduct = new Product2(
                    Name = 'Test Design Product', Family = ConstUtils.PRODUCT_DESIGN_FAMILY, IsActive = true, ProductCode = 'Design Product'
            );
            Product2 addOnProduct = new Product2(
                    Name = 'Test Add On Product', Family = ConstUtils.PRODUCT_EXTRA_FAMILY, IsActive = true, ProductCode = 'Add On Product'
            );
            Product2 giftProduct = new Product2(
                    Name = 'Test Gift Product', Family = ConstUtils.PRODUCT_GIFT_FAMILY, IsActive = true, ProductCode = 'Gift Product'
            );
            insert new List<Product2> {designProduct, addOnProduct, giftProduct};
            PricebookEntry designPriceBookEntry = new PricebookEntry(Product2Id = designProduct.Id, IsActive = true, Pricebook2Id = Test.getStandardPricebookId(), UnitPrice = 1000);
            PricebookEntry addOnPriceBookEntry = new PricebookEntry(Product2Id = addOnProduct.Id, IsActive = true, Pricebook2Id = Test.getStandardPricebookId(), UnitPrice = 100);
            PricebookEntry giftPriceBookEntry = new PricebookEntry(Product2Id = giftProduct.Id, IsActive = true, Pricebook2Id = Test.getStandardPricebookId(), UnitPrice = 150);
            insert new List<PricebookEntry> {designPriceBookEntry, addOnPriceBookEntry, giftPriceBookEntry};
            Order oThisMonth = new Order (
                AccountId = acc.Id, EffectiveDate = System.now().date(), Status = 'Draft', Pricebook2Id = Test.getStandardPricebookId(),
                RecordTypeId = ConstUtils.ORDER_REGULAR_RT_ID, PODate = System.now().date(), COK_Target__c = 100
            );
            Order oOld = new Order (
                AccountId = acc.Id, EffectiveDate = System.now().date().addDays(-40), Status = 'Draft', Pricebook2Id = Test.getStandardPricebookId(),
                RecordTypeId = ConstUtils.ORDER_REGULAR_RT_ID, PODate = System.now().date().addDays(-40), COK_Target__c = 100
            );
            List<Order> orders = new List<Order> {oThisMonth, oOld};
            insert orders;
    
            List<OrderItem> orderItems = new List<OrderItem> ();
            for (Order o : orders) {
                orderItems.add(new OrderItem(
                        PricebookEntryId = designPriceBookEntry.Id, Quantity = 2, Discount_Sales__c = 0, List_Price__c = 100,
                        OrderId = o.Id, UnitPrice = 0
                ));
                orderItems.add(new OrderItem(
                        PricebookEntryId = addOnPriceBookEntry.Id, Quantity = 1, Discount_Sales__c = 0, List_Price__c = 100,
                        OrderId = o.Id, UnitPrice = 0
                ));
                orderItems.add(new OrderItem(
                        PricebookEntryId = giftPriceBookEntry.Id, Quantity = 1, Discount_Sales__c = 0, List_Price__c = 100,
                        OrderId = o.Id, UnitPrice = 0
                ));
            }
            insert orderItems;
            
            for (Order o : orders) {
                o.Status = ConstUtils.ORDER_ACCEPTED_STATUS;
            }
            update orders;
            
            List<Order> reductionOrders = new List<Order>();
            
            Map<Id, Order> ordersMap = new Map<Id, Order>([
                SELECT Id, AccountId, EffectiveDate, PODate, Pricebook2Id, 
                    (
                        SELECT Id, AvailableQuantity, PricebookEntryId
                        FROM OrderItems WHERE PricebookEntry.Product2.Family = :ConstUtils.PRODUCT_DESIGN_FAMILY
                    )
                FROM Order
            ]);
            for (Order o : ordersMap.values()) {
                Order reductionOrder = new Order(IsReductionOrder = true, OriginalOrderId = o.Id, Pricebook2Id = o.Pricebook2Id,
                    EffectiveDate = o.EffectiveDate, Status = ConstUtils.ORDER_DRAFT_STATUS, AccountId = o.AccountId, PODate = o.PoDate,
                    COK_Target__c = 100
                );
                reductionOrders.add(reductionOrder);
            }
            insert reductionOrders;
            List<OrderItem> reductionItems = new List<OrderItem> ();
            for (Order reductionOrder : reductionOrders) {
                Order originalOrder = ordersMap.get(reductionOrder.OriginalOrderId);
                OrderItem oi = new OrderItem(
                    OriginalOrderItemId = originalOrder.OrderItems.get(0).Id, Quantity = -1,
                    PricebookEntryId = originalOrder.OrderItems.get(0).PricebookEntryId, OrderId = reductionOrder.Id
                );
                reductionItems.add(oi);
            }
            insert reductionItems;

            for (Order reductionOrder : reductionOrders) {
                reductionOrder.Status = ConstUtils.ORDER_ACCEPTED_STATUS;
            }
            update reductionOrders;
        }              
        
    }
}