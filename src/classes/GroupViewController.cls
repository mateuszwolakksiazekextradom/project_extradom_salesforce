public with sharing class GroupViewController {

    private final String groupId;
    
    public GroupViewController() {
        this.groupId = ApexPages.currentPage().getParameters().get('id');
    }
    
    public ConnectApi.FeedElementPage getFeedElementsFromFeed() {
        return ConnectApi.ChatterFeeds.getFeedElementsFromFeed(Network.getNetworkId(), ConnectApi.FeedType.Record, groupId, null, ConnectApi.FeedDensity.FewerUpdates, null, null, null);
    }
    
    public ConnectApi.ChatterGroupDetail getGroup() {
        return ConnectApi.ChatterGroups.getGroup(Network.getNetworkId(), groupId);
    }
    
    public ConnectApi.TopicPage getRecentlyTalkingAboutTopics() {
        return ConnectApi.Topics.getRecentlyTalkingAboutTopicsForGroup(Network.getNetworkId(), groupId);
    }
    
    public ConnectApi.FeedElement getFeaturedFeedElement() {
        Community_Home_Feed_Settings__c chfs = Community_Home_Feed_Settings__c.getInstance();
        try {
            if (String.isNotBlank(chfs.Featured_Feed_Item__c)) {
                return ConnectApi.ChatterFeeds.getFeedElement(Network.getNetworkId(), chfs.Featured_Feed_Item_Expert_Group__c);
            }
        } catch (Exception e) {
        }
        return null;
    }
    

}