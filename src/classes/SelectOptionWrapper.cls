public with sharing class SelectOptionWrapper {
    @AuraEnabled
    public String value;
    
    @AuraEnabled
    public String label;
    
    public SelectOptionWrapper(String value, String label) {
        this.value = value;
        this.label = label;
    }
}