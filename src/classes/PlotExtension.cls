global with sharing class PlotExtension {
    
    public PlotExtension() { }
    
    @RemoteAction
    global static Boolean requestBuildingConditionsAnalysisExtended(ID plotId, String description, Boolean optInRegulations, Boolean optInPrivacyPolicy, Boolean optInMarketing, String hasPlot, String hasMpzp, String constructionPlan, String hasApp, Boolean optInEmail, Boolean optInPhone, Boolean optInProfiling) {
        User u = [SELECT Id, Free_Plot_Analysis_Usage__c, Email FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        if (u.Free_Plot_Analysis_Usage__c && !Test.isRunningTest()) {
            return false;
        } else {
            u.Free_Plot_Analysis_Usage__c = true;
        }
        Plot__c p = [SELECT Id FROM Plot__c WHERE Id = :plotId LIMIT 1];
        p.Request_Building_Conditions_Analysis__c = true;
        p.Request_Description__c = description;
        update p;
        update u;
        Map<String,Id> data = OrderMethods.getWebLeadContactDetails('Plots', '', u.Email, u.Email, '', 'Internal', false, false, 'No', hasPlot, hasMpzp, constructionPlan, hasApp);
        OrderMethods.updateOptIns(u.Email, '', optInRegulations, optInPrivacyPolicy, optInMarketing, false, false, false, optInEmail, optInPhone, optInProfiling, false);
        return true;
    }
    
}