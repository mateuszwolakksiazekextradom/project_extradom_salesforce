public class AccountViewExtension {

    Account acc;
    public AccountViewExtension(ApexPages.StandardController stdController) {
        this.acc = (Account)stdController.getRecord();
    }
           
    public List<Order> getOrdersWithOrderItems() {
        return [select id, name, ordernumber, podate, status, isreductionorder, totalamount, total_amount__c, recordtype.name, (select id, orderitemnumber, pricebookentry.product2.name, quantity__c, unitprice, totalprice, version__c from orderitems) from order where accountid = :acc.Id ORDER BY EffectiveDate DESC];
    }
    
}