@RestResource(urlMapping='/app/filter/itemsets-options')
global class AppFilterItemSetsOptions {
    @HttpGet
    global static Options doGet() {
    
        Options options = new Options();
        String type = RestContext.request.params.get('type');
        options.type = type;
        options.url = '/app/filter/itemsets';
        
        
            options.departments = new List<Section>();
            Map<String,String> departmentsMap = new Map<String,String>{};
            Schema.DescribeFieldResult departmentFieldResult = Item__c.Department__c.getDescribe();
            List<Schema.PicklistEntry> departmentPicklistEntries = departmentFieldResult.getPicklistValues();
            for (Schema.PicklistEntry picklistEntry : departmentPicklistEntries) {
                departmentsMap.put(picklistEntry.getValue(),picklistEntry.getLabel());
            }
            Map<String,String> categoriesMap = new Map<String,String>{};
            Schema.DescribeFieldResult categoryFieldResult = Item__c.Category__c.getDescribe();
            List<Schema.PicklistEntry> categoryPicklistEntries = categoryFieldResult.getPicklistValues();
            for (Schema.PicklistEntry categoryPicklistEntry : categoryPicklistEntries) {
                departmentsMap.put(categoryPicklistEntry.getValue(),categoryPicklistEntry.getLabel());
            }
            
            Map<String,Map<String,Integer>> departmentsCategoriesTotalsMap = new Map<String,Map<String,Integer>>();
            for (AggregateResult ar : [SELECT Department__c, Category__c, count(Id) total FROM Item__c WHERE Category__c != '' AND Department__c != '' AND RecordType.Name = :type GROUP BY Department__c, Category__c]) {
                String department = (String)ar.get('Department__c');
                String category = (String)ar.get('Category__c');
                Integer total = (Integer)ar.get('total');
                if (!departmentsCategoriesTotalsMap.containsKey(department)) {
                    departmentsCategoriesTotalsMap.put(department,new Map<String,Integer>{});
                }
                departmentsCategoriesTotalsMap.get(department).put(category,total);
            }
            
            for (Schema.PicklistEntry picklistEntry : departmentPicklistEntries) {
                String departmentValue = picklistEntry.getValue();
                String departmentLabel = picklistEntry.getLabel();
                if (departmentsCategoriesTotalsMap.containsKey(departmentValue)) {
                    Map<String,Integer> categoriesTotalsMap = departmentsCategoriesTotalsMap.get(departmentValue);
                    Section department = new Section();
                    department.categories = new List<Category>{};
                    department.total = 0;
                    for (Schema.PicklistEntry categoryPicklistEntry : categoryPicklistEntries) {
                        String categoryValue = categoryPicklistEntry.getValue();
                        String categoryLabel = categoryPicklistEntry.getLabel();
                        if (categoriesTotalsMap.containsKey(categoryValue)) {
                            Integer categoryTotal = categoriesTotalsMap.get(categoryValue);
                            Category category = new Category();
                            category.total = categoryTotal;
                            department.total += categoryTotal;
                            category.label = categoryLabel;
                            category.value = categoryValue;
                            department.categories.add(category);
                        }
                    }
                    department.label = departmentLabel;
                    department.value = departmentValue;
                    department.icon = new ExtradomApi.Image();
                    department.icon.vectorImageUrl = 'https://static.extradom.pl/icons/items/departments/'+department.value+'.svg';
                    options.departments.add(department);
                }
            }
            
            options.locations = new List<Section>{};
            Schema.DescribeFieldResult locationsFieldResult = Item__c.Locations__c.getDescribe();
            List<Schema.PicklistEntry> locationsPicklistEntries = locationsFieldResult.getPicklistValues();
            for (Schema.PicklistEntry locationPicklistEntry : locationsPicklistEntries) {
            
                Section location = new Section();
                location.categories = new List<Category>{};
                location.value = locationPicklistEntry.getValue();
                location.label = locationPicklistEntry.getLabel();
                location.icon = new ExtradomApi.Image();
                location.icon.vectorImageUrl = 'https://static.extradom.pl/icons/items/locations/'+location.value+'.svg';
                Map<String,Integer> categoriesTotalsMap = new Map<String,Integer>();
                for (AggregateResult ar : (List<AggregateResult>)Database.query('SELECT Category__c, count(Id) total FROM Item__c WHERE Category__c != \'\' AND RecordType.Name = :type AND Locations__c includes (\''+location.value+'\') GROUP BY Category__c')) {
                    String category = (String)ar.get('Category__c');
                    Integer total = (Integer)ar.get('total');
                    categoriesTotalsMap.put(category,total);
                }
                
                Schema.DescribeFieldResult locationCategoryFieldResult = Item__c.Category__c.getDescribe();
                List<Schema.PicklistEntry> locationCategoryPicklistEntries = locationCategoryFieldResult.getPicklistValues();
                for (Schema.PicklistEntry locationCategoryPicklistEntry : locationCategoryPicklistEntries) {
                    String categoryValue = locationCategoryPicklistEntry.getValue();
                    String categoryLabel = locationCategoryPicklistEntry.getLabel();
                    if (categoriesTotalsMap.containsKey(categoryValue)) {
                        Integer categoryTotal = categoriesTotalsMap.get(categoryValue);
                        Category category = new Category();
                        category.total = categoryTotal;
                        category.label = categoryLabel;
                        category.value = categoryValue;
                        location.categories.add(category);
                    }
                }
                
                if (location.categories.size()>0) {
                    options.locations.add(location);
                }
                
            }
            
        
            options.filters = new List<Filter>{};
            
            FilterMultiselect filterStyles = new FilterMultiselect();
            filterStyles.label = 'Styl';
            filterStyles.name = 'styles';
            filterStyles.options = new List<FilterOption>{};
            Schema.DescribeFieldResult stylesFieldResult = Item__c.Styles__c.getDescribe();
            List<Schema.PicklistEntry> stylesPicklistEntries = stylesFieldResult.getPicklistValues();
            for (Schema.PicklistEntry stylePicklistEntry : stylesPicklistEntries) {
                FilterOption filterOption = new FilterOption();
                filterOption.label = stylePicklistEntry.getLabel();
                filterOption.value = stylePicklistEntry.getValue();
                filterStyles.options.add(filterOption);
            }
            options.filters.add(filterStyles);
            
            FilterMultiselect filterColors = new FilterMultiselect();
            filterColors.label = 'Kolor';
            filterColors.name = 'colors';
            filterColors.options = new List<FilterOption>{};
            Schema.DescribeFieldResult colorsFieldResult = Item_Color__c.Colors__c.getDescribe();
            List<Schema.PicklistEntry> colorsPicklistEntries = colorsFieldResult.getPicklistValues();
            for (Schema.PicklistEntry colorPicklistEntry : colorsPicklistEntries) {
                FilterOption filterOption = new FilterOption();
                filterOption.label = colorPicklistEntry.getLabel();
                filterOption.value = colorPicklistEntry.getValue();
                filtercolors.options.add(filterOption);
            }
            options.filters.add(filterColors);
            
            FilterMultiselect filterMaterials = new FilterMultiselect();
            filterMaterials.label = 'Materiał';
            filterMaterials.name = 'materials';
            filterMaterials.options = new List<FilterOption>{};
            Schema.DescribeFieldResult materialsFieldResult = Item_Color__c.Materials__c.getDescribe();
            List<Schema.PicklistEntry> materialsPicklistEntries = materialsFieldResult.getPicklistValues();
            for (Schema.PicklistEntry materialPicklistEntry : materialsPicklistEntries) {
                FilterOption filterOption = new FilterOption();
                filterOption.label = materialPicklistEntry.getLabel();
                filterOption.value = materialPicklistEntry.getValue();
                filtermaterials.options.add(filterOption);
            }
            options.filters.add(filterMaterials);
        
        return options;
    }
    
    public enum FilterType { multiselect }
    public abstract class Filter {
        public String label;
        public FilterType type;
    }
    public class FilterMultiselect extends Filter {
        public String name;
        public List<FilterOption> options;
        public FilterMultiselect() {
            this.type = FilterType.multiselect;
        }
    }
    public virtual class FilterOption {
        public String label;
        public String value;
        public ExtradomApi.Image icon;
    }
    public virtual class Category extends FilterOption {
        public Integer total;
    }
    public class Section extends Category {
        public List<Category> categories;
    }
    global class Options {
        public String url;
        public String type;
        public List<Section> departments;
        public List<Section> locations;
        public List<Filter> filters;
    }
    
}