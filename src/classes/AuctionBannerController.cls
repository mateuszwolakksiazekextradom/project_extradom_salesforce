/**
 * Created by grzegorz.dlugosz on 28.05.2019.
 */

public without sharing class AuctionBannerController {

    @AuraEnabled
    public static Offer__c getAuctionInfoSrv(Id auctionId) {
        Offer__c result;

        List<Offer__c> offerList = [
            SELECT Name, Minimum_Value__c, End_Date__c, Rules__c
            FROM Offer__c
            WHERE Id = :auctionId
        ];

        if (!offerList.isEmpty()) {
            result = offerList.get(0);
        }

        return result;
    }
}