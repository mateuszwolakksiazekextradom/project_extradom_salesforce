@isTest
public class NotusToolsTest {    
    public static testmethod void testNotusToolsSchedule() {
        
        SingleRequestMock fakeResponse = new SingleRequestMock(200,'Complete','{"Stage": "test","statusSystemName": "test"}',null);              
        List<Telemarketing__c> scope = new List<Telemarketing__c>{new Telemarketing__c(Name='DK Notus',Post_Conversion_Stage__c='Waiting',Notus_Id__c='Test')};        
        insert scope;       
        NotusToolsSchedule nt = new NotusToolsSchedule();
        Database.BatchableContext bc;
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, fakeResponse);       
        nt.execute(bc,scope);
        nt.finish(bc);
        System.schedule('my job', '0 0 13 * * ?', new NotusToolsSchedule());       
        Test.stopTest();
    }
    public static testmethod void testNotusToolsQueue() {
        
        SingleRequestMock fakeResponse = new SingleRequestMock(200,'Complete','{"Stage": "test","statusSystemName": "test"}',null);              
        List<Telemarketing__c> scope = new List<Telemarketing__c>{new Telemarketing__c(Name='DK Notus',Post_Conversion_Stage__c='Waiting',Notus_Id__c='Test')};        
        insert scope;       
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, fakeResponse);       
        System.enqueueJob(new NotusToolsQueue(scope));      
        Test.stopTest();
    }
    public static testmethod void testNotusToolsTrigger() {
        
        SingleRequestMock fakeResponse = new SingleRequestMock(200,'Complete','{"Stage": "test","statusSystemName": "test"}',null);              
        Telemarketing__c tel = new Telemarketing__c(Name='DK Notus',Post_Conversion_Stage__c='Waiting');        
        insert tel;       
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        tel.Post_Conversion_Stage__c = 'In Progress';
        update tel;            
        Test.stopTest();
    }
}