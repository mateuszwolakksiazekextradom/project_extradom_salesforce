@RestResource(urlMapping='/sketchesestimatesrequest')
global class SketchesEstimatesRequestRest {
    @HttpPost
    global static Map<String,String> doPost(String email, String phone, List<ID> designids, Boolean sketches, Boolean estimates, Boolean materials, Boolean optInRegulations, Boolean optInPrivacyPolicy, Boolean optInMarketing, Boolean optInMarketingExtended, Boolean optInMarketingPartner, Boolean optInLeadForward, String gclid, String hasPlot, String hasMpzp, String constructionPlan, String hasApp,  Boolean optInEmail, Boolean optInPhone, Boolean optInProfiling, Boolean optInPKO) {
        Map<String,Id> data = OrderMethods.getWebLeadContactDetails('Sketches & Estimates', '', email, email, phone, 'Internal', false, false, 'No',hasPlot, hasMpzp, constructionPlan, hasApp);
        RestRequest req = RestContext.request;
        if (req.headers.containsKey('gaClientId')) {
            OrderMethods.updateGaClientId(data.get('accountId'), req.headers.get('gaClientId'));
        }
        OrderMethods.updateOptIns(email, phone, optInRegulations, optInPrivacyPolicy, optInMarketing, optInMarketingExtended, optInMarketingPartner, optInLeadForward, optInEmail, optInPhone, optInProfiling, optInPKO);
        OrderMethods.markSalesReadyFromResources(data.get('accountId'), constructionPlan);
        List<Task> tasks = new List<Task>{};
        for (Design__c d : [SELECT Id, Show_Sketches__c, Show_Estimates__c, Supplier__r.Has_Materials__c, Documents_Materials__c FROM Design__c WHERE Id IN :designids]) {
            if (sketches) {
                tasks.add(new Task(Subject='Zamówienie rysunków szczegółowych', Status=d.Show_Sketches__c?'Completed':'Not Started', Type='Question', WhoId=data.get('contactEmailId'), WhatId=d.Id, OwnerId=data.get('ownerId'), ActivityDate=System.Today()));
            }
            if (estimates) {
                tasks.add(new Task(Subject='Zamówienie kosztorysu', Status=d.Show_Estimates__c?'Completed':'Not Started', Type='Estimates Request', WhoId=data.get('contactEmailId'), WhatId=d.Id, OwnerId=data.get('ownerId'), ActivityDate=System.Today()));
            }
            if (materials) {
                tasks.add(new Task(Subject='Zamówienie zestawienia materiałów', Status=String.isNotBlank(d.Documents_Materials__c)?'Completed':(d.Supplier__r.Has_Materials__c?'Not Started':'Canceled'), Type='Materials Request', WhoId=data.get('contactEmailId'), WhatId=d.Id, OwnerId=data.get('ownerId'), ActivityDate=System.Today()));
            }
        }
        insert tasks;
        Map<String,String> result = new Map<String,String>{'result' => 'Dziękujemy za zainteresowanie. Materiały do wybranych projektów zostaną przesłane na podany adres poczty e-mail.'};
        return result;
    }
    global class OverLimitOrderException extends Exception {}
}