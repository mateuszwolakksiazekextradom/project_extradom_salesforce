public class SupplierExtension {

    public String templateName {get; set;}
    public String attachmentName {get; set;}
    public String attachmentPageName {get; set;}
    
    Supplier__c supp;
    private ApexPages.StandardSetController stdSetController;
        public SupplierExtension(ApexPages.StandardSetController stdSetController) {
        this.stdSetController = stdSetController;
    }
    public SupplierExtension(ApexPages.StandardController stdController) {
        this.supp = (Supplier__c)stdController.getRecord();
    }
           
    public List<Design__c> getIdeaDesigns() {
        return [SELECT Id, Name FROM Design__c WHERE Supplier__c = :supp.Id AND Stage__c = 'Idea' AND Status__c = 'Active'];
    }
    
    public PageReference sendIdeaEmails() {
        List<Supplier__c> selectedSuppliers = (List<Supplier__c>) stdSetController.getSelected();        
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage>();
        List<EmailTemplate> et = [SELECT Id FROM EmailTemplate WHERE Name='Pracownie - koncepcje' LIMIT 1];
        List<OrgWideEmailAddress> owea = [SELECT Id FROM OrgWideEmailAddress WHERE Address = 'pracownie@extradom.pl' LIMIT 1];
              
        for (Supplier__c supplier : selectedSuppliers) {
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.toAddresses = new String[] {supplier.Email__c};
            message.setTemplateId (et[0].Id);
            message.setTargetObjectId(UserInfo.getUserId());
            message.setSaveAsActivity(true);
            message.setTreatTargetObjectAsRecipient(false);
            message.setOrgWideEmailAddressId(owea[0].Id);            
                                      
            PageReference pdf = Page.SupplierIdeaAttachment;
            pdf.getParameters().put('id',supplier.Id);
                        
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            Blob b = pdf.getContent();
            efa.setFileName('koncepcje.pdf');
            efa.setBody(b);
            message.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
            
            messages.add(message);
        }
        
        Messaging.sendEmail(messages);
        return stdSetController.save();
    }
}