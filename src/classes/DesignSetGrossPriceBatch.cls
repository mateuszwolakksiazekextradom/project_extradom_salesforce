public with sharing class DesignSetGrossPriceBatch implements Database.Batchable<sObject> {
    public Database.querylocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
                SELECT Id, Pending_Gross_Price__c, Gross_Price__c, Gross_Price_inc__c, Pending_Gross_Price_Start_Date__c
                FROM Design__c
                WHERE Pending_Gross_Price_Start_Date__c <= :system.now() AND Pending_Gross_Price_Start_Date__c != null
                    AND Pending_Gross_Price__c != null
        ]);
    }

    public void execute(Database.BatchableContext BC, List<sObject> scope) {
        List<Design__c> designs = (List<Design__c> ) scope;
        for (Design__c design : designs) {
            design.Gross_Price__c = design.Pending_Gross_Price__c;
            design.Gross_Price_inc__c = design.Pending_Gross_Price__c;
            design.Pending_Gross_Price__c = null;
            design.Pending_Gross_Price_Start_Date__c = null;
        }
        DesignTriggerHandler.BYPASS_INVALIDATE_DESIGNS = true;
        update designs;
        DesignTriggerHandler.BYPASS_INVALIDATE_DESIGNS = false;
    }

    public void finish(Database.BatchableContext BC) {
    }
}