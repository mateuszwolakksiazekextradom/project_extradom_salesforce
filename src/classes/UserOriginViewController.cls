public with sharing class UserOriginViewController {

    private final String userId;
    
    public UserOriginViewController() {
        if (ApexPages.currentPage().getHeaders().containsKey('Origin')) {
            String origin = ApexPages.currentPage().getHeaders().get('Origin');
            for (User u : [SELECT Id FROM User WHERE Referer__c = :origin AND Referer__c != '' LIMIT 1]) {
                this.userId = u.Id;
            }
        }
        if (String.isBlank(this.userId)) {
            this.userId = ApexPages.currentPage().getParameters().get('id');
        }
    }
    
    public ConnectApi.UserDetail getUser() {
        return ConnectApi.ChatterUsers.getUser(Network.getNetworkId(), userId);
    }
    
    public User getSFUser() {
        return [SELECT Id, Partner_Type__c, Location__latitude__s, Location__longitude__s, VAT_No__c, REGON__c, KRS__c, IBAN__c, fixedSearchParams__c, Feed_News__c, Feed_Offer__c, Feed_About__c, Tab_Loans__c, Hide_Author__c FROM User WHERE Id = :userId];
    }
    
    public List<ConnectApi.FeedElement> getTopContentPosts() {
        List<Id> ids = ChatterMethods.topUserProfileContentPostIds(this.userId);
        List<ConnectApi.FeedElement> feedElements = new List<ConnectApi.FeedElement>{};
        ConnectApi.BatchResult[] batchResults = ConnectApi.ChatterFeeds.getFeedElementBatch(Network.getNetworkId(), ids);
        for (ConnectApi.BatchResult batchResult : batchResults) {
            if (batchResult.isSuccess()) {
                ConnectApi.FeedElement element;
                if (batchResult.getResult() instanceof ConnectApi.FeedElement) {
                    element = (ConnectApi.FeedElement) batchResult.getResult();
                    feedElements.add(element);
                }
            }
        }
        return feedElements;
    }
    
    public List<ConnectApi.FeedElement> getContentPosts() {
        List<Id> ids = ChatterMethods.userProfileContentPostIds(this.userId);
        List<ConnectApi.FeedElement> feedElements = new List<ConnectApi.FeedElement>{};
        ConnectApi.BatchResult[] batchResults = ConnectApi.ChatterFeeds.getFeedElementBatch(Network.getNetworkId(), ids);
        for (ConnectApi.BatchResult batchResult : batchResults) {
            if (batchResult.isSuccess()) {
                ConnectApi.FeedElement element;
                if (batchResult.getResult() instanceof ConnectApi.FeedElement) {
                    element = (ConnectApi.FeedElement) batchResult.getResult();
                    feedElements.add(element);
                }
            }
        }
        return feedElements;
    }
    
    public Integer getCountContentPosts() {
        return ChatterMethods.countUserProfileContentPosts(this.userId);
    }
    
    public List<ConnectApi.FeedElement> getSFUserNews() {
        return ChatterMethods.getFeedElementsFromStringList(this.getSFUser().Feed_News__c);
    }
    
    public List<ConnectApi.FeedElement> getSFUserOffer() {
        return ChatterMethods.getFeedElementsFromStringList(this.getSFUser().Feed_Offer__c);
    }
    
    public List<ConnectApi.FeedElement> getSFUserAbout() {
        return ChatterMethods.getFeedElementsFromStringList(this.getSFUser().Feed_About__c);
    }
    
    public List<Design__c> getDesignResults() {
        return [SELECT Full_Name__c, Canonical_URL__c, Thumbnail_Base_URL__c, Code__c FROM Design__c WHERE type_1__c = 1 AND Status__c = 'Active' ORDER BY Order_Score__c DESC NULLS LAST LIMIT 6];
    }
    
    public Integer getCountDesignResults() {
        return [SELECT count() FROM Design__c WHERE type_1__c = 1 AND Status__c = 'Active'];
    }

}