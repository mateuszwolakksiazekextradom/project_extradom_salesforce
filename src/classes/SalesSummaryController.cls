public without sharing class SalesSummaryController {

    public SalesSummaryController() {}
    
    private static TV__c tv = TV__c.getInstance();
    public Decimal cokTarget {get {return tv.COK_Target__c;}}
    
    public List<AggregateResult> getUnansweredCalls() {
        return [SELECT Owner.Alias alias, COUNT_DISTINCT(AccountId) unansweredCalls FROM Task WHERE Type = 'Unanswered Call' AND IsClosed = false AND CreatedDate = LAST_N_DAYS:3 AND (NOT Subject LIKE '%na numer 81%') AND (NOT Subject LIKE '%na numer 06%') AND (NOT Subject LIKE '%na numer 08%') AND (NOT Subject LIKE '%na numer 90%') AND (NOT Subject LIKE '%na numer 61%') AND (NOT Subject LIKE '%na numer 98%') AND (NOT Subject LIKE '%na numer 89%') AND (NOT Subject LIKE '%na numer 87%') AND (NOT Subject LIKE '%na numer 77%') AND (NOT Subject LIKE '%na numer 85%') AND (NOT Subject LIKE '%na numer 83%') AND OwnerId NOT IN (SELECT UserOrGroupId FROM GroupMember WHERE GroupId = '00G0N000004fbd0UAA') AND OwnerId NOT IN (SELECT Id FROM User WHERE Department = 'B2B') GROUP BY Owner.Alias ORDER BY COUNT_DISTINCT(AccountId) DESC];
    }
    
    public List<AggregateResult> getCallRequests() {
        return [SELECT Owner.Alias alias, COUNT_DISTINCT(AccountId) callRequests FROM Task WHERE Type = 'Call Request' AND IsClosed = false AND CreatedDate = LAST_N_DAYS:3 AND Owner.Type = 'User' AND Owner.UserRole.Name = 'Design Sales Agent' GROUP BY Owner.Alias ORDER BY COUNT_DISTINCT(AccountId) DESC];
    }
    
    public List<AggregateResult> getDraftOrders() {
        return [SELECT Owner.Alias alias, COUNT_DISTINCT(Id) draftOrders FROM Order WHERE Status IN ('','Draft') AND IsReductionOrder=false AND Has_Catalogues_Only__c = false AND Owner.Type = 'User' AND Owner.UserRole.Name = 'Design Sales Agent' GROUP BY Owner.Alias ORDER BY COUNT_DISTINCT(Id) DESC];
    }
    
    public List<AggregateResult> getTodaysFollowUpCalls() {
        return [SELECT Owner.Alias alias, COUNT_DISTINCT(AccountId) followUpCalls FROM Task WHERE Type = 'Follow Up Call' AND IsClosed = false AND ActivityDate = TODAY AND Account.Has_Contact_with_Phone__c = true AND Account.Sales_Has_Design__c IN ('','No') AND OwnerId IN (SELECT Id FROM User WHERE Is_Live__c = true AND UserRole.Name = 'Design Sales Agent') GROUP BY Owner.Alias ORDER BY COUNT_DISTINCT(AccountId) DESC];
    }
    
    public Integer getTodaysOutboundCalls() {
        return [SELECT COUNT() FROM Task WHERE CallType = 'Outbound' AND CallDurationInSeconds >= 25 AND ActivityDate = TODAY AND IsClosed = true AND Owner.UserRole.Name = 'Design Sales Agent'];
    }
    
    public Integer getTodaysInboundCalls() {
        return [SELECT COUNT() FROM Task WHERE CallType = 'Inbound' AND ActivityDate = TODAY AND IsClosed = true AND Owner.UserRole.Name = 'Design Sales Agent'];
    }
    
    public List<AggregateResult> getThisMonthsSales() {
        // nowe
        return [SELECT Order.Owner.Alias alias, SUM(Quantity) quantity, Sum(Total_Sales_Billing_Price__c) price, MAX(Order.Order_Owner_COK_Target__c) target FROM OrderItem WHERE Order.Owner.Type = 'User' AND Order.Owner.UserRole.Name = 'Design Sales Agent' AND Order.PODate = THIS_MONTH AND PricebookEntry.Product2.Family IN ('Plan Set','Add-On','Misc') AND (Order.Is_Reduction_Month_Order_Month__c = True OR Order.isReductionOrder = false) AND (Order.StatusCode = 'A' OR (Order.Is_Cancel_Month_Order_Month__c = false AND Order.Cancel_Date__c != null)) GROUP BY Order.Owner.Alias ORDER BY SUM(Quantity) DESC];
    }
    
    public List<AggregateResult> getTodaysSales() {
        // nowe
        return [SELECT Order.Owner.Alias alias, SUM(AvailableQuantity) quantity, SUM(Total_Sales_Billing_Price__c) price FROM OrderItem WHERE Order.Owner.Type = 'User' AND Order.IsReductionOrder=false AND Order.StatusCode = 'A' AND Order.Owner.UserRole.Name = 'Design Sales Agent' AND Order.PODate = TODAY AND PricebookEntry.Product2.Family IN ('Plan Set','Add-On','Misc') GROUP BY Order.Owner.Alias ORDER BY SUM(AvailableQuantity) DESC];
    }
    
    public Integer getTodaysSalesTotal() {
        // nowe
        List<AggregateResult> results = [SELECT SUM(Count_Plan_Sets__c) quantity FROM Order WHERE Owner.Type = 'User' AND IsReductionOrder=false AND ActivatedDate!=null AND Owner.UserRole.Name = 'Design Sales Agent' AND PODate = TODAY];
        try {
            return ((decimal)results[0].get('quantity')).intValue();
        } catch(Exception e) {
        }
        return 0;
    }
    
    public Double getTodaysSalesTotalPrice() {
        // nowe
        List<AggregateResult> results = [SELECT SUM(Total_Sales_Billing_Price__c) price FROM OrderItem WHERE Order.Owner.Type = 'User' AND Order.Owner.UserRole.Name = 'Design Sales Agent' AND Order.IsReductionOrder=false AND AvailableQuantity > 0 AND Order.PODate = TODAY AND Order.StatusCode = 'A' AND PricebookEntry.Product2.Family IN ('Plan Set','Add-On','Misc')];
        try {
            return (decimal)results[0].get('price');
        } catch(Exception e) {
        }
        return 0;
    }
    
    public Integer getTodaysSalesDealerTotal() {
        // nowe
        List<AggregateResult> results = [SELECT SUM(Count_Plan_Sets__c) dealerQuantity FROM Order WHERE Owner.Type = 'User' AND IsReductionOrder=false AND ActivatedDate!=null AND Owner.UserRole.Name != 'Design Sales Agent' AND PODate = TODAY];
        try {
            return ((decimal)results[0].get('dealerQuantity')).intValue();
        } catch(Exception e) {
        }
        return 0;
    }
    
    public Integer getThisMonthsSalesTotal() {
        // nowe
        List<AggregateResult> results = [SELECT SUM(Count_Plan_Sets__c) quantity FROM Order WHERE Owner.Type = 'User' AND IsReductionOrder=false AND ActivatedDate!=null AND Owner.UserRole.Name = 'Design Sales Agent' AND PODate = THIS_MONTH];
        try {
            return ((decimal)results[0].get('quantity')).intValue();
        } catch(Exception e) {
        }
        return 0;
    }
    
    public Double getThisMonthsSalesTotalPrice() {
        // nowe
        List<AggregateResult> results = [SELECT SUM(Total_Sales_Billing_Price__c) price FROM OrderItem WHERE Order.Owner.Type = 'User' AND Order.Owner.UserRole.Name = 'Design Sales Agent' AND Order.PODate = THIS_MONTH AND PricebookEntry.Product2.Family IN ('Plan Set','Add-On','Misc') AND (Order.Is_Reduction_Month_Order_Month__c = True OR Order.isReductionOrder = false) AND (Order.StatusCode = 'A' OR (Order.Is_Cancel_Month_Order_Month__c = false AND Order.Cancel_Date__c != null))];
        try {
            return (decimal)results[0].get('price');
        } catch(Exception e) {
        }
        return 0;
    }
    
    public Integer getThisMonthsSalesDealerTotal() {
        // nowe
        List<AggregateResult> results = [SELECT SUM(Count_Plan_Sets__c) dealerQuantity FROM Order WHERE Owner.Type = 'User' AND Owner.UserRole.Name != 'Design Sales Agent' AND IsReductionOrder=false AND ActivatedDate!=null AND PODate = THIS_MONTH];
        try {
            return ((decimal)results[0].get('dealerQuantity')).intValue();
        } catch(Exception e) {
        }
        return 0;
    }
    
    public List<AggregateResult> getDraftOrdersTotal() {
        // nowe
        return [SELECT Count_Distinct(Id) quantity FROM Order WHERE Status IN ('','Draft') AND IsReductionOrder=false AND Owner.Type = 'User' AND Order.Owner.UserRole.Name = 'Design Sales Agent'];
    }
    
    public List<Order__c> getLastOrderedDraftDesign() {
        return [SELECT Id, Name, CreatedDate, Sales_Agent_Alias__c, Quantity__c, Design__r.Name, Design__r.Thumbnail_Base_URL__c FROM Order__c WHERE Status__c IN ('','Draft') AND Quantity__c > 0 AND RecordType.Name != 'Product' AND Design__c != null AND Gross_Price__c > 0 AND CreatedDate >= :Datetime.now().addMinutes(-3) ORDER BY CreatedDate DESC LIMIT 1];
    }
    
}