global class DesignUpdateScheduler {
   
    public static void start(){
        //Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
        System.schedule('Pending Design Update', '0 0 15 * * ?', New DesignUpdateSchedulerContext());
    }
    
}