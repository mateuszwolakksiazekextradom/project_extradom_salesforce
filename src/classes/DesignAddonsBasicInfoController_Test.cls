@IsTest
private class DesignAddonsBasicInfoController_Test{

    @IsTest
    private static void shouldReturnAddonsData() {
        // given
        Supplier__c s = new Supplier__c (Name = 'Test Name', Code__c = 'TST', Full_Name__c = 'Test Name TST');
        insert s;
        Design__c d = new Design__c (Name = 'Test Name', Supplier__c = s.Id, Code__c = 'TST1001', Full_Name__c = 'Test Name TST1001');
        insert d;
        Product2 p = new Product2 (Name = 'Kosztorys', Family = 'Add-on', Design__c = d.Id);
        insert p;
        PricebookEntry pe = new PricebookEntry (UnitPrice = 100, Regular_Price__c = 100, Product2Id = p.Id, Pricebook2Id = Test.getStandardPricebookId());
        insert pe;

        // when
        DesignAddonsBasicInfoController ctrl = new DesignAddonsBasicInfoController();
        ctrl.setdesignId(d.Id);
        List<Product2> addons = ctrl.addons;
        String designId = ctrl.getDesignId();
    }
}