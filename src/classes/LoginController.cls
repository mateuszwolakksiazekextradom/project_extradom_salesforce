global without sharing class LoginController {

    public String username { get; set; }
    public String recoveryUsername { get; set; }
    public String password { get; set; }
    public String passwordRecoveryStatus { get; set; }
    
    private String startUrl;
    private final String source;

    global LoginController() {
        this.source = (System.currentPageReference().getParameters().get('source')!=null) ? System.currentPageReference().getParameters().get('source') : 'Community';
        if (System.currentPageReference().getParameters().get('startURL')!=null) {
            this.startUrl = System.currentPageReference().getParameters().get('startURL');
        } else if (System.currentPageReference().getHeaders().get('Referer')!=null && System.currentPageReference().getHeaders().get('Host')!=null) {
            String referer = System.currentPageReference().getHeaders().get('Referer').replaceFirst('(http|https)://', '');
            if (referer.startsWith(System.currentPageReference().getHeaders().get('Host'))) {
                String relativeRef = referer.replaceFirst(System.currentPageReference().getHeaders().get('Host'), '');
                if (!(relativeRef.startsWith('/LoginView') || relativeRef.startsWith('/RegisterView') || relativeRef.startsWith('/secur/logout.jsp'))) {
                    this.startUrl = relativeRef;
                }
            }
        }
        this.startUrl = this.startUrl==null?'/':this.startUrl;
    }
    
    public String getActiveStartUrl() {
        return this.startUrl;
    }
    
    public String getActiveSource() {
        return this.source;
    }
    
    public String getReferer() {
        return System.currentPageReference().getHeaders().get('Referer');
    }
    
    global PageReference loginInline() {
        return Site.login(this.username, this.password, this.startUrl);
    }
    
    global PageReference autoLogin() {
        this.username = System.currentPageReference().getParameters().get('username');
        this.password = System.currentPageReference().getParameters().get('password');
        this.startUrl = System.currentPageReference().getParameters().get('startURL');
        PageReference pr = Site.login(this.username, this.password, this.startUrl);
        if (pr == null) {
            pr = new PageReference('/logininline?status=failed');
            pr.setAnchor('error_message=Nieprawid%C5%82owe%20has%C5%82o');
        }
        return pr;
    }
    
    global PageReference passwordRecovery() {
        Site.forgotPassword(this.recoveryUsername);
        return null;
    }
    
    global PageReference autoPasswordRecovery() {
        this.username = System.currentPageReference().getParameters().get('username');
        if (!Site.isValidUsername(this.username)) {
            this.passwordRecoveryStatus = 'Podany e-mail jest nieprawidłowy';
            return null;
        }
        for (User u : [SELECT Id FROM User WHERE Username = :this.username AND UserType = 'CspLitePortal' LIMIT 1]) {
            this.passwordRecoveryStatus = 'Instrukcja resetu hasła została wysłana';
            Site.forgotPassword(this.username);
            return null;
        }
        this.passwordRecoveryStatus = 'Podany e-mail nie jest powiązany z żadnym kontem';
        return null;
    }
    
}