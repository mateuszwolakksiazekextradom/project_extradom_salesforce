/**
 * Created by grzegorz.dlugosz on 23.05.2019.
 */
public without sharing class AuctionListController {

    /**
     * @author Grzegorz Długosz
     * @description Returns all bids connected to given auction (offer__c Id)
     * @param auctionId
     *        - Id of Offer__c record
     * @return List of Bid__c records under given offer__c. If auction is in progress, then all bids except the ones
     * that are assigned to user will have 'Inna Pracownia' in NazwaPracowni__c field. After the auction is complete,
     * it will return all correct NazwaPracowni__c values.
     */
    @AuraEnabled
    public static List<Bid__c> getBidListSrv(Id auctionId) {
        List<Bid__c> bidList = [
                SELECT Value__c, Pracownia__c, NazwaPracowni__c, LastModifiedDate, OwnerId, Offer__r.End_Date__c, CreatedDate
                FROM Bid__c
                WHERE Offer__c = :auctionId
                ORDER BY Value__c DESC NULLS LAST, LastModifiedDate ASC NULLS LAST
        ];

        if (bidList != null && !bidList.isEmpty()) {
            if (bidList.get(0).Offer__r.End_Date__c != null && System.now() < bidList.get(0).Offer__r.End_Date__c) {
                for (Bid__c bid : bidList) {
                    if (bid.OwnerId != UserInfo.getUserId()) {
                        bid.NazwaPracowni__c = 'Inna pracownia';
                    }
                }
            }
        }

        return bidList;
    }
}