public with sharing class COKBonus {
    public static void processOrderProducts(Integer year, Integer month) {
        Date startDate = Date.newInstance(year, month, 1);
        Date endDate = startDate.addMonths(1);
        String csv = '';
        Map<Id,Decimal> totals = new Map<Id,Decimal>{};
        for (User u : [SELECT Id FROM User WHERE UserRole.Name = 'Design Sales Agent']) {
            totals.put(u.Id, 0.0);
        }
        List<OrderItem> oisToUpdate = new List<OrderItem>();
        for (OrderItem oi : [SELECT Id, Sales_Billing_Price__c, Quantity, Order.OwnerId, Order.COK_Target__c, (SELECT Quantity FROM ChildOrderItems WHERE (Order.PODate >= :startDate AND Order.PODate < :endDate) AND Order.StatusCode = 'A') FROM OrderItem WHERE (Order.PODate >= :startDate AND Order.PODate < :endDate) AND Order.Owner.UserRole.Name = 'Design Sales Agent' AND Order.IsReductionOrder = false AND PriceBookEntry.Product2.Family IN ('Plan Set', 'Add-On', 'Misc') AND (Order.StatusCode = 'A' OR (Order.Status = 'Canceled' AND Order.Cancel_Date__c >= :endDate)) ORDER BY Order.PODate, Order.CreatedDate]){
            ID ownerId = oi.Order.OwnerId;
            ID oId = oi.Id;
            Decimal target = oi.Order.COK_Target__c;
            Decimal total = totals.get(ownerId);
            Decimal quantity = oi.Quantity;
            for (OrderItem childOi: oi.ChildOrderItems) {
                quantity += childOi.Quantity;
            }
            if (quantity > 0) {
                Decimal base = oi.Sales_Billing_Price__c*quantity;
                Decimal bonus1baseleft = Math.max(target*0.7-total,0.0);
                Decimal bonus2baseleft = Math.max(target-total-bonus1baseleft,0.0);
                Decimal bonus1base = Math.min(base,bonus1baseleft);
                Decimal bonus2base = Math.min(base-bonus1base,bonus2baseleft);
                Decimal bonus3base = base-bonus1base-bonus2base;
                Decimal bonus = (bonus1base*0.007+bonus2base*0.01+bonus3base*0.012)/quantity;
                oisToUpdate.add(new OrderItem(Id=oId,COK_Bonus__c=bonus));
                csv += oid + ',' + String.valueOf(bonus) + '\n';
                totals.put(ownerId,total+base);
            }
        }
        System.debug(totals);
        Blob b = Blob.valueOf(csv);
        Document attach1= new Document();
        attach1.FolderId = UserInfo.getUserId();
        attach1.Name = 'COK Bonus '+String.valueOf(startDate);
        attach1.Body = b;
        insert attach1;
    }
}