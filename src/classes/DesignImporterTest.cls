@isTest
private class DesignImporterTest {

    static testMethod void importXMLDataTest() {
        
        List<String> expectedLog = new List<String>();
        
        Supplier__c s = new Supplier__c(Name='Test Supplier ABC', Full_Name__c='Test Supplier', Code__c='ABC');
        insert s;
        expectedLog.add('supplier: ABC');
        
        Design__c d1 = new Design__c(Name='Test Design', Full_Name__c='Test Design', Supplier__c=s.Id);
        insert d1;
        
        Design__c d2 = new Design__c(Name='Test Design 0', Full_Name__c='Test Design 0', Supplier__c=s.Id, External_Web_ID__c='abc: 0');
        insert d2;
        
        expectedLog.add('nameIds: 2');
        expectedLog.add('webIds: 1');
        
        Attachment a = new Attachment(Name='import-data.xml', ParentId=s.Id);
        String body = '<importer>';
        body += '<Item />';
        expectedLog.add('[bad row]');
        
        body += '<Item><id></id><fullName></fullName></Item>';
        expectedLog.add('[bad row]');
        
        body += '<Item><id>1</id><fullName></fullName></Item>';
        expectedLog.add('[bad row]');
        
        body += '<Item><id></id><fullName>Test Design</fullName></Item>';
        expectedLog.add('[correct row][webId "" not found][nameId "abc: test design" found][fullNameInc changed "null" > "Test Design"]');
        
        body += '<Item><id></id><fullName>test design</fullName></Item>';
        expectedLog.add('[correct row][webId "" not found][nameId "abc: test design" found][fullNameInc changed "Test Design" > "test design"]');
        
        body += '<Item><id>1</id><fullName>Test Design</fullName></Item>';
        expectedLog.add('[correct row][webId "abc: 1" not found][nameId "abc: test design" found][new webId added][fullNameInc changed "test design" > "Test Design"]');
        
        body += '<Item><id>2</id><fullName>Test Design 2</fullName></Item>';
        expectedLog.add('[correct row][webId "abc: 2" not found][nameId "abc: test design 2" not found][new design added][new nameId added][new webId added]');
        
        body += '<Item><id>2</id><fullName>Test Design 2</fullName></Item>';
        expectedLog.add('[correct row][webId "abc: 2" found as "abc: test design 2"][nameId "abc: test design 2" omitted]');
        
        body += '<Item><id></id><fullName>Test Design 2</fullName></Item>';
        expectedLog.add('[correct row][webId "" not found][nameId "abc: test design 2" found]');
        
        body += '<Item><id>2</id><fullName>Test Design 2</fullName></Item>';
        expectedLog.add('[correct row][webId "abc: 2" found as "abc: test design 2"][nameId "abc: test design 2" omitted]');
        
        body += '<Item><id>1</id><fullName>Test Design ver. A</fullName></Item>';
        expectedLog.add('[correct row][webId "abc: 1" found as "abc: test design"][nameId "abc: test design ver. a" omitted][fullNameInc changed "Test Design" > "Test Design ver. A"]');
        
        body += '<Item><id></id><fullName>Test Design 3</fullName></Item>';
        expectedLog.add('[correct row][webId "" not found][nameId "abc: test design 3" not found][new design added][new nameId added]');
        
        body += '<Item><id></id><fullName>Test Design 3</fullName></Item>';
        expectedLog.add('[correct row][webId "" not found][nameId "abc: test design 3" found]');
        
        body += '<Item><id>3</id><fullName>Test Design 3</fullName></Item>';
        expectedLog.add('[correct row][webId "abc: 3" not found][nameId "abc: test design 3" found][new webId added]');
        
        body += '<Item><id>3</id><fullName>Test Design 3</fullName></Item>';
        expectedLog.add('[correct row][webId "abc: 3" found as "abc: test design 3"][nameId "abc: test design 3" omitted]');
        
        
        body += '<Item><id>1</id><fullName>Test Design ver. A</fullName><usableArea>120.55</usableArea></Item>';
        expectedLog.add('[correct row][webId "abc: 1" found as "abc: test design"][nameId "abc: test design ver. a" omitted][usableAreaInc changed "null" > "120.55"]');
        
        body += '<Item><id></id><fullName>Test Design</fullName><usableArea>120.55</usableArea></Item>';
        expectedLog.add('[correct row][webId "" not found][nameId "abc: test design" found][fullNameInc changed "Test Design ver. A" > "Test Design"]');
        
        body += '<Item><id>2</id><fullName>Test Design 2 ver. A</fullName><usableArea>92</usableArea></Item>';
        expectedLog.add('[correct row][webId "abc: 2" found as "abc: test design 2"][nameId "abc: test design 2 ver. a" omitted][fullNameInc changed "Test Design 2" > "Test Design 2 ver. A"][usableArea set "92.0"]');
        
        body += '<Item><id>2</id><fullName>Test Design 2 ver. A</fullName><usableArea>92</usableArea></Item>';
        expectedLog.add('[correct row][webId "abc: 2" found as "abc: test design 2"][nameId "abc: test design 2 ver. a" omitted]');
        
        body += '<Item><id>2</id><fullName>Test Design 2 ver. A</fullName><usableArea>92.1</usableArea></Item>';
        expectedLog.add('[correct row][webId "abc: 2" found as "abc: test design 2"][nameId "abc: test design 2 ver. a" omitted][usableArea set "92.1"]');
        
        
        body += '<Item><id></id><fullName>Test Design</fullName><usableArea>120.55</usableArea><capacity>360.25</capacity><footprint>98.14</footprint><plotH>16.23</plotH><plotV>20.05</plotV><height>8.22</height><roofSlope>30</roofSlope><constructionPricingEstimate>212000</constructionPricingEstimate><euco>32.94</euco><roofCoverArea>150.65</roofCoverArea><price>2500</price><promoPrice>2350</promoPrice></Item>';
        expectedLog.add('[correct row][webId "" not found][nameId "abc: test design" found][capacityInc changed "null" > "360.25"][footprintInc changed "null" > "98.14"][plotHInc changed "null" > "16.23"][plotVInc changed "null" > "20.05"][heightInc changed "null" > "8.22"][roofSlopeInc changed "null" > "30.0"][constructionPricingEstimateInc changed "null" > "212000.0"][eucoInc changed "null" > "32.94"][roofCoverAreaInc changed "null" > "150.65"][priceInc changed "null" > "2500.0"][promoPriceInc changed "null" > "2350.0"]');
        
        body += '<Item><id></id><fullName>Test Design</fullName><usableArea>120.55</usableArea><capacity>360.25</capacity><footprint>98.14</footprint><plotH>16.23</plotH><plotV>20.05</plotV><height>8.22</height><roofSlope>30</roofSlope><constructionPricingEstimate>212000</constructionPricingEstimate><euco>32.94</euco><roofCoverArea>150.65</roofCoverArea><price>2500</price><promoPrice>2350</promoPrice></Item>';
        expectedLog.add('[correct row][webId "" not found][nameId "abc: test design" found]');
        
        body += '<Item><id></id><fullName>Test Design</fullName><description>Test Design is a lovely house that... Test Design is a lovely house that... Test Design is a lovely house that... Test Design is a lovely house that... Test Design is a lovely house that... Test Design is a lovely house that... Test Design is a lovely house that... Test Design is a lovely house that... Test Design is a lovely house that... Test Design is a lovely house that... Test Design is a lovely house that...</description></Item>';
        expectedLog.add('[correct row][webId "" not found][nameId "abc: test design" found][descriptionInc changed "null" > "Test Design is a lovely house that... Test Design is a lovely house that... Test Design is a lovely house that... Test Design is a lovely house that... Test Design is a lovely house that... Test Design is a lovely house that... Test Design is a lovely house that... Test Design is a lovely house that... Test Design is a lovely house that... Test Design is a lovely house that... Test Design is a lovely house that..."]');
        
        body += '<Item><id></id><fullName>Test Design</fullName><description>Test Design is a lovely house that... Test Design is a lovely house that... Test Design is a lovely house that... Test Design is a lovely house that... Test Design is a lovely house that... Test Design is a lovely house that... Test Design is a lovely house that... Test Design is a lovely house that... Test Design is a lovely house that... Test Design is a lovely house that... Test Design is a lovely house that...</description></Item>';
        expectedLog.add('[correct row][webId "" not found][nameId "abc: test design" found]');
        
        body += '<Item><id></id><fullName>Test Design</fullName><technology>Technology of Test Design... Technology of Test Design... Technology of Test Design... Technology of Test Design... Technology of Test Design... Technology of Test Design... Technology of Test Design... Technology of Test Design... Technology of Test Design... Technology of Test Design...</technology></Item>';
        expectedLog.add('[correct row][webId "" not found][nameId "abc: test design" found][technologyInc changed "null" > "Technology of Test Design... Technology of Test Design... Technology of Test Design... Technology of Test Design... Technology of Test Design... Technology of Test Design... Technology of Test Design... Technology of Test Design... Technology of Test Design... Technology of Test Design..."]');
        
        body += '<Item><id></id><fullName>Test Design</fullName><technology>Technology of Test Design... Technology of Test Design... Technology of Test Design... Technology of Test Design... Technology of Test Design... Technology of Test Design... Technology of Test Design... Technology of Test Design... Technology of Test Design... Technology of Test Design...</technology></Item>';
        expectedLog.add('[correct row][webId "" not found][nameId "abc: test design" found]');
        
        body += '<Item><id></id><fullName>Test Design</fullName><ceiling>terriva</ceiling></Item>';
        expectedLog.add('[correct row][webId "" not found][nameId "abc: test design" found][ceilingInc changed "null" > "terriva"]');
        
        body += '<Item><id></id><fullName>Test Design</fullName><ceiling>terriva</ceiling></Item>';
        expectedLog.add('[correct row][webId "" not found][nameId "abc: test design" found]');
        
        body += '<Item><id></id><fullName>Test Design</fullName><ItemID>1</ItemID><category>Attic Plan</category><N>01</N><R>Taras</R><U>12.11</U><V>12.6</V></Item>';
        expectedLog.add('[correct row][webId "" not found][nameId "abc: test design" found]');
        
        body += '<Item><id></id><fullName><![CDATA[Test & Design]]></fullName><category>Outdoor</category><ListItemIndex>1</ListItemIndex><mediaUrl>http://extradom.pl/</mediaUrl></Item>';
        expectedLog.add('[correct row][webId "" not found][nameId "abc: test & design" not found][new design added][new nameId added]');
        
        
        body += '</importer>';
        a.Body = Blob.valueOf(body);
        insert a;
        
        expectedLog.add('processed: 4');
        expectedLog.add('changed: 1');
        expectedLog.add('created: 3');
        expectedLog.add('processed unchanged: 0');
        
        List<String> log = DesignImporter.importXMLData(a, true);
        System.assertEquals(expectedLog.size(), log.size());
        for (Integer i=0; i<log.size(); i++) {
        	System.assertEquals(i+':'+expectedLog[i],i+':'+log[i]);
        }
        
        DesignImporter.import(a.Id);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(a);
		DesignImporterController ext = new DesignImporterController(sc);
		ext.checkImport();
        
    }
}