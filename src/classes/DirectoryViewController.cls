public with sharing class DirectoryViewController {

    public String col {get;set;}
    public Map<String,String> pattern {get;set;}
    
    public DirectoryViewController() {
        try {
            this.col = ApexPages.currentPage().getParameters().get('col');
        } catch(Exception e) {
            this.col = '';
        }
        pattern = new Map<String,String>{};
        pattern.put('a', 'A');
        pattern.put('a-2', 'Ą');
        pattern.put('b', 'B');
        pattern.put('c', 'C');
        pattern.put('c-2', 'Ć');
        pattern.put('d', 'D');
        pattern.put('e', 'E');
        pattern.put('e-2', 'Ę');
        pattern.put('f', 'F');
        pattern.put('g', 'G');
        pattern.put('h', 'H');
        pattern.put('i', 'I');
        pattern.put('j', 'J');
        pattern.put('k', 'K');
        pattern.put('l', 'L');
        pattern.put('l-2', 'Ł');
        pattern.put('m', 'M');
        pattern.put('n', 'N');
        pattern.put('n-2', 'Ń');
        pattern.put('o', 'O');
        pattern.put('o-2', 'Ó');
        pattern.put('p', 'P');
        pattern.put('q', 'Q');
        pattern.put('r', 'R');
        pattern.put('s', 'S');
        pattern.put('s-2', 'Ś');
        pattern.put('t', 'T');
        pattern.put('u', 'U');
        pattern.put('v', 'V');
        pattern.put('w', 'W');
        pattern.put('x', 'X');
        pattern.put('y', 'Y');
        pattern.put('z', 'Z');
        pattern.put('z-2', 'Ż');
        pattern.put('z-3', 'Ź');
        pattern.put('pozostale', 'Cyfry i inne znaki');
    }
    
    public List<Design__c> getDesigns() {
        if (this.col == 'pozostale') {
            return [SELECT Id, Name FROM Design__c WHERE Status__c IN ('Active', 'Shadow', 'Rejected') AND (NOT Name LIKE 'a%') AND (NOT Name LIKE 'ą%') AND (NOT Name LIKE 'b%') AND (NOT Name LIKE 'c%') AND (NOT Name LIKE 'ć%') AND (NOT Name LIKE 'd%') AND (NOT Name LIKE 'e%') AND (NOT Name LIKE 'ę%') AND (NOT Name LIKE 'f%') AND (NOT Name LIKE 'g%') AND (NOT Name LIKE 'h%') AND (NOT Name LIKE 'i%') AND (NOT Name LIKE 'j%') AND (NOT Name LIKE 'k%') AND (NOT Name LIKE 'l%') AND (NOT Name LIKE 'ł%') AND (NOT Name LIKE 'm%') AND (NOT Name LIKE 'n%') AND (NOT Name LIKE 'ń%') AND (NOT Name LIKE 'o%') AND (NOT Name LIKE 'ó%') AND (NOT Name LIKE 'p%') AND (NOT Name LIKE 'q%') AND (NOT Name LIKE 'r%') AND (NOT Name LIKE 's%') AND (NOT Name LIKE 'ś%') AND (NOT Name LIKE 't%') AND (NOT Name LIKE 'u%') AND (NOT Name LIKE 'v%') AND (NOT Name LIKE 'w%') AND (NOT Name LIKE 'x%') AND (NOT Name LIKE 'y%') AND (NOT Name LIKE 'z%') AND (NOT Name LIKE 'ż%') AND (NOT Name LIKE 'ź%') ORDER BY Name LIMIT 5000];
        } else if (this.pattern.containsKey(this.col)) {
            return [SELECT Id, Name FROM Design__c WHERE Status__c IN ('Active', 'Shadow', 'Rejected') AND Name LIKE :pattern.get(this.col)+'%' ORDER BY Name LIMIT 5000];
        } else {
            return new List<Design__c>{};
        }
    }
    
    public List<User> getUsers() {
        if (this.col == 'pozostale') {
            return [SELECT Id, CommunityNickname FROM User WHERE IsActive = true AND (NOT CommunityNickname LIKE 'a%') AND (NOT CommunityNickname LIKE 'ą%') AND (NOT CommunityNickname LIKE 'b%') AND (NOT CommunityNickname LIKE 'c%') AND (NOT CommunityNickname LIKE 'ć%') AND (NOT CommunityNickname LIKE 'd%') AND (NOT CommunityNickname LIKE 'e%') AND (NOT CommunityNickname LIKE 'ę%') AND (NOT CommunityNickname LIKE 'f%') AND (NOT CommunityNickname LIKE 'g%') AND (NOT CommunityNickname LIKE 'h%') AND (NOT CommunityNickname LIKE 'i%') AND (NOT CommunityNickname LIKE 'j%') AND (NOT CommunityNickname LIKE 'k%') AND (NOT CommunityNickname LIKE 'l%') AND (NOT CommunityNickname LIKE 'ł%') AND (NOT CommunityNickname LIKE 'm%') AND (NOT CommunityNickname LIKE 'n%') AND (NOT CommunityNickname LIKE 'ń%') AND (NOT CommunityNickname LIKE 'o%') AND (NOT CommunityNickname LIKE 'ó%') AND (NOT CommunityNickname LIKE 'p%') AND (NOT CommunityNickname LIKE 'q%') AND (NOT CommunityNickname LIKE 'r%') AND (NOT CommunityNickname LIKE 's%') AND (NOT CommunityNickname LIKE 'ś%') AND (NOT CommunityNickname LIKE 't%') AND (NOT CommunityNickname LIKE 'u%') AND (NOT CommunityNickname LIKE 'v%') AND (NOT CommunityNickname LIKE 'w%') AND (NOT CommunityNickname LIKE 'x%') AND (NOT CommunityNickname LIKE 'y%') AND (NOT CommunityNickname LIKE 'z%') AND (NOT CommunityNickname LIKE 'ż%') AND (NOT CommunityNickname LIKE 'ź%') ORDER BY CommunityNickname LIMIT 5000];
        } else if (this.pattern.containsKey(this.col)) {
            return [SELECT Id, CommunityNickname FROM User WHERE IsActive = true AND CommunityNickname LIKE :pattern.get(this.col)+'%' ORDER BY CommunityNickname LIMIT 5000];
        } else {
            return new List<User>{};
        }
    }
    
}