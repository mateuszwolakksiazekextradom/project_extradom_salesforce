public with sharing class OrderUtils {

    public static Map<Id, OrderItem> getExistingOrderItems(List<OrderItem> orderItems) {
        Map<Id, OrderItem> orderItemsMap = new Map<Id, OrderItem> ();
        for (OrderItem oi : orderItems) {
            if (String.isNotBlank(oi.Id)) {
                orderItemsMap.put(oi.Id, oi);
            }
        }
        return orderItemsMap;
    }
    
    public static List<OrderItem> getNewOrderItems(List<OrderItem> orderItems) {
        List<OrderItem> orderItemsList = new List<OrderItem> ();
        for (OrderItem oi : orderItems) {
            if (String.isBlank(oi.Id)) {
                orderItemsList.add(oi);
            }
        }
        return orderItemsList;
    }
    
    public static List<OrderItem> getDeleteOrderItems(List<OrderItem> orderItems, Map<Id, OrderItem> newOrderItems) {
        List<OrderItem> orderItemsList = new List<OrderItem> ();
        if (orderItems != null && orderItems.size() > 0) {
            for (OrderItem oi : orderItems) {
                if (!newOrderItems.containsKey(oi.Id)) {
                    orderItemsList.add(oi);
                }
            }
        }
        return orderItemsList;
    }

    public static String getPricebookId (String pricebookId) {
        String pbId = pricebookId;
        if (String.isBlank(pbId)) {
            List<PriceBook2> pricebooks = [SELECT Id FROM PriceBook2 WHERE IsActive=true AND IsStandard=true];
            if (pricebooks.size() > 0) {
                pbId = pricebooks.get(0).Id;
            }
        }
        if (Test.isRunningTest() && pbId == null) pbId = Test.getStandardPricebookId();
        return pbId;
    }
    
    public static String saveOrder(ExtradomApi.DesignOrderInput input) {
        return new OrderUtilsWithoutSharing().createOrderWithOrderItems(input);
    }

    public static Map<String, PricebookEntry> getProductsByCodes(Set<String> codes) {
        Map<String, PricebookEntry> productMap = new Map<String, PricebookEntry> ();
        if (codes != null && codes.size() > 0) {
            List<PricebookEntry> pricebookEntries = [
                SELECT Id, UnitPrice, Product2.ProductCode, Product2.Design__c, Product2.Design__r.Type__c FROM PricebookEntry
                WHERE Product2.ProductCode in :codes
            ];
            for (PricebookEntry pe : pricebookEntries) {
                productMap.put(pe.Product2.ProductCode, pe);
            }
        }
        return productMap;
    }

    public static void calculateAccountOrderItems(Set<String> accountIds) {
        new OrderUtilsWithoutSharing().calculateOrderItemsOnAccount(accountIds);
    }
    
    private without sharing class OrderUtilsWithoutSharing {
        private String createOrderWithOrderItems(ExtradomApi.DesignOrderInput input) {
            SavePoint sp = Database.setSavepoint();
            String orderId = null;
            try {                
                Order o = createOrder(input);
                createOrderItems(o, input);
                orderId = o.Id;
            } catch (Exception e) {
                Database.rollback(sp);
                throw new OrderUtilsException(e.getMessage());
            }
            return orderId;
        }
        
        private Order createOrder(ExtradomApi.DesignOrderInput input) {
            Map<String, Decimal> shippingCosts = Utils.getExternalShippingCosts();
            Order o = new Order();
            o.OrderSource__c = input.source;
            o.Pricebook2Id = getPricebookId(null);
            o.EffectiveDate = system.now().date();
            o.Status = ConstUtils.ORDER_DRAFT_STATUS;
            o.BillingFirstName__c = input.billingAddress.firstName;
            o.BillingLastName__c = input.billingAddress.lastName;
            o.BillingStreet = getStreet(input.billingAddress);
            o.BillingPostalCode = input.billingAddress.zip;
            o.BillingCity = input.billingAddress.city;
            o.BillingEmail__c = input.billingAddress.email;
            o.BillingPhone__c = input.billingAddress.phone;
            o.BillingCompany__c = input.billingAddress.company;
            o.BillingTaxNumber__c = input.billingAddress.vatNumber;
            o.Version__c = input.version;
            o.Code__c = input.code;
            if (input.shippingAddress != null) {
                o.ShippingFirstName__c = input.shippingAddress.firstName;
                o.ShippingLastName__c = input.shippingAddress.lastName;
                o.ShippingStreet = getStreet(input.shippingAddress);
                o.ShippingPostalCode = input.shippingAddress.zip;
                o.ShippingCity = input.shippingAddress.city;
                o.ShippingPhone__c = input.shippingAddress.phone;
            }
            o.Shipping_Method__c = input.shippingMethod;
            o.Payment_Method__c = input.paymentMethod;
            if (String.isNotBlank(o.Payment_Method__c) && String.isNotBlank(o.Shipping_Method__c)) {
                if (shippingCosts.containsKey(o.Shipping_Method__c + o.Payment_Method__c)) {
                    o.Shipping_Cost__c = shippingCosts.get(o.Shipping_Method__c + o.Payment_Method__c);
                }
            }
            
            Boolean optInRegulations = false;
            Boolean optInPrivacyPolicy = false;
            Boolean optInMarketing = false;
            Boolean optInMarketingExtended = false;
            Boolean optInMarketingPartner = false;
            Boolean optInLeadForward = false;
            Boolean optInRegulationsAndPrivacyPolicy = false;
            Boolean optInEmail = false;
            Boolean optInPhone = false;
            Boolean optInProfiling = false;
            Boolean optInPKO = false;
            for (ExtradomApi.OptInInput optInInput : input.optIns) {
                if (optInInput.name == ConstUtils.ORDER_OPT_IN_REGULATIONS_PARAM) {
                    optInRegulations = optInInput.value;
                }
                if (optInInput.name == ConstUtils.ORDER_OPT_IN_PRIVACY_POLICY_PARAM) {
                    optInPrivacyPolicy = optInInput.value;
                }
                if (optInInput.name == ConstUtils.ORDER_OPT_IN_MARKETING_PARAM) {
                    optInMarketing = optInInput.value;
                }
                if (optInInput.name == ConstUtils.ORDER_OPT_IN_MARKETING_EXTENDED_PARAM) {
                    optInMarketingExtended = optInInput.value;
                }
                if (optInInput.name == ConstUtils.ORDER_OPT_IN_MARKETING_PARTNER_PARAM) {
                    optInMarketingPartner = optInInput.value;
                }
                if (optInInput.name == ConstUtils.ORDER_OPT_IN_LEAD_FORWARD_PARAM) {
                    optInLeadForward = optInInput.value;
                }
                if (optInInput.name == ConstUtils.ORDER_OPT_IN_REGULATIONS_AND_PRIVACY_POLICY_PARAM) {
                    optInRegulationsAndPrivacyPolicy = optInInput.value;
                }
                if (optInInput.name == ConstUtils.ORDER_OPT_IN_EMAIL_PARAM) {
                    optInEmail = optInInput.value;
                }
                if (optInInput.name == ConstUtils.ORDER_OPT_IN_PHONE_PARAM) {
                    optInPhone = optInInput.value;
                }
                if (optInInput.name == ConstUtils.ORDER_OPT_IN_PROFILING_PARAM) {
                    optInProfiling = optInInput.value;
                }
                if (optInInput.name == ConstUtils.ORDER_OPT_IN_PKO_PARAM) {
                    optInPKO = optInInput.value;
                }
            }
            o.Opt_In_Regulations__c = optInRegulations || optInRegulationsAndPrivacyPolicy;
            o.Opt_In_Privacy_Policy__c = optInPrivacyPolicy || optInRegulationsAndPrivacyPolicy;
            o.Opt_In_Marketing__c = optInMarketing;
            o.Opt_In_Marketing_Extended__c = optInMarketingExtended;
            o.Opt_In_Marketing_Partner__c = optInMarketingPartner;
            o.Opt_In_Lead_Forward__c = optInLeadForward;
            o.Opt_In_Email__c = optInEmail;
            o.Opt_In_Phone__c = optInPhone;
            o.Opt_In_Profiling__c = optInProfiling;
            o.Opt_In_PKO__c = optInPKO;
            o.AccountId = OrderMethods.getAccountId(
                input.billingAddress.firstName, input.billingAddress.lastName,
                input.billingAddress.email, input.billingAddress.phone, input.source,
                'Design', optInMarketing, optInMarketing, 'No'
            );
            OrderMethods.updateOptIns(input.billingAddress.email, input.billingAddress.phone, optInRegulations, optInPrivacyPolicy, optInMarketing, optInMarketingExtended, optInMarketingPartner, optInLeadForward, optInEmail, optInPhone, optInProfiling, optInPKO);
            o.OwnerId = getOwner(o.AccountId);
            Account acc = [SELECT Id, Type FROM Account WHERE Id = :o.AccountId];
            if (acc.Type=='Investor') {
                o.Connect_Opportunity__c = true;
            }
            insert o;
            return o;
        }
        
        private void createOrderItems(Order o, ExtradomApi.DesignOrderInput input) {
            Set<String> productCodes = new Set<String> ();
            productCodes.add(input.code);
            for (ExtradomApi.AddOnInput addOnInput : input.addOns) {
                if (String.isNotBlank(addOnInput.name)) {
                    productCodes.add(addOnInput.name);
                }
            }
            Map<String, PricebookEntry> productMap = getProductsByCodes(productCodes);
            PricebookEntry projectProduct = productMap.get(input.code);
            
            List<OrderItem> orderItems = new List<OrderItem> ();
            OrderItem oi = new OrderItem (
                Design__c = projectProduct.Product2.Design__c, Quantity = 1, Version__c = input.version,
                OrderId = o.Id, Changes_Agreement__c = input.comments, PricebookEntryId = projectProduct.Id,
                List_Price__c = projectProduct.UnitPrice, UnitPrice = projectProduct.UnitPrice
            );
            orderItems.add(oi);

            for (ExtradomApi.AddOnInput addOnInput : input.addOns) {
                if (productMap.containsKey(addOnInput.name)) {
                    PricebookEntry pe = productMap.get(addOnInput.name);
                    orderItems.add(new OrderItem(
                        PricebookEntryId = pe.Id, Quantity = 1, UnitPrice = pe.UnitPrice, List_Price__c = pe.UnitPrice, OrderId = o.Id
                    ));
                }
            }
            insert orderItems;
        }

        private String getStreet(ExtradomApi.AddressInput ai) {
            String street = ai.streetName;
            street += String.isNotBlank(ai.streetNumber) ? ' ' + ai.streetNumber : '';
            street += String.isNotBlank(ai.streetNumber) && String.isNotBlank(ai.streetFlatNumber)
                    ? ' /' : '';
            street += String.isNotBlank(ai.streetFlatNumber) ? ' ' + ai.streetFlatNumber : '';
            return street;
        }
        
        private String getOwner(String accountId) {
            String ownerId = null;
            if (String.isNotBlank(accountId)) {
                List<Account> accounts = [SELECT OwnerId FROM Account WHERE Id = :accountId];
                if (accounts.size() > 0) {
                    ownerId = accounts.get(0).OwnerId;
                }
            }
            return ownerId;
        }

        private void calculateOrderItemsOnAccount(Set<String> accountIds) {
            if (accountIds.size() > 0) {
                List<Account> accounts = [
                        SELECT Id, Upsell_Products__c, Count_Plan_Sets__c,
                        (
                                SELECT Id, Quantity, PricebookEntry.Product2.Family FROM Order_Products__r
                                WHERE (PricebookEntry.Product2.Family = :ConstUtils.PRODUCT_UPSELL_FAMILY
                                OR PricebookEntry.Product2.Family = :ConstUtils.PRODUCT_DESIGN_FAMILY)
                                AND Order.StatusCode = :ConstUtils.ORDER_ACTIVATED_STATUS_CODE
                        )
                        FROM Account WHERE Id in :accountIds
                ];
                List<Account> accountsToUpdate = new List<Account> ();
                for (Account a : accounts) {
                    Integer numOfUpsellProducts = 0;
                    Integer numOfPlanSetsProducts = 0;
                    Boolean accountModified = false;
                    for (OrderItem oi : a.Order_Products__r) {
                        if (oi.PricebookEntry.Product2.Family == ConstUtils.PRODUCT_UPSELL_FAMILY) {
                            numOfUpsellProducts += oi.Quantity != null ? Integer.valueOf(oi.Quantity) : 0;
                        } else if (oi.PricebookEntry.Product2.Family == ConstUtils.PRODUCT_DESIGN_FAMILY) {
                            numOfPlanSetsProducts += oi.Quantity != null ? Integer.valueOf(oi.Quantity) : 0;
                        }
                    }
                    if (a.Upsell_Products__c != numOfUpsellProducts) {
                        a.Upsell_Products__c = numOfUpsellProducts;
                        accountModified = true;
                    }
                    if (a.Count_Plan_Sets__c != numOfPlanSetsProducts) {
                        a.Count_Plan_Sets__c = numOfPlanSetsProducts;
                        accountModified = true;
                    }
                    if (accountModified) {
                        accountsToUpdate.add(a);
                    }
                }
                update accountsToUpdate;
            }
        }
    }
    public class OrderUtilsException extends Exception { }
}