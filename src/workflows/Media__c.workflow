<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Design_Count_Bathrooms_1F</fullName>
        <field>Count_Bathrooms_1st_Floor__c</field>
        <formula>IF(R1__c=&apos;łazienka&apos;,1,0)+IF(R2__c=&apos;łazienka&apos;,1,0)+IF(R3__c=&apos;łazienka&apos;,1,0)+IF(R4__c=&apos;łazienka&apos;,1,0)+IF(R5__c=&apos;łazienka&apos;,1,0)+IF(R6__c=&apos;łazienka&apos;,1,0)+IF(R7__c=&apos;łazienka&apos;,1,0)+IF(R8__c=&apos;łazienka&apos;,1,0)+IF(R9__c=&apos;łazienka&apos;,1,0)+IF(R10__c=&apos;łazienka&apos;,1,0)+IF(R11__c=&apos;łazienka&apos;,1,0)+IF(R12__c=&apos;łazienka&apos;,1,0)+IF(R13__c=&apos;łazienka&apos;,1,0)+IF(R14__c=&apos;łazienka&apos;,1,0)+IF(R15__c=&apos;łazienka&apos;,1,0)+IF(R16__c=&apos;łazienka&apos;,1,0)+IF(R17__c=&apos;łazienka&apos;,1,0)+IF(R18__c=&apos;łazienka&apos;,1,0)+IF(R19__c=&apos;łazienka&apos;,1,0)+IF(R20__c=&apos;łazienka&apos;,1,0)+IF(R21__c=&apos;łazienka&apos;,1,0)+IF(R22__c=&apos;łazienka&apos;,1,0)+IF(R23__c=&apos;łazienka&apos;,1,0)+IF(R24__c=&apos;łazienka&apos;,1,0)+IF(R25__c=&apos;łazienka&apos;,1,0)+IF(R26__c=&apos;łazienka&apos;,1,0)+IF(R27__c=&apos;łazienka&apos;,1,0)+IF(R28__c=&apos;łazienka&apos;,1,0)+IF(R29__c=&apos;łazienka&apos;,1,0)+IF(R30__c=&apos;łazienka&apos;,1,0)+IF(R31__c=&apos;łazienka&apos;,1,0)+IF(R32__c=&apos;łazienka&apos;,1,0)+IF(R33__c=&apos;łazienka&apos;,1,0)+IF(R34__c=&apos;łazienka&apos;,1,0)+IF(R35__c=&apos;łazienka&apos;,1,0)+IF(R36__c=&apos;łazienka&apos;,1,0)+IF(R37__c=&apos;łazienka&apos;,1,0)+IF(R38__c=&apos;łazienka&apos;,1,0)+IF(R39__c=&apos;łazienka&apos;,1,0)+IF(R40__c=&apos;łazienka&apos;,1,0)+IF(R41__c=&apos;łazienka&apos;,1,0)+IF(R42__c=&apos;łazienka&apos;,1,0)+IF(R43__c=&apos;łazienka&apos;,1,0)+IF(R44__c=&apos;łazienka&apos;,1,0)+IF(R45__c=&apos;łazienka&apos;,1,0)+IF(R46__c=&apos;łazienka&apos;,1,0)+IF(R47__c=&apos;łazienka&apos;,1,0)+IF(R48__c=&apos;łazienka&apos;,1,0)+IF(R49__c=&apos;łazienka&apos;,1,0)+IF(R50__c=&apos;łazienka&apos;,1,0)</formula>
        <name>Design: Count Bathrooms - 1F</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Design__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Count_Bathrooms_AT</fullName>
        <field>Count_Bathrooms_Attic__c</field>
        <formula>IF(R1__c=&apos;łazienka&apos;,1,0)+IF(R2__c=&apos;łazienka&apos;,1,0)+IF(R3__c=&apos;łazienka&apos;,1,0)+IF(R4__c=&apos;łazienka&apos;,1,0)+IF(R5__c=&apos;łazienka&apos;,1,0)+IF(R6__c=&apos;łazienka&apos;,1,0)+IF(R7__c=&apos;łazienka&apos;,1,0)+IF(R8__c=&apos;łazienka&apos;,1,0)+IF(R9__c=&apos;łazienka&apos;,1,0)+IF(R10__c=&apos;łazienka&apos;,1,0)+IF(R11__c=&apos;łazienka&apos;,1,0)+IF(R12__c=&apos;łazienka&apos;,1,0)+IF(R13__c=&apos;łazienka&apos;,1,0)+IF(R14__c=&apos;łazienka&apos;,1,0)+IF(R15__c=&apos;łazienka&apos;,1,0)+IF(R16__c=&apos;łazienka&apos;,1,0)+IF(R17__c=&apos;łazienka&apos;,1,0)+IF(R18__c=&apos;łazienka&apos;,1,0)+IF(R19__c=&apos;łazienka&apos;,1,0)+IF(R20__c=&apos;łazienka&apos;,1,0)+IF(R21__c=&apos;łazienka&apos;,1,0)+IF(R22__c=&apos;łazienka&apos;,1,0)+IF(R23__c=&apos;łazienka&apos;,1,0)+IF(R24__c=&apos;łazienka&apos;,1,0)+IF(R25__c=&apos;łazienka&apos;,1,0)+IF(R26__c=&apos;łazienka&apos;,1,0)+IF(R27__c=&apos;łazienka&apos;,1,0)+IF(R28__c=&apos;łazienka&apos;,1,0)+IF(R29__c=&apos;łazienka&apos;,1,0)+IF(R30__c=&apos;łazienka&apos;,1,0)+IF(R31__c=&apos;łazienka&apos;,1,0)+IF(R32__c=&apos;łazienka&apos;,1,0)+IF(R33__c=&apos;łazienka&apos;,1,0)+IF(R34__c=&apos;łazienka&apos;,1,0)+IF(R35__c=&apos;łazienka&apos;,1,0)+IF(R36__c=&apos;łazienka&apos;,1,0)+IF(R37__c=&apos;łazienka&apos;,1,0)+IF(R38__c=&apos;łazienka&apos;,1,0)+IF(R39__c=&apos;łazienka&apos;,1,0)+IF(R40__c=&apos;łazienka&apos;,1,0)+IF(R41__c=&apos;łazienka&apos;,1,0)+IF(R42__c=&apos;łazienka&apos;,1,0)+IF(R43__c=&apos;łazienka&apos;,1,0)+IF(R44__c=&apos;łazienka&apos;,1,0)+IF(R45__c=&apos;łazienka&apos;,1,0)+IF(R46__c=&apos;łazienka&apos;,1,0)+IF(R47__c=&apos;łazienka&apos;,1,0)+IF(R48__c=&apos;łazienka&apos;,1,0)+IF(R49__c=&apos;łazienka&apos;,1,0)+IF(R50__c=&apos;łazienka&apos;,1,0)</formula>
        <name>Design: Count Bathrooms - AT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Design__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Count_Bathrooms_GF</fullName>
        <field>Count_Bathrooms_Ground_Floor__c</field>
        <formula>IF(R1__c=&apos;łazienka&apos;,1,0)+IF(R2__c=&apos;łazienka&apos;,1,0)+IF(R3__c=&apos;łazienka&apos;,1,0)+IF(R4__c=&apos;łazienka&apos;,1,0)+IF(R5__c=&apos;łazienka&apos;,1,0)+IF(R6__c=&apos;łazienka&apos;,1,0)+IF(R7__c=&apos;łazienka&apos;,1,0)+IF(R8__c=&apos;łazienka&apos;,1,0)+IF(R9__c=&apos;łazienka&apos;,1,0)+IF(R10__c=&apos;łazienka&apos;,1,0)+IF(R11__c=&apos;łazienka&apos;,1,0)+IF(R12__c=&apos;łazienka&apos;,1,0)+IF(R13__c=&apos;łazienka&apos;,1,0)+IF(R14__c=&apos;łazienka&apos;,1,0)+IF(R15__c=&apos;łazienka&apos;,1,0)+IF(R16__c=&apos;łazienka&apos;,1,0)+IF(R17__c=&apos;łazienka&apos;,1,0)+IF(R18__c=&apos;łazienka&apos;,1,0)+IF(R19__c=&apos;łazienka&apos;,1,0)+IF(R20__c=&apos;łazienka&apos;,1,0)+IF(R21__c=&apos;łazienka&apos;,1,0)+IF(R22__c=&apos;łazienka&apos;,1,0)+IF(R23__c=&apos;łazienka&apos;,1,0)+IF(R24__c=&apos;łazienka&apos;,1,0)+IF(R25__c=&apos;łazienka&apos;,1,0)+IF(R26__c=&apos;łazienka&apos;,1,0)+IF(R27__c=&apos;łazienka&apos;,1,0)+IF(R28__c=&apos;łazienka&apos;,1,0)+IF(R29__c=&apos;łazienka&apos;,1,0)+IF(R30__c=&apos;łazienka&apos;,1,0)+IF(R31__c=&apos;łazienka&apos;,1,0)+IF(R32__c=&apos;łazienka&apos;,1,0)+IF(R33__c=&apos;łazienka&apos;,1,0)+IF(R34__c=&apos;łazienka&apos;,1,0)+IF(R35__c=&apos;łazienka&apos;,1,0)+IF(R36__c=&apos;łazienka&apos;,1,0)+IF(R37__c=&apos;łazienka&apos;,1,0)+IF(R38__c=&apos;łazienka&apos;,1,0)+IF(R39__c=&apos;łazienka&apos;,1,0)+IF(R40__c=&apos;łazienka&apos;,1,0)+IF(R41__c=&apos;łazienka&apos;,1,0)+IF(R42__c=&apos;łazienka&apos;,1,0)+IF(R43__c=&apos;łazienka&apos;,1,0)+IF(R44__c=&apos;łazienka&apos;,1,0)+IF(R45__c=&apos;łazienka&apos;,1,0)+IF(R46__c=&apos;łazienka&apos;,1,0)+IF(R47__c=&apos;łazienka&apos;,1,0)+IF(R48__c=&apos;łazienka&apos;,1,0)+IF(R49__c=&apos;łazienka&apos;,1,0)+IF(R50__c=&apos;łazienka&apos;,1,0)</formula>
        <name>Design: Count Bathrooms - GF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Design__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Count_Bedroom_Wardrobes_1F</fullName>
        <field>Count_Bedroom_Wardrobes_1st_Floor__c</field>
        <formula>IF(R1__c=&apos;pokój + garderoba&apos;,1,0)+IF(R2__c=&apos;pokój + garderoba&apos;,1,0)+IF(R3__c=&apos;pokój + garderoba&apos;,1,0)+IF(R4__c=&apos;pokój + garderoba&apos;,1,0)+IF(R5__c=&apos;pokój + garderoba&apos;,1,0)+IF(R6__c=&apos;pokój + garderoba&apos;,1,0)+IF(R7__c=&apos;pokój + garderoba&apos;,1,0)+IF(R8__c=&apos;pokój + garderoba&apos;,1,0)+IF(R9__c=&apos;pokój + garderoba&apos;,1,0)+IF(R10__c=&apos;pokój + garderoba&apos;,1,0)+IF(R11__c=&apos;pokój + garderoba&apos;,1,0)+IF(R12__c=&apos;pokój + garderoba&apos;,1,0)+IF(R13__c=&apos;pokój + garderoba&apos;,1,0)+IF(R14__c=&apos;pokój + garderoba&apos;,1,0)+IF(R15__c=&apos;pokój + garderoba&apos;,1,0)+IF(R16__c=&apos;pokój + garderoba&apos;,1,0)+IF(R17__c=&apos;pokój + garderoba&apos;,1,0)+IF(R18__c=&apos;pokój + garderoba&apos;,1,0)+IF(R19__c=&apos;pokój + garderoba&apos;,1,0)+IF(R20__c=&apos;pokój + garderoba&apos;,1,0)+IF(R21__c=&apos;pokój + garderoba&apos;,1,0)+IF(R22__c=&apos;pokój + garderoba&apos;,1,0)+IF(R23__c=&apos;pokój + garderoba&apos;,1,0)+IF(R24__c=&apos;pokój + garderoba&apos;,1,0)+IF(R25__c=&apos;pokój + garderoba&apos;,1,0)+IF(R26__c=&apos;pokój + garderoba&apos;,1,0)+IF(R27__c=&apos;pokój + garderoba&apos;,1,0)+IF(R28__c=&apos;pokój + garderoba&apos;,1,0)+IF(R29__c=&apos;pokój + garderoba&apos;,1,0)+IF(R30__c=&apos;pokój + garderoba&apos;,1,0)+IF(R31__c=&apos;pokój + garderoba&apos;,1,0)+IF(R32__c=&apos;pokój + garderoba&apos;,1,0)+IF(R33__c=&apos;pokój + garderoba&apos;,1,0)+IF(R34__c=&apos;pokój + garderoba&apos;,1,0)+IF(R35__c=&apos;pokój + garderoba&apos;,1,0)+IF(R36__c=&apos;pokój + garderoba&apos;,1,0)+IF(R37__c=&apos;pokój + garderoba&apos;,1,0)+IF(R38__c=&apos;pokój + garderoba&apos;,1,0)+IF(R39__c=&apos;pokój + garderoba&apos;,1,0)+IF(R40__c=&apos;pokój + garderoba&apos;,1,0)+IF(R41__c=&apos;pokój + garderoba&apos;,1,0)+IF(R42__c=&apos;pokój + garderoba&apos;,1,0)+IF(R43__c=&apos;pokój + garderoba&apos;,1,0)+IF(R44__c=&apos;pokój + garderoba&apos;,1,0)+IF(R45__c=&apos;pokój + garderoba&apos;,1,0)+IF(R46__c=&apos;pokój + garderoba&apos;,1,0)+IF(R47__c=&apos;pokój + garderoba&apos;,1,0)+IF(R48__c=&apos;pokój + garderoba&apos;,1,0)+IF(R49__c=&apos;pokój + garderoba&apos;,1,0)+IF(R50__c=&apos;pokój + garderoba&apos;,1,0)</formula>
        <name>Design: Count Bedroom-Wardrobes - 1F</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Design__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Count_Bedroom_Wardrobes_AT</fullName>
        <field>Count_Bedroom_Wardrobes_Attic__c</field>
        <formula>IF(R1__c=&apos;pokój + garderoba&apos;,1,0)+IF(R2__c=&apos;pokój + garderoba&apos;,1,0)+IF(R3__c=&apos;pokój + garderoba&apos;,1,0)+IF(R4__c=&apos;pokój + garderoba&apos;,1,0)+IF(R5__c=&apos;pokój + garderoba&apos;,1,0)+IF(R6__c=&apos;pokój + garderoba&apos;,1,0)+IF(R7__c=&apos;pokój + garderoba&apos;,1,0)+IF(R8__c=&apos;pokój + garderoba&apos;,1,0)+IF(R9__c=&apos;pokój + garderoba&apos;,1,0)+IF(R10__c=&apos;pokój + garderoba&apos;,1,0)+IF(R11__c=&apos;pokój + garderoba&apos;,1,0)+IF(R12__c=&apos;pokój + garderoba&apos;,1,0)+IF(R13__c=&apos;pokój + garderoba&apos;,1,0)+IF(R14__c=&apos;pokój + garderoba&apos;,1,0)+IF(R15__c=&apos;pokój + garderoba&apos;,1,0)+IF(R16__c=&apos;pokój + garderoba&apos;,1,0)+IF(R17__c=&apos;pokój + garderoba&apos;,1,0)+IF(R18__c=&apos;pokój + garderoba&apos;,1,0)+IF(R19__c=&apos;pokój + garderoba&apos;,1,0)+IF(R20__c=&apos;pokój + garderoba&apos;,1,0)+IF(R21__c=&apos;pokój + garderoba&apos;,1,0)+IF(R22__c=&apos;pokój + garderoba&apos;,1,0)+IF(R23__c=&apos;pokój + garderoba&apos;,1,0)+IF(R24__c=&apos;pokój + garderoba&apos;,1,0)+IF(R25__c=&apos;pokój + garderoba&apos;,1,0)+IF(R26__c=&apos;pokój + garderoba&apos;,1,0)+IF(R27__c=&apos;pokój + garderoba&apos;,1,0)+IF(R28__c=&apos;pokój + garderoba&apos;,1,0)+IF(R29__c=&apos;pokój + garderoba&apos;,1,0)+IF(R30__c=&apos;pokój + garderoba&apos;,1,0)+IF(R31__c=&apos;pokój + garderoba&apos;,1,0)+IF(R32__c=&apos;pokój + garderoba&apos;,1,0)+IF(R33__c=&apos;pokój + garderoba&apos;,1,0)+IF(R34__c=&apos;pokój + garderoba&apos;,1,0)+IF(R35__c=&apos;pokój + garderoba&apos;,1,0)+IF(R36__c=&apos;pokój + garderoba&apos;,1,0)+IF(R37__c=&apos;pokój + garderoba&apos;,1,0)+IF(R38__c=&apos;pokój + garderoba&apos;,1,0)+IF(R39__c=&apos;pokój + garderoba&apos;,1,0)+IF(R40__c=&apos;pokój + garderoba&apos;,1,0)+IF(R41__c=&apos;pokój + garderoba&apos;,1,0)+IF(R42__c=&apos;pokój + garderoba&apos;,1,0)+IF(R43__c=&apos;pokój + garderoba&apos;,1,0)+IF(R44__c=&apos;pokój + garderoba&apos;,1,0)+IF(R45__c=&apos;pokój + garderoba&apos;,1,0)+IF(R46__c=&apos;pokój + garderoba&apos;,1,0)+IF(R47__c=&apos;pokój + garderoba&apos;,1,0)+IF(R48__c=&apos;pokój + garderoba&apos;,1,0)+IF(R49__c=&apos;pokój + garderoba&apos;,1,0)+IF(R50__c=&apos;pokój + garderoba&apos;,1,0)</formula>
        <name>Design: Count Bedroom-Wardrobes - AT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Design__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Count_Bedroom_Wardrobes_GF</fullName>
        <field>Count_Bedroom_Wardrobes_Ground_Floor__c</field>
        <formula>IF(R1__c=&apos;pokój + garderoba&apos;,1,0)+IF(R2__c=&apos;pokój + garderoba&apos;,1,0)+IF(R3__c=&apos;pokój + garderoba&apos;,1,0)+IF(R4__c=&apos;pokój + garderoba&apos;,1,0)+IF(R5__c=&apos;pokój + garderoba&apos;,1,0)+IF(R6__c=&apos;pokój + garderoba&apos;,1,0)+IF(R7__c=&apos;pokój + garderoba&apos;,1,0)+IF(R8__c=&apos;pokój + garderoba&apos;,1,0)+IF(R9__c=&apos;pokój + garderoba&apos;,1,0)+IF(R10__c=&apos;pokój + garderoba&apos;,1,0)+IF(R11__c=&apos;pokój + garderoba&apos;,1,0)+IF(R12__c=&apos;pokój + garderoba&apos;,1,0)+IF(R13__c=&apos;pokój + garderoba&apos;,1,0)+IF(R14__c=&apos;pokój + garderoba&apos;,1,0)+IF(R15__c=&apos;pokój + garderoba&apos;,1,0)+IF(R16__c=&apos;pokój + garderoba&apos;,1,0)+IF(R17__c=&apos;pokój + garderoba&apos;,1,0)+IF(R18__c=&apos;pokój + garderoba&apos;,1,0)+IF(R19__c=&apos;pokój + garderoba&apos;,1,0)+IF(R20__c=&apos;pokój + garderoba&apos;,1,0)+IF(R21__c=&apos;pokój + garderoba&apos;,1,0)+IF(R22__c=&apos;pokój + garderoba&apos;,1,0)+IF(R23__c=&apos;pokój + garderoba&apos;,1,0)+IF(R24__c=&apos;pokój + garderoba&apos;,1,0)+IF(R25__c=&apos;pokój + garderoba&apos;,1,0)+IF(R26__c=&apos;pokój + garderoba&apos;,1,0)+IF(R27__c=&apos;pokój + garderoba&apos;,1,0)+IF(R28__c=&apos;pokój + garderoba&apos;,1,0)+IF(R29__c=&apos;pokój + garderoba&apos;,1,0)+IF(R30__c=&apos;pokój + garderoba&apos;,1,0)+IF(R31__c=&apos;pokój + garderoba&apos;,1,0)+IF(R32__c=&apos;pokój + garderoba&apos;,1,0)+IF(R33__c=&apos;pokój + garderoba&apos;,1,0)+IF(R34__c=&apos;pokój + garderoba&apos;,1,0)+IF(R35__c=&apos;pokój + garderoba&apos;,1,0)+IF(R36__c=&apos;pokój + garderoba&apos;,1,0)+IF(R37__c=&apos;pokój + garderoba&apos;,1,0)+IF(R38__c=&apos;pokój + garderoba&apos;,1,0)+IF(R39__c=&apos;pokój + garderoba&apos;,1,0)+IF(R40__c=&apos;pokój + garderoba&apos;,1,0)+IF(R41__c=&apos;pokój + garderoba&apos;,1,0)+IF(R42__c=&apos;pokój + garderoba&apos;,1,0)+IF(R43__c=&apos;pokój + garderoba&apos;,1,0)+IF(R44__c=&apos;pokój + garderoba&apos;,1,0)+IF(R45__c=&apos;pokój + garderoba&apos;,1,0)+IF(R46__c=&apos;pokój + garderoba&apos;,1,0)+IF(R47__c=&apos;pokój + garderoba&apos;,1,0)+IF(R48__c=&apos;pokój + garderoba&apos;,1,0)+IF(R49__c=&apos;pokój + garderoba&apos;,1,0)+IF(R50__c=&apos;pokój + garderoba&apos;,1,0)</formula>
        <name>Design: Count Bedroom-Wardrobes - GF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Design__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Count_Bedrooms_1F</fullName>
        <field>Count_Bedrooms_1st_Floor__c</field>
        <formula>IF(R1__c=&apos;pokój&apos;,1,0)+IF(R2__c=&apos;pokój&apos;,1,0)+IF(R3__c=&apos;pokój&apos;,1,0)+IF(R4__c=&apos;pokój&apos;,1,0)+IF(R5__c=&apos;pokój&apos;,1,0)+IF(R6__c=&apos;pokój&apos;,1,0)+IF(R7__c=&apos;pokój&apos;,1,0)+IF(R8__c=&apos;pokój&apos;,1,0)+IF(R9__c=&apos;pokój&apos;,1,0)+IF(R10__c=&apos;pokój&apos;,1,0)+IF(R11__c=&apos;pokój&apos;,1,0)+IF(R12__c=&apos;pokój&apos;,1,0)+IF(R13__c=&apos;pokój&apos;,1,0)+IF(R14__c=&apos;pokój&apos;,1,0)+IF(R15__c=&apos;pokój&apos;,1,0)+IF(R16__c=&apos;pokój&apos;,1,0)+IF(R17__c=&apos;pokój&apos;,1,0)+IF(R18__c=&apos;pokój&apos;,1,0)+IF(R19__c=&apos;pokój&apos;,1,0)+IF(R20__c=&apos;pokój&apos;,1,0)+IF(R21__c=&apos;pokój&apos;,1,0)+IF(R22__c=&apos;pokój&apos;,1,0)+IF(R23__c=&apos;pokój&apos;,1,0)+IF(R24__c=&apos;pokój&apos;,1,0)+IF(R25__c=&apos;pokój&apos;,1,0)+IF(R26__c=&apos;pokój&apos;,1,0)+IF(R27__c=&apos;pokój&apos;,1,0)+IF(R28__c=&apos;pokój&apos;,1,0)+IF(R29__c=&apos;pokój&apos;,1,0)+IF(R30__c=&apos;pokój&apos;,1,0)+IF(R31__c=&apos;pokój&apos;,1,0)+IF(R32__c=&apos;pokój&apos;,1,0)+IF(R33__c=&apos;pokój&apos;,1,0)+IF(R34__c=&apos;pokój&apos;,1,0)+IF(R35__c=&apos;pokój&apos;,1,0)+IF(R36__c=&apos;pokój&apos;,1,0)+IF(R37__c=&apos;pokój&apos;,1,0)+IF(R38__c=&apos;pokój&apos;,1,0)+IF(R39__c=&apos;pokój&apos;,1,0)+IF(R40__c=&apos;pokój&apos;,1,0)+IF(R41__c=&apos;pokój&apos;,1,0)+IF(R42__c=&apos;pokój&apos;,1,0)+IF(R43__c=&apos;pokój&apos;,1,0)+IF(R44__c=&apos;pokój&apos;,1,0)+IF(R45__c=&apos;pokój&apos;,1,0)+IF(R46__c=&apos;pokój&apos;,1,0)+IF(R47__c=&apos;pokój&apos;,1,0)+IF(R48__c=&apos;pokój&apos;,1,0)+IF(R49__c=&apos;pokój&apos;,1,0)+IF(R50__c=&apos;pokój&apos;,1,0)</formula>
        <name>Design: Count Bedrooms - 1F</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Design__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Count_Bedrooms_AT</fullName>
        <field>Count_Bedrooms_Attic__c</field>
        <formula>IF(R1__c=&apos;pokój&apos;,1,0)+IF(R2__c=&apos;pokój&apos;,1,0)+IF(R3__c=&apos;pokój&apos;,1,0)+IF(R4__c=&apos;pokój&apos;,1,0)+IF(R5__c=&apos;pokój&apos;,1,0)+IF(R6__c=&apos;pokój&apos;,1,0)+IF(R7__c=&apos;pokój&apos;,1,0)+IF(R8__c=&apos;pokój&apos;,1,0)+IF(R9__c=&apos;pokój&apos;,1,0)+IF(R10__c=&apos;pokój&apos;,1,0)+IF(R11__c=&apos;pokój&apos;,1,0)+IF(R12__c=&apos;pokój&apos;,1,0)+IF(R13__c=&apos;pokój&apos;,1,0)+IF(R14__c=&apos;pokój&apos;,1,0)+IF(R15__c=&apos;pokój&apos;,1,0)+IF(R16__c=&apos;pokój&apos;,1,0)+IF(R17__c=&apos;pokój&apos;,1,0)+IF(R18__c=&apos;pokój&apos;,1,0)+IF(R19__c=&apos;pokój&apos;,1,0)+IF(R20__c=&apos;pokój&apos;,1,0)+IF(R21__c=&apos;pokój&apos;,1,0)+IF(R22__c=&apos;pokój&apos;,1,0)+IF(R23__c=&apos;pokój&apos;,1,0)+IF(R24__c=&apos;pokój&apos;,1,0)+IF(R25__c=&apos;pokój&apos;,1,0)+IF(R26__c=&apos;pokój&apos;,1,0)+IF(R27__c=&apos;pokój&apos;,1,0)+IF(R28__c=&apos;pokój&apos;,1,0)+IF(R29__c=&apos;pokój&apos;,1,0)+IF(R30__c=&apos;pokój&apos;,1,0)+IF(R31__c=&apos;pokój&apos;,1,0)+IF(R32__c=&apos;pokój&apos;,1,0)+IF(R33__c=&apos;pokój&apos;,1,0)+IF(R34__c=&apos;pokój&apos;,1,0)+IF(R35__c=&apos;pokój&apos;,1,0)+IF(R36__c=&apos;pokój&apos;,1,0)+IF(R37__c=&apos;pokój&apos;,1,0)+IF(R38__c=&apos;pokój&apos;,1,0)+IF(R39__c=&apos;pokój&apos;,1,0)+IF(R40__c=&apos;pokój&apos;,1,0)+IF(R41__c=&apos;pokój&apos;,1,0)+IF(R42__c=&apos;pokój&apos;,1,0)+IF(R43__c=&apos;pokój&apos;,1,0)+IF(R44__c=&apos;pokój&apos;,1,0)+IF(R45__c=&apos;pokój&apos;,1,0)+IF(R46__c=&apos;pokój&apos;,1,0)+IF(R47__c=&apos;pokój&apos;,1,0)+IF(R48__c=&apos;pokój&apos;,1,0)+IF(R49__c=&apos;pokój&apos;,1,0)+IF(R50__c=&apos;pokój&apos;,1,0)</formula>
        <name>Design: Count Bedrooms - AT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Design__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Count_Bedrooms_GF</fullName>
        <field>Count_Bedrooms_Ground_Floor__c</field>
        <formula>IF(R1__c=&apos;pokój&apos;,1,0)+IF(R2__c=&apos;pokój&apos;,1,0)+IF(R3__c=&apos;pokój&apos;,1,0)+IF(R4__c=&apos;pokój&apos;,1,0)+IF(R5__c=&apos;pokój&apos;,1,0)+IF(R6__c=&apos;pokój&apos;,1,0)+IF(R7__c=&apos;pokój&apos;,1,0)+IF(R8__c=&apos;pokój&apos;,1,0)+IF(R9__c=&apos;pokój&apos;,1,0)+IF(R10__c=&apos;pokój&apos;,1,0)+IF(R11__c=&apos;pokój&apos;,1,0)+IF(R12__c=&apos;pokój&apos;,1,0)+IF(R13__c=&apos;pokój&apos;,1,0)+IF(R14__c=&apos;pokój&apos;,1,0)+IF(R15__c=&apos;pokój&apos;,1,0)+IF(R16__c=&apos;pokój&apos;,1,0)+IF(R17__c=&apos;pokój&apos;,1,0)+IF(R18__c=&apos;pokój&apos;,1,0)+IF(R19__c=&apos;pokój&apos;,1,0)+IF(R20__c=&apos;pokój&apos;,1,0)+IF(R21__c=&apos;pokój&apos;,1,0)+IF(R22__c=&apos;pokój&apos;,1,0)+IF(R23__c=&apos;pokój&apos;,1,0)+IF(R24__c=&apos;pokój&apos;,1,0)+IF(R25__c=&apos;pokój&apos;,1,0)+IF(R26__c=&apos;pokój&apos;,1,0)+IF(R27__c=&apos;pokój&apos;,1,0)+IF(R28__c=&apos;pokój&apos;,1,0)+IF(R29__c=&apos;pokój&apos;,1,0)+IF(R30__c=&apos;pokój&apos;,1,0)+IF(R31__c=&apos;pokój&apos;,1,0)+IF(R32__c=&apos;pokój&apos;,1,0)+IF(R33__c=&apos;pokój&apos;,1,0)+IF(R34__c=&apos;pokój&apos;,1,0)+IF(R35__c=&apos;pokój&apos;,1,0)+IF(R36__c=&apos;pokój&apos;,1,0)+IF(R37__c=&apos;pokój&apos;,1,0)+IF(R38__c=&apos;pokój&apos;,1,0)+IF(R39__c=&apos;pokój&apos;,1,0)+IF(R40__c=&apos;pokój&apos;,1,0)+IF(R41__c=&apos;pokój&apos;,1,0)+IF(R42__c=&apos;pokój&apos;,1,0)+IF(R43__c=&apos;pokój&apos;,1,0)+IF(R44__c=&apos;pokój&apos;,1,0)+IF(R45__c=&apos;pokój&apos;,1,0)+IF(R46__c=&apos;pokój&apos;,1,0)+IF(R47__c=&apos;pokój&apos;,1,0)+IF(R48__c=&apos;pokój&apos;,1,0)+IF(R49__c=&apos;pokój&apos;,1,0)+IF(R50__c=&apos;pokój&apos;,1,0)</formula>
        <name>Design: Count Bedrooms - GF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Design__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Count_Pantries_GF</fullName>
        <field>Count_Pantries_Ground_Floor__c</field>
        <formula>IF(R1__c=&apos;spiżarnia&apos;,1,0)+IF(R2__c=&apos;spiżarnia&apos;,1,0)+IF(R3__c=&apos;spiżarnia&apos;,1,0)+IF(R4__c=&apos;spiżarnia&apos;,1,0)+IF(R5__c=&apos;spiżarnia&apos;,1,0)+IF(R6__c=&apos;spiżarnia&apos;,1,0)+IF(R7__c=&apos;spiżarnia&apos;,1,0)+IF(R8__c=&apos;spiżarnia&apos;,1,0)+IF(R9__c=&apos;spiżarnia&apos;,1,0)+IF(R10__c=&apos;spiżarnia&apos;,1,0)+IF(R11__c=&apos;spiżarnia&apos;,1,0)+IF(R12__c=&apos;spiżarnia&apos;,1,0)+IF(R13__c=&apos;spiżarnia&apos;,1,0)+IF(R14__c=&apos;spiżarnia&apos;,1,0)+IF(R15__c=&apos;spiżarnia&apos;,1,0)+IF(R16__c=&apos;spiżarnia&apos;,1,0)+IF(R17__c=&apos;spiżarnia&apos;,1,0)+IF(R18__c=&apos;spiżarnia&apos;,1,0)+IF(R19__c=&apos;spiżarnia&apos;,1,0)+IF(R20__c=&apos;spiżarnia&apos;,1,0)+IF(R21__c=&apos;spiżarnia&apos;,1,0)+IF(R22__c=&apos;spiżarnia&apos;,1,0)+IF(R23__c=&apos;spiżarnia&apos;,1,0)+IF(R24__c=&apos;spiżarnia&apos;,1,0)+IF(R25__c=&apos;spiżarnia&apos;,1,0)+IF(R26__c=&apos;spiżarnia&apos;,1,0)+IF(R27__c=&apos;spiżarnia&apos;,1,0)+IF(R28__c=&apos;spiżarnia&apos;,1,0)+IF(R29__c=&apos;spiżarnia&apos;,1,0)+IF(R30__c=&apos;spiżarnia&apos;,1,0)+IF(R31__c=&apos;spiżarnia&apos;,1,0)+IF(R32__c=&apos;spiżarnia&apos;,1,0)+IF(R33__c=&apos;spiżarnia&apos;,1,0)+IF(R34__c=&apos;spiżarnia&apos;,1,0)+IF(R35__c=&apos;spiżarnia&apos;,1,0)+IF(R36__c=&apos;spiżarnia&apos;,1,0)+IF(R37__c=&apos;spiżarnia&apos;,1,0)+IF(R38__c=&apos;spiżarnia&apos;,1,0)+IF(R39__c=&apos;spiżarnia&apos;,1,0)+IF(R40__c=&apos;spiżarnia&apos;,1,0)+IF(R41__c=&apos;spiżarnia&apos;,1,0)+IF(R42__c=&apos;spiżarnia&apos;,1,0)+IF(R43__c=&apos;spiżarnia&apos;,1,0)+IF(R44__c=&apos;spiżarnia&apos;,1,0)+IF(R45__c=&apos;spiżarnia&apos;,1,0)+IF(R46__c=&apos;spiżarnia&apos;,1,0)+IF(R47__c=&apos;spiżarnia&apos;,1,0)+IF(R48__c=&apos;spiżarnia&apos;,1,0)+IF(R49__c=&apos;spiżarnia&apos;,1,0)+IF(R50__c=&apos;spiżarnia&apos;,1,0)</formula>
        <name>Design: Count Pantries - GF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Design__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Count_Toilets_1F</fullName>
        <field>Count_Toilets_1st_Floor__c</field>
        <formula>IF(R1__c=&apos;wc&apos;,1,0)+IF(R2__c=&apos;wc&apos;,1,0)+IF(R3__c=&apos;wc&apos;,1,0)+IF(R4__c=&apos;wc&apos;,1,0)+IF(R5__c=&apos;wc&apos;,1,0)+IF(R6__c=&apos;wc&apos;,1,0)+IF(R7__c=&apos;wc&apos;,1,0)+IF(R8__c=&apos;wc&apos;,1,0)+IF(R9__c=&apos;wc&apos;,1,0)+IF(R10__c=&apos;wc&apos;,1,0)+IF(R11__c=&apos;wc&apos;,1,0)+IF(R12__c=&apos;wc&apos;,1,0)+IF(R13__c=&apos;wc&apos;,1,0)+IF(R14__c=&apos;wc&apos;,1,0)+IF(R15__c=&apos;wc&apos;,1,0)+IF(R16__c=&apos;wc&apos;,1,0)+IF(R17__c=&apos;wc&apos;,1,0)+IF(R18__c=&apos;wc&apos;,1,0)+IF(R19__c=&apos;wc&apos;,1,0)+IF(R20__c=&apos;wc&apos;,1,0)+IF(R21__c=&apos;wc&apos;,1,0)+IF(R22__c=&apos;wc&apos;,1,0)+IF(R23__c=&apos;wc&apos;,1,0)+IF(R24__c=&apos;wc&apos;,1,0)+IF(R25__c=&apos;wc&apos;,1,0)+IF(R26__c=&apos;wc&apos;,1,0)+IF(R27__c=&apos;wc&apos;,1,0)+IF(R28__c=&apos;wc&apos;,1,0)+IF(R29__c=&apos;wc&apos;,1,0)+IF(R30__c=&apos;wc&apos;,1,0)+IF(R31__c=&apos;wc&apos;,1,0)+IF(R32__c=&apos;wc&apos;,1,0)+IF(R33__c=&apos;wc&apos;,1,0)+IF(R34__c=&apos;wc&apos;,1,0)+IF(R35__c=&apos;wc&apos;,1,0)+IF(R36__c=&apos;wc&apos;,1,0)+IF(R37__c=&apos;wc&apos;,1,0)+IF(R38__c=&apos;wc&apos;,1,0)+IF(R39__c=&apos;wc&apos;,1,0)+IF(R40__c=&apos;wc&apos;,1,0)+IF(R41__c=&apos;wc&apos;,1,0)+IF(R42__c=&apos;wc&apos;,1,0)+IF(R43__c=&apos;wc&apos;,1,0)+IF(R44__c=&apos;wc&apos;,1,0)+IF(R45__c=&apos;wc&apos;,1,0)+IF(R46__c=&apos;wc&apos;,1,0)+IF(R47__c=&apos;wc&apos;,1,0)+IF(R48__c=&apos;wc&apos;,1,0)+IF(R49__c=&apos;wc&apos;,1,0)+IF(R50__c=&apos;wc&apos;,1,0)</formula>
        <name>Design: Count Toilets - 1F</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Design__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Count_Toilets_AT</fullName>
        <field>Count_Toilets_Attic__c</field>
        <formula>IF(R1__c=&apos;wc&apos;,1,0)+IF(R2__c=&apos;wc&apos;,1,0)+IF(R3__c=&apos;wc&apos;,1,0)+IF(R4__c=&apos;wc&apos;,1,0)+IF(R5__c=&apos;wc&apos;,1,0)+IF(R6__c=&apos;wc&apos;,1,0)+IF(R7__c=&apos;wc&apos;,1,0)+IF(R8__c=&apos;wc&apos;,1,0)+IF(R9__c=&apos;wc&apos;,1,0)+IF(R10__c=&apos;wc&apos;,1,0)+IF(R11__c=&apos;wc&apos;,1,0)+IF(R12__c=&apos;wc&apos;,1,0)+IF(R13__c=&apos;wc&apos;,1,0)+IF(R14__c=&apos;wc&apos;,1,0)+IF(R15__c=&apos;wc&apos;,1,0)+IF(R16__c=&apos;wc&apos;,1,0)+IF(R17__c=&apos;wc&apos;,1,0)+IF(R18__c=&apos;wc&apos;,1,0)+IF(R19__c=&apos;wc&apos;,1,0)+IF(R20__c=&apos;wc&apos;,1,0)+IF(R21__c=&apos;wc&apos;,1,0)+IF(R22__c=&apos;wc&apos;,1,0)+IF(R23__c=&apos;wc&apos;,1,0)+IF(R24__c=&apos;wc&apos;,1,0)+IF(R25__c=&apos;wc&apos;,1,0)+IF(R26__c=&apos;wc&apos;,1,0)+IF(R27__c=&apos;wc&apos;,1,0)+IF(R28__c=&apos;wc&apos;,1,0)+IF(R29__c=&apos;wc&apos;,1,0)+IF(R30__c=&apos;wc&apos;,1,0)+IF(R31__c=&apos;wc&apos;,1,0)+IF(R32__c=&apos;wc&apos;,1,0)+IF(R33__c=&apos;wc&apos;,1,0)+IF(R34__c=&apos;wc&apos;,1,0)+IF(R35__c=&apos;wc&apos;,1,0)+IF(R36__c=&apos;wc&apos;,1,0)+IF(R37__c=&apos;wc&apos;,1,0)+IF(R38__c=&apos;wc&apos;,1,0)+IF(R39__c=&apos;wc&apos;,1,0)+IF(R40__c=&apos;wc&apos;,1,0)+IF(R41__c=&apos;wc&apos;,1,0)+IF(R42__c=&apos;wc&apos;,1,0)+IF(R43__c=&apos;wc&apos;,1,0)+IF(R44__c=&apos;wc&apos;,1,0)+IF(R45__c=&apos;wc&apos;,1,0)+IF(R46__c=&apos;wc&apos;,1,0)+IF(R47__c=&apos;wc&apos;,1,0)+IF(R48__c=&apos;wc&apos;,1,0)+IF(R49__c=&apos;wc&apos;,1,0)+IF(R50__c=&apos;wc&apos;,1,0)</formula>
        <name>Design: Count Toilets - AT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Design__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Count_Toilets_GF</fullName>
        <field>Count_Toilets_Ground_Floor__c</field>
        <formula>IF(R1__c=&apos;wc&apos;,1,0)+IF(R2__c=&apos;wc&apos;,1,0)+IF(R3__c=&apos;wc&apos;,1,0)+IF(R4__c=&apos;wc&apos;,1,0)+IF(R5__c=&apos;wc&apos;,1,0)+IF(R6__c=&apos;wc&apos;,1,0)+IF(R7__c=&apos;wc&apos;,1,0)+IF(R8__c=&apos;wc&apos;,1,0)+IF(R9__c=&apos;wc&apos;,1,0)+IF(R10__c=&apos;wc&apos;,1,0)+IF(R11__c=&apos;wc&apos;,1,0)+IF(R12__c=&apos;wc&apos;,1,0)+IF(R13__c=&apos;wc&apos;,1,0)+IF(R14__c=&apos;wc&apos;,1,0)+IF(R15__c=&apos;wc&apos;,1,0)+IF(R16__c=&apos;wc&apos;,1,0)+IF(R17__c=&apos;wc&apos;,1,0)+IF(R18__c=&apos;wc&apos;,1,0)+IF(R19__c=&apos;wc&apos;,1,0)+IF(R20__c=&apos;wc&apos;,1,0)+IF(R21__c=&apos;wc&apos;,1,0)+IF(R22__c=&apos;wc&apos;,1,0)+IF(R23__c=&apos;wc&apos;,1,0)+IF(R24__c=&apos;wc&apos;,1,0)+IF(R25__c=&apos;wc&apos;,1,0)+IF(R26__c=&apos;wc&apos;,1,0)+IF(R27__c=&apos;wc&apos;,1,0)+IF(R28__c=&apos;wc&apos;,1,0)+IF(R29__c=&apos;wc&apos;,1,0)+IF(R30__c=&apos;wc&apos;,1,0)+IF(R31__c=&apos;wc&apos;,1,0)+IF(R32__c=&apos;wc&apos;,1,0)+IF(R33__c=&apos;wc&apos;,1,0)+IF(R34__c=&apos;wc&apos;,1,0)+IF(R35__c=&apos;wc&apos;,1,0)+IF(R36__c=&apos;wc&apos;,1,0)+IF(R37__c=&apos;wc&apos;,1,0)+IF(R38__c=&apos;wc&apos;,1,0)+IF(R39__c=&apos;wc&apos;,1,0)+IF(R40__c=&apos;wc&apos;,1,0)+IF(R41__c=&apos;wc&apos;,1,0)+IF(R42__c=&apos;wc&apos;,1,0)+IF(R43__c=&apos;wc&apos;,1,0)+IF(R44__c=&apos;wc&apos;,1,0)+IF(R45__c=&apos;wc&apos;,1,0)+IF(R46__c=&apos;wc&apos;,1,0)+IF(R47__c=&apos;wc&apos;,1,0)+IF(R48__c=&apos;wc&apos;,1,0)+IF(R49__c=&apos;wc&apos;,1,0)+IF(R50__c=&apos;wc&apos;,1,0)</formula>
        <name>Design: Count Toilets - GF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Design__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Count_Utility_Rooms_BA</fullName>
        <field>Count_Utility_Rooms_Basement__c</field>
        <formula>IF(R1__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R2__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R3__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R4__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R5__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R6__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R7__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R8__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R9__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R10__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R11__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R12__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R13__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R14__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R15__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R16__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R17__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R18__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R19__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R20__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R21__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R22__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R23__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R24__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R25__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R26__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R27__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R28__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R29__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R30__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R31__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R32__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R33__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R34__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R35__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R36__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R37__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R38__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R39__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R40__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R41__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R42__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R43__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R44__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R45__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R46__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R47__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R48__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R49__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R50__c=&apos;pomieszczenie gospodarcze&apos;,1,0)</formula>
        <name>Design: Count Utility Rooms - BA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Design__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Count_Utility_Rooms_GF</fullName>
        <field>Count_Utility_Rooms_Ground_Floor__c</field>
        <formula>IF(R1__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R2__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R3__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R4__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R5__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R6__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R7__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R8__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R9__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R10__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R11__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R12__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R13__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R14__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R15__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R16__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R17__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R18__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R19__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R20__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R21__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R22__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R23__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R24__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R25__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R26__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R27__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R28__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R29__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R30__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R31__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R32__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R33__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R34__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R35__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R36__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R37__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R38__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R39__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R40__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R41__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R42__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R43__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R44__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R45__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R46__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R47__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R48__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R49__c=&apos;pomieszczenie gospodarcze&apos;,1,0)+IF(R50__c=&apos;pomieszczenie gospodarcze&apos;,1,0)</formula>
        <name>Design: Count Utility Rooms - GF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Design__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Count_Wardrobes_1F</fullName>
        <field>Count_Wardrobes_1st_Floor__c</field>
        <formula>IF(R1__c=&apos;garderoba&apos;,1,0)+IF(R2__c=&apos;garderoba&apos;,1,0)+IF(R3__c=&apos;garderoba&apos;,1,0)+IF(R4__c=&apos;garderoba&apos;,1,0)+IF(R5__c=&apos;garderoba&apos;,1,0)+IF(R6__c=&apos;garderoba&apos;,1,0)+IF(R7__c=&apos;garderoba&apos;,1,0)+IF(R8__c=&apos;garderoba&apos;,1,0)+IF(R9__c=&apos;garderoba&apos;,1,0)+IF(R10__c=&apos;garderoba&apos;,1,0)+IF(R11__c=&apos;garderoba&apos;,1,0)+IF(R12__c=&apos;garderoba&apos;,1,0)+IF(R13__c=&apos;garderoba&apos;,1,0)+IF(R14__c=&apos;garderoba&apos;,1,0)+IF(R15__c=&apos;garderoba&apos;,1,0)+IF(R16__c=&apos;garderoba&apos;,1,0)+IF(R17__c=&apos;garderoba&apos;,1,0)+IF(R18__c=&apos;garderoba&apos;,1,0)+IF(R19__c=&apos;garderoba&apos;,1,0)+IF(R20__c=&apos;garderoba&apos;,1,0)+IF(R21__c=&apos;garderoba&apos;,1,0)+IF(R22__c=&apos;garderoba&apos;,1,0)+IF(R23__c=&apos;garderoba&apos;,1,0)+IF(R24__c=&apos;garderoba&apos;,1,0)+IF(R25__c=&apos;garderoba&apos;,1,0)+IF(R26__c=&apos;garderoba&apos;,1,0)+IF(R27__c=&apos;garderoba&apos;,1,0)+IF(R28__c=&apos;garderoba&apos;,1,0)+IF(R29__c=&apos;garderoba&apos;,1,0)+IF(R30__c=&apos;garderoba&apos;,1,0)+IF(R31__c=&apos;garderoba&apos;,1,0)+IF(R32__c=&apos;garderoba&apos;,1,0)+IF(R33__c=&apos;garderoba&apos;,1,0)+IF(R34__c=&apos;garderoba&apos;,1,0)+IF(R35__c=&apos;garderoba&apos;,1,0)+IF(R36__c=&apos;garderoba&apos;,1,0)+IF(R37__c=&apos;garderoba&apos;,1,0)+IF(R38__c=&apos;garderoba&apos;,1,0)+IF(R39__c=&apos;garderoba&apos;,1,0)+IF(R40__c=&apos;garderoba&apos;,1,0)+IF(R41__c=&apos;garderoba&apos;,1,0)+IF(R42__c=&apos;garderoba&apos;,1,0)+IF(R43__c=&apos;garderoba&apos;,1,0)+IF(R44__c=&apos;garderoba&apos;,1,0)+IF(R45__c=&apos;garderoba&apos;,1,0)+IF(R46__c=&apos;garderoba&apos;,1,0)+IF(R47__c=&apos;garderoba&apos;,1,0)+IF(R48__c=&apos;garderoba&apos;,1,0)+IF(R49__c=&apos;garderoba&apos;,1,0)+IF(R50__c=&apos;garderoba&apos;,1,0)</formula>
        <name>Design: Count Wardrobes - 1F</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Design__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Count_Wardrobes_AT</fullName>
        <field>Count_Wardrobes_Attic__c</field>
        <formula>IF(R1__c=&apos;garderoba&apos;,1,0)+IF(R2__c=&apos;garderoba&apos;,1,0)+IF(R3__c=&apos;garderoba&apos;,1,0)+IF(R4__c=&apos;garderoba&apos;,1,0)+IF(R5__c=&apos;garderoba&apos;,1,0)+IF(R6__c=&apos;garderoba&apos;,1,0)+IF(R7__c=&apos;garderoba&apos;,1,0)+IF(R8__c=&apos;garderoba&apos;,1,0)+IF(R9__c=&apos;garderoba&apos;,1,0)+IF(R10__c=&apos;garderoba&apos;,1,0)+IF(R11__c=&apos;garderoba&apos;,1,0)+IF(R12__c=&apos;garderoba&apos;,1,0)+IF(R13__c=&apos;garderoba&apos;,1,0)+IF(R14__c=&apos;garderoba&apos;,1,0)+IF(R15__c=&apos;garderoba&apos;,1,0)+IF(R16__c=&apos;garderoba&apos;,1,0)+IF(R17__c=&apos;garderoba&apos;,1,0)+IF(R18__c=&apos;garderoba&apos;,1,0)+IF(R19__c=&apos;garderoba&apos;,1,0)+IF(R20__c=&apos;garderoba&apos;,1,0)+IF(R21__c=&apos;garderoba&apos;,1,0)+IF(R22__c=&apos;garderoba&apos;,1,0)+IF(R23__c=&apos;garderoba&apos;,1,0)+IF(R24__c=&apos;garderoba&apos;,1,0)+IF(R25__c=&apos;garderoba&apos;,1,0)+IF(R26__c=&apos;garderoba&apos;,1,0)+IF(R27__c=&apos;garderoba&apos;,1,0)+IF(R28__c=&apos;garderoba&apos;,1,0)+IF(R29__c=&apos;garderoba&apos;,1,0)+IF(R30__c=&apos;garderoba&apos;,1,0)+IF(R31__c=&apos;garderoba&apos;,1,0)+IF(R32__c=&apos;garderoba&apos;,1,0)+IF(R33__c=&apos;garderoba&apos;,1,0)+IF(R34__c=&apos;garderoba&apos;,1,0)+IF(R35__c=&apos;garderoba&apos;,1,0)+IF(R36__c=&apos;garderoba&apos;,1,0)+IF(R37__c=&apos;garderoba&apos;,1,0)+IF(R38__c=&apos;garderoba&apos;,1,0)+IF(R39__c=&apos;garderoba&apos;,1,0)+IF(R40__c=&apos;garderoba&apos;,1,0)+IF(R41__c=&apos;garderoba&apos;,1,0)+IF(R42__c=&apos;garderoba&apos;,1,0)+IF(R43__c=&apos;garderoba&apos;,1,0)+IF(R44__c=&apos;garderoba&apos;,1,0)+IF(R45__c=&apos;garderoba&apos;,1,0)+IF(R46__c=&apos;garderoba&apos;,1,0)+IF(R47__c=&apos;garderoba&apos;,1,0)+IF(R48__c=&apos;garderoba&apos;,1,0)+IF(R49__c=&apos;garderoba&apos;,1,0)+IF(R50__c=&apos;garderoba&apos;,1,0)</formula>
        <name>Design: Count Wardrobes - AT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Design__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Count_Wardrobes_GF</fullName>
        <field>Count_Wardrobes_Ground_Floor__c</field>
        <formula>IF(R1__c=&apos;garderoba&apos;,1,0)+IF(R2__c=&apos;garderoba&apos;,1,0)+IF(R3__c=&apos;garderoba&apos;,1,0)+IF(R4__c=&apos;garderoba&apos;,1,0)+IF(R5__c=&apos;garderoba&apos;,1,0)+IF(R6__c=&apos;garderoba&apos;,1,0)+IF(R7__c=&apos;garderoba&apos;,1,0)+IF(R8__c=&apos;garderoba&apos;,1,0)+IF(R9__c=&apos;garderoba&apos;,1,0)+IF(R10__c=&apos;garderoba&apos;,1,0)+IF(R11__c=&apos;garderoba&apos;,1,0)+IF(R12__c=&apos;garderoba&apos;,1,0)+IF(R13__c=&apos;garderoba&apos;,1,0)+IF(R14__c=&apos;garderoba&apos;,1,0)+IF(R15__c=&apos;garderoba&apos;,1,0)+IF(R16__c=&apos;garderoba&apos;,1,0)+IF(R17__c=&apos;garderoba&apos;,1,0)+IF(R18__c=&apos;garderoba&apos;,1,0)+IF(R19__c=&apos;garderoba&apos;,1,0)+IF(R20__c=&apos;garderoba&apos;,1,0)+IF(R21__c=&apos;garderoba&apos;,1,0)+IF(R22__c=&apos;garderoba&apos;,1,0)+IF(R23__c=&apos;garderoba&apos;,1,0)+IF(R24__c=&apos;garderoba&apos;,1,0)+IF(R25__c=&apos;garderoba&apos;,1,0)+IF(R26__c=&apos;garderoba&apos;,1,0)+IF(R27__c=&apos;garderoba&apos;,1,0)+IF(R28__c=&apos;garderoba&apos;,1,0)+IF(R29__c=&apos;garderoba&apos;,1,0)+IF(R30__c=&apos;garderoba&apos;,1,0)+IF(R31__c=&apos;garderoba&apos;,1,0)+IF(R32__c=&apos;garderoba&apos;,1,0)+IF(R33__c=&apos;garderoba&apos;,1,0)+IF(R34__c=&apos;garderoba&apos;,1,0)+IF(R35__c=&apos;garderoba&apos;,1,0)+IF(R36__c=&apos;garderoba&apos;,1,0)+IF(R37__c=&apos;garderoba&apos;,1,0)+IF(R38__c=&apos;garderoba&apos;,1,0)+IF(R39__c=&apos;garderoba&apos;,1,0)+IF(R40__c=&apos;garderoba&apos;,1,0)+IF(R41__c=&apos;garderoba&apos;,1,0)+IF(R42__c=&apos;garderoba&apos;,1,0)+IF(R43__c=&apos;garderoba&apos;,1,0)+IF(R44__c=&apos;garderoba&apos;,1,0)+IF(R45__c=&apos;garderoba&apos;,1,0)+IF(R46__c=&apos;garderoba&apos;,1,0)+IF(R47__c=&apos;garderoba&apos;,1,0)+IF(R48__c=&apos;garderoba&apos;,1,0)+IF(R49__c=&apos;garderoba&apos;,1,0)+IF(R50__c=&apos;garderoba&apos;,1,0)</formula>
        <name>Design: Count Wardrobes - GF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Design__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Dining_Living_Usable_Area_GF</fullName>
        <field>Dining_Living_Usable_Area__c</field>
        <formula>MAX(IF(R1__c==&apos;pokój dzienny + jadalnia&apos;,V1__c,0), 
MAX(IF(R2__c==&apos;pokój dzienny + jadalnia&apos;,V2__c,0), 
MAX(IF(R3__c==&apos;pokój dzienny + jadalnia&apos;,V3__c,0), 
MAX(IF(R4__c==&apos;pokój dzienny + jadalnia&apos;,V4__c,0), 
MAX(IF(R5__c==&apos;pokój dzienny + jadalnia&apos;,V5__c,0), 
MAX(IF(R6__c==&apos;pokój dzienny + jadalnia&apos;,V6__c,0), 
MAX(IF(R7__c==&apos;pokój dzienny + jadalnia&apos;,V7__c,0), 
MAX(IF(R8__c==&apos;pokój dzienny + jadalnia&apos;,V8__c,0), 
MAX(IF(R9__c==&apos;pokój dzienny + jadalnia&apos;,V9__c,0), 
MAX(IF(R10__c==&apos;pokój dzienny + jadalnia&apos;,V10__c,0), 
MAX(IF(R11__c==&apos;pokój dzienny + jadalnia&apos;,V11__c,0), 
MAX(IF(R12__c==&apos;pokój dzienny + jadalnia&apos;,V12__c,0), 
MAX(IF(R13__c==&apos;pokój dzienny + jadalnia&apos;,V13__c,0), 
MAX(IF(R14__c==&apos;pokój dzienny + jadalnia&apos;,V14__c,0), 
MAX(IF(R15__c==&apos;pokój dzienny + jadalnia&apos;,V15__c,0), 
MAX(IF(R16__c==&apos;pokój dzienny + jadalnia&apos;,V16__c,0), 
MAX(IF(R17__c==&apos;pokój dzienny + jadalnia&apos;,V17__c,0), 
MAX(IF(R18__c==&apos;pokój dzienny + jadalnia&apos;,V18__c,0), 
MAX(IF(R19__c==&apos;pokój dzienny + jadalnia&apos;,V19__c,0), 
MAX(IF(R20__c==&apos;pokój dzienny + jadalnia&apos;,V20__c,0), 
MAX(IF(R21__c==&apos;pokój dzienny + jadalnia&apos;,V21__c,0), 
MAX(IF(R22__c==&apos;pokój dzienny + jadalnia&apos;,V22__c,0), 
MAX(IF(R23__c==&apos;pokój dzienny + jadalnia&apos;,V23__c,0), 
MAX(IF(R24__c==&apos;pokój dzienny + jadalnia&apos;,V24__c,0), 
MAX(IF(R25__c==&apos;pokój dzienny + jadalnia&apos;,V25__c,0), 
MAX(IF(R26__c==&apos;pokój dzienny + jadalnia&apos;,V26__c,0), 
MAX(IF(R27__c==&apos;pokój dzienny + jadalnia&apos;,V27__c,0), 
MAX(IF(R28__c==&apos;pokój dzienny + jadalnia&apos;,V28__c,0), 
MAX(IF(R29__c==&apos;pokój dzienny + jadalnia&apos;,V29__c,0), 
MAX(IF(R30__c==&apos;pokój dzienny + jadalnia&apos;,V30__c,0), 
MAX(IF(R31__c==&apos;pokój dzienny + jadalnia&apos;,V31__c,0), 
MAX(IF(R32__c==&apos;pokój dzienny + jadalnia&apos;,V32__c,0), 
MAX(IF(R33__c==&apos;pokój dzienny + jadalnia&apos;,V33__c,0), 
MAX(IF(R34__c==&apos;pokój dzienny + jadalnia&apos;,V34__c,0), 
MAX(IF(R35__c==&apos;pokój dzienny + jadalnia&apos;,V35__c,0), 
MAX(IF(R36__c==&apos;pokój dzienny + jadalnia&apos;,V36__c,0), 
MAX(IF(R37__c==&apos;pokój dzienny + jadalnia&apos;,V37__c,0), 
MAX(IF(R38__c==&apos;pokój dzienny + jadalnia&apos;,V38__c,0), 
MAX(IF(R39__c==&apos;pokój dzienny + jadalnia&apos;,V39__c,0), 
MAX(IF(R40__c==&apos;pokój dzienny + jadalnia&apos;,V40__c,0), 
MAX(IF(R41__c==&apos;pokój dzienny + jadalnia&apos;,V41__c,0), 
MAX(IF(R42__c==&apos;pokój dzienny + jadalnia&apos;,V42__c,0), 
MAX(IF(R43__c==&apos;pokój dzienny + jadalnia&apos;,V43__c,0), 
MAX(IF(R44__c==&apos;pokój dzienny + jadalnia&apos;,V44__c,0), 
MAX(IF(R45__c==&apos;pokój dzienny + jadalnia&apos;,V45__c,0),0)
))))))))))))))))))))))))))))))))))))))))))))</formula>
        <name>Design: Dining-Living Usable Area - GF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Design__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Dining_Usable_Area_GF</fullName>
        <field>Dining_Usable_Area__c</field>
        <formula>MAX(IF(R1__c==&apos;jadalnia&apos;,V1__c,0), 
MAX(IF(R2__c==&apos;jadalnia&apos;,V2__c,0), 
MAX(IF(R3__c==&apos;jadalnia&apos;,V3__c,0), 
MAX(IF(R4__c==&apos;jadalnia&apos;,V4__c,0), 
MAX(IF(R5__c==&apos;jadalnia&apos;,V5__c,0), 
MAX(IF(R6__c==&apos;jadalnia&apos;,V6__c,0), 
MAX(IF(R7__c==&apos;jadalnia&apos;,V7__c,0), 
MAX(IF(R8__c==&apos;jadalnia&apos;,V8__c,0), 
MAX(IF(R9__c==&apos;jadalnia&apos;,V9__c,0), 
MAX(IF(R10__c==&apos;jadalnia&apos;,V10__c,0), 
MAX(IF(R11__c==&apos;jadalnia&apos;,V11__c,0), 
MAX(IF(R12__c==&apos;jadalnia&apos;,V12__c,0), 
MAX(IF(R13__c==&apos;jadalnia&apos;,V13__c,0), 
MAX(IF(R14__c==&apos;jadalnia&apos;,V14__c,0), 
MAX(IF(R15__c==&apos;jadalnia&apos;,V15__c,0), 
MAX(IF(R16__c==&apos;jadalnia&apos;,V16__c,0), 
MAX(IF(R17__c==&apos;jadalnia&apos;,V17__c,0), 
MAX(IF(R18__c==&apos;jadalnia&apos;,V18__c,0), 
MAX(IF(R19__c==&apos;jadalnia&apos;,V19__c,0), 
MAX(IF(R20__c==&apos;jadalnia&apos;,V20__c,0), 
MAX(IF(R21__c==&apos;jadalnia&apos;,V21__c,0), 
MAX(IF(R22__c==&apos;jadalnia&apos;,V22__c,0), 
MAX(IF(R23__c==&apos;jadalnia&apos;,V23__c,0), 
MAX(IF(R24__c==&apos;jadalnia&apos;,V24__c,0), 
MAX(IF(R25__c==&apos;jadalnia&apos;,V25__c,0), 
MAX(IF(R26__c==&apos;jadalnia&apos;,V26__c,0), 
MAX(IF(R27__c==&apos;jadalnia&apos;,V27__c,0), 
MAX(IF(R28__c==&apos;jadalnia&apos;,V28__c,0), 
MAX(IF(R29__c==&apos;jadalnia&apos;,V29__c,0), 
MAX(IF(R30__c==&apos;jadalnia&apos;,V30__c,0), 
MAX(IF(R31__c==&apos;jadalnia&apos;,V31__c,0), 
MAX(IF(R32__c==&apos;jadalnia&apos;,V32__c,0), 
MAX(IF(R33__c==&apos;jadalnia&apos;,V33__c,0), 
MAX(IF(R34__c==&apos;jadalnia&apos;,V34__c,0), 
MAX(IF(R35__c==&apos;jadalnia&apos;,V35__c,0), 
MAX(IF(R36__c==&apos;jadalnia&apos;,V36__c,0), 
MAX(IF(R37__c==&apos;jadalnia&apos;,V37__c,0), 
MAX(IF(R38__c==&apos;jadalnia&apos;,V38__c,0), 
MAX(IF(R39__c==&apos;jadalnia&apos;,V39__c,0), 
MAX(IF(R40__c==&apos;jadalnia&apos;,V40__c,0), 
MAX(IF(R41__c==&apos;jadalnia&apos;,V41__c,0), 
MAX(IF(R42__c==&apos;jadalnia&apos;,V42__c,0), 
MAX(IF(R43__c==&apos;jadalnia&apos;,V43__c,0), 
MAX(IF(R44__c==&apos;jadalnia&apos;,V44__c,0), 
MAX(IF(R45__c==&apos;jadalnia&apos;,V45__c,0), 
MAX(IF(R46__c==&apos;jadalnia&apos;,V46__c,0), 
MAX(IF(R47__c==&apos;jadalnia&apos;,V47__c,0), 
MAX(IF(R48__c==&apos;jadalnia&apos;,V48__c,0),0) 
)))))))))))))))))))))))))))))))))))))))))))))))</formula>
        <name>Design: Dining Usable Area - GF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Design__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Heating_Room_Usable_Area_BA</fullName>
        <field>Heating_Room_Usable_Area_Basement__c</field>
        <formula>MAX(IF(R1__c==&apos;kotłownia&apos;,V1__c,0), 
MAX(IF(R2__c==&apos;kotłownia&apos;,V2__c,0), 
MAX(IF(R3__c==&apos;kotłownia&apos;,V3__c,0), 
MAX(IF(R4__c==&apos;kotłownia&apos;,V4__c,0), 
MAX(IF(R5__c==&apos;kotłownia&apos;,V5__c,0), 
MAX(IF(R6__c==&apos;kotłownia&apos;,V6__c,0), 
MAX(IF(R7__c==&apos;kotłownia&apos;,V7__c,0), 
MAX(IF(R8__c==&apos;kotłownia&apos;,V8__c,0), 
MAX(IF(R9__c==&apos;kotłownia&apos;,V9__c,0), 
MAX(IF(R10__c==&apos;kotłownia&apos;,V10__c,0), 
MAX(IF(R11__c==&apos;kotłownia&apos;,V11__c,0), 
MAX(IF(R12__c==&apos;kotłownia&apos;,V12__c,0), 
MAX(IF(R13__c==&apos;kotłownia&apos;,V13__c,0), 
MAX(IF(R14__c==&apos;kotłownia&apos;,V14__c,0), 
MAX(IF(R15__c==&apos;kotłownia&apos;,V15__c,0), 
MAX(IF(R16__c==&apos;kotłownia&apos;,V16__c,0), 
MAX(IF(R17__c==&apos;kotłownia&apos;,V17__c,0), 
MAX(IF(R18__c==&apos;kotłownia&apos;,V18__c,0), 
MAX(IF(R19__c==&apos;kotłownia&apos;,V19__c,0), 
MAX(IF(R20__c==&apos;kotłownia&apos;,V20__c,0), 
MAX(IF(R21__c==&apos;kotłownia&apos;,V21__c,0), 
MAX(IF(R22__c==&apos;kotłownia&apos;,V22__c,0), 
MAX(IF(R23__c==&apos;kotłownia&apos;,V23__c,0), 
MAX(IF(R24__c==&apos;kotłownia&apos;,V24__c,0), 
MAX(IF(R25__c==&apos;kotłownia&apos;,V25__c,0), 
MAX(IF(R26__c==&apos;kotłownia&apos;,V26__c,0), 
MAX(IF(R27__c==&apos;kotłownia&apos;,V27__c,0), 
MAX(IF(R28__c==&apos;kotłownia&apos;,V28__c,0), 
MAX(IF(R29__c==&apos;kotłownia&apos;,V29__c,0), 
MAX(IF(R30__c==&apos;kotłownia&apos;,V30__c,0), 
MAX(IF(R31__c==&apos;kotłownia&apos;,V31__c,0), 
MAX(IF(R32__c==&apos;kotłownia&apos;,V32__c,0), 
MAX(IF(R33__c==&apos;kotłownia&apos;,V33__c,0), 
MAX(IF(R34__c==&apos;kotłownia&apos;,V34__c,0), 
MAX(IF(R35__c==&apos;kotłownia&apos;,V35__c,0), 
MAX(IF(R36__c==&apos;kotłownia&apos;,V36__c,0), 
MAX(IF(R37__c==&apos;kotłownia&apos;,V37__c,0), 
MAX(IF(R38__c==&apos;kotłownia&apos;,V38__c,0), 
MAX(IF(R39__c==&apos;kotłownia&apos;,V39__c,0), 
MAX(IF(R40__c==&apos;kotłownia&apos;,V40__c,0),0) 
)))))))))))))))))))))))))))))))))))))))</formula>
        <name>Design: Heating Room Usable Area - BA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Design__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Heating_Room_Usable_Area_GF</fullName>
        <field>Heating_Room_Usable_Area_Ground_Floor__c</field>
        <formula>MAX(IF(R1__c==&apos;kotłownia&apos;,V1__c,0), 
MAX(IF(R2__c==&apos;kotłownia&apos;,V2__c,0), 
MAX(IF(R3__c==&apos;kotłownia&apos;,V3__c,0), 
MAX(IF(R4__c==&apos;kotłownia&apos;,V4__c,0), 
MAX(IF(R5__c==&apos;kotłownia&apos;,V5__c,0), 
MAX(IF(R6__c==&apos;kotłownia&apos;,V6__c,0), 
MAX(IF(R7__c==&apos;kotłownia&apos;,V7__c,0), 
MAX(IF(R8__c==&apos;kotłownia&apos;,V8__c,0), 
MAX(IF(R9__c==&apos;kotłownia&apos;,V9__c,0), 
MAX(IF(R10__c==&apos;kotłownia&apos;,V10__c,0), 
MAX(IF(R11__c==&apos;kotłownia&apos;,V11__c,0), 
MAX(IF(R12__c==&apos;kotłownia&apos;,V12__c,0), 
MAX(IF(R13__c==&apos;kotłownia&apos;,V13__c,0), 
MAX(IF(R14__c==&apos;kotłownia&apos;,V14__c,0), 
MAX(IF(R15__c==&apos;kotłownia&apos;,V15__c,0), 
MAX(IF(R16__c==&apos;kotłownia&apos;,V16__c,0), 
MAX(IF(R17__c==&apos;kotłownia&apos;,V17__c,0), 
MAX(IF(R18__c==&apos;kotłownia&apos;,V18__c,0), 
MAX(IF(R19__c==&apos;kotłownia&apos;,V19__c,0), 
MAX(IF(R20__c==&apos;kotłownia&apos;,V20__c,0), 
MAX(IF(R21__c==&apos;kotłownia&apos;,V21__c,0), 
MAX(IF(R22__c==&apos;kotłownia&apos;,V22__c,0), 
MAX(IF(R23__c==&apos;kotłownia&apos;,V23__c,0), 
MAX(IF(R24__c==&apos;kotłownia&apos;,V24__c,0), 
MAX(IF(R25__c==&apos;kotłownia&apos;,V25__c,0), 
MAX(IF(R26__c==&apos;kotłownia&apos;,V26__c,0), 
MAX(IF(R27__c==&apos;kotłownia&apos;,V27__c,0), 
MAX(IF(R28__c==&apos;kotłownia&apos;,V28__c,0), 
MAX(IF(R29__c==&apos;kotłownia&apos;,V29__c,0), 
MAX(IF(R30__c==&apos;kotłownia&apos;,V30__c,0), 
MAX(IF(R31__c==&apos;kotłownia&apos;,V31__c,0), 
MAX(IF(R32__c==&apos;kotłownia&apos;,V32__c,0), 
MAX(IF(R33__c==&apos;kotłownia&apos;,V33__c,0), 
MAX(IF(R34__c==&apos;kotłownia&apos;,V34__c,0), 
MAX(IF(R35__c==&apos;kotłownia&apos;,V35__c,0), 
MAX(IF(R36__c==&apos;kotłownia&apos;,V36__c,0), 
MAX(IF(R37__c==&apos;kotłownia&apos;,V37__c,0), 
MAX(IF(R38__c==&apos;kotłownia&apos;,V38__c,0), 
MAX(IF(R39__c==&apos;kotłownia&apos;,V39__c,0), 
MAX(IF(R40__c==&apos;kotłownia&apos;,V40__c,0),0) 
)))))))))))))))))))))))))))))))))))))))</formula>
        <name>Design: Heating Room Usable Area - GF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Design__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Kitch_Din_Livin_Usable_Area_GF</fullName>
        <field>Kitchen_Dining_Living_Usable_Area__c</field>
        <formula>MAX(IF(R1__c==&apos;pokój dzienny + kuchnia + jadalnia&apos;,V1__c,0), 
MAX(IF(R2__c==&apos;pokój dzienny + kuchnia + jadalnia&apos;,V2__c,0), 
MAX(IF(R3__c==&apos;pokój dzienny + kuchnia + jadalnia&apos;,V3__c,0), 
MAX(IF(R4__c==&apos;pokój dzienny + kuchnia + jadalnia&apos;,V4__c,0), 
MAX(IF(R5__c==&apos;pokój dzienny + kuchnia + jadalnia&apos;,V5__c,0), 
MAX(IF(R6__c==&apos;pokój dzienny + kuchnia + jadalnia&apos;,V6__c,0), 
MAX(IF(R7__c==&apos;pokój dzienny + kuchnia + jadalnia&apos;,V7__c,0), 
MAX(IF(R8__c==&apos;pokój dzienny + kuchnia + jadalnia&apos;,V8__c,0), 
MAX(IF(R9__c==&apos;pokój dzienny + kuchnia + jadalnia&apos;,V9__c,0), 
MAX(IF(R10__c==&apos;pokój dzienny + kuchnia + jadalnia&apos;,V10__c,0), 
MAX(IF(R11__c==&apos;pokój dzienny + kuchnia + jadalnia&apos;,V11__c,0), 
MAX(IF(R12__c==&apos;pokój dzienny + kuchnia + jadalnia&apos;,V12__c,0), 
MAX(IF(R13__c==&apos;pokój dzienny + kuchnia + jadalnia&apos;,V13__c,0), 
MAX(IF(R14__c==&apos;pokój dzienny + kuchnia + jadalnia&apos;,V14__c,0), 
MAX(IF(R15__c==&apos;pokój dzienny + kuchnia + jadalnia&apos;,V15__c,0), 
MAX(IF(R16__c==&apos;pokój dzienny + kuchnia + jadalnia&apos;,V16__c,0), 
MAX(IF(R17__c==&apos;pokój dzienny + kuchnia + jadalnia&apos;,V17__c,0), 
MAX(IF(R18__c==&apos;pokój dzienny + kuchnia + jadalnia&apos;,V18__c,0), 
MAX(IF(R19__c==&apos;pokój dzienny + kuchnia + jadalnia&apos;,V19__c,0), 
MAX(IF(R20__c==&apos;pokój dzienny + kuchnia + jadalnia&apos;,V20__c,0), 
MAX(IF(R21__c==&apos;pokój dzienny + kuchnia + jadalnia&apos;,V21__c,0), 
MAX(IF(R22__c==&apos;pokój dzienny + kuchnia + jadalnia&apos;,V22__c,0), 
MAX(IF(R23__c==&apos;pokój dzienny + kuchnia + jadalnia&apos;,V23__c,0), 
MAX(IF(R24__c==&apos;pokój dzienny + kuchnia + jadalnia&apos;,V24__c,0), 
MAX(IF(R25__c==&apos;pokój dzienny + kuchnia + jadalnia&apos;,V25__c,0), 
MAX(IF(R26__c==&apos;pokój dzienny + kuchnia + jadalnia&apos;,V26__c,0), 
MAX(IF(R27__c==&apos;pokój dzienny + kuchnia + jadalnia&apos;,V27__c,0), 
MAX(IF(R28__c==&apos;pokój dzienny + kuchnia + jadalnia&apos;,V28__c,0), 
MAX(IF(R29__c==&apos;pokój dzienny + kuchnia + jadalnia&apos;,V29__c,0), 
MAX(IF(R30__c==&apos;pokój dzienny + kuchnia + jadalnia&apos;,V30__c,0), 
MAX(IF(R31__c==&apos;pokój dzienny + kuchnia + jadalnia&apos;,V31__c,0), 
MAX(IF(R32__c==&apos;pokój dzienny + kuchnia + jadalnia&apos;,V32__c,0), 
MAX(IF(R33__c==&apos;pokój dzienny + kuchnia + jadalnia&apos;,V33__c,0), 
MAX(IF(R34__c==&apos;pokój dzienny + kuchnia + jadalnia&apos;,V34__c,0), 
MAX(IF(R35__c==&apos;pokój dzienny + kuchnia + jadalnia&apos;,V35__c,0), 
MAX(IF(R36__c==&apos;pokój dzienny + kuchnia + jadalnia&apos;,V36__c,0), 
MAX(IF(R37__c==&apos;pokój dzienny + kuchnia + jadalnia&apos;,V37__c,0), 
MAX(IF(R38__c==&apos;pokój dzienny + kuchnia + jadalnia&apos;,V38__c,0), 
MAX(IF(R39__c==&apos;pokój dzienny + kuchnia + jadalnia&apos;,V39__c,0), 
MAX(IF(R40__c==&apos;pokój dzienny + kuchnia + jadalnia&apos;,V40__c,0),0)
)))))))))))))))))))))))))))))))))))))))</formula>
        <name>Design: Kitch-Din-Livin Usable Area - GF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Design__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Kitchen_Dining_Usable_Area_GF</fullName>
        <field>Kitchen_Dining_Usable_Area__c</field>
        <formula>MAX(IF(R1__c==&apos;kuchnia + jadalnia&apos;,V1__c,0), 
MAX(IF(R2__c==&apos;kuchnia + jadalnia&apos;,V2__c,0), 
MAX(IF(R3__c==&apos;kuchnia + jadalnia&apos;,V3__c,0), 
MAX(IF(R4__c==&apos;kuchnia + jadalnia&apos;,V4__c,0), 
MAX(IF(R5__c==&apos;kuchnia + jadalnia&apos;,V5__c,0), 
MAX(IF(R6__c==&apos;kuchnia + jadalnia&apos;,V6__c,0), 
MAX(IF(R7__c==&apos;kuchnia + jadalnia&apos;,V7__c,0), 
MAX(IF(R8__c==&apos;kuchnia + jadalnia&apos;,V8__c,0), 
MAX(IF(R9__c==&apos;kuchnia + jadalnia&apos;,V9__c,0), 
MAX(IF(R10__c==&apos;kuchnia + jadalnia&apos;,V10__c,0), 
MAX(IF(R11__c==&apos;kuchnia + jadalnia&apos;,V11__c,0), 
MAX(IF(R12__c==&apos;kuchnia + jadalnia&apos;,V12__c,0), 
MAX(IF(R13__c==&apos;kuchnia + jadalnia&apos;,V13__c,0), 
MAX(IF(R14__c==&apos;kuchnia + jadalnia&apos;,V14__c,0), 
MAX(IF(R15__c==&apos;kuchnia + jadalnia&apos;,V15__c,0), 
MAX(IF(R16__c==&apos;kuchnia + jadalnia&apos;,V16__c,0), 
MAX(IF(R17__c==&apos;kuchnia + jadalnia&apos;,V17__c,0), 
MAX(IF(R18__c==&apos;kuchnia + jadalnia&apos;,V18__c,0), 
MAX(IF(R19__c==&apos;kuchnia + jadalnia&apos;,V19__c,0), 
MAX(IF(R20__c==&apos;kuchnia + jadalnia&apos;,V20__c,0), 
MAX(IF(R21__c==&apos;kuchnia + jadalnia&apos;,V21__c,0), 
MAX(IF(R22__c==&apos;kuchnia + jadalnia&apos;,V22__c,0), 
MAX(IF(R23__c==&apos;kuchnia + jadalnia&apos;,V23__c,0), 
MAX(IF(R24__c==&apos;kuchnia + jadalnia&apos;,V24__c,0), 
MAX(IF(R25__c==&apos;kuchnia + jadalnia&apos;,V25__c,0), 
MAX(IF(R26__c==&apos;kuchnia + jadalnia&apos;,V26__c,0), 
MAX(IF(R27__c==&apos;kuchnia + jadalnia&apos;,V27__c,0), 
MAX(IF(R28__c==&apos;kuchnia + jadalnia&apos;,V28__c,0), 
MAX(IF(R29__c==&apos;kuchnia + jadalnia&apos;,V29__c,0), 
MAX(IF(R30__c==&apos;kuchnia + jadalnia&apos;,V30__c,0), 
MAX(IF(R31__c==&apos;kuchnia + jadalnia&apos;,V31__c,0), 
MAX(IF(R32__c==&apos;kuchnia + jadalnia&apos;,V32__c,0), 
MAX(IF(R33__c==&apos;kuchnia + jadalnia&apos;,V33__c,0), 
MAX(IF(R34__c==&apos;kuchnia + jadalnia&apos;,V34__c,0), 
MAX(IF(R35__c==&apos;kuchnia + jadalnia&apos;,V35__c,0), 
MAX(IF(R36__c==&apos;kuchnia + jadalnia&apos;,V36__c,0), 
MAX(IF(R37__c==&apos;kuchnia + jadalnia&apos;,V37__c,0), 
MAX(IF(R38__c==&apos;kuchnia + jadalnia&apos;,V38__c,0), 
MAX(IF(R39__c==&apos;kuchnia + jadalnia&apos;,V39__c,0), 
MAX(IF(R40__c==&apos;kuchnia + jadalnia&apos;,V40__c,0), 
MAX(IF(R41__c==&apos;kuchnia + jadalnia&apos;,V41__c,0), 
MAX(IF(R42__c==&apos;kuchnia + jadalnia&apos;,V42__c,0), 
MAX(IF(R43__c==&apos;kuchnia + jadalnia&apos;,V43__c,0), 
MAX(IF(R44__c==&apos;kuchnia + jadalnia&apos;,V44__c,0), 
MAX(IF(R45__c==&apos;kuchnia + jadalnia&apos;,V45__c,0), 
MAX(IF(R46__c==&apos;kuchnia + jadalnia&apos;,V46__c,0), 
MAX(IF(R47__c==&apos;kuchnia + jadalnia&apos;,V47__c,0), 
MAX(IF(R48__c==&apos;kuchnia + jadalnia&apos;,V48__c,0),0) 
)))))))))))))))))))))))))))))))))))))))))))))))</formula>
        <name>Design: Kitchen-Dining Usable Area - GF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Design__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Kitchen_Living_Usable_Area_GF</fullName>
        <field>Kitchen_Living_Usable_Area__c</field>
        <formula>MAX(IF(R1__c==&apos;pokój dzienny + kuchnia&apos;,V1__c,0), 
MAX(IF(R2__c==&apos;pokój dzienny + kuchnia&apos;,V2__c,0), 
MAX(IF(R3__c==&apos;pokój dzienny + kuchnia&apos;,V3__c,0), 
MAX(IF(R4__c==&apos;pokój dzienny + kuchnia&apos;,V4__c,0), 
MAX(IF(R5__c==&apos;pokój dzienny + kuchnia&apos;,V5__c,0), 
MAX(IF(R6__c==&apos;pokój dzienny + kuchnia&apos;,V6__c,0), 
MAX(IF(R7__c==&apos;pokój dzienny + kuchnia&apos;,V7__c,0), 
MAX(IF(R8__c==&apos;pokój dzienny + kuchnia&apos;,V8__c,0), 
MAX(IF(R9__c==&apos;pokój dzienny + kuchnia&apos;,V9__c,0), 
MAX(IF(R10__c==&apos;pokój dzienny + kuchnia&apos;,V10__c,0), 
MAX(IF(R11__c==&apos;pokój dzienny + kuchnia&apos;,V11__c,0), 
MAX(IF(R12__c==&apos;pokój dzienny + kuchnia&apos;,V12__c,0), 
MAX(IF(R13__c==&apos;pokój dzienny + kuchnia&apos;,V13__c,0), 
MAX(IF(R14__c==&apos;pokój dzienny + kuchnia&apos;,V14__c,0), 
MAX(IF(R15__c==&apos;pokój dzienny + kuchnia&apos;,V15__c,0), 
MAX(IF(R16__c==&apos;pokój dzienny + kuchnia&apos;,V16__c,0), 
MAX(IF(R17__c==&apos;pokój dzienny + kuchnia&apos;,V17__c,0), 
MAX(IF(R18__c==&apos;pokój dzienny + kuchnia&apos;,V18__c,0), 
MAX(IF(R19__c==&apos;pokój dzienny + kuchnia&apos;,V19__c,0), 
MAX(IF(R20__c==&apos;pokój dzienny + kuchnia&apos;,V20__c,0), 
MAX(IF(R21__c==&apos;pokój dzienny + kuchnia&apos;,V21__c,0), 
MAX(IF(R22__c==&apos;pokój dzienny + kuchnia&apos;,V22__c,0), 
MAX(IF(R23__c==&apos;pokój dzienny + kuchnia&apos;,V23__c,0), 
MAX(IF(R24__c==&apos;pokój dzienny + kuchnia&apos;,V24__c,0), 
MAX(IF(R25__c==&apos;pokój dzienny + kuchnia&apos;,V25__c,0), 
MAX(IF(R26__c==&apos;pokój dzienny + kuchnia&apos;,V26__c,0), 
MAX(IF(R27__c==&apos;pokój dzienny + kuchnia&apos;,V27__c,0), 
MAX(IF(R28__c==&apos;pokój dzienny + kuchnia&apos;,V28__c,0), 
MAX(IF(R29__c==&apos;pokój dzienny + kuchnia&apos;,V29__c,0), 
MAX(IF(R30__c==&apos;pokój dzienny + kuchnia&apos;,V30__c,0), 
MAX(IF(R31__c==&apos;pokój dzienny + kuchnia&apos;,V31__c,0), 
MAX(IF(R32__c==&apos;pokój dzienny + kuchnia&apos;,V32__c,0), 
MAX(IF(R33__c==&apos;pokój dzienny + kuchnia&apos;,V33__c,0), 
MAX(IF(R34__c==&apos;pokój dzienny + kuchnia&apos;,V34__c,0), 
MAX(IF(R35__c==&apos;pokój dzienny + kuchnia&apos;,V35__c,0), 
MAX(IF(R36__c==&apos;pokój dzienny + kuchnia&apos;,V36__c,0), 
MAX(IF(R37__c==&apos;pokój dzienny + kuchnia&apos;,V37__c,0), 
MAX(IF(R38__c==&apos;pokój dzienny + kuchnia&apos;,V38__c,0), 
MAX(IF(R39__c==&apos;pokój dzienny + kuchnia&apos;,V39__c,0), 
MAX(IF(R40__c==&apos;pokój dzienny + kuchnia&apos;,V40__c,0), 
MAX(IF(R41__c==&apos;pokój dzienny + kuchnia&apos;,V41__c,0), 
MAX(IF(R42__c==&apos;pokój dzienny + kuchnia&apos;,V42__c,0), 
MAX(IF(R43__c==&apos;pokój dzienny + kuchnia&apos;,V43__c,0), 
MAX(IF(R44__c==&apos;pokój dzienny + kuchnia&apos;,V44__c,0), 
MAX(IF(R45__c==&apos;pokój dzienny + kuchnia&apos;,V45__c,0), 
MAX(IF(R46__c==&apos;pokój dzienny + kuchnia&apos;,V46__c,0),0)
)))))))))))))))))))))))))))))))))))))))))))))</formula>
        <name>Design: Kitchen-Living Usable Area - GF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Design__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Kitchen_Pantry_Usable_Area_GF</fullName>
        <field>Kitchen_Pantry_Usable_Area__c</field>
        <formula>MAX(IF(R1__c==&apos;kuchnia + spiżarnia&apos;,V1__c,0), 
MAX(IF(R2__c==&apos;kuchnia + spiżarnia&apos;,V2__c,0), 
MAX(IF(R3__c==&apos;kuchnia + spiżarnia&apos;,V3__c,0), 
MAX(IF(R4__c==&apos;kuchnia + spiżarnia&apos;,V4__c,0), 
MAX(IF(R5__c==&apos;kuchnia + spiżarnia&apos;,V5__c,0), 
MAX(IF(R6__c==&apos;kuchnia + spiżarnia&apos;,V6__c,0), 
MAX(IF(R7__c==&apos;kuchnia + spiżarnia&apos;,V7__c,0), 
MAX(IF(R8__c==&apos;kuchnia + spiżarnia&apos;,V8__c,0), 
MAX(IF(R9__c==&apos;kuchnia + spiżarnia&apos;,V9__c,0), 
MAX(IF(R10__c==&apos;kuchnia + spiżarnia&apos;,V10__c,0), 
MAX(IF(R11__c==&apos;kuchnia + spiżarnia&apos;,V11__c,0), 
MAX(IF(R12__c==&apos;kuchnia + spiżarnia&apos;,V12__c,0), 
MAX(IF(R13__c==&apos;kuchnia + spiżarnia&apos;,V13__c,0), 
MAX(IF(R14__c==&apos;kuchnia + spiżarnia&apos;,V14__c,0), 
MAX(IF(R15__c==&apos;kuchnia + spiżarnia&apos;,V15__c,0), 
MAX(IF(R16__c==&apos;kuchnia + spiżarnia&apos;,V16__c,0), 
MAX(IF(R17__c==&apos;kuchnia + spiżarnia&apos;,V17__c,0), 
MAX(IF(R18__c==&apos;kuchnia + spiżarnia&apos;,V18__c,0), 
MAX(IF(R19__c==&apos;kuchnia + spiżarnia&apos;,V19__c,0), 
MAX(IF(R20__c==&apos;kuchnia + spiżarnia&apos;,V20__c,0), 
MAX(IF(R21__c==&apos;kuchnia + spiżarnia&apos;,V21__c,0), 
MAX(IF(R22__c==&apos;kuchnia + spiżarnia&apos;,V22__c,0), 
MAX(IF(R23__c==&apos;kuchnia + spiżarnia&apos;,V23__c,0), 
MAX(IF(R24__c==&apos;kuchnia + spiżarnia&apos;,V24__c,0), 
MAX(IF(R25__c==&apos;kuchnia + spiżarnia&apos;,V25__c,0), 
MAX(IF(R26__c==&apos;kuchnia + spiżarnia&apos;,V26__c,0), 
MAX(IF(R27__c==&apos;kuchnia + spiżarnia&apos;,V27__c,0), 
MAX(IF(R28__c==&apos;kuchnia + spiżarnia&apos;,V28__c,0), 
MAX(IF(R29__c==&apos;kuchnia + spiżarnia&apos;,V29__c,0), 
MAX(IF(R30__c==&apos;kuchnia + spiżarnia&apos;,V30__c,0), 
MAX(IF(R31__c==&apos;kuchnia + spiżarnia&apos;,V31__c,0), 
MAX(IF(R32__c==&apos;kuchnia + spiżarnia&apos;,V32__c,0), 
MAX(IF(R33__c==&apos;kuchnia + spiżarnia&apos;,V33__c,0), 
MAX(IF(R34__c==&apos;kuchnia + spiżarnia&apos;,V34__c,0), 
MAX(IF(R35__c==&apos;kuchnia + spiżarnia&apos;,V35__c,0), 
MAX(IF(R36__c==&apos;kuchnia + spiżarnia&apos;,V36__c,0), 
MAX(IF(R37__c==&apos;kuchnia + spiżarnia&apos;,V37__c,0), 
MAX(IF(R38__c==&apos;kuchnia + spiżarnia&apos;,V38__c,0), 
MAX(IF(R39__c==&apos;kuchnia + spiżarnia&apos;,V39__c,0), 
MAX(IF(R40__c==&apos;kuchnia + spiżarnia&apos;,V40__c,0), 
MAX(IF(R41__c==&apos;kuchnia + spiżarnia&apos;,V41__c,0), 
MAX(IF(R42__c==&apos;kuchnia + spiżarnia&apos;,V42__c,0), 
MAX(IF(R43__c==&apos;kuchnia + spiżarnia&apos;,V43__c,0), 
MAX(IF(R44__c==&apos;kuchnia + spiżarnia&apos;,V44__c,0), 
MAX(IF(R45__c==&apos;kuchnia + spiżarnia&apos;,V45__c,0), 
MAX(IF(R46__c==&apos;kuchnia + spiżarnia&apos;,V46__c,0), 
MAX(IF(R47__c==&apos;kuchnia + spiżarnia&apos;,V47__c,0), 
MAX(IF(R48__c==&apos;kuchnia + spiżarnia&apos;,V48__c,0),0) 
)))))))))))))))))))))))))))))))))))))))))))))))</formula>
        <name>Design: Kitchen-Pantry Usable Area - GF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Design__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Kitchen_Usable_Area_GF</fullName>
        <field>Kitchen_Usable_Area__c</field>
        <formula>MAX(IF(R1__c==&apos;kuchnia&apos;,V1__c,0), 
MAX(IF(R2__c==&apos;kuchnia&apos;,V2__c,0), 
MAX(IF(R3__c==&apos;kuchnia&apos;,V3__c,0), 
MAX(IF(R4__c==&apos;kuchnia&apos;,V4__c,0), 
MAX(IF(R5__c==&apos;kuchnia&apos;,V5__c,0), 
MAX(IF(R6__c==&apos;kuchnia&apos;,V6__c,0), 
MAX(IF(R7__c==&apos;kuchnia&apos;,V7__c,0), 
MAX(IF(R8__c==&apos;kuchnia&apos;,V8__c,0), 
MAX(IF(R9__c==&apos;kuchnia&apos;,V9__c,0), 
MAX(IF(R10__c==&apos;kuchnia&apos;,V10__c,0), 
MAX(IF(R11__c==&apos;kuchnia&apos;,V11__c,0), 
MAX(IF(R12__c==&apos;kuchnia&apos;,V12__c,0), 
MAX(IF(R13__c==&apos;kuchnia&apos;,V13__c,0), 
MAX(IF(R14__c==&apos;kuchnia&apos;,V14__c,0), 
MAX(IF(R15__c==&apos;kuchnia&apos;,V15__c,0), 
MAX(IF(R16__c==&apos;kuchnia&apos;,V16__c,0), 
MAX(IF(R17__c==&apos;kuchnia&apos;,V17__c,0), 
MAX(IF(R18__c==&apos;kuchnia&apos;,V18__c,0), 
MAX(IF(R19__c==&apos;kuchnia&apos;,V19__c,0), 
MAX(IF(R20__c==&apos;kuchnia&apos;,V20__c,0), 
MAX(IF(R21__c==&apos;kuchnia&apos;,V21__c,0), 
MAX(IF(R22__c==&apos;kuchnia&apos;,V22__c,0), 
MAX(IF(R23__c==&apos;kuchnia&apos;,V23__c,0), 
MAX(IF(R24__c==&apos;kuchnia&apos;,V24__c,0), 
MAX(IF(R25__c==&apos;kuchnia&apos;,V25__c,0), 
MAX(IF(R26__c==&apos;kuchnia&apos;,V26__c,0), 
MAX(IF(R27__c==&apos;kuchnia&apos;,V27__c,0), 
MAX(IF(R28__c==&apos;kuchnia&apos;,V28__c,0), 
MAX(IF(R29__c==&apos;kuchnia&apos;,V29__c,0), 
MAX(IF(R30__c==&apos;kuchnia&apos;,V30__c,0), 
MAX(IF(R31__c==&apos;kuchnia&apos;,V31__c,0), 
MAX(IF(R32__c==&apos;kuchnia&apos;,V32__c,0), 
MAX(IF(R33__c==&apos;kuchnia&apos;,V33__c,0), 
MAX(IF(R34__c==&apos;kuchnia&apos;,V34__c,0), 
MAX(IF(R35__c==&apos;kuchnia&apos;,V35__c,0), 
MAX(IF(R36__c==&apos;kuchnia&apos;,V36__c,0), 
MAX(IF(R37__c==&apos;kuchnia&apos;,V37__c,0), 
MAX(IF(R38__c==&apos;kuchnia&apos;,V38__c,0), 
MAX(IF(R39__c==&apos;kuchnia&apos;,V39__c,0), 
MAX(IF(R40__c==&apos;kuchnia&apos;,V40__c,0), 
MAX(IF(R41__c==&apos;kuchnia&apos;,V41__c,0), 
MAX(IF(R42__c==&apos;kuchnia&apos;,V42__c,0), 
MAX(IF(R43__c==&apos;kuchnia&apos;,V43__c,0), 
MAX(IF(R44__c==&apos;kuchnia&apos;,V44__c,0), 
MAX(IF(R45__c==&apos;kuchnia&apos;,V45__c,0), 
MAX(IF(R46__c==&apos;kuchnia&apos;,V46__c,0), 
MAX(IF(R47__c==&apos;kuchnia&apos;,V47__c,0), 
MAX(IF(R48__c==&apos;kuchnia&apos;,V48__c,0),0) 
)))))))))))))))))))))))))))))))))))))))))))))))</formula>
        <name>Design: Kitchen Usable Area - GF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Design__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Living_Usable_Area_GF</fullName>
        <field>Living_Usable_Area__c</field>
        <formula>MAX(IF(R1__c==&apos;pokój dzienny&apos;,V1__c,0), 
MAX(IF(R2__c==&apos;pokój dzienny&apos;,V2__c,0), 
MAX(IF(R3__c==&apos;pokój dzienny&apos;,V3__c,0), 
MAX(IF(R4__c==&apos;pokój dzienny&apos;,V4__c,0), 
MAX(IF(R5__c==&apos;pokój dzienny&apos;,V5__c,0), 
MAX(IF(R6__c==&apos;pokój dzienny&apos;,V6__c,0), 
MAX(IF(R7__c==&apos;pokój dzienny&apos;,V7__c,0), 
MAX(IF(R8__c==&apos;pokój dzienny&apos;,V8__c,0), 
MAX(IF(R9__c==&apos;pokój dzienny&apos;,V9__c,0), 
MAX(IF(R10__c==&apos;pokój dzienny&apos;,V10__c,0), 
MAX(IF(R11__c==&apos;pokój dzienny&apos;,V11__c,0), 
MAX(IF(R12__c==&apos;pokój dzienny&apos;,V12__c,0), 
MAX(IF(R13__c==&apos;pokój dzienny&apos;,V13__c,0), 
MAX(IF(R14__c==&apos;pokój dzienny&apos;,V14__c,0), 
MAX(IF(R15__c==&apos;pokój dzienny&apos;,V15__c,0), 
MAX(IF(R16__c==&apos;pokój dzienny&apos;,V16__c,0), 
MAX(IF(R17__c==&apos;pokój dzienny&apos;,V17__c,0), 
MAX(IF(R18__c==&apos;pokój dzienny&apos;,V18__c,0), 
MAX(IF(R19__c==&apos;pokój dzienny&apos;,V19__c,0), 
MAX(IF(R20__c==&apos;pokój dzienny&apos;,V20__c,0), 
MAX(IF(R21__c==&apos;pokój dzienny&apos;,V21__c,0), 
MAX(IF(R22__c==&apos;pokój dzienny&apos;,V22__c,0), 
MAX(IF(R23__c==&apos;pokój dzienny&apos;,V23__c,0), 
MAX(IF(R24__c==&apos;pokój dzienny&apos;,V24__c,0), 
MAX(IF(R25__c==&apos;pokój dzienny&apos;,V25__c,0), 
MAX(IF(R26__c==&apos;pokój dzienny&apos;,V26__c,0), 
MAX(IF(R27__c==&apos;pokój dzienny&apos;,V27__c,0), 
MAX(IF(R28__c==&apos;pokój dzienny&apos;,V28__c,0), 
MAX(IF(R29__c==&apos;pokój dzienny&apos;,V29__c,0), 
MAX(IF(R30__c==&apos;pokój dzienny&apos;,V30__c,0), 
MAX(IF(R31__c==&apos;pokój dzienny&apos;,V31__c,0), 
MAX(IF(R32__c==&apos;pokój dzienny&apos;,V32__c,0), 
MAX(IF(R33__c==&apos;pokój dzienny&apos;,V33__c,0), 
MAX(IF(R34__c==&apos;pokój dzienny&apos;,V34__c,0), 
MAX(IF(R35__c==&apos;pokój dzienny&apos;,V35__c,0), 
MAX(IF(R36__c==&apos;pokój dzienny&apos;,V36__c,0), 
MAX(IF(R37__c==&apos;pokój dzienny&apos;,V37__c,0), 
MAX(IF(R38__c==&apos;pokój dzienny&apos;,V38__c,0), 
MAX(IF(R39__c==&apos;pokój dzienny&apos;,V39__c,0), 
MAX(IF(R40__c==&apos;pokój dzienny&apos;,V40__c,0), 
MAX(IF(R41__c==&apos;pokój dzienny&apos;,V41__c,0), 
MAX(IF(R42__c==&apos;pokój dzienny&apos;,V42__c,0), 
MAX(IF(R43__c==&apos;pokój dzienny&apos;,V43__c,0), 
MAX(IF(R44__c==&apos;pokój dzienny&apos;,V44__c,0), 
MAX(IF(R45__c==&apos;pokój dzienny&apos;,V45__c,0), 
MAX(IF(R46__c==&apos;pokój dzienny&apos;,V46__c,0), 
MAX(IF(R47__c==&apos;pokój dzienny&apos;,V47__c,0), 
MAX(IF(R48__c==&apos;pokój dzienny&apos;,V48__c,0),0) 
)))))))))))))))))))))))))))))))))))))))))))))))</formula>
        <name>Design: Living Usable Area - GF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Design__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Master_Bathroom_Usable_Area_1F</fullName>
        <field>Master_Bathroom_Usable_Area_1st_Floor__c</field>
        <formula>MAX(IF(R1__c==&apos;łazienka&apos;,V1__c,0), 
MAX(IF(R2__c==&apos;łazienka&apos;,V2__c,0), 
MAX(IF(R3__c==&apos;łazienka&apos;,V3__c,0), 
MAX(IF(R4__c==&apos;łazienka&apos;,V4__c,0), 
MAX(IF(R5__c==&apos;łazienka&apos;,V5__c,0), 
MAX(IF(R6__c==&apos;łazienka&apos;,V6__c,0), 
MAX(IF(R7__c==&apos;łazienka&apos;,V7__c,0), 
MAX(IF(R8__c==&apos;łazienka&apos;,V8__c,0), 
MAX(IF(R9__c==&apos;łazienka&apos;,V9__c,0), 
MAX(IF(R10__c==&apos;łazienka&apos;,V10__c,0), 
MAX(IF(R11__c==&apos;łazienka&apos;,V11__c,0), 
MAX(IF(R12__c==&apos;łazienka&apos;,V12__c,0), 
MAX(IF(R13__c==&apos;łazienka&apos;,V13__c,0), 
MAX(IF(R14__c==&apos;łazienka&apos;,V14__c,0), 
MAX(IF(R15__c==&apos;łazienka&apos;,V15__c,0), 
MAX(IF(R16__c==&apos;łazienka&apos;,V16__c,0), 
MAX(IF(R17__c==&apos;łazienka&apos;,V17__c,0), 
MAX(IF(R18__c==&apos;łazienka&apos;,V18__c,0), 
MAX(IF(R19__c==&apos;łazienka&apos;,V19__c,0), 
MAX(IF(R20__c==&apos;łazienka&apos;,V20__c,0), 
MAX(IF(R21__c==&apos;łazienka&apos;,V21__c,0), 
MAX(IF(R22__c==&apos;łazienka&apos;,V22__c,0), 
MAX(IF(R23__c==&apos;łazienka&apos;,V23__c,0), 
MAX(IF(R24__c==&apos;łazienka&apos;,V24__c,0), 
MAX(IF(R25__c==&apos;łazienka&apos;,V25__c,0), 
MAX(IF(R26__c==&apos;łazienka&apos;,V26__c,0), 
MAX(IF(R27__c==&apos;łazienka&apos;,V27__c,0), 
MAX(IF(R28__c==&apos;łazienka&apos;,V28__c,0), 
MAX(IF(R29__c==&apos;łazienka&apos;,V29__c,0), 
MAX(IF(R30__c==&apos;łazienka&apos;,V30__c,0), 
MAX(IF(R31__c==&apos;łazienka&apos;,V31__c,0), 
MAX(IF(R32__c==&apos;łazienka&apos;,V32__c,0), 
MAX(IF(R33__c==&apos;łazienka&apos;,V33__c,0), 
MAX(IF(R34__c==&apos;łazienka&apos;,V34__c,0), 
MAX(IF(R35__c==&apos;łazienka&apos;,V35__c,0), 
MAX(IF(R36__c==&apos;łazienka&apos;,V36__c,0), 
MAX(IF(R37__c==&apos;łazienka&apos;,V37__c,0), 
MAX(IF(R38__c==&apos;łazienka&apos;,V38__c,0), 
MAX(IF(R39__c==&apos;łazienka&apos;,V39__c,0), 
MAX(IF(R40__c==&apos;łazienka&apos;,V40__c,0), 
MAX(IF(R41__c==&apos;łazienka&apos;,V41__c,0), 
MAX(IF(R42__c==&apos;łazienka&apos;,V42__c,0), 
MAX(IF(R43__c==&apos;łazienka&apos;,V43__c,0), 
MAX(IF(R44__c==&apos;łazienka&apos;,V44__c,0), 
MAX(IF(R45__c==&apos;łazienka&apos;,V45__c,0), 
MAX(IF(R46__c==&apos;łazienka&apos;,V46__c,0), 
MAX(IF(R47__c==&apos;łazienka&apos;,V47__c,0), 
MAX(IF(R48__c==&apos;łazienka&apos;,V48__c,0), 
MAX(IF(R49__c==&apos;łazienka&apos;,V49__c,0), 
MAX(IF(R50__c==&apos;łazienka&apos;,V50__c,0),0) 
)))))))))))))))))))))))))))))))))))))))))))))))))</formula>
        <name>Design: Master Bathroom Usable Area - 1F</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Design__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Master_Bathroom_Usable_Area_AT</fullName>
        <field>Master_Bathroom_Usable_Area_Attic__c</field>
        <formula>MAX(IF(R1__c==&apos;łazienka&apos;,V1__c,0), 
MAX(IF(R2__c==&apos;łazienka&apos;,V2__c,0), 
MAX(IF(R3__c==&apos;łazienka&apos;,V3__c,0), 
MAX(IF(R4__c==&apos;łazienka&apos;,V4__c,0), 
MAX(IF(R5__c==&apos;łazienka&apos;,V5__c,0), 
MAX(IF(R6__c==&apos;łazienka&apos;,V6__c,0), 
MAX(IF(R7__c==&apos;łazienka&apos;,V7__c,0), 
MAX(IF(R8__c==&apos;łazienka&apos;,V8__c,0), 
MAX(IF(R9__c==&apos;łazienka&apos;,V9__c,0), 
MAX(IF(R10__c==&apos;łazienka&apos;,V10__c,0), 
MAX(IF(R11__c==&apos;łazienka&apos;,V11__c,0), 
MAX(IF(R12__c==&apos;łazienka&apos;,V12__c,0), 
MAX(IF(R13__c==&apos;łazienka&apos;,V13__c,0), 
MAX(IF(R14__c==&apos;łazienka&apos;,V14__c,0), 
MAX(IF(R15__c==&apos;łazienka&apos;,V15__c,0), 
MAX(IF(R16__c==&apos;łazienka&apos;,V16__c,0), 
MAX(IF(R17__c==&apos;łazienka&apos;,V17__c,0), 
MAX(IF(R18__c==&apos;łazienka&apos;,V18__c,0), 
MAX(IF(R19__c==&apos;łazienka&apos;,V19__c,0), 
MAX(IF(R20__c==&apos;łazienka&apos;,V20__c,0), 
MAX(IF(R21__c==&apos;łazienka&apos;,V21__c,0), 
MAX(IF(R22__c==&apos;łazienka&apos;,V22__c,0), 
MAX(IF(R23__c==&apos;łazienka&apos;,V23__c,0), 
MAX(IF(R24__c==&apos;łazienka&apos;,V24__c,0), 
MAX(IF(R25__c==&apos;łazienka&apos;,V25__c,0), 
MAX(IF(R26__c==&apos;łazienka&apos;,V26__c,0), 
MAX(IF(R27__c==&apos;łazienka&apos;,V27__c,0), 
MAX(IF(R28__c==&apos;łazienka&apos;,V28__c,0), 
MAX(IF(R29__c==&apos;łazienka&apos;,V29__c,0), 
MAX(IF(R30__c==&apos;łazienka&apos;,V30__c,0), 
MAX(IF(R31__c==&apos;łazienka&apos;,V31__c,0), 
MAX(IF(R32__c==&apos;łazienka&apos;,V32__c,0), 
MAX(IF(R33__c==&apos;łazienka&apos;,V33__c,0), 
MAX(IF(R34__c==&apos;łazienka&apos;,V34__c,0), 
MAX(IF(R35__c==&apos;łazienka&apos;,V35__c,0), 
MAX(IF(R36__c==&apos;łazienka&apos;,V36__c,0), 
MAX(IF(R37__c==&apos;łazienka&apos;,V37__c,0), 
MAX(IF(R38__c==&apos;łazienka&apos;,V38__c,0), 
MAX(IF(R39__c==&apos;łazienka&apos;,V39__c,0), 
MAX(IF(R40__c==&apos;łazienka&apos;,V40__c,0), 
MAX(IF(R41__c==&apos;łazienka&apos;,V41__c,0), 
MAX(IF(R42__c==&apos;łazienka&apos;,V42__c,0), 
MAX(IF(R43__c==&apos;łazienka&apos;,V43__c,0), 
MAX(IF(R44__c==&apos;łazienka&apos;,V44__c,0), 
MAX(IF(R45__c==&apos;łazienka&apos;,V45__c,0), 
MAX(IF(R46__c==&apos;łazienka&apos;,V46__c,0), 
MAX(IF(R47__c==&apos;łazienka&apos;,V47__c,0), 
MAX(IF(R48__c==&apos;łazienka&apos;,V48__c,0), 
MAX(IF(R49__c==&apos;łazienka&apos;,V49__c,0), 
MAX(IF(R50__c==&apos;łazienka&apos;,V50__c,0),0) 
)))))))))))))))))))))))))))))))))))))))))))))))))</formula>
        <name>Design: Master Bathroom Usable Area - AT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Design__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Master_Bathroom_Usable_Area_GF</fullName>
        <field>Master_Bathroom_Usable_Area_Ground_Floor__c</field>
        <formula>MAX(IF(R1__c==&apos;łazienka&apos;,V1__c,0), 
MAX(IF(R2__c==&apos;łazienka&apos;,V2__c,0), 
MAX(IF(R3__c==&apos;łazienka&apos;,V3__c,0), 
MAX(IF(R4__c==&apos;łazienka&apos;,V4__c,0), 
MAX(IF(R5__c==&apos;łazienka&apos;,V5__c,0), 
MAX(IF(R6__c==&apos;łazienka&apos;,V6__c,0), 
MAX(IF(R7__c==&apos;łazienka&apos;,V7__c,0), 
MAX(IF(R8__c==&apos;łazienka&apos;,V8__c,0), 
MAX(IF(R9__c==&apos;łazienka&apos;,V9__c,0), 
MAX(IF(R10__c==&apos;łazienka&apos;,V10__c,0), 
MAX(IF(R11__c==&apos;łazienka&apos;,V11__c,0), 
MAX(IF(R12__c==&apos;łazienka&apos;,V12__c,0), 
MAX(IF(R13__c==&apos;łazienka&apos;,V13__c,0), 
MAX(IF(R14__c==&apos;łazienka&apos;,V14__c,0), 
MAX(IF(R15__c==&apos;łazienka&apos;,V15__c,0), 
MAX(IF(R16__c==&apos;łazienka&apos;,V16__c,0), 
MAX(IF(R17__c==&apos;łazienka&apos;,V17__c,0), 
MAX(IF(R18__c==&apos;łazienka&apos;,V18__c,0), 
MAX(IF(R19__c==&apos;łazienka&apos;,V19__c,0), 
MAX(IF(R20__c==&apos;łazienka&apos;,V20__c,0), 
MAX(IF(R21__c==&apos;łazienka&apos;,V21__c,0), 
MAX(IF(R22__c==&apos;łazienka&apos;,V22__c,0), 
MAX(IF(R23__c==&apos;łazienka&apos;,V23__c,0), 
MAX(IF(R24__c==&apos;łazienka&apos;,V24__c,0), 
MAX(IF(R25__c==&apos;łazienka&apos;,V25__c,0), 
MAX(IF(R26__c==&apos;łazienka&apos;,V26__c,0), 
MAX(IF(R27__c==&apos;łazienka&apos;,V27__c,0), 
MAX(IF(R28__c==&apos;łazienka&apos;,V28__c,0), 
MAX(IF(R29__c==&apos;łazienka&apos;,V29__c,0), 
MAX(IF(R30__c==&apos;łazienka&apos;,V30__c,0), 
MAX(IF(R31__c==&apos;łazienka&apos;,V31__c,0), 
MAX(IF(R32__c==&apos;łazienka&apos;,V32__c,0), 
MAX(IF(R33__c==&apos;łazienka&apos;,V33__c,0), 
MAX(IF(R34__c==&apos;łazienka&apos;,V34__c,0), 
MAX(IF(R35__c==&apos;łazienka&apos;,V35__c,0), 
MAX(IF(R36__c==&apos;łazienka&apos;,V36__c,0), 
MAX(IF(R37__c==&apos;łazienka&apos;,V37__c,0), 
MAX(IF(R38__c==&apos;łazienka&apos;,V38__c,0), 
MAX(IF(R39__c==&apos;łazienka&apos;,V39__c,0), 
MAX(IF(R40__c==&apos;łazienka&apos;,V40__c,0), 
MAX(IF(R41__c==&apos;łazienka&apos;,V41__c,0), 
MAX(IF(R42__c==&apos;łazienka&apos;,V42__c,0), 
MAX(IF(R43__c==&apos;łazienka&apos;,V43__c,0), 
MAX(IF(R44__c==&apos;łazienka&apos;,V44__c,0), 
MAX(IF(R45__c==&apos;łazienka&apos;,V45__c,0), 
MAX(IF(R46__c==&apos;łazienka&apos;,V46__c,0), 
MAX(IF(R47__c==&apos;łazienka&apos;,V47__c,0), 
MAX(IF(R48__c==&apos;łazienka&apos;,V48__c,0), 
MAX(IF(R49__c==&apos;łazienka&apos;,V49__c,0), 
MAX(IF(R50__c==&apos;łazienka&apos;,V50__c,0),0) 
)))))))))))))))))))))))))))))))))))))))))))))))))</formula>
        <name>Design: Master Bathroom Usable Area - GF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Design__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Master_Bed_Ward_Usable_Area_1F</fullName>
        <field>Master_Bed_Ward_Usable_Area_1st_Floor__c</field>
        <formula>MAX(IF(R1__c==&apos;pokój + garderoba&apos;,V1__c,0), 
MAX(IF(R2__c==&apos;pokój + garderoba&apos;,V2__c,0), 
MAX(IF(R3__c==&apos;pokój + garderoba&apos;,V3__c,0), 
MAX(IF(R4__c==&apos;pokój + garderoba&apos;,V4__c,0), 
MAX(IF(R5__c==&apos;pokój + garderoba&apos;,V5__c,0), 
MAX(IF(R6__c==&apos;pokój + garderoba&apos;,V6__c,0), 
MAX(IF(R7__c==&apos;pokój + garderoba&apos;,V7__c,0), 
MAX(IF(R8__c==&apos;pokój + garderoba&apos;,V8__c,0), 
MAX(IF(R9__c==&apos;pokój + garderoba&apos;,V9__c,0), 
MAX(IF(R10__c==&apos;pokój + garderoba&apos;,V10__c,0), 
MAX(IF(R11__c==&apos;pokój + garderoba&apos;,V11__c,0), 
MAX(IF(R12__c==&apos;pokój + garderoba&apos;,V12__c,0), 
MAX(IF(R13__c==&apos;pokój + garderoba&apos;,V13__c,0), 
MAX(IF(R14__c==&apos;pokój + garderoba&apos;,V14__c,0), 
MAX(IF(R15__c==&apos;pokój + garderoba&apos;,V15__c,0), 
MAX(IF(R16__c==&apos;pokój + garderoba&apos;,V16__c,0), 
MAX(IF(R17__c==&apos;pokój + garderoba&apos;,V17__c,0), 
MAX(IF(R18__c==&apos;pokój + garderoba&apos;,V18__c,0), 
MAX(IF(R19__c==&apos;pokój + garderoba&apos;,V19__c,0), 
MAX(IF(R20__c==&apos;pokój + garderoba&apos;,V20__c,0), 
MAX(IF(R21__c==&apos;pokój + garderoba&apos;,V21__c,0), 
MAX(IF(R22__c==&apos;pokój + garderoba&apos;,V22__c,0), 
MAX(IF(R23__c==&apos;pokój + garderoba&apos;,V23__c,0), 
MAX(IF(R24__c==&apos;pokój + garderoba&apos;,V24__c,0), 
MAX(IF(R25__c==&apos;pokój + garderoba&apos;,V25__c,0), 
MAX(IF(R26__c==&apos;pokój + garderoba&apos;,V26__c,0), 
MAX(IF(R27__c==&apos;pokój + garderoba&apos;,V27__c,0), 
MAX(IF(R28__c==&apos;pokój + garderoba&apos;,V28__c,0), 
MAX(IF(R29__c==&apos;pokój + garderoba&apos;,V29__c,0), 
MAX(IF(R30__c==&apos;pokój + garderoba&apos;,V30__c,0), 
MAX(IF(R31__c==&apos;pokój + garderoba&apos;,V31__c,0), 
MAX(IF(R32__c==&apos;pokój + garderoba&apos;,V32__c,0), 
MAX(IF(R33__c==&apos;pokój + garderoba&apos;,V33__c,0), 
MAX(IF(R34__c==&apos;pokój + garderoba&apos;,V34__c,0), 
MAX(IF(R35__c==&apos;pokój + garderoba&apos;,V35__c,0), 
MAX(IF(R36__c==&apos;pokój + garderoba&apos;,V36__c,0), 
MAX(IF(R37__c==&apos;pokój + garderoba&apos;,V37__c,0), 
MAX(IF(R38__c==&apos;pokój + garderoba&apos;,V38__c,0), 
MAX(IF(R39__c==&apos;pokój + garderoba&apos;,V39__c,0), 
MAX(IF(R40__c==&apos;pokój + garderoba&apos;,V40__c,0), 
MAX(IF(R41__c==&apos;pokój + garderoba&apos;,V41__c,0), 
MAX(IF(R42__c==&apos;pokój + garderoba&apos;,V42__c,0), 
MAX(IF(R43__c==&apos;pokój + garderoba&apos;,V43__c,0), 
MAX(IF(R44__c==&apos;pokój + garderoba&apos;,V44__c,0), 
MAX(IF(R45__c==&apos;pokój + garderoba&apos;,V45__c,0), 
MAX(IF(R46__c==&apos;pokój + garderoba&apos;,V46__c,0), 
MAX(IF(R47__c==&apos;pokój + garderoba&apos;,V47__c,0), 
MAX(IF(R48__c==&apos;pokój + garderoba&apos;,V48__c,0),0) 
)))))))))))))))))))))))))))))))))))))))))))))))</formula>
        <name>Design: Master Bed-Ward Usable Area - 1F</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Design__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Master_Bed_Ward_Usable_Area_AT</fullName>
        <field>Master_Bed_Ward_Usable_Area_Attic__c</field>
        <formula>MAX(IF(R1__c==&apos;pokój + garderoba&apos;,V1__c,0), 
MAX(IF(R2__c==&apos;pokój + garderoba&apos;,V2__c,0), 
MAX(IF(R3__c==&apos;pokój + garderoba&apos;,V3__c,0), 
MAX(IF(R4__c==&apos;pokój + garderoba&apos;,V4__c,0), 
MAX(IF(R5__c==&apos;pokój + garderoba&apos;,V5__c,0), 
MAX(IF(R6__c==&apos;pokój + garderoba&apos;,V6__c,0), 
MAX(IF(R7__c==&apos;pokój + garderoba&apos;,V7__c,0), 
MAX(IF(R8__c==&apos;pokój + garderoba&apos;,V8__c,0), 
MAX(IF(R9__c==&apos;pokój + garderoba&apos;,V9__c,0), 
MAX(IF(R10__c==&apos;pokój + garderoba&apos;,V10__c,0), 
MAX(IF(R11__c==&apos;pokój + garderoba&apos;,V11__c,0), 
MAX(IF(R12__c==&apos;pokój + garderoba&apos;,V12__c,0), 
MAX(IF(R13__c==&apos;pokój + garderoba&apos;,V13__c,0), 
MAX(IF(R14__c==&apos;pokój + garderoba&apos;,V14__c,0), 
MAX(IF(R15__c==&apos;pokój + garderoba&apos;,V15__c,0), 
MAX(IF(R16__c==&apos;pokój + garderoba&apos;,V16__c,0), 
MAX(IF(R17__c==&apos;pokój + garderoba&apos;,V17__c,0), 
MAX(IF(R18__c==&apos;pokój + garderoba&apos;,V18__c,0), 
MAX(IF(R19__c==&apos;pokój + garderoba&apos;,V19__c,0), 
MAX(IF(R20__c==&apos;pokój + garderoba&apos;,V20__c,0), 
MAX(IF(R21__c==&apos;pokój + garderoba&apos;,V21__c,0), 
MAX(IF(R22__c==&apos;pokój + garderoba&apos;,V22__c,0), 
MAX(IF(R23__c==&apos;pokój + garderoba&apos;,V23__c,0), 
MAX(IF(R24__c==&apos;pokój + garderoba&apos;,V24__c,0), 
MAX(IF(R25__c==&apos;pokój + garderoba&apos;,V25__c,0), 
MAX(IF(R26__c==&apos;pokój + garderoba&apos;,V26__c,0), 
MAX(IF(R27__c==&apos;pokój + garderoba&apos;,V27__c,0), 
MAX(IF(R28__c==&apos;pokój + garderoba&apos;,V28__c,0), 
MAX(IF(R29__c==&apos;pokój + garderoba&apos;,V29__c,0), 
MAX(IF(R30__c==&apos;pokój + garderoba&apos;,V30__c,0), 
MAX(IF(R31__c==&apos;pokój + garderoba&apos;,V31__c,0), 
MAX(IF(R32__c==&apos;pokój + garderoba&apos;,V32__c,0), 
MAX(IF(R33__c==&apos;pokój + garderoba&apos;,V33__c,0), 
MAX(IF(R34__c==&apos;pokój + garderoba&apos;,V34__c,0), 
MAX(IF(R35__c==&apos;pokój + garderoba&apos;,V35__c,0), 
MAX(IF(R36__c==&apos;pokój + garderoba&apos;,V36__c,0), 
MAX(IF(R37__c==&apos;pokój + garderoba&apos;,V37__c,0), 
MAX(IF(R38__c==&apos;pokój + garderoba&apos;,V38__c,0), 
MAX(IF(R39__c==&apos;pokój + garderoba&apos;,V39__c,0), 
MAX(IF(R40__c==&apos;pokój + garderoba&apos;,V40__c,0), 
MAX(IF(R41__c==&apos;pokój + garderoba&apos;,V41__c,0), 
MAX(IF(R42__c==&apos;pokój + garderoba&apos;,V42__c,0), 
MAX(IF(R43__c==&apos;pokój + garderoba&apos;,V43__c,0), 
MAX(IF(R44__c==&apos;pokój + garderoba&apos;,V44__c,0), 
MAX(IF(R45__c==&apos;pokój + garderoba&apos;,V45__c,0), 
MAX(IF(R46__c==&apos;pokój + garderoba&apos;,V46__c,0), 
MAX(IF(R47__c==&apos;pokój + garderoba&apos;,V47__c,0), 
MAX(IF(R48__c==&apos;pokój + garderoba&apos;,V48__c,0),0) 
)))))))))))))))))))))))))))))))))))))))))))))))</formula>
        <name>Design: Master Bed-Ward Usable Area - AT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Design__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Master_Bed_Ward_Usable_Area_GF</fullName>
        <field>Master_Bed_Ward_Usable_Area_Ground_Floor__c</field>
        <formula>MAX(IF(R1__c==&apos;pokój + garderoba&apos;,V1__c,0), 
MAX(IF(R2__c==&apos;pokój + garderoba&apos;,V2__c,0), 
MAX(IF(R3__c==&apos;pokój + garderoba&apos;,V3__c,0), 
MAX(IF(R4__c==&apos;pokój + garderoba&apos;,V4__c,0), 
MAX(IF(R5__c==&apos;pokój + garderoba&apos;,V5__c,0), 
MAX(IF(R6__c==&apos;pokój + garderoba&apos;,V6__c,0), 
MAX(IF(R7__c==&apos;pokój + garderoba&apos;,V7__c,0), 
MAX(IF(R8__c==&apos;pokój + garderoba&apos;,V8__c,0), 
MAX(IF(R9__c==&apos;pokój + garderoba&apos;,V9__c,0), 
MAX(IF(R10__c==&apos;pokój + garderoba&apos;,V10__c,0), 
MAX(IF(R11__c==&apos;pokój + garderoba&apos;,V11__c,0), 
MAX(IF(R12__c==&apos;pokój + garderoba&apos;,V12__c,0), 
MAX(IF(R13__c==&apos;pokój + garderoba&apos;,V13__c,0), 
MAX(IF(R14__c==&apos;pokój + garderoba&apos;,V14__c,0), 
MAX(IF(R15__c==&apos;pokój + garderoba&apos;,V15__c,0), 
MAX(IF(R16__c==&apos;pokój + garderoba&apos;,V16__c,0), 
MAX(IF(R17__c==&apos;pokój + garderoba&apos;,V17__c,0), 
MAX(IF(R18__c==&apos;pokój + garderoba&apos;,V18__c,0), 
MAX(IF(R19__c==&apos;pokój + garderoba&apos;,V19__c,0), 
MAX(IF(R20__c==&apos;pokój + garderoba&apos;,V20__c,0), 
MAX(IF(R21__c==&apos;pokój + garderoba&apos;,V21__c,0), 
MAX(IF(R22__c==&apos;pokój + garderoba&apos;,V22__c,0), 
MAX(IF(R23__c==&apos;pokój + garderoba&apos;,V23__c,0), 
MAX(IF(R24__c==&apos;pokój + garderoba&apos;,V24__c,0), 
MAX(IF(R25__c==&apos;pokój + garderoba&apos;,V25__c,0), 
MAX(IF(R26__c==&apos;pokój + garderoba&apos;,V26__c,0), 
MAX(IF(R27__c==&apos;pokój + garderoba&apos;,V27__c,0), 
MAX(IF(R28__c==&apos;pokój + garderoba&apos;,V28__c,0), 
MAX(IF(R29__c==&apos;pokój + garderoba&apos;,V29__c,0), 
MAX(IF(R30__c==&apos;pokój + garderoba&apos;,V30__c,0), 
MAX(IF(R31__c==&apos;pokój + garderoba&apos;,V31__c,0), 
MAX(IF(R32__c==&apos;pokój + garderoba&apos;,V32__c,0), 
MAX(IF(R33__c==&apos;pokój + garderoba&apos;,V33__c,0), 
MAX(IF(R34__c==&apos;pokój + garderoba&apos;,V34__c,0), 
MAX(IF(R35__c==&apos;pokój + garderoba&apos;,V35__c,0), 
MAX(IF(R36__c==&apos;pokój + garderoba&apos;,V36__c,0), 
MAX(IF(R37__c==&apos;pokój + garderoba&apos;,V37__c,0), 
MAX(IF(R38__c==&apos;pokój + garderoba&apos;,V38__c,0), 
MAX(IF(R39__c==&apos;pokój + garderoba&apos;,V39__c,0), 
MAX(IF(R40__c==&apos;pokój + garderoba&apos;,V40__c,0), 
MAX(IF(R41__c==&apos;pokój + garderoba&apos;,V41__c,0), 
MAX(IF(R42__c==&apos;pokój + garderoba&apos;,V42__c,0), 
MAX(IF(R43__c==&apos;pokój + garderoba&apos;,V43__c,0), 
MAX(IF(R44__c==&apos;pokój + garderoba&apos;,V44__c,0), 
MAX(IF(R45__c==&apos;pokój + garderoba&apos;,V45__c,0), 
MAX(IF(R46__c==&apos;pokój + garderoba&apos;,V46__c,0), 
MAX(IF(R47__c==&apos;pokój + garderoba&apos;,V47__c,0), 
MAX(IF(R48__c==&apos;pokój + garderoba&apos;,V48__c,0),0)
)))))))))))))))))))))))))))))))))))))))))))))))</formula>
        <name>Design: Master Bed-Ward Usable Area - GF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Design__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Master_Bedroom_Usable_Area_1F</fullName>
        <field>Master_Bedroom_Usable_Area_1st_Floor__c</field>
        <formula>MAX(IF(R1__c==&apos;pokój&apos;,V1__c,0), 
MAX(IF(R2__c==&apos;pokój&apos;,V2__c,0), 
MAX(IF(R3__c==&apos;pokój&apos;,V3__c,0), 
MAX(IF(R4__c==&apos;pokój&apos;,V4__c,0), 
MAX(IF(R5__c==&apos;pokój&apos;,V5__c,0), 
MAX(IF(R6__c==&apos;pokój&apos;,V6__c,0), 
MAX(IF(R7__c==&apos;pokój&apos;,V7__c,0), 
MAX(IF(R8__c==&apos;pokój&apos;,V8__c,0), 
MAX(IF(R9__c==&apos;pokój&apos;,V9__c,0), 
MAX(IF(R10__c==&apos;pokój&apos;,V10__c,0), 
MAX(IF(R11__c==&apos;pokój&apos;,V11__c,0), 
MAX(IF(R12__c==&apos;pokój&apos;,V12__c,0), 
MAX(IF(R13__c==&apos;pokój&apos;,V13__c,0), 
MAX(IF(R14__c==&apos;pokój&apos;,V14__c,0), 
MAX(IF(R15__c==&apos;pokój&apos;,V15__c,0), 
MAX(IF(R16__c==&apos;pokój&apos;,V16__c,0), 
MAX(IF(R17__c==&apos;pokój&apos;,V17__c,0), 
MAX(IF(R18__c==&apos;pokój&apos;,V18__c,0), 
MAX(IF(R19__c==&apos;pokój&apos;,V19__c,0), 
MAX(IF(R20__c==&apos;pokój&apos;,V20__c,0), 
MAX(IF(R21__c==&apos;pokój&apos;,V21__c,0), 
MAX(IF(R22__c==&apos;pokój&apos;,V22__c,0), 
MAX(IF(R23__c==&apos;pokój&apos;,V23__c,0), 
MAX(IF(R24__c==&apos;pokój&apos;,V24__c,0), 
MAX(IF(R25__c==&apos;pokój&apos;,V25__c,0), 
MAX(IF(R26__c==&apos;pokój&apos;,V26__c,0), 
MAX(IF(R27__c==&apos;pokój&apos;,V27__c,0), 
MAX(IF(R28__c==&apos;pokój&apos;,V28__c,0), 
MAX(IF(R29__c==&apos;pokój&apos;,V29__c,0), 
MAX(IF(R30__c==&apos;pokój&apos;,V30__c,0), 
MAX(IF(R31__c==&apos;pokój&apos;,V31__c,0), 
MAX(IF(R32__c==&apos;pokój&apos;,V32__c,0), 
MAX(IF(R33__c==&apos;pokój&apos;,V33__c,0), 
MAX(IF(R34__c==&apos;pokój&apos;,V34__c,0), 
MAX(IF(R35__c==&apos;pokój&apos;,V35__c,0), 
MAX(IF(R36__c==&apos;pokój&apos;,V36__c,0), 
MAX(IF(R37__c==&apos;pokój&apos;,V37__c,0), 
MAX(IF(R38__c==&apos;pokój&apos;,V38__c,0), 
MAX(IF(R39__c==&apos;pokój&apos;,V39__c,0), 
MAX(IF(R40__c==&apos;pokój&apos;,V40__c,0), 
MAX(IF(R41__c==&apos;pokój&apos;,V41__c,0), 
MAX(IF(R42__c==&apos;pokój&apos;,V42__c,0), 
MAX(IF(R43__c==&apos;pokój&apos;,V43__c,0), 
MAX(IF(R44__c==&apos;pokój&apos;,V44__c,0), 
MAX(IF(R45__c==&apos;pokój&apos;,V45__c,0), 
MAX(IF(R46__c==&apos;pokój&apos;,V46__c,0), 
MAX(IF(R47__c==&apos;pokój&apos;,V47__c,0), 
MAX(IF(R48__c==&apos;pokój&apos;,V48__c,0), 
MAX(IF(R49__c==&apos;pokój&apos;,V49__c,0), 
MAX(IF(R50__c==&apos;pokój&apos;,V50__c,0),0) 
)))))))))))))))))))))))))))))))))))))))))))))))))</formula>
        <name>Design: Master Bedroom Usable Area - 1F</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Design__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Master_Bedroom_Usable_Area_AT</fullName>
        <field>Master_Bedroom_Usable_Area_Attic__c</field>
        <formula>MAX(IF(R1__c==&apos;pokój&apos;,V1__c,0), 
MAX(IF(R2__c==&apos;pokój&apos;,V2__c,0), 
MAX(IF(R3__c==&apos;pokój&apos;,V3__c,0), 
MAX(IF(R4__c==&apos;pokój&apos;,V4__c,0), 
MAX(IF(R5__c==&apos;pokój&apos;,V5__c,0), 
MAX(IF(R6__c==&apos;pokój&apos;,V6__c,0), 
MAX(IF(R7__c==&apos;pokój&apos;,V7__c,0), 
MAX(IF(R8__c==&apos;pokój&apos;,V8__c,0), 
MAX(IF(R9__c==&apos;pokój&apos;,V9__c,0), 
MAX(IF(R10__c==&apos;pokój&apos;,V10__c,0), 
MAX(IF(R11__c==&apos;pokój&apos;,V11__c,0), 
MAX(IF(R12__c==&apos;pokój&apos;,V12__c,0), 
MAX(IF(R13__c==&apos;pokój&apos;,V13__c,0), 
MAX(IF(R14__c==&apos;pokój&apos;,V14__c,0), 
MAX(IF(R15__c==&apos;pokój&apos;,V15__c,0), 
MAX(IF(R16__c==&apos;pokój&apos;,V16__c,0), 
MAX(IF(R17__c==&apos;pokój&apos;,V17__c,0), 
MAX(IF(R18__c==&apos;pokój&apos;,V18__c,0), 
MAX(IF(R19__c==&apos;pokój&apos;,V19__c,0), 
MAX(IF(R20__c==&apos;pokój&apos;,V20__c,0), 
MAX(IF(R21__c==&apos;pokój&apos;,V21__c,0), 
MAX(IF(R22__c==&apos;pokój&apos;,V22__c,0), 
MAX(IF(R23__c==&apos;pokój&apos;,V23__c,0), 
MAX(IF(R24__c==&apos;pokój&apos;,V24__c,0), 
MAX(IF(R25__c==&apos;pokój&apos;,V25__c,0), 
MAX(IF(R26__c==&apos;pokój&apos;,V26__c,0), 
MAX(IF(R27__c==&apos;pokój&apos;,V27__c,0), 
MAX(IF(R28__c==&apos;pokój&apos;,V28__c,0), 
MAX(IF(R29__c==&apos;pokój&apos;,V29__c,0), 
MAX(IF(R30__c==&apos;pokój&apos;,V30__c,0), 
MAX(IF(R31__c==&apos;pokój&apos;,V31__c,0), 
MAX(IF(R32__c==&apos;pokój&apos;,V32__c,0), 
MAX(IF(R33__c==&apos;pokój&apos;,V33__c,0), 
MAX(IF(R34__c==&apos;pokój&apos;,V34__c,0), 
MAX(IF(R35__c==&apos;pokój&apos;,V35__c,0), 
MAX(IF(R36__c==&apos;pokój&apos;,V36__c,0), 
MAX(IF(R37__c==&apos;pokój&apos;,V37__c,0), 
MAX(IF(R38__c==&apos;pokój&apos;,V38__c,0), 
MAX(IF(R39__c==&apos;pokój&apos;,V39__c,0), 
MAX(IF(R40__c==&apos;pokój&apos;,V40__c,0), 
MAX(IF(R41__c==&apos;pokój&apos;,V41__c,0), 
MAX(IF(R42__c==&apos;pokój&apos;,V42__c,0), 
MAX(IF(R43__c==&apos;pokój&apos;,V43__c,0), 
MAX(IF(R44__c==&apos;pokój&apos;,V44__c,0), 
MAX(IF(R45__c==&apos;pokój&apos;,V45__c,0), 
MAX(IF(R46__c==&apos;pokój&apos;,V46__c,0), 
MAX(IF(R47__c==&apos;pokój&apos;,V47__c,0), 
MAX(IF(R48__c==&apos;pokój&apos;,V48__c,0), 
MAX(IF(R49__c==&apos;pokój&apos;,V49__c,0), 
MAX(IF(R50__c==&apos;pokój&apos;,V50__c,0),0) 
)))))))))))))))))))))))))))))))))))))))))))))))))</formula>
        <name>Design: Master Bedroom Usable Area - AT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Design__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Master_Bedroom_Usable_Area_GF</fullName>
        <field>Master_Bedroom_Usable_Area_Ground_Floor__c</field>
        <formula>MAX(IF(R1__c==&apos;pokój&apos;,V1__c,0), 
MAX(IF(R2__c==&apos;pokój&apos;,V2__c,0), 
MAX(IF(R3__c==&apos;pokój&apos;,V3__c,0), 
MAX(IF(R4__c==&apos;pokój&apos;,V4__c,0), 
MAX(IF(R5__c==&apos;pokój&apos;,V5__c,0), 
MAX(IF(R6__c==&apos;pokój&apos;,V6__c,0), 
MAX(IF(R7__c==&apos;pokój&apos;,V7__c,0), 
MAX(IF(R8__c==&apos;pokój&apos;,V8__c,0), 
MAX(IF(R9__c==&apos;pokój&apos;,V9__c,0), 
MAX(IF(R10__c==&apos;pokój&apos;,V10__c,0), 
MAX(IF(R11__c==&apos;pokój&apos;,V11__c,0), 
MAX(IF(R12__c==&apos;pokój&apos;,V12__c,0), 
MAX(IF(R13__c==&apos;pokój&apos;,V13__c,0), 
MAX(IF(R14__c==&apos;pokój&apos;,V14__c,0), 
MAX(IF(R15__c==&apos;pokój&apos;,V15__c,0), 
MAX(IF(R16__c==&apos;pokój&apos;,V16__c,0), 
MAX(IF(R17__c==&apos;pokój&apos;,V17__c,0), 
MAX(IF(R18__c==&apos;pokój&apos;,V18__c,0), 
MAX(IF(R19__c==&apos;pokój&apos;,V19__c,0), 
MAX(IF(R20__c==&apos;pokój&apos;,V20__c,0), 
MAX(IF(R21__c==&apos;pokój&apos;,V21__c,0), 
MAX(IF(R22__c==&apos;pokój&apos;,V22__c,0), 
MAX(IF(R23__c==&apos;pokój&apos;,V23__c,0), 
MAX(IF(R24__c==&apos;pokój&apos;,V24__c,0), 
MAX(IF(R25__c==&apos;pokój&apos;,V25__c,0), 
MAX(IF(R26__c==&apos;pokój&apos;,V26__c,0), 
MAX(IF(R27__c==&apos;pokój&apos;,V27__c,0), 
MAX(IF(R28__c==&apos;pokój&apos;,V28__c,0), 
MAX(IF(R29__c==&apos;pokój&apos;,V29__c,0), 
MAX(IF(R30__c==&apos;pokój&apos;,V30__c,0), 
MAX(IF(R31__c==&apos;pokój&apos;,V31__c,0), 
MAX(IF(R32__c==&apos;pokój&apos;,V32__c,0), 
MAX(IF(R33__c==&apos;pokój&apos;,V33__c,0), 
MAX(IF(R34__c==&apos;pokój&apos;,V34__c,0), 
MAX(IF(R35__c==&apos;pokój&apos;,V35__c,0), 
MAX(IF(R36__c==&apos;pokój&apos;,V36__c,0), 
MAX(IF(R37__c==&apos;pokój&apos;,V37__c,0), 
MAX(IF(R38__c==&apos;pokój&apos;,V38__c,0), 
MAX(IF(R39__c==&apos;pokój&apos;,V39__c,0), 
MAX(IF(R40__c==&apos;pokój&apos;,V40__c,0), 
MAX(IF(R41__c==&apos;pokój&apos;,V41__c,0), 
MAX(IF(R42__c==&apos;pokój&apos;,V42__c,0), 
MAX(IF(R43__c==&apos;pokój&apos;,V43__c,0), 
MAX(IF(R44__c==&apos;pokój&apos;,V44__c,0), 
MAX(IF(R45__c==&apos;pokój&apos;,V45__c,0), 
MAX(IF(R46__c==&apos;pokój&apos;,V46__c,0), 
MAX(IF(R47__c==&apos;pokój&apos;,V47__c,0), 
MAX(IF(R48__c==&apos;pokój&apos;,V48__c,0), 
MAX(IF(R49__c==&apos;pokój&apos;,V49__c,0), 
MAX(IF(R50__c==&apos;pokój&apos;,V50__c,0),0) 
)))))))))))))))))))))))))))))))))))))))))))))))))</formula>
        <name>Design: Master Bedroom Usable Area - GF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Design__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Master_Utility_Usable_Area_BA</fullName>
        <field>Master_Utility_Usable_Area_Basement__c</field>
        <formula>MAX(IF(R1__c==&apos;pomieszczenie gospodarcze&apos;,V1__c,0), 
MAX(IF(R2__c==&apos;pomieszczenie gospodarcze&apos;,V2__c,0), 
MAX(IF(R3__c==&apos;pomieszczenie gospodarcze&apos;,V3__c,0), 
MAX(IF(R4__c==&apos;pomieszczenie gospodarcze&apos;,V4__c,0), 
MAX(IF(R5__c==&apos;pomieszczenie gospodarcze&apos;,V5__c,0), 
MAX(IF(R6__c==&apos;pomieszczenie gospodarcze&apos;,V6__c,0), 
MAX(IF(R7__c==&apos;pomieszczenie gospodarcze&apos;,V7__c,0), 
MAX(IF(R8__c==&apos;pomieszczenie gospodarcze&apos;,V8__c,0), 
MAX(IF(R9__c==&apos;pomieszczenie gospodarcze&apos;,V9__c,0), 
MAX(IF(R10__c==&apos;pomieszczenie gospodarcze&apos;,V10__c,0), 
MAX(IF(R11__c==&apos;pomieszczenie gospodarcze&apos;,V11__c,0), 
MAX(IF(R12__c==&apos;pomieszczenie gospodarcze&apos;,V12__c,0), 
MAX(IF(R13__c==&apos;pomieszczenie gospodarcze&apos;,V13__c,0), 
MAX(IF(R14__c==&apos;pomieszczenie gospodarcze&apos;,V14__c,0), 
MAX(IF(R15__c==&apos;pomieszczenie gospodarcze&apos;,V15__c,0), 
MAX(IF(R16__c==&apos;pomieszczenie gospodarcze&apos;,V16__c,0), 
MAX(IF(R17__c==&apos;pomieszczenie gospodarcze&apos;,V17__c,0), 
MAX(IF(R18__c==&apos;pomieszczenie gospodarcze&apos;,V18__c,0), 
MAX(IF(R19__c==&apos;pomieszczenie gospodarcze&apos;,V19__c,0), 
MAX(IF(R20__c==&apos;pomieszczenie gospodarcze&apos;,V20__c,0), 
MAX(IF(R21__c==&apos;pomieszczenie gospodarcze&apos;,V21__c,0), 
MAX(IF(R22__c==&apos;pomieszczenie gospodarcze&apos;,V22__c,0), 
MAX(IF(R23__c==&apos;pomieszczenie gospodarcze&apos;,V23__c,0), 
MAX(IF(R24__c==&apos;pomieszczenie gospodarcze&apos;,V24__c,0), 
MAX(IF(R25__c==&apos;pomieszczenie gospodarcze&apos;,V25__c,0), 
MAX(IF(R26__c==&apos;pomieszczenie gospodarcze&apos;,V26__c,0), 
MAX(IF(R27__c==&apos;pomieszczenie gospodarcze&apos;,V27__c,0), 
MAX(IF(R28__c==&apos;pomieszczenie gospodarcze&apos;,V28__c,0), 
MAX(IF(R29__c==&apos;pomieszczenie gospodarcze&apos;,V29__c,0), 
MAX(IF(R30__c==&apos;pomieszczenie gospodarcze&apos;,V30__c,0), 
MAX(IF(R31__c==&apos;pomieszczenie gospodarcze&apos;,V31__c,0), 
MAX(IF(R32__c==&apos;pomieszczenie gospodarcze&apos;,V32__c,0), 
MAX(IF(R33__c==&apos;pomieszczenie gospodarcze&apos;,V33__c,0), 
MAX(IF(R34__c==&apos;pomieszczenie gospodarcze&apos;,V34__c,0), 
MAX(IF(R35__c==&apos;pomieszczenie gospodarcze&apos;,V35__c,0), 
MAX(IF(R36__c==&apos;pomieszczenie gospodarcze&apos;,V36__c,0), 
MAX(IF(R37__c==&apos;pomieszczenie gospodarcze&apos;,V37__c,0), 
MAX(IF(R38__c==&apos;pomieszczenie gospodarcze&apos;,V38__c,0), 
MAX(IF(R39__c==&apos;pomieszczenie gospodarcze&apos;,V39__c,0), 
MAX(IF(R40__c==&apos;pomieszczenie gospodarcze&apos;,V40__c,0),0) 
)))))))))))))))))))))))))))))))))))))))</formula>
        <name>Design: Master Utility Usable Area - BA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Design__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Master_Utility_Usable_Area_GF</fullName>
        <field>Master_Utility_Usable_Area_Ground_Floor__c</field>
        <formula>MAX(IF(R1__c==&apos;pomieszczenie gospodarcze&apos;,V1__c,0), 
MAX(IF(R2__c==&apos;pomieszczenie gospodarcze&apos;,V2__c,0), 
MAX(IF(R3__c==&apos;pomieszczenie gospodarcze&apos;,V3__c,0), 
MAX(IF(R4__c==&apos;pomieszczenie gospodarcze&apos;,V4__c,0), 
MAX(IF(R5__c==&apos;pomieszczenie gospodarcze&apos;,V5__c,0), 
MAX(IF(R6__c==&apos;pomieszczenie gospodarcze&apos;,V6__c,0), 
MAX(IF(R7__c==&apos;pomieszczenie gospodarcze&apos;,V7__c,0), 
MAX(IF(R8__c==&apos;pomieszczenie gospodarcze&apos;,V8__c,0), 
MAX(IF(R9__c==&apos;pomieszczenie gospodarcze&apos;,V9__c,0), 
MAX(IF(R10__c==&apos;pomieszczenie gospodarcze&apos;,V10__c,0), 
MAX(IF(R11__c==&apos;pomieszczenie gospodarcze&apos;,V11__c,0), 
MAX(IF(R12__c==&apos;pomieszczenie gospodarcze&apos;,V12__c,0), 
MAX(IF(R13__c==&apos;pomieszczenie gospodarcze&apos;,V13__c,0), 
MAX(IF(R14__c==&apos;pomieszczenie gospodarcze&apos;,V14__c,0), 
MAX(IF(R15__c==&apos;pomieszczenie gospodarcze&apos;,V15__c,0), 
MAX(IF(R16__c==&apos;pomieszczenie gospodarcze&apos;,V16__c,0), 
MAX(IF(R17__c==&apos;pomieszczenie gospodarcze&apos;,V17__c,0), 
MAX(IF(R18__c==&apos;pomieszczenie gospodarcze&apos;,V18__c,0), 
MAX(IF(R19__c==&apos;pomieszczenie gospodarcze&apos;,V19__c,0), 
MAX(IF(R20__c==&apos;pomieszczenie gospodarcze&apos;,V20__c,0), 
MAX(IF(R21__c==&apos;pomieszczenie gospodarcze&apos;,V21__c,0), 
MAX(IF(R22__c==&apos;pomieszczenie gospodarcze&apos;,V22__c,0), 
MAX(IF(R23__c==&apos;pomieszczenie gospodarcze&apos;,V23__c,0), 
MAX(IF(R24__c==&apos;pomieszczenie gospodarcze&apos;,V24__c,0), 
MAX(IF(R25__c==&apos;pomieszczenie gospodarcze&apos;,V25__c,0), 
MAX(IF(R26__c==&apos;pomieszczenie gospodarcze&apos;,V26__c,0), 
MAX(IF(R27__c==&apos;pomieszczenie gospodarcze&apos;,V27__c,0), 
MAX(IF(R28__c==&apos;pomieszczenie gospodarcze&apos;,V28__c,0), 
MAX(IF(R29__c==&apos;pomieszczenie gospodarcze&apos;,V29__c,0), 
MAX(IF(R30__c==&apos;pomieszczenie gospodarcze&apos;,V30__c,0), 
MAX(IF(R31__c==&apos;pomieszczenie gospodarcze&apos;,V31__c,0), 
MAX(IF(R32__c==&apos;pomieszczenie gospodarcze&apos;,V32__c,0), 
MAX(IF(R33__c==&apos;pomieszczenie gospodarcze&apos;,V33__c,0), 
MAX(IF(R34__c==&apos;pomieszczenie gospodarcze&apos;,V34__c,0), 
MAX(IF(R35__c==&apos;pomieszczenie gospodarcze&apos;,V35__c,0), 
MAX(IF(R36__c==&apos;pomieszczenie gospodarcze&apos;,V36__c,0), 
MAX(IF(R37__c==&apos;pomieszczenie gospodarcze&apos;,V37__c,0), 
MAX(IF(R38__c==&apos;pomieszczenie gospodarcze&apos;,V38__c,0), 
MAX(IF(R39__c==&apos;pomieszczenie gospodarcze&apos;,V39__c,0), 
MAX(IF(R40__c==&apos;pomieszczenie gospodarcze&apos;,V40__c,0),0) 
)))))))))))))))))))))))))))))))))))))))</formula>
        <name>Design: Master Utility Usable Area - GF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Design__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Master_Wardrobe_Usable_Area_1F</fullName>
        <field>Master_Wardrobe_Usable_Area_1st_Floor__c</field>
        <formula>MAX(IF(R1__c==&apos;garderoba&apos;,V1__c,0), 
MAX(IF(R2__c==&apos;garderoba&apos;,V2__c,0), 
MAX(IF(R3__c==&apos;garderoba&apos;,V3__c,0), 
MAX(IF(R4__c==&apos;garderoba&apos;,V4__c,0), 
MAX(IF(R5__c==&apos;garderoba&apos;,V5__c,0), 
MAX(IF(R6__c==&apos;garderoba&apos;,V6__c,0), 
MAX(IF(R7__c==&apos;garderoba&apos;,V7__c,0), 
MAX(IF(R8__c==&apos;garderoba&apos;,V8__c,0), 
MAX(IF(R9__c==&apos;garderoba&apos;,V9__c,0), 
MAX(IF(R10__c==&apos;garderoba&apos;,V10__c,0), 
MAX(IF(R11__c==&apos;garderoba&apos;,V11__c,0), 
MAX(IF(R12__c==&apos;garderoba&apos;,V12__c,0), 
MAX(IF(R13__c==&apos;garderoba&apos;,V13__c,0), 
MAX(IF(R14__c==&apos;garderoba&apos;,V14__c,0), 
MAX(IF(R15__c==&apos;garderoba&apos;,V15__c,0), 
MAX(IF(R16__c==&apos;garderoba&apos;,V16__c,0), 
MAX(IF(R17__c==&apos;garderoba&apos;,V17__c,0), 
MAX(IF(R18__c==&apos;garderoba&apos;,V18__c,0), 
MAX(IF(R19__c==&apos;garderoba&apos;,V19__c,0), 
MAX(IF(R20__c==&apos;garderoba&apos;,V20__c,0), 
MAX(IF(R21__c==&apos;garderoba&apos;,V21__c,0), 
MAX(IF(R22__c==&apos;garderoba&apos;,V22__c,0), 
MAX(IF(R23__c==&apos;garderoba&apos;,V23__c,0), 
MAX(IF(R24__c==&apos;garderoba&apos;,V24__c,0), 
MAX(IF(R25__c==&apos;garderoba&apos;,V25__c,0), 
MAX(IF(R26__c==&apos;garderoba&apos;,V26__c,0), 
MAX(IF(R27__c==&apos;garderoba&apos;,V27__c,0), 
MAX(IF(R28__c==&apos;garderoba&apos;,V28__c,0), 
MAX(IF(R29__c==&apos;garderoba&apos;,V29__c,0), 
MAX(IF(R30__c==&apos;garderoba&apos;,V30__c,0), 
MAX(IF(R31__c==&apos;garderoba&apos;,V31__c,0), 
MAX(IF(R32__c==&apos;garderoba&apos;,V32__c,0), 
MAX(IF(R33__c==&apos;garderoba&apos;,V33__c,0), 
MAX(IF(R34__c==&apos;garderoba&apos;,V34__c,0), 
MAX(IF(R35__c==&apos;garderoba&apos;,V35__c,0), 
MAX(IF(R36__c==&apos;garderoba&apos;,V36__c,0), 
MAX(IF(R37__c==&apos;garderoba&apos;,V37__c,0), 
MAX(IF(R38__c==&apos;garderoba&apos;,V38__c,0), 
MAX(IF(R39__c==&apos;garderoba&apos;,V39__c,0), 
MAX(IF(R40__c==&apos;garderoba&apos;,V40__c,0), 
MAX(IF(R41__c==&apos;garderoba&apos;,V41__c,0), 
MAX(IF(R42__c==&apos;garderoba&apos;,V42__c,0), 
MAX(IF(R43__c==&apos;garderoba&apos;,V43__c,0), 
MAX(IF(R44__c==&apos;garderoba&apos;,V44__c,0), 
MAX(IF(R45__c==&apos;garderoba&apos;,V45__c,0), 
MAX(IF(R46__c==&apos;garderoba&apos;,V46__c,0), 
MAX(IF(R47__c==&apos;garderoba&apos;,V47__c,0), 
MAX(IF(R48__c==&apos;garderoba&apos;,V48__c,0),0) 
)))))))))))))))))))))))))))))))))))))))))))))))</formula>
        <name>Design: Master Wardrobe Usable Area - 1F</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Design__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Master_Wardrobe_Usable_Area_AT</fullName>
        <field>Master_Wardrobe_Usable_Area_Attic__c</field>
        <formula>MAX(IF(R1__c==&apos;garderoba&apos;,V1__c,0), 
MAX(IF(R2__c==&apos;garderoba&apos;,V2__c,0), 
MAX(IF(R3__c==&apos;garderoba&apos;,V3__c,0), 
MAX(IF(R4__c==&apos;garderoba&apos;,V4__c,0), 
MAX(IF(R5__c==&apos;garderoba&apos;,V5__c,0), 
MAX(IF(R6__c==&apos;garderoba&apos;,V6__c,0), 
MAX(IF(R7__c==&apos;garderoba&apos;,V7__c,0), 
MAX(IF(R8__c==&apos;garderoba&apos;,V8__c,0), 
MAX(IF(R9__c==&apos;garderoba&apos;,V9__c,0), 
MAX(IF(R10__c==&apos;garderoba&apos;,V10__c,0), 
MAX(IF(R11__c==&apos;garderoba&apos;,V11__c,0), 
MAX(IF(R12__c==&apos;garderoba&apos;,V12__c,0), 
MAX(IF(R13__c==&apos;garderoba&apos;,V13__c,0), 
MAX(IF(R14__c==&apos;garderoba&apos;,V14__c,0), 
MAX(IF(R15__c==&apos;garderoba&apos;,V15__c,0), 
MAX(IF(R16__c==&apos;garderoba&apos;,V16__c,0), 
MAX(IF(R17__c==&apos;garderoba&apos;,V17__c,0), 
MAX(IF(R18__c==&apos;garderoba&apos;,V18__c,0), 
MAX(IF(R19__c==&apos;garderoba&apos;,V19__c,0), 
MAX(IF(R20__c==&apos;garderoba&apos;,V20__c,0), 
MAX(IF(R21__c==&apos;garderoba&apos;,V21__c,0), 
MAX(IF(R22__c==&apos;garderoba&apos;,V22__c,0), 
MAX(IF(R23__c==&apos;garderoba&apos;,V23__c,0), 
MAX(IF(R24__c==&apos;garderoba&apos;,V24__c,0), 
MAX(IF(R25__c==&apos;garderoba&apos;,V25__c,0), 
MAX(IF(R26__c==&apos;garderoba&apos;,V26__c,0), 
MAX(IF(R27__c==&apos;garderoba&apos;,V27__c,0), 
MAX(IF(R28__c==&apos;garderoba&apos;,V28__c,0), 
MAX(IF(R29__c==&apos;garderoba&apos;,V29__c,0), 
MAX(IF(R30__c==&apos;garderoba&apos;,V30__c,0), 
MAX(IF(R31__c==&apos;garderoba&apos;,V31__c,0), 
MAX(IF(R32__c==&apos;garderoba&apos;,V32__c,0), 
MAX(IF(R33__c==&apos;garderoba&apos;,V33__c,0), 
MAX(IF(R34__c==&apos;garderoba&apos;,V34__c,0), 
MAX(IF(R35__c==&apos;garderoba&apos;,V35__c,0), 
MAX(IF(R36__c==&apos;garderoba&apos;,V36__c,0), 
MAX(IF(R37__c==&apos;garderoba&apos;,V37__c,0), 
MAX(IF(R38__c==&apos;garderoba&apos;,V38__c,0), 
MAX(IF(R39__c==&apos;garderoba&apos;,V39__c,0), 
MAX(IF(R40__c==&apos;garderoba&apos;,V40__c,0), 
MAX(IF(R41__c==&apos;garderoba&apos;,V41__c,0), 
MAX(IF(R42__c==&apos;garderoba&apos;,V42__c,0), 
MAX(IF(R43__c==&apos;garderoba&apos;,V43__c,0), 
MAX(IF(R44__c==&apos;garderoba&apos;,V44__c,0), 
MAX(IF(R45__c==&apos;garderoba&apos;,V45__c,0), 
MAX(IF(R46__c==&apos;garderoba&apos;,V46__c,0), 
MAX(IF(R47__c==&apos;garderoba&apos;,V47__c,0), 
MAX(IF(R48__c==&apos;garderoba&apos;,V48__c,0),0) 
)))))))))))))))))))))))))))))))))))))))))))))))</formula>
        <name>Design: Master Wardrobe Usable Area - AT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Design__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Master_Wardrobe_Usable_Area_GF</fullName>
        <field>Master_Wardrobe_Usable_Area_Ground_Floor__c</field>
        <formula>MAX(IF(R1__c==&apos;garderoba&apos;,V1__c,0), 
MAX(IF(R2__c==&apos;garderoba&apos;,V2__c,0), 
MAX(IF(R3__c==&apos;garderoba&apos;,V3__c,0), 
MAX(IF(R4__c==&apos;garderoba&apos;,V4__c,0), 
MAX(IF(R5__c==&apos;garderoba&apos;,V5__c,0), 
MAX(IF(R6__c==&apos;garderoba&apos;,V6__c,0), 
MAX(IF(R7__c==&apos;garderoba&apos;,V7__c,0), 
MAX(IF(R8__c==&apos;garderoba&apos;,V8__c,0), 
MAX(IF(R9__c==&apos;garderoba&apos;,V9__c,0), 
MAX(IF(R10__c==&apos;garderoba&apos;,V10__c,0), 
MAX(IF(R11__c==&apos;garderoba&apos;,V11__c,0), 
MAX(IF(R12__c==&apos;garderoba&apos;,V12__c,0), 
MAX(IF(R13__c==&apos;garderoba&apos;,V13__c,0), 
MAX(IF(R14__c==&apos;garderoba&apos;,V14__c,0), 
MAX(IF(R15__c==&apos;garderoba&apos;,V15__c,0), 
MAX(IF(R16__c==&apos;garderoba&apos;,V16__c,0), 
MAX(IF(R17__c==&apos;garderoba&apos;,V17__c,0), 
MAX(IF(R18__c==&apos;garderoba&apos;,V18__c,0), 
MAX(IF(R19__c==&apos;garderoba&apos;,V19__c,0), 
MAX(IF(R20__c==&apos;garderoba&apos;,V20__c,0), 
MAX(IF(R21__c==&apos;garderoba&apos;,V21__c,0), 
MAX(IF(R22__c==&apos;garderoba&apos;,V22__c,0), 
MAX(IF(R23__c==&apos;garderoba&apos;,V23__c,0), 
MAX(IF(R24__c==&apos;garderoba&apos;,V24__c,0), 
MAX(IF(R25__c==&apos;garderoba&apos;,V25__c,0), 
MAX(IF(R26__c==&apos;garderoba&apos;,V26__c,0), 
MAX(IF(R27__c==&apos;garderoba&apos;,V27__c,0), 
MAX(IF(R28__c==&apos;garderoba&apos;,V28__c,0), 
MAX(IF(R29__c==&apos;garderoba&apos;,V29__c,0), 
MAX(IF(R30__c==&apos;garderoba&apos;,V30__c,0), 
MAX(IF(R31__c==&apos;garderoba&apos;,V31__c,0), 
MAX(IF(R32__c==&apos;garderoba&apos;,V32__c,0), 
MAX(IF(R33__c==&apos;garderoba&apos;,V33__c,0), 
MAX(IF(R34__c==&apos;garderoba&apos;,V34__c,0), 
MAX(IF(R35__c==&apos;garderoba&apos;,V35__c,0), 
MAX(IF(R36__c==&apos;garderoba&apos;,V36__c,0), 
MAX(IF(R37__c==&apos;garderoba&apos;,V37__c,0), 
MAX(IF(R38__c==&apos;garderoba&apos;,V38__c,0), 
MAX(IF(R39__c==&apos;garderoba&apos;,V39__c,0), 
MAX(IF(R40__c==&apos;garderoba&apos;,V40__c,0), 
MAX(IF(R41__c==&apos;garderoba&apos;,V41__c,0), 
MAX(IF(R42__c==&apos;garderoba&apos;,V42__c,0), 
MAX(IF(R43__c==&apos;garderoba&apos;,V43__c,0), 
MAX(IF(R44__c==&apos;garderoba&apos;,V44__c,0), 
MAX(IF(R45__c==&apos;garderoba&apos;,V45__c,0), 
MAX(IF(R46__c==&apos;garderoba&apos;,V46__c,0), 
MAX(IF(R47__c==&apos;garderoba&apos;,V47__c,0), 
MAX(IF(R48__c==&apos;garderoba&apos;,V48__c,0),0) 
)))))))))))))))))))))))))))))))))))))))))))))))</formula>
        <name>Design: Master Wardrobe Usable Area - GF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Design__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Update_Last_Modified_Media</fullName>
        <field>Last_Modified_Media__c</field>
        <formula>NOW()</formula>
        <name>Design: Update Last Modified Media</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Design__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Media_Generate_Media_Filename</fullName>
        <field>Document_Filename__c</field>
        <formula>CASE(Category__c, &apos;Document PDF&apos;, &apos;extradom-&apos; &amp; Design__r.Slug__c &amp; &apos;-&apos; &amp; Label_Slug__c &amp; &apos;-&apos; &amp; Name, null)</formula>
        <name>Media: Generate Media Filename</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Media_Generate_Valid_Name_ID</fullName>
        <field>Media_Name_ID__c</field>
        <formula>lower(Design__r.Design_Name_ID__c + &apos;: &apos;+ Media_Name_ID_Postfix__c)</formula>
        <name>Media: Generate Valid Name ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Media_Master_Bathroom_Usable_Area</fullName>
        <field>Master_Bathroom_Usable_Area__c</field>
        <formula>MAX(IF(R1__c==&apos;łazienka&apos;,V1__c,0),
MAX(IF(R2__c==&apos;łazienka&apos;,V2__c,0),
MAX(IF(R3__c==&apos;łazienka&apos;,V3__c,0),
MAX(IF(R4__c==&apos;łazienka&apos;,V4__c,0),
MAX(IF(R5__c==&apos;łazienka&apos;,V5__c,0),
MAX(IF(R6__c==&apos;łazienka&apos;,V6__c,0),
MAX(IF(R7__c==&apos;łazienka&apos;,V7__c,0),
MAX(IF(R8__c==&apos;łazienka&apos;,V8__c,0),
MAX(IF(R9__c==&apos;łazienka&apos;,V9__c,0),
MAX(IF(R10__c==&apos;łazienka&apos;,V10__c,0),
MAX(IF(R11__c==&apos;łazienka&apos;,V11__c,0),
MAX(IF(R12__c==&apos;łazienka&apos;,V12__c,0),
MAX(IF(R13__c==&apos;łazienka&apos;,V13__c,0),
MAX(IF(R14__c==&apos;łazienka&apos;,V14__c,0),
MAX(IF(R15__c==&apos;łazienka&apos;,V15__c,0),
MAX(IF(R16__c==&apos;łazienka&apos;,V16__c,0),
MAX(IF(R17__c==&apos;łazienka&apos;,V17__c,0),
MAX(IF(R18__c==&apos;łazienka&apos;,V18__c,0),
MAX(IF(R19__c==&apos;łazienka&apos;,V19__c,0),
MAX(IF(R20__c==&apos;łazienka&apos;,V20__c,0),
MAX(IF(R21__c==&apos;łazienka&apos;,V21__c,0),
MAX(IF(R22__c==&apos;łazienka&apos;,V22__c,0),
MAX(IF(R23__c==&apos;łazienka&apos;,V23__c,0),
MAX(IF(R24__c==&apos;łazienka&apos;,V24__c,0),
MAX(IF(R25__c==&apos;łazienka&apos;,V25__c,0),
MAX(IF(R26__c==&apos;łazienka&apos;,V26__c,0),
MAX(IF(R27__c==&apos;łazienka&apos;,V27__c,0),
MAX(IF(R28__c==&apos;łazienka&apos;,V28__c,0),
MAX(IF(R29__c==&apos;łazienka&apos;,V29__c,0),
MAX(IF(R30__c==&apos;łazienka&apos;,V30__c,0),
MAX(IF(R31__c==&apos;łazienka&apos;,V31__c,0),
MAX(IF(R32__c==&apos;łazienka&apos;,V32__c,0),
MAX(IF(R33__c==&apos;łazienka&apos;,V33__c,0),
MAX(IF(R34__c==&apos;łazienka&apos;,V34__c,0),
MAX(IF(R35__c==&apos;łazienka&apos;,V35__c,0),
MAX(IF(R36__c==&apos;łazienka&apos;,V36__c,0),
MAX(IF(R37__c==&apos;łazienka&apos;,V37__c,0),
MAX(IF(R38__c==&apos;łazienka&apos;,V38__c,0),
MAX(IF(R39__c==&apos;łazienka&apos;,V39__c,0),
MAX(IF(R40__c==&apos;łazienka&apos;,V40__c,0),
MAX(IF(R41__c==&apos;łazienka&apos;,V41__c,0),
MAX(IF(R42__c==&apos;łazienka&apos;,V42__c,0),
MAX(IF(R43__c==&apos;łazienka&apos;,V43__c,0),
MAX(IF(R44__c==&apos;łazienka&apos;,V44__c,0),
MAX(IF(R45__c==&apos;łazienka&apos;,V45__c,0),
MAX(IF(R46__c==&apos;łazienka&apos;,V46__c,0),
MAX(IF(R47__c==&apos;łazienka&apos;,V47__c,0),
MAX(IF(R48__c==&apos;łazienka&apos;,V48__c,0),
MAX(IF(R49__c==&apos;łazienka&apos;,V49__c,0),
MAX(IF(R50__c==&apos;łazienka&apos;,V50__c,0),0)
)))))))))))))))))))))))))))))))))))))))))))))))))</formula>
        <name>Media: Master Bathroom Usable Area</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Media_Master_Bedroom_Usable_Area</fullName>
        <field>Master_Bedroom_Usable_Area__c</field>
        <formula>MAX(IF(R1__c==&apos;pokój&apos;,V1__c,0),
MAX(IF(R2__c==&apos;pokój&apos;,V2__c,0),
MAX(IF(R3__c==&apos;pokój&apos;,V3__c,0),
MAX(IF(R4__c==&apos;pokój&apos;,V4__c,0),
MAX(IF(R5__c==&apos;pokój&apos;,V5__c,0),
MAX(IF(R6__c==&apos;pokój&apos;,V6__c,0),
MAX(IF(R7__c==&apos;pokój&apos;,V7__c,0),
MAX(IF(R8__c==&apos;pokój&apos;,V8__c,0),
MAX(IF(R9__c==&apos;pokój&apos;,V9__c,0),
MAX(IF(R10__c==&apos;pokój&apos;,V10__c,0),
MAX(IF(R11__c==&apos;pokój&apos;,V11__c,0),
MAX(IF(R12__c==&apos;pokój&apos;,V12__c,0),
MAX(IF(R13__c==&apos;pokój&apos;,V13__c,0),
MAX(IF(R14__c==&apos;pokój&apos;,V14__c,0),
MAX(IF(R15__c==&apos;pokój&apos;,V15__c,0),
MAX(IF(R16__c==&apos;pokój&apos;,V16__c,0),
MAX(IF(R17__c==&apos;pokój&apos;,V17__c,0),
MAX(IF(R18__c==&apos;pokój&apos;,V18__c,0),
MAX(IF(R19__c==&apos;pokój&apos;,V19__c,0),
MAX(IF(R20__c==&apos;pokój&apos;,V20__c,0),
MAX(IF(R21__c==&apos;pokój&apos;,V21__c,0),
MAX(IF(R22__c==&apos;pokój&apos;,V22__c,0),
MAX(IF(R23__c==&apos;pokój&apos;,V23__c,0),
MAX(IF(R24__c==&apos;pokój&apos;,V24__c,0),
MAX(IF(R25__c==&apos;pokój&apos;,V25__c,0),
MAX(IF(R26__c==&apos;pokój&apos;,V26__c,0),
MAX(IF(R27__c==&apos;pokój&apos;,V27__c,0),
MAX(IF(R28__c==&apos;pokój&apos;,V28__c,0),
MAX(IF(R29__c==&apos;pokój&apos;,V29__c,0),
MAX(IF(R30__c==&apos;pokój&apos;,V30__c,0),
MAX(IF(R31__c==&apos;pokój&apos;,V31__c,0),
MAX(IF(R32__c==&apos;pokój&apos;,V32__c,0),
MAX(IF(R33__c==&apos;pokój&apos;,V33__c,0),
MAX(IF(R34__c==&apos;pokój&apos;,V34__c,0),
MAX(IF(R35__c==&apos;pokój&apos;,V35__c,0),
MAX(IF(R36__c==&apos;pokój&apos;,V36__c,0),
MAX(IF(R37__c==&apos;pokój&apos;,V37__c,0),
MAX(IF(R38__c==&apos;pokój&apos;,V38__c,0),
MAX(IF(R39__c==&apos;pokój&apos;,V39__c,0),
MAX(IF(R40__c==&apos;pokój&apos;,V40__c,0),
MAX(IF(R41__c==&apos;pokój&apos;,V41__c,0),
MAX(IF(R42__c==&apos;pokój&apos;,V42__c,0),
MAX(IF(R43__c==&apos;pokój&apos;,V43__c,0),
MAX(IF(R44__c==&apos;pokój&apos;,V44__c,0),
MAX(IF(R45__c==&apos;pokój&apos;,V45__c,0),
MAX(IF(R46__c==&apos;pokój&apos;,V46__c,0),
MAX(IF(R47__c==&apos;pokój&apos;,V47__c,0),
MAX(IF(R48__c==&apos;pokój&apos;,V48__c,0),
MAX(IF(R49__c==&apos;pokój&apos;,V49__c,0),
MAX(IF(R50__c==&apos;pokój&apos;,V50__c,0),0)
)))))))))))))))))))))))))))))))))))))))))))))))))</formula>
        <name>Media: Master Bedroom Usable Area</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Media_On_Rooms_changed</fullName>
        <field>Last_Rooms_Changed__c</field>
        <formula>TEXT(Category__c) &amp; &apos;: &apos; &amp; TEXT(today())</formula>
        <name>Media: On Rooms changed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Design__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Media_Recalculate_Last_Modified_Source</fullName>
        <field>Last_Modified_Source__c</field>
        <formula>NOW()</formula>
        <name>Media: Recalculate Last Modified Source</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Media_Set_Free_Plan_ID</fullName>
        <field>Plan_ID__c</field>
        <formula>IF(CONTAINS(TEXT(Category__c),&apos;Plan&apos;),VALUE(Name)+133386,null)</formula>
        <name>Media: Set Free Plan ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Media_Set_default_URL</fullName>
        <field>Onet_URL__c</field>
        <formula>&quot;http://img.extradom.pl/extradom/projekt%20domu/&quot; &amp; Design__r.Onet_ID__c &amp; &quot;/render/&quot; &amp; CASE(Category__c, 
&apos;1st Floor Plan&apos;, &apos;1pietro_big&apos;, 
&apos;2nd Floor Plan&apos;, &apos;2pietro_big&apos;, 
&apos;Basement Plan&apos;, &apos;piwnica_big&apos;, 
&apos;Garage Plan&apos;, &apos;garaz_big&apos;, 
&apos;Ground Floor Plan&apos;, &apos;parter_big&apos;, 
&apos;Attic Plan&apos;, &apos;poddasze_big&apos;, 
&apos;Mezzanine Plan&apos;, &apos;antresola_big&apos;, 
&apos;Outdoor&apos;, IF(Featured__c, &apos;big695&apos;, &apos;zd0_big695&apos;), 
&apos;Front-side elevation&apos;, &apos;elewacja_front&apos;, 
&apos;Back-side elevation&apos;, &apos;elewacja_tyl&apos;, 
&apos;Left-side elevation&apos;, &apos;elewacja_lewa&apos;, 
&apos;Right-side elevation&apos;, &apos;elewacja_prawa&apos;, 
&apos;Building Placement&apos;, &apos;teren_big&apos;, 
&apos;Section AA&apos;, &apos;aa&apos;, 
&apos;&apos;) &amp; &quot;/source&quot;</formula>
        <name>Media: Set default URL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Media_Sum_Total_1_25</fullName>
        <field>Sum_Total_1_25__c</field>
        <formula>IF(V1__c&gt;0,IF(U1__c&gt;0,U1__c,V1__c),0)+
IF(V2__c&gt;0,IF(U2__c&gt;0,U2__c,V2__c),0)+
IF(V3__c&gt;0,IF(U3__c&gt;0,U3__c,V3__c),0)+
IF(V4__c&gt;0,IF(U4__c&gt;0,U4__c,V4__c),0)+
IF(V5__c&gt;0,IF(U5__c&gt;0,U5__c,V5__c),0)+
IF(V6__c&gt;0,IF(U6__c&gt;0,U6__c,V6__c),0)+
IF(V7__c&gt;0,IF(U7__c&gt;0,U7__c,V7__c),0)+
IF(V8__c&gt;0,IF(U8__c&gt;0,U8__c,V8__c),0)+
IF(V9__c&gt;0,IF(U9__c&gt;0,U9__c,V9__c),0)+
IF(V10__c&gt;0,IF(U10__c&gt;0,U10__c,V10__c),0)+
IF(V11__c&gt;0,IF(U11__c&gt;0,U11__c,V11__c),0)+
IF(V12__c&gt;0,IF(U12__c&gt;0,U12__c,V12__c),0)+
IF(V13__c&gt;0,IF(U13__c&gt;0,U13__c,V13__c),0)+
IF(V14__c&gt;0,IF(U14__c&gt;0,U14__c,V14__c),0)+
IF(V15__c&gt;0,IF(U15__c&gt;0,U15__c,V15__c),0)+
IF(V16__c&gt;0,IF(U16__c&gt;0,U16__c,V16__c),0)+
IF(V17__c&gt;0,IF(U17__c&gt;0,U17__c,V17__c),0)+
IF(V18__c&gt;0,IF(U18__c&gt;0,U18__c,V18__c),0)+
IF(V19__c&gt;0,IF(U19__c&gt;0,U19__c,V19__c),0)+
IF(V20__c&gt;0,IF(U20__c&gt;0,U20__c,V20__c),0)+
IF(V21__c&gt;0,IF(U21__c&gt;0,U21__c,V21__c),0)+
IF(V22__c&gt;0,IF(U22__c&gt;0,U22__c,V22__c),0)+
IF(V23__c&gt;0,IF(U23__c&gt;0,U23__c,V23__c),0)+
IF(V24__c&gt;0,IF(U24__c&gt;0,U24__c,V24__c),0)+
IF(V25__c&gt;0,IF(U25__c&gt;0,U25__c,V25__c),0)</formula>
        <name>Media: Sum: Total (1-25)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Media_Sum_Total_26_50</fullName>
        <field>Sum_Total_26_50__c</field>
        <formula>IF(V26__c&gt;0,IF(U26__c&gt;0,U26__c,V26__c),0)+
IF(V27__c&gt;0,IF(U27__c&gt;0,U27__c,V27__c),0)+
IF(V28__c&gt;0,IF(U28__c&gt;0,U28__c,V28__c),0)+
IF(V29__c&gt;0,IF(U29__c&gt;0,U29__c,V29__c),0)+
IF(V30__c&gt;0,IF(U30__c&gt;0,U30__c,V30__c),0)+
IF(V31__c&gt;0,IF(U31__c&gt;0,U31__c,V31__c),0)+
IF(V32__c&gt;0,IF(U32__c&gt;0,U32__c,V32__c),0)+
IF(V33__c&gt;0,IF(U33__c&gt;0,U33__c,V33__c),0)+
IF(V34__c&gt;0,IF(U34__c&gt;0,U34__c,V34__c),0)+
IF(V35__c&gt;0,IF(U35__c&gt;0,U35__c,V35__c),0)+
IF(V36__c&gt;0,IF(U36__c&gt;0,U36__c,V36__c),0)+
IF(V37__c&gt;0,IF(U37__c&gt;0,U37__c,V37__c),0)+
IF(V38__c&gt;0,IF(U38__c&gt;0,U38__c,V38__c),0)+
IF(V39__c&gt;0,IF(U39__c&gt;0,U39__c,V39__c),0)+
IF(V40__c&gt;0,IF(U40__c&gt;0,U40__c,V40__c),0)+
IF(V41__c&gt;0,IF(U41__c&gt;0,U41__c,V41__c),0)+
IF(V42__c&gt;0,IF(U42__c&gt;0,U42__c,V42__c),0)+
IF(V43__c&gt;0,IF(U43__c&gt;0,U43__c,V43__c),0)+
IF(V44__c&gt;0,IF(U44__c&gt;0,U44__c,V44__c),0)+
IF(V45__c&gt;0,IF(U45__c&gt;0,U45__c,V45__c),0)+
IF(V46__c&gt;0,IF(U46__c&gt;0,U46__c,V46__c),0)+
IF(V47__c&gt;0,IF(U47__c&gt;0,U47__c,V47__c),0)+
IF(V48__c&gt;0,IF(U48__c&gt;0,U48__c,V48__c),0)+
IF(V49__c&gt;0,IF(U49__c&gt;0,U49__c,V49__c),0)+
IF(V50__c&gt;0,IF(U50__c&gt;0,U50__c,V50__c),0)</formula>
        <name>Media: Sum: Total (26-50)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Media_Sum_Usable</fullName>
        <field>Sum_Usable__c</field>
        <formula>BLANKVALUE(V1__c,0)+
BLANKVALUE(V2__c,0)+
BLANKVALUE(V3__c,0)+
BLANKVALUE(V4__c,0)+
BLANKVALUE(V5__c,0)+
BLANKVALUE(V6__c,0)+
BLANKVALUE(V7__c,0)+
BLANKVALUE(V8__c,0)+
BLANKVALUE(V9__c,0)+
BLANKVALUE(V10__c,0)+
BLANKVALUE(V11__c,0)+
BLANKVALUE(V12__c,0)+
BLANKVALUE(V13__c,0)+
BLANKVALUE(V14__c,0)+
BLANKVALUE(V15__c,0)+
BLANKVALUE(V16__c,0)+
BLANKVALUE(V17__c,0)+
BLANKVALUE(V18__c,0)+
BLANKVALUE(V19__c,0)+
BLANKVALUE(V20__c,0)+
BLANKVALUE(V21__c,0)+
BLANKVALUE(V22__c,0)+
BLANKVALUE(V23__c,0)+
BLANKVALUE(V24__c,0)+
BLANKVALUE(V25__c,0)+
BLANKVALUE(V26__c,0)+
BLANKVALUE(V27__c,0)+
BLANKVALUE(V28__c,0)+
BLANKVALUE(V29__c,0)+
BLANKVALUE(V30__c,0)+
BLANKVALUE(V31__c,0)+
BLANKVALUE(V32__c,0)+
BLANKVALUE(V33__c,0)+
BLANKVALUE(V34__c,0)+
BLANKVALUE(V35__c,0)+
BLANKVALUE(V36__c,0)+
BLANKVALUE(V37__c,0)+
BLANKVALUE(V38__c,0)+
BLANKVALUE(V39__c,0)+
BLANKVALUE(V40__c,0)+
BLANKVALUE(V41__c,0)+
BLANKVALUE(V42__c,0)+
BLANKVALUE(V43__c,0)+
BLANKVALUE(V44__c,0)+
BLANKVALUE(V45__c,0)+
BLANKVALUE(V46__c,0)+
BLANKVALUE(V47__c,0)+
BLANKVALUE(V48__c,0)+
BLANKVALUE(V49__c,0)+
BLANKVALUE(V50__c,0)</formula>
        <name>Media: Sum: Usable</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rooms_Count_Update</fullName>
        <field>Count_Rooms__c</field>
        <formula>IF(R1__c=&apos;pokój&apos;,1,0)+IF(R2__c=&apos;pokój&apos;,1,0)+IF(R3__c=&apos;pokój&apos;,1,0)+IF(R4__c=&apos;pokój&apos;,1,0)+IF(R5__c=&apos;pokój&apos;,1,0)+IF(R6__c=&apos;pokój&apos;,1,0)+IF(R7__c=&apos;pokój&apos;,1,0)+IF(R8__c=&apos;pokój&apos;,1,0)+IF(R9__c=&apos;pokój&apos;,1,0)+IF(R10__c=&apos;pokój&apos;,1,0)+IF(R11__c=&apos;pokój&apos;,1,0)+IF(R12__c=&apos;pokój&apos;,1,0)+IF(R13__c=&apos;pokój&apos;,1,0)+IF(R14__c=&apos;pokój&apos;,1,0)+IF(R15__c=&apos;pokój&apos;,1,0)+IF(R16__c=&apos;pokój&apos;,1,0)+IF(R17__c=&apos;pokój&apos;,1,0)+IF(R18__c=&apos;pokój&apos;,1,0)+IF(R19__c=&apos;pokój&apos;,1,0)+IF(R20__c=&apos;pokój&apos;,1,0)+IF(R21__c=&apos;pokój&apos;,1,0)+IF(R22__c=&apos;pokój&apos;,1,0)+IF(R23__c=&apos;pokój&apos;,1,0)+IF(R24__c=&apos;pokój&apos;,1,0)+IF(R25__c=&apos;pokój&apos;,1,0)+IF(R26__c=&apos;pokój&apos;,1,0)+IF(R27__c=&apos;pokój&apos;,1,0)+IF(R28__c=&apos;pokój&apos;,1,0)+IF(R29__c=&apos;pokój&apos;,1,0)+IF(R30__c=&apos;pokój&apos;,1,0)+IF(R31__c=&apos;pokój&apos;,1,0)+IF(R32__c=&apos;pokój&apos;,1,0)+IF(R33__c=&apos;pokój&apos;,1,0)+IF(R34__c=&apos;pokój&apos;,1,0)+IF(R35__c=&apos;pokój&apos;,1,0)+IF(R36__c=&apos;pokój&apos;,1,0)+IF(R37__c=&apos;pokój&apos;,1,0)+IF(R38__c=&apos;pokój&apos;,1,0)+IF(R39__c=&apos;pokój&apos;,1,0)+IF(R40__c=&apos;pokój&apos;,1,0)+IF(R41__c=&apos;pokój&apos;,1,0)+IF(R42__c=&apos;pokój&apos;,1,0)+IF(R43__c=&apos;pokój&apos;,1,0)+IF(R44__c=&apos;pokój&apos;,1,0)+IF(R45__c=&apos;pokój&apos;,1,0)+IF(R46__c=&apos;pokój&apos;,1,0)+IF(R47__c=&apos;pokój&apos;,1,0)+IF(R48__c=&apos;pokój&apos;,1,0)+IF(R49__c=&apos;pokój&apos;,1,0)+IF(R50__c=&apos;pokój&apos;,1,0)+
IF(R1__c=&apos;pokój + garderoba&apos;,1,0)+IF(R2__c=&apos;pokój + garderoba&apos;,1,0)+IF(R3__c=&apos;pokój + garderoba&apos;,1,0)+IF(R4__c=&apos;pokój + garderoba&apos;,1,0)+IF(R5__c=&apos;pokój + garderoba&apos;,1,0)+IF(R6__c=&apos;pokój + garderoba&apos;,1,0)+IF(R7__c=&apos;pokój + garderoba&apos;,1,0)+IF(R8__c=&apos;pokój + garderoba&apos;,1,0)+IF(R9__c=&apos;pokój + garderoba&apos;,1,0)+IF(R10__c=&apos;pokój + garderoba&apos;,1,0)+IF(R11__c=&apos;pokój + garderoba&apos;,1,0)+IF(R12__c=&apos;pokój + garderoba&apos;,1,0)+IF(R13__c=&apos;pokój + garderoba&apos;,1,0)+IF(R14__c=&apos;pokój + garderoba&apos;,1,0)+IF(R15__c=&apos;pokój + garderoba&apos;,1,0)+IF(R16__c=&apos;pokój + garderoba&apos;,1,0)+IF(R17__c=&apos;pokój + garderoba&apos;,1,0)+IF(R18__c=&apos;pokój + garderoba&apos;,1,0)+IF(R19__c=&apos;pokój + garderoba&apos;,1,0)+IF(R20__c=&apos;pokój + garderoba&apos;,1,0)+IF(R21__c=&apos;pokój + garderoba&apos;,1,0)+IF(R22__c=&apos;pokój + garderoba&apos;,1,0)+IF(R23__c=&apos;pokój + garderoba&apos;,1,0)+IF(R24__c=&apos;pokój + garderoba&apos;,1,0)+IF(R25__c=&apos;pokój + garderoba&apos;,1,0)+IF(R26__c=&apos;pokój + garderoba&apos;,1,0)+IF(R27__c=&apos;pokój + garderoba&apos;,1,0)+IF(R28__c=&apos;pokój + garderoba&apos;,1,0)+IF(R29__c=&apos;pokój + garderoba&apos;,1,0)+IF(R30__c=&apos;pokój + garderoba&apos;,1,0)+IF(R31__c=&apos;pokój + garderoba&apos;,1,0)+IF(R32__c=&apos;pokój + garderoba&apos;,1,0)+IF(R33__c=&apos;pokój + garderoba&apos;,1,0)+IF(R34__c=&apos;pokój + garderoba&apos;,1,0)+IF(R35__c=&apos;pokój + garderoba&apos;,1,0)+IF(R36__c=&apos;pokój + garderoba&apos;,1,0)+IF(R37__c=&apos;pokój + garderoba&apos;,1,0)+IF(R38__c=&apos;pokój + garderoba&apos;,1,0)+IF(R39__c=&apos;pokój + garderoba&apos;,1,0)</formula>
        <name>Rooms Count Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Ground_Floor_Guest_Rooms</fullName>
        <field>Ground_Floor_Guest_Rooms__c</field>
        <formula>IF(R1__c=&apos;pokój&apos;,1,0)+IF(R2__c=&apos;pokój&apos;,1,0)+IF(R3__c=&apos;pokój&apos;,1,0)+IF(R4__c=&apos;pokój&apos;,1,0)+IF(R5__c=&apos;pokój&apos;,1,0)+IF(R6__c=&apos;pokój&apos;,1,0)+IF(R7__c=&apos;pokój&apos;,1,0)+IF(R8__c=&apos;pokój&apos;,1,0)+IF(R9__c=&apos;pokój&apos;,1,0)+IF(R10__c=&apos;pokój&apos;,1,0)+IF(R11__c=&apos;pokój&apos;,1,0)+IF(R12__c=&apos;pokój&apos;,1,0)+IF(R13__c=&apos;pokój&apos;,1,0)+IF(R14__c=&apos;pokój&apos;,1,0)+IF(R15__c=&apos;pokój&apos;,1,0)+IF(R16__c=&apos;pokój&apos;,1,0)+IF(R17__c=&apos;pokój&apos;,1,0)+IF(R18__c=&apos;pokój&apos;,1,0)+IF(R19__c=&apos;pokój&apos;,1,0)+IF(R20__c=&apos;pokój&apos;,1,0)+IF(R21__c=&apos;pokój&apos;,1,0)+IF(R22__c=&apos;pokój&apos;,1,0)+IF(R23__c=&apos;pokój&apos;,1,0)+IF(R24__c=&apos;pokój&apos;,1,0)+IF(R25__c=&apos;pokój&apos;,1,0)+IF(R26__c=&apos;pokój&apos;,1,0)+IF(R27__c=&apos;pokój&apos;,1,0)+IF(R28__c=&apos;pokój&apos;,1,0)+IF(R29__c=&apos;pokój&apos;,1,0)+IF(R30__c=&apos;pokój&apos;,1,0)+IF(R31__c=&apos;pokój&apos;,1,0)+IF(R32__c=&apos;pokój&apos;,1,0)+IF(R33__c=&apos;pokój&apos;,1,0)+IF(R34__c=&apos;pokój&apos;,1,0)+IF(R35__c=&apos;pokój&apos;,1,0)+IF(R36__c=&apos;pokój&apos;,1,0)+IF(R37__c=&apos;pokój&apos;,1,0)+IF(R38__c=&apos;pokój&apos;,1,0)+IF(R39__c=&apos;pokój&apos;,1,0)+IF(R40__c=&apos;pokój&apos;,1,0)+IF(R41__c=&apos;pokój&apos;,1,0)+IF(R42__c=&apos;pokój&apos;,1,0)+IF(R43__c=&apos;pokój&apos;,1,0)+IF(R44__c=&apos;pokój&apos;,1,0)+IF(R45__c=&apos;pokój&apos;,1,0)+IF(R46__c=&apos;pokój&apos;,1,0)+IF(R47__c=&apos;pokój&apos;,1,0)+IF(R48__c=&apos;pokój&apos;,1,0)+IF(R49__c=&apos;pokój&apos;,1,0)+IF(R50__c=&apos;pokój&apos;,1,0)</formula>
        <name>Update Ground Floor Guest Rooms</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Design__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Media%3A On 1st Floor Changed</fullName>
        <actions>
            <name>Design_Count_Bathrooms_1F</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Count_Bedroom_Wardrobes_1F</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Count_Bedrooms_1F</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Count_Toilets_1F</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Count_Wardrobes_1F</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Master_Bathroom_Usable_Area_1F</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Master_Bed_Ward_Usable_Area_1F</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Master_Bedroom_Usable_Area_1F</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Master_Wardrobe_Usable_Area_1F</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Media__c.Category__c</field>
            <operation>equals</operation>
            <value>1st Floor Plan</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Media%3A On Attic Changed</fullName>
        <actions>
            <name>Design_Count_Bathrooms_AT</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Count_Bedroom_Wardrobes_AT</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Count_Bedrooms_AT</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Count_Toilets_AT</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Count_Wardrobes_AT</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Master_Bathroom_Usable_Area_AT</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Master_Bed_Ward_Usable_Area_AT</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Master_Bedroom_Usable_Area_AT</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Master_Wardrobe_Usable_Area_AT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Media__c.Category__c</field>
            <operation>equals</operation>
            <value>Attic Plan</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Media%3A On Basement Changed</fullName>
        <actions>
            <name>Design_Count_Utility_Rooms_BA</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Heating_Room_Usable_Area_BA</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Master_Utility_Usable_Area_BA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Media__c.Category__c</field>
            <operation>equals</operation>
            <value>Basement Plan</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Media%3A On Create</fullName>
        <actions>
            <name>Media_Generate_Valid_Name_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Media_Set_Free_Plan_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Media_Set_default_URL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Media%3A On Ground Floor Changed</fullName>
        <actions>
            <name>Design_Count_Bathrooms_GF</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Count_Bedroom_Wardrobes_GF</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Count_Bedrooms_GF</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Count_Toilets_GF</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Count_Wardrobes_GF</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Master_Bathroom_Usable_Area_GF</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Master_Bed_Ward_Usable_Area_GF</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Master_Bedroom_Usable_Area_GF</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Master_Wardrobe_Usable_Area_GF</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Ground_Floor_Guest_Rooms</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Media__c.Category__c</field>
            <operation>equals</operation>
            <value>Ground Floor Plan</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Media%3A On Ground Floor Changed 2</fullName>
        <actions>
            <name>Design_Dining_Living_Usable_Area_GF</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Dining_Usable_Area_GF</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Kitch_Din_Livin_Usable_Area_GF</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Kitchen_Dining_Usable_Area_GF</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Kitchen_Living_Usable_Area_GF</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Kitchen_Pantry_Usable_Area_GF</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Kitchen_Usable_Area_GF</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Living_Usable_Area_GF</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Media__c.Category__c</field>
            <operation>equals</operation>
            <value>Ground Floor Plan</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Media%3A On Ground Floor Changed 3</fullName>
        <actions>
            <name>Design_Count_Pantries_GF</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Count_Utility_Rooms_GF</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Heating_Room_Usable_Area_GF</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Master_Utility_Usable_Area_GF</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Media__c.Category__c</field>
            <operation>equals</operation>
            <value>Ground Floor Plan</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Media%3A On Label changed</fullName>
        <actions>
            <name>Media_Generate_Media_Filename</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>(ISNEW() &amp;&amp; NOT(ISBLANK(Label__c))) || ISCHANGED(Label__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Media%3A On Rooms changed</fullName>
        <actions>
            <name>Media_On_Rooms_changed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Rooms__c) || ISCHANGED(Sum_Usable__c) || ISCHANGED(Sum_Total_1_25__c) || ISCHANGED(Sum_Total_26_50__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Media%3A On Update</fullName>
        <actions>
            <name>Design_Update_Last_Modified_Media</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Media_Generate_Valid_Name_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Media_Master_Bathroom_Usable_Area</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Media_Master_Bedroom_Usable_Area</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Media_Sum_Total_1_25</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Media_Sum_Total_26_50</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Media_Sum_Usable</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Rooms_Count_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Media%3A On Update %28Source changed%29</fullName>
        <actions>
            <name>Media_Recalculate_Last_Modified_Source</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Source_URL__c) || ISCHANGED(Bot__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
