<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Community_Member_Org_Member_Update</fullName>
        <field>Org_Member_Update__c</field>
        <formula>NOW()</formula>
        <name>Community Member: Org Member Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Community_Member_Remove_Premium_Member</fullName>
        <apiVersion>32.0</apiVersion>
        <endpointUrl>http://extradom.pl/Api/SFSOAP/Member/RemoveRole?roleName=PremiumMember</endpointUrl>
        <fields>Id</fields>
        <fields>Membercode__c</fields>
        <fields>login__c</fields>
        <fields>password__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>mariusz.juszczyk@extradom.pl</integrationUser>
        <name>Community Member: Remove Premium Member</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Community_Member_Set_Premium_Member</fullName>
        <apiVersion>32.0</apiVersion>
        <endpointUrl>http://extradom.pl/Api/SFSOAP/Member/SetRole?roleName=PremiumMember</endpointUrl>
        <fields>Id</fields>
        <fields>Membercode__c</fields>
        <fields>login__c</fields>
        <fields>password__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>mariusz.juszczyk@extradom.pl</integrationUser>
        <name>Community Member: Set Premium Member</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Send_Update_Org_XML</fullName>
        <apiVersion>32.0</apiVersion>
        <endpointUrl>http://extradom.pl/Api/SFSOAP/Member/SetOrgs</endpointUrl>
        <fields>Id</fields>
        <fields>Membercode__c</fields>
        <fields>login__c</fields>
        <fields>orgXml__c</fields>
        <fields>password__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>mariusz.juszczyk@extradom.pl</integrationUser>
        <name>Send / Update Org XML</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Community Member%3A On Org Changed</fullName>
        <actions>
            <name>Community_Member_Org_Member_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Send_Update_Org_XML</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(orgXml__c) &amp;&amp; Is_Org_Member__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Community Member%3A On Premium Member Remove</fullName>
        <actions>
            <name>Community_Member_Remove_Premium_Member</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <formula>Membercode__c &lt;&gt; null &amp;&amp; Premium_Member__c = null &amp;&amp; ISCHANGED(Premium_Member__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Community Member%3A On Premium Member Set</fullName>
        <actions>
            <name>Community_Member_Set_Premium_Member</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Community_Member__c.Membercode__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Community_Member__c.Premium_Member__c</field>
            <operation>greaterThan</operation>
            <value>TODAY</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Community_Member_Remove_Premium_Member</name>
                <type>OutboundMessage</type>
            </actions>
            <offsetFromField>Community_Member__c.Premium_Member__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
