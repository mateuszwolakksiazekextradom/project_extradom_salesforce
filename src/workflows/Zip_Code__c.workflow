<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Zip_Code_Adjust_ID_to_Name</fullName>
        <field>Unique_Name_ID__c</field>
        <formula>Name</formula>
        <name>Zip Code: Adjust Unique Name ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Zip Code%3A On every change</fullName>
        <actions>
            <name>Zip_Code_Adjust_ID_to_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>True</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
