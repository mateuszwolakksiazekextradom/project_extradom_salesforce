<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>SMS_Community_Verification_Secret_Code</fullName>
        <description>SMS: Community Verification Secret Code</description>
        <protected>false</protected>
        <recipients>
            <recipient>mariusz.juszczyk+smsapi@extradom.pl</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SMS_extradomCommunityPhoneVerification</template>
    </alerts>
    <fieldUpdates>
        <fullName>User_Phone_Verification_Code</fullName>
        <field>Phone_Verification_Verification_Code__c</field>
        <formula>RIGHT(
TEXT(
sqrt(
(value(
(left(right(text(now()),6),2))&amp;
text(DAY(DATEVALUE(now())))&amp;
text(Month(DATEVALUE(now())))&amp;
text(Year(DATEVALUE(now())))&amp;
(left(right(text(now()),9),2)))
)/VALUE(Phone)
)
),6)</formula>
        <name>User: Phone Verification Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>User_Phone_Verification_Matched</fullName>
        <field>Phone_Verification__c</field>
        <literalValue>1</literalValue>
        <name>User: Phone Verification Matched</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>User_Set_FirstName_to_Blank</fullName>
        <field>FirstName</field>
        <name>User: Set FirstName to Blank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>User_Set_LastName_to_CommunityNickname</fullName>
        <field>LastName</field>
        <formula>CommunityNickname</formula>
        <name>User: Set LastName to CommunityNickname</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>User_Username_equals_Email</fullName>
        <field>Username</field>
        <formula>Email</formula>
        <name>User: Username equals Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>User%3A Email equals Username for External Users</fullName>
        <actions>
            <name>User_Username_equals_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Email) &amp;&amp; ISPICKVAL(UserType,&apos;CspLitePortal&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>User%3A External Users</fullName>
        <actions>
            <name>User_Set_FirstName_to_Blank</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>User_Set_LastName_to_CommunityNickname</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.UserType</field>
            <operation>equals</operation>
            <value>High Volume Portal</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>User%3A Phone Verification Check</fullName>
        <actions>
            <name>User_Phone_Verification_Matched</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT(Phone_Verification__c) &amp;&amp; ISCHANGED(Phone_Verification_Response_Code__c) &amp;&amp; Phone_Verification_Response_Code__c ==  Phone_Verification_Verification_Code__c &amp;&amp; NOT(ISBLANK(Phone_Verification_Verification_Code__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>User%3A Phone Verification Request</fullName>
        <actions>
            <name>SMS_Community_Verification_Secret_Code</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>User_Phone_Verification_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT(Phone_Verification__c) &amp;&amp; ISCHANGED(Phone_Verification_Request_Date__c) &amp;&amp; NOT(ISBLANK(Phone))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
