<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>OrderProduct_SetUnitPrice</fullName>
        <field>UnitPrice</field>
        <formula>List_Price__c -  Discount_Internal__c -  Discount_Sales__c -  Discount_Supplier__c</formula>
        <name>Order Product - Set Unit Price</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>OrderProduct_SetUnitPrice</fullName>
        <actions>
            <name>OrderProduct_SetUnitPrice</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>testtest</fullName>
        <active>false</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
