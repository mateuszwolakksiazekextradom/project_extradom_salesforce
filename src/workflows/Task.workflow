<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Analiza_dzia_ki_potw_zam_wienia</fullName>
        <description>Analiza działki - potw. zamówienia</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>kontakt@extradom.pl</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>COK/Analiza_dzia_ki_potw_zam_wienia</template>
    </alerts>
    <alerts>
        <fullName>Email_Potwierdzenie_zam_wienia_kosztorysu</fullName>
        <description>Email: Potwierdzenie zamówienia kosztorysu</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>kontakt@extradom.pl</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>COK/Potwierdzenie_zam_wienia_kosztorysu</template>
    </alerts>
    <alerts>
        <fullName>Email_Potwierdzenie_zam_wienia_rysunk_w_szczeg_owych</fullName>
        <description>Email: Potwierdzenie zamówienia rysunków szczegółowych</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>kontakt@extradom.pl</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Potwierdzenia_automatyczne/Potwierdzenie_rysunk_w</template>
    </alerts>
    <alerts>
        <fullName>Email_Potwierdzenie_zg_oszenia_konkursowego</fullName>
        <description>Email: Potwierdzenie zgłoszenia konkursowego</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>kontakt@extradom.pl</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>COK/Zg_oszenie_konkursowe</template>
    </alerts>
    <alerts>
        <fullName>Task_W_sprawie_materia_w_od_o_one</fullName>
        <description>Task: W sprawie materiałów (odłożone)</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>kontakt@extradom.pl</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>COK/Extradom_W_sprawie_materia_w_od_o_one</template>
    </alerts>
    <alerts>
        <fullName>Task_W_sprawie_materia_w_od_o_one_po_1_mies</fullName>
        <description>Task: W sprawie materiałów (odłożone) - po 1 mies.</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>kontakt@extradom.pl</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>COK/Extradom_W_sprawie_materia_w_od_o_one_po_1_mies</template>
    </alerts>
    <fieldUpdates>
        <fullName>Task_Calculate_Net_Value_From_Gross</fullName>
        <field>Value__c</field>
        <formula>Item_Gross_Value__c / (1+$User.Base_VAT__c)</formula>
        <name>Task: Calculate Net Value From Gross</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Task_Change_Owner_of_Plot_Analysis_Task</fullName>
        <field>OwnerId</field>
        <lookupValue>marcin.wiater@extradom.pl</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Task: Change Owner of Plot Analysis Task</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Task_Change_Owner_of_Question_Task</fullName>
        <field>OwnerId</field>
        <lookupValue>karolina.rybczynska@extradom.pl</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Task: Change Owner of Question Task</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Task_Change_Subject_of_Question_Task</fullName>
        <field>Subject</field>
        <formula>&apos;rysunki szczegółowe&apos;</formula>
        <name>Task: Change Subject of Question Task</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Task_Owner_Type_Estimates_Request</fullName>
        <field>OwnerId</field>
        <lookupValue>karolina.rybczynska@extradom.pl</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Task: Owner - Type: Estimates Request</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Task_Quantity_1</fullName>
        <field>Quantity__c</field>
        <formula>1</formula>
        <name>Task: Quantity: 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Task_Record_Type_Telemarketing_Task</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Telemarketing_Task</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Task: Record Type - Telemarketing Task</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Task_Source_Campaign_Extradom_pl</fullName>
        <field>Source_Campaign__c</field>
        <formula>&apos;Extradom.pl&apos;</formula>
        <name>Task: Source Campaign - Extradom.pl</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Task_Status_Completed</fullName>
        <field>Status</field>
        <literalValue>Completed</literalValue>
        <name>Task: Status - Completed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Task_Teaser_Update</fullName>
        <field>Teaser__c</field>
        <formula>LEFT(Description, 80)</formula>
        <name>Task: Teaser Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Task_Type_Follow_Up_Call</fullName>
        <field>Type</field>
        <literalValue>Follow Up Call</literalValue>
        <name>Task: Type: Follow Up Call</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Task_Type_Telemarketing_Task</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Telemarketing_Task</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Task: Type: Telemarketing Task</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Task_Value_of_Plot_Analysis</fullName>
        <field>Value__c</field>
        <formula>IF(ISPICKVAL(Status,&apos;Ordered - Waiting for Payment&apos;),9.99,0.00)</formula>
        <name>Task: Value of Plot Analysis</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Task%3A Autoclose Sketches on create if comments blank</fullName>
        <actions>
            <name>Task_Status_Completed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Task.Type</field>
            <operation>equals</operation>
            <value>Question</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserType</field>
            <operation>equals</operation>
            <value>Guest,High Volume Portal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Description</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Task%3A Calculate Net Value From Gross %28on create%2C edit%29</fullName>
        <actions>
            <name>Task_Calculate_Net_Value_From_Gross</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Item_Gross_Value__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Task%3A Change Owner Dominika</fullName>
        <actions>
            <name>Task_Change_Owner_of_Question_Task</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.OwnerId</field>
            <operation>equals</operation>
            <value>Dominika Rabczyńska</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Task%3A Change Owner of Estimates Request Task</fullName>
        <actions>
            <name>Email_Potwierdzenie_zam_wienia_kosztorysu</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Task_Owner_Type_Estimates_Request</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Type</field>
            <operation>equals</operation>
            <value>Estimates Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserType</field>
            <operation>equals</operation>
            <value>Guest,High Volume Portal</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Task%3A Change Owner of Question Task</fullName>
        <actions>
            <name>Task_Change_Owner_of_Question_Task</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Task_Change_Subject_of_Question_Task</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Type</field>
            <operation>equals</operation>
            <value>Question</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserType</field>
            <operation>equals</operation>
            <value>Guest,High Volume Portal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Not Started</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Task%3A Competition task - rules</fullName>
        <actions>
            <name>Email_Potwierdzenie_zg_oszenia_konkursowego</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Type</field>
            <operation>equals</operation>
            <value>Competition</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Task%3A DK NOTUS</fullName>
        <actions>
            <name>Task_Record_Type_Telemarketing_Task</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Task_Source_Campaign_Extradom_pl</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Task_Status_Completed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Partner_ID__c</field>
            <operation>equals</operation>
            <value>001b000000WlS1P</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Task%3A DK Notus Record Type</fullName>
        <actions>
            <name>Task_Type_Telemarketing_Task</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>equals</operation>
            <value>DK Notus</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Type</field>
            <operation>equals</operation>
            <value>Telemarketing: Qualification</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Task%3A On Pardot Email to Send SMS</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>equals</operation>
            <value>Pardot Web Email: Prezentacje projektów</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Task%3A Plot Analysis - rules</fullName>
        <actions>
            <name>Analiza_dzia_ki_potw_zam_wienia</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Task_Change_Owner_of_Plot_Analysis_Task</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Task_Quantity_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Task_Value_of_Plot_Analysis</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Type</field>
            <operation>equals</operation>
            <value>Plot Analysis</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Task%3A Prezentacje projektów - Next Task</fullName>
        <actions>
            <name>Oddzwo_ws_prezentacji</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Type</field>
            <operation>equals</operation>
            <value>E-mail</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Phone</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Task%3A Question task - rules</fullName>
        <actions>
            <name>Email_Potwierdzenie_zam_wienia_rysunk_w_szczeg_owych</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Type</field>
            <operation>equals</operation>
            <value>Question</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Task%3A Question task - rules %28for testing%29</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Task.Type</field>
            <operation>equals</operation>
            <value>Question</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Email</field>
            <operation>equals</operation>
            <value>bartosz.lukjanski@extradom.pl</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Task%3A Rysunki oczekujące</fullName>
        <actions>
            <name>Task_W_sprawie_materia_w_od_o_one</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Waiting on someone else</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Case_Safe_What_Id__c</field>
            <operation>startsWith</operation>
            <value>a06</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Task_W_sprawie_materia_w_od_o_one_po_1_mies</name>
                <type>Alert</type>
            </actions>
            <timeLength>30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <tasks>
        <fullName>Oddzwo_ws_prezentacji</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>3</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Oddzwoń ws. prezentacji</subject>
    </tasks>
</Workflow>
