<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Plot_Request_Building_Conditions_A_Dat</fullName>
        <field>Request_Building_Conditions_Analys_Date__c</field>
        <formula>NOW()</formula>
        <name>Plot: Request Building Conditions A. Dat</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Plot%3A On Request Building Conditions Analysis set</fullName>
        <actions>
            <name>Plot_Request_Building_Conditions_A_Dat</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Plot__c.Request_Building_Conditions_Analysis__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
