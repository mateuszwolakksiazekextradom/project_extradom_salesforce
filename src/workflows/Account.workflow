<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Account_extraNagroda_saldo</fullName>
        <description>Account: extraNagroda - saldo</description>
        <protected>false</protected>
        <recipients>
            <field>Transactional_Email_Secondary__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Transactional_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>b2b@extradom.pl</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Dealerzy/Account_extraNagroda_saldo</template>
    </alerts>
    <alerts>
        <fullName>Email</fullName>
        <ccEmails>bartosz.lukjanski@extradom.pl</ccEmails>
        <description>Email</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Przekazanie_danych/Telemarketing_Arkada_Wygrany_Klient</template>
    </alerts>
    <alerts>
        <fullName>SMS_Lista_partner_w_do_adaptacji_projektu_dla_Klienta</fullName>
        <description>SMS: Lista partnerów do adaptacji projektu (dla Klienta)</description>
        <protected>false</protected>
        <recipients>
            <recipient>mariusz.juszczyk+smsapi@extradom.pl</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Architekci/SMS_Lista_partner_w_do_adaptacji_projek</template>
    </alerts>
    <alerts>
        <fullName>asd</fullName>
        <ccEmails>bartosz.lukjanski@extradom.pl</ccEmails>
        <description>asd</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Przekazanie_danych/Telemarketing_Gaspol_Wygrany_Klient</template>
    </alerts>
    <alerts>
        <fullName>extraNagroda_przypomnienie</fullName>
        <description>extraNagroda - przypomnienie</description>
        <protected>false</protected>
        <recipients>
            <field>Transactional_Email_Secondary__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Transactional_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>b2b@extradom.pl</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Dealerzy/Account_extraNagroda_przypomnienie</template>
    </alerts>
    <alerts>
        <fullName>extraNagroda_przypomnienie2</fullName>
        <description>extraNagroda - przypomnienie</description>
        <protected>false</protected>
        <recipients>
            <field>Transactional_Email_Secondary__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Transactional_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>b2b@extradom.pl</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Dealerzy/Account_extraNagroda_jeszcze_tylko_miesiac</template>
    </alerts>
    <fieldUpdates>
        <fullName>Account_Investment_City</fullName>
        <field>Investment_City__c</field>
        <formula>CASE(Investment_Location__r.RecordType.Name, 
&apos;City&apos;, Investment_Location__r.Name,
Investment_City__c)</formula>
        <name>Account: Investment City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Investment_Region</fullName>
        <field>Investment_Region__c</field>
        <formula>CASE(Investment_Location__r.RecordType.Name,
&apos;City&apos;, Investment_Location__r.Parent_Location__r.Parent_Location__r.Name,
&apos;Region&apos;, Investment_Location__r.Name,
Investment_Region__c)</formula>
        <name>Account: Investment Region</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Investment_State</fullName>
        <field>Investment_State__c</field>
        <formula>CASE(Investment_Location__r.RecordType.Name, 
&apos;City&apos;, Investment_Location__r.Parent_Location__r.Parent_Location__r.Parent_Location__r.Name, 
&apos;Region&apos;, Investment_Location__r.Parent_Location__r.Name, 
Investment_State__c)</formula>
        <name>Account: Investment State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Lead_Status_Enter_Converted</fullName>
        <field>Enter_Date_Converted__c</field>
        <formula>IF(
  ISPICKVAL(Lead_Status__c,&apos;Converted&apos;),
  BLANKVALUE(Enter_Date_Converted__c,NOW()),
  IF(
    ISPICKVAL(Lead_Status__c,&apos;Unqualified&apos;),
    Enter_Date_Converted__c,
    null
  )
)</formula>
        <name>Account: Lead Status: Enter Converted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Lead_Status_Enter_MQL</fullName>
        <field>Enter_Date_Marketing_Qualified__c</field>
        <formula>IF(
  OR(
    ISPICKVAL(Lead_Status__c,&apos;Marketing Qualified&apos;),
    ISPICKVAL(Lead_Status__c,&apos;Sales Ready&apos;),
    ISPICKVAL(Lead_Status__c,&apos;Sales Qualified&apos;),
    ISPICKVAL(Lead_Status__c,&apos;Converted&apos;)
  ),
  BLANKVALUE(Enter_Date_Marketing_Qualified__c,NOW()),
  IF(
    ISPICKVAL(Lead_Status__c,&apos;Unqualified&apos;),
    Enter_Date_Marketing_Qualified__c,
    null
  )
)</formula>
        <name>Account: Lead Status: Enter MQL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Lead_Status_Enter_New</fullName>
        <field>Enter_Date_New__c</field>
        <formula>IF(
  OR(
    ISPICKVAL(Lead_Status__c,&apos;New&apos;),
    ISPICKVAL(Lead_Status__c,&apos;Marketing Qualified&apos;),
    ISPICKVAL(Lead_Status__c,&apos;Sales Ready&apos;),
    ISPICKVAL(Lead_Status__c,&apos;Sales Qualified&apos;),
    ISPICKVAL(Lead_Status__c,&apos;Converted&apos;)
  ),
  BLANKVALUE(Enter_Date_New__c,NOW()),
  IF(
    ISPICKVAL(Lead_Status__c,&apos;Unqualified&apos;),
    Enter_Date_New__c,
    null
  )
)</formula>
        <name>Account: Lead Status: Enter New</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Lead_Status_Enter_SQL</fullName>
        <field>Enter_Date_Sales_Qualified__c</field>
        <formula>IF(
  OR(
    ISPICKVAL(Lead_Status__c,&apos;Sales Qualified&apos;),
    ISPICKVAL(Lead_Status__c,&apos;Converted&apos;)
  ),
  BLANKVALUE(Enter_Date_Sales_Qualified__c,NOW()),
  IF(
    ISPICKVAL(Lead_Status__c,&apos;Unqualified&apos;),
    Enter_Date_Sales_Qualified__c,
    null
  )
)</formula>
        <name>Account: Lead Status: Enter SQL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Lead_Status_Enter_Sales_Ready</fullName>
        <field>Enter_Date_Sales_Ready__c</field>
        <formula>IF(
  OR(
    ISPICKVAL(Lead_Status__c,&apos;Sales Ready&apos;),
    ISPICKVAL(Lead_Status__c,&apos;Sales Qualified&apos;),
    ISPICKVAL(Lead_Status__c,&apos;Converted&apos;)
  ),
  BLANKVALUE(Enter_Date_Sales_Ready__c,NOW()),
  IF(
    ISPICKVAL(Lead_Status__c,&apos;Unqualified&apos;),
    Enter_Date_Sales_Ready__c,
    null
  )
)</formula>
        <name>Account: Lead Status: Enter Sales Ready</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Lead_Status_Enter_Unqualified</fullName>
        <field>Enter_Date_Unqualified__c</field>
        <formula>IF(
  ISPICKVAL(Lead_Status__c,&apos;Unqualified&apos;),
  BLANKVALUE(Enter_Date_Unqualified__c,NOW()),
  null
)</formula>
        <name>Account: Lead Status: Enter Unqualified</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Lead_Status_Unqualified</fullName>
        <field>Lead_Status__c</field>
        <literalValue>Unqualified</literalValue>
        <name>Account: Lead Status: Unqualified</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Source_Question_next_call_7d</fullName>
        <field>Next_Call__c</field>
        <formula>TODAY()+7+CASE(MOD(TODAY()+7-DATE(1900,1,7),7),0,1,6,2,0)</formula>
        <name>Account: Source - Question: next call 7d</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>loop_2</fullName>
        <field>Loop__c</field>
        <formula>1</formula>
        <name>loop 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update</fullName>
        <field>Loop__c</field>
        <formula>2</formula>
        <name>update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Account%3A Dealer called by Sales Agent now</fullName>
        <actions>
            <name>Diler_kontaktowa_si_z_COK</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <formula>ISPICKVAL(Type,&apos;Dealer&apos;) &amp;&amp; ISCHANGED(Count_Calls__c) &amp;&amp;  $UserRole.Name == &apos;Design Sales Agent&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Dealer with %3C 12 Design Orders CY and not purchased from 90 days</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>equals</operation>
            <value>Dealer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Count_Designs_This_Year_Estimate__c</field>
            <operation>lessThan</operation>
            <value>12</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Diler_nie_zam_wi_nic_od_90_dni_zadzwo</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Account.Last_Ordered_Design__c</offsetFromField>
            <timeLength>90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Account%3A Dealer with %3E%3D 12 Design Orders CY and not purchased from 30 days</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>equals</operation>
            <value>Dealer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Count_Designs_This_Year_Estimate__c</field>
            <operation>greaterOrEqual</operation>
            <value>12</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Diler_nie_zam_wi_nic_od_30_dni_zadzwo</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Account.Last_Ordered_Design__c</offsetFromField>
            <timeLength>30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Account%3A Dealer with no Design Orders CY and created last 12 months</fullName>
        <actions>
            <name>Diler_nie_zam_wi_nic_zadzwo</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL(Type,&apos;Dealer&apos;) &amp;&amp; TODAY()-DATEVALUE(CreatedDate) &lt;= 365 &amp;&amp; ISBLANK(Last_Ordered_Design__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Investment Location change</fullName>
        <actions>
            <name>Account_Investment_City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Account_Investment_Region</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Account_Investment_State</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Investment_Location__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Loop</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Account.Loop__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Email</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>update</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Account%3A Loop 2</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Account.Loop__c</field>
            <operation>equals</operation>
            <value>2</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>asd</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>loop_2</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Account%3A On Follow Up Data Changed</fullName>
        <actions>
            <name>Nast_pny_kontakt_telefoniczny</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <formula>ISCHANGED(Next_Call__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A On Lead Status Changed</fullName>
        <actions>
            <name>Account_Lead_Status_Enter_Converted</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Account_Lead_Status_Enter_MQL</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Account_Lead_Status_Enter_New</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Account_Lead_Status_Enter_SQL</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Account_Lead_Status_Enter_Sales_Ready</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Account_Lead_Status_Enter_Unqualified</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(   ISCHANGED(Lead_Status__c),   ISNEW() )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Source - Question%3A next call</fullName>
        <actions>
            <name>Account_Source_Question_next_call_7d</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.AccountSource</field>
            <operation>equals</operation>
            <value>Extradom.pl (Question)</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A Unqualified Reason set</fullName>
        <actions>
            <name>Account_Lead_Status_Unqualified</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Unqualified_Reason__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account%3A extraNagroda - przypomnienie</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Account.Partner_Dealer__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Promo_Sales_Target__c</field>
            <operation>greaterOrEqual</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Promo_Reminder__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>extraNagroda_przypomnienie2</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Account.Promo_Reminder__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Account%3A extraNagroda - saldo</fullName>
        <actions>
            <name>Account_extraNagroda_saldo</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Sum_of_Promo_Sales__c) &amp;&amp; Partner_Dealer__c &amp;&amp; Promo_Sales_Target__c &gt;= 0</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>Diler_kontaktowa_si_z_COK</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Diler kontaktował się z COK</subject>
    </tasks>
    <tasks>
        <fullName>Diler_nie_zam_wi_nic_od_30_dni_zadzwo</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Diler nie zamówił nic od 30 dni, zadzwoń</subject>
    </tasks>
    <tasks>
        <fullName>Diler_nie_zam_wi_nic_od_90_dni_zadzwo</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Diler nie zamówił nic od 90 dni, zadzwoń</subject>
    </tasks>
    <tasks>
        <fullName>Diler_nie_zam_wi_nic_zadzwo</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Diler nie zamówił nic, zadzwoń</subject>
    </tasks>
    <tasks>
        <fullName>Nast_pny_kontakt_telefoniczny</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Account.Next_Call__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Następny kontakt telefoniczny</subject>
    </tasks>
</Workflow>
