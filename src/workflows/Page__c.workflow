<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Page_Category_Keys</fullName>
        <field>Category_Keys__c</field>
        <formula>CASE(Design_Type_Key__c, &apos;&apos;, &apos;&apos;, &apos;[&apos;&amp;Design_Type_Key__c&amp;&apos;]&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;litera L&apos;),&apos;[44]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;litera U&apos;),&apos;[56]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;z dziedzińcem&apos;),&apos;[57]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;kształt klasyczny&apos;),&apos;[61]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;wejście od południa&apos;),&apos;[45]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;salon w głębi&apos;),&apos;[59]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;styl tradycyjny&apos;),&apos;[77]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;nowoczesne&apos;),&apos;[81]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;styl dworkowy&apos;),&apos;[90]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;góralski&apos;),&apos;[98]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;mur pruski / szachulec&apos;),&apos;[104]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;bez tarasu&apos;),&apos;[55]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;z tarasem&apos;),&apos;[69]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;rezydencje&apos;),&apos;[92]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;wille&apos;),&apos;[91]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;bez werandy&apos;),&apos;[51]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;z werandą&apos;),&apos;[68]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;z ogrodem zimowym&apos;),&apos;[76]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;bez ogrodu zimowego&apos;),&apos;[107]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;kalenica równoległa do drogi&apos;),&apos;[105]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;kalenica prostopadła do drogi&apos;),&apos;[106]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;zabudowa bliźniacza&apos;),&apos;[42]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;zabudowa szeregowa&apos;),&apos;[43]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;zabudowa wolnostojąca&apos;),&apos;[49]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;bez antresoli&apos;),&apos;[60]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;z antresolą&apos;),&apos;[94]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;z wykuszem&apos;),&apos;[64]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;bez wykusza&apos;),&apos;[74]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;bez basenu&apos;),&apos;[35]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;z basenem&apos;),&apos;[71]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;garaż w bryle budynku&apos;),&apos;[66]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;garaż wysunięty do przodu&apos;),&apos;[67]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;garaż poza bryłą, w linii wejścia&apos;),&apos;[70]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;garaż cofnięty&apos;),&apos;[73]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;garaż wolnostojący&apos;),&apos;[78]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;bez wiaty garażowej&apos;),&apos;[62]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;z wiatą garażową&apos;),&apos;[80]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;strop drewniany&apos;),&apos;[95]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;strop gęstożebrowy&apos;),&apos;[96]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;strop żelbetowy&apos;),&apos;[97]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;wiązary&apos;),&apos;[99]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;drewniane&apos;),&apos;[38]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;z bali&apos;),&apos;[39]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;blaszane&apos;),&apos;[40]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;szkieletowe&apos;),&apos;[41]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;murowane&apos;),&apos;[82]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;tani w budowie&apos;),&apos;[102]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;energooszczędne&apos;),&apos;[83]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;pasywne&apos;),&apos;[84]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;z częścią rekreacyjną&apos;),&apos;[110]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;z częścią usługową&apos;),&apos;[25]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;z częścią mieszkalną&apos;),&apos;[1]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;dwurodzinny&apos;),&apos;[113]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;jednorodzinny&apos;),&apos;[112]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;z pomieszczeniem gospodarczym&apos;),&apos;[10]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;z sutereną&apos;),&apos;[114]&apos;,&apos;&apos;)&amp;
IF(INCLUDES(Categories__c,&apos;bez sutereny&apos;),&apos;[115]&apos;,&apos;&apos;)</formula>
        <name>Page: Category Keys</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Page_Copy_Design_Code_to_Name</fullName>
        <field>Name</field>
        <formula>Design__r.Code__c</formula>
        <name>Page: Copy Design Code to Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Page_Query_Update</fullName>
        <field>Query__c</field>
        <formula>SUBSTITUTE(
SUBSTITUTE(
SUBSTITUTE(

IF(NOT(ISBLANK(Search_Text__c)), &apos;Text.Value=&apos; &amp; Search_Text__c &amp; &apos;&amp;&apos;, null) &amp;
IF(NOT(ISBLANK(Usable_Area_min__c)), &apos;Area.From=&apos; &amp; TEXT(Usable_Area_min__c) &amp; &apos;&amp;&apos;, null) &amp;
IF(NOT(ISBLANK(Usable_Area_max__c)), &apos;Area.To=&apos; &amp; TEXT(Usable_Area_max__c) &amp; &apos;&amp;&apos;, null) &amp;
IF(NOT(ISBLANK(Plot_H_min__c)), &apos;AllotmentMinWidth.Value=&apos; &amp; TEXT(Plot_H_min__c) &amp; &apos;&amp;&apos;, null) &amp;
IF(NOT(ISBLANK(Height_max__c)), &apos;TotalHeight.Value=&apos; &amp; TEXT(Height_max__c) &amp; &apos;&amp;&apos;, null) &amp;
IF(NOT(ISBLANK(Footprint_max__c)), &apos;BuildingArea.Value=&apos; &amp; TEXT(Footprint_max__c) &amp; &apos;&amp;&apos;, null) &amp;
IF(INCLUDES(Levels__c, &apos;parter&apos;), &apos;Flight.47=on&amp;&apos;, null) &amp;
IF(INCLUDES(Levels__c, &apos;parter + poddasze&apos;), &apos;Flight.48=on&amp;&apos;, null) &amp;
IF(INCLUDES(Levels__c, &apos;parter + pełne piętro&apos;), &apos;Flight.63=on&amp;&apos;, null) &amp;
IF(INCLUDES(Garage__c, &apos;brak garażu&apos;), &apos;Garage.65=on&amp;&apos;, null) &amp;
IF(INCLUDES(Garage__c, &apos;garaż jedno miejsce&apos;), &apos;Garage.50=on&amp;&apos;, null) &amp;
IF(INCLUDES(Garage__c, &apos;garaż dwa lub więcej miejsc&apos;), &apos;Garage.52=on&amp;&apos;, null) &amp;

&apos;Basement.&apos; &amp; CASE(Basement__c,
&apos;bez podpiwniczenia&apos;, &apos;72&apos;,
&apos;z podpiwniczeniem&apos;, &apos;46&apos;,
null) &amp; &apos;=on&amp;&apos; &amp;

&apos;Roof.&apos; &amp; CASE(Roof__c,
&apos;dwuspadowy&apos;, &apos;4&apos;,
&apos;czterospadowy&apos;, &apos;5&apos;,
&apos;płaski&apos;, &apos;12&apos;,
&apos;jednospadowy&apos;, &apos;13&apos;,
&apos;kopertowy&apos;, &apos;7&apos;,
&apos;naczółkowy&apos;, &apos;9&apos;,
&apos;mansardowy&apos;, &apos;3&apos;,
&apos;wielospadowy&apos;, &apos;6&apos;,
&apos;namiotowy&apos;, &apos;8&apos;,
null) &amp; &apos;=on&amp;&apos; &amp;

&apos;Category.&apos; &amp; CASE(Main_Category__c, 
&apos;projekty garaży&apos;, &apos;2&apos;, 
&apos;projekty budynków gospodarczych&apos;, &apos;10&apos;, 
&apos;projekty wiat garażowych&apos;, &apos;11&apos;, 
&apos;projekty dla rolnictwa&apos;, &apos;21&apos;, 
&apos;projekty dla biznesu&apos;, &apos;25&apos;, 
&apos;projekty dodatkowe&apos;, &apos;30&apos;, 
&apos;projekty pensjonatów&apos;, &apos;22&apos;, 
&apos;projekty hoteli&apos;, &apos;23&apos;, 
&apos;projekty agroturystyczne&apos;, &apos;24&apos;, 
&apos;projekty socjalne&apos;, &apos;32&apos;, 
&apos;projekty warsztatów samochodowych&apos;, &apos;85&apos;, 
&apos;projekty budynków biurowych&apos;, &apos;103&apos;, 
&apos;projekty sklepów&apos;, &apos;86&apos;, 
&apos;projekty domów weselnych&apos;, &apos;87&apos;, 
&apos;projekty zajazdów&apos;, &apos;93&apos;, 
&apos;projekty magazynów&apos;, &apos;88&apos;, 
&apos;projekty wiat magazynowych&apos;, &apos;89&apos;, 
&apos;projekty altan&apos;, &apos;26&apos;, 
&apos;projekty basenów&apos;, &apos;27&apos;, 
&apos;projekty saun&apos;, &apos;28&apos;, 
&apos;projekty grilli i wędzarni&apos;, &apos;29&apos;, 
&apos;projekty szamb&apos;, &apos;31&apos;, 
&apos;projekty kuchni letnich&apos;, &apos;33&apos;, 
&apos;projekty sanitarne&apos;, &apos;35&apos;, 
&apos;projekty ogrodzeń&apos;, &apos;37&apos;, 
&apos;projekty stodół&apos;, &apos;14&apos;, 
&apos;projekty stajni&apos;, &apos;15&apos;, 
&apos;projekty kurników&apos;, &apos;16&apos;, 
&apos;projekty tuczarni&apos;, &apos;17&apos;, 
&apos;projekty obór&apos;, &apos;18&apos;, 
&apos;projekty lonżowni&apos;, &apos;19&apos;, 
&apos;projekty ujeżdżalni&apos;, &apos;20&apos;, 
&apos;projekty chlewni&apos;, &apos;34&apos;, 
&apos;projekty zbiorników na gnojowicę&apos;, &apos;36&apos;, 
null) &amp; &apos;=on&amp;&apos; &amp;

&apos;Category.&apos; &amp; CASE(Building_Cost__c, 
&apos;energooszczędne&apos;, &apos;83&apos;, 
&apos;pasywne&apos;, &apos;84&apos;, 
&apos;tani w budowie&apos;, &apos;102&apos;, 
null) &amp; &apos;=on&amp;&apos; &amp;

&apos;Category.&apos; &amp; CASE(Technology__c, 
&apos;drewniane&apos;, &apos;38&apos;, 
&apos;z bali&apos;, &apos;39&apos;, 
&apos;blaszane&apos;, &apos;40&apos;, 
&apos;szkieletowe&apos;, &apos;41&apos;, 
null) &amp; &apos;=on&amp;&apos; &amp;

&apos;Category.&apos; &amp; CASE(Style__c, 
&apos;nowoczesne&apos;, &apos;81&apos;, 
&apos;w stylu dworkowym&apos;, &apos;90&apos;, 
&apos;wille&apos;, &apos;91&apos;, 
&apos;rezydencje&apos;, &apos;92&apos;, 
&apos;w kształcie litery L&apos;, &apos;44&apos;, 
null) &amp; &apos;=on&amp;&apos; &amp;

&apos;Category.&apos; &amp; CASE(Living_Type__c, 
&apos;projekty bloków&apos;, &apos;49&apos;, 
&apos;do zabudowy bliźniaczej&apos;, &apos;42&apos;, 
&apos;do zabudowy szeregowej&apos;, &apos;43&apos;, 
&apos;dwurodzinne&apos;, &apos;74&apos;, 
&apos;wielorodzinne&apos;, &apos;75&apos;, 
&apos;jednorodzinne&apos;, &apos;77&apos;, 
&apos;kamienica&apos;, &apos;100&apos;, 
&apos;dla deweloperów&apos;, &apos;101&apos;, 
null) &amp; &apos;=on&amp;&apos; &amp;

&apos;Category.&apos; &amp; CASE(Add_Ons__c, 
&apos;z pomieszczeniem gospodarczym&apos;, &apos;62&apos;, 
&apos;z wykuszem&apos;, &apos;64&apos;, 
&apos;z siłownią&apos;, &apos;66&apos;, 
&apos;z jacuzzi&apos;, &apos;67&apos;, 
&apos;z werandą&apos;, &apos;68&apos;, 
&apos;z tarasem&apos;, &apos;69&apos;, 
&apos;z salą fitness&apos;, &apos;70&apos;, 
&apos;z basenem&apos;, &apos;71&apos;, 
&apos;z częścią mieszkalną&apos;, &apos;73&apos;, 
&apos;z ogrodem zimowym&apos;, &apos;76&apos;, 
&apos;ze spiżarnią&apos;, &apos;78&apos;, 
&apos;z antresolą&apos;, &apos;94&apos;, 
&apos;z wejściem od południa&apos;, &apos;45&apos;, 
&apos;z wiatą garażową&apos;, &apos;80&apos;, 
null) &amp; &apos;=on&amp;&apos;

, &apos;Category.=on&amp;&apos;, null), &apos;Roof.=on&amp;&apos;, null), &apos;Basement.=on&amp;&apos;, null)</formula>
        <name>Page: Query Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Page_Update_Unique_Name</fullName>
        <field>Name__c</field>
        <formula>Name</formula>
        <name>Page: Update Unique Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Page%3A Another Design specified or just assigned</fullName>
        <actions>
            <name>Page_Copy_Design_Code_to_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>NOT(ISBLANK(Design__r.Code__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Page%3A Every change</fullName>
        <actions>
            <name>Page_Category_Keys</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Page_Query_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>True</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Page%3A Unique Name</fullName>
        <actions>
            <name>Page_Update_Unique_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Name) || ISNEW()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
