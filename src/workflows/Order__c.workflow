<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Potwierdzenia_zam_wienia_Partner</fullName>
        <description>Potwierdzenia zamówienia (Partner)</description>
        <protected>false</protected>
        <recipients>
            <field>Billing_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Partner_User__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COK/Potwierdzenie_zamowienia</template>
    </alerts>
    <alerts>
        <fullName>Potwierdzenia_zam_wienia_katalogu</fullName>
        <description>Potwierdzenia zamówienia katalogu</description>
        <protected>false</protected>
        <recipients>
            <field>Billing_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>kontakt@extradom.pl</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>COK/Potwierdzenie_zam_wienia_katalogu</template>
    </alerts>
    <alerts>
        <fullName>Potwierdzenia_zam_wienia_projektu</fullName>
        <description>Potwierdzenia zamówienia</description>
        <protected>false</protected>
        <recipients>
            <field>Billing_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>kontakt@extradom.pl</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>COK/Potwierdzenie_zamowienia</template>
    </alerts>
    <alerts>
        <fullName>Potwierdzenia_zam_wienia_projektu_test</fullName>
        <description>Potwierdzenia zamówienia TEST</description>
        <protected>false</protected>
        <recipients>
            <field>Billing_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>kontakt@extradom.pl</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>COK/Potwierdzenie_zamowienia</template>
    </alerts>
    <fieldUpdates>
        <fullName>Account_Next_Call_after_12_days_cat</fullName>
        <field>Next_Call__c</field>
        <formula>TODAY()+12+CASE(MOD(TODAY()+12-DATE(1900,1,7),7),0,1,6,2,0)</formula>
        <name>Account: Next Call after 12 days / cat</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_On_Catalogue_Sent_Next_Call</fullName>
        <field>Next_Call__c</field>
        <formula>TODAY()+30</formula>
        <name>Account: On Catalogue Sent Next Call</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_Account_Update_Billing_Address</fullName>
        <field>BillingStreet</field>
        <formula>Billing_Address__c</formula>
        <name>Order / Account: Update Billing Address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_Account_Update_Billing_City</fullName>
        <field>BillingCity</field>
        <formula>Billing_City__c</formula>
        <name>Order / Account: Update Billing City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_Account_Update_Billing_Postal</fullName>
        <field>BillingPostalCode</field>
        <formula>Billing_Postal_Code__c</formula>
        <name>Order / Account: Update Billing Postal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_Account_Update_Shipping_Address</fullName>
        <field>ShippingStreet</field>
        <formula>Shipping_Address__c</formula>
        <name>Order / Account: Update Shipping Address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_Account_Update_Shipping_City</fullName>
        <field>ShippingCity</field>
        <formula>Shipping_City__c</formula>
        <name>Order / Account: Update Shipping City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_Account_Update_Shipping_Postal</fullName>
        <field>ShippingPostalCode</field>
        <formula>Shipping_Postal_Code__c</formula>
        <name>Order / Account: Update Shipping Postal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_Address_Billing_Shipping</fullName>
        <field>Shipping_Address__c</field>
        <formula>BLANKVALUE(Billing_Address__c,
Billing_Address_Street_Name__c &amp; &apos; &apos; &amp; Billing_Address_Street_No__c &amp; IF(ISBLANK(Billing_Address_Street_Flat__c),&apos;&apos;,&apos;/&apos; &amp; Billing_Address_Street_Flat__c))</formula>
        <name>Order: Address (Billing-&gt;Shipping)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_Billing_Address</fullName>
        <field>Billing_Address__c</field>
        <formula>Billing_Address_Street_Name__c &amp; &apos; &apos; &amp; Billing_Address_Street_No__c &amp; IF(ISBLANK(Billing_Address_Street_Flat__c),&apos;&apos;,&apos;/&apos; &amp; Billing_Address_Street_Flat__c)</formula>
        <name>Order: Billing Address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_Billing_Name</fullName>
        <field>Billing_Name__c</field>
        <formula>Billing_First_Name__c &amp; &apos; &apos; &amp; Billing_Last_Name__c</formula>
        <name>Order: Billing Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_Billing_Postal_Code</fullName>
        <field>Billing_Postal_Code__c</field>
        <formula>Billing_Postal_Code_Part_1__c &amp; &apos;-&apos; &amp; Billing_Postal_Code_Part_2__c</formula>
        <name>Order: Billing Postal Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_City_Billing_Shipping</fullName>
        <field>Shipping_City__c</field>
        <formula>Billing_City__c</formula>
        <name>Order: City (Billing-&gt;Shipping)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_Confirm_Agent</fullName>
        <field>Confirm_Agent__c</field>
        <formula>$User.Short_Name__c</formula>
        <name>Order: Confirm Agent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_Current_Price</fullName>
        <field>Gross_Price__c</field>
        <formula>IF(ISBLANK(Design__c),null, IF(Design__r.Current_Price__c&gt;0 &amp;&amp; ISPICKVAL(Design__r.Status__c,&apos;Active&apos;),Design__r.Current_Price__c,null))</formula>
        <name>Order: Current Price</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_Design_Type</fullName>
        <field>Design_Type__c</field>
        <formula>TEXT(Design__r.Type__c)</formula>
        <name>Order: Design Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_Name_Billing_Shipping</fullName>
        <field>Shipping_Name__c</field>
        <formula>IF(ISBLANK(Billing_Company__c),
BLANKVALUE(Billing_Name__c,Billing_First_Name__c &amp; &apos; &apos; &amp; Billing_Last_Name__c),
Billing_Company__c &amp; &apos; (&apos; &amp; BLANKVALUE(Billing_Name__c,Billing_First_Name__c &amp; &apos; &apos; &amp; Billing_Last_Name__c) &amp; &apos;)&apos;)</formula>
        <name>Order: Name (Billing-&gt;Shipping)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_Order_Source_Extradom_pl</fullName>
        <field>Order_Source__c</field>
        <literalValue>Extradom.pl</literalValue>
        <name>Order: Order Source Extradom.pl</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_Postal_Code_Billing_Shipping</fullName>
        <field>Shipping_Postal_Code__c</field>
        <formula>BLANKVALUE(Billing_Postal_Code__c, Billing_Postal_Code_Part_1__c &amp; &apos;-&apos; &amp;Billing_Postal_Code_Part_2__c)</formula>
        <name>Order: Postal Code (Billing-&gt;Shipping)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_Returned_Date</fullName>
        <field>Returned_Date__c</field>
        <formula>TODAY()</formula>
        <name>Order: Returned Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_Sales_Agent_Alias</fullName>
        <field>Sales_Agent_Alias__c</field>
        <formula>CASE(Sales_Agent__c,&apos;Karolina S.&apos;,&apos;kstep&apos;,&apos;Małgorzata G.&apos;,&apos;mgrze&apos;,&apos;Anita S.&apos;,&apos;asnoc&apos;,&apos;Dagmara J.&apos;,&apos;djagu&apos;,&apos;Joanna K.&apos;,&apos;jklec&apos;,&apos;Joanna S.&apos;,&apos;jskoc&apos;,&apos;Judyta R.&apos;,&apos;jrybk&apos;,&apos;Malwina K.&apos;,&apos;mkaka&apos;,&apos;Martyna K.&apos;,&apos;mkuch&apos;,&apos;Paulina D.&apos;,&apos;pdrze&apos;,&apos;Martyna S.&apos;,&apos;mszym&apos;,&apos;Izabela Z.&apos;,&apos;izygm&apos;,&apos;&apos;)</formula>
        <name>Order: Sales Agent Alias</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_Sent_Date</fullName>
        <field>Sent_Date__c</field>
        <formula>TODAY()</formula>
        <name>Order: Sent Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_Shipping_Address</fullName>
        <field>Shipping_Address__c</field>
        <formula>Shipping_Address_Street_Name__c &amp; &apos; &apos; &amp; Shipping_Address_Street_No__c &amp; IF(ISBLANK(Shipping_Address_Street_Flat__c),&apos;&apos;,&apos;/&apos; &amp; Shipping_Address_Street_Flat__c)</formula>
        <name>Order: Shipping Address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_Shipping_Methods_Express_Cash</fullName>
        <field>Shipping_Method__c</field>
        <literalValue>Express (Cash)</literalValue>
        <name>Order: Shipping Methods Express Cash</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_Shipping_Methods_Express_Prepayme</fullName>
        <field>Shipping_Method__c</field>
        <literalValue>Express (Prepayment)</literalValue>
        <name>Order: Shipping Methods Express Prepayme</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_Shipping_Name</fullName>
        <field>Shipping_Name__c</field>
        <formula>Shipping_First_Name__c &amp; &apos; &apos; &amp; Shipping_Last_Name__c</formula>
        <name>Order: Shipping Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_Shipping_Phone</fullName>
        <field>Shipping_Phone__c</field>
        <formula>Billing_Phone__c</formula>
        <name>Order: Shipping Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_Shipping_Postal_Code</fullName>
        <field>Shipping_Postal_Code__c</field>
        <formula>Shipping_Postal_Code_Part_1__c &amp; &apos;-&apos; &amp; Shipping_Postal_Code_Part_2__c</formula>
        <name>Order: Shipping Postal Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_Update_Order_Date</fullName>
        <field>Order_Date__c</field>
        <formula>TODAY()</formula>
        <name>Order: Update Order Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_Update_Shipping_Cost</fullName>
        <field>Gross_Shipping_Cost__c</field>
        <formula>CASE(Shipping_Method__c, &apos;Regular (Prepayment)&apos;, 0, &apos;DPD (Prepayment)&apos;, 19, &apos;DPD (Cash on Delivery)&apos;, 25, &apos;Express (Cash)&apos;, 25, &apos;Express (Prepayment)&apos;, 19, &apos;Express (Online)&apos;, 19, &apos;Office (Prepayment)&apos;, 0, &apos;Office (Cash)&apos;, 0, &apos;Office (Online)&apos;, 0, null)</formula>
        <name>Order: Update Shipping Cost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Order%3A DPD %28Cash on Delivery%29</fullName>
        <actions>
            <name>Order_Shipping_Methods_Express_Cash</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order__c.Shipping_Method__c</field>
            <operation>equals</operation>
            <value>DPD (Cash on Delivery)</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Order%3A DPD %28Prepayment%29</fullName>
        <actions>
            <name>Order_Shipping_Methods_Express_Prepayme</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order__c.Shipping_Method__c</field>
            <operation>equals</operation>
            <value>DPD (Prepayment)</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Order%3A Grupa 400 - Zamówienie promocyjne - w toku</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Account.Partner_Dealer__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Status__c</field>
            <operation>equals</operation>
            <value>To Process,Reserved,Waiting For Payment,Ordered,In Progress,Ready,Sent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Promo_Sales_Target__c</field>
            <operation>greaterThan</operation>
            <value>10000</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Promo_Sales_Target_Balance__c</field>
            <operation>lessThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.CreatedDate</field>
            <operation>greaterOrEqual</operation>
            <value>6/1/2017</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.CreatedDate</field>
            <operation>lessOrEqual</operation>
            <value>8/31/2017</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Order%3A On Billing Address Edit</fullName>
        <actions>
            <name>Order_Account_Update_Billing_Address</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Order_Account_Update_Billing_City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Order_Account_Update_Billing_Postal</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>ISBLANK(Account__r.BillingCity) &amp;&amp; (ISCHANGED(Billing_Address__c) ||
ISCHANGED(Billing_City__c) ||
ISCHANGED(Billing_Name__c) ||
ISCHANGED(Billing_Postal_Code__c) ||
ISCHANGED(Billing_Tax_Number__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Order%3A On Catalogue Sent</fullName>
        <actions>
            <name>Account_On_Catalogue_Sent_Next_Call</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order__c.Order_Type__c</field>
            <operation>equals</operation>
            <value>Catalogue</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>equals</operation>
            <value>Investor</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Next_Call__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Status__c</field>
            <operation>equals</operation>
            <value>Sent</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Order%3A On Catalogue Sent and Lead</fullName>
        <actions>
            <name>Account_Next_Call_after_12_days_cat</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Next_Call__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Add_Ons__c</field>
            <operation>includes</operation>
            <value>Katalog</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Status__c</field>
            <operation>equals</operation>
            <value>Sent</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Order%3A On Create</fullName>
        <actions>
            <name>Potwierdzenia_zam_wienia_projektu</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Order_Design_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.UserType</field>
            <operation>equals</operation>
            <value>Guest,High Volume Portal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Gross_Price__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Partner_User__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Design_Status__c</field>
            <operation>equals</operation>
            <value>Active</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Order%3A On Create %28Billing Address%29</fullName>
        <actions>
            <name>Order_Billing_Address</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order__c.Billing_Address__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Billing_Address_Street_Name__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Billing_Address_Street_No__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Order%3A On Create %28Billing Name%29</fullName>
        <actions>
            <name>Order_Billing_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order__c.Billing_Name__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Billing_First_Name__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Billing_Last_Name__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Order%3A On Create %28Billing Postal Code%29</fullName>
        <actions>
            <name>Order_Billing_Postal_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order__c.Billing_Postal_Code__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Billing_Postal_Code_Part_1__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Billing_Postal_Code_Part_2__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Order%3A On Create %28Catalogue%29</fullName>
        <actions>
            <name>Potwierdzenia_zam_wienia_katalogu</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.UserType</field>
            <operation>equals</operation>
            <value>Guest,High Volume Portal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Order_Type__c</field>
            <operation>equals</operation>
            <value>Catalogue</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Partner_User__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Order%3A On Create %28Gross Price%29</fullName>
        <actions>
            <name>Order_Current_Price</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order__c.Gross_Price__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Order%3A On Create %28Partner%29</fullName>
        <actions>
            <name>Potwierdzenia_zam_wienia_Partner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.UserType</field>
            <operation>equals</operation>
            <value>Guest,High Volume Portal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Gross_Price__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Partner_User__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Design_Status__c</field>
            <operation>equals</operation>
            <value>Active</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Order%3A On Create %28Shipping Address - not specified%29</fullName>
        <actions>
            <name>Order_Address_Billing_Shipping</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Order_City_Billing_Shipping</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Order_Postal_Code_Billing_Shipping</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7 AND 8</booleanFilter>
        <criteriaItems>
            <field>Order__c.Shipping_Address__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Shipping_Postal_Code__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Shipping_City__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Shipping_Address_Street_Name__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Shipping_Address_Street_No__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Shipping_Address_Street_Flat__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Shipping_Postal_Code_Part_1__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Shipping_Postal_Code_Part_2__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Order%3A On Create %28Shipping Address%29</fullName>
        <actions>
            <name>Order_Shipping_Address</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order__c.Shipping_Address__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Shipping_Address_Street_Name__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Shipping_Address_Street_No__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Order%3A On Create %28Shipping Cost%29</fullName>
        <actions>
            <name>Order_Update_Shipping_Cost</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order__c.Gross_Shipping_Cost__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>notEqual</operation>
            <value>Dealer</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Order%3A On Create %28Shipping Name - not specified%29</fullName>
        <actions>
            <name>Order_Name_Billing_Shipping</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order__c.Shipping_Name__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Shipping_First_Name__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Shipping_Last_Name__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Order%3A On Create %28Shipping Name%29</fullName>
        <actions>
            <name>Order_Shipping_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order__c.Shipping_Name__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Shipping_First_Name__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Shipping_Last_Name__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Order%3A On Create %28Shipping Phone - not specified%29</fullName>
        <actions>
            <name>Order_Shipping_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order__c.Shipping_Phone__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Order%3A On Create %28Shipping Postal Code%29</fullName>
        <actions>
            <name>Order_Shipping_Postal_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order__c.Shipping_Postal_Code__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Shipping_Postal_Code_Part_1__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Shipping_Postal_Code_Part_2__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Order%3A On Create %28Source%29</fullName>
        <actions>
            <name>Order_Order_Source_Extradom_pl</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>(ISPICKVAL($User.UserType,&apos;Guest&apos;) || ISPICKVAL($User.UserType,&apos;High Volume Portal&apos;)) &amp;&amp; (ISPICKVAL(Order_Source__c,&apos;Internal&apos;) || ISBLANK(TEXT(Order_Source__c)))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Order%3A On Returned</fullName>
        <actions>
            <name>Order_Returned_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order__c.Status__c</field>
            <operation>equals</operation>
            <value>Returned</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Returned_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Order%3A On Shipping Address Edit</fullName>
        <actions>
            <name>Order_Account_Update_Shipping_Address</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Order_Account_Update_Shipping_City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Order_Account_Update_Shipping_Postal</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>ISBLANK(Account__r.ShippingCity) &amp;&amp; (ISCHANGED(Shipping_Address__c) ||
ISCHANGED(Shipping_City__c) ||
ISCHANGED(Shipping_Name__c) ||
ISCHANGED(Shipping_Postal_Code__c) ||
ISCHANGED(Shipping_Phone__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Order%3A On Status Changed%3A Sent</fullName>
        <actions>
            <name>Order_Sent_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order__c.Status__c</field>
            <operation>equals</operation>
            <value>Sent</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Order%3A On Status Changed%3A To Process</fullName>
        <actions>
            <name>Order_Confirm_Agent</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Order_Design_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Order_Update_Order_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order__c.Status__c</field>
            <operation>equals</operation>
            <value>To Process</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Order%3A Sales Agent Alias</fullName>
        <actions>
            <name>Order_Sales_Agent_Alias</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
