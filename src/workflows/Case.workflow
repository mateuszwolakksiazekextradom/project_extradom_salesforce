<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Potwierdzenie_zam_wienia_projektu_Partner_QCA</fullName>
        <ccEmails>biuro@qcastudio.com</ccEmails>
        <description>Potwierdzenie zamówienia projektu Partner: QCA</description>
        <protected>false</protected>
        <recipients>
            <recipient>agnieszka.lozinska@extradom.pl</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Zam_wienie_projektu_QCA</template>
    </alerts>
    <alerts>
        <fullName>Prze_lij_zam_wienie_do_odpowiedniego_dzia_u</fullName>
        <description>Prześlij zamówienie do odpowiedniego działu</description>
        <protected>false</protected>
        <recipients>
            <recipient>agnieszka.lozinska@extradom.pl</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Zam_wienie_profilu</template>
    </alerts>
    <rules>
        <fullName>Case%3A On Create</fullName>
        <active>false</active>
        <formula>True</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Zamówienie profilu</fullName>
        <actions>
            <name>Prze_lij_zam_wienie_do_odpowiedniego_dzia_u</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>contains</operation>
            <value>Zamówienie</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Zamówienie projektu domu QCA Studio Projektowe</fullName>
        <actions>
            <name>Potwierdzenie_zam_wienia_projektu_Partner_QCA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>startsWith</operation>
            <value>Zamówienie projektu</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Source_Partner_ID__c</field>
            <operation>equals</operation>
            <value>qca-studio-projektowe</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
