<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Prezentacja_new</fullName>
        <description>Prezentacja (new)</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COK/Prezentacja_projekt_w_NOWA</template>
    </alerts>
</Workflow>
