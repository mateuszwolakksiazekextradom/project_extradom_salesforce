<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>New_Order_Notification</fullName>
        <description>New Order Notification</description>
        <protected>false</protected>
        <recipients>
            <field>BillingEmail__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>kontakt@extradom.pl</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>COK/Zam_wienie_NOWE</template>
    </alerts>
    <alerts>
        <fullName>Order_Confirmation</fullName>
        <description>Order_Confirmation</description>
        <protected>false</protected>
        <recipients>
            <field>BillingEmail__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>kontakt@extradom.pl</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>COK/Test2</template>
    </alerts>
    <fieldUpdates>
        <fullName>Billing_Phone_Remove_special_characters</fullName>
        <field>BillingPhone__c</field>
        <formula>SUBSTITUTE( 
SUBSTITUTE( 
SUBSTITUTE( 
SUBSTITUTE( 
SUBSTITUTE(BillingPhone__c, &quot;(&quot;, &quot;&quot;), &quot;)&quot;, &quot;&quot;), &quot; &quot;, &quot;&quot;), &quot;-&quot;, &quot;&quot;),&quot;+&quot;,&quot;00&quot;)</formula>
        <name>Billing Phone: Remove special characters</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_SetSendEmailToFalse</fullName>
        <field>Send_Email__c</field>
        <literalValue>0</literalValue>
        <name>Order: Set Send Email To False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Reduction</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ShippingPhone_Remove_special_characters</fullName>
        <field>ShippingPhone__c</field>
        <formula>SUBSTITUTE( 
SUBSTITUTE( 
SUBSTITUTE( 
SUBSTITUTE( 
SUBSTITUTE(ShippingPhone__c, &quot;(&quot;, &quot;&quot;), &quot;)&quot;, &quot;&quot;), &quot; &quot;, &quot;&quot;), &quot;-&quot;, &quot;&quot;),&quot;+&quot;,&quot;00&quot;)</formula>
        <name>ShippingPhone: Remove special characters</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Order On Update Test</fullName>
        <actions>
            <name>Order_Confirmation</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>User.UserType</field>
            <operation>equals</operation>
            <value>Guest,High Volume Portal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.Status</field>
            <operation>equals</operation>
            <value>Draft</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.TotalAmount</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Order%3A On Activation</fullName>
        <actions>
            <name>New_Order_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Order_SetSendEmailToFalse</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(and(ISPICKVAL(StatusCode, &apos;A&apos;), Owner:User.Department == &apos;COK&apos;, RecordType.DeveloperName == &apos;Regular&apos;, ISCHANGED(StatusCode) ),  Send_Email__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Order%3A On Phone Edit</fullName>
        <actions>
            <name>Billing_Phone_Remove_special_characters</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ShippingPhone_Remove_special_characters</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(BillingPhone__c) || ISCHANGED(ShippingPhone__c) || (ISNEW() &amp;&amp; NOT(ISBLANK(BillingPhone__c) || NOT(ISBLANK(ShippingPhone__c))))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Order%3A Reduction Opportunity</fullName>
        <active>false</active>
        <formula>IsReductionOrder &amp;&amp; ISBLANK(OpportunityId)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SetPricebookId</fullName>
        <active>false</active>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SetReductionOrder</fullName>
        <actions>
            <name>Set_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IsReductionOrder</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
