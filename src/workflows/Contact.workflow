<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Contact_Test</fullName>
        <description>Contact: Test</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>COK/Analiza_dzia_ki_potw_zam_wienia</template>
    </alerts>
    <alerts>
        <fullName>Contact_Test2</fullName>
        <ccEmails>bartosz.lukjanski@extradom.pl</ccEmails>
        <description>Contact: Test2</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>COK/Analiza_dzia_ki_potw_zam_wienia</template>
    </alerts>
    <alerts>
        <fullName>DK_Notus</fullName>
        <description>DK Notus</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Telemarketing_Request_Web_To_Lead_DK_Notus</template>
    </alerts>
    <alerts>
        <fullName>SMS_Informacja_o_opiekunie_COK</fullName>
        <description>SMS: Informacja o opiekunie COK</description>
        <protected>false</protected>
        <recipients>
            <field>Email_to_SMS__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SMS_Informacja_o_opiekunie_COK</template>
    </alerts>
    <alerts>
        <fullName>SMS_Po_rysunkach_i_prezentacji</fullName>
        <description>SMS: Po rysunkach i prezentacji</description>
        <protected>false</protected>
        <recipients>
            <field>Email_to_SMS__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SMS_Po_rysunkach_i_prezentacji</template>
    </alerts>
    <fieldUpdates>
        <fullName>Community_Premium_Member_Set_Date</fullName>
        <field>Community_Premium_Member_Set_Date__c</field>
        <formula>TODAY()</formula>
        <name>Community Premium Member: Set Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Community_Premium_Member_Set_User</fullName>
        <field>Community_Premium_Member_Set_User__c</field>
        <formula>$User.FirstName &amp; &apos; &apos; &amp; $User.LastName</formula>
        <name>Community Premium Member: Set User</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_CM_Connected_Orgs_List_Update</fullName>
        <field>CM_Connected_Orgs_List__c</field>
        <formula>Community_Member__r.Connected_Orgs_List__c</formula>
        <name>Contact: CM Connected Orgs List Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_CM_Connected_Orgs_Update</fullName>
        <field>CM_Connected_Orgs__c</field>
        <formula>Community_Member__r.Connected_Orgs__c</formula>
        <name>Contact: CM Connected Orgs Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Update_Valid_Email_ID</fullName>
        <field>EmailId__c</field>
        <formula>Email</formula>
        <name>Contact: Update Valid Email ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Update_Valid_Phone_ID</fullName>
        <field>PhoneId__c</field>
        <formula>Phone</formula>
        <name>Contact: Update Valid Phone ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Phone_Remove_special_characters</fullName>
        <field>Phone</field>
        <formula>SUBSTITUTE(
SUBSTITUTE( 
SUBSTITUTE( 
SUBSTITUTE( 
SUBSTITUTE(Phone, &quot;(&quot;, &quot;&quot;), &quot;)&quot;, &quot;&quot;), &quot; &quot;, &quot;&quot;), &quot;-&quot;, &quot;&quot;),&quot;+&quot;,&quot;00&quot;)</formula>
        <name>Phone: Remove special characters</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reveived_On_Create_Email</fullName>
        <field>Received_On_Create_Email__c</field>
        <literalValue>1</literalValue>
        <name>Reveived On Create Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Contact%3A After Dealer send sketches</fullName>
        <actions>
            <name>Diler_dosta_rysunki_szczeg_owe</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <formula>NOT(ISBLANK(Last_Reminder_SMS_Date__c)) &amp;&amp; ISCHANGED(Last_Reminder_SMS_Date__c) &amp;&amp; ISPICKVAL(Account.Type,&apos;Dealer&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Live Dealers</fullName>
        <actions>
            <name>Diler_na_stronie</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Is_Live__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>equals</operation>
            <value>Dealer</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Next Call</fullName>
        <active>false</active>
        <formula>Next_Call__c &gt;= TODAY() &amp;&amp; (
  Next_Call__c &gt; PRIORVALUE(Next_Call__c) ||
  ISBLANK(PRIORVALUE(Next_Call__c))
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A On Community Member Data Changed</fullName>
        <actions>
            <name>Contact_CM_Connected_Orgs_List_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Contact_CM_Connected_Orgs_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>Community_Member__r.Connected_Orgs__c &lt;&gt; CM_Connected_Orgs__c || Community_Member__r.Connected_Orgs_List__c &lt;&gt; CM_Connected_Orgs_List__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A On Edit</fullName>
        <actions>
            <name>Contact_Update_Valid_Email_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Contact_Update_Valid_Phone_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Phone_Remove_special_characters</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A On First Sales Design Call Task</fullName>
        <actions>
            <name>SMS_Informacja_o_opiekunie_COK</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7</booleanFilter>
        <criteriaItems>
            <field>Contact.First_Call_Sales_Design__c</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.DoNotCall</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.HasOptedOutOfEmail</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Sales_Has_Design__c</field>
            <operation>equals</operation>
            <value>No</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Is_Mobile__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Account_Owner_First_Name_SMS__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>User.First_Name_SMS__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A On Org Member Started</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Contact.Community_Is_Company_Member__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Community_Membercode__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.orgXml__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A On Premium Member Canceled</fullName>
        <active>true</active>
        <formula>PRIORVALUE(Community_Is_Premium_Member__c)=True &amp;&amp;
Community_Is_Premium_Member__c=False &amp;&amp; NOT(ISBLANK(Community_Membercode__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A On Premium Member Set</fullName>
        <actions>
            <name>Community_Premium_Member_Set_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Community_Premium_Member_Set_User</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Community_Is_Premium_Member__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A On Premium Member Started</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Contact.Community_Is_Premium_Member__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Community_Membercode__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Contact.Community_Premium_Member_End_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Contact%3A SMS%3A Po rysunkach i prezentacji</fullName>
        <actions>
            <name>SMS_Po_rysunkach_i_prezentacji</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>NOT(ISBLANK(Last_Reminder_SMS_Date__c)) &amp;&amp; ISCHANGED(Last_Reminder_SMS_Date__c) &amp;&amp; Hour_Now__c &lt;= 22 &amp;&amp; Hour_Now__c &gt;= 8 &amp;&amp; NOT(DoNotCall) &amp;&amp; Is_Mobile__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contact%3A Test</fullName>
        <actions>
            <name>Contact_Test2</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Reveived_On_Create_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Wys_ano_klauzul_na_dodanie_emaila</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <formula>($User.Department == &apos;COK&apos; || $User.Id == &apos;005b00000019iwBAAQ&apos;)   &amp;&amp; ((ISNEW() &amp;&amp; Email != NULL) || (ISCHANGED(Email) &amp;&amp; PRIORVALUE(Email) == NULL &amp;&amp; Email != NULL)) &amp;&amp; Received_On_Create_Email__c = false</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>Automatyczny_SMS_Opiekun</fullName>
        <assignedTo>mariusz.juszczyk@extradom.pl</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Do klienta został wysłany SMS automatyczny z informacją o opiekunie COK.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Low</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Automatyczny SMS (Opiekun)</subject>
    </tasks>
    <tasks>
        <fullName>Automatyczny_SMS_Sprawd_poczt</fullName>
        <assignedTo>mariusz.juszczyk@extradom.pl</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Witam, sprawdz poczte e-mail, wlasnie otrzymales dodatkowe materialy do Twojego projektu. Az 3250zl w prezencie. Tel 717152060 www.extradom.pl</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Low</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Automatyczny SMS (Sprawdź pocztę)</subject>
    </tasks>
    <tasks>
        <fullName>Diler_dosta_rysunki_szczeg_owe</fullName>
        <assignedToType>accountOwner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Diler dostał rysunki szczegółowe</subject>
    </tasks>
    <tasks>
        <fullName>Diler_na_stronie</fullName>
        <assignedToType>accountOwner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Diler na stronie</subject>
    </tasks>
    <tasks>
        <fullName>Wys_ano_klauzul_na_dodanie_emaila</fullName>
        <assignedToType>creator</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Wysłano klauzulę - na dodanie emaila</subject>
    </tasks>
</Workflow>
