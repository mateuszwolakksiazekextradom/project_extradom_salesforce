<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Cost_Total_R_M_S</fullName>
        <field>Total__c</field>
        <formula>BLANKVALUE(R__c,0) + BLANKVALUE(M__c,0) + BLANKVALUE(S__c,0)</formula>
        <name>Cost: Total = R+M+S</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Cost%3A On Edit %28Calc Total from R%2BM%2BS%29</fullName>
        <actions>
            <name>Cost_Total_R_M_S</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Cost__c.Total__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Cost__c.R__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Cost__c.M__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Cost__c.S__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
