<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Opportunity_Stage_Enter_Determining_Cr</fullName>
        <field>Enter_Date_Determining_Criteria__c</field>
        <formula>IF(
  OR(
    ISPICKVAL(StageName,&apos;Determining Critera&apos;),
    ISPICKVAL(StageName,&apos;Solution Proposal&apos;),
    ISPICKVAL(StageName,&apos;Negotiating Quote&apos;),
    ISPICKVAL(StageName,&apos;Closed Won&apos;)
  ),
  BLANKVALUE(Enter_Date_Determining_Criteria__c,NOW()),
  IF(
    AND(
      NOT(IsWon),
      IsClosed
    ),
    Enter_Date_Determining_Criteria__c,
    null
  )
)</formula>
        <name>Opportunity: Stage: Enter Determining Cr</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Stage_Enter_Lost</fullName>
        <field>Enter_Date_Lost__c</field>
        <formula>IF(
  AND(
    NOT(IsWon),
    IsClosed
  ),
  BLANKVALUE(Enter_Date_Lost__c,NOW()),
  null
)</formula>
        <name>Opportunity: Stage: Enter Lost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Stage_Enter_Negotiating_Qu</fullName>
        <field>Enter_Date_Negotiating_Quote__c</field>
        <formula>IF(
  OR(
    ISPICKVAL(StageName,&apos;Negotiating Quote&apos;),
    ISPICKVAL(StageName,&apos;Closed Won&apos;)
  ),
  BLANKVALUE(Enter_Date_Negotiating_Quote__c,NOW()),
  IF(
    AND(
      NOT(IsWon),
      IsClosed
    ),
    Enter_Date_Negotiating_Quote__c,
    null
  )
)</formula>
        <name>Opportunity: Stage: Enter Negotiating Qu</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Stage_Enter_Solution_Propo</fullName>
        <field>Enter_Date_Solution_Proposal__c</field>
        <formula>IF(
  OR(
    ISPICKVAL(StageName,&apos;Solution Proposal&apos;),
    ISPICKVAL(StageName,&apos;Negotiating Quote&apos;),
    ISPICKVAL(StageName,&apos;Closed Won&apos;)
  ),
  BLANKVALUE(Enter_Date_Solution_Proposal__c,NOW()),
  IF(
    AND(
      NOT(IsWon),
      IsClosed
    ),
    Enter_Date_Solution_Proposal__c,
    null
  )
)</formula>
        <name>Opportunity: Stage: Enter Solution Propo</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Stage_Enter_Won</fullName>
        <field>Enter_Date_Won__c</field>
        <formula>IF( 
  ISPICKVAL(StageName,&apos;Closed Won&apos;), 
  BLANKVALUE(Enter_Date_Won__c,NOW()), 
  IF( 
    AND(
      NOT(IsWon),
      IsClosed
    ),
    Enter_Date_Won__c, 
    null 
  ) 
)</formula>
        <name>Opportunity: Stage: Enter Won</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Opportunity%3A On Stage Change</fullName>
        <actions>
            <name>Opportunity_Stage_Enter_Determining_Cr</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Opportunity_Stage_Enter_Lost</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Opportunity_Stage_Enter_Negotiating_Qu</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Opportunity_Stage_Enter_Solution_Propo</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Opportunity_Stage_Enter_Won</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(    ISCHANGED(StageName),    ISNEW()  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
