<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Keyword_Assign_Date</fullName>
        <field>Assign_Date__c</field>
        <formula>TODAY()</formula>
        <name>Keyword: Assign Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Keyword_Assigned_Page_User</fullName>
        <field>Assigned_Page_User__c</field>
        <formula>$User.FirstName &amp; &apos; &apos; &amp; $User.LastName</formula>
        <name>Keyword: Assigned Page User</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Keyword_Update_Unique_Name</fullName>
        <field>Name__c</field>
        <formula>Name</formula>
        <name>Keyword: Update Unique Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Keyword%3A Assign Date</fullName>
        <actions>
            <name>Keyword_Assign_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>(ISBLANK(PRIORVALUE(Page__c)) || ISNEW() || PRIORVALUE(Page__c)=&apos;a0Gb0000005y7D3&apos;)
&amp;&amp;
NOT(ISBLANK(Page__c))
&amp;&amp;
Page__c&lt;&gt;&apos;a0Gb0000005y7D3&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Keyword%3A Assign Owner</fullName>
        <actions>
            <name>Keyword_Assigned_Page_User</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>(ISBLANK(PRIORVALUE(Page__c)) || ISNEW() || PRIORVALUE(Page__c)=&apos;a0Gb0000005y7D3&apos;) 
&amp;&amp; 
NOT(ISBLANK(Page__c)) 
&amp;&amp; 
Page__c&lt;&gt;&apos;a0Gb0000005y7D3&apos;
&amp;&amp;
$User.LastName&lt;&gt;&apos;Juszczyk&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Keyword%3A Tracking Organic Position</fullName>
        <active>false</active>
        <formula>ISCHANGED(Organic_Average_Position__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Keyword%3A Unique Name</fullName>
        <actions>
            <name>Keyword_Update_Unique_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Name) || ISNEW()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
