<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Category_Keys</fullName>
        <field>Category_Keys__c</field>
        <formula>CASE(Type__c,&apos;Dom&apos;,&apos;[1][112]&apos;&amp;IF(Usable_Area__c&gt;=250,&apos;[92]&apos;,&apos;&apos;)&amp;IF(Usable_Area__c&gt;=200&amp;&amp;Footprint__c&lt;=250,&apos;[91]&apos;,&apos;&apos;),&apos;Garaż&apos;,&apos;[2]&apos;,&apos;Budynek gospodarczy&apos;,&apos;[10]&apos;,&apos;Agroturystyka&apos;,&apos;[24][21][25]&apos;,&apos;Altana&apos;,&apos;[26][30][110]&apos;,&apos;Basen&apos;,&apos;[27][110]&apos;,&apos;Budynek biurowy&apos;,&apos;[103][25][116]&apos;,&apos;Budynek inwentarski&apos;,&apos;[108][25][30]&apos;,&apos;Budynek wielorodzinny&apos;,&apos;[1][75][25]&apos;,&apos;Chlewnia&apos;,&apos;[34][21][108]&apos;,&apos;Dom letniskowy&apos;,&apos;[101][30][110]&apos;,&apos;Dom opieki&apos;,&apos;[53][25][116]&apos;,&apos;Dom weselny&apos;,&apos;[87][25]&apos;,&apos;Grill / wędzarnia&apos;,&apos;[29][30][110]&apos;,&apos;Hotel&apos;,&apos;[23][25]&apos;,&apos;Kamienica&apos;,&apos;[100][1][75][25]&apos;,&apos;Kuchnia letnia&apos;,&apos;[33][30][110]&apos;,&apos;Kurnik&apos;,&apos;[16][21][108]&apos;,&apos;Lonżownia&apos;,&apos;[19][21]&apos;,&apos;Magazyn&apos;,&apos;[88][21][25]&apos;,&apos;Obora&apos;,&apos;[18][21][108]&apos;,&apos;Ogrodzenie&apos;,&apos;[37][30]&apos;,&apos;Pensjonat&apos;,&apos;[22][25]&apos;,&apos;Projekt socjalny&apos;,&apos;[32][25]&apos;,&apos;Przedszkole&apos;,&apos;[54][25][116]&apos;,&apos;Przychodnia&apos;,&apos;[58][25][116]&apos;,&apos;Sauna&apos;,&apos;[28][30][25]&apos;,&apos;Sklep&apos;,&apos;[86][25][116]&apos;,&apos;Stajnia&apos;,&apos;[15][21][108]&apos;,&apos;Stodoła&apos;,&apos;[14][21][108]&apos;,&apos;Szambo&apos;,&apos;[31][30]&apos;,&apos;Szklarnia&apos;,&apos;[109][21][25][30]&apos;,&apos;Tuczarnia&apos;,&apos;[17][21][108]&apos;,&apos;Ujeżdżalnia&apos;,&apos;[20][21]&apos;,&apos;Warsztat&apos;,&apos;[85][25][116]&apos;,&apos;Wiata garażowa&apos;,&apos;[11][30]&apos;,&apos;Wiata magazynowa&apos;,&apos;[89][25]&apos;,&apos;Restauracja&apos;,&apos;[93][25][116]&apos;,&apos;Zbiornik na gnojowicę&apos;,&apos;[36][21]&apos;,&apos;Toaleta publiczna&apos;,&apos;[117][25][116]&apos;,&apos;Budynek usługowy&apos;,&apos;[116][25]&apos;,&apos;&apos;)&amp;
CASE(Secondary_Type__c,&apos;Dom&apos;,&apos;[1][112]&apos;,&apos;Garaż&apos;,&apos;[2]&apos;,&apos;Budynek gospodarczy&apos;,&apos;[10]&apos;,&apos;Agroturystyka&apos;,&apos;[24][21][25]&apos;,&apos;Altana&apos;,&apos;[26][30]&apos;,&apos;Budynek biurowy&apos;,&apos;[103][25][116]&apos;,&apos;Dom letniskowy&apos;,&apos;[101][30]&apos;,&apos;Dom weselny&apos;,&apos;[87][25]&apos;,&apos;Grill / wędzarnia&apos;,&apos;[29][30]&apos;,&apos;Hotel&apos;,&apos;[23][25]&apos;,&apos;Magazyn&apos;,&apos;[88][21][25]&apos;,&apos;Pensjonat&apos;,&apos;[22][25]&apos;,&apos;Projekt socjalny&apos;,&apos;[32][25]&apos;,&apos;Sklep&apos;,&apos;[86][25][116]&apos;,&apos;Warsztat&apos;,&apos;[85][25][116]&apos;,&apos;Wiata garażowa&apos;,&apos;[11][30]&apos;,&apos;Wiata magazynowa&apos;,&apos;[89][25]&apos;,&apos;Restauracja&apos;,&apos;[93][25][116]&apos;,&apos;Budynek usługowy&apos;,&apos;[116][25]&apos;,&apos;&apos;)&amp;CASE(Roof__c,&apos;czterospadowy&apos;,&apos;[5][6]&apos;,&apos;dwuspadowy&apos;,&apos;[4]&apos;,&apos;jednospadowy&apos;,&apos;[13]&apos;,&apos;kopertowy&apos;,&apos;[7][5][6]&apos;,&apos;mansardowy&apos;,&apos;[3]&apos;,
&apos;naczółkowy&apos;,&apos;[9]&apos;,&apos;namiotowy&apos;,&apos;[8][5][6]&apos;,&apos;płaski&apos;,&apos;[12]&apos;,&apos;wielospadowy&apos;,&apos;[6]&apos;,
&apos;&apos;)&amp;CASE(Living_Type__c,&apos;bliźniacza&apos;,&apos;[42]&apos;,&apos;szeregowa&apos;,&apos;[43]&apos;,&apos;szeregowa / bliźniacza&apos;,&apos;[42][43]&apos;,&apos;wolnostojąca&apos;,&apos;[49]&apos;&amp;CASE(Kitchen__c,2,&apos;[113]&apos;,&apos;&apos;),&apos;&apos;)&amp;CASE(Oriel__c,&apos;tak&apos;,&apos;[64]&apos;,&apos;nie&apos;,&apos;[74]&apos;,&apos;&apos;)&amp;CASE(South_Entrance__c,&apos;tak&apos;,&apos;[45]&apos;,&apos;nie&apos;,&apos;[59]&apos;,&apos;&apos;)&amp;
CASE(Mezzanine__c,&apos;tak&apos;,&apos;[94]&apos;,&apos;nie&apos;,&apos;[60]&apos;,&apos;&apos;)&amp;CASE(Veranda__c,&apos;tak&apos;,&apos;[68]&apos;,&apos;nie&apos;,&apos;[51]&apos;,&apos;&apos;)&amp;
CASE(Terrace__c,&apos;tak&apos;,&apos;[69]&apos;,&apos;nie&apos;,&apos;[55]&apos;,&apos;&apos;)&amp;CASE(Pool__c,&apos;tak&apos;,&apos;[71]&apos;,&apos;nie&apos;,&apos;[35]&apos;,&apos;&apos;)&amp;CASE(Winter_Garden__c,&apos;tak&apos;,&apos;[76]&apos;,&apos;nie&apos;,&apos;[107]&apos;,&apos;&apos;)&amp;CASE(Style__c,&apos;dworkowy&apos;,&apos;[90]&apos;,&apos;góralski&apos;,&apos;[98]&apos;,&apos;nowoczesny&apos;,&apos;[81]&apos;,&apos;tradycyjny&apos;,&apos;[77]&apos;,&apos;mur pruski / szachulcowy&apos;,&apos;[104]&apos;,&apos;&apos;)&amp;CASE(Construction__c,&apos;murowana&apos;,&apos;[82]&apos;,&apos;szkieletowa&apos;,&apos;[41][38]&apos;,&apos;z bali&apos;,&apos;[39][38]&apos;,&apos;blaszana&apos;,&apos;[40]&apos;,&apos;stalowa&apos;,&apos;[125]&apos;,&apos;&apos;)&amp;CASE(Shape__c,&apos;litera L&apos;,&apos;[44]&apos;,&apos;z atrium&apos;,&apos;[56]&apos;,&apos;nietypowy&apos;,&apos;[57]&apos;,&apos;&apos;)&amp;
CASE(Garage_Location__c,&apos;w bryle budynku&apos;,&apos;[79][66]&apos;,&apos;wysunięty do przodu&apos;,&apos;[79][67]&apos;,&apos;w linii budynku&apos;,&apos;[79][70]&apos;,&apos;cofnięty&apos;,&apos;[79][73]&apos;,&apos;wolnostojący&apos;,&apos;[79][78]&apos;,&apos;brak&apos;,&apos;[65]&apos;,&apos;&apos;)&amp;CASE(Carport__c,0,&apos;[62]&apos;,&apos;[80]&apos;)&amp;CASE(Garage__c,0,&apos;[65]&apos;,1,&apos;[79][50]&apos;,2,&apos;[79][52]&apos;,&apos;[79][111]&apos;)&amp;IF(Count_Rooms__c&gt;Count_Rooms_Ground_Floor__c&amp;&amp;Count_Rooms_Ground_Floor__c&gt;0,&apos;[119]&apos;,&apos;&apos;)&amp;CASE(Basement__c,1,&apos;[46][115]&apos;,2,&apos;[46][114]&apos;,&apos;[72]&apos;)&amp;CASE(Level__c,1,IF(Attic__c==1,&apos;[48]&apos;,&apos;[47]&apos;&amp;IF(Adaptable_Attic__c==1,&apos;[127]&apos;,&apos;&apos;)),2,&apos;[63]&apos;,3,&apos;[63][75]&apos;,&apos;&apos;)&amp;CASE(Ceiling__c,&apos;drewniany&apos;,&apos;[95]&apos;,&apos;gęstożebrowy&apos;,&apos;[96]&apos;,&apos;żelbetowy&apos;,&apos;[97]&apos;,&apos;wiązary&apos;,&apos;[99]&apos;,&apos;&apos;)&amp;IF(EUco__c&lt;=40, IF(Is_Passive__c,&apos;[83][84]&apos;,&apos;[83]&apos;),&apos;&apos;)&amp;IF(Construction_Pricing_Estimate__c&lt;=150000,&apos;[102]&apos;,&apos;&apos;)&amp;IF(CONTAINS(Name,&apos;ezydencja&apos;),&apos;[92]&apos;,&apos;&apos;)&amp;IF(CONTAINS(Name,&apos;Willa&apos;),&apos;[91]&apos;,&apos;&apos;)</formula>
        <name>Category Keys</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Description_Inc_to_Prev</fullName>
        <field>Description_Preview_inc__c</field>
        <formula>IF(ISBLANK(Description_inc__c),&quot;&quot;,LEFT(TRIM(Description_inc__c),255))</formula>
        <name>Description Inc to Prev</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Automated_Description_Len</fullName>
        <field>Automated_Description_Len__c</field>
        <formula>LEN(Automated_Description__c)</formula>
        <name>Design: Automated Description Len</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Cancel_Gross_Price_inc</fullName>
        <field>Gross_Price_inc__c</field>
        <name>Design: Cancel Gross Price (inc)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Cancel_Pending_Gross_Price</fullName>
        <field>Pending_Gross_Price__c</field>
        <name>Design: Cancel Pending Gross Price</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Cancel_Pending_Gross_Start_Date</fullName>
        <field>Pending_Gross_Price_Start_Date__c</field>
        <name>Design: Cancel Pending Gross Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Cancel_Pending_Promo_End_Date</fullName>
        <field>Pending_Promo_End_Date__c</field>
        <name>Design: Cancel Pending Promo End Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Cancel_Pending_Promo_Name</fullName>
        <field>Pending_Promo_Name__c</field>
        <name>Design: Cancel Pending Promo Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Cancel_Pending_Promo_Price</fullName>
        <field>Pending_Promo_Price__c</field>
        <name>Design: Cancel Pending Promo Price</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Cancel_Pending_Promo_Start_Date</fullName>
        <field>Pending_Promo_Start_Date__c</field>
        <name>Design: Cancel Pending Promo Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Cancel_Promo_Name</fullName>
        <field>Promo_Name__c</field>
        <name>Design: Cancel Promo Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Cancel_Promo_Price</fullName>
        <field>Promo_Price__c</field>
        <name>Design: Cancel Promo Price</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Cancel_Promo_Price_End_Date</fullName>
        <field>Promo_Price_End_Date__c</field>
        <name>Design: Cancel Promo Price End Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Canonical_URL</fullName>
        <field>Canonical_URL__c</field>
        <formula>IF(ISBLANK(Code__c),null,&apos;/projekt-&apos; &amp;
CASE(Type__c,
&apos;Dom&apos;,&apos;domu-&apos;,
&apos;Garaż&apos;,&apos;garazu-&apos;,
&apos;Budynek gospodarczy&apos;,&apos;budynku-gospodarczego-&apos;,
&apos;Agroturystyka&apos;,&apos;agroturystyczny-&apos;,
&apos;Altana&apos;,&apos;altany-&apos;,
&apos;Basen&apos;,&apos;basenu-&apos;,
&apos;Budynek biurowy&apos;,&apos;budynku-biurowego-&apos;,
&apos;Budynek inwentarski&apos;,&apos;budynku-inwentarskiego-&apos;,
&apos;Budynek wielorodzinny&apos;,&apos;budynku-wielorodzinnego-&apos;,
&apos;Chlewnia&apos;,&apos;chlewni-&apos;,
&apos;Dom letniskowy&apos;,&apos;domu-letniskowego-&apos;,
&apos;Dom opieki&apos;,&apos;domu-opieki-&apos;,
&apos;Dom weselny&apos;,&apos;domu-weselnego-&apos;,
&apos;Grill / wędzarnia&apos;,&apos;grilla-&apos;,
&apos;Hotel&apos;,&apos;hotelu-&apos;,
&apos;Kamienica&apos;,&apos;kamienicy-&apos;,
&apos;Kuchnia letnia&apos;,&apos;kuchni-letniej-&apos;,
&apos;Kurnik&apos;,&apos;kurnika-&apos;,
&apos;Lonżownia&apos;,&apos;lonzowni-&apos;,
&apos;Magazyn&apos;,&apos;magazynu-&apos;,
&apos;Obora&apos;,&apos;obory-&apos;,
&apos;Ogrodzenie&apos;,&apos;ogrodzenia-&apos;,
&apos;Pensjonat&apos;,&apos;pensjonatu-&apos;,
&apos;Projekt socjalny&apos;,&apos;socjalny-&apos;,
&apos;Przedszkole&apos;,&apos;przedszkola-&apos;,
&apos;Przychodnia&apos;,&apos;przychodni-&apos;,
&apos;Sauna&apos;,&apos;sauny-&apos;,
&apos;Sklep&apos;,&apos;sklepu-&apos;,
&apos;Stajnia&apos;,&apos;stajni-&apos;,
&apos;Stodoła&apos;,&apos;stodoly-&apos;,
&apos;Szambo&apos;,&apos;szamba-&apos;,
&apos;Szklarnia&apos;,&apos;szklarni-&apos;,
&apos;Tuczarnia&apos;,&apos;tuczarni-&apos;,
&apos;Ujeżdżalnia&apos;,&apos;ujezdzalni-&apos;,
&apos;Warsztat&apos;,&apos;warsztatu-&apos;,
&apos;Wiata garażowa&apos;,&apos;wiaty-garazowej-&apos;,
&apos;Wiata magazynowa&apos;,&apos;wiaty-magazynowej-&apos;,
&apos;Restauracja&apos;,&apos;restauracji-&apos;,
&apos;Zbiornik na gnojowicę&apos;,&apos;zbiornika-na-gnojowice-&apos;,
&apos;Toaleta publiczna&apos;,&apos;toalety-publicznej-&apos;,
&apos;Budynek usługowy&apos;,&apos;budynku-uslugowego-&apos;,&apos;&apos;)
&amp; Slug__c)</formula>
        <name>Design: Canonical URL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Custom_Description_Len</fullName>
        <field>Custom_Description_Len__c</field>
        <formula>LEN(Custom_Description__c)</formula>
        <name>Design: Custom Description Len</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Description_Len</fullName>
        <field>Description_Len__c</field>
        <formula>LEN(Description__c)</formula>
        <name>Design: Description Len</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Factors_By</fullName>
        <field>Design_Factors_By__c</field>
        <formula>$User.FirstName &amp; &apos; &apos; &amp; $User.LastName</formula>
        <name>Design: Factors By</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Factors_Date</fullName>
        <field>Design_Factors_Date__c</field>
        <formula>NOW()</formula>
        <name>Design: Factors Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Factors_Plans_By</fullName>
        <field>Design_Factors_Plans_By__c</field>
        <formula>$User.FirstName &amp; &apos; &apos; &amp; $User.LastName</formula>
        <name>Design: Factors Plans By</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Factors_Plans_Date</fullName>
        <field>Design_Factors_Plans_Date__c</field>
        <formula>NOW()</formula>
        <name>Design: Factors Plans Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Generate_Design_Version_Filter</fullName>
        <field>Design_Version_Filter_Code__c</field>
        <formula>Design_Versions_Page__r.Filter_Code__c</formula>
        <name>Design: Generate Design Version Filter</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Generate_Order_Score</fullName>
        <field>Order_Score__c</field>
        <formula>Page_Score_Unique__c</formula>
        <name>Design: Generate Order Score</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Generate_Valid_Name</fullName>
        <field>Name</field>
        <formula>Left(Full_Name__c, 72)+&apos; &apos;+Code__c</formula>
        <name>Design: Generate Valid Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Generate_Valid_Name_ID</fullName>
        <field>Design_Name_ID__c</field>
        <formula>lower(Supplier__r.Code__c+ &apos;: &apos; + Design_Name_ID_Postfix__c)</formula>
        <name>Design: Generate Valid Name ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Groups_DKn_Update_DKn_Answer</fullName>
        <field>Groups_Don_t_know_Groups__c</field>
        <formula>SUBSTITUTE(Groups_Don_t_know_Groups__c, &apos;[&apos;+Group_Answer_Don_t_know__c+&apos;]&apos;, &apos;&apos;) + &apos;[&apos;+Group_Answer_Don_t_know__c+&apos;]&apos;</formula>
        <name>Design: Groups: DKn Update (DKn Answer)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Groups_DKn_Update_No_Answer</fullName>
        <field>Groups_Don_t_know_Groups__c</field>
        <formula>SUBSTITUTE(Groups_Don_t_know_Groups__c, &apos;[&apos;+Group_Answer_No__c+&apos;]&apos;, &apos;&apos;)</formula>
        <name>Design: Groups: DKn Update (No Answer)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Groups_DKn_Update_Yes_Answer</fullName>
        <field>Groups_Don_t_know_Groups__c</field>
        <formula>SUBSTITUTE(Groups_Don_t_know_Groups__c, &apos;[&apos;+Group_Answer_Yes__c+&apos;]&apos;, &apos;&apos;)</formula>
        <name>Design: Groups: DKn Update (Yes Answer)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Groups_No_Update_DKn_Answer</fullName>
        <field>Groups_No_Groups__c</field>
        <formula>SUBSTITUTE(Groups_No_Groups__c, &apos;[&apos;+Group_Answer_Don_t_know__c+&apos;]&apos;, &apos;&apos;)</formula>
        <name>Design: Groups: No Update (DKn Answer)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Groups_No_Update_No_Answer</fullName>
        <field>Groups_No_Groups__c</field>
        <formula>SUBSTITUTE(Groups_No_Groups__c, &apos;[&apos;+Group_Answer_No__c+&apos;]&apos;, &apos;&apos;) + &apos;[&apos;+Group_Answer_No__c+&apos;]&apos;</formula>
        <name>Design: Groups: No Update (No Answer)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Groups_No_Update_Yes_Answer</fullName>
        <field>Groups_No_Groups__c</field>
        <formula>SUBSTITUTE(Groups_No_Groups__c, &apos;[&apos;+Group_Answer_Yes__c+&apos;]&apos;, &apos;&apos;)</formula>
        <name>Design: Groups: No Update (Yes Answer)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Groups_Yes_Update_DKn_Answer</fullName>
        <field>Groups_Yes_Groups__c</field>
        <formula>SUBSTITUTE(Groups_Yes_Groups__c, &apos;[&apos;+Group_Answer_Don_t_know__c+&apos;]&apos;, &apos;&apos;)</formula>
        <name>Design: Groups: Yes Update (DKn Answer)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Groups_Yes_Update_No_Answer</fullName>
        <field>Groups_Yes_Groups__c</field>
        <formula>SUBSTITUTE(Groups_Yes_Groups__c, &apos;[&apos;+Group_Answer_No__c+&apos;]&apos;, &apos;&apos;)</formula>
        <name>Design: Groups: Yes Update (No Answer)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Groups_Yes_Update_Yes_Answer</fullName>
        <field>Groups_Yes_Groups__c</field>
        <formula>SUBSTITUTE(Groups_Yes_Groups__c, &apos;[&apos;+Group_Answer_Yes__c+&apos;]&apos;, &apos;&apos;) + &apos;[&apos;+Group_Answer_Yes__c+&apos;]&apos;</formula>
        <name>Design: Groups: Yes Update (Yes Answer)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Set_Activation_Date</fullName>
        <field>Activation_Date__c</field>
        <formula>NOW()</formula>
        <name>Design: Set Activation Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Set_Gross_Price_Scheduled</fullName>
        <field>Gross_Price__c</field>
        <formula>Pending_Gross_Price__c</formula>
        <name>Design: Set Gross Price Scheduled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Set_Pending_Featured</fullName>
        <field>Featured__c</field>
        <formula>Pending_Featured__c</formula>
        <name>Design: Set Pending Featured</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Set_Promo_End_Date_Scheduled</fullName>
        <field>Promo_Price_End_Date__c</field>
        <formula>Pending_Promo_End_Date__c</formula>
        <name>Design: Set Promo End Date Scheduled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Set_Promo_Name_Scheduled</fullName>
        <field>Promo_Name__c</field>
        <formula>Pending_Promo_Name__c</formula>
        <name>Design: Set Promo Name Scheduled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Set_Promo_Price_Scheduled</fullName>
        <field>Promo_Price__c</field>
        <formula>Pending_Promo_Price__c</formula>
        <name>Design: Set Promo Price Scheduled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Sizes_Change_Date</fullName>
        <field>Sizes_Change_Date__c</field>
        <formula>NOW()</formula>
        <name>Design: Sizes Change Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Sketchfab_ID_Date</fullName>
        <field>Sketchfab_ID_Date__c</field>
        <formula>TODAY()</formula>
        <name>Design: Sketchfab ID Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Track_Category_Data_Changes</fullName>
        <field>Category_Data_Changes__c</field>
        <formula>LEFT(IF(ISCHANGED(Type__c), &apos;/typ:&apos; &amp; TEXT(Type__c), &apos;&apos;) 
&amp; IF(ISCHANGED(Secondary_Type__c), &apos;/typ2:&apos; &amp; TEXT(Secondary_Type__c), &apos;&apos;) 
&amp; IF(ISCHANGED(Living_Type__c), &apos;/zab.:&apos; &amp; TEXT(Living_Type__c), &apos;&apos;) 
&amp; IF(ISCHANGED(Scarp__c), &apos;/skarpa:&apos; &amp; TEXT(Scarp__c), &apos;&apos;)
&amp; IF(ISCHANGED(Oriel__c), &apos;/wykusz:&apos; &amp; TEXT(Oriel__c), &apos;&apos;)
&amp; IF(ISCHANGED(Mezzanine__c), &apos;/antresola:&apos; &amp; TEXT(Mezzanine__c), &apos;&apos;)
&amp; IF(ISCHANGED(Veranda__c), &apos;/weranda:&apos; &amp; TEXT(Veranda__c), &apos;&apos;)
&amp; IF(ISCHANGED(Terrace__c), &apos;/taras:&apos; &amp; TEXT(Terrace__c), &apos;&apos;)
&amp; IF(ISCHANGED(Pool__c), &apos;/basen:&apos; &amp; TEXT(Pool__c), &apos;&apos;)
&amp; IF(ISCHANGED(Winter_Garden__c), &apos;/ogrodzim:&apos; &amp; TEXT(Winter_Garden__c), &apos;&apos;)
&amp; IF(ISCHANGED(Fireplace__c), &apos;/kominek:&apos; &amp; TEXT(Fireplace__c), &apos;&apos;)
&amp; IF(ISCHANGED(Construction__c), &apos;/techn:&apos; &amp; TEXT(Construction__c), &apos;&apos;)
&amp; IF(ISCHANGED(Is_Passive__c), &apos;/pas:&apos; &amp; IF(Is_Passive__c,&apos;tak&apos;,&apos;nie&apos;), &apos;&apos;)
&amp; IF(ISCHANGED(Roof__c), &apos;/dach:&apos; &amp; TEXT(Roof__c), &apos;&apos;)
&amp; IF(ISCHANGED(Eaves__c), &apos;/okap:&apos; &amp; TEXT(Eaves__c), &apos;&apos;)
&amp; IF(ISCHANGED(Bullseye__c), &apos;/woleoko:&apos; &amp; TEXT(Bullseye__c), &apos;&apos;)
&amp; IF(ISCHANGED(Ridge__c), &apos;/kalenica:&apos; &amp; TEXT(Ridge__c), &apos;&apos;)
&amp; IF(ISCHANGED(Style__c), &apos;/styl:&apos; &amp; TEXT(Style__c), &apos;&apos;)
&amp; IF(ISCHANGED(Shape__c), &apos;/bryla:&apos; &amp; TEXT(Shape__c), &apos;&apos;)
&amp; IF(ISCHANGED(Border_Ready__c), &apos;/nagranicy:&apos; &amp; TEXT(Border_Ready__c), &apos;&apos;)
&amp; IF(ISCHANGED(South_Entrance__c), &apos;/wejsciepld:&apos; &amp; TEXT(South_Entrance__c), &apos;&apos;)
&amp; IF(ISCHANGED(Garage_Location__c), &apos;/usytgar:&apos; &amp; TEXT(Garage_Location__c), &apos;&apos;),255)</formula>
        <name>Design: Track Category Data Changes</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Track_Previous_Slug</fullName>
        <field>Previous_Slug__c</field>
        <formula>PRIORVALUE(Slug__c)</formula>
        <name>Design: Track Previous Slug</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Track_Primary_Data_Changes</fullName>
        <field>Primary_Data_Changes__c</field>
        <formula>IF(ISCHANGED(Footprint__c), &apos;/ pow. zabudowy: &apos; &amp; TEXT(Footprint__c), &apos;&apos;)
&amp; IF(ISCHANGED(Usable_Area__c), &apos;/ pow. użytkowa: &apos; &amp; TEXT(Usable_Area__c), &apos;&apos;)
&amp; IF(ISCHANGED(Height__c), &apos;/ wys: &apos; &amp; TEXT(Height__c), &apos;&apos;)
&amp; IF(ISCHANGED(Capacity__c), &apos;/ kubatura: &apos; &amp; TEXT(Capacity__c), &apos;&apos;)
&amp; IF(ISCHANGED(Roof_slope__c), &apos;/ kąt dachu: &apos; &amp; TEXT(Roof_slope__c), &apos;&apos;)
&amp; IF(ISCHANGED(Minimum_Plot_Size_Horizontal__c), &apos;/ szer. działki: &apos; &amp; TEXT(Minimum_Plot_Size_Horizontal__c), &apos;&apos;)
&amp; IF(ISCHANGED(Minimum_Plot_Size_Vertical__c), &apos;/ dł. działki: &apos; &amp; TEXT(Minimum_Plot_Size_Vertical__c), &apos;&apos;)
&amp; IF(ISCHANGED(Construction_Pricing_Estimate__c), &apos;/ koszt budowy: &apos; &amp; TEXT(Construction_Pricing_Estimate__c), &apos;&apos;)
&amp; IF(ISCHANGED(Roof_Cover_Area__c), &apos;/ pow dachu: &apos; &amp; TEXT(Roof_Cover_Area__c), &apos;&apos;)
&amp; IF(ISCHANGED(EUco__c), &apos;/ euco: &apos; &amp; TEXT(EUco__c), &apos;&apos;)</formula>
        <name>Design: Track Primary Data Changes</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Update_Data_Changes_Date</fullName>
        <field>Last_Modified_Data__c</field>
        <formula>NOW()</formula>
        <name>Design: Update Data Changes Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Update_Date</fullName>
        <field>Update_Date__c</field>
        <formula>NOW()</formula>
        <name>Design: Update Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Design_Usable_Area_Changed</fullName>
        <field>Last_Usable_Area_Change__c</field>
        <formula>now()</formula>
        <name>Design: Usable Area Changed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Technology_Inc_to_Prev</fullName>
        <field>Technology_Preview_inc__c</field>
        <formula>IF(ISBLANK(Technology_inc__c),&quot;&quot;,LEFT(TRIM(Technology_inc__c),255))</formula>
        <name>Technology Inc to Prev</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Design%3A Description Inc Update</fullName>
        <actions>
            <name>Description_Inc_to_Prev</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Description_inc__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Design%3A Factors Completed</fullName>
        <actions>
            <name>Design_Factors_By</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Factors_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Design__c.Factors_Completed__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Design%3A Factors Plans Completed</fullName>
        <actions>
            <name>Design_Factors_Plans_By</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Factors_Plans_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Design__c.Factors_Completed_Plans__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Design%3A Generate Category Keys</fullName>
        <actions>
            <name>Category_Keys</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>True</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Design%3A Group%3A Answer Don%27t know changed</fullName>
        <actions>
            <name>Design_Groups_DKn_Update_DKn_Answer</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Groups_No_Update_DKn_Answer</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Groups_Yes_Update_DKn_Answer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Group_Answer_Don_t_know__c) &amp;&amp; ISNUMBER(Group_Answer_Don_t_know__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Design%3A Group%3A Answer No changed</fullName>
        <actions>
            <name>Design_Groups_DKn_Update_No_Answer</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Groups_No_Update_No_Answer</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Groups_Yes_Update_No_Answer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Group_Answer_No__c) &amp;&amp; ISNUMBER(Group_Answer_No__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Design%3A Group%3A Answer Yes changed</fullName>
        <actions>
            <name>Design_Groups_DKn_Update_Yes_Answer</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Groups_No_Update_Yes_Answer</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Groups_Yes_Update_Yes_Answer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Group_Answer_Yes__c) &amp;&amp; ISNUMBER(Group_Answer_Yes__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Design%3A On Activation</fullName>
        <actions>
            <name>Design_Canonical_URL</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Set_Activation_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Status__c) &amp;&amp; ISPICKVAL(Status__c, &apos;Active&apos;) &amp;&amp; ISBLANK(Activation_Date__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Design%3A On Create</fullName>
        <active>false</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Design%3A On Create%2C On Update</fullName>
        <actions>
            <name>Design_Automated_Description_Len</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Custom_Description_Len</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Description_Len</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Generate_Design_Version_Filter</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Generate_Valid_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Generate_Valid_Name_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Design%3A On Featured Pending which differ</fullName>
        <active>true</active>
        <formula>BLANKVALUE(Featured__c,0) &lt;&gt; BLANKVALUE(Pending_Featured__c,0)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Design_Set_Pending_Featured</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Design__c.Next_Month_1_Day__c</offsetFromField>
            <timeLength>-25</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Design%3A On Gross Price Start Date Schedule</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Design__c.Pending_Gross_Price_Start_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Design_Cancel_Gross_Price_inc</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Design_Cancel_Pending_Gross_Start_Date</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Design_Set_Gross_Price_Scheduled</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Design_Update_Date</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Design__c.Pending_Gross_Price_Start_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Design%3A On Pending Gross Price Start Date Erase</fullName>
        <actions>
            <name>Design_Cancel_Pending_Gross_Price</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Pending_Gross_Price_Start_Date__c) &amp;&amp; ISBLANK(Pending_Gross_Price_Start_Date__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Design%3A On Pending Promo Start Date Erase</fullName>
        <actions>
            <name>Design_Cancel_Pending_Promo_End_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Cancel_Pending_Promo_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Cancel_Pending_Promo_Price</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Pending_Promo_Start_Date__c) &amp;&amp; ISBLANK(Pending_Promo_Start_Date__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Design%3A On Promo Price End Date Set</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Design__c.Promo_Price_End_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Design_Cancel_Promo_Name</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Design_Cancel_Promo_Price</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Design_Cancel_Promo_Price_End_Date</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Design_Update_Date</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Design__c.Promo_Price_End_Date_1_Day__c</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Design%3A On Promo Price Start Date Schedule</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Design__c.Pending_Promo_Start_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Design_Cancel_Pending_Promo_Start_Date</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Design_Set_Promo_End_Date_Scheduled</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Design_Set_Promo_Name_Scheduled</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Design_Set_Promo_Price_Scheduled</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Design_Update_Date</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Design__c.Pending_Promo_Start_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Design%3A On Sketchfab ID Set</fullName>
        <actions>
            <name>Design_Sketchfab_ID_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Design__c.Sketchfab_ID__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Design%3A On Slug changed</fullName>
        <actions>
            <name>Design_Track_Previous_Slug</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>PRIORVALUE(Slug__c) &lt;&gt; Slug__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Design%3A On Usable Area Change</fullName>
        <actions>
            <name>Design_Usable_Area_Changed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Usable_Area__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Design%3A Order Score</fullName>
        <actions>
            <name>Design_Generate_Order_Score</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Design%3A Sizes Change</fullName>
        <actions>
            <name>Design_Sizes_Change_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Usable_Area__c) || ISCHANGED(Height__c) || ISCHANGED(Minimum_Plot_Size_Horizontal__c) || ISCHANGED(Roof_Cover_Area__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Design%3A Technology Inc Update</fullName>
        <actions>
            <name>Technology_Inc_to_Prev</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Technology_inc__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Design%3A Tracking Category Data Changes</fullName>
        <actions>
            <name>Design_Track_Category_Data_Changes</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Update_Data_Changes_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>(ISCHANGED(Type__c) || ISCHANGED(Secondary_Type__c) || ISCHANGED(Living_Type__c) || ISCHANGED(Scarp__c) || ISCHANGED(Oriel__c) ||  ISCHANGED(Mezzanine__c) ||  ISCHANGED(Veranda__c) || ISCHANGED(Terrace__c) || ISCHANGED(Pool__c) || ISCHANGED(Winter_Garden__c) || ISCHANGED(Fireplace__c) || ISCHANGED(Construction__c) || ISCHANGED(Is_Passive__c) || ISCHANGED(Roof__c) || ISCHANGED(Eaves__c) || ISCHANGED(Bullseye__c) || ISCHANGED(Ridge__c) || ISCHANGED(Style__c) || ISCHANGED(Shape__c) || ISCHANGED(Border_Ready__c) || ISCHANGED(South_Entrance__c) || ISCHANGED(Garage_Location__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Design%3A Tracking Primary Data Changes</fullName>
        <actions>
            <name>Design_Track_Primary_Data_Changes</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Design_Update_Data_Changes_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>(ISCHANGED(Footprint__c) || ISCHANGED(Usable_Area__c) || ISCHANGED(Height__c) ||  ISCHANGED(Capacity__c) || ISCHANGED(Roof_slope__c) ||  ISCHANGED(Minimum_Plot_Size_Horizontal__c) ||  ISCHANGED(Minimum_Plot_Size_Vertical__c) || ISCHANGED(Construction_Pricing_Estimate__c) || ISCHANGED(Roof_Cover_Area__c) || ISCHANGED(EUco__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Design%3A Update Date</fullName>
        <actions>
            <name>Design_Update_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT(ISCHANGED(Last_Seen_Bot__c)) &amp;&amp; LastModifiedBy.LastName != &apos;Juszczyk&apos; &amp;&amp; LastModifiedBy.LastName != &apos;Łukjański&apos; &amp;&amp; LastModifiedBy.LastName != &apos;Sadowski&apos; &amp;&amp; NOT(ISNEW())</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
