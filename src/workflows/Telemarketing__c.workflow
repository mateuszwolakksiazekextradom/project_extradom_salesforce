<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Budowa_domu_SMS_Informacja_dla_Partnera_o_przekazaniu_danych_Klienta</fullName>
        <description>Budowa domu: SMS: Informacja dla Partnera o przekazaniu danych Klienta</description>
        <protected>false</protected>
        <recipients>
            <recipient>mariusz.juszczyk+smsapi@extradom.pl</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mariusz.juszczyk@extradom.pl</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Budowa_domu_SMS_Informacja_dla_Partnera_o_przekazaniu_danych_Klienta</template>
    </alerts>
    <alerts>
        <fullName>Email_Informacja_dla_Partnera_o_przekazaniu_danych_Klienta</fullName>
        <ccEmails>marta.miecznikowska@extradom.pl</ccEmails>
        <description>Email: Informacja dla Partnera o przekazaniu danych Klienta</description>
        <protected>false</protected>
        <recipients>
            <field>Transfer_Account_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Informacja_dla_Partnera_o_przekazaniu</template>
    </alerts>
    <alerts>
        <fullName>Email_Informacja_dla_Partnera_o_przekazaniu_danych_Klienta_Ruukki</fullName>
        <ccEmails>marta.miecznikowska@extradom.pl</ccEmails>
        <ccEmails>michal.dyda@ruukki.com</ccEmails>
        <description>Email: Informacja dla Partnera o przekazaniu danych Klienta - Ruukki</description>
        <protected>false</protected>
        <recipients>
            <field>Transfer_Account_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Informacja_dla_Partnera_o_przekazaniu</template>
    </alerts>
    <alerts>
        <fullName>Email_Informacja_dla_Partnera_o_przekazaniu_danych_Klienta_wylewki_anhydrytowe</fullName>
        <ccEmails>marta.miecznikowska@extradom.pl</ccEmails>
        <description>Email: Informacja dla Partnera o przekazaniu danych Klienta (wylewki anhydrytowe)</description>
        <protected>false</protected>
        <recipients>
            <field>Transfer_Account_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Przekazanie_danych/Informacja_dla_Partnera_Lafarge</template>
    </alerts>
    <alerts>
        <fullName>Przekazanie_danych_bramy_gara_owe_Klient</fullName>
        <ccEmails>marta.miecznikowska@extradom.pl</ccEmails>
        <description>Przekazanie danych: bramy garażowe - Klient</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Przekazanie_danych_bramy_gara_owe_Klient</template>
    </alerts>
    <alerts>
        <fullName>Przekazanie_danych_dach_wki_ceramiczne_Klient</fullName>
        <ccEmails>marta.miecznikowska@extradom.pl</ccEmails>
        <description>Przekazanie danych: dachówki ceramiczne - Klient</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Przekazanie_danych_dach_wki_ceramic</template>
    </alerts>
    <alerts>
        <fullName>Przekazanie_danych_finansowanie_inwestycji_Klient</fullName>
        <ccEmails>marta.miecznikowska@extradom.pl</ccEmails>
        <description>Przekazanie danych: finansowanie inwestycji - Klient</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Przekazanie_danych_finansowanie_i</template>
    </alerts>
    <alerts>
        <fullName>Przekazanie_danych_instalator_rekuperacji_Klient</fullName>
        <ccEmails>marta.miecznikowska@extradom.pl</ccEmails>
        <description>Przekazanie danych: instalator rekuperacji - Klient</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Przekazanie_danych/Przekazanie_danych_instalator_reku</template>
    </alerts>
    <alerts>
        <fullName>Przekazanie_danych_izolacje_Klient</fullName>
        <ccEmails>marta.miecznikowska@extradom.pl</ccEmails>
        <description>Przekazanie danych: izolacje - Klient</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Przekazanie_danych/Przekazanie_danych_izo2</template>
    </alerts>
    <alerts>
        <fullName>Przekazanie_danych_izolacje_natryskowe_Klient</fullName>
        <ccEmails>marta.miecznikowska@extradom.pl</ccEmails>
        <description>Przekazanie danych: izolacje natryskowe - Klient</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Przekazanie_danych_izo</template>
    </alerts>
    <alerts>
        <fullName>Przekazanie_danych_materia_y_budowlane_Klient</fullName>
        <ccEmails>marta.miecznikowska@extradom.pl</ccEmails>
        <description>Przekazanie danych: materiały budowlane - Klient</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Przekazanie_danych_materia_y_budo</template>
    </alerts>
    <alerts>
        <fullName>Przekazanie_danych_schody_Klient</fullName>
        <ccEmails>marta.miecznikowska@extradom.pl</ccEmails>
        <description>Przekazanie danych: schody - Klient</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Przekazanie_danych_schody_Klient</template>
    </alerts>
    <alerts>
        <fullName>Przekazanie_danych_wylewki_anhydrytowe_Klient</fullName>
        <ccEmails>marta.miecznikowska@extradom.pl</ccEmails>
        <description>Przekazanie danych: wylewki anhydrytowe - Klient</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Przekazanie_danych/Przekazanie_danych_wylewki_anhydryto</template>
    </alerts>
    <alerts>
        <fullName>SMS_Informacja_dla_Partnera_o_przekazaniu_danych_Klienta</fullName>
        <description>SMS: Informacja dla Partnera o przekazaniu danych Klienta</description>
        <protected>false</protected>
        <recipients>
            <recipient>mariusz.juszczyk+smsapi@extradom.pl</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SMS_Informacja_dla_Partnera_o_przekazaniu_danych_Klienta</template>
    </alerts>
    <alerts>
        <fullName>SMS_Informacja_dla_Partnera_o_przekazaniu_danych_Klienta_wylewki_anhydrytowe</fullName>
        <description>SMS: Informacja dla Partnera o przekazaniu danych Klienta (wylewki anhydrytowe)</description>
        <protected>false</protected>
        <recipients>
            <recipient>mariusz.juszczyk+smsapi@extradom.pl</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mariusz.juszczyk@extradom.pl</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Przekazanie_danych/Lafarge_SMS_Informacja_dla_Partnera_o_przekazaniu_danych_Klienta</template>
    </alerts>
    <alerts>
        <fullName>Telemarketing_Arkada_Wygrany_Klient</fullName>
        <ccEmails>marta.miecznikowska@extradom.pl</ccEmails>
        <description>Telemarketing: Arkada (Wygrany) - Klient</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Przekazanie_danych/Telemarketing_Arkada_Wygrany_Klient</template>
    </alerts>
    <alerts>
        <fullName>Telemarketing_Arkada_Wygrany_Partner</fullName>
        <ccEmails>marta.miecznikowska@extradom.pl</ccEmails>
        <ccEmails>s.suchodolski@arkada.net.pl</ccEmails>
        <description>Telemarketing: Arkada (Wygrany) - Partner</description>
        <protected>false</protected>
        <recipients>
            <field>Partner_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Przekazanie_danych/Telemarketing_Arkada_Wygrany_Partner</template>
    </alerts>
    <alerts>
        <fullName>Telemarketing_Danfoss_Przegrany_Ulotka_Klient</fullName>
        <ccEmails>marta.miecznikowska@extradom.pl</ccEmails>
        <description>Telemarketing: Danfoss (Przegrany, Ulotka) - Klient</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Przekazanie_danych/Telemarketing_Danfoss_Przegrany_Ulotka_Klient</template>
    </alerts>
    <alerts>
        <fullName>Telemarketing_Danfoss_Wygrany_Klient</fullName>
        <ccEmails>marta.miecznikowska@extradom.pl</ccEmails>
        <description>Telemarketing: Danfoss (Wygrany) - Klient</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Przekazanie_danych/Telemarketing_Danfoss_Wygrany_Klient</template>
    </alerts>
    <alerts>
        <fullName>Telemarketing_Danfoss_Wygrany_Partner</fullName>
        <ccEmails>marta.miecznikowska@extradom.pl</ccEmails>
        <description>Telemarketing: Danfoss (Wygrany) - Partner</description>
        <protected>false</protected>
        <recipients>
            <field>Partner_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Przekazanie_danych/Telemarketing_Danfoss_Wygrany_Partner</template>
    </alerts>
    <alerts>
        <fullName>Telemarketing_Danfoss_Wygrany_Partner_SMS</fullName>
        <description>Telemarketing: Danfoss (Wygrany) - Partner (SMS)</description>
        <protected>false</protected>
        <recipients>
            <recipient>mariusz.juszczyk+smsapi@extradom.pl</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mariusz.juszczyk@extradom.pl</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Przekazanie_danych/Telemarketing_Danfoss_Wygrany_Partner_SMS</template>
    </alerts>
    <alerts>
        <fullName>Telemarketing_Eco_Partner_Wygrany_Klient</fullName>
        <ccEmails>marta.miecznikowska@extradom.pl</ccEmails>
        <description>Telemarketing: Eco Partner (Wygrany) - Klient</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Przekazanie_danych/Telemarketing_Eco_Partner_Wygrany_Klient</template>
    </alerts>
    <alerts>
        <fullName>Telemarketing_Eco_Partner_Wygrany_Partner</fullName>
        <ccEmails>marta.miecznikowska@extradom.pl</ccEmails>
        <description>Telemarketing: Eco Partner (Wygrany) - Partner</description>
        <protected>false</protected>
        <recipients>
            <field>Partner_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Przekazanie_danych/Telemarketing_Eco_Partner_Wygrany_Partner</template>
    </alerts>
    <alerts>
        <fullName>Telemarketing_Gaspol_Wygrany_Klient</fullName>
        <ccEmails>marta.miecznikowska@extradom.pl</ccEmails>
        <ccEmails>kjedruszak@gaspol.pl</ccEmails>
        <ccEmails>iosiak@gaspol.pl</ccEmails>
        <description>Telemarketing: Gaspol (Wygrany) - Klient</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Przekazanie_danych/Telemarketing_Gaspol_Wygrany_Klient</template>
    </alerts>
    <alerts>
        <fullName>Telemarketing_Gaspol_Wygrany_Partner</fullName>
        <ccEmails>marta.miecznikowska@extradom.pl</ccEmails>
        <ccEmails>kjedruszak@gaspol.pl</ccEmails>
        <ccEmails>iosiak@gaspol.pl</ccEmails>
        <description>Telemarketing: Gaspol (Wygrany) - Partner</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Przekazanie_danych/Telemarketing_Gaspol_Wygrany_Partner</template>
    </alerts>
    <alerts>
        <fullName>Telemarketing_Lafarge_Wygrany_Klient</fullName>
        <ccEmails>marta.miecznikowska@extradom.pl</ccEmails>
        <description>Telemarketing: Lafarge (Wygrany) - Klient</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Przekazanie_danych/Telemarketing_Lafarge_Wygrany_Klient</template>
    </alerts>
    <alerts>
        <fullName>Telemarketing_Lafarge_Wygrany_Partner</fullName>
        <ccEmails>marta.miecznikowska@extradom.pl</ccEmails>
        <description>Telemarketing: Lafarge (Wygrany) - Partner</description>
        <protected>false</protected>
        <recipients>
            <field>Partner_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Przekazanie_danych/Telemarketing_Lafarge_Wygrany_Partner</template>
    </alerts>
    <fieldUpdates>
        <fullName>Telemarketing_Braas_Meeting</fullName>
        <field>Braas_Stage__c</field>
        <literalValue>do przekazania hurtowni</literalValue>
        <name>Telemarketing: Braas: Meeting</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Telemarketing_Close_Date</fullName>
        <field>Close_Date__c</field>
        <formula>TODAY()</formula>
        <name>Telemarketing: Close Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Telemarketing_Conversion_Date</fullName>
        <field>Conversion_Date__c</field>
        <formula>TODAY()</formula>
        <name>Telemarketing: Conversion Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Telemarketing_Start_Date</fullName>
        <field>Start_Date__c</field>
        <formula>TODAY()</formula>
        <name>Telemarketing: Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Transfer_Account_Email</fullName>
        <field>Transfer_Account_Email__c</field>
        <formula>BLANKVALUE(BLANKVALUE(Transfer_Account__r.Account_Community_Member__r.Contractor_Email__c, Transfer_Account__r.Account_Community_Member__r.Business_Email__c), Transfer_Account__r.Transfer_Email__c)</formula>
        <name>Update Transfer Account Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Telemarketing%3A Arkada %28Wygrany%29</fullName>
        <actions>
            <name>Telemarketing_Arkada_Wygrany_Klient</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Telemarketing_Arkada_Wygrany_Partner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Telemarketing__c.Name</field>
            <operation>equals</operation>
            <value>Arkada</value>
        </criteriaItems>
        <criteriaItems>
            <field>Telemarketing__c.Status__c</field>
            <operation>equals</operation>
            <value>Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Telemarketing__c.Post_Conversion_Stage__c</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Telemarketing%3A Close Date</fullName>
        <actions>
            <name>Telemarketing_Close_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Telemarketing__c.Status__c</field>
            <operation>equals</operation>
            <value>Won,Lost</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Telemarketing%3A Conversion Date</fullName>
        <actions>
            <name>Telemarketing_Conversion_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Telemarketing__c.Status__c</field>
            <operation>equals</operation>
            <value>Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Telemarketing__c.Post_Conversion_Stage__c</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Telemarketing%3A Danfoss %28Przegrany%2C Ulotka%29 - Klient</fullName>
        <actions>
            <name>Telemarketing_Danfoss_Przegrany_Ulotka_Klient</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Telemarketing__c.Name</field>
            <operation>equals</operation>
            <value>Danfoss</value>
        </criteriaItems>
        <criteriaItems>
            <field>Telemarketing__c.Status__c</field>
            <operation>equals</operation>
            <value>Lost</value>
        </criteriaItems>
        <criteriaItems>
            <field>Telemarketing__c.Lost_Reason__c</field>
            <operation>equals</operation>
            <value>Oferta na maila</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Telemarketing%3A Danfoss %28Wygrany%29</fullName>
        <actions>
            <name>Telemarketing_Danfoss_Wygrany_Klient</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Telemarketing_Danfoss_Wygrany_Partner</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Telemarketing_Danfoss_Wygrany_Partner_SMS</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Telemarketing__c.Name</field>
            <operation>equals</operation>
            <value>Danfoss</value>
        </criteriaItems>
        <criteriaItems>
            <field>Telemarketing__c.Status__c</field>
            <operation>equals</operation>
            <value>Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Telemarketing__c.Post_Conversion_Stage__c</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Telemarketing%3A Eco Partner %28Wygrany%29</fullName>
        <actions>
            <name>Telemarketing_Eco_Partner_Wygrany_Klient</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Telemarketing_Eco_Partner_Wygrany_Partner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Telemarketing__c.Name</field>
            <operation>equals</operation>
            <value>Eco Partner</value>
        </criteriaItems>
        <criteriaItems>
            <field>Telemarketing__c.Status__c</field>
            <operation>equals</operation>
            <value>Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Telemarketing__c.Post_Conversion_Stage__c</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Telemarketing%3A Gaspol %28Wygrany%29</fullName>
        <actions>
            <name>Telemarketing_Gaspol_Wygrany_Klient</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Telemarketing_Gaspol_Wygrany_Partner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Telemarketing__c.Name</field>
            <operation>equals</operation>
            <value>Gaspol</value>
        </criteriaItems>
        <criteriaItems>
            <field>Telemarketing__c.Status__c</field>
            <operation>equals</operation>
            <value>Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Telemarketing__c.Post_Conversion_Stage__c</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Telemarketing%3A Lafarge %28Wygrany%29</fullName>
        <actions>
            <name>Telemarketing_Lafarge_Wygrany_Klient</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Telemarketing_Lafarge_Wygrany_Partner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Telemarketing__c.Name</field>
            <operation>equals</operation>
            <value>Lafarge</value>
        </criteriaItems>
        <criteriaItems>
            <field>Telemarketing__c.Status__c</field>
            <operation>equals</operation>
            <value>Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Telemarketing__c.Post_Conversion_Stage__c</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Telemarketing%3A On Brass Conversion</fullName>
        <actions>
            <name>Telemarketing_Braas_Meeting</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Telemarketing__c.Braas_Stage__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Telemarketing__c.Status__c</field>
            <operation>equals</operation>
            <value>Won</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Telemarketing%3A Przekazanie danych %28Ruukki%29 - Partner</fullName>
        <actions>
            <name>Email_Informacja_dla_Partnera_o_przekazaniu_danych_Klienta_Ruukki</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Telemarketing__c.Name</field>
            <operation>equals</operation>
            <value>Przekazanie danych: blachodachówki</value>
        </criteriaItems>
        <criteriaItems>
            <field>Telemarketing__c.Status__c</field>
            <operation>equals</operation>
            <value>Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Telemarketing__c.Post_Conversion_Stage__c</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Telemarketing%3A Przekazanie danych %28pojedyńcze firmy%29 - Partner</fullName>
        <actions>
            <name>Email_Informacja_dla_Partnera_o_przekazaniu_danych_Klienta</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Telemarketing__c.Name</field>
            <operation>notEqual</operation>
            <value>Przekazanie danych: adaptacje projektów,Przekazanie danych: budowa domu,Przekazanie danych: materiały budowlane,Przekazanie danych: wylewki anhydrytowe,Przekazanie danych: blachodachówki</value>
        </criteriaItems>
        <criteriaItems>
            <field>Telemarketing__c.Status__c</field>
            <operation>equals</operation>
            <value>Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Telemarketing__c.Post_Conversion_Stage__c</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>Telemarketing__c.Name</field>
            <operation>startsWith</operation>
            <value>Przekazanie danych:</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Telemarketing%3A Przekazanie danych%3A adaptacje projektów %28Partner%29</fullName>
        <actions>
            <name>Email_Informacja_dla_Partnera_o_przekazaniu_danych_Klienta</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>SMS_Informacja_dla_Partnera_o_przekazaniu_danych_Klienta</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Telemarketing__c.Name</field>
            <operation>equals</operation>
            <value>Przekazanie danych: adaptacje projektów,Przekazanie danych: budowa domu</value>
        </criteriaItems>
        <criteriaItems>
            <field>Telemarketing__c.Status__c</field>
            <operation>equals</operation>
            <value>Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Telemarketing__c.Post_Conversion_Stage__c</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Telemarketing%3A Przekazanie danych%3A bramy garażowe - Klient</fullName>
        <actions>
            <name>Przekazanie_danych_bramy_gara_owe_Klient</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Telemarketing__c.Name</field>
            <operation>equals</operation>
            <value>Przekazanie danych: bramy garażowe</value>
        </criteriaItems>
        <criteriaItems>
            <field>Telemarketing__c.Status__c</field>
            <operation>equals</operation>
            <value>Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Telemarketing__c.Post_Conversion_Stage__c</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Telemarketing%3A Przekazanie danych%3A dachówki ceramiczne - Klient</fullName>
        <actions>
            <name>Przekazanie_danych_dach_wki_ceramiczne_Klient</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Telemarketing__c.Name</field>
            <operation>equals</operation>
            <value>Przekazanie danych: dachówki ceramiczne</value>
        </criteriaItems>
        <criteriaItems>
            <field>Telemarketing__c.Status__c</field>
            <operation>equals</operation>
            <value>Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Telemarketing__c.Post_Conversion_Stage__c</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Telemarketing%3A Przekazanie danych%3A finansowanie inwestycji - Klient</fullName>
        <actions>
            <name>Przekazanie_danych_finansowanie_inwestycji_Klient</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Telemarketing__c.Name</field>
            <operation>equals</operation>
            <value>Przekazanie danych: finansowanie inwestycji</value>
        </criteriaItems>
        <criteriaItems>
            <field>Telemarketing__c.Status__c</field>
            <operation>equals</operation>
            <value>Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Telemarketing__c.Post_Conversion_Stage__c</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Telemarketing%3A Przekazanie danych%3A instalator rekuperacji - Klient</fullName>
        <actions>
            <name>Przekazanie_danych_instalator_rekuperacji_Klient</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Telemarketing__c.Name</field>
            <operation>equals</operation>
            <value>Przekazanie danych: instalator rekuperacji</value>
        </criteriaItems>
        <criteriaItems>
            <field>Telemarketing__c.Status__c</field>
            <operation>equals</operation>
            <value>Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Telemarketing__c.Post_Conversion_Stage__c</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Telemarketing%3A Przekazanie danych%3A izolacje - Klient</fullName>
        <actions>
            <name>Przekazanie_danych_izolacje_Klient</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Telemarketing__c.Name</field>
            <operation>equals</operation>
            <value>Przekazanie danych: izolacje</value>
        </criteriaItems>
        <criteriaItems>
            <field>Telemarketing__c.Status__c</field>
            <operation>equals</operation>
            <value>Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Telemarketing__c.Post_Conversion_Stage__c</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Telemarketing%3A Przekazanie danych%3A izolacje natryskowe - Klient</fullName>
        <actions>
            <name>Przekazanie_danych_izolacje_natryskowe_Klient</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Telemarketing__c.Name</field>
            <operation>equals</operation>
            <value>Przekazanie danych: izolacje natryskowe</value>
        </criteriaItems>
        <criteriaItems>
            <field>Telemarketing__c.Status__c</field>
            <operation>equals</operation>
            <value>Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Telemarketing__c.Post_Conversion_Stage__c</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Telemarketing%3A Przekazanie danych%3A materiały budowlane - Klient</fullName>
        <actions>
            <name>Przekazanie_danych_materia_y_budowlane_Klient</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Telemarketing__c.Name</field>
            <operation>equals</operation>
            <value>Przekazanie danych: materiały budowlane</value>
        </criteriaItems>
        <criteriaItems>
            <field>Telemarketing__c.Status__c</field>
            <operation>equals</operation>
            <value>Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Telemarketing__c.Post_Conversion_Stage__c</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Telemarketing%3A Przekazanie danych%3A schody - Klient</fullName>
        <actions>
            <name>Przekazanie_danych_schody_Klient</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Telemarketing__c.Name</field>
            <operation>equals</operation>
            <value>Przekazanie danych: schody</value>
        </criteriaItems>
        <criteriaItems>
            <field>Telemarketing__c.Status__c</field>
            <operation>equals</operation>
            <value>Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Telemarketing__c.Post_Conversion_Stage__c</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Telemarketing%3A Przekazanie danych%3A wylewki anhydrytowe %28Partner%29</fullName>
        <actions>
            <name>Email_Informacja_dla_Partnera_o_przekazaniu_danych_Klienta_wylewki_anhydrytowe</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>SMS_Informacja_dla_Partnera_o_przekazaniu_danych_Klienta_wylewki_anhydrytowe</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Telemarketing__c.Name</field>
            <operation>equals</operation>
            <value>Przekazanie danych: wylewki anhydrytowe</value>
        </criteriaItems>
        <criteriaItems>
            <field>Telemarketing__c.Status__c</field>
            <operation>equals</operation>
            <value>Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Telemarketing__c.Post_Conversion_Stage__c</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Telemarketing%3A Przekazanie danych%3A wylewki anhydrytowe - Klient</fullName>
        <actions>
            <name>Przekazanie_danych_wylewki_anhydrytowe_Klient</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Telemarketing__c.Name</field>
            <operation>equals</operation>
            <value>Przekazanie danych: wylewki anhydrytowe</value>
        </criteriaItems>
        <criteriaItems>
            <field>Telemarketing__c.Status__c</field>
            <operation>equals</operation>
            <value>Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Telemarketing__c.Post_Conversion_Stage__c</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Telemarketing%3A Start Date</fullName>
        <actions>
            <name>Telemarketing_Start_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Telemarketing__c.Status__c</field>
            <operation>notEqual</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Telemarketing__c.Start_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Telemarketing%3A Update Partner Email</fullName>
        <actions>
            <name>Update_Transfer_Account_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>Wys_ano_email_z_Klientem_do_partnera</fullName>
        <assignedTo>marta.miecznikowska@extradom.pl</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Wysłano email z Klientem do partnera</subject>
    </tasks>
</Workflow>
