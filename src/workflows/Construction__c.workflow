<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Construction_On_Location_PlotName_Edit</fullName>
        <description>Construction: On Location/PlotName Edit</description>
        <protected>false</protected>
        <recipients>
            <recipient>marcin.wiater@extradom.pl</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Construction_On_Location_PlotName_Edit</template>
    </alerts>
    <rules>
        <fullName>Construction%3A On Location%2FPlotName Edit</fullName>
        <actions>
            <name>Construction_On_Location_PlotName_Edit</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>(NOT(ISNEW()) &amp;&amp; (ISCHANGED(Location__c) || ISCHANGED(Plot_Name__c))) &amp;&amp; ISBLANK(Plot__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
