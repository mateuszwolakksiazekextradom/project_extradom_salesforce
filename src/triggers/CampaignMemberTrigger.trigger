/**
 * Created by grzegorz.dlugosz on 20.05.2019.
 */
trigger CampaignMemberTrigger on CampaignMember (before insert, before update, before delete, after insert, after update, after delete) {
    new CampaignMemberTriggerHandler().run();
}