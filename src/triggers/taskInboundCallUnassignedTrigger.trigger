trigger taskInboundCallUnassignedTrigger on Task (after insert, after update) {
  
  List<Id> tasksToCheck = new List<Id>{};
  for (Task t : Trigger.new) {
      if (String.isNotBlank(t.Caller_Called_Phone__c) && String.isBlank(t.WhoId)) {
          tasksToCheck.add(t.Id);
      }
  }
  if (!tasksToCheck.isEmpty()) {
      CallTaskProcessing.processRecords(tasksToCheck);
  }

}