trigger afterInsertOrDeleteAttachmentOnMedia on Attachment (after insert, after delete) {

    List<Id> mids = new List<Id>();
    for (Attachment att : Trigger.IsDelete ? Trigger.old : Trigger.new) {
        if (att.Name.endsWith('babylon') && String.valueOf(att.ParentId).startsWith('a07')) {
            mids.add(att.ParentId);          
        }
    }
    
    Map<Id,Boolean> designsAndAttachment = new Map<Id,Boolean>();
    for (Media__c m : [SELECT Id, Design__r.Id FROM Media__c WHERE Id IN :mids]) {
        designsAndAttachment.put(m.Design__r.Id,false);
    }
    
    for (Media__c m : [SELECT Id, Design__r.Id, (SELECT Id FROM Attachments WHERE Name LIKE '%babylon') FROM Media__c WHERE Design__r.Id IN :designsAndAttachment.keySet()]) {
        designsAndAttachment.put(m.Design__r.Id,m.Attachments.isEmpty() ? designsAndAttachment.get(m.Design__r.Id) : true);
    }
    
    List<Design__c> toUpdate = new List<Design__c>();
    for (Design__c d : [SELECT Id, Has_Media_With_Babylon__c FROM Design__c WHERE Id IN :designsAndAttachment.keySet()]) {
        if (d.Has_Media_With_Babylon__c != designsAndAttachment.get(d.Id)) {
            d.Has_Media_With_Babylon__c = designsAndAttachment.get(d.Id);
            toUpdate.add(d);
        } 
    }
    
    update toUpdate;  
}