trigger designAfterChange on Design__c (after insert, after update) {
    
    Set<ID> baseDesignIdsToUpdate = new Set<ID>{};
    for (Design__c d: Trigger.new) {
        ID oldBaseDesignId = null;
        ID newBaseDesignId = null;
        if (Trigger.isUpdate) {
            oldBaseDesignId = Trigger.oldMap.get(d.Id).Base_Design__c;
            newBaseDesignId = Trigger.newMap.get(d.Id).Base_Design__c;
        } else {
            oldBaseDesignId = null;
            newBaseDesignId = Trigger.newMap.get(d.Id).Base_Design__c;
        }
        if (oldBaseDesignId != newBaseDesignId) {
            if (oldBaseDesignId != null) {
                baseDesignIdsToUpdate.add(oldBaseDesignId);
            }
            if (newBaseDesignId != null) {
                baseDesignIdsToUpdate.add(newBaseDesignId);
            }
        }
    }
    
    List<Design__c> designsToUpdate = [SELECT Id FROM Design__c WHERE Id IN :baseDesignIdsToUpdate OR Base_Design__c IN :baseDesignIdsToUpdate];
    for (Design__c d: designsToUpdate) {
        d.Update_Date__c = System.now();
    }
    if (!designsToUpdate.isEmpty()) {
        update designsToUpdate;
    }
    
}