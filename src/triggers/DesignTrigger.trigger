trigger DesignTrigger on Design__c (before delete, before insert, before update, after insert, after update) {
    new DesignTriggerHandler().run();
}