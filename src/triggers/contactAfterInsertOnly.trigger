trigger contactAfterInsertOnly on Contact (after insert) {
    
    Map<Id,Id> ids = new Map<Id, Id>();
    Map<String,Id> ms = new Map<String,Id>();
    
    for (Contact c : [SELECT Id, Phone FROM Contact WHERE Id IN :Trigger.newMap.keySet()]) {
        ms.put(c.Phone, c.Id);
    }
    
    List<Task> inboundCalls = [SELECT Id, Inbound_Number__c FROM Task WHERE Type = 'Call' AND CreatedDate = today AND WhoId = '' AND CallType = 'Inbound' AND Inbound_Number__c IN :ms.keySet()];
    
    for (Task t : inboundCalls) {
        if (ms.get(t.Inbound_Number__c) != null) {
            ids.put(t.Id, ms.get(t.Inbound_Number__c));
        }
    }
    
    List<Task> toUpdate = [SELECT Id, WhoId FROM Task WHERE Id IN :ids.keySet()];
    
    for (Task t : toUpdate) {
        t.WhoId = ids.get(t.Id); 
    }    
    
    update toUpdate;
}