trigger taskCallTrigger on Task (after insert) {
    
    List<Task> answeredCalls = [SELECT Id, AccountId, OwnerId FROM Task WHERE CallType != '' AND CallDurationInSeconds > 0 AND AccountId != null AND IsClosed = true AND Id IN :Trigger.newMap.keySet() AND (CreatedBy.Department = 'COK' OR CreatedBy.Department = 'B2B')];
    Set<Id> answeredAccountIds = new Set<Id>{};
    for (Task t : answeredCalls) {
        answeredAccountIds.add(t.AccountId);
    }
    List<Task> openActionsToClose = new List<Task>();
    if (!answeredAccountIds.isEmpty()) {
        openActionsToClose = [SELECT Id FROM Task WHERE IsClosed = false AND (Type = 'Unanswered Call' OR Type = 'Call Request' OR Type = 'Lead Nurturing Alert' OR Type = 'Alert' OR Type = 'Abandoned Cart') AND AccountId IN :answeredAccountIds];
        for (Task t : openActionsToClose) {
            t.ActivityDate = System.today();
            t.Status = 'Completed';
            t.OwnerId = UserInfo.getUserId();
            t.Close_Date__c = System.now();
        }
    }
    if (!openActionsToClose.isEmpty()) {
        update openActionsToClose;
    }
    
    /*
    List<Account> answeredAccounts = [SELECT Id, Next_Call__c, Teaser__c, OwnerId, Owner.IsActive FROM Account WHERE Id IN :answeredAccountIds AND (Sales_Has_Design__c = 'No' OR Sales_Has_Design__c = '') AND (Type = 'Investor' OR Type = 'Dealer')];
    List<Task> followUpsToCreate = new List<Task>();
    for (Account a : answeredAccounts) {
        Task t = new Task();
        t.Subject = 'Następny kontakt telefoniczny';
        t.Type = 'Follow Up Call';
        t.ActivityDate = a.Next_Call__c;
        t.WhatId = a.Id;
        if (a.Owner.IsActive) {
            t.OwnerId = a.OwnerId;
        }
        t.Teaser__c = a.Teaser__c;
        followUpsToCreate.add(t);
    }
    if (!followUpsToCreate.isEmpty()) {
        insert followUpsToCreate;
    }
    */
    
}