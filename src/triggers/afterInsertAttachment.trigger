trigger afterInsertAttachment on Attachment (after insert) {
    
    for (Attachment att : Trigger.New) {
        if (att.ContentType != null) {
            if (att.ContentType == 'image/svg+xml' && String.valueOf(att.ParentId).startsWith('a05')) {
                AWSTools.uploadFileBase64('static3d', UserInfo.getOrganizationId()+'/'+att.ParentId+'.svg', EncodingUtil.base64Encode(att.Body), att.ContentType, 'AKIAJO25LHDRMTH7SLBA', 'zjzdMrMfTi3i26wIqHEKAwvZkjFBN0qJmGE4sH8n');
            } else if (att.ContentType.startsWith('image/') && (String.valueOf(att.ParentId).startsWith('a05') || String.valueOf(att.ParentId).startsWith('a0R'))) {
                AWSTools.uploadFileBase64('extradom.media', UserInfo.getOrganizationId()+'-'+att.Id+'/source', EncodingUtil.base64Encode(att.Body), att.ContentType, 'AKIAJO25LHDRMTH7SLBA', 'zjzdMrMfTi3i26wIqHEKAwvZkjFBN0qJmGE4sH8n');
            } else if (att.ContentType == 'application/javascript' && att.Name.endsWith('.babylon') && String.valueOf(att.ParentId).startsWith('a05')) {
                AWSTools.uploadFileBase64('static3d', UserInfo.getOrganizationId()+'/'+att.ParentId+'.babylon', EncodingUtil.base64Encode(att.Body), 'application/json', 'AKIAJO25LHDRMTH7SLBA', 'zjzdMrMfTi3i26wIqHEKAwvZkjFBN0qJmGE4sH8n');
            } else if (att.ContentType == 'application/javascript' && att.Name.endsWith('.babylon') && String.valueOf(att.ParentId).startsWith('a07')) {
                String version = att.Name.containsIgnoreCase('mirror')?'-mirror':'';
                AWSTools.uploadFileBase64('static3d', UserInfo.getOrganizationId()+'/'+att.ParentId+'/plan'+version+'.babylon', EncodingUtil.base64Encode(att.Body), 'application/json', 'AKIAJO25LHDRMTH7SLBA', 'zjzdMrMfTi3i26wIqHEKAwvZkjFBN0qJmGE4sH8n');
            } else if (att.ContentType == 'application/javascript' && att.Name.endsWith('.babylon') && String.valueOf(att.ParentId).startsWith('a0R')) {
                Item_Color__c ic = [SELECT Id FROM Item_Color__c WHERE Id = :att.ParentId];
                ic.Babylon_Materials__c = JSON.serialize(((Babylon)(JSON.deserialize(att.Body.toString(), Babylon.class))).materials);
                update ic;
                AWSTools.uploadFileBase64('static3d', UserInfo.getOrganizationId()+'/'+att.ParentId+'.babylon', EncodingUtil.base64Encode(att.Body), 'application/json', 'AKIAJO25LHDRMTH7SLBA', 'zjzdMrMfTi3i26wIqHEKAwvZkjFBN0qJmGE4sH8n');
            }
        }
    }

}