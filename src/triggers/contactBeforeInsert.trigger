trigger contactBeforeInsert on Contact (before insert) {
    
    for (Contact c : Trigger.new) {
        
        if (String.isBlank(c.AccountId)) {
            
            if (String.isBlank(c.Email)) {
                c.AddError('Cannot add Contact with no Account and Email specified');
            } else {
                if (String.isBlank(c.LastName)) {
                    c.LastName = c.Email;
                }
                Account a = new Account(Name=c.LastName);
                if (String.isBlank(c.Community_Email__c)) {
                    a.AccountSource='Import';
                } else {
                    a.AccountSource='Extradom.pl (Community)';
                }
                insert a;
                c.AccountId = a.Id;
            }
            
        }
        
    }

}