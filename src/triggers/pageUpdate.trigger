trigger pageUpdate on Page__c (before update) {
	
	for (Page__c p: Trigger.new) {
		if (p.Type__c == 'Text' || p.Type__c == 'Collection') {
			String likeString = ('%[' + p.Search_Text__c + ']%').replace('%[null]%','%%');
			likeString = likeString.replace('-',' ').replace('+',' ').replace('%[null]%','%%');
			String main_category = ('%['+ p.Main_Category_Key__c +']%').replace('%[null]%','%%');
			String building_cost = ('%['+ p.Building_Cost_Key__c +']%').replace('%[null]%','%%');
			String technology = ('%['+ p.Technology_Key__c +']%').replace('%[null]%','%%');
			String style = ('%['+ p.Style_Key__c +']%').replace('%[null]%','%%');
			String living_type = ('%['+ p.Living_Type_Key__c +']%').replace('%[null]%','%%');
			String add_ons = ('%['+ p.Add_Ons_Key__c +']%').replace('%[null]%','%%');
			String roof = ('%'+ p.Roof__c + '%').replace('%null%','%%');
			p.Average_Page_Value__c = (Decimal) [SELECT AVG(Page_Value__c)avg FROM Design__c WHERE Usable_Area__c >= :p.Usable_Area_min__c AND Usable_Area__c <= :p.Usable_Area_max__c AND Minimum_Plot_Size_Horizontal__c <= :p.Plot_H_min__c AND Height__c <= :p.Height_max__c AND Footprint__c <= :p.Footprint_max__c AND Name like :likeString AND Categories__c like :main_category AND Categories__c like :building_cost AND Categories__c like :technology AND Categories__c like :style AND Categories__c like :living_type AND Categories__c like :add_ons AND Tags__c like :roof AND Status__c = 'Active'][0].get('avg');
		}
		//p.Average_Keyword_Pages_Session__c = (Decimal) [SELECT AVG(Pages_Session__c)avg FROM Keyword__c WHERE Page__c = :p.Id AND Pages_Session__c > 0 AND Pages_Session__c < 60][0].get('avg');
	}
	
}