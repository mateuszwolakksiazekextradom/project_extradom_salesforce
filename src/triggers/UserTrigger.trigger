/**
 * Created by mateusz.wolak on 05.06.2019.
 */

trigger UserTrigger on User (before insert, before update, before delete, after insert, after update, after delete) {
    new UserTriggerHandler().run();
}