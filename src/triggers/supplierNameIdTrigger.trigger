trigger supplierNameIdTrigger on Supplier__c (after insert, after update) {

  // This queries all Design related to the incoming Supplier records in a single SOQL query.
  List<Supplier__c> suppliersWithDesigns = [select id, Code__c, (select id, Design_Name_ID__c, Design_Name_ID_Postfix__c from Designs__r) from Supplier__c where Id IN :Trigger.newMap.keySet()];

  List<Design__c> designsToUpdate = new List<Design__c>{};
  // For loop to iterate through all the queried Design records 
  for (Supplier__c s: suppliersWithDesigns) {
     // Use the child relationships dot syntax to access the related Media
     for (Design__c d: s.Designs__r) {
     	if (designsToUpdate.size()>300) {
     		break;
     	}
     	if(d.Design_Name_ID__c != s.Code__c + ': ' + d.Design_Name_ID_Postfix__c) {
			d.Design_Name_ID__c = s.Code__c + ': ' + d.Design_Name_ID_Postfix__c;
			designsToUpdate.add(d);
     	}
     }
   }
      
   //Now outside the FOR Loop, perform a single Update DML statement.
     update designsToUpdate;

	
}