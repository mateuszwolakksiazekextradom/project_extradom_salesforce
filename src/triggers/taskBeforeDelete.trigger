trigger taskBeforeDelete on Task (before Delete) {

    Trigger_Profiles__c tp = Trigger_Profiles__c.getInstance();
    if (UserInfo.getUserId() != null && tp.Ids__c.contains(UserInfo.getUserRoleId())) {
        throw new NoAccessException();
    } else {
        Task coverage = new Task(Subject='Coverage');
    }
}