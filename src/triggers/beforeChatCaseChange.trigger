trigger beforeChatCaseChange on Case (before insert, before update) {
    
    for (Case cs : Trigger.new) {
        
        if (String.isBlank(cs.OwnerId)) {
            cs.OwnerId = UserInfo.getUserId();
        }
        String phoneWithoutWithespaces;
        if (!String.isBlank(cs.SuppliedPhone)){
            phoneWithoutWithespaces = cs.SuppliedPhone.deleteWhitespace();
        }
        if (String.isBlank(cs.ContactId) && String.isNotBlank(cs.SuppliedName)) {
            Contact c;
            Contact[] contacts = [SELECT Id, Account.Lead_Source__c FROM Contact WHERE Email != '' AND Email = :cs.SuppliedEmail LIMIT 1];
            Contact[] contactsPhone = [SELECT Id FROM Contact WHERE PhoneId__c != '' AND PhoneId__c = :phoneWithoutWithespaces LIMIT 1];
            if (contacts.isEmpty()) {
                Account a = New Account(Name=cs.SuppliedName.toLowerCase().capitalize(), OwnerId=cs.OwnerId, AccountSource='Extradom.pl (Chat)', Lead_Source__c='Chat', Lead_Source_Date__c=System.now());
                if (String.isNotBlank(cs.Source_Partner_ID__c)) {
                    a.AccountSource='Partner';
                }
                insert a;
                c = new Contact(FirstName=cs.SuppliedName.toLowerCase().capitalize().left(40), LastName='-', Email=cs.SuppliedEmail, AccountId=a.Id, OwnerId=cs.OwnerId);
                if (contactsPhone.isEmpty()) {
                    c.Phone = phoneWithoutWithespaces;
                }
                insert c;
            } else {
                c = contacts[0];
                if (String.isBlank(c.Account.Lead_Source__c)) {
                    c.Account.Lead_Source__c = 'Chat';
                    c.Account.Lead_Source_Date__c = System.now();
                    update c.Account;
                }
            }
            cs.ContactId = c.Id;
        } else {
            Contact c = [SELECT Id, FirstName, Phone, Account.Lead_Source__c FROM Contact WHERE Id =: cs.contactId LIMIT 1];
            Boolean changed = false;
            if (String.isBlank(c.Phone)) {
                Contact[] contactsPhone = [SELECT Id FROM Contact WHERE PhoneId__c != '' AND PhoneId__c = :phoneWithoutWithespaces LIMIT 1];
                if (contactsPhone.isEmpty()) {
                    c.Phone = phoneWithoutWithespaces;
                    changed = true;
                }
            }
            if (String.isBlank(c.FirstName)) {
                c.FirstName = cs.SuppliedName.toLowerCase().capitalize();
                changed = true;
            }
            if (changed) {
                update c;
            }
            if (String.isBlank(c.Account.Lead_Source__c)) {
                c.Account.Lead_Source__c = 'Chat';
                c.Account.Lead_Source_Date__c = System.now();
                update c.Account;
            }
        }
        
    }
    
}