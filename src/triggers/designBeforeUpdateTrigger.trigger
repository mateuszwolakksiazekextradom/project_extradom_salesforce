trigger designBeforeUpdateTrigger on Design__c (before update) {
    
try {
    Map<Id,Integer> countContentPosts = new Map<Id,Integer>();
    for (AggregateResult ar : [SELECT ParentId, count(Id)contentPosts FROM Design__Feed WHERE ParentId IN :Trigger.newMap.keySet() AND Type = 'ContentPost' AND Visibility = 'AllUsers' GROUP BY ParentId]) {
        countContentPosts.put((Id)ar.get('ParentId'), (Integer)ar.get('contentPosts'));
    }
    Map<Id,Integer> countConstructionOwners = new Map<Id,Integer>();
    for (AggregateResult ar : [SELECT Design__c, count(Id)constructionOwners FROM Construction__c WHERE Design__c IN :Trigger.newMap.keySet() GROUP BY Design__c]) {
        countConstructionOwners.put((Id)ar.get('Design__c'), (Integer)ar.get('constructionOwners'));
    }
    
    for (Design__c d: Trigger.new) {
        
        if (countContentPosts.containsKey(d.Id)) {
            d.Community_Images__c = countContentPosts.get(d.Id);
        } else {
            d.Community_Images__c = 0;
        }
        if (countConstructionOwners.containsKey(d.Id)) {
            d.Community_Builders__c = countConstructionOwners.get(d.Id);
        } else {
            d.Community_Builders__c = 0;
        }
    
    }

} catch(Exception e) {
}

}