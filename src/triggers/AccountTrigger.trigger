trigger AccountTrigger on Account (before update, before delete, after update, after insert, after delete) {
    new AccountTriggerHandler().run();
}