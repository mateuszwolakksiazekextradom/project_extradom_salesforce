trigger contactBeforeUpdate on Contact (before insert, before update) {
    
    List<String> emails = new List<String>{};
    for (Contact c : Trigger.new) {
        if (String.isNotBlank(c.Email) && c.Community_Member__c == null) {
            emails.add(c.Email);
        }
    }
    
    List<Community_Member__c> cms = [SELECT Id, Email__c FROM Community_Member__c WHERE Email__c != '' AND Email__c IN :emails];
    Map<String,ID> emailsCms = new Map<String,ID>{};
    for (Community_Member__c cm : cms) {
        emailsCms.put(cm.Email__c, cm.Id);
    }
    
    for (Contact c : Trigger.new) {
        if (String.isNotBlank(c.Email) && c.Community_Member__c == null) {
            if (emailsCms.containsKey(c.Email)) {
                c.Community_Member__c = emailsCms.get(c.Email);
            }
        }
        if (String.isBlank(c.Salutation) && String.isNotBlank(c.FirstName)) {
            try {
                FirstName__c fn = FirstName__c.getInstance(c.FirstName);
                if (fn!=null) {
                    c.Salutation = fn.Salutation__c;
                }
            } catch (Exception e) {
            }
        }
    }
    
}