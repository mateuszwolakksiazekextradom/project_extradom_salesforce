trigger accountContactInfoOnMerge on Account (after delete, after update) {

    if (trigger.isDelete){
        for(Account a : trigger.old){
            if(a.MasterRecordId != null){
                AccountMasterIds.masterIds.add(a.MasterRecordId);
            }
        }
    }
    
    if (trigger.isUpdate && !AccountMasterIds.masterIds.isEmpty()){
        
        Set<Id> masterIds = new Set<Id>(AccountMasterIds.masterIds);
        
        Map<Id,Boolean> phonesToChange = new Map<id,Boolean>{};
        Map<Id,Boolean> emailsToChange = new Map<id,Boolean>{};
    
        for (Account a : [Select Id From Account where Id IN :masterIds]) {
            phonesToChange.put(a.Id, false);
            emailsToChange.put(a.Id, false);
        }
        
        for (Contact c : [SELECT Id, Phone, Email, AccountId, Account.Has_Contact_with_Phone__c, Account.Has_Contact_with_Email__c FROM Contact WHERE AccountId IN :masterIds]) {
            If (String.isNotBlank(c.Phone) && c.Account.Has_Contact_with_Phone__c == false){
                phonesToChange.put(c.AccountId, true);
            }
            If (String.isNotBlank(c.Email) && c.Account.Has_Contact_with_Email__c == false){
                emailsToChange.put(c.AccountId, true);
            }
        }
       
        Boolean toUpdate = false;
                
        List<account> accounts = [SELECT Id, Has_Contact_with_Phone__c, Has_Contact_with_Email__c FROM Account WHERE Id IN :masterIds];
        for (Account a : accounts) {
            if(phonesToChange.get(a.Id) == true){
                a.Has_Contact_with_Phone__c = true;
                toUpdate = true;
                }
            if(emailsToChange.get(a.Id) == true){
                a.Has_Contact_with_Email__c = true;
                toUpdate = true;
                }
        }
        if(toUpdate){
            update accounts;
        }
    }
}