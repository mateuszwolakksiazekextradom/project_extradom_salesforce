trigger communityMemberDetailsRoleTrigger on Community_Member_Detail__c (before insert, before update) {
	
	for (Community_Member_Detail__c cmd_t : Trigger.new) {
		
		System.debug(cmd_t.RecordType.DeveloperName);
		
		if (cmd_t.Role__c == 'Organization') {
		
			Community_Member__c cm = [SELECT Id, Business_Type__c, Business_Phone__c, Business_Full_Address__c, (SELECT Name, Name__c, RecordType.DeveloperName, Location__r.Name, Location__r.Parent_Location__r.Name, Location__r.Parent_Location__r.Parent_Location__r.Name, Location__r.RecordType.DeveloperName, Community_Tag__r.Name, Community_Tag__r.RecordType.DeveloperName, Related_Community_Member__r.Name, Related_Community_Member__r.Membercode__c FROM Community_Member_Details__r) FROM Community_Member__c WHERE Id =: cmd_t.Community_Member__c];
			
			XmlStreamWriter xml = new XmlStreamWriter();
			xml.writeStartElement(null, 'member', null);
			xml.writeAttribute(null, null, 'type', (AmazonSqsSender.xmlString(cm.Business_Type__c)).toLowerCase());
			
				xml.writeStartElement(null, 'attributes', null); /* <attributes> */
					xml.writeStartElement(null, 'att', null);
						xml.writeAttribute(null, null, 'name', 'phone');
						xml.writeCharacters(AmazonSqsSender.xmlString(cm.Business_Phone__c));
					xml.writeEndElement();
					xml.writeStartElement(null, 'att', null);
						xml.writeAttribute(null, null, 'name', 'address');
						xml.writeCharacters(AmazonSqsSender.xmlString(cm.Business_Full_Address__c));
					xml.writeEndElement();
					List<String> range = new List<String>();
					for (Community_Member_Detail__c cmd: cm.Community_Member_Details__r) {
			    		if (cmd.RecordType.DeveloperName == 'Business_Activity_Location') {
							if (cmd.Location__r.Name.startsWith('m. st. ')) {
							    range.add(cmd.Location__r.Name.substring(7));
							} else if (cmd.Location__r.Name.startsWith('m. ')) {
							    range.add(cmd.Location__r.Name.substring(3));
							} else {
							    range.add(cmd.Location__r.Name);
							}
			    		}
			    	}
			    	xml.writeStartElement(null, 'att', null);
						xml.writeAttribute(null, null, 'name', 'range');
						xml.writeCharacters(String.join(range,', '));
					xml.writeEndElement();
					List<String> links = new List<String>();
					for (Community_Member_Detail__c cmd: cm.Community_Member_Details__r) {
			    		if (cmd.RecordType.DeveloperName == 'Business_Connection' && String.isBlank(cmd.Related_Community_Member__r.Membercode__c)) {
							links.add(cmd.Related_Community_Member__r.Name);
			    		}
			    	}
			    	if (!links.isEmpty()) {
			    	xml.writeStartElement(null, 'att', null);
						xml.writeAttribute(null, null, 'name', 'links');
						xml.writeCharacters(String.join(links,', '));
					xml.writeEndElement();
			    	}
				xml.writeEndElement(); /* </attributes> */
				
				xml.writeStartElement(null, 'products', null);
				for (Community_Member_Detail__c cmd: cm.Community_Member_Details__r) {
		    		if (cmd.Community_Tag__r.RecordType.DeveloperName == 'Product' && (cm.Business_Type__c == 'Brand' || cm.Business_Type__c == 'Shop' || cm.Business_Type__c == 'Warehouse')) {
				
					xml.writeStartElement(null, 'product', null);
						xml.writeAttribute(null, null, 'name', cmd.Community_Tag__r.Name);
					xml.writeEndElement();
					
		    		}
				}
				xml.writeEndElement();
				
				xml.writeStartElement(null, 'locations', null);
			
				for (Community_Member_Detail__c cmd: cm.Community_Member_Details__r) {
		    		if (cmd.RecordType.DeveloperName == 'Business_Activity_Location' && cmd.Location__r.RecordType.DeveloperName == 'Region') {
	                    
	                    xml.writeStartElement(null, 'location', null);
						xml.writeAttribute(null, null, 'type', 'country');
						xml.writeAttribute(null, null, 'name', cmd.Location__r.Parent_Location__r.Parent_Location__r.Name);
	                        
	                        xml.writeStartElement(null, 'location', null);
							xml.writeAttribute(null, null, 'type', 'state');
							xml.writeAttribute(null, null, 'name', cmd.Location__r.Parent_Location__r.Name);
	
	                            xml.writeStartElement(null, 'location', null);
								xml.writeAttribute(null, null, 'type', 'region');
								xml.writeAttribute(null, null, 'name', cmd.Location__r.Name);
	                            
	                            if (cmd.Location__r.Name.startsWith('m. st. ')) {
	                                xml.writeStartElement(null, 'location', null);
	                                xml.writeAttribute(null, null, 'type', 'city');
	                                xml.writeAttribute(null, null, 'name', cmd.Location__r.Name.substring(7));
	                                xml.writeEndElement();
	                            } else if (cmd.Location__r.Name.startsWith('m. ')) {
	                                xml.writeStartElement(null, 'location', null);
	                                xml.writeAttribute(null, null, 'type', 'city');
	                                xml.writeAttribute(null, null, 'name', cmd.Location__r.Name.substring(3));
	                                xml.writeEndElement();
	                            }
	                            
	                            xml.writeEndElement();
	                        xml.writeEndElement();
	                    xml.writeEndElement();
	                    
	                } else if (cmd.RecordType.DeveloperName == 'Business_Activity_Location' && cmd.Location__r.RecordType.DeveloperName == 'State') {
	                    
	                    xml.writeStartElement(null, 'location', null);
						xml.writeAttribute(null, null, 'type', 'country');
						xml.writeAttribute(null, null, 'name', cmd.Location__r.Parent_Location__r.Name);
	                        
	                        xml.writeStartElement(null, 'location', null);
							xml.writeAttribute(null, null, 'type', 'state');
							xml.writeAttribute(null, null, 'name', cmd.Location__r.Name);
	                    
	                    	List<Location__c> regions = [SELECT Name FROM Location__c WHERE Parent_Location__c = :cmd.Location__c];
	                        
	                        for (Location__c region : regions) {
	                            xml.writeStartElement(null, 'location', null);
								xml.writeAttribute(null, null, 'type', 'region');
								xml.writeAttribute(null, null, 'name', region.Name);
	                            
	                            if (region.Name.startsWith('m. st. ')) {
	                                xml.writeStartElement(null, 'location', null);
	                                xml.writeAttribute(null, null, 'type', 'city');
	                                xml.writeAttribute(null, null, 'name', region.Name.substring(7));
	                                xml.writeEndElement();
	                            } else if (region.Name.startsWith('m. ')) {
	                                xml.writeStartElement(null, 'location', null);
	                                xml.writeAttribute(null, null, 'type', 'city');
	                                xml.writeAttribute(null, null, 'name', region.Name.substring(3));
	                                xml.writeEndElement();
	                            }
	                            xml.writeEndElement();
	                        }
	                        xml.writeEndElement();
	                    xml.writeEndElement();
	                    
	                } else if (cmd.RecordType.DeveloperName == 'Business_Activity_Location' && cmd.Location__r.RecordType.DeveloperName == 'Country') {
	                    
	                    xml.writeStartElement(null, 'location', null);
						xml.writeAttribute(null, null, 'type', 'country');
						xml.writeAttribute(null, null, 'name', cmd.Location__r.Name);
	                    
	                    List<Location__c> states = [SELECT Name, (SELECT Name FROM Locations__r) FROM Location__c WHERE Parent_Location__c = :cmd.Location__c];
	                    for (Location__c state : states) {
	                        
	                        xml.writeStartElement(null, 'location', null);
							xml.writeAttribute(null, null, 'type', 'state');
							xml.writeAttribute(null, null, 'name', state.Name);
	                        
	                        for (Location__c region : state.Locations__r) {
	
	                            xml.writeStartElement(null, 'location', null);
								xml.writeAttribute(null, null, 'type', 'region');
								xml.writeAttribute(null, null, 'name', region.Name);
	                            
	                            if (region.Name.startsWith('m. st. ')) {
	                                xml.writeStartElement(null, 'location', null);
	                                xml.writeAttribute(null, null, 'type', 'city');
	                                xml.writeAttribute(null, null, 'name', region.Name.substring(7));
	                                xml.writeEndElement();
	                            } else if (region.Name.startsWith('m. ')) {
	                                xml.writeStartElement(null, 'location', null);
	                                xml.writeAttribute(null, null, 'type', 'city');
	                                xml.writeAttribute(null, null, 'name', region.Name.substring(3));
	                                xml.writeEndElement();
	                            }
	                            xml.writeEndElement();
	                        }
	                        xml.writeEndElement();
	                    }
	                    xml.writeEndElement();
	                }
				}
				xml.writeEndElement();
				
				xml.writeStartElement(null, 'services', null);
				for (Community_Member_Detail__c cmd: cm.Community_Member_Details__r) {
		    		if (cmd.Community_Tag__r.RecordType.DeveloperName == 'Service' && cm.Business_Type__c == 'Service') {
				
					xml.writeStartElement(null, 'service', null);
						xml.writeAttribute(null, null, 'name', cmd.Community_Tag__r.Name);
					xml.writeEndElement();
					
		    		}
				}
				xml.writeEndElement();
				
				xml.writeStartElement(null, 'items', null);
				xml.writeEndElement();
				
				xml.writeStartElement(null, 'media', null);
					for (Community_Member_Detail__c cmd: cm.Community_Member_Details__r) {
			    		if (cmd.RecordType.DeveloperName == 'Certificate' || cmd.RecordType.DeveloperName == 'Award') {

						xml.writeStartElement(null, 'image', null);
							xml.writeAttribute(null, null, 'name', cmd.Name__c);
							xml.writeAttribute(null, null, 'type', cmd.RecordType.DeveloperName.toLowerCase());
							xml.writeAttribute(null, null, 'bucket', 'extradom.media');
							xml.writeAttribute(null, null, 'key', 'cwd-'+cmd.Name);
						xml.writeEndElement();
						
			    		}
					}
				xml.writeEndElement();
				
				xml.writeStartElement(null, 'links', null);
					for (Community_Member_Detail__c cmd: cm.Community_Member_Details__r) {
			    		if (cmd.RecordType.DeveloperName == 'Business_Connection' && String.isNotBlank(cmd.Related_Community_Member__r.Membercode__c)) {

						xml.writeStartElement(null, 'link', null);
							xml.writeAttribute(null, null, 'membercode', cmd.Related_Community_Member__r.Membercode__c);
						xml.writeEndElement();
						
			    		}
					}
				xml.writeEndElement();
				
			xml.writeEndElement();
			
			cmd_t.orgXml__c = xml.getXmlString();
			
		}
		
	}
}