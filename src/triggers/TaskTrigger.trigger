trigger TaskTrigger on Task (after update, before update, after insert) {
    new TaskTriggerHandler().run();
}