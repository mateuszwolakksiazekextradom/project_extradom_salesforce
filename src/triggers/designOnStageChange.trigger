trigger designOnStageChange on Design__c (before update, before insert) {

    List<Id> supp = new List<Id>();
    
    for (Design__c dnew : Trigger.New) {
        for (Design__c dold : Trigger.Old) {
            if (dold==dnew && ((dnew.Stage__c == 'Idea' && dold.Stage__c != 'Idea') || (dnew.Stage__c != 'Idea' && dold.Stage__c == 'Idea'))) {
                supp.add(dnew.Supplier__r.Id);
            } 
        }     
    }
    
    List<Supplier__c> toUpdate = new List<Supplier__c>();
    
    for (Supplier__c s : [SELECT Id, Count_Idea_Design__c FROM Supplier__c WHERE Id IN :supp]) {
        s.Count_Idea_Design__c = [SELECT Id FROM Design__c WHERE Supplier__r.Id = :s.Id].size();
        toUpdate.add(s);
    }
    
    if (!toUpdate.isEmpty()) {
        update toUpdate;
    }
}