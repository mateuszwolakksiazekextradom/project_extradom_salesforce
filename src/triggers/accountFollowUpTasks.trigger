trigger accountFollowUpTasks on Account (after update) {
    
    /*
    List<Account> salesReadyAccounts = [SELECT Id, Next_Call__c, Teaser__c, OwnerId, Owner.IsActive, Owner.Manager.Id FROM Account WHERE Id IN :Trigger.newMap.keySet() AND (Sales_Has_Design__c = 'No' OR Sales_Has_Design__c = '' OR Sales_Has_Design__c = 'Yes (Next Design)') AND (Type = 'Investor') and Has_Contact_with_Phone__c = true];
    List<Task> openFollowUpTasks = [SELECT Id, AccountId FROM Task WHERE AccountId IN :salesReadyAccounts AND IsClosed=false AND Type='Follow Up Call'];
    List<Task> openFollowUpTasksToCancel = [SELECT Id, AccountId FROM Task WHERE AccountId NOT IN :salesReadyAccounts AND AccountId IN :Trigger.newMap.keySet() AND IsClosed=false AND Type='Follow Up Call'];
    for (Task t : openFollowUpTasksToCancel) {
        t.Status = 'Canceled';
    }
    if (!openFollowUpTasksToCancel.isEmpty()) {
        update openFollowUpTasksToCancel;
    }
    Set<Id> accountsWithFollowUpTasks = new Set<Id>{};
    for (Task t : openFollowUpTasks) {
        accountsWithFollowUpTasks.add(t.AccountId);
    }
    List<Task> followUpsToCreate = new List<Task>();
    for (Account a : salesReadyAccounts) {
        if (!accountsWithFollowUpTasks.contains(a.Id)) {
            Task t = new Task();
            t.Subject = 'Następny kontakt telefoniczny';
            t.Type = 'Follow Up Call';
            t.ActivityDate = a.Next_Call__c;
            t.WhatId = a.Id;
            if (a.Owner.IsActive) {
                t.OwnerId = a.OwnerId;
            } else {
                t.OwnerId = a.Owner.Manager.Id;
            }
            t.Teaser__c = a.Teaser__c;
            followUpsToCreate.add(t);
        }
    }
    if (!followUpsToCreate.isEmpty()) {
        insert followUpsToCreate;
    }
    
    Map<Id,Account> changedAccounts = new Map<Id,Account>();
    for (Account a : Trigger.new) {
        if (Trigger.oldMap.get(a.Id).Next_Call__c != Trigger.newMap.get(a.Id).Next_Call__c) {
            changedAccounts.put(a.Id,a);
        }
    }
    List<Task> tasks = [SELECT Id, AccountId FROM Task WHERE AccountId IN :changedAccounts.keySet() AND IsClosed=false AND Type='Follow Up Call'];
    for (Task t : tasks) {
        Account a = changedAccounts.get(t.AccountId);
        t.ActivityDate = a.Next_Call__c;
        t.Teaser__c = a.Teaser__c;
    }
    update tasks;
    */

}