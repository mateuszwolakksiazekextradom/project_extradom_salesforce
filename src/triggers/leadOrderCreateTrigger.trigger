trigger leadOrderCreateTrigger on Lead (after insert) {

    for (Lead l : Trigger.new) {
        
        Map<String, String> params = new Map<String, String>{};
        params.put('utm_source', '');
        params.put('utm_medium', '');
        params.put('utm_campaign', '');
        if (String.isNotBlank(l.Source_URL__c)) {
            PageReference page = new PageReference(l.Source_URL__c);
            for (String key : page.getParameters().keySet()) {
                params.put(key, page.getParameters().get(key));
            }
        }
        
        String BillingPhone = '';
        
        if (l.Phone!=null) {
            BillingPhone = l.Phone.replace(' ','').replace('-','').replace('+48','');
        }
        
        String BillingEmail = l.Email;
        
        if (BillingPhone.length()<9) {
            BillingPhone = '';
        }
        
        Integer countLetters = 0;
        for (Integer i=0; i<BillingPhone.length(); i++) {
            if (!(BillingPhone.substring(i,i+1)).isNumeric()) {
                countLetters += 1;
            }
        }
        
        if (countLetters >= 5) {
            break;
        }
        
        List<Design__c> designCheck = new List<Design__c>();
        if (String.isNotBlank(l.Code__c)) {
            designCheck = [select id, name, code__c, gross_price__c, current_price__c from Design__c where code__c = :l.Code__c];
        }
        
        List<Contact> contactCheck = new List<Contact>();
        List<Contact> phoneCheck = new List<Contact>();
        List<Contact> emailCheck = new List<Contact>();
        
        if (String.isNotBlank(BillingPhone)) {
            phoneCheck = [select id, accountId, salutation, firstname, lastname, phone, email, account.ownerId, account.Sales_Has_Design__c from Contact where phoneId__c = :BillingPhone and account.spam__c = false];
        }
        if (String.isNotBlank(BillingEmail)) {
            emailCheck = [select id, accountId, salutation, firstname, lastname, phone, email, account.ownerId, account.Sales_Has_Design__c from Contact where emailId__c = :BillingEmail and account.spam__c = false];
        }
        
        if (!phoneCheck.isEmpty() && !emailCheck.isEmpty()) {
            // if there is both email and phone for different contact - use phone contact, omit email
            BillingEmail = '';
            contactCheck = phoneCheck;
        } else if (!phoneCheck.isEmpty() && emailCheck.isEmpty()) {
            // if there is phone only contact - use phone contact
            contactCheck = phoneCheck;
        } else if (phoneCheck.isEmpty() && !emailCheck.isEmpty()) {
            // if there is email only contact - use email contact
            contactCheck = emailCheck;
        }
        
        List<AggregateResult> usersLogin = [SELECT userid, min(logintime) firstLogin FROM LoginHistory WHERE logintime = TODAY GROUP BY userid];
        List<GroupMember> members = [SELECT UserOrGroupId FROM GroupMember WHERE GroupId = '00Gb0000000Qpp7'];
        List<GroupMember> lastSeenMembers = New List<GroupMember>();
        
        List<AggregateResult> days = [
        Select DAY_IN_MONTH(loginTime) day
        FROM LoginHistory
        WHERE LoginTime = THIS_MONTH AND LoginTime <> TODAY AND SourceIp = '195.94.192.98'
        AND UserId IN (Select Id From User WHERE IsActive = true AND ProfileId = '00eb0000000I9GZ')
        GROUP BY DAY_IN_MONTH(loginTime)
        HAVING count_distinct(userId) > 1];
        
        Integer[] daysArray = new Integer[]{};
        for (AggregateResult day : days) {
            daysArray.add((Integer)day.get('day'));
        }
        
        /*List<AggregateResult> activeUsers = [
        Select userId
        FROM LoginHistory
        WHERE LoginTime = THIS_MONTH AND LoginTime <> TODAY AND SourceIp = '193.239.58.97'
        AND UserId IN (Select Id From User WHERE IsActive = true AND ProfileId = '00eb0000000I9GZ')
        AND day_in_month(loginTime) in :daysArray
        GROUP BY UserId, day_in_month(loginTime)];*/
        
        List<AggregateResult> activeUsers = [
        Select userId
        FROM LoginHistory
        WHERE LoginTime = THIS_MONTH AND LoginTime <> TODAY AND SourceIp = '195.94.192.98'
        AND UserId IN (Select Id From User WHERE IsActive = true AND ProfileId = '00eb0000000I9GZ')
        GROUP BY UserId, day_in_month(loginTime)];
        
        String orderAgentId = '';
        Double orderAgentRate = 0;
        Integer orderAgentCount = 0;
        
        for (AggregateResult ul : usersLogin) {
           for (GroupMember m : members) {
                if (ul.get('userid')==(String)m.UserOrGroupId) {
                    DateTime qStart = (DateTime)ul.get('firstLogin');
                    Double qHours = (System.now().getTime()-qStart.getTime())/1000.0/60.0/60.0;
                    Double qTotalHours = qHours;
                    for (AggregateResult activeUser : activeUsers) {
                        if (activeUser.get('userId')==(String)m.UserOrGroupId) {
                            qTotalHours += 8.0;
                        }
                    }
                    lastSeenMembers.add(m);
                    if (qHours < 8.0) {
                        Integer qCount = 0;
                        if (l.Code__c == 'TEL') {
                            qCount = [SELECT count() FROM Account WHERE OwnerId = :m.UserOrGroupId AND SPAM__c = false AND AccountSource like '%(Telemarketing)' AND CreatedDate = THIS_MONTH];
                        } else if (l.Code__c == 'CAT') {
                            qCount = [SELECT count() FROM Account WHERE OwnerId = :m.UserOrGroupId AND SPAM__c = false AND AccountSource like '%(Catalogue)' AND CreatedDate = THIS_MONTH];
                        } else if (l.Code__c == 'VCR') {
                            qCount = [SELECT count() FROM Account WHERE OwnerId = :m.UserOrGroupId AND SPAM__c = false AND AccountSource like '%(Voucher)' AND CreatedDate = THIS_MONTH];
                        } else if (l.Source_Type__c == 'Question') {
                            qCount = [SELECT count() FROM Account WHERE OwnerId = :m.UserOrGroupId AND SPAM__c = false AND AccountSource like '%(Question)' AND CreatedDate = THIS_MONTH];
                        } else if (l.Source_Type__c == 'Competition') {
                            qCount = [SELECT count() FROM Account WHERE OwnerId = :m.UserOrGroupId AND SPAM__c = false AND AccountSource like '%(Competition)' AND CreatedDate = THIS_MONTH];
                        } else {
                            qCount = [SELECT count() FROM Account WHERE OwnerId = :m.UserOrGroupId AND SPAM__c = false AND AccountSource like '%(Design)' AND CreatedDate = THIS_MONTH];
                        }
                        Double qRate = qCount / qTotalHours;
                        if (qRate <= orderAgentRate || orderAgentId == '') {
                            orderAgentId = (String)m.UserOrGroupId;
                            orderAgentRate = qRate;
                            orderAgentCount = qCount;
                        }
                    }
                }
            }
        }
        
        /*if (orderAgentId == '' && !lastSeenMembers.isEmpty()) {
            orderAgentId = lastSeenMembers.get(Math.mod(Math.round(Math.random()*1000*lastSeenMembers.size()),lastSeenMembers.size())).userOrGroupId;
        }
        
        if (orderAgentId == '') {
            orderAgentId = members.get(Math.mod(Math.round(Math.random()*1000*members.size()),members.size())).userOrGroupId;
        }*/
        
        if (orderAgentId == '') {
            orderAgentId = '005b0000000RxOP';
        }
        
        String BillingFirstName = l.FirstName;
        String BillingLastName = l.LastName;
        String BillingName = (BillingFirstName==null)?(BillingLastName):(BillingFirstName + ' ' +BillingLastName);
        String BillingAccount = l.Billing_Company__c;
        String BillingCompany = l.Billing_Company__c;
        
        if (BillingAccount == null) {
            BillingAccount = BillingName;
        }
        
        if (l.Source_Type__c != 'Question' && l.Source_Type__c != 'Competition' && l.Code__c != 'TEL') {
        
            String BillingTaxNumber = l.Billing_Tax_Number__c;
            String BillingAddress = l.Billing_Address__c;
            String BillingPostalCode = l.Billing_Postal_Code__c;
            String BillingCity = l.Billing_City__c;
            
            String ShippingName = l.Shipping_Name__c;
            String ShippingAddress = l.Shipping_Address__c;
            String ShippingPostalCode = l.Shipping_Postal_Code__c;
            String ShippingCity = l.Shipping_City__c;
            String ShippingPhone = l.Shipping_Phone__c;
            
            if (ShippingPhone == null) {
                ShippingPhone = BillingPhone;
            }
            
            String ShippingConditions = l.Shipping_Conditions__c;
            String ShippingMethod = l.Shipping_Method__c;
            
            String DesignVersion = l.Design_Version__c;
        
            Order__c o = new Order__c();
            
            o.Status__c='Draft';
            o.Order_Source__c = l.LeadSource;
            o.Source_URL__c = l.Source_URL__c;
            o.Campaign_Name__c = params.get('utm_campaign');
            o.Campaign_Source__c = params.get('utm_source');
            o.Campaign_Medium__c = params.get('utm_medium');
            
            if (l.Code__c == 'CAT') {
                o.Add_Ons__c = 'Catalogue';
            } else if (l.Code__c == 'VCR') {
                o.Add_Ons__c = 'Voucher';
            }
            
            o.Billing_Name__c = BillingName;
            o.Billing_Company__c = BillingCompany;
            o.Billing_Tax_Number__c = BillingTaxNumber;
            o.Billing_Address__c = BillingAddress;
            o.Billing_Postal_Code__c = BillingPostalCode;
            o.Billing_City__c = BillingCity;
            
            o.Shipping_Name__c = ShippingName;
            o.Shipping_Phone__c = ShippingPhone;
            o.Shipping_Address__c = ShippingAddress;
            o.Shipping_Postal_Code__c = ShippingPostalCode;
            o.Shipping_City__c = ShippingCity;
            
            o.Shipping_Method__c = ShippingMethod;
            o.Shipping_Conditions__c = ShippingConditions;
            
            o.Add_Ons_Details__c=l.Add_Ons_Details__c;
            
            if (!designCheck.isEmpty()) {
                Design__c d = designCheck.get(0);
                o.Design__c = d.Id;
                o.Gross_Price__c = d.Current_Price__c;
                o.Quantity__c = 1;
                o.Design_Version__c = DesignVersion;
            }
        
            if (contactCheck.isEmpty()) {
                Account a;
                if (l.Code__c == 'CAT') {
                    a = new Account(Name=BillingAccount, OwnerId=orderAgentId, AccountSource=l.LeadSource+' (Catalogue)');
                } else if (l.Code__c == 'VCR') {
                    a = new Account(Name=BillingAccount, OwnerId=orderAgentId, AccountSource=l.LeadSource+' (Voucher)');
                } else {
                    a = new Account(Name=BillingAccount, OwnerId=orderAgentId, AccountSource=l.LeadSource+' (Design)');
                }
                a.Source_URL__c = l.Source_URL__c;
                a.Campaign_Name__c = params.get('utm_campaign');
                a.Campaign_Source__c = params.get('utm_source');
                a.Campaign_Medium__c = params.get('utm_medium');
                insert a;
                o.Account__c = a.Id;
                insert o;
                Contact c = new Contact(FirstName=BillingFirstName, LastName=BillingLastName, AccountId=a.Id, Phone=BillingPhone, Email=BillingEmail, OwnerId=orderAgentId);
                insert c;
            } else {
                Contact c = contactCheck.get(0);
                o.Account__c = c.accountId;
                insert o;
            }
            
        } else {
            
            Task t = New Task(ActivityDate = System.today(), Priority = 'Normal', Status = 'Not Started', Source_URL__c=l.Source_URL__c);
            
            String leadType = 'Question';
            
            if (l.Code__c == 'TEL') {
                leadType = 'Telemarketing';
            } else if (l.Source_Type__c == 'Competition') {
                leadType = 'Competition';
            } else {
                leadType = 'Question';
            }
            
            String hasDesign = '';
            
            if (l.Has_Design__c == 'Yes' || l.Has_Design__c == 'No') {
                hasDesign = l.Has_Design__c;
            }
            
            if (contactCheck.isEmpty()) {
                Account a = new Account(Name=BillingEmail, OwnerId=orderAgentId, AccountSource=l.LeadSource+' ('+leadType+')', Sales_Has_Design__c=hasDesign);
                insert a;
                Contact c = new Contact(LastName=BillingEmail, AccountId=a.Id, Phone=BillingPhone, Email=BillingEmail, OwnerId=orderAgentId, Phone_Opt_In__c=l.Phone_Opt_In__c);
                insert c;
                t.WhoId = c.Id;
                t.OwnerId = orderAgentId;
            } else if (!emailCheck.isEmpty() && phoneCheck.isEmpty()) {
                Contact c = emailCheck.get(0);
                if (String.isBlank(c.Account.Sales_Has_Design__c)) {
                    c.Account.Sales_Has_Design__c = hasDesign;
                    update c.Account;
                }
                if (c.Account.OwnerId=='005b0000000RxOP') {
                    c.Account.OwnerId = orderAgentId;
                    c.OwnerId = orderAgentId;
                    update c.Account;
                }
                t.WhoId = c.Id;
                if (String.isBlank(c.Phone)) {
                    c.Phone = BillingPhone;
                    c.Phone_Opt_In__c=l.Phone_Opt_In__c;
                } else {
                    Contact c1 = new Contact(LastName=BillingPhone, AccountId=c.AccountId, Phone=BillingPhone, OwnerId=c.Account.OwnerId, Phone_Opt_In__c=l.Phone_Opt_In__c);
                    insert c1;
                }
                if (l.Email_Opt_In__c) {
                    c.HasOptedOutOfEmail = false;
                }
                update c;
                t.OwnerId = orderAgentId;
                for (GroupMember gm : members) {
                    if (c.account.ownerId == gm.UserOrGroupId) {
                        t.OwnerId = c.account.ownerId;
                    }
                }
            } else if (emailCheck.isEmpty() && !phoneCheck.isEmpty()) {
                Contact c = phoneCheck.get(0);
                if (String.isBlank(c.Account.Sales_Has_Design__c)) {
                    c.Account.Sales_Has_Design__c = hasDesign;
                    update c.Account;
                }
                if (c.Account.OwnerId=='005b0000000RxOP') {
                    c.Account.OwnerId = orderAgentId;
                    c.OwnerId = orderAgentId;
                    update c.Account;
                }
                t.WhoId = c.Id;
                if (String.isBlank(c.Email)) {
                    c.Email = BillingEmail;
                } else {
                    Contact c1 = new Contact(LastName=BillingEmail, AccountId=c.AccountId, Email=BillingEmail, OwnerId=c.Account.OwnerId);
                    insert c1;
                    t.WhoId = c1.Id;
                }
                if (l.Phone_Opt_In__c) {
                    c.Phone_Opt_In__c = true;
                }
                update c;
                t.OwnerId = orderAgentId;
                for (GroupMember gm : members) {
                    if (c.account.ownerId == gm.UserOrGroupId) {
                        t.OwnerId = c.account.ownerId;
                    }
                }
            } else {
                Contact c = emailCheck.get(0);
                if (String.isBlank(c.Account.Sales_Has_Design__c)) {
                    c.Account.Sales_Has_Design__c = hasDesign;
                    update c.Account;
                }
                if (c.Account.OwnerId=='005b0000000RxOP') {
                    c.Account.OwnerId = orderAgentId;
                    c.OwnerId = orderAgentId;
                    update c.Account;
                }
                if (l.Email_Opt_In__c) {
                    c.HasOptedOutOfEmail = false;
                }
                if (emailCheck.get(0).id==phoneCheck.get(0).id) {
                    if (l.Phone_Opt_In__c) {
                        c.Phone_Opt_In__c = true;
                    }
                }
                update c;
                t.WhoId = c.Id;
                t.OwnerId = orderAgentId;
                for (GroupMember gm : members) {
                    if (c.account.ownerId == gm.UserOrGroupId) {
                        t.OwnerId = c.account.ownerId;
                    }
                }
            }
            
            if (l.Code__c == 'TEL') {
                t.Type = 'Telemarketing: Request';
                t.Subject = l.Description;
                t.Description = l.LastName + ', ' + BillingPhone;
                
                try {
                    t.WhatId = l.Partner_ID__c;
                } catch (Exception e) {
                }
                
                try {
                    t.Partner_First_Name__c = l.FirstName;
                } catch (Exception e) {
                }
                
                try {
                    t.Partner_Last_Name__c = l.LastName;
                } catch (Exception e) {
                }
                
                try {
                    t.Partner_Email__c = l.Email;
                } catch (Exception e) {
                }
                
                try {
                    t.Partner_Phone__c = l.Phone;
                } catch (Exception e) {
                }
                
                try {
                    t.Partner_ID__c = l.Partner_ID__c;
                } catch (Exception e) {
                }
                
                List<Group> telReqGroups = [Select (Select UserOrGroupId From GroupMembers) From Group g WHERE g.Name = 'Telemarketing: Requests'];
                for (Group g : telReqGroups) {
                    for (GroupMember m: g.GroupMembers) {
                        t.OwnerId = m.UserOrGroupId;
                    }
                }
                
            } else if (l.Source_Type__c == 'Competition') {
                
                t.Type = 'Competition';
                t.Description = BillingFirstName + ' ' + BillingLastName + '\n' + l.Email + '\n' + l.Phone + '\n\n' + l.description;
                t.Subject = 'Zgłoszenie konkursowe ze strony '+l.LeadSource+': '+l.Source_URL__c;
                
            } else {
            
                t.Type = 'Question';
                t.Description=l.description;
                
                if (!designCheck.isEmpty()) {
                    Design__c d = designCheck.get(0);
                    t.WhatId = d.Id;
                    t.Subject = 'Pytanie ze strony '+l.LeadSource+': '+d.Name;
                } else {
                    t.Subject = 'Pytanie ze strony '+l.LeadSource+': '+l.Source_URL__c;
                }
                
            }
            
            insert t;
            
        }
        
        //if (String.isNotBlank(l.Email)) {
            //l.Processed_Email__c = l.Email;
            //l.Email = '';
        //}
        
    }

}