trigger designDeleteControl on Design__c (before delete) {
    for (Design__c d : Trigger.old) {
        if (String.isNotBlank(d.Code__c)) {
            d.addError('You cannot delete the record since Design Code is assigned');
        }
    }
}