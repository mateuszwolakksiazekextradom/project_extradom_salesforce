trigger afterInsertChatCase on Case (after insert, after update) {
    
    for (Case cs : Trigger.new) {

        if (cs.Subject.startsWith('Olark chat')) {
            List<Task> tasks = [SELECT Id, OwnerId FROM Task WHERE Case_Number__c = :cs.CaseNumber];
            for (Task t : tasks) {
                t.Description = cs.Description;
            }
            if (tasks.isEmpty()) {
                Task t = new Task(Case_Number__c=cs.CaseNumber, Subject=cs.Subject+' (#'+cs.CaseNumber+')'+' '+cs.SuppliedPhone, Description=cs.Description, OwnerId=cs.OwnerId, WhoId=cs.ContactId, Status='Not Started', Type='Chat', ActivityDate=System.today());
                tasks.add(t);
            }
            upsert tasks;
        }
        
    }
    
    if (Trigger.isInsert) {
        
        Map<String, Account> partnerAccounts = new Map<String, Account>{};
        for (Account a: [SELECT Id, Partner_Id__c FROM Account WHERE Partner_Id__c != null]) {
            partnerAccounts.put(a.Partner_Id__c, a);
        }
        Map<String, Design__c> designs = new Map<String, Design__c>{};
        for (Design__c d: [SELECT Id, Code__c, Gross_Price__c, Current_Price__c FROM Design__c WHERE Code__c != null]) {
            designs.put(d.Code__c, d);
        }
        List<Order__c> ordersToInsert = new List<Order__c>{};
        for (Case cs : Trigger.new) {
            if (cs.Type=='Design Order') {
                Order__c o = new Order__c();
                o.Account__c = cs.AccountId;
                o.Status__c = 'Draft';
                if (String.isNotBlank(cs.Source_Partner_ID__c)) {
                    if (partnerAccounts.containsKey(cs.Source_Partner_ID__c)) {
                        Account a = partnerAccounts.get(cs.Source_Partner_ID__c);
                        o.Source_Account__c = a.Id;
                    }
                }
                if (String.isNotBlank(cs.Order_Code__c)) {
                    if (designs.containsKey(cs.Order_Code__c)) {
                        Design__c d = designs.get(cs.Order_Code__c);
                        o.Design__c = d.Id;
                        o.Gross_Price__c = d.Current_Price__c;
                    }
                    o.Quantity__c = 1;
                }
                o.Design_Version__c = cs.Order_Code_Version__c;
                o.Source_URL__c = cs.Source_URL__c.left(255);
                o.Special_Conditions__c = cs.Description;
                o.Shipping_Method__c = cs.Shipping_Method__c;
                o.Billing_Name__c = cs.SuppliedName + ' ' + cs.Supplied_Last_Name__c;
                o.Billing_Company__c = cs.SuppliedCompany;
                o.Billing_Tax_Number__c = cs.Supplied_VAT_No__c;
                o.Billing_Address__c = cs.Supplied_Street__c;
                if (String.isNotBlank(cs.Supplied_Street_No__c)) {
                    o.Billing_Address__c += ' ' + cs.Supplied_Street_No__c;
                }
                if (String.isNotBlank(cs.Supplied_Street_Flat__c)) {
                    o.Billing_Address__c += '/' + cs.Supplied_Street_Flat__c;
                }
                o.Billing_Postal_Code__c = cs.Supplied_Postal_Code__c;
                o.Billing_City__c = cs.Supplied_City__c;
                o.Billing_Phone__c = cs.SuppliedPhone;
                o.Billing_Email__c = cs.SuppliedEmail;
                o.Shipping_Name__c = cs.Shipping_Name__c;
                o.Shipping_Phone__c = cs.Shipping_Phone__c;
                o.Shipping_Address__c = cs.Shipping_Street__c;
                if (String.isNotBlank(cs.Shipping_Street_No__c)) {
                    o.Shipping_Address__c += ' ' + cs.Shipping_Street_No__c;
                }
                if (String.isNotBlank(cs.Shipping_Street_Flat__c)) {
                    o.Shipping_Address__c += '/' + cs.Shipping_Street_Flat__c;
                }
                o.Shipping_Postal_Code__c = cs.Shipping_Postal_Code__c;
                o.Shipping_City__c = cs.Shipping_City__c;
                o.Agreement_Website_Regulations__c = cs.Agreement_Website_Regulations__c;
                o.Agreement_Global_Transfer__c = cs.Agreement_Global_Transfer__c;
                o.Agreement_Commercial_Contact__c = cs.Agreement_Commercial_Contact__c;
                ordersToInsert.add(o);
            }
        }
        insert ordersToInsert;
    }
    
}