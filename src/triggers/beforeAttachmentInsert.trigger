trigger beforeAttachmentInsert on Attachment (before insert) {
    
    for (Attachment att : Trigger.New) {
        if (String.isBlank(att.ContentType) && att.Name.endsWith('.babylon')) {
            att.ContentType = 'application/javascript';
        }
    }
	
}