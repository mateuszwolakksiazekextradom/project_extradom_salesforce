trigger designNameIdTrigger on Design__c (after insert, after update) {

  // This queries all Media related to the incoming Design records in a single SOQL query.
  List<Design__c> designsWithMedia = [select id, Design_Name_ID__c, (select id, Media_Name_ID__c, Media_Name_ID_Postfix__c from Media__r) from Design__c where Id IN :Trigger.newMap.keySet()];

  List<Media__c> mediaToUpdate = new List<Media__c>{};
  // For loop to iterate through all the queried Design records 
  for (Design__c d: designsWithMedia) {
     // Use the child relationships dot syntax to access the related Media
     for (Media__c m: d.Media__r) {
     	if(m.Media_Name_ID__c != d.Design_Name_ID__c + ': ' + m.Media_Name_ID_Postfix__c) {
			m.Media_Name_ID__c = d.Design_Name_ID__c + ': ' + m.Media_Name_ID_Postfix__c;
			mediaToUpdate.add(m);
     	}
     }
     
   }
      
   //Now outside the FOR Loop, perform a single Update DML statement. 
   update mediaToUpdate;
	
}