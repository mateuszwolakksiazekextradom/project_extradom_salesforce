trigger PlotAnalysisTrigger on Plot_Analysis__c (before insert) {
    new PlotAnalysisTriggerHandler().run();
}