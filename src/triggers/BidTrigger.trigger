/**
 * Created by grzegorz.dlugosz on 27.05.2019.
 */
trigger BidTrigger on Bid__c (before insert, before update, before delete, after insert, after update, after delete) {
    new BidTriggerHandler().run();
}