trigger filtersUpdate on Design__c (before insert, before update) {
    for (Design__c d : Trigger.new) {
        if (String.isNotBlank(d.Category_Keys__c)) {
            if (d.Category_Keys__c.contains('[46]')) { d.basement_46__c = 1; } else { d.basement_46__c = 0; }
            if (d.Category_Keys__c.contains('[72]')) { d.basement_72__c = 1; } else { d.basement_72__c = 0; }
            if (d.Category_Keys__c.contains('[114]')) { d.basement_114__c = 1; } else { d.basement_114__c = 0; }
            if (d.Border_Ready__c=='tak') { d.border_118__c = 1; } else { d.border_118__c = 0; }
            if (d.Bullseye__c=='tak') { d.bullseye_122__c = 1; } else { d.bullseye_122__c = 0; }
            if (d.Category_Keys__c.contains('[80]')) { d.carport_80__c = 1; } else { d.carport_80__c = 0; }
            if (d.Category_Keys__c.contains('[95]')) { d.ceiling_95__c = 1; } else { d.ceiling_95__c = 0; }
            if (d.Category_Keys__c.contains('[96]')) { d.ceiling_96__c = 1; } else { d.ceiling_96__c = 0; }
            if (d.Category_Keys__c.contains('[97]')) { d.ceiling_97__c = 1; } else { d.ceiling_97__c = 0; }
            if (d.Category_Keys__c.contains('[99]')) { d.ceiling_99__c = 1; } else { d.ceiling_99__c = 0; }
            if (d.Category_Keys__c.contains('[38]')) { d.construction_38__c = 1; } else { d.construction_38__c = 0; }
            if (d.Category_Keys__c.contains('[39]')) { d.construction_39__c = 1; } else { d.construction_39__c = 0; }
            if (d.Category_Keys__c.contains('[40]')) { d.construction_40__c = 1; } else { d.construction_40__c = 0; }
            if (d.Category_Keys__c.contains('[41]')) { d.construction_41__c = 1; } else { d.construction_41__c = 0; }
            if (d.Category_Keys__c.contains('[82]')) { d.construction_82__c = 1; } else { d.construction_82__c = 0; }
            if (d.Category_Keys__c.contains('[125]')) { d.construction_125__c = 1; } else { d.construction_125__c = 0; }
            if (d.Ridge__c=='równoległa do drogi') { d.ridge_105__c = 1; } else { d.ridge_105__c = 0; }
            if (d.Ridge__c=='prostopadła do drogi') { d.ridge_106__c = 1; } else { d.ridge_106__c = 0; }
            if (d.Category_Keys__c.contains('[102]')) { d.cost_102__c = 1; } else { d.cost_102__c = 0; }
            if (d.Eaves__c=='nie') { d.eaves_124__c = 1; } else { d.eaves_124__c = 0; }
            if (d.Category_Keys__c.contains('[83]')) { d.eco_83__c = 1; } else { d.eco_83__c = 0; }
            if (d.Category_Keys__c.contains('[84]')) { d.eco_84__c = 1; } else { d.eco_84__c = 0; }
            if (d.Category_Keys__c.contains('[45]')) { d.entrance_45__c = 1; } else { d.entrance_45__c = 0; }
            if (d.Fireplace__c=='tak') { d.fireplace_123__c = 1; } else { d.fireplace_123__c = 0; }
            if (d.Category_Keys__c.contains('[111]')) { d.garage_111__c = 1; } else { d.garage_111__c = 0; }
            if (d.Category_Keys__c.contains('[50]')) { d.garage_50__c = 1; } else { d.garage_50__c = 0; }
            if (d.Category_Keys__c.contains('[79]')) { d.garage_79__c = 1; } else { d.garage_79__c = 0; }
            if (d.Category_Keys__c.contains('[52]')) { d.garage_52__c = 1; } else { d.garage_52__c = 0; }
            if (d.Category_Keys__c.contains('[65]')) { d.garage_65__c = 1; } else { d.garage_65__c = 0; }
            if (d.Category_Keys__c.contains('[66]')) { d.garagelocation_66__c = 1; } else { d.garagelocation_66__c = 0; }
            if (d.Category_Keys__c.contains('[67]')) { d.garagelocation_67__c = 1; } else { d.garagelocation_67__c = 0; }
            if (d.Category_Keys__c.contains('[70]')) { d.garagelocation_70__c = 1; } else { d.garagelocation_70__c = 0; }
            if (d.Category_Keys__c.contains('[73]')) { d.garagelocation_73__c = 1; } else { d.garagelocation_73__c = 0; }
            if (d.Category_Keys__c.contains('[78]')) { d.garagelocation_78__c = 1; } else { d.garagelocation_78__c = 0; }
            if (d.Garage_Location__c == 'garaż podziemny') { d.garagelocation_126__c = 1; } else { d.garagelocation_126__c = 0; }
            if (d.Category_Keys__c.contains('[119]')) { d.gfroom_119__c = 1; } else { d.gfroom_119__c = 0; }
            if (d.Category_Keys__c.contains('[47]')) { d.level_47__c = 1; } else { d.level_47__c = 0; }
            if (d.Category_Keys__c.contains('[48]')) { d.level_48__c = 1; } else { d.level_48__c = 0; }
            if (d.Category_Keys__c.contains('[63]')) { d.level_63__c = 1; } else { d.level_63__c = 0; }
            if (d.Category_Keys__c.contains('[127]')) { d.level_127__c = 1; } else { d.level_127__c = 0; }
            if (d.Category_Keys__c.contains('[42]')) { d.livingtype_42__c = 1; } else { d.livingtype_42__c = 0; }
            if (d.Category_Keys__c.contains('[43]')) { d.livingtype_43__c = 1; } else { d.livingtype_43__c = 0; }
            if (d.Category_Keys__c.contains('[49]')) { d.livingtype_49__c = 1; } else { d.livingtype_49__c = 0; }
            if (d.Category_Keys__c.contains('[113]')) { d.livingtype_113__c = 1; } else { d.livingtype_113__c = 0; }
            if (d.Category_Keys__c.contains('[94]')) { d.mezzanine_94__c = 1; } else { d.mezzanine_94__c = 0; }
            if (d.Category_Keys__c.contains('[64]')) { d.oriel_64__c = 1; } else { d.oriel_64__c = 0; }
            if (d.Category_Keys__c.contains('[71]')) { d.pool_71__c = 1; } else { d.pool_71__c = 0; }
            if (d.Category_Keys__c.contains('[12]')) { d.roof_12__c = 1; } else { d.roof_12__c = 0; }
            if (d.Category_Keys__c.contains('[13]')) { d.roof_13__c = 1; } else { d.roof_13__c = 0; }
            if (d.Category_Keys__c.contains('[3]')) { d.roof_3__c = 1; } else { d.roof_3__c = 0; }
            if (d.Category_Keys__c.contains('[4]')) { d.roof_4__c = 1; } else { d.roof_4__c = 0; }
            if (d.Category_Keys__c.contains('[5]')) { d.roof_5__c = 1; } else { d.roof_5__c = 0; }
            if (d.Category_Keys__c.contains('[6]')) { d.roof_6__c = 1; } else { d.roof_6__c = 0; }
            if (d.Category_Keys__c.contains('[7]')) { d.roof_7__c = 1; } else { d.roof_7__c = 0; }
            if (d.Category_Keys__c.contains('[8]')) { d.roof_8__c = 1; } else { d.roof_8__c = 0; }
            if (d.Category_Keys__c.contains('[9]')) { d.roof_9__c = 1; } else { d.roof_9__c = 0; }
            if (d.Scarp__c=='tak') { d.scarp_121__c = 1; } else { d.scarp_121__c = 0; }
            if (d.Category_Keys__c.contains('[44]')) { d.shape_44__c = 1; } else { d.shape_44__c = 0; }
            if (d.Category_Keys__c.contains('[56]')) { d.shape_56__c = 1; } else { d.shape_56__c = 0; }
            if (d.Category_Keys__c.contains('[57]')) { d.shape_57__c = 1; } else { d.shape_57__c = 0; }
            if (d.Category_Keys__c.contains('[77]')) { d.style_77__c = 1; } else { d.style_77__c = 0; }
            if (d.Category_Keys__c.contains('[81]')) { d.style_81__c = 1; } else { d.style_81__c = 0; }
            if (d.Category_Keys__c.contains('[90]')) { d.style_90__c = 1; } else { d.style_90__c = 0; }
            if (d.Category_Keys__c.contains('[98]')) { d.style_98__c = 1; } else { d.style_98__c = 0; }
            if (d.Category_Keys__c.contains('[104]')) { d.style_104__c = 1; } else { d.style_104__c = 0; }
            if (d.Category_Keys__c.contains('[92]')) { d.style_92__c = 1; } else { d.style_92__c = 0; }
            if (d.Category_Keys__c.contains('[91]')) { d.style_91__c = 1; } else { d.style_91__c = 0; }
            if (d.Category_Keys__c.contains('[120]')) { d.style_120__c = 1; } else { d.style_120__c = 0; }
            if (d.Category_Keys__c.contains('[69]')) { d.terrace_69__c = 1; } else { d.terrace_69__c = 0; }
            if (d.Category_Keys__c.contains('[1]')) { d.type_1__c = 1; } else { d.type_1__c = 0; }
            if (d.Category_Keys__c.contains('[2]')) { d.type_2__c = 1; } else { d.type_2__c = 0; }
            if (d.Category_Keys__c.contains('[10]')) { d.type_10__c = 1; } else { d.type_10__c = 0; }
            if (d.Category_Keys__c.contains('[11]')) { d.type_11__c = 1; } else { d.type_11__c = 0; }
            if (d.Category_Keys__c.contains('[14]')) { d.type_14__c = 1; } else { d.type_14__c = 0; }
            if (d.Category_Keys__c.contains('[15]')) { d.type_15__c = 1; } else { d.type_15__c = 0; }
            if (d.Category_Keys__c.contains('[16]')) { d.type_16__c = 1; } else { d.type_16__c = 0; }
            if (d.Category_Keys__c.contains('[17]')) { d.type_17__c = 1; } else { d.type_17__c = 0; }
            if (d.Category_Keys__c.contains('[18]')) { d.type_18__c = 1; } else { d.type_18__c = 0; }
            if (d.Category_Keys__c.contains('[19]')) { d.type_19__c = 1; } else { d.type_19__c = 0; }
            if (d.Category_Keys__c.contains('[20]')) { d.type_20__c = 1; } else { d.type_20__c = 0; }
            if (d.Category_Keys__c.contains('[21]')) { d.type_21__c = 1; } else { d.type_21__c = 0; }
            if (d.Category_Keys__c.contains('[22]')) { d.type_22__c = 1; } else { d.type_22__c = 0; }
            if (d.Category_Keys__c.contains('[23]')) { d.type_23__c = 1; } else { d.type_23__c = 0; }
            if (d.Category_Keys__c.contains('[24]')) { d.type_24__c = 1; } else { d.type_24__c = 0; }
            if (d.Category_Keys__c.contains('[25]')) { d.type_25__c = 1; } else { d.type_25__c = 0; }
            if (d.Category_Keys__c.contains('[26]')) { d.type_26__c = 1; } else { d.type_26__c = 0; }
            if (d.Category_Keys__c.contains('[27]')) { d.type_27__c = 1; } else { d.type_27__c = 0; }
            if (d.Category_Keys__c.contains('[28]')) { d.type_28__c = 1; } else { d.type_28__c = 0; }
            if (d.Category_Keys__c.contains('[29]')) { d.type_29__c = 1; } else { d.type_29__c = 0; }
            if (d.Category_Keys__c.contains('[30]')) { d.type_30__c = 1; } else { d.type_30__c = 0; }
            if (d.Category_Keys__c.contains('[31]')) { d.type_31__c = 1; } else { d.type_31__c = 0; }
            if (d.Category_Keys__c.contains('[32]')) { d.type_32__c = 1; } else { d.type_32__c = 0; }
            if (d.Category_Keys__c.contains('[33]')) { d.type_33__c = 1; } else { d.type_33__c = 0; }
            if (d.Category_Keys__c.contains('[34]')) { d.type_34__c = 1; } else { d.type_34__c = 0; }
            if (d.Category_Keys__c.contains('[36]')) { d.type_36__c = 1; } else { d.type_36__c = 0; }
            if (d.Category_Keys__c.contains('[37]')) { d.type_37__c = 1; } else { d.type_37__c = 0; }
            if (d.Category_Keys__c.contains('[53]')) { d.type_53__c = 1; } else { d.type_53__c = 0; }
            if (d.Category_Keys__c.contains('[54]')) { d.type_54__c = 1; } else { d.type_54__c = 0; }
            if (d.Category_Keys__c.contains('[58]')) { d.type_58__c = 1; } else { d.type_58__c = 0; }
            if (d.Category_Keys__c.contains('[75]')) { d.type_75__c = 1; } else { d.type_75__c = 0; }
            if (d.Category_Keys__c.contains('[85]')) { d.type_85__c = 1; } else { d.type_85__c = 0; }
            if (d.Category_Keys__c.contains('[86]')) { d.type_86__c = 1; } else { d.type_86__c = 0; }
            if (d.Category_Keys__c.contains('[87]')) { d.type_87__c = 1; } else { d.type_87__c = 0; }
            if (d.Category_Keys__c.contains('[88]')) { d.type_88__c = 1; } else { d.type_88__c = 0; }
            if (d.Category_Keys__c.contains('[89]')) { d.type_89__c = 1; } else { d.type_89__c = 0; }
            if (d.Category_Keys__c.contains('[93]')) { d.type_93__c = 1; } else { d.type_93__c = 0; }
            if (d.Category_Keys__c.contains('[100]')) { d.type_100__c = 1; } else { d.type_100__c = 0; }
            if (d.Category_Keys__c.contains('[101]')) { d.type_101__c = 1; } else { d.type_101__c = 0; }
            if (d.Category_Keys__c.contains('[103]')) { d.type_103__c = 1; } else { d.type_103__c = 0; }
            if (d.Category_Keys__c.contains('[108]')) { d.type_108__c = 1; } else { d.type_108__c = 0; }
            if (d.Category_Keys__c.contains('[109]')) { d.type_109__c = 1; } else { d.type_109__c = 0; }
            if (d.Category_Keys__c.contains('[110]')) { d.type_110__c = 1; } else { d.type_110__c = 0; }
            if (d.Category_Keys__c.contains('[116]')) { d.type_116__c = 1; } else { d.type_116__c = 0; }
            if (d.Category_Keys__c.contains('[117]')) { d.type_117__c = 1; } else { d.type_117__c = 0; }
            if (d.Category_Keys__c.contains('[68]')) { d.veranda_68__c = 1; } else { d.veranda_68__c = 0; }
            if (d.Category_Keys__c.contains('[76]')) { d.wintergarden_76__c = 1; } else { d.wintergarden_76__c = 0; }
        }
    }
}