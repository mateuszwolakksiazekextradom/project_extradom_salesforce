trigger mediaAfterCreateAfterDelete on Media__c (after insert, after delete) {
    
    Set<Id> ids = new Set<Id>(); 
    List<Id> designIds = new List<Id>();
    for (Media__c m : Trigger.isInsert ? Trigger.New : Trigger.Old) {
        if (m.Category__c == 'Document PDF') {
            ids.add(m.Design__c);
        } else if (m.Category__c == '3D Rotation') {
            designIds.add(m.Design__c);
        }
    }
    
    if (!ids.isEmpty()) MediaExtension.fillDocuments(ids);
    
    if (!designIds.isEmpty()) {
        List<Design__c> designsToUpdate = new List<Design__c>();
        for (Design__c d : [SELECT Id, Has_Panorama__c, (SELECT Id FROM Media__r WHERE Category__c = '3D Rotation') FROM Design__c WHERE Id IN :designIds]) {
            if (d.Media__r.isEmpty() && d.Has_Panorama__c) {
                d.Has_Panorama__c = false;
                designsToUpdate.add(d);
            } else if (!d.Media__r.isEmpty() && !d.Has_Panorama__c) {
                d.Has_Panorama__c = true;
                designsToUpdate.add(d);
            }       
        }
        mediaExtension.pdfs = true;
        update designsToUpdate;
    }   
}