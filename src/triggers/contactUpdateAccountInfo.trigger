trigger contactUpdateAccountInfo on Contact (after insert, after update) {

    Map<Id,Boolean> phonesToChange = new Map<id,Boolean>{};
    Map<Id,Boolean> emailsToChange = new Map<id,Boolean>{};

    for (Contact c : [Select Id, AccountId From Contact where Id IN :Trigger.newMap.keyset()]) {
        phonesToChange.put(c.AccountId, false);
        emailsToChange.put(c.AccountId, false);
    }


    for (Contact cs : [SELECT Id, Phone, Email, AccountId, Account.Has_Contact_with_Phone__c, Account.Has_Contact_with_Email__c FROM Contact WHERE AccountId IN :phonesToChange.keyset()]) {
        
        if (String.isNotBlank(cs.Phone)) {
            phonesToChange.put(cs.AccountId, true);
        } else {
            phonesToChange.put(cs.AccountId, phonesToChange.get(cs.AccountId) || false);
        }
        
        if (String.isNotBlank(cs.Email)){
            emailsToChange.put(cs.AccountId, true);
        } else {
            emailsToChange.put(cs.AccountId, emailsToChange.get(cs.AccountId) || false);
        }
    }
    
    Boolean toUpdate = false;
    
    List<account> accounts = [SELECT Id, Has_Contact_with_Phone__c, Has_Contact_with_Email__c FROM Account WHERE Id IN :phonesToChange.keyset()];
    for (Account a : accounts) {
        if(a.Has_Contact_with_Phone__c != phonesToChange.get(a.Id)){
            a.Has_Contact_with_Phone__c = phonesToChange.get(a.Id);
            toUpdate = true;
            }
        if(a.Has_Contact_with_Email__c != emailsToChange.get(a.Id)){
            a.Has_Contact_with_Email__c = emailsToChange.get(a.Id);
            toUpdate = true;
            }
    }
    if(toUpdate){
        update accounts;
    }
}