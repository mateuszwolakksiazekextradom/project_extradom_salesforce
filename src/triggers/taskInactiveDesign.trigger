trigger taskInactiveDesign on Task (before insert) {
    
    List<Id> ids = new List<Id>{};
    
    for (Task t : Trigger.new) {        
        if (String.isNotBlank(t.WhatId)) {
            if (String.valueOf(t.WhatId).left(3) == 'a06') {
                ids.add(t.WhatId);
            }
        }
    }
    
    if (!ids.isEmpty()) { 
        Set<ID> invalidIds = new Set<ID>{};
        for (Design__c d : [SELECT Id, Status__c FROM Design__c WHERE id IN :ids]) {
            if (d.Status__c != 'Active' && d.Status__c != 'Draft' && d.Status__c != 'Version') {
                invalidIds.add(d.Id);
            }
        }
        
        for (Task t : Trigger.new) {
           if (String.isNotBlank(t.WhatId)) {
               if (invalidIds.contains(t.WhatId)) {
                   t.WhatId.addError('Wybrany projekt jest wycofany z oferty');
               }
           }
        }
    }
}