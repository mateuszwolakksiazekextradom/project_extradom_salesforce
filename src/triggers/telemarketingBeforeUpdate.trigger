trigger telemarketingBeforeUpdate on Telemarketing__c (before update) {

    List<Telemarketing__c> tels = new List<Telemarketing__c>();
    for (Telemarketing__c tel : trigger.new) {
        if (String.Isempty(tel.Notus_Id__c) && trigger.oldMap.get(tel.Id).Post_Conversion_Stage__c != 'In Progress' && tel.Post_Conversion_Stage__c == 'In Progress' && tel.Name == 'DK Notus') {
            tels.add(tel);
        }
    }
    if (!tels.isEmpty() && NotusToolsQueue.firstRun) {
        NotusToolsQueue.firstRun = false;
        System.enqueueJob(new NotusToolsQueue(tels));
    }
}