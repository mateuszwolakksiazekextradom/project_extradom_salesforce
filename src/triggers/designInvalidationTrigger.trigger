trigger designInvalidationTrigger on Design__c (after update) {

    Map<ID, Design__c> oldMap = new Map<ID, Design__c>(Trigger.old);

    List<Id> designIds = new List<Id>();

    for (Design__c d: Trigger.new) {
        if (d.Update_Date__c != oldMap.get(d.Id).Update_Date__c){
            designIds.add(d.Id);
        }
    }
    
    if (designIds.size() > 0 && !MediaExtension.pdfs) {
        DesignExtension.invalidateDesigns(designIds);
    }
}